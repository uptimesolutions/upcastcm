/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package com.uptime.upcastcm.http.utils.json.dom;

import com.uptime.upcastcm.utils.vo.AssetVO;
import com.uptime.upcastcm.utils.vo.PointLocationVO;
import com.uptime.upcastcm.utils.vo.PointVO;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Joseph
 */
public class AssetDomJsonParserTest {
    
    private static AssetDomJsonParser instance;
    public AssetDomJsonParserTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        instance = AssetDomJsonParser.getInstance();
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    
    /**
     * Test of getJsonFromAssetVO method, of class AssetDomJsonParser.
     */
    @Test
    public void testGetJsonFromAssetVO() {
        System.out.println("getJsonFromAssetVO");
        AssetVO assetVO = new AssetVO();
        assetVO.setAreaName("Test Area");
        assetVO.setAssetId(UUID.randomUUID());
        assetVO.setAssetName("Test Asset Name");
        assetVO.setAssetType("Test Asset Type");
        assetVO.setCustomerAccount("77777");
        assetVO.setDescription("Test Description");
        assetVO.setSampleInterval(2);
        assetVO.setSiteName("Test Site Name");
        
        List<PointLocationVO> plvoList = new ArrayList<>();
        PointLocationVO plvo = new PointLocationVO();
        plvo.setAreaName(assetVO.getAreaName());
        plvo.setAssetId(assetVO.getAssetId());
        plvo.setAssetName(assetVO.getAssetName());
        plvo.setCustomerAccount(assetVO.getCustomerAccount());
        plvo.setDescription("Point Loc 1 Desc");
        plvo.setDeviceSerialNumber("Point Loc 1 Dev Serial Num");
        plvo.setFfSetNames("Point Loc 1 FF Set");
        plvo.setPointLocationName("Point Loc 1");
        plvo.setSiteName(assetVO.getSiteName());
        plvo.setSpeedRatio(111);
        plvo.setTachName("Point Loc 1 Tach Name");
  
        List<PointVO> pvo1List = new ArrayList<>();
        PointVO pvo = new PointVO();
        pvo.setAlSetName("Point 1 AL Set");
        pvo.setApSetName("Point 1 AP Set");
        pvo.setAreaName(assetVO.getAreaName());
        pvo.setAssetName(assetVO.getAssetName());
        pvo.setCustomerAccount(assetVO.getCustomerAccount());
        pvo.setDisabled(true);
        pvo.setPointLocationName("Point Loc 1");
        pvo.setPointName("Point 1");
        pvo.setPointType("Point Type 1");
        pvo.setSensorChannelNum(11);
        pvo.setSensorOffset(12);
        pvo.setSensorSensitivity(13);
        pvo.setSensorType("Point 1 Sensor Type");
        pvo.setSensorUnits("Point 1 Sensor Unit");
        pvo.setSiteName(assetVO.getSiteName());
        pvo.setCustomized(true);
        pvo1List.add(pvo);
        plvo.setPointList(pvo1List);
        plvoList.add(plvo);
        
        plvo = new PointLocationVO();
        plvo.setAreaName(assetVO.getAreaName());
        plvo.setAssetId(assetVO.getAssetId());
        plvo.setAssetName(assetVO.getAssetName());
        plvo.setCustomerAccount(assetVO.getCustomerAccount());
        plvo.setDescription("Point Loc 2 Desc");
        plvo.setDeviceSerialNumber("Point Loc 2 Dev Serial Num");
        plvo.setFfSetNames("Point Loc 2 FF Set");
        plvo.setPointLocationName("Point Loc 2");
        plvo.setSiteName(assetVO.getSiteName());
        plvo.setSpeedRatio(222);
        plvo.setTachName("Point Loc 2 Tach Name");
        
        List<PointVO> pvo2List = new ArrayList<>();
        pvo = new PointVO();
        pvo.setAlSetName("Point 2 AL Set");
        pvo.setApSetName("Point 2 AP Set");
        pvo.setAreaName(assetVO.getAreaName());
        pvo.setAssetName(assetVO.getAssetName());
        pvo.setCustomerAccount(assetVO.getCustomerAccount());
        pvo.setDisabled(true);
        pvo.setPointLocationName("Point Loc 2");
        pvo.setPointName("Point 2");
        pvo.setPointType("Point Type 2");
        pvo.setSensorChannelNum(21);
        pvo.setSensorOffset(22);
        pvo.setSensorSensitivity(23);
        pvo.setSensorType("Point 2 Sensor Type");
        pvo.setSensorUnits("Point 2 Sensor Unit");
        pvo.setSiteName(assetVO.getSiteName());
        pvo.setCustomized(true);
        pvo2List.add(pvo);
        plvo.setPointList(pvo2List);
        plvoList.add(plvo);
        assetVO.setPointLocationList(plvoList);
        String expResult = "{\"customerAccount\":\"77777\",\"siteName\":\"Test Site Name\",\"areaName\":\"Test Area\",\"assetId\":\""+ assetVO.getAssetId()+"\",\"assetName\":\"Test Asset Name\",\"assetType\":\"Test Asset Type\",\"description\":\"Test Description\",\"sampleInterval\":2,\"pointLocationList\":[{\"customerAccount\":\"77777\",\"siteName\":\"Test Site Name\",\"areaName\":\"Test Area\",\"assetId\":\""+ assetVO.getAssetId()+"\",\"assetName\":\"Test Asset Name\",\"pointLocationName\":\"Point Loc 1\",\"description\":\"Point Loc 1 Desc\",\"deviceSerialNumber\":\"Point Loc 1 Dev Serial Num\",\"ffSetNames\":\"Point Loc 1 FF Set\",\"speedRatio\":111.0,\"sampleInterval\":0,\"tachName\":\"Point Loc 1 Tach Name\",\"alarmEnabled\":false,\"basestationPortNum\":0,\"rollDiameter\":0.0,\"pointList\":[{\"customerAccount\":\"77777\",\"pointName\":\"Point 1\",\"disabled\":true,\"pointType\":\"Point Type 1\",\"sensorChannelNum\":11,\"sensorOffset\":12.0,\"sensorSensitivity\":13.0,\"sensorType\":\"Point 1 Sensor Type\",\"sensorUnits\":\"Point 1 Sensor Unit\",\"demodEnabled\":false,\"demodHighPass\":0.0,\"demodLowPass\":0.0,\"dwell\":0,\"fMax\":0,\"siteName\":\"Test Site Name\",\"areaName\":\"Test Area\",\"assetName\":\"Test Asset Name\",\"pointLocationName\":\"Point Loc 1\",\"apSetName\":\"Point 1 AP Set\",\"alSetName\":\"Point 1 AL Set\",\"demodInterval\":0,\"alarmEnabled\":false,\"highAlert\":0.0,\"highAlertActive\":false,\"highFault\":0.0,\"highFaultActive\":false,\"customized\":true,\"lowAlert\":0.0,\"lowAlertActive\":false,\"lowFault\":0.0,\"lowFaultActive\":false,\"maxFreq\":0.0,\"minFreq\":0.0,\"period\":0.0,\"resolution\":0,\"sampleRate\":0.0}]},{\"customerAccount\":\"77777\",\"siteName\":\"Test Site Name\",\"areaName\":\"Test Area\",\"assetId\":\""+ assetVO.getAssetId()+"\",\"assetName\":\"Test Asset Name\",\"pointLocationName\":\"Point Loc 2\",\"description\":\"Point Loc 2 Desc\",\"deviceSerialNumber\":\"Point Loc 2 Dev Serial Num\",\"ffSetNames\":\"Point Loc 2 FF Set\",\"speedRatio\":222.0,\"sampleInterval\":0,\"tachName\":\"Point Loc 2 Tach Name\",\"alarmEnabled\":false,\"basestationPortNum\":0,\"rollDiameter\":0.0,\"pointList\":[{\"customerAccount\":\"77777\",\"pointName\":\"Point 2\",\"disabled\":true,\"pointType\":\"Point Type 2\",\"sensorChannelNum\":21,\"sensorOffset\":22.0,\"sensorSensitivity\":23.0,\"sensorType\":\"Point 2 Sensor Type\",\"sensorUnits\":\"Point 2 Sensor Unit\",\"demodEnabled\":false,\"demodHighPass\":0.0,\"demodLowPass\":0.0,\"dwell\":0,\"fMax\":0,\"siteName\":\"Test Site Name\",\"areaName\":\"Test Area\",\"assetName\":\"Test Asset Name\",\"pointLocationName\":\"Point Loc 2\",\"apSetName\":\"Point 2 AP Set\",\"alSetName\":\"Point 2 AL Set\",\"demodInterval\":0,\"alarmEnabled\":false,\"highAlert\":0.0,\"highAlertActive\":false,\"highFault\":0.0,\"highFaultActive\":false,\"customized\":true,\"lowAlert\":0.0,\"lowAlertActive\":false,\"lowFault\":0.0,\"lowFaultActive\":false,\"maxFreq\":0.0,\"minFreq\":0.0,\"period\":0.0,\"resolution\":0,\"sampleRate\":0.0}]}],\"alarmEnabled\":false}";
        String result = instance.getJsonFromAssetVO(assetVO);
        System.out.println("Result    - " + result);
        System.out.println("expResult - " + expResult);
        assertEquals(expResult, result);
    }
    
}
