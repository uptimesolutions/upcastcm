/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package com.uptime.upcastcm.http.utils.json.dom;

import com.uptime.upcastcm.http.utils.json.dom.AreaDomJsonParser;
import com.uptime.upcastcm.utils.vo.AreaVO;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Joseph
 */
public class AreaDomJsonParserTest {

    private static List<AreaVO> areaVOList;
    static AreaDomJsonParser instance;

    public AreaDomJsonParserTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        areaVOList = new ArrayList();
        AreaVO areaVO1 = new AreaVO();
        areaVO1.setAreaName("Area1");
        areaVO1.setClimateControlled(false);
        // areaVO1.set
        areaVO1.setCustomerAccount("77777");
        // areaVO1.setDayOfWeek((byte) 1);
        areaVO1.setDescription("description1");
        areaVO1.setEnvironment("environment1");
        // areaVO1.setHoursOfDay("12");
        //  areaVO1.setScheduleName("scheduleName1");
        areaVO1.setSiteName("site1");

        AreaVO areaVO2 = new AreaVO();

        areaVO2.setAreaName("Area2");
        areaVO2.setClimateControlled(false);
        //  areaVO2.setContinuous(false);
        areaVO2.setCustomerAccount("22222");
        //  areaVO2.setDayOfWeek((byte) 2);
        areaVO2.setDescription("description2");
        areaVO2.setEnvironment("environment2");
        //  areaVO2.setHoursOfDay("24");
        //  areaVO2.setScheduleName("scheduleName2");
        areaVO2.setSiteName("site2");

        areaVOList.add(areaVO1);
        areaVOList.add(areaVO2);
        instance = AreaDomJsonParser.getInstance();

    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getJsonFromArea method, of class AreaDomJsonParser.
     */
    @Test
    public void testGetJsonFromArea() {
        System.out.println("getJsonFromArea");
        String expResult = "{\"customerAccount\":\"77777\",\"siteName\":\"site1\",\"areaName\":\"Area1\",\"climateControlled\":false,\"description\":\"description1\",\"environment\":\"environment1\"}";
        String result = instance.getJsonFromArea(areaVOList.get(0));
        assertEquals(expResult, result);
    }

    /**
     * Test of getJsonFromAreaList method, of class AreaDomJsonParser.
     */
    @Test
    public void testGetJsonFromAreaList() {
        System.out.println("getJsonFromAreaList");
        String expResult = "[{\"customerAccount\":\"77777\",\"siteName\":\"site1\",\"areaName\":\"Area1\",\"climateControlled\":false,\"description\":\"description1\",\"environment\":\"environment1\"},{\"customerAccount\":\"22222\",\"siteName\":\"site2\",\"areaName\":\"Area2\",\"climateControlled\":false,\"description\":\"description2\",\"environment\":\"environment2\"}]";
        String result1 = instance.getJsonFromAreaList(areaVOList);
        assertEquals(expResult, result1);
    }

}
