/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package com.uptime.upcastcm.http.utils.json.dom;

import com.uptime.upcastcm.http.utils.json.dom.SiteDomJsonParser;
import com.uptime.upcastcm.utils.vo.SiteContactVO;
import com.uptime.upcastcm.utils.vo.SiteVO;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Joseph
 */
public class SiteDomJsonParserTest {

    private static List<SiteVO> siteVOList;
    private static SiteDomJsonParser instance;

    public SiteDomJsonParserTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        siteVOList = new ArrayList();
        SiteVO siteVO1 = new SiteVO();
        siteVO1.setBillingAddress1("billingAddress11");
        siteVO1.setBillingAddress2("billingAddress21");
        siteVO1.setBillingCity("BillingCity1");
        siteVO1.setBillingPostalCode("billingPostalCode1");
        siteVO1.setBillingStateProvince("billingStateProvince1");
        siteVO1.setCountry("country1");
        siteVO1.setCustomerAccount("customerAccount1");
        siteVO1.setIndustry("industry1");
        siteVO1.setLatitude("latitude1");
        siteVO1.setLongitude("longitude1");
        siteVO1.setPhysicalAddress1("physicalAddress11");
        siteVO1.setPhysicalAddress2("physicalAddress22");
        siteVO1.setPhysicalCity("physicalCity1");
        siteVO1.setPhysicalPostalCode("physicalPostalCode1");
        siteVO1.setPhysicalStateProvince("physicalStateProvince1");
        siteVO1.setRegion("region1");
        siteVO1.setShipAddress1("shipAddress11");
        siteVO1.setShipAddress2("shipAddress21");
        siteVO1.setShipCity("shipCity1");
        siteVO1.setShipPostalCode("shipPostalCode1");
        siteVO1.setShipStateProvince("shipStateProvince1");
        List<SiteContactVO> siteContactList = new ArrayList<>();
        SiteContactVO scvo = new SiteContactVO();
        scvo.setContactEmail("aaa@gmail.com");
        scvo.setContactName("aaa");
        scvo.setContactPhone("1111111111");
        scvo.setContactTitle("Title1");
        scvo.setCustomerAccount("77777");
        scvo.setRole("User");
        scvo.setSiteName("siteName1");
        siteContactList.add(scvo);
        siteVO1.setSiteContactList(siteContactList);
        siteVO1.setSiteName("siteName1");
        siteVO1.setTimezone("timezone1");

        SiteVO siteVO2 = new SiteVO();
        siteVO2.setBillingAddress1("billingAddress11");
        siteVO2.setBillingAddress2("billingAddress21");
        siteVO2.setBillingCity("BillingCity1");
        siteVO2.setBillingPostalCode("billingPostalCode1");
        siteVO2.setBillingStateProvince("billingStateProvince1");
        siteVO2.setCountry("country1");
        siteVO2.setCustomerAccount("customerAccount1");
        siteVO2.setIndustry("industry1");
        siteVO2.setLatitude("latitude1");
        siteVO2.setLongitude("longitude1");
        siteVO2.setPhysicalAddress1("physicalAddress11");
        siteVO2.setPhysicalAddress2("physicalAddress22");
        siteVO2.setPhysicalCity("physicalCity1");
        siteVO2.setPhysicalPostalCode("physicalPostalCode1");
        siteVO2.setPhysicalStateProvince("physicalStateProvince1");
        siteVO2.setRegion("region1");
        siteVO2.setShipAddress1("shipAddress11");
        siteVO2.setShipAddress2("shipAddress21");
        siteVO2.setShipCity("shipCity1");
        siteVO2.setShipPostalCode("shipPostalCode1");
        siteVO2.setShipStateProvince("shipStateProvince1");
        List<SiteContactVO> siteContactList1 = new ArrayList<>();
        SiteContactVO scvo1 = new SiteContactVO();
        scvo1.setContactEmail("bbb@gmail.com");
        scvo1.setContactName("bbb");
        scvo1.setContactPhone("2222222222");
        scvo1.setContactTitle("Title2");
        scvo1.setCustomerAccount("22222");
        scvo1.setRole("User");
        scvo1.setSiteName("siteName2");
        siteContactList.add(scvo1);
        siteVO2.setSiteContactList(siteContactList1);
        siteVO2.setSiteName("siteName2");
        siteVO2.setTimezone("timezone2");

        siteVOList.add(siteVO1);
        siteVOList.add(siteVO2);

        instance = SiteDomJsonParser.getInstance();
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getJsonFromSite method, of class SiteDomJsonParser.
     */
    @Test
    public void testGetJsonFromSite() {
        System.out.println("getJsonFromSite");
        String expResult ="{\"customerAccount\":\"customerAccount1\",\"siteName\":\"siteName1\",\"region\":\"region1\",\"country\":\"country1\",\"physicalAddress1\":\"physicalAddress11\",\"physicalAddress2\":\"physicalAddress22\",\"physicalCity\":\"physicalCity1\",\"physicalStateProvince\":\"physicalStateProvince1\",\"physicalPostalCode\":\"physicalPostalCode1\",\"latitude\":\"latitude1\",\"longitude\":\"longitude1\",\"timezone\":\"timezone1\",\"industry\":\"industry1\",\"shipAddress1\":\"shipAddress11\",\"shipAddress2\":\"shipAddress21\",\"shipCity\":\"shipCity1\",\"shipStateProvince\":\"shipStateProvince1\",\"shipPostalCode\":\"shipPostalCode1\",\"billingAddress1\":\"billingAddress11\",\"billingAddress2\":\"billingAddress21\",\"billingCity\":\"BillingCity1\",\"billingStateProvince\":\"billingStateProvince1\",\"billingPostalCode\":\"billingPostalCode1\",\"siteContactList\":[{\"customerAccount\":\"77777\",\"siteName\":\"siteName1\",\"role\":\"User\",\"contactName\":\"aaa\",\"contactTitle\":\"Title1\",\"contactPhone\":\"1111111111\",\"contactEmail\":\"aaa@gmail.com\",\"roleReadOnly\":false},{\"customerAccount\":\"22222\",\"siteName\":\"siteName2\",\"role\":\"User\",\"contactName\":\"bbb\",\"contactTitle\":\"Title2\",\"contactPhone\":\"2222222222\",\"contactEmail\":\"bbb@gmail.com\",\"roleReadOnly\":false}]}";
        String result = instance.getJsonFromSite(siteVOList.get(0));
        assertEquals(expResult, result);
    }

    /**
     * Test of populateSiteVOFromGeocodeJson method, of class SiteDomJsonParser.
     */
    @Test
    public void testPopulateSiteVOFromGeocodeJson() {
        System.out.println("populateSiteVOFromGeocodeJson");
        String content ="{\"info\":{\"statuscode\":0,\"copyright\":{\"text\":\"\\u00A9 2022 MapQuest, Inc.\",\"imageUrl\":\"http://api.mqcdn.com/res/mqlogo.gif\",\"imageAltText\":\"\\u00A9 2022 MapQuest, Inc.\"},\"messages\":[]},\"options\":{\"maxResults\":-1,\"thumbMaps\":true,\"ignoreLatLngInput\":false},\"results\":[{\"providedLocation\":{\"street\":\"4820 Executive Park Ct\",\"city\":\"Jacksonville\"},\"locations\":[{\"street\":\"Executive Park Court\",\"adminArea6\":\"\",\"adminArea6Type\":\"Neighborhood\",\"adminArea5\":\"Jacksonville\",\"adminArea5Type\":\"City\",\"adminArea4\":\"Duval County\",\"adminArea4Type\":\"County\",\"adminArea3\":\"FL\",\"adminArea3Type\":\"State\",\"adminArea1\":\"US\",\"adminArea1Type\":\"Country\",\"postalCode\":\"32216\",\"geocodeQualityCode\":\"B1CAX\",\"geocodeQuality\":\"STREET\",\"dragPoint\":false,\"sideOfStreet\":\"N\",\"linkId\":\"0\",\"unknownInput\":\"\",\"type\":\"s\",\"latLng\":{\"lat\":30.252254,\"lng\":-81.602805},\"displayLatLng\":{\"lat\":30.252254,\"lng\":-81.602805},\"mapUrl\":\"http://open.mapquestapi.com/staticmap/v5/map?key=v1Lkg6zQJ8u0YXchu2AbORhBNN5uovn3&type=map&size=225,160&locations=30.252254,-81.602805|marker-sm-50318A-1&scalebar=true&zoom=15&rand=-53587413\"}]}]}"; 
        SiteVO expResult = new SiteVO();
        expResult.setLatitude("30.252254");
        expResult.setLongitude("-81.602805");
        SiteVO result = instance.populateSiteVOFromGeocodeJson(content);
        System.out.println("populateSiteVOFromGeocodeJson :" + result);
        assertEquals(expResult, result);      
    }

}
