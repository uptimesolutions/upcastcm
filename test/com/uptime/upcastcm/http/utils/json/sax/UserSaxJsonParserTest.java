/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package com.uptime.upcastcm.http.utils.json.sax;

import com.uptime.upcastcm.http.utils.json.sax.UserSaxJsonParser;
import com.uptime.upcastcm.utils.vo.UserPreferencesVO;
import com.uptime.upcastcm.utils.vo.UserSitesVO;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Joseph
 */
public class UserSaxJsonParserTest {

    private static UserSaxJsonParser instance;
    private static UserPreferencesVO userPreferencesVO;

    public UserSaxJsonParserTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        userPreferencesVO = new UserPreferencesVO();
        userPreferencesVO.setCustomerAccount("77777");
        userPreferencesVO.setDefaultSite(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        userPreferencesVO.setLanguage("English");
        userPreferencesVO.setLocaleDisplayName("English");
        userPreferencesVO.setTimezone("UTC");
        userPreferencesVO.setUserId("sam.spade@uptimesolutions.us");

        instance = UserSaxJsonParser.getInstance();
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of populateUserPreferenceFromJson method, of class
     * UserSaxJsonParser.
     */
    @Test
    public void testPopulateUserPreferenceFromJson() {
        System.out.println("populateUserPreferenceFromJson");
        String content = "{\n"
                + "    \"userId\": \"sam.spade@uptimesolutions.us\",\n"
                + "    \"userPreferences\": [\n"
                + "        {\n"
                + "            \"userId\": \"sam.spade@uptimesolutions.us\",\n"
                + "            \"customerAccount\": \"77777\",\n"
                + "            \"language\": \"English\",\n"
                + "            \"timezone\": \"UTC\",\n"
                + "            \"defaultSite\": \"e4c66964-5a8c-451d-ae2e-9d04d83c8241\"\n"
                + "        }\n"
                + "    ],\n"
                + "    \"outcome\": \"GET worked successfully.\"\n"
                + "}";
        UserPreferencesVO expResult = userPreferencesVO;
        UserPreferencesVO result = instance.populateUserPreferenceFromJson(content);
        assertEquals(expResult, result);
    }

    /**
     * Test of populateUserSitesFromJson method, of class UserSaxJsonParser.
     */
    @Test
    public void testPopulateUserSitesFromJson() {
        System.out.println("populateUserSitesFromJson");
        String content = "{\n"
                + "    \"userId\": \"Test userId 111\",\n"
                + "    \"userSites\": [\n"
                + "        {\n"
                + "            \"userId\": \"Test userId 111\",\n"
                + "            \"customerAccount\": \"Test customerAccount 111\",\n"
                + "            \"siteName\": \"Test siteName 111\",\n"
                + "            \"siteRole\": \"Test siteRole 111\"\n"
                + "        }\n"
                + "    ],\n"
                + "    \"outcome\": \"GET worked successfully.\"\n"
                + "}";
        List<UserSitesVO> expResult = new ArrayList();
        UserSitesVO userSitesVO = new UserSitesVO();
        userSitesVO.setCustomerAccount("Test customerAccount 111");
        userSitesVO.setSiteName("Test siteName 111");
        userSitesVO.setSiteRole("Test siteRole 111");
        userSitesVO.setUserId("Test userId 111");
        expResult.add(userSitesVO);
        List<UserSitesVO> result = instance.populateUserSitesFromJson(content);
        assertEquals(expResult, result);
    }

    /**
     * Test of getJsonFromObject method, of class UserSaxJsonParser.
     */
//    @Test
//    public void testGetJsonFromObject() {
//        System.out.println("getJsonFromObject");
//        String expResult = "{\"userId\":\"sam.spade@uptimesolutions.us\",\"customerAccount\":\"77777\",\"timezone\":\"UTC\",\"localeDisplayName\":\"English\",\"defaultSite\":\"defaultSite\",\"language\":\"English\"}";
//        String result = instance.getJsonFromObject(userPreferencesVO);
//        assertEquals(expResult, result);
//    }

}
