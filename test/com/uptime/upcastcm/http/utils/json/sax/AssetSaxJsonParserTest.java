/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package com.uptime.upcastcm.http.utils.json.sax;

import com.uptime.upcastcm.utils.vo.AssetVO;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Joseph
 */
public class AssetSaxJsonParserTest {

    private static AssetSaxJsonParser instance;
    private static UUID assetId;

    public AssetSaxJsonParserTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        instance = AssetSaxJsonParser.getInstance();
        assetId = UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241");
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of populateAssetVOFromJson method, of class AssetSaxJsonParser.
     */
    @Test
    public void testPopulateAssetVOFromJson() {
        System.out.println("populateAssetVOFromJson");
        String content = "{\n"
                + "	\"customer\": \"Cust Acct 111\",\n"
                + "	\"siteName\": \"Test Site 111\",\n"
                + "	\"areaName\": \"Test Area 111\",\n"
                + "	\"AssetSiteAreas\": [{\n"
                + "		\"customerAccount\": \"CUST ACCT 111\",\n"
                + "		\"siteName\": \"TEST SITE 111\",\n"
                + "		\"areaName\": \"TEST AREA 111\",\n"
                + "		\"assetId\": \"e4c66964-5a8c-451d-ae2e-9d04d83c8241\",\n"
                + "		\"assetName\": \"ASSET 111\",\n"
                + "		\"assetType\": \"ASSET TYPE 111\",\n"
                + "		\"description\": \"TEST DESCRIPTION 222\",\n"
                + "		\"sampleInterval\": 1\n"
                + "	}],\n"
                + "	\"outcome\": \"GET worked successfully.\"\n"
                + "}";
        List<AssetVO> expResult = new ArrayList();
        AssetVO assetVO = new AssetVO();
        assetVO.setAreaName("TEST AREA 111");
        assetVO.setAssetId(assetId);
        assetVO.setAssetName("ASSET 111");
        assetVO.setAssetType("ASSET TYPE 111");
        assetVO.setCustomerAccount("CUST ACCT 111");
        assetVO.setDescription("TEST DESCRIPTION 222");
       // assetVO.setPointLocationList(pointLocationList);
        assetVO.setSampleInterval(1);
        assetVO.setSiteName("TEST SITE 111");
        expResult.add(assetVO);
        List<AssetVO> result = instance.populateAssetVOFromJson(content);
        assertEquals(expResult, result);
    }

}
