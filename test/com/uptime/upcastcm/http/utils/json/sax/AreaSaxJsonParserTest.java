/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package com.uptime.upcastcm.http.utils.json.sax;

import com.uptime.upcastcm.http.utils.json.sax.AreaSaxJsonParser;
import com.uptime.upcastcm.utils.vo.AreaConfigScheduleVO;
import com.uptime.upcastcm.utils.vo.AreaVO;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Joseph
 */
public class AreaSaxJsonParserTest {
    
    private static AreaSaxJsonParser instance;
    
    public AreaSaxJsonParserTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        instance = AreaSaxJsonParser.getInstance();
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of testPopulateAreaSiteFromJson method, of class AreaSaxJsonParser.
     */
    @Test
    public void testPopulateAreaSiteFromJson() {
        System.out.println("populateAreaFromJson");
        String content = "{\n"
                + "    \"customerAccount\": \"77777\",\n"
                + "    \"siteName\": \"TEST SITE\",\n"
                + "    \"areaName\": \"AREA51\",\n"
                + "    \"areaSites\": [\n"
                + "        {\n"
                + "            \"customerAccount\": \"77777\",\n"
                + "            \"siteName\": \"TEST SITE\",\n"
                + "            \"areaName\": \"AREA51\",\n"
                + "            \"description\": \"DESC11\",\n"
                + "            \"environment\": \"TRUE,FALSE\",\n"
                + "            \"climateControlled\": true\n"
                + "        }\n"
                + "    ],\n"
                + "    \"outcome\": \"GET worked successfully.\"\n"
                + "}";
        List<AreaVO> expResult = new ArrayList();
        AreaVO areaVO = new AreaVO();
        areaVO.setCustomerAccount("77777");
        areaVO.setSiteName("TEST SITE");
        areaVO.setAreaName("AREA51");
        areaVO.setDescription("DESC11");
        areaVO.setClimateControlled(true);
        areaVO.setEnvironment("TRUE,FALSE");
        
        expResult.add(areaVO);
        List<AreaVO> result = instance.populateAreaSiteFromJson(content);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of populateAreaConfigScheduleFromJson method, of class
     * AreaSaxJsonParser.
     */
    @Test
    public void testPopulateAreaConfigScheduleFromJson() {
        System.out.println("populateAreaConfigScheduleFromJson");
        String content = "{\n"
                + "    \"customerAccount\": \"77777\",\n"
                + "    \"siteName\": \"TEST SITE\",\n"
                + "    \"areaName\": \"AREA51\",\n"
                + "    \"areaConfigSchedules\": [\n"
                + "        {\n"
                + "            \"customerAccount\": \"77777\",\n"
                + "            \"siteName\": \"TEST SITE\",\n"
                + "            \"areaName\": \"AREA51\",\n"
                + "            \"scheduleName\": \"AREA51 SC2\",\n"
                + "            \"dayOfWeek\": 2,\n"
                + "            \"description\": \"AREA51 SC2 DESC\",\n"
                + "            \"continuous\": false,\n"
                + "            \"hoursOfDay\": \"01:00-2:00\"\n"
                + "        }\n"
                + "    ],\n"
                + "    \"outcome\": \"GET worked successfully.\"\n"
                + "}";
        List<AreaConfigScheduleVO> expResult = new ArrayList();
        AreaConfigScheduleVO acsvo = new AreaConfigScheduleVO();
        acsvo.setCustomerAccount("77777");
        acsvo.setSiteName("TEST SITE");
        acsvo.setAreaName("AREA51");
        acsvo.setScheduleName("AREA51 SC2");
        acsvo.setDayOfWeek((byte) 2);
        acsvo.setSchDescription("AREA51 SC2 DESC");
        acsvo.setContinuous(false);
        acsvo.setHoursOfDay("01:00-2:00");
        expResult.add(acsvo);
        List<AreaConfigScheduleVO> result = instance.populateAreaConfigScheduleFromJson(content);
        assertEquals(expResult, result);
    }

    /**
     * Test of populateAreaListFromJson method, of class AreaSaxJsonParser.
     */
    @Test
    public void testPopulateAreaListFromJson() {
        System.out.println("populateAreaListFromJson");
        String content = "{\n"
                + "\"customerAccount\": \"77777\","
                + "\"siteName\": \"Test Site\","
                + "\"areaName\": \"area51\","
                + "\"climateControlled\": true,"
                + "\"description\": \"desc11\","
                + "\"environment\": \"true,false\","
                + "\"scheduleVOList\": [\n"
                + "{\n"
                + "\"customerAccount\": \"77777\","
                + "\"siteName\": \"Test Site\","
                + "\"areaName\": \"area51\","
                + "\"scheduleName\": \"area51 sc1\","
                + "\"dayOfWeek\": 2,"
                + "\"schDescription\": \"area51 sc1 desc\","
                + "\"hoursOfDay\": \"01:00-2:00\","
                + "\"continuous\": false"
                + "}\n"
                + "]\n"
                + "}";
        
        AreaVO areaVO = new AreaVO();
        areaVO.setCustomerAccount("77777");
        areaVO.setSiteName("Test Site");
        areaVO.setAreaName("area51");
        areaVO.setClimateControlled(true);
        areaVO.setDescription("desc11");
        areaVO.setEnvironment("true,false");
        List<AreaConfigScheduleVO> scheduleVOList = new ArrayList();
        AreaConfigScheduleVO acsvo = new AreaConfigScheduleVO();
        acsvo.setCustomerAccount("77777");
        acsvo.setSiteName("Test Site");
        acsvo.setAreaName("area51");
        acsvo.setScheduleName("area51 sc1");
        acsvo.setDayOfWeek((byte) 2);
        // acsvo.setSchDescription("area51 sc1 desc");
        acsvo.setContinuous(false);
        acsvo.setHoursOfDay("01:00-2:00");
        scheduleVOList.add(acsvo);
        areaVO.setScheduleVOList(scheduleVOList);
        
        AreaVO result = instance.populateAreaListFromJson(content);
        assertEquals(areaVO, result);
    }
}
