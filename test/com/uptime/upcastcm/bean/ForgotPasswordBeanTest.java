/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.EmailClient;
import com.uptime.upcastcm.http.client.LdapClient;
import com.uptime.upcastcm.http.client.UserClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import com.uptime.upcastcm.utils.vo.LdapUserVO;
import java.util.HashMap;
import java.util.Map;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import org.junit.Test;
import org.junit.FixMethodOrder;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import static org.junit.Assert.*;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import static org.powermock.api.mockito.PowerMockito.when;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.primefaces.PrimeFaces;

/**
 *
 * @author madhavi
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(PowerMockRunner.class)
@PowerMockIgnore("jdk.internal.reflect.*")
@PrepareForTest({FacesContext.class, ExternalContext.class, ServiceRegistrarClient.class, PrimeFaces.class, PrimeFaces.Ajax.class, UserClient.class, LdapClient.class, EmailClient.class})
public class ForgotPasswordBeanTest {

    private final ForgotPasswordBean instance;

    @Mock
    ServiceRegistrarClient serviceRegistrarClient;
    @Mock
    FacesContext facesContext;
    @Mock
    ExternalContext externalContext;
    @Mock
    HttpServletRequest request;
    @Mock
    UserClient userClient;
    @Mock
    LdapClient ldapClient;
    @Mock
    EmailClient emailClient;

    Map<String, Object> sessionMap;
    String customerAccount;
    
    public ForgotPasswordBeanTest() {
        PowerMockito.mockStatic(FacesContext.class);
        PowerMockito.mockStatic(ServiceRegistrarClient.class);
        PowerMockito.mockStatic(UserClient.class);
        PowerMockito.mockStatic(LdapClient.class);
        PowerMockito.mockStatic(EmailClient.class);

        facesContext = PowerMockito.mock(FacesContext.class);
        externalContext = PowerMockito.mock(ExternalContext.class);
        request = PowerMockito.mock(HttpServletRequest.class);
        serviceRegistrarClient = PowerMockito.mock(ServiceRegistrarClient.class);
        userClient = PowerMockito.mock(UserClient.class);
        ldapClient = PowerMockito.mock(LdapClient.class);
        emailClient = PowerMockito.mock(EmailClient.class);

        Map<String, String> hm = new HashMap<>();
        hm.put("account_number", "UPTIME");
        
        when(FacesContext.getCurrentInstance()).thenReturn(facesContext);
        when(facesContext.getExternalContext()).thenReturn(externalContext);
        when(externalContext.getRequest()).thenReturn(request);
        when(externalContext.getRequestParameterMap()).thenReturn(hm);
        when(request.getHeader("host")).thenReturn("1.1.1.1:8000");
        when(LdapClient.getInstance()).thenReturn(ldapClient);
        when(ServiceRegistrarClient.getInstance()).thenReturn(serviceRegistrarClient);
        when(serviceRegistrarClient.getServiceHostURL(LDAP_SERVICE_NAME)).thenReturn(LDAP_SERVICE_NAME);
        
        
        PowerMockito.mockStatic(PrimeFaces.class);
        PowerMockito.mockStatic(PrimeFaces.Ajax.class);
        PrimeFaces pf = PowerMockito.mock(PrimeFaces.class);
        PrimeFaces.Ajax ax = PowerMockito.mock(PrimeFaces.Ajax.class);
        when(PrimeFaces.current()).thenReturn(pf);
        when(pf.ajax()).thenReturn(ax);

        
        instance = new ForgotPasswordBean();
    }

    /**
     * Test of createPasswordToken method, of class ForgotPasswordBean.
     */
    @Test
    public void CreatePasswordToken() {
        System.out.println("createPasswordToken");
        instance.setEmail("sam.spade@uptime-solutions.us");
        when(ldapClient.getUserAccountByUserId(LDAP_SERVICE_NAME, "sam.spade@uptime-solutions.us" , "UPTIME")).thenReturn(new LdapUserVO());
        instance.createPasswordToken();
        assertTrue(instance.isEmailSent());
        when(ldapClient.getUserAccountByUserId(LDAP_SERVICE_NAME, "sam.spade@uptime-solutions.us" , "UPTIME")).thenReturn(null);
        instance.createPasswordToken();
        assertFalse(instance.isEmailSent());
        System.out.println("createPasswordToken Complete");
    }
}
