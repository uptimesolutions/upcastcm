/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.APALSetClient;
import com.uptime.upcastcm.http.client.DevicePresetClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import static com.uptime.upcastcm.utils.enums.ACSensorTypeEnum.getACSensorTypeItemList;
import static com.uptime.upcastcm.utils.enums.DCSensorTypeEnum.getDCSensorTypeItemList;
import static com.uptime.upcastcm.utils.enums.DeviceTypeEnum.getDeviceTypeItemList;
import com.uptime.upcastcm.utils.vo.DevicePresetVO;
import com.uptime.upcastcm.utils.vo.UserSitesVO;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import org.junit.Before;
import org.junit.Test;
import org.junit.FixMethodOrder;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import static org.powermock.api.mockito.PowerMockito.when;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.primefaces.PrimeFaces;

/**
 *
 * @author Joseph
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(PowerMockRunner.class)
@PowerMockIgnore("jdk.internal.reflect.*")
@PrepareForTest({FacesContext.class, ExternalContext.class, ServiceRegistrarClient.class, UserManageBean.class, APALSetClient.class, DevicePresetClient.class, PrimeFaces.class, PrimeFaces.Ajax.class})
public class CopyEditPresetsDeviceBeanTest {

    @Mock
    ServiceRegistrarClient serviceRegistrarClient;

    @Mock
    APALSetClient aPALSetClient;

    @Mock
    FacesContext facesContext;

    @Mock
    ExternalContext externalContext;

    @Mock
    UserManageBean userManageBean;

    @Mock
    PresetsBean presetsBean;

    @Mock
    DevicePresetClient devicePresetClient;
    
    @Mock
    HttpServletRequest request;
    
    @Mock
    HttpSession session;

    Map<String, Object> sessionMap;
    DevicePresetVO devicePresetVO;
    private final CopyEditPresetsDeviceBean instance;

    public CopyEditPresetsDeviceBeanTest() {
        PowerMockito.mockStatic(FacesContext.class);
        PowerMockito.mockStatic(DevicePresetClient.class);
        PowerMockito.mockStatic(ServiceRegistrarClient.class);

        facesContext = PowerMockito.mock(FacesContext.class);
        externalContext = PowerMockito.mock(ExternalContext.class);
        request = PowerMockito.mock(HttpServletRequest.class);
        session = PowerMockito.mock(HttpSession.class);
        userManageBean = PowerMockito.mock(UserManageBean.class);
        presetsBean = PowerMockito.mock(PresetsBean.class);
        devicePresetClient = PowerMockito.mock(DevicePresetClient.class);
        serviceRegistrarClient = PowerMockito.mock(ServiceRegistrarClient.class);

        sessionMap = new HashMap();
        sessionMap.put("userManageBean", userManageBean);
        sessionMap.put("presetsBean", presetsBean);
        session.setAttribute("userManageBean", userManageBean);

        UserSitesVO uvo = new UserSitesVO();
        uvo.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        uvo.setSiteName("Test Site 1");

        when(DevicePresetClient.getInstance()).thenReturn(devicePresetClient);
        when(ServiceRegistrarClient.getInstance()).thenReturn(serviceRegistrarClient);
        when(FacesContext.getCurrentInstance()).thenReturn(facesContext);
        when(facesContext.getExternalContext()).thenReturn(externalContext);
        when(externalContext.getSessionMap()).thenReturn(sessionMap);
        when(facesContext.getExternalContext().getRequest()).thenReturn(request);
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("userManageBean")).thenReturn(userManageBean);
        when(userManageBean.getCurrentSite()).thenReturn(uvo);
        UserSitesVO usvo = new UserSitesVO();
        usvo.setCustomerAccount("77777");
        when(userManageBean.getCurrentSite()).thenReturn(usvo);

        instance = new CopyEditPresetsDeviceBean();
    }

    @Before
    public void setUp() {
        devicePresetVO = new DevicePresetVO();
    }

    /**
     * Test of init method, of class CopyEditPresetsDeviceBean.
     */
    @Test
    public void test00_Init() {
        System.out.println("init");
        instance.init();
        System.out.println("init completed");
    }

    /**
     * Test of resetPage method, of class CopyEditPresetsDeviceBean.
     */
    @Test
    public void test01_ResetPage() {
        System.out.println("resetPage");
        instance.resetPage();
        System.out.println("resetPage completed");
    }

    /**
     * Test of submit method, of class CopyEditPresetsDeviceBean.
     */
    @Test
    public void test051_Submit() {
        System.out.println("submit");
        instance.setSetName("Set Name 1");
        instance.setSelectedDeviceType("Device Type 1");
        instance.setOperationType(1);
        List<DevicePresetVO> devicePresetsVOList = new ArrayList();
        DevicePresetVO dpvo = new DevicePresetVO();
        dpvo.setAlSetId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        dpvo.setAlSetName("Al Set Name 1");
        dpvo.setApSetId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        dpvo.setApSetName("Ap Set Name 1");
        dpvo.setCustomerAccount("77777");
        dpvo.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        dpvo.setSiteName("Test Site 1");
        dpvo.setSensorType("MistLX");
        instance.setDisable(true);
        instance.setDevicePresetVO(dpvo);
        devicePresetsVOList.add(dpvo);
        instance.setDevicePresetsVOList(devicePresetsVOList);
        when(DevicePresetClient.getInstance().updateSiteDevicePresetVO(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), instance.getDevicePresetsVOList())).thenReturn(Boolean.TRUE);
        PowerMockito.mockStatic(PrimeFaces.class);
        PowerMockito.mockStatic(PrimeFaces.Ajax.class);
        PrimeFaces pf = PowerMockito.mock(PrimeFaces.class);
        PrimeFaces.Ajax ax = PowerMockito.mock(PrimeFaces.Ajax.class);
        when(pf.current()).thenReturn(pf);
        when(pf.ajax()).thenReturn(ax);
        ax.addCallbackParam("presetsTabViewId:devicesFormId", new Object());

        instance.submit();
        assertEquals(Boolean.FALSE, instance.isValidationFailed());
        System.out.println("submit completed");
    }

    /**
     * Test of submit method, of class CopyEditPresetsDeviceBean.
     */
    @Test
    public void test053_Submit() {
        System.out.println("submit");
        instance.setSetName("Set Name 1");
        instance.setSelectedDeviceType("Device Type 1");
        instance.setOperationType(2);
        List<DevicePresetVO> devicePresetsVOList = new ArrayList();
        DevicePresetVO dpvo = new DevicePresetVO();
        dpvo.setAlSetId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        dpvo.setAlSetName("Al Set Name 1");
        dpvo.setApSetId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        dpvo.setApSetName("Ap Set Name 1");
        dpvo.setCustomerAccount("77777");
        dpvo.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        dpvo.setSiteName("Test Site 1");
        dpvo.setSensorType("MistLX");
        instance.setDisable(true);
        instance.setDevicePresetVO(dpvo);
        devicePresetsVOList.add(dpvo);
        instance.setDevicePresetsVOList(devicePresetsVOList);
        when(DevicePresetClient.getInstance().createDevicePresetVO(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), instance.getDevicePresetsVOList())).thenReturn(Boolean.TRUE);
        PowerMockito.mockStatic(PrimeFaces.class);
        PowerMockito.mockStatic(PrimeFaces.Ajax.class);
        PrimeFaces pf = PowerMockito.mock(PrimeFaces.class);
        PrimeFaces.Ajax ax = PowerMockito.mock(PrimeFaces.Ajax.class);
        when(pf.current()).thenReturn(pf);
        when(pf.ajax()).thenReturn(ax);
        ax.addCallbackParam("presetsTabViewId:devicesFormId", new Object());

        instance.submit();
        assertEquals(Boolean.FALSE, instance.isValidationFailed());
        System.out.println("submit completed");
    }

    /**
     * Test of submit method, of class CopyEditPresetsDeviceBean.
     */
    @Test
    public void test054_Submit() {
        System.out.println("submit");
        instance.setSetName(null);
        instance.setSelectedDeviceType("Device Type 1");
        instance.setOperationType(2);
        List<DevicePresetVO> devicePresetsVOList = new ArrayList();
        DevicePresetVO dpvo = new DevicePresetVO();
        dpvo.setAlSetId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        dpvo.setAlSetName("Al Set Name 1");
        dpvo.setApSetId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        dpvo.setApSetName("Ap Set Name 1");
        dpvo.setCustomerAccount("77777");
        dpvo.setSiteId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        dpvo.setSensorType("MistLX");
        instance.setDisable(true);
        instance.setDevicePresetVO(dpvo);
        devicePresetsVOList.add(dpvo);
        instance.setDevicePresetsVOList(devicePresetsVOList);

        instance.submit();
        assertEquals(Boolean.TRUE, instance.isValidationFailed());
        System.out.println("submit completed");
    }

    /**
     * Test of submit method, of class CopyEditPresetsDeviceBean.
     */
    @Test
    public void test056_Submit() {
        System.out.println("submit");
        instance.setSetName("Set Name 1");
        instance.setSelectedDeviceType(null);
        instance.setOperationType(2);
        List<DevicePresetVO> devicePresetsVOList = new ArrayList();
        DevicePresetVO dpvo = new DevicePresetVO();
        dpvo.setAlSetId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        dpvo.setAlSetName("Al Set Name 1");
        dpvo.setApSetId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        dpvo.setApSetName("Ap Set Name 1");
        dpvo.setCustomerAccount("77777");
        dpvo.setSiteId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        dpvo.setSensorType("MistLX");
        instance.setDisable(true);
        instance.setDevicePresetVO(dpvo);
        devicePresetsVOList.add(dpvo);
        instance.setDevicePresetsVOList(devicePresetsVOList);

        instance.submit();
        assertEquals(Boolean.TRUE, instance.isValidationFailed());
        System.out.println("submit completed");
    }

    /**
     * Test of submit method, of class CopyEditPresetsDeviceBean.
     */
    @Test
    public void test057_Submit() {
        System.out.println("submit");
        instance.setSetName("Set Name 1");
        instance.setSelectedDeviceType("Device Type 1");
        instance.setOperationType(2);
        List<DevicePresetVO> devicePresetsVOList = new ArrayList();
        DevicePresetVO dpvo = new DevicePresetVO();
        dpvo.setAlSetId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        dpvo.setAlSetName("Al Set Name 1");
        dpvo.setApSetId(null);
        dpvo.setApSetName("Ap Set Name 1");
        dpvo.setCustomerAccount("77777");
        dpvo.setSiteId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
         dpvo.setSensorType("MistLX");
        instance.setDisable(true);
        instance.setDevicePresetVO(dpvo);
        devicePresetsVOList.add(dpvo);
        instance.setDevicePresetsVOList(devicePresetsVOList);

        instance.submit();
        assertEquals(Boolean.TRUE, instance.isValidationFailed());
        System.out.println("submit completed");
    }

    /**
     * Test of submit method, of class CopyEditPresetsDeviceBean.
     */
    @Test
    public void test058_Submit() {
        System.out.println("submit");
        instance.setSetName("Set Name 1");
        instance.setSelectedDeviceType("Device Type 1");
        instance.setOperationType(2);
        List<DevicePresetVO> devicePresetsVOList = new ArrayList();
        DevicePresetVO dpvo = new DevicePresetVO();
        dpvo.setAlSetId(null);
        dpvo.setAlSetName("Al Set Name 1");
        dpvo.setApSetId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        dpvo.setApSetName("Ap Set Name 1");
        dpvo.setCustomerAccount("77777");
        dpvo.setSiteId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
         dpvo.setSensorType("MistLX");
        instance.setDisable(true);
        instance.setDevicePresetVO(dpvo);
        devicePresetsVOList.add(dpvo);
        instance.setDevicePresetsVOList(devicePresetsVOList);

        instance.submit();
        assertEquals(Boolean.TRUE, instance.isValidationFailed());
        System.out.println("submit completed");
    }

    /**
     * Test of addDevicePreset method, of class CopyEditPresetsDeviceBean.
     */
    @Test
    public void test06_AddDevicePreset() {
        System.out.println("addDevicePreset");
        int dplistSize = instance.getDevicePresetsVOList().size();

        instance.addDevicePreset();

        assertEquals(dplistSize + 1, instance.getDevicePresetsVOList().size());

        System.out.println("addDevicePreset completed");
    }

    /**
     * Test of addDevicePreset method, of class CopyEditPresetsDeviceBean.
     */
    @Test
    public void test061_AddDevicePreset() {
        System.out.println("addDevicePreset");
        int dplistSize = instance.getDevicePresetsVOList().size();

        DevicePresetVO dpvo = new DevicePresetVO();
        dpvo.setAlSetId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        dpvo.setAlSetName("Al Set Name 1");
        dpvo.setApSetId(null);
        dpvo.setApSetName("Ap Set Name 1");
        dpvo.setCustomerAccount("77777");
        dpvo.setSiteId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));

        instance.getDevicePresetsVOList().add(dpvo);
        instance.addDevicePreset();

        assertNotEquals(dplistSize, instance.getDevicePresetsVOList().size());

        System.out.println("addDevicePreset completed");
    }

    /**
     * Test of deleteApAlRow method, of class CopyEditPresetsDeviceBean.
     */
    @Test
    public void test09_DeleteApAlRow() {
        System.out.println("deleteApAlRow");
        DevicePresetVO dpvo = new DevicePresetVO();
        dpvo.setAlSetId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8242"));
        dpvo.setAlSetName("Al Set Name 2");
        dpvo.setApSetId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8242"));
        dpvo.setApSetName("Ap Set Name 2");
        dpvo.setCustomerAccount("77777");
        dpvo.setSiteId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8242"));
        instance.getDevicePresetsVOList().add(dpvo);

        DevicePresetVO dpvo1 = new DevicePresetVO();
        dpvo1.setAlSetId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        dpvo1.setAlSetName("Al Set Name 1");
        dpvo1.setApSetId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        dpvo1.setApSetName("Ap Set Name 1");
        dpvo1.setCustomerAccount("77777");
        dpvo1.setSiteId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));

        instance.getDevicePresetsVOList().add(dpvo1);
        int dpsize = instance.getDevicePresetsVOList().size();
        instance.deleteApAlRow(dpvo);

        assertEquals(dpsize - 1, instance.getDevicePresetsVOList().size());
        System.out.println("deleteApAlRow completed");
    }

    /**
     * Test of deleteApAlRow method, of class CopyEditPresetsDeviceBean.
     */
    @Test
    public void test091_DeleteApAlRow() {
        System.out.println("deleteApAlRow");
        DevicePresetVO dpvo = new DevicePresetVO();
        dpvo.setAlSetId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8242"));
        dpvo.setAlSetName("Al Set Name 2");
        dpvo.setApSetId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8242"));
        dpvo.setApSetName("Ap Set Name 2");
        dpvo.setCustomerAccount("77777");
        dpvo.setSiteId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8242"));
        instance.getDevicePresetsVOList().add(dpvo);

        DevicePresetVO dpvo1 = new DevicePresetVO();
        dpvo1.setAlSetId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        dpvo1.setAlSetName("Al Set Name 1");
        dpvo1.setApSetId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        dpvo1.setApSetName("Ap Set Name 1");
        dpvo1.setCustomerAccount("77777");
        dpvo1.setSiteId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));

        instance.getDevicePresetsVOList().add(dpvo1);
        int dpsize = instance.getDevicePresetsVOList().size();
        instance.deleteApAlRow(new DevicePresetVO());

        assertEquals(dpsize, instance.getDevicePresetsVOList().size());
        System.out.println("deleteApAlRow completed");
    }

    /**
     * Test of getDeviceTypeList method, of class CopyEditPresetsDeviceBean.
     */
   // @Test
    public void test10_GetDeviceTypeList() {
        System.out.println("getDeviceTypeList");
        assertNotEquals(instance.getDeviceTypeList().size(), getDeviceTypeItemList().size());
        System.out.println("getDeviceTypeList");
    }

    /**
     * Test of getDcSensorTypeList method, of class CopyEditPresetsDeviceBean.
     */
    @Test
    public void test11_GetDcSensorTypeList() {
        System.out.println("getDcSensorTypeList");
        assertEquals(instance.getDcSensorTypeList().size(), getDCSensorTypeItemList().size());
        System.out.println("getDcSensorTypeList");
    }

    /**
     * Test of getAcSensorTypeList method, of class CopyEditPresetsDeviceBean.
     */
    @Test
    public void test12_GetAcSensorTypeList() {
        System.out.println("getAcSensorTypeList");
        assertEquals(instance.getAcSensorTypeList().size(), getACSensorTypeItemList().size());
        System.out.println("getAcSensorTypeList");
    }
}
