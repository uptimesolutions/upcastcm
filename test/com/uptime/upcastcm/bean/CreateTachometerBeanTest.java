/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.TachometerClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import com.uptime.upcastcm.utils.helperclass.PresetUtil;
import com.uptime.upcastcm.utils.vo.TachometerVO;
import com.uptime.upcastcm.utils.vo.UserSitesVO;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.junit.Test;
import org.junit.FixMethodOrder;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import static org.powermock.api.mockito.PowerMockito.when;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.primefaces.PrimeFaces;

/**
 *
 * @author Joseph
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(PowerMockRunner.class)
@PowerMockIgnore("jdk.internal.reflect.*")
@PrepareForTest({FacesContext.class, ExternalContext.class, ServiceRegistrarClient.class, UserManageBean.class, TachometerClient.class, PrimeFaces.class, PrimeFaces.Ajax.class})
public class CreateTachometerBeanTest {

    @Mock
    ServiceRegistrarClient serviceRegistrarClient;

    @Mock
    TachometerClient tachometerClient;

    @Mock
    FacesContext facesContext;

    @Mock
    ExternalContext externalContext;

    @Mock
    UserManageBean userManageBean;

    @Mock
    PresetsBean presetsBean;

    @Mock
    HttpServletRequest request;
    @Mock
    HttpSession session;

    private final CreateTachometerBean instance;
    Map<String, Object> sessionMap;
    PrimeFaces pf;

    public CreateTachometerBeanTest() {
        UserSitesVO uvo;
        TachometerVO tachometerVO;
        PrimeFaces.Ajax ax;
        
        PowerMockito.mockStatic(FacesContext.class);
        PowerMockito.mockStatic(TachometerClient.class);
        PowerMockito.mockStatic(ServiceRegistrarClient.class);
        PowerMockito.mockStatic(PrimeFaces.class);
        PowerMockito.mockStatic(PrimeFaces.Ajax.class);

        pf = PowerMockito.mock(PrimeFaces.class);
        facesContext = PowerMockito.mock(FacesContext.class);
        externalContext = PowerMockito.mock(ExternalContext.class);
        request = PowerMockito.mock(HttpServletRequest.class);
        session = PowerMockito.mock(HttpSession.class);
        userManageBean = PowerMockito.mock(UserManageBean.class);
        presetsBean = PowerMockito.mock(PresetsBean.class);
        tachometerClient = PowerMockito.mock(TachometerClient.class);
        serviceRegistrarClient = PowerMockito.mock(ServiceRegistrarClient.class);

        sessionMap = new HashMap();
        sessionMap.put("userManageBean", userManageBean);
        sessionMap.put("presetsBean", presetsBean);
        session.setAttribute("userManageBean", userManageBean);
        session.setAttribute("presetsBean", presetsBean);

        uvo = new UserSitesVO();
        uvo.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        uvo.setSiteName("Test Site 1");
        
        ax = PowerMockito.mock(PrimeFaces.Ajax.class);
        
        when(TachometerClient.getInstance()).thenReturn(tachometerClient);
        when(ServiceRegistrarClient.getInstance()).thenReturn(serviceRegistrarClient);
        when(FacesContext.getCurrentInstance()).thenReturn(facesContext);
        when(facesContext.getExternalContext()).thenReturn(externalContext);
        when(externalContext.getSessionMap()).thenReturn(sessionMap);
        when(facesContext.getExternalContext().getRequest()).thenReturn(request);
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("userManageBean")).thenReturn(userManageBean);
        when(session.getAttribute("presetsBean")).thenReturn(presetsBean);
        when(userManageBean.getCurrentSite()).thenReturn(uvo);
        UserSitesVO usvo = new UserSitesVO();
        usvo.setCustomerAccount("77777");
        when(userManageBean.getCurrentSite()).thenReturn(usvo);
        when(userManageBean.getPresetUtil()).thenReturn(new PresetUtil());
        
        
        when(PrimeFaces.current()).thenReturn(pf);
        when(pf.ajax()).thenReturn(ax);

        instance = new CreateTachometerBean();
        
        tachometerVO = new TachometerVO();
        tachometerVO.setSiteName("Test Site 1");
        tachometerVO.setCustomerAccount("77777");
        tachometerVO.setTachName("Tach Name 1");
        
        instance.setTachometerVO(tachometerVO);
        
    }
    /**
     * Test of submit method, of class CreateTachometerBean.
     */
    @Test
    public void test2_Site_Submit() {
        System.out.println("global submit");
        TachometerVO tachometerVO = new TachometerVO();
        tachometerVO.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        tachometerVO.setSiteName("Test Site 1");
        tachometerVO.setCustomerAccount("77777");
        tachometerVO.setTachName("Tach Name 1");
        tachometerVO.setTachType("Test Tach Type");
        instance.setTachometerVO(tachometerVO);
        
        when(tachometerClient.createSiteTachometers(serviceRegistrarClient.getServiceHostURL(PRESET_SERVICE_NAME), tachometerVO)).thenReturn(Boolean.TRUE);
        instance.submit();
        System.out.println("site submit completed");
    }
    
    /**
     * Test of submit method, of class CreateTachometerBean.
     */
    @Test
    public void test2_Global_Submit() {
        System.out.println("global submit");
        TachometerVO tachometerVO = new TachometerVO();      
        tachometerVO.setCustomerAccount("77777");
        tachometerVO.setTachName("Tach Name 1");
        tachometerVO.setTachType("Test Tach Type");
        instance.setTachometerVO(tachometerVO);
        when(tachometerClient.createGlobalTachometers(serviceRegistrarClient.getServiceHostURL(PRESET_SERVICE_NAME), tachometerVO)).thenReturn(Boolean.TRUE);
        instance.submit();
        System.out.println("global submit completed");
    }
}
