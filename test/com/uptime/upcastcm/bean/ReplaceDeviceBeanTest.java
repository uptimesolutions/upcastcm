/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.HwUnitToPointClient;
import com.uptime.upcastcm.http.client.ManageDeviceClient;
import com.uptime.upcastcm.http.client.PointClient;
import com.uptime.upcastcm.http.client.PointToHwUnitClient;
import com.uptime.upcastcm.http.client.SiteClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import com.uptime.upcastcm.utils.helperclass.Filters;
import com.uptime.upcastcm.utils.helperclass.PresetUtil;
import com.uptime.upcastcm.utils.vo.DeviceVO;
import com.uptime.upcastcm.utils.vo.HwUnitVO;
import com.uptime.upcastcm.utils.vo.PointVO;
import com.uptime.upcastcm.utils.vo.UserSitesVO;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import static org.powermock.api.mockito.PowerMockito.when;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.primefaces.PrimeFaces;
import org.powermock.reflect.Whitebox;

/**
 *
 * @author kpati
 */
@RunWith(PowerMockRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@PowerMockIgnore("jdk.internal.reflect.*")
@PrepareForTest({FacesContext.class, ExternalContext.class, UserManageBean.class, ServiceRegistrarClient.class, NavigationBean.class, SiteClient.class, PointClient.class, PointToHwUnitClient.class, ManageDeviceClient.class, HwUnitToPointClient.class, ConfigurationBean.class, PrimeFaces.class, PrimeFaces.Ajax.class})
public class ReplaceDeviceBeanTest {

    @Mock
    ServiceRegistrarClient serviceRegistrarClient;
    @Mock
    ConfigurationBean configurationBean;
    @Mock
    PointClient pointClient;
    @Mock
    PointToHwUnitClient pointToHwUnitClient;
    @Mock
    HwUnitToPointClient hwUnitToPointClient;
    @Mock
    ManageDeviceClient manageDeviceClient;

    @Mock
    FacesContext facesContext;

    @Mock
    ExternalContext externalContext;

    @Mock
    UserManageBean userManageBean;

    @Mock
    HttpServletRequest request;
    @Mock
    HttpSession session;

    private final ReplaceDeviceBean instance;
    private final Filters filters;
    Map<String, Object> sessionMap;
    PrimeFaces pf;
    List<DeviceVO> deviceTypeList;
    List<PointVO> pointVOList;
    String serialNumber;
    DeviceVO replaceDevice;

    public ReplaceDeviceBeanTest() {
        UserSitesVO uvo;
        
        PrimeFaces.Ajax ax;
        PowerMockito.mockStatic(FacesContext.class);
        PowerMockito.mockStatic(ServiceRegistrarClient.class);
        PowerMockito.mockStatic(PointClient.class);
        PowerMockito.mockStatic(PointToHwUnitClient.class);
        PowerMockito.mockStatic(HwUnitToPointClient.class);
        PowerMockito.mockStatic(ManageDeviceClient.class);
        PowerMockito.mockStatic(PrimeFaces.class);
        PowerMockito.mockStatic(PrimeFaces.Ajax.class);

        pf = PowerMockito.mock(PrimeFaces.class);
        facesContext = PowerMockito.mock(FacesContext.class);
        externalContext = PowerMockito.mock(ExternalContext.class);
        request = PowerMockito.mock(HttpServletRequest.class);
        session = PowerMockito.mock(HttpSession.class);
        userManageBean = PowerMockito.mock(UserManageBean.class);
        configurationBean = PowerMockito.mock(ConfigurationBean.class);
        serviceRegistrarClient = PowerMockito.mock(ServiceRegistrarClient.class);
        pointClient = PowerMockito.mock(PointClient.class);
        pointToHwUnitClient = PowerMockito.mock(PointToHwUnitClient.class);
        hwUnitToPointClient = PowerMockito.mock(HwUnitToPointClient.class);
        manageDeviceClient = PowerMockito.mock(ManageDeviceClient.class);

        sessionMap = new HashMap();
        sessionMap.put("userManageBean", userManageBean);
        sessionMap.put("configurationBean", configurationBean);
        session.setAttribute("userManageBean", userManageBean);
        session.setAttribute("configurationBean", configurationBean);
        

        uvo = new UserSitesVO();
        uvo.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        uvo.setSiteName("Test Site 1");

        ax = PowerMockito.mock(PrimeFaces.Ajax.class);

        when(ServiceRegistrarClient.getInstance()).thenReturn(serviceRegistrarClient);
        when(PointClient.getInstance()).thenReturn(pointClient);
        when(PointToHwUnitClient.getInstance()).thenReturn(pointToHwUnitClient);
        when(HwUnitToPointClient.getInstance()).thenReturn(hwUnitToPointClient);
        when(ManageDeviceClient.getInstance()).thenReturn(manageDeviceClient);
        when(FacesContext.getCurrentInstance()).thenReturn(facesContext);
        when(facesContext.getExternalContext()).thenReturn(externalContext);
        when(externalContext.getSessionMap()).thenReturn(sessionMap);
        when(facesContext.getExternalContext().getRequest()).thenReturn(request);
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("userManageBean")).thenReturn(userManageBean);
        when(userManageBean.getCurrentSite()).thenReturn(uvo);
        UserSitesVO usvo = new UserSitesVO();
        usvo.setCustomerAccount("77777");
        when(userManageBean.getCurrentSite()).thenReturn(usvo);
        when(userManageBean.getPresetUtil()).thenReturn(new PresetUtil());

        when(PrimeFaces.current()).thenReturn(pf);
        when(pf.ajax()).thenReturn(ax);
        
        when(serviceRegistrarClient.getServiceHostURL(CONFIGURATION_SERVICE_NAME)).thenReturn(CONFIGURATION_SERVICE_NAME);

        filters = new Filters();
        filters.setSite(new SelectItem(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), "Test Site 1"));
        filters.setArea(new SelectItem(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"), "Test Area 1"));
        filters.setAsset(new SelectItem(UUID.fromString("f33fcf5a-5f93-4504-bc09-74a61a460bf0"), "Test Asset 1"));

        instance = new ReplaceDeviceBean();

        replaceDevice = new DeviceVO();
        replaceDevice.setCustomerAccount("77777");
        replaceDevice.setExistingDeviceId("BB002407");
        replaceDevice.setNewDeviceId("BB001788");

        instance.setReplaceDeviceVO(replaceDevice);

        deviceTypeList = new ArrayList();
        pointVOList = new ArrayList();

    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        DeviceVO deviceVO = new DeviceVO(1, "Test Channel Num", "test channel Type", "Test 1");
        deviceTypeList.add(deviceVO);
        serialNumber = "BB02307";
        instance.setSerialNumberInput(serialNumber);
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of init method, of class ReplaceDeviceBean.
     */
    @Test
    public void test01_Init() {
        System.out.println("init");
        instance.setSerialNumberInput("BB001788");
        instance.init();
        assertEquals(instance.getSerialNumberInput(), null);
    }
    
    /**
     * Test of init method, of class ReplaceDeviceBean.
     */
    @Test
    public void test02_Init() {
        System.out.println("init");
        instance.setSerialNumberInput("BB001788");
        instance.init();
        assertNotEquals(instance.getSerialNumberInput(), "BB001788");
    }

    /**
     * Test of resetPage method, of class ReplaceDeviceBean.
     */
    @Test
    public void test03_ResetPage() {
        System.out.println("resetPage");
        instance.setSerialNumberInput("BB001788");
        instance.resetPage();
        assertEquals(instance.getSerialNumberInput(), null);
    }
    
    /**
     * Test of resetPage method, of class ReplaceDeviceBean.
     */
    @Test
    public void test04_ResetPage() {
        System.out.println("resetPage");
        instance.setSerialNumberInput("BB001788");
        instance.resetPage();
        assertNotEquals(instance.getSerialNumberInput(), "BB001788");
    }

    /**
     * Test of presetFields method, of class ReplaceDeviceBean.
     */
    @Test
    public void test05_PresetFields_Filters_Object() {
        System.out.println("presetFields");
        filters.setArea(new SelectItem(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"), "Test Area 2"));
        Filters presets = filters;
        Object object = null;
        
        when(pointClient.getPointsByCustomerSiteAreaAssetPointLocation(CONFIGURATION_SERVICE_NAME, "FEDEX EXPRESS",
                UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"),
                UUID.fromString("f11fcf5a-5f93-4504-bc07-74a61a460bf0"),
                UUID.fromString("f22fcf5a-5f93-4504-bc07-74a61a460bf0"),
                UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), true)).thenReturn(pointVOList);
        
        Whitebox.setInternalState(instance, "existingDeviceId", "BB002307");
        UUID expectedResult = UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0");
        instance.presetFields(presets, object);
        UUID result = instance.getReplaceDeviceVO().getSiteId();

        assertEquals(expectedResult, result);
        System.out.println("presetFields Done");
    }
    
    /**
     * Test of presetFields method, of class ReplaceDeviceBean.
     */
    @Test
    public void test06_PresetFields_Filters_Object() {
        System.out.println("presetFields");
        filters.setArea(new SelectItem(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"), "Test Area 2"));
        Filters presets = filters;
        Object object = null;
        
        when(pointClient.getPointsByCustomerSiteAreaAssetPointLocation(CONFIGURATION_SERVICE_NAME, "FEDEX EXPRESS",
                UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"),
                UUID.fromString("f11fcf5a-5f93-4504-bc07-74a61a460bf0"),
                UUID.fromString("f22fcf5a-5f93-4504-bc07-74a61a460bf0"),
                UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), true)).thenReturn(pointVOList);
        
        Whitebox.setInternalState(instance, "existingDeviceId", "BB002307");
        UUID expectedResult = null;
        instance.presetFields(presets, object);
        UUID result = instance.getReplaceDeviceVO().getSiteId();

        assertNotEquals(expectedResult, result);
        System.out.println("presetFields Done");
    }

    /**
     * Test of validateSerialNum method, of class ReplaceDeviceBean.
     */
    @Test
    public void test07_ValidateSerialNum() {
        System.out.println("validateSerialNum");
        List<HwUnitVO> list = new ArrayList();
        HwUnitVO hwUnitVO = new HwUnitVO();
        hwUnitVO.setExistingDeviceId("BB024038");
        list.add(hwUnitVO);
        when(hwUnitToPointClient.getHwUnitToPointByDevice(CONFIGURATION_SERVICE_NAME, serialNumber)).thenReturn(list);
        String exptedResult = "test channel Type";
        instance.validateSerialNum();
        assertEquals(exptedResult, deviceTypeList.get(0).getChannelType());
        System.out.println("validateSerialNum Done");
    }
    
    
    /**
     * Test of validateSerialNum method, of class ReplaceDeviceBean.
     */
    @Test
    public void test08_ValidateSerialNum() {
        System.out.println("validateSerialNum");
        List<HwUnitVO> list = new ArrayList();
        HwUnitVO hwUnitVO = new HwUnitVO();
        hwUnitVO.setExistingDeviceId("BB024038");
        list.add(hwUnitVO);
        when(hwUnitToPointClient.getHwUnitToPointByDevice(CONFIGURATION_SERVICE_NAME, serialNumber)).thenReturn(list);
        String exptedResult = "";
        instance.validateSerialNum();
        assertNotEquals(exptedResult, deviceTypeList.get(0).getChannelType());
        System.out.println("validateSerialNum Done");
    }


    /**
     * Test of submit method, of class ReplaceDeviceBean.
     */
    @Test
    public void test09_Submit() {
        System.out.println("submit");
        String existingDeviceId = "BB023056";
        instance.onSerialNumberChange();
        instance.setExistingDeviceId(existingDeviceId);
        instance.submit();
        System.out.println("submit done");
    }
    
    
    /**
     * Test of submit method, of class ReplaceDeviceBean.
     */
    @Test
    public void test10_Submit() {
        System.out.println("submit");
        String existingDeviceId = "BB023056";
        instance.onSerialNumberChange();
        instance.setExistingDeviceId(existingDeviceId);
        when(manageDeviceClient.replaceDevice(CONFIGURATION_SERVICE_NAME, replaceDevice)).thenReturn(false);
        instance.submit();
        System.out.println("submit done");
    }

}
