/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.AreaClient;
import com.uptime.upcastcm.http.client.AssetClient;
import com.uptime.upcastcm.http.client.PointClient;
import com.uptime.upcastcm.http.client.PointLocationClient;
import com.uptime.upcastcm.http.client.SiteClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import com.uptime.upcastcm.utils.helperclass.Filters;
import com.uptime.upcastcm.utils.helperclass.PresetUtil;
import com.uptime.upcastcm.utils.vo.AreaVO;
import com.uptime.upcastcm.utils.vo.AssetVO;
import com.uptime.upcastcm.utils.vo.PointLocationVO;
import com.uptime.upcastcm.utils.vo.PointVO;
import com.uptime.upcastcm.utils.vo.SiteVO;
import com.uptime.upcastcm.utils.vo.UserSitesVO;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.faces.component.UIViewRoot;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.junit.Assert;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.Test;
import org.junit.FixMethodOrder;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.junit.runners.MethodSorters;
import org.mockito.Mock;
import static org.mockito.Mockito.mock;
import org.powermock.api.mockito.PowerMockito;
import static org.powermock.api.mockito.PowerMockito.when;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;
import org.powermock.reflect.Whitebox;
import org.primefaces.PrimeFaces;
import org.primefaces.event.NodeCollapseEvent;
import org.primefaces.event.NodeExpandEvent;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.TreeNode;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.MenuModel;

/**
 *
 * @author twilcox
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(PowerMockRunner.class)
@PowerMockIgnore("jdk.internal.reflect.*")
@PowerMockRunnerDelegate(JUnit4.class)
@PrepareForTest({FacesContext.class, ExternalContext.class, ServiceRegistrarClient.class, NavigationBean.class, UserManageBean.class, CreateAssetBean.class, SiteClient.class, AreaClient.class, AssetClient.class, PointLocationClient.class, PointClient.class, PresetUtil.class, PrimeFaces.class, PrimeFaces.Ajax.class})
public class ConfigurationBeanTest {

    @Mock
    ServiceRegistrarClient serviceRegistrarClient;
    private final List<UserSitesVO> userSiteList;
    @Mock
    SiteClient siteClient;
    @Mock
    AreaClient areaClient;
    @Mock
    AssetClient assetClient;
    @Mock
    PointLocationClient pointLocationClient;
    @Mock
    PointClient pointClient;
    @Mock
    PresetUtil presetUtil;
    @Mock
    HttpServletRequest request;
    @Mock
    HttpServletResponse response;  // Mock HttpServletRespons
    @Mock
    HttpSession session;
    @Mock
    UIViewRoot root;
    @Mock
    FacesContext facesContext;
    @Mock
    ExternalContext externalContext;
    @Mock
    PrimeFaces pf;
    @Mock
    PrimeFaces.Ajax ax;
    @Mock
    NavigationBean navigationBean;
    @Mock
    UserManageBean userManageBean;
    @Mock
    CreateAssetBean createAssetBean;
    List<AssetVO> assetVOList;
    private final ConfigurationBean instance;
    Map<String, Object> sessionMap;
    String customerAccount;
    List<SiteVO> siteVOList;
    MenuModel breadcrumbModel;
    private final Filters filters;

    public ConfigurationBeanTest() {
        PowerMockito.mockStatic(FacesContext.class);
        PowerMockito.mockStatic(PrimeFaces.class);
        PowerMockito.mockStatic(PrimeFaces.Ajax.class);
        PowerMockito.mockStatic(ServiceRegistrarClient.class);
        PowerMockito.mockStatic(SiteClient.class);
        PowerMockito.mockStatic(AreaClient.class);
        PowerMockito.mockStatic(AssetClient.class);
        PowerMockito.mockStatic(PointLocationClient.class);
        PowerMockito.mockStatic(PointClient.class);

        PrimeFaces pf = PowerMockito.mock(PrimeFaces.class);
        PrimeFaces.Ajax ax = PowerMockito.mock(PrimeFaces.Ajax.class);
        when(pf.current()).thenReturn(pf);
        when(pf.current().ajax()).thenReturn(ax);

        facesContext = PowerMockito.mock(FacesContext.class);
        externalContext = PowerMockito.mock(ExternalContext.class);
        request = PowerMockito.mock(HttpServletRequest.class);
        response = PowerMockito.mock(HttpServletResponse.class);
        session = PowerMockito.mock(HttpSession.class);
        root = PowerMockito.mock(UIViewRoot.class);
        navigationBean = PowerMockito.mock(NavigationBean.class);
        userManageBean = PowerMockito.mock(UserManageBean.class);
        createAssetBean = PowerMockito.mock(CreateAssetBean.class);
        serviceRegistrarClient = PowerMockito.mock(ServiceRegistrarClient.class);
        siteClient = PowerMockito.mock(SiteClient.class);
        areaClient = PowerMockito.mock(AreaClient.class);
        presetUtil = PowerMockito.mock(PresetUtil.class);
        assetClient = PowerMockito.mock(AssetClient.class);
        pointLocationClient = PowerMockito.mock(PointLocationClient.class);
        pointClient = PowerMockito.mock(PointClient.class);

        sessionMap = new HashMap();
        sessionMap.put("navigationBean", navigationBean);
        sessionMap.put("userManageBean", userManageBean);
        sessionMap.put("createAssetBean", createAssetBean);

        session.setAttribute("navigationBean", navigationBean);
        session.setAttribute("userManageBean", userManageBean);
        session.setAttribute("createAssetBean", createAssetBean);

        userSiteList = new ArrayList<>();
        UserSitesVO uvo = new UserSitesVO();
        uvo.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        uvo.setSiteName("Test Site 1");
        userSiteList.add(uvo);

        when(FacesContext.getCurrentInstance()).thenReturn(facesContext);
        when(facesContext.getExternalContext()).thenReturn(externalContext);
        when(externalContext.getSessionMap()).thenReturn(sessionMap);
        when(facesContext.getExternalContext().getRequest()).thenReturn(request);
        when(facesContext.getExternalContext().getResponse()).thenReturn(response);
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("navigationBean")).thenReturn(navigationBean);
        when(session.getAttribute("userManageBean")).thenReturn(userManageBean);
        when(session.getAttribute("createAssetBean")).thenReturn(createAssetBean);
        when(userManageBean.getUserSiteList()).thenReturn(userSiteList);
        when(facesContext.getViewRoot()).thenReturn(root);
        when(ServiceRegistrarClient.getInstance()).thenReturn(serviceRegistrarClient);
        when(SiteClient.getInstance()).thenReturn(siteClient);
        when(AreaClient.getInstance()).thenReturn(areaClient);
        when(AssetClient.getInstance()).thenReturn(assetClient);
        when(PointLocationClient.getInstance()).thenReturn(pointLocationClient);
        when(PointClient.getInstance()).thenReturn(pointClient);

        when(userManageBean.getPresetUtil()).thenReturn(presetUtil);

        TreeNode tn = PowerMockito.mock(TreeNode.class);
        when(tn.getData()).thenReturn(new String());
        List<TreeNode> tnList = new ArrayList<>();
        tnList.add(tn);
        when(navigationBean.getTreeNodeRoot()).thenReturn(tn);
        when(navigationBean.getTreeNodeRoot().getChildren()).thenReturn(tnList);

        customerAccount = "77777";

        UserSitesVO usvo = new UserSitesVO();
        usvo.setCustomerAccount("77777");
        when(userManageBean.getCurrentSite()).thenReturn(usvo);
        instance = new ConfigurationBean();

        instance.setSelectedFilters();
        instance.getSelectedFilters().setCountry("Country 1");
        instance.getSelectedFilters().setRegion("Region 1");
        instance.getSelectedFilters().setSite(new SelectItem(UUID.randomUUID(), "site"));
        instance.getSelectedFilters().setArea(new SelectItem(UUID.randomUUID(), "area"));
        instance.getSelectedFilters().setAsset(new SelectItem(UUID.randomUUID(), "asset"));
        instance.getSelectedFilters().setPointLocation(new SelectItem(UUID.randomUUID(), "ptlo"));
        instance.getSelectedFilters().setPoint(new SelectItem(UUID.randomUUID(), "point"));
         filters = new Filters();
        filters.setCountry("Test Country 1");
        filters.setRegion("Test Region 1");
        filters.setSite(new SelectItem(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), "Test Site 1"));
        filters.setArea(new SelectItem(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"), "Test Area 1"));
        filters.setAsset(new SelectItem(UUID.fromString("f33fcf5a-5f93-4504-bc09-74a61a460bf0"), "Test Asset 1"));
       
         when(userManageBean.getSelectedFilters()).thenReturn(filters);
    }

    @Before
    public void setUp() {
        SiteVO siteVO;
        //String siteId = "e4c66964-5a8c-451d-ae2e-9d04d83c8241";
        //String assetTypeId = "53a1bc71-94c5-4ab9-acfa-07304e74c4df";  
        siteVOList = new ArrayList();
        siteVO = new SiteVO();
        siteVO.setRegion("REGION_1");
        siteVO.setCountry("COUNTRY_1");
        siteVO.setSiteName("SITE_1");
        siteVO.setSiteId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        siteVOList.add(siteVO);
        UserSitesVO uvo = new UserSitesVO();
        uvo.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        uvo.setSiteName("Test Site 1");
        when(userManageBean.getCurrentSite()).thenReturn(uvo);

        breadcrumbModel = new DefaultMenuModel();
        when(navigationBean.getBreadcrumbModel()).thenReturn(breadcrumbModel);
    }

    /**
     * Test of init method, of class ConfigurationBean.
     */
    @Test
    public void test01_Init() {
        System.out.println("test01_Init");
        try {
            List<SiteVO> list = new ArrayList();
            SiteVO s1 = new SiteVO();
            s1.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
            s1.setSiteName("Test Site 1");
            s1.setRegion("Region1");
            s1.setCountry("Country1");
            list.add(s1);
            when(serviceRegistrarClient.getServiceHostURL(CONFIGURATION_SERVICE_NAME)).thenReturn(CONFIGURATION_SERVICE_NAME);
            when(siteClient.getSiteRegionCountryByCustomer(CONFIGURATION_SERVICE_NAME, customerAccount)).thenReturn(list);
            instance.init();
            System.out.println("init completed");
        } catch (Exception ex) {
            fail("The method init Failed.");
        }
    }

    /**
     * Test of init method, of class ConfigurationBean.
     */
    @Test
    public void test02_Init() {
        System.out.println("test01_Init");
        try {
            List<SiteVO> list = new ArrayList();
            SiteVO s1 = new SiteVO();
            s1.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
            s1.setSiteName("Test Site 1");
            s1.setRegion("Region1");
            s1.setCountry("Country1");
            list.add(s1);
            when(serviceRegistrarClient.getServiceHostURL(CONFIGURATION_SERVICE_NAME)).thenReturn(CONFIGURATION_SERVICE_NAME);
            when(siteClient.getSiteRegionCountryByCustomer(CONFIGURATION_SERVICE_NAME, customerAccount)).thenReturn(list);
            instance.init();
            System.out.println("init completed");
        } catch (Exception ex) {
            fail("The method init Failed.");
        }
    }

    /**
     * Test of resetPage method, of class ConfigurationBean.
     */
    @Test
    public void test03_ResetPage() {
        System.out.println("test02_ResetPage");
        try {
            Whitebox.setInternalState(instance, "siteVOList", siteVOList);
            when(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME)).thenReturn("test");
            when(siteClient.getSiteRegionCountryByCustomer("test", customerAccount)).thenReturn(siteVOList);
            instance.resetPage();
            assertEquals(instance.getSiteVOList().size(), 0);
            System.out.println("resetPage completed");
        } catch (Exception ex) {
            fail("The method resetPage Failed.");
        }
    }

    /**
     * Test of resetPage method, of class ConfigurationBean.
     */
    @Test
    public void test04_ResetPage() {
        System.out.println("test02_ResetPage");
        try {
            Whitebox.setInternalState(instance, "siteVOList", siteVOList);
            when(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME)).thenReturn("test");
            when(siteClient.getSiteRegionCountryByCustomer("test", customerAccount)).thenReturn(siteVOList);
            instance.resetPage();
            assertNotEquals(instance.getSiteVOList().size(), 1);
            System.out.println("resetPage completed");
        } catch (Exception ex) {
            fail("The method resetPage Failed.");
        }
    }

    /**
     * Test of onRegionChange method, of class ConfigurationBean.
     */
    @Test
    public void test05_OnRegionChange() {
        System.out.println("test05_OnRegionChange");
        try {
            instance.getSelectedFilters().setRegion("Test Region");
            instance.onRegionChange(true);
            boolean expectedResult = instance.getSelectedFilters().getCountry() == null;
            assertTrue(instance.getSelectedFilters().getRegion().equals("Test Region"));
            assertTrue(expectedResult);
            assertTrue(instance.getSelectedFilters().getSite() == null);
            System.out.println("test05_OnRegionChange completed");
        } catch (Exception ex) {
            System.out.println("Exception occured :  completed" + ex.getMessage());
            fail("The method onRegionChange Failed.");
        }
    }

    /**
     * Test of onRegionChange method, of class ConfigurationBean.
     */
    @Test
    public void test06_OnRegionChange() {
        System.out.println("test06_OnRegionChange");
        try {
            instance.getSelectedFilters().setRegion("Test Region");
            instance.onRegionChange(true);
            boolean expectedResult = instance.getSelectedFilters().getCountry() != null;
            assertFalse(expectedResult);
            System.out.println("test06_OnRegionChange completed");
        } catch (Exception ex) {
            System.out.println("Exception occured :  completed" + ex.getMessage());
            fail("The method onRegionChange Failed.");
        }
    }

    /**
     * Test of onCountryChange method, of class ConfigurationBean.
     */
    @Test
    public void test07_OnCountryChange() {
        System.out.println("test08_OnCountryChange");

        try {
            instance.getSelectedFilters().setCountry("US");
            instance.onCountryChange(true);
            boolean expectedResult = instance.getSelectedFilters().getSite() == null;
            assertTrue(expectedResult);
            System.out.println("test08_OnCountryChange completed");
        } catch (Exception ex) {
            fail("The method onCountryChange Failed.");
        }
    }

    /**
     * Test of onCountryChange method, of class ConfigurationBean.
     */
    @Test
    public void test08_OnCountryChange() {
        System.out.println("test09_OnCountryChange");

        try {
            instance.getSelectedFilters().setCountry("US");
            instance.onCountryChange(true);
            boolean expectedResult = instance.getSelectedFilters().getSite() != null;
            assertFalse(expectedResult);
            System.out.println("test09_OnCountryChange completed");
        } catch (Exception ex) {
            fail("The method onCountryChange Failed.");
        }
    }

    /**
     * Test of onSiteChange method, of class ConfigurationBean.
     */
    @Test
    public void test09_OnSiteChange() {
        System.out.println("test09_OnSiteChange");

        try {
            instance.getSelectedFilters().setSite(new SelectItem(UUID.randomUUID(), "Test site 1"));
            instance.onSiteChange(true);
            boolean expectedResult = instance.getSelectedFilters().getArea() == null;
            assertTrue(expectedResult);
            System.out.println("test09_OnSiteChange completed");
        } catch (Exception ex) {
            fail("The method onSiteChange Failed.");
        }
    }

    /**
     * Test of onSiteChange method, of class ConfigurationBean.
     */
    @Test
    public void test10_OnSiteChange() {
        System.out.println("test10_OnSiteChange");

        try {
            instance.getSelectedFilters().setSite(new SelectItem(UUID.randomUUID(), "Test site 1"));
            instance.onSiteChange(true);
            boolean expectedResult = instance.getSelectedFilters().getArea() != null;
            assertFalse(expectedResult);
            System.out.println("test09_OnSiteChange completed");
        } catch (Exception ex) {
            fail("The method onSiteChange Failed.");
        }
    }

    /**
     * Test of onAreaChange method, of class ConfigurationBean.
     */
    @Test
    public void test11_OnAreaChange() {
        System.out.println("test10_OnAreaChange");

        try {
            instance.getSelectedFilters().setArea(new SelectItem(UUID.randomUUID(), "Test Area 1"));
            instance.onAreaChange(true);
            boolean expectedResult = instance.getSelectedFilters().getAsset() == null;
            assertTrue(expectedResult);
            System.out.println("test10_OnAreaChange completed");
        } catch (Exception ex) {
            fail("The method onAreaChange Failed.");
        }
    }

    /**
     * Test of onAreaChange method, of class ConfigurationBean.
     */
    @Test
    public void test12_OnAreaChange() {
        System.out.println("test12_OnAreaChange");

        try {
            instance.getSelectedFilters().setArea(new SelectItem(UUID.randomUUID(), "Test Area 1"));
            instance.onAreaChange(true);
            boolean expectedResult = instance.getSelectedFilters().getAsset() != null;
            assertFalse(expectedResult);
            System.out.println("test12_OnAreaChange completed");
        } catch (Exception ex) {
            fail("The method onAreaChange Failed.");
        }
    }

    /**
     * Test of onAssetChange method, of class ConfigurationBean.
     */
    @Test
    public void test13_OnAssetChange() {
        System.out.println("test13_OnAssetChange");

        try {
            List<AssetVO> assetVOList = new ArrayList();
            AssetVO assetVO = new AssetVO();

            List<PointLocationVO> pointLocationVoList = new ArrayList();
            PointLocationVO pointLocation = new PointLocationVO();
            instance.setSelectedFilters();
            instance.getSelectedFilters().setCountry("Country 1");
            instance.getSelectedFilters().setRegion("Region 1");
            instance.getSelectedFilters().setSite(new SelectItem(UUID.randomUUID(), "site"));
            instance.getSelectedFilters().setArea(new SelectItem(UUID.randomUUID(), "area"));
            instance.getSelectedFilters().setAsset(new SelectItem(UUID.randomUUID(), "asset"));
            instance.getSelectedFilters().setPointLocation(new SelectItem(UUID.randomUUID(), "ptlo"));
            instance.getSelectedFilters().setPoint(new SelectItem(UUID.randomUUID(), "point"));
            Filters selectedFilters = instance.getSelectedFilters();

            assetVO.setCustomerAccount(customerAccount);
            assetVO.setSiteId((UUID) selectedFilters.getSite().getValue());
            assetVO.setAreaId((UUID) selectedFilters.getArea().getValue());
            assetVO.setAssetId((UUID) selectedFilters.getAsset().getValue());
            assetVOList.add(assetVO);

            pointLocation.setCustomerAccount(customerAccount);
            pointLocation.setSiteId((UUID) selectedFilters.getSite().getValue());
            pointLocation.setAreaId((UUID) selectedFilters.getArea().getValue());
            pointLocation.setAssetId((UUID) selectedFilters.getAsset().getValue());
            pointLocation.setPointLocationId((UUID) selectedFilters.getPointLocation().getValue());
            pointLocationVoList.add(pointLocation);
            when(serviceRegistrarClient.getServiceHostURL(CONFIGURATION_SERVICE_NAME)).thenReturn(CONFIGURATION_SERVICE_NAME);
            when(assetClient.getAssetSiteAreaByCustomerSiteArea(CONFIGURATION_SERVICE_NAME, customerAccount, (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue())).thenReturn(assetVOList);
            when(pointLocationClient.getPointLocationsByCustomerSiteAreaAsset(CONFIGURATION_SERVICE_NAME, customerAccount, (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue(), (UUID) selectedFilters.getAsset().getValue())).thenReturn(pointLocationVoList);
            instance.onAssetChange(true);
            boolean expectedResult = instance.getSelectedFilters().getPointLocation() == null;
            assertTrue(expectedResult);
            System.out.println("test13_OnAssetChange completed");
        } catch (Exception ex) {
            fail("The method onAssetChange Failed.");
        }
    }

    /**
     * Test of onAssetChange method, of class ConfigurationBean.
     */
    @Test
    public void test14_OnAssetChange() {
        System.out.println("test14_OnAssetChange");

        try {
            List<AssetVO> assetVOList = new ArrayList();
            AssetVO assetVO = new AssetVO();

            List<PointLocationVO> pointLocationVoList = new ArrayList();
            PointLocationVO pointLocation = new PointLocationVO();
            instance.setSelectedFilters();
            instance.getSelectedFilters().setCountry("Country 1");
            instance.getSelectedFilters().setRegion("Region 1");
            instance.getSelectedFilters().setSite(new SelectItem(UUID.randomUUID(), "site"));
            instance.getSelectedFilters().setArea(new SelectItem(UUID.randomUUID(), "area"));
            instance.getSelectedFilters().setAsset(new SelectItem(UUID.randomUUID(), "asset"));
            instance.getSelectedFilters().setPointLocation(new SelectItem(UUID.randomUUID(), "ptlo"));
            instance.getSelectedFilters().setPoint(new SelectItem(UUID.randomUUID(), "point"));
            Filters selectedFilters = instance.getSelectedFilters();

            assetVO.setCustomerAccount(customerAccount);
            assetVO.setSiteId((UUID) selectedFilters.getSite().getValue());
            assetVO.setAreaId((UUID) selectedFilters.getArea().getValue());
            assetVO.setAssetId((UUID) selectedFilters.getAsset().getValue());
            assetVOList.add(assetVO);

            pointLocation.setCustomerAccount(customerAccount);
            pointLocation.setSiteId((UUID) selectedFilters.getSite().getValue());
            pointLocation.setAreaId((UUID) selectedFilters.getArea().getValue());
            pointLocation.setAssetId((UUID) selectedFilters.getAsset().getValue());
            pointLocation.setPointLocationId((UUID) selectedFilters.getPointLocation().getValue());
            pointLocationVoList.add(pointLocation);
            when(serviceRegistrarClient.getServiceHostURL(CONFIGURATION_SERVICE_NAME)).thenReturn(CONFIGURATION_SERVICE_NAME);
            when(assetClient.getAssetSiteAreaByCustomerSiteArea(CONFIGURATION_SERVICE_NAME, customerAccount, (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue())).thenReturn(assetVOList);
            when(pointLocationClient.getPointLocationsByCustomerSiteAreaAsset(CONFIGURATION_SERVICE_NAME, customerAccount, (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue(), (UUID) selectedFilters.getAsset().getValue())).thenReturn(pointLocationVoList);
            instance.onAssetChange(true);
            boolean expectedResult = instance.getSelectedFilters().getPointLocation() != null;
            assertFalse(expectedResult);
            System.out.println("test14_OnAssetChange completed");
        } catch (Exception ex) {
            fail("The method onAssetChange Failed.");
        }
    }

    /**
     * Test of onPointLocationChange method, of class ConfigurationBean.
     */
    @Test
    public void test15_OnPointLocationChange() {
        System.out.println("test15_OnPointLocationChange");

        try {
            List<PointLocationVO> pointLocationVoList = new ArrayList();
            PointLocationVO selectedPointLocation = new PointLocationVO();
            instance.setSelectedFilters();
            instance.getSelectedFilters().setCountry("Country 1");
            instance.getSelectedFilters().setRegion("Region 1");
            instance.getSelectedFilters().setSite(new SelectItem(UUID.randomUUID(), "site"));
            instance.getSelectedFilters().setArea(new SelectItem(UUID.randomUUID(), "area"));
            instance.getSelectedFilters().setAsset(new SelectItem(UUID.randomUUID(), "asset"));
            instance.getSelectedFilters().setPointLocation(new SelectItem(UUID.randomUUID(), "ptlo"));
            instance.getSelectedFilters().setPoint(new SelectItem(UUID.randomUUID(), "point"));
            Filters selectedFilters = instance.getSelectedFilters();

            selectedPointLocation.setCustomerAccount(customerAccount);
            selectedPointLocation.setSiteId((UUID) selectedFilters.getSite().getValue());
            selectedPointLocation.setAreaId((UUID) selectedFilters.getArea().getValue());
            selectedPointLocation.setAssetId((UUID) selectedFilters.getAsset().getValue());
            selectedPointLocation.setPointLocationId((UUID) selectedFilters.getPointLocation().getValue());
            pointLocationVoList.add(selectedPointLocation);
            when(serviceRegistrarClient.getServiceHostURL(CONFIGURATION_SERVICE_NAME)).thenReturn(CONFIGURATION_SERVICE_NAME);
            when(pointLocationClient.getPointLocationsByPK(CONFIGURATION_SERVICE_NAME, customerAccount, (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue(), (UUID) selectedFilters.getAsset().getValue(), (UUID) selectedFilters.getPointLocation().getValue())).thenReturn(pointLocationVoList);
            instance.onPointLocationChange(true);
            boolean expectedResult = instance.getSelectedFilters().getPoint() == null;
            assertTrue(expectedResult);
            System.out.println("test15_OnPointLocationChange completed");
        } catch (Exception ex) {
            fail("The method onPointLocationChange Failed.");
        }
    }

    /**
     * Test of onPointLocationChange method, of class ConfigurationBean.
     */
    @Test
    public void test16_OnPointLocationChange() {
        System.out.println("test16_OnPointLocationChange");

        try {
            List<PointLocationVO> pointLocationVoList = new ArrayList();
            PointLocationVO selectedPointLocation = new PointLocationVO();
            instance.setSelectedFilters();
            instance.getSelectedFilters().setCountry("Country 1");
            instance.getSelectedFilters().setRegion("Region 1");
            instance.getSelectedFilters().setSite(new SelectItem(UUID.randomUUID(), "site"));
            instance.getSelectedFilters().setArea(new SelectItem(UUID.randomUUID(), "area"));
            instance.getSelectedFilters().setAsset(new SelectItem(UUID.randomUUID(), "asset"));
            instance.getSelectedFilters().setPointLocation(new SelectItem(UUID.randomUUID(), "ptlo"));
            instance.getSelectedFilters().setPoint(new SelectItem(UUID.randomUUID(), "point"));
            Filters selectedFilters = instance.getSelectedFilters();

            selectedPointLocation.setCustomerAccount(customerAccount);
            selectedPointLocation.setSiteId((UUID) selectedFilters.getSite().getValue());
            selectedPointLocation.setAreaId((UUID) selectedFilters.getArea().getValue());
            selectedPointLocation.setAssetId((UUID) selectedFilters.getAsset().getValue());
            selectedPointLocation.setPointLocationId((UUID) selectedFilters.getPointLocation().getValue());
            pointLocationVoList.add(selectedPointLocation);
            when(serviceRegistrarClient.getServiceHostURL(CONFIGURATION_SERVICE_NAME)).thenReturn(CONFIGURATION_SERVICE_NAME);
            when(pointLocationClient.getPointLocationsByPK(CONFIGURATION_SERVICE_NAME, customerAccount, (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue(), (UUID) selectedFilters.getAsset().getValue(), (UUID) selectedFilters.getPointLocation().getValue())).thenReturn(pointLocationVoList);
            instance.onPointLocationChange(true);
            boolean expectedResult = instance.getSelectedFilters().getPoint() != null;
            assertFalse(expectedResult);
            System.out.println("test16_OnPointLocationChange completed");
        } catch (Exception ex) {
            fail("The method onPointLocationChange Failed.");
        }
    }

    /**
     * Test of onPointChange method, of class ConfigurationBean.
     */
    @Test
    public void test17_OnPointChange() {
        System.out.println("test17_OnPointChange");

        try {
            List<PointVO> pointVOList = new ArrayList();
            PointVO pointVO = new PointVO();
            pointVO.setPointId(UUID.randomUUID());
            pointVO.setApSetId(UUID.randomUUID());
            pointVO.setAlSetId(UUID.randomUUID());
            pointVO.setPointName("Test Point Name11");
            pointVOList.add(pointVO);

            instance.setSelectedFilters();
            instance.getSelectedFilters().setCountry("Country 1");
            instance.getSelectedFilters().setRegion("Region 1");
            instance.getSelectedFilters().setSite(new SelectItem(UUID.randomUUID(), "site"));
            instance.getSelectedFilters().setArea(new SelectItem(UUID.randomUUID(), "area"));
            instance.getSelectedFilters().setAsset(new SelectItem(UUID.randomUUID(), "asset"));
            instance.getSelectedFilters().setPointLocation(new SelectItem(UUID.randomUUID(), "ptlo"));
            instance.getSelectedFilters().setPoint(new SelectItem(UUID.randomUUID(), "point"));
            Filters selectedFilters = instance.getSelectedFilters();
            when(serviceRegistrarClient.getServiceHostURL(CONFIGURATION_SERVICE_NAME)).thenReturn(CONFIGURATION_SERVICE_NAME);
            when(pointClient.getPointsByCustomerSiteAreaAssetPointLocationPoint(CONFIGURATION_SERVICE_NAME, customerAccount, (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue(), (UUID) selectedFilters.getAsset().getValue(), (UUID) selectedFilters.getPointLocation().getValue(), (UUID) selectedFilters.getPoint().getValue(), true)).thenReturn(pointVOList);

            SiteVO siteVO = new SiteVO();
            siteVO.setSiteId(UUID.randomUUID());
            siteVO.setSiteName("Test Site111");
            instance.getSiteVOList().add(siteVO);

            instance.onPointChange(true);

            boolean expectedResult = instance.getSiteVOList().isEmpty();
            assertTrue(expectedResult);
            System.out.println("test17_OnPointChange completed");
        } catch (Exception ex) {
            fail("The method onPointChange Failed.");
        }
    }

    /**
     * Test of onPointChange method, of class ConfigurationBean.
     */
    @Test
    public void test18_OnPointChange() {
        System.out.println("test18_OnPointChange");

        try {
            List<PointVO> pointVOList = new ArrayList();
            PointVO pointVO = new PointVO();
            pointVO.setPointId(UUID.randomUUID());
            pointVO.setApSetId(UUID.randomUUID());
            pointVO.setAlSetId(UUID.randomUUID());
            pointVO.setPointName("Test Point Name11");
            pointVOList.add(pointVO);

            instance.setSelectedFilters();
            instance.getSelectedFilters().setCountry("Country 1");
            instance.getSelectedFilters().setRegion("Region 1");
            instance.getSelectedFilters().setSite(new SelectItem(UUID.randomUUID(), "site"));
            instance.getSelectedFilters().setArea(new SelectItem(UUID.randomUUID(), "area"));
            instance.getSelectedFilters().setAsset(new SelectItem(UUID.randomUUID(), "asset"));
            instance.getSelectedFilters().setPointLocation(new SelectItem(UUID.randomUUID(), "ptlo"));
            instance.getSelectedFilters().setPoint(new SelectItem(UUID.randomUUID(), "point"));
            Filters selectedFilters = instance.getSelectedFilters();
            when(serviceRegistrarClient.getServiceHostURL(CONFIGURATION_SERVICE_NAME)).thenReturn(CONFIGURATION_SERVICE_NAME);
            when(pointClient.getPointsByCustomerSiteAreaAssetPointLocationPoint(CONFIGURATION_SERVICE_NAME, customerAccount, (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue(), (UUID) selectedFilters.getAsset().getValue(), (UUID) selectedFilters.getPointLocation().getValue(), (UUID) selectedFilters.getPoint().getValue(), true)).thenReturn(pointVOList);

            SiteVO siteVO = new SiteVO();
            siteVO.setSiteId(UUID.randomUUID());
            siteVO.setSiteName("Test Site111");
            instance.getSiteVOList().add(siteVO);

            instance.onPointChange(true);

            boolean expectedResult = instance.getSiteVOList().size() > 0;
            assertFalse(expectedResult);
            System.out.println("test18_OnPointChange completed");
        } catch (Exception ex) {
            fail("The method onPointChange Failed.");
        }
    }

    /**
     * Test of updateUI method, of class ConfigurationBean.
     */
    @Test
    public void test19_UpdateUI() {
        System.out.println("test19_UpdateUI");

        ax.addCallbackParam("configurationFilterFormId", new Object());
        ax.addCallbackParam("configurationResultFormId", new Object());
        instance.getSelectedFilters().setPoint(new SelectItem(UUID.randomUUID(), "point1"));
        instance.updateUI("content");
        boolean expectedResult = instance.getSelectedFilters().getPoint().getLabel().equals("point1");
        assertTrue(expectedResult);
        System.out.println("test19_UpdateUI completed");
    }

    /**
     * Test of updateUI method, of class ConfigurationBean.
     */
    @Test
    public void test20_UpdateUI() {
        System.out.println("test20_UpdateUI");

        ax.addCallbackParam("configurationFilterFormId", new Object());
        ax.addCallbackParam("configurationResultFormId", new Object());
        instance.getSelectedFilters().setPoint(new SelectItem(UUID.randomUUID(), "point1"));
        instance.updateUI("content");
        boolean expectedResult = instance.getSelectedFilters().getPoint().getLabel().equals("point11");
        assertFalse(expectedResult);
        System.out.println("test20_UpdateUI completed");
    }

    /**
     * Test of createItem method, of class ConfigurationBean.
     */
    @Test
    public void test21_CreateItem() {
        System.out.println("test21_CreateItem");
        instance.setSelectedFilters();
        instance.setSelectedFilters();
        instance.getSelectedFilters().setCountry("Country 1");
        instance.getSelectedFilters().setRegion("Region 1");
        instance.getSelectedFilters().setSite(new SelectItem(UUID.randomUUID(), "site"));
        instance.getSelectedFilters().setArea(new SelectItem(UUID.randomUUID(), "area"));
        instance.getSelectedFilters().setAsset(new SelectItem(UUID.randomUUID(), "asset"));
        instance.getSelectedFilters().setPointLocation(new SelectItem(UUID.randomUUID(), "ptlo"));
        instance.getSelectedFilters().setPoint(new SelectItem(UUID.randomUUID(), "point"));

        //instance.getSelectedFilters().setPoint(new SelectItem(UUID.randomUUID(), "point1"));
        instance.createItem();

        boolean expectedResult = instance.getSelectedFilters().getPointLocation().getLabel().equals("ptlo");
        assertTrue(expectedResult);
        System.out.println("test21_CreateItem completed");
    }

    /**
     * Test of createItem method, of class ConfigurationBean.
     */
    @Test
    public void test22_CreateItem() {
        System.out.println("test22_CreateItem");
        instance.setSelectedFilters();
        instance.setSelectedFilters();
        instance.getSelectedFilters().setCountry("Country 1");
        instance.getSelectedFilters().setRegion("Region 1");
        instance.getSelectedFilters().setSite(new SelectItem(UUID.randomUUID(), "site"));
        instance.getSelectedFilters().setArea(new SelectItem(UUID.randomUUID(), "area"));
        instance.getSelectedFilters().setAsset(new SelectItem(UUID.randomUUID(), "asset"));
        instance.getSelectedFilters().setPointLocation(new SelectItem(UUID.randomUUID(), "ptlo"));
        instance.getSelectedFilters().setPoint(new SelectItem(UUID.randomUUID(), "point"));

        //instance.getSelectedFilters().setPoint(new SelectItem(UUID.randomUUID(), "point1"));
        instance.createItem();

        boolean expectedResult = instance.getSelectedFilters().getPointLocation().getLabel().equals("ptloooo");
        assertFalse(expectedResult);
        System.out.println("test22_CreateItem completed");
    }

    /**
     * Test of updateItem method, of class ConfigurationBean.
     */
    @Test
    public void test23_UpdateItem() {
        System.out.println("test23_UpdateItem");
        Object item = new PointVO();
        instance.setSelectedFilters();
        instance.setSelectedFilters();
        instance.getSelectedFilters().setCountry("Country 1");
        instance.getSelectedFilters().setRegion("Region 1");
        instance.getSelectedFilters().setSite(new SelectItem(UUID.randomUUID(), "site"));
        instance.getSelectedFilters().setArea(new SelectItem(UUID.randomUUID(), "area"));
        instance.getSelectedFilters().setAsset(new SelectItem(UUID.randomUUID(), "asset"));
        instance.getSelectedFilters().setPointLocation(new SelectItem(UUID.randomUUID(), "ptlo"));
        instance.getSelectedFilters().setPoint(new SelectItem(UUID.randomUUID(), "point"));
        PointLocationVO selectedPointLocation = new PointLocationVO();
        selectedPointLocation.setPointLocationId(UUID.randomUUID());
        selectedPointLocation.setPointLocationName("ptlo");
        Whitebox.setInternalState(instance, "selectedPointLocation", selectedPointLocation);
        instance.updateItem(item);

        System.out.println("test23_UpdateItem completed");
    }

    /**
     * Test of pointLookupByRowToggle method, of class ConfigurationBean.
     */
    @Test
    public void test24_PointLookupByRowToggle() {
        System.out.println("test24_PointLookupByRowToggle");
        PointLocationVO pointLocationVO = new PointLocationVO();
        pointLocationVO.setPointLocationId(UUID.randomUUID());
        pointLocationVO.setPointLocationName("ptlo");

        List<PointVO> pointVOList = new ArrayList();
        PointVO pointVO = new PointVO();
        pointVO.setPointId(UUID.randomUUID());
        pointVO.setApSetId(UUID.randomUUID());
        pointVO.setAlSetId(UUID.randomUUID());
        pointVO.setPointName("Test Point Name11");
        pointVOList.add(pointVO);

        instance.setSelectedFilters();
        instance.setSelectedFilters();
        instance.getSelectedFilters().setCountry("Country 1");
        instance.getSelectedFilters().setRegion("Region 1");
        instance.getSelectedFilters().setSite(new SelectItem(UUID.randomUUID(), "site"));
        instance.getSelectedFilters().setArea(new SelectItem(UUID.randomUUID(), "area"));
        instance.getSelectedFilters().setAsset(new SelectItem(UUID.randomUUID(), "asset"));
        instance.getSelectedFilters().setPointLocation(new SelectItem(UUID.randomUUID(), "ptlo"));
        instance.getSelectedFilters().setPoint(new SelectItem(UUID.randomUUID(), "point"));
        Filters selectedFilters = instance.getSelectedFilters();
        Whitebox.setInternalState(instance, "pointVOList", pointVOList);
        when(serviceRegistrarClient.getServiceHostURL(CONFIGURATION_SERVICE_NAME)).thenReturn(CONFIGURATION_SERVICE_NAME);
        when(pointClient.getPointsByCustomerSiteAreaAssetPointLocation(CONFIGURATION_SERVICE_NAME, customerAccount, (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue(), (UUID) selectedFilters.getAsset().getValue(), pointLocationVO.getPointLocationId(), true)).thenReturn(pointVOList);

        List<SelectItem> list = new ArrayList();
        list.add(new SelectItem(UUID.fromString("f44fcf5a-5f93-4504-bc07-74a61a460bf0"), "Test Tach Name1"));
        when(presetUtil.populateTachometerList(userManageBean, customerAccount, pointLocationVO.getSiteId())).thenReturn(list);

        list = new ArrayList();
        list.add(new SelectItem(UUID.fromString("f44fcf5a-5f93-4504-bc07-74a61a460bf1"), "Test FF Set Name1"));
        //when(presetUtil.populateFFSetNameList(userManageBean, customerAccount, pointLocationVO.getSiteId())).thenReturn(list);

        instance.pointLookupByRowToggle(pointLocationVO);
        int expectedResult = 1;
        int result = instance.getPointVOList().size();
        assertEquals(expectedResult, result);
        System.out.println("test24_PointLookupByRowToggle completed");
    }

    /**
     * Test of pointLookupByRowToggle method, of class ConfigurationBean.
     */
    @Test
    public void test25_PointLookupByRowToggle() {
        System.out.println("test25_PointLookupByRowToggle");
        PointLocationVO pointLocationVO = new PointLocationVO();
        pointLocationVO.setPointLocationId(UUID.randomUUID());
        pointLocationVO.setPointLocationName("ptlo");

        List<PointVO> pointVOList = new ArrayList();
        PointVO pointVO = new PointVO();
        pointVO.setPointId(UUID.randomUUID());
        pointVO.setApSetId(UUID.randomUUID());
        pointVO.setAlSetId(UUID.randomUUID());
        pointVO.setPointName("Test Point Name11");
        pointVOList.add(pointVO);

        instance.setSelectedFilters();
        instance.setSelectedFilters();
        instance.getSelectedFilters().setCountry("Country 1");
        instance.getSelectedFilters().setRegion("Region 1");
        instance.getSelectedFilters().setSite(new SelectItem(UUID.randomUUID(), "site"));
        instance.getSelectedFilters().setArea(new SelectItem(UUID.randomUUID(), "area"));
        instance.getSelectedFilters().setAsset(new SelectItem(UUID.randomUUID(), "asset"));
        instance.getSelectedFilters().setPointLocation(new SelectItem(UUID.randomUUID(), "ptlo"));
        instance.getSelectedFilters().setPoint(new SelectItem(UUID.randomUUID(), "point"));
        Filters selectedFilters = instance.getSelectedFilters();
        Whitebox.setInternalState(instance, "pointVOList", pointVOList);
        when(serviceRegistrarClient.getServiceHostURL(CONFIGURATION_SERVICE_NAME)).thenReturn(CONFIGURATION_SERVICE_NAME);
        when(pointClient.getPointsByCustomerSiteAreaAssetPointLocation(CONFIGURATION_SERVICE_NAME, customerAccount, (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue(), (UUID) selectedFilters.getAsset().getValue(), pointLocationVO.getPointLocationId(), true)).thenReturn(pointVOList);

        instance.pointLookupByRowToggle(pointLocationVO);
        int expectedResult = 0;
        int result = instance.getPointVOList().size();
        assertNotEquals(expectedResult, result);
        System.out.println("test25_PointLookupByRowToggle completed");
    }

    /**
     * Test of updateSelection method, of class ConfigurationBean.
     */
    @Test
    public void test26_UpdateSelection() {
        System.out.println("test26_UpdateSelection");
        instance.setSelectedFilters();
        String expectedResult = "test site update";
        instance.updateSelection("site", "test site update", UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"), "Asset 2", UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        assertEquals(expectedResult, instance.getSelectedFilters().getSite().getLabel());
        System.out.println("test26_UpdateSelection Done");
    }

    /**
     * Test of updateSelection method, of class ConfigurationBean.
     */
    @Test
    public void test27_UpdateSelection() {
        System.out.println("test27_UpdateSelection");
        instance.setSelectedFilters();
        String expectedResult = "test site update";
        instance.updateSelection("site", "test site update", UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"), "Asset 2", UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        String result = instance.getSelectedFilters().getSite().getLabel() + " One";
        assertNotEquals(expectedResult, result);
        System.out.println("test27_UpdateSelection Done");
    }

    /**
     * Test of updateBreadcrumbModel method, of class ConfigurationBean.
     */
    @Test
    public void test28_UpdateBreadcrumbModel() {
        System.out.println("test28_UpdateBreadcrumbModel");

        try {
            when(userManageBean.getResourceBundleString("breadcrumb_4")).thenReturn("test");
            instance.setSelectedFilters();
            instance.getSelectedFilters().setCountry("Country 1");
            instance.getSelectedFilters().setRegion("Region 1");
            instance.getSelectedFilters().setSite(new SelectItem(UUID.randomUUID(), "site"));
            instance.getSelectedFilters().setArea(new SelectItem(UUID.randomUUID(), "area"));
            instance.getSelectedFilters().setAsset(new SelectItem(UUID.randomUUID(), "asset"));
            instance.getSelectedFilters().setPointLocation(new SelectItem(UUID.randomUUID(), "ptlo"));
            instance.getSelectedFilters().setPoint(new SelectItem(UUID.randomUUID(), "point"));
            instance.updateBreadcrumbModel();
            boolean expectedResult = instance.getSelectedFilters().getArea().getLabel().equals("area");
            assertTrue(expectedResult);
            System.out.println("test28_UpdateBreadcrumbModel Done");
        } catch (Exception ex) {
            fail("The method updateBreadcrumbModel Failed.");
        }
    }

    /**
     * Test of updateBreadcrumbModel method, of class ConfigurationBean.
     */
    @Test
    public void test29_UpdateBreadcrumbModel() {
        System.out.println("test29_UpdateBreadcrumbModel");

        try {
            when(userManageBean.getResourceBundleString("breadcrumb_4")).thenReturn("test");
            instance.setSelectedFilters();
            instance.getSelectedFilters().setCountry("Country 1");
            instance.getSelectedFilters().setRegion("Region 1");
            instance.getSelectedFilters().setSite(new SelectItem(UUID.randomUUID(), "site"));
            instance.getSelectedFilters().setArea(new SelectItem(UUID.randomUUID(), "area"));
            instance.getSelectedFilters().setAsset(new SelectItem(UUID.randomUUID(), "asset"));
            instance.getSelectedFilters().setPointLocation(new SelectItem(UUID.randomUUID(), "ptlo"));
            instance.getSelectedFilters().setPoint(new SelectItem(UUID.randomUUID(), "point"));
            instance.updateBreadcrumbModel();
            boolean expectedResult = instance.getSelectedFilters().getArea().getLabel().equals("");
            assertFalse(expectedResult);
            System.out.println("test29_UpdateBreadcrumbModel Done");
        } catch (Exception ex) {
            fail("The method updateBreadcrumbModel Failed.");
        }
    }

    /**
     * Test of treeContentMenuBuilder method, of class ConfigurationBean.
     */
    @Test
    public void test30_TreeContentMenuBuilder() {
        System.out.println("test30_TreeContentMenuBuilder");

        try {
            when(userManageBean.getResourceBundleString("breadcrumb_4")).thenReturn("test");
            instance.setSelectedFilters();
            instance.getSelectedFilters().setCountry("Country 1");

            instance.treeContentMenuBuilder(null, null, null, null);
            instance.treeContentMenuBuilder(ROOT, "", null, null);
            boolean expectedResult = instance.getSelectedFilters().getCountry().equals("Country 1");
            assertTrue(expectedResult);
            System.out.println("test30_TreeContentMenuBuilder completed");
        } catch (Exception ex) {
            fail("The method treeContentMenuBuilder Failed.");
        }
    }

    /**
     * Test of treeContentMenuBuilder method, of class ConfigurationBean.
     */
    @Test
    public void test31_TreeContentMenuBuilder() {
        System.out.println("test31_TreeContentMenuBuilder");

        try {
            when(userManageBean.getResourceBundleString("breadcrumb_4")).thenReturn("test");
            instance.setSelectedFilters();
            instance.getSelectedFilters().setCountry("Country 1");

            instance.treeContentMenuBuilder(null, null, null, null);
            instance.treeContentMenuBuilder(ROOT, "", null, null);
            boolean expectedResult = instance.getSelectedFilters().getCountry().equals("Country111");
            assertFalse(expectedResult);
            System.out.println("test31_TreeContentMenuBuilder completed");
        } catch (Exception ex) {
            fail("The method treeContentMenuBuilder Failed.");
        }
    }

    /**
     * Test of onNodeExpand method, of class ConfigurationBean.
     */
    @Test
    public void test32_OnNodeExpand() {
        System.out.println("test32_OnNodeExpand");
        NodeExpandEvent nee = PowerMockito.mock(NodeExpandEvent.class);
        TreeNode tn = PowerMockito.mock(TreeNode.class);

        when(nee.getTreeNode()).thenReturn(tn);
        when(tn.getType()).thenReturn("region");
        when(tn.getData()).thenReturn("Region1");

        List<TreeNode> tnList = new ArrayList<>();
        tnList.add(PowerMockito.mock(TreeNode.class));
        when(tn.getChildren()).thenReturn(tnList);
        test01_Init();

        instance.onNodeExpand(nee);
        assertEquals(nee.getTreeNode().getChildren().size(), 1);
        System.out.println("test32_OnNodeExpand completed");
        System.out.println("onNodeExpand completed");
    }

    /**
     * Test of onNodeExpand method, of class ConfigurationBean.
     */
    @Test
    public void test33_OnNodeExpand() {
        System.out.println("test33_OnNodeExpand");
        NodeExpandEvent nee = PowerMockito.mock(NodeExpandEvent.class);
        TreeNode tn = PowerMockito.mock(TreeNode.class);

        when(nee.getTreeNode()).thenReturn(tn);
        when(tn.getType()).thenReturn("region");
        when(tn.getData()).thenReturn("Region1");

        List<TreeNode> tnList = new ArrayList<>();
        tnList.add(PowerMockito.mock(TreeNode.class));
        when(tn.getChildren()).thenReturn(tnList);
        test01_Init();

        instance.onNodeExpand(nee);
        assertNotEquals(nee.getTreeNode().getChildren().size(), 0);
        assertNotEquals(nee.getTreeNode().getChildren().get(0).getType(), "country11");
        System.out.println("test33_OnNodeExpand completed");
        System.out.println("onNodeExpand completed");
    }

    /**
     * Test of onNodeCollapse method, of class ConfigurationBean.
     */
    @Test
    public void test34_OnNodeCollapse() {
        System.out.println("test34_OnNodeCollapse");
        NodeCollapseEvent nce = PowerMockito.mock(NodeCollapseEvent.class);
        TreeNode tn = PowerMockito.mock(TreeNode.class);

        when(nce.getTreeNode()).thenReturn(tn);
        when(tn.getType()).thenReturn("region");
        when(tn.getData()).thenReturn("Region1");

        List<TreeNode> tnList = new ArrayList<>();
        tnList.add(PowerMockito.mock(TreeNode.class));
        when(tn.getChildren()).thenReturn(tnList);

        instance.onNodeCollapse(nce);
        assertEquals(nce.getTreeNode().getChildren().size(), 1);
        assertEquals(nce.getTreeNode().getChildren().get(0).getType(), EMPTY);
        //instance.onNodeCollapse(null);
        System.out.println("test34_OnNodeCollapse completed");
    }

    /**
     * Test of onNodeCollapse method, of class ConfigurationBean.
     */
    @Test
    public void test35_OnNodeCollapse() {
        System.out.println("test35_OnNodeCollapse");
        NodeCollapseEvent nce = PowerMockito.mock(NodeCollapseEvent.class);
        TreeNode tn = PowerMockito.mock(TreeNode.class);

        when(nce.getTreeNode()).thenReturn(tn);
        when(tn.getType()).thenReturn("region");
        when(tn.getData()).thenReturn("Region1");

        List<TreeNode> tnList = new ArrayList<>();
        tnList.add(PowerMockito.mock(TreeNode.class));
        when(tn.getChildren()).thenReturn(tnList);

        instance.onNodeCollapse(nce);
        assertNotEquals(nce.getTreeNode().getChildren().size(), 0);
        assertNotEquals(nce.getTreeNode().getChildren().get(0).getType(), "empty11");
        //instance.onNodeCollapse(null);
        System.out.println("test35_OnNodeCollapse completed");
    }

    /**
     * Test of onNodeSelect method, of class ConfigurationBean.
     */
    @Test
    public void test36_OnNodeSelect() {
        System.out.println("test36_OnNodeSelect");
        NodeSelectEvent nse = PowerMockito.mock(NodeSelectEvent.class);
        TreeNode tn = PowerMockito.mock(TreeNode.class);

        when(nse.getTreeNode()).thenReturn(tn);
        when(tn.getType()).thenReturn("region");
        when(tn.getData()).thenReturn("region");

        List<TreeNode> tnList = new ArrayList<>();
        tnList.add(PowerMockito.mock(TreeNode.class));
        when(tn.getChildren()).thenReturn(tnList);
        when(tn.getParent()).thenReturn(tn);
        Whitebox.setInternalState(instance, "siteVOList", siteVOList);
        instance.onNodeSelect(nse);
        assertEquals(nse.getTreeNode().getChildren().size(), 1);
        assertEquals(instance.getSiteVOList().size(), 0);
        System.out.println("test36_OnNodeSelect completed");
    }

    /**
     * Test of onNodeSelect method, of class ConfigurationBean.
     */
    @Test
    public void test37_OnNodeSelect() {
        System.out.println("test37_OnNodeSelect");
        NodeSelectEvent nse = PowerMockito.mock(NodeSelectEvent.class);
        TreeNode tn = PowerMockito.mock(TreeNode.class);

        when(nse.getTreeNode()).thenReturn(tn);
        when(tn.getType()).thenReturn("region");
        when(tn.getData()).thenReturn("region");

        List<TreeNode> tnList = new ArrayList<>();
        tnList.add(PowerMockito.mock(TreeNode.class));
        when(tn.getChildren()).thenReturn(tnList);
        when(tn.getParent()).thenReturn(tn);
        Whitebox.setInternalState(instance, "siteVOList", siteVOList);
        instance.onNodeSelect(nse);
        assertNotEquals(nse.getTreeNode().getChildren().size(), 0);
        assertNotEquals(instance.getSiteVOList().size(), 1);
        System.out.println("test37_OnNodeSelect completed");
    }

    /**
     * Test of setRegionCountrySiteMap method, of class ConfigurationBean.
     */
    @Test
    public void test38_SetRegionCountrySiteMap() {
        System.out.println("test38_SetRegionCountrySiteMap");
        List<SiteVO> list = new ArrayList();
        SiteVO s1 = new SiteVO();
        s1.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        s1.setSiteName("Test Site 1");
        s1.setRegion("Region1");
        s1.setCountry("Country1");
        list.add(s1);
        when(serviceRegistrarClient.getServiceHostURL(CONFIGURATION_SERVICE_NAME)).thenReturn(CONFIGURATION_SERVICE_NAME);
        when(siteClient.getSiteRegionCountryByCustomer(CONFIGURATION_SERVICE_NAME, customerAccount)).thenReturn(list);
        instance.setRegionCountrySiteMap();
        boolean expectedResult = instance.countryListCheck();
        assertTrue(expectedResult);
        System.out.println("test38_SetRegionCountrySiteMap completed");
    }

    /**
     * Test of setRegionCountrySiteMap method, of class ConfigurationBean.
     */
    @Test
    public void test39_SetRegionCountrySiteMap() {
        System.out.println("test39_SetRegionCountrySiteMap");
        List<SiteVO> list = new ArrayList();
        SiteVO s1 = new SiteVO();
        s1.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        s1.setSiteName("Test Site 1");
        s1.setRegion("Region1");
        s1.setCountry("Country1");
        list.add(s1);
        when(serviceRegistrarClient.getServiceHostURL(CONFIGURATION_SERVICE_NAME)).thenReturn(CONFIGURATION_SERVICE_NAME);
        when(siteClient.getSiteRegionCountryByCustomer(CONFIGURATION_SERVICE_NAME, customerAccount)).thenReturn(list);
        instance.setRegionCountrySiteMap();
        boolean expectedResult = instance.countryListCheck() == false;
        assertFalse(expectedResult);
        System.out.println("test39_SetRegionCountrySiteMap completed");
    }

    /**
     * Test of getSiteVOList method, of class ConfigurationBean.
     */
    @Test
    public void test40_GetSiteVOList() {
        System.out.println("test40_GetSiteVOList");
        List<SiteVO> list = new ArrayList();
        SiteVO siteVO = new SiteVO();
        siteVO.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        siteVO.setSiteName("Test Site Name");
        list.add(siteVO);
        instance.getSelectedFilters().setRegion("Test Region");
        instance.getSelectedFilters().setCountry("Test Country");
        when(serviceRegistrarClient.getServiceHostURL(CONFIGURATION_SERVICE_NAME)).thenReturn(CONFIGURATION_SERVICE_NAME);
        when(siteClient.getSiteRegionCountryByCustomerRegionCountry(CONFIGURATION_SERVICE_NAME, customerAccount, instance.getSelectedFilters().getRegion(), instance.getSelectedFilters().getCountry())).thenReturn(list);
        assertEquals(new ArrayList(), instance.getSiteVOList());
        System.out.println("test40_GetSiteVOList completed");
    }

    /**
     * Test of getSiteVOList method, of class ConfigurationBean.
     */
    @Test
    public void test41_GetSiteVOList() {
        System.out.println("test41_GetSiteVOList");
        List<SiteVO> list = new ArrayList();
        SiteVO siteVO = new SiteVO();
        siteVO.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        siteVO.setSiteName("Test Site Name");
        list.add(siteVO);
        instance.getSelectedFilters().setRegion("Test Region");
        instance.getSelectedFilters().setCountry("Test Country");
        when(serviceRegistrarClient.getServiceHostURL(CONFIGURATION_SERVICE_NAME)).thenReturn(CONFIGURATION_SERVICE_NAME);
        when(siteClient.getSiteRegionCountryByCustomerRegionCountry(CONFIGURATION_SERVICE_NAME, customerAccount, instance.getSelectedFilters().getRegion(), instance.getSelectedFilters().getCountry())).thenReturn(list);
        List<SiteVO> expectedList = list;
        assertNotEquals(expectedList, instance.getSiteVOList());
        System.out.println("test41_GetSiteVOList completed");
    }

    /**
     * Test of getAreaVOList method, of class ConfigurationBean.
     */
    @Test
    public void test42_GetAreaVOList() {
        System.out.println("test42_GetAreaVOList");
        List<AreaVO> areaVOList = new ArrayList();
        AreaVO areaVO = new AreaVO();
        areaVO.setAreaId(UUID.randomUUID());
        areaVO.setAreaName("Test Area Name");
        areaVOList.add(areaVO);
        Whitebox.setInternalState(instance, "areaVOList", areaVOList);
        assertEquals(areaVOList, instance.getAreaVOList());
        System.out.println("test42_GetAreaVOList completed");
    }

    /**
     * Test of getAreaVOList method, of class ConfigurationBean.
     */
    @Test
    public void test43_GetAreaVOList() {
        System.out.println("test43_GetAreaVOList");
        List<AreaVO> areaVOList = new ArrayList();
        AreaVO areaVO = new AreaVO();
        areaVO.setAreaId(UUID.randomUUID());
        areaVO.setAreaName("Test Area Name");
        areaVOList.add(areaVO);
        Whitebox.setInternalState(instance, "areaVOList", areaVOList);
        assertNotEquals(new ArrayList(), instance.getAreaVOList());
        System.out.println("test43_GetAreaVOList completed");
    }

    /**
     * Test of getAssetVOList method, of class ConfigurationBean.
     */
    @Test
    public void test44_GetAssetVOList() {
        System.out.println("test44_GetAssetVOList");
        List<AssetVO> assetVOList = new ArrayList();
        AssetVO assetVO = new AssetVO();
        assetVO.setAssetId(UUID.randomUUID());
        assetVO.setAssetName("Test AssetName");
        assetVOList.add(assetVO);
        Whitebox.setInternalState(instance, "assetVOList", assetVOList);
        List<AssetVO> expectedResult = assetVOList;
        List<AssetVO> result = instance.getAssetVOList();
        assertEquals(expectedResult, result);
        System.out.println("test44_GetAssetVOList completed");
    }

    /**
     * Test of getAssetVOList method, of class ConfigurationBean.
     */
    @Test
    public void test45_GetAssetVOList() {
        System.out.println("test45_GetAssetVOList");
        List<AssetVO> assetVOList = new ArrayList();
        AssetVO assetVO = new AssetVO();
        assetVO.setAssetId(UUID.randomUUID());
        assetVO.setAssetName("Test AssetName");
        assetVOList.add(assetVO);
        Whitebox.setInternalState(instance, "assetVOList", assetVOList);
        List<AssetVO> expectedResult = new ArrayList();
        List<AssetVO> result = instance.getAssetVOList();
        assertNotEquals(expectedResult, result);
        System.out.println("test45_GetAssetVOList completed");
    }

    /**
     * Test of getPointLocationVOList method, of class ConfigurationBean.
     */
    @Test
    public void test46_GetPointLocationVOList() {
        System.out.println("test46_GetPointLocationVOList");
        List<PointLocationVO> pointLocationVOList = new ArrayList();
        PointLocationVO pointLocationVO = new PointLocationVO();
        pointLocationVO.setPointLocationId(UUID.randomUUID());
        pointLocationVO.setPointLocationName("Test PointLocation Name");
        pointLocationVOList.add(pointLocationVO);
        Whitebox.setInternalState(instance, "pointLocationVOList", pointLocationVOList);
        List<PointLocationVO> expectedResult = pointLocationVOList;
        List<PointLocationVO> result = instance.getPointLocationVOList();
        assertEquals(expectedResult, result);
        System.out.println("test46_GetPointLocationVOList completed");
    }

    /**
     * Test of getPointLocationVOList method, of class ConfigurationBean.
     */
    @Test
    public void test47_GetPointLocationVOList() {
        System.out.println("test47_GetPointLocationVOList");
        List<PointLocationVO> pointLocationVOList = new ArrayList();
        PointLocationVO pointLocationVO = new PointLocationVO();
        pointLocationVO.setPointLocationId(UUID.randomUUID());
        pointLocationVO.setPointLocationName("Test PointLocation Name");
        pointLocationVOList.add(pointLocationVO);
        Whitebox.setInternalState(instance, "pointLocationVOList", pointLocationVOList);
        List<PointLocationVO> expectedResult = new ArrayList();
        List<PointLocationVO> result = instance.getPointLocationVOList();
        assertNotEquals(expectedResult, result);
        System.out.println("test47_GetPointLocationVOList completed");
    }

    /**
     * Test of getPointVOList method, of class ConfigurationBean.
     */
    @Test
    public void test48_GetPointVOList() {
        System.out.println("test48_GetPointVOList");
        List<PointVO> pointVOList = new ArrayList();
        PointVO pointVO = new PointVO();
        pointVO.setPointId(UUID.randomUUID());
        pointVO.setPointName("Test Point Name");
        pointVOList.add(pointVO);
        Whitebox.setInternalState(instance, "pointVOList", pointVOList);
        assertEquals(pointVOList, instance.getPointVOList());
        System.out.println("test48_GetPointVOList completed");
    }

    /**
     * Test of getPointVOList method, of class ConfigurationBean.
     */
    @Test
    public void test49_GetPointVOList() {
        System.out.println("test49_GetPointVOList");
        List<PointVO> pointVOList = new ArrayList();
        PointVO pointVO = new PointVO();
        pointVO.setPointId(UUID.randomUUID());
        pointVO.setPointName("Test Point Name");
        pointVOList.add(pointVO);
        Whitebox.setInternalState(instance, "pointVOList", pointVOList);
        assertNotEquals(new ArrayList(), instance.getPointVOList());
        System.out.println("test49_GetPointVOList completed");
    }

    /**
     * Test of isRenderCreateBtn method, of class ConfigurationBean.
     */
    @Test
    public void test50_IsRenderCreateBtn() {
        System.out.println("test50_IsRenderCreateBtn");
        instance.getSelectedFilters().setPoint(null);
        instance.updateUI("content");
        assertTrue(instance.isRenderCreateBtn());
        System.out.println("test50_IsRenderCreateBtn completed");
    }

    /**
     * Test of isRenderCreateBtn method, of class ConfigurationBean.
     */
    @Test
    public void test51_IsRenderCreateBtn() {
        System.out.println("test51_IsRenderCreateBtn");
        instance.getSelectedFilters().setPoint(null);
        instance.updateUI("content");
        boolean expectedResult = instance.isRenderCreateBtn() == false;
        assertFalse(expectedResult);
        System.out.println("test51_IsRenderCreateBtn completed");
    }

    /**
     * Test of getCreateBtnValue method, of class ConfigurationBean.
     */
    @Test
    public void test52_GetCreateBtnValue() {
        System.out.println("test52_GetCreateBtnValue");
        when(userManageBean.getResourceBundleString("dialog_createPoint_header")).thenReturn("Point");
        instance.getSelectedFilters().setPoint(null);
        String expectedResult = "Point";
        instance.updateUI("content");
        assertEquals(instance.getCreateBtnValue(), expectedResult);
        System.out.println("test52_GetCreateBtnValue completed");
    }

    /**
     * Test of getCreateBtnValue method, of class ConfigurationBean.
     */
    @Test
    public void test53_GetCreateBtnValue() {
        System.out.println("test53_GetCreateBtnValue");
        when(userManageBean.getResourceBundleString("dialog_createPoint_header")).thenReturn("Point");
        instance.getSelectedFilters().setPoint(null);
        String expectedResult = "";
        instance.updateUI("content");
        assertNotEquals(instance.getCreateBtnValue(), expectedResult);
        System.out.println("test53_GetCreateBtnValue completed");
    }

    /**
     * Test of getSelectedAsset method, of class ConfigurationBean.
     */
    @Test
    public void test54_GetSelectedAsset() {
        System.out.println("test54_GetSelectedAsset");
        AssetVO selectedAsset = new AssetVO();
        selectedAsset.setAssetId(UUID.randomUUID());
        selectedAsset.setAssetName("Test Asset Name");
        Whitebox.setInternalState(instance, "selectedAsset", selectedAsset);
        assertEquals(selectedAsset, instance.getSelectedAsset());
        System.out.println("test54_GetSelectedAsset completed");
    }

    /**
     * Test of getSelectedAsset method, of class ConfigurationBean.
     */
    @Test
    public void test55_GetSelectedAsset() {
        System.out.println("test55_GetSelectedAsset");
        AssetVO selectedAsset = new AssetVO();
        selectedAsset.setAssetId(UUID.randomUUID());
        selectedAsset.setAssetName("Test Asset Name");
        Whitebox.setInternalState(instance, "selectedAsset", selectedAsset);
        assertNotEquals(null, instance.getSelectedAsset());
        System.out.println("test55_GetSelectedAsset completed");
    }

    /**
     * Test of getSelectedPointLocation method, of class ConfigurationBean.
     */
    @Test
    public void test56_GetSelectedPointLocation() {
        System.out.println("test56_GetSelectedPointLocation");
        PointLocationVO selectedPointLocation = new PointLocationVO();
        selectedPointLocation.setPointLocationId(UUID.randomUUID());
        selectedPointLocation.setPointLocationName("Test PointLocation Name");
        Whitebox.setInternalState(instance, "selectedPointLocation", selectedPointLocation);
        assertEquals(selectedPointLocation, instance.getSelectedPointLocation());
        System.out.println("test56_GetSelectedPointLocation completed");
    }

    /**
     * Test of getSelectedPointLocation method, of class ConfigurationBean.
     */
    @Test
    public void test57_GetSelectedPointLocation() {
        System.out.println("test57_GetSelectedPointLocation");
        PointLocationVO selectedPointLocation = new PointLocationVO();
        selectedPointLocation.setPointLocationId(UUID.randomUUID());
        selectedPointLocation.setPointLocationName("Test PointLocation Name");
        Whitebox.setInternalState(instance, "selectedPointLocation", selectedPointLocation);
        assertNotEquals(null, instance.getSelectedPointLocation());
        System.out.println("test57_GetSelectedPointLocation completed");
    }

    @Test
    public void test60_UpdateAlarmStatus() {
        System.out.println("test_UpdateAlarmStatus");
        AssetVO assetVO = new AssetVO();
        assetVO.setCustomerAccount("77777");
        assetVO.setSiteName("Test Site 1");
        assetVO.setSiteId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        assetVO.setAreaName("Test Area 1");
        assetVO.setAreaId(UUID.fromString("57e923d1-15ed-4993-8068-72a01f91a6d9"));
        assetVO.setAssetId(UUID.fromString("e5f2a849-e5cc-49c2-ac51-1c74866e42f9"));
        assetVO.setAssetType("Test Asset Type 1");
        assetVO.setAssetTypeId(UUID.fromString("4e2e46f7-e7e7-42be-bca3-205251863c65"));
        assetVO.setAssetName("Test Asset Name 1");
        assetVO.setDescription("Test Site 1 Desc");
        assetVO.setSampleInterval(5);
        Whitebox.setInternalState(instance, "selectedAsset", assetVO);
        instance.setAlarmEnabled(true);

        when(assetClient.updateAsset(serviceRegistrarClient.getServiceHostURL(CONFIGURATION_SERVICE_NAME), assetVO)).thenReturn(true);
        instance.setSensorTypeDefaultLabel("All (Default)");
        instance.updateAlarmStatus();
        assertTrue(!instance.isAlarmEnabled());
    }

    @Test
    public void test61_UpdateAlarmStatus() {
        System.out.println("test_UpdateAlarmStatus");

        List<PointVO> pointVOList = new ArrayList();
        PointVO pointVO = new PointVO();
        pointVO.setPointId(UUID.randomUUID());
        pointVO.setApSetId(UUID.randomUUID());
        pointVO.setAlSetId(UUID.randomUUID());
        pointVO.setPointName("Test Point Name11");
        pointVO.setSensorType("Acceleration");
        pointVOList.add(pointVO);

        List<PointLocationVO> pointLocationVoList = new ArrayList();
        PointLocationVO pointLocation = new PointLocationVO();
        instance.setSelectedFilters();
        instance.getSelectedFilters().setCountry("Country 1");
        instance.getSelectedFilters().setRegion("Region 1");
        instance.getSelectedFilters().setSite(new SelectItem(UUID.randomUUID(), "site"));
        instance.getSelectedFilters().setArea(new SelectItem(UUID.randomUUID(), "area"));
        instance.getSelectedFilters().setAsset(new SelectItem(UUID.randomUUID(), "asset"));
        instance.getSelectedFilters().setPointLocation(new SelectItem(UUID.randomUUID(), "ptlo"));
        instance.getSelectedFilters().setPoint(new SelectItem(UUID.randomUUID(), "point"));
        Filters selectedFilters = instance.getSelectedFilters();

        pointLocation.setCustomerAccount(customerAccount);
        pointLocation.setSiteId((UUID) selectedFilters.getSite().getValue());
        pointLocation.setAreaId((UUID) selectedFilters.getArea().getValue());
        pointLocation.setAssetId((UUID) selectedFilters.getAsset().getValue());
        pointLocation.setPointLocationId((UUID) selectedFilters.getPointLocation().getValue());
        pointLocation.setPointList(pointVOList);
        pointLocationVoList.add(pointLocation);
        Whitebox.setInternalState(instance, "selectedPointLocation", pointLocation);
        instance.setAlarmEnabled(true);

        when(pointLocationClient.updatePointLocation(serviceRegistrarClient.getServiceHostURL(CONFIGURATION_SERVICE_NAME), pointLocation)).thenReturn(true);
        instance.setSensorTypeDefaultLabel("All (Default)");
        instance.updateAlarmStatus();
        assertTrue(!instance.isAlarmEnabled());
    }

    @Test
    public void test70_LaunchModifyAlarmStatusOverlay() {
        System.out.println("test_LaunchModifyAlarmStatusOverlay");
        instance.setSensorTypeDefaultLabel("Testing sensor default type");
        instance.launchModifyAlarmStatusOverlay();
        assertEquals(userManageBean.getResourceBundleString("label_sensor_type_default"), instance.getSensorTypeDefaultLabel());
    }
}
