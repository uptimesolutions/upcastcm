/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.FaultFrequenciesClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import static com.uptime.upcastcm.utils.enums.ParamUnitEnum.getFrequencyPlusUnits;
import com.uptime.upcastcm.utils.vo.FaultFrequenciesVO;
import com.uptime.upcastcm.utils.vo.UserSitesVO;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import static org.powermock.api.mockito.PowerMockito.when;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.primefaces.PrimeFaces;

/**
 *
 * @author Joseph
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(PowerMockRunner.class)
@PowerMockIgnore("jdk.internal.reflect.*")
@PrepareForTest({FacesContext.class, ExternalContext.class, ServiceRegistrarClient.class, UserManageBean.class, FaultFrequenciesClient.class, PresetsBean.class, PrimeFaces.class, PrimeFaces.Ajax.class})
public class CreateFaultFrequenciesBeanTest {

    @Mock
    ServiceRegistrarClient serviceRegistrarClient;

    @Mock
    FaultFrequenciesClient faultFrequenciesClient;

    @Mock
    FacesContext facesContext;

    @Mock
    ExternalContext externalContext;

    @Mock
    HttpServletRequest request;
    
    @Mock
    HttpSession session;
    
    @Mock
    UserManageBean userManageBean;

    @Mock
    PresetsBean presetsBean;
    
    @Mock
    PrimeFaces pf;
    
    @Mock
    PrimeFaces.Ajax ax;
    
    private final CreateFaultFrequenciesBean instance;
    Map<String, Object> sessionMap;
    
    public CreateFaultFrequenciesBeanTest() {
        List<FaultFrequenciesVO> ffList;
        FaultFrequenciesVO ffVO;
        UserSitesVO uvo;
        
        PowerMockito.mockStatic(FacesContext.class);
        PowerMockito.mockStatic(FaultFrequenciesClient.class);
        PowerMockito.mockStatic(ServiceRegistrarClient.class);
        PowerMockito.mockStatic(PrimeFaces.class);
        PowerMockito.mockStatic(PrimeFaces.Ajax.class);
        pf = PowerMockito.mock(PrimeFaces.class);
        ax = PowerMockito.mock(PrimeFaces.Ajax.class);
        
        facesContext = PowerMockito.mock(FacesContext.class);
        externalContext = PowerMockito.mock(ExternalContext.class);
        request = PowerMockito.mock(HttpServletRequest.class);
        session = PowerMockito.mock(HttpSession.class);
        userManageBean = PowerMockito.mock(UserManageBean.class);
        presetsBean = PowerMockito.mock(PresetsBean.class);
        faultFrequenciesClient = PowerMockito.mock(FaultFrequenciesClient.class);
        serviceRegistrarClient = PowerMockito.mock(ServiceRegistrarClient.class);
        
        sessionMap = new HashMap();
        sessionMap.put("userManageBean", userManageBean);
        sessionMap.put("presetsBean", presetsBean);
        session.setAttribute("userManageBean", userManageBean);
        session.setAttribute("presetsBean", presetsBean);
        
        ffVO = new FaultFrequenciesVO();
        ffVO.setFfName("Test FFName");
        ffVO.setFfUnit("Test FFUnit");
        ffVO.setFfStartHarmonic(20);
        ffVO.setFfEndHarmonic(60);
        
        ffList = new ArrayList();
        ffList.add(ffVO);
        
        uvo = new UserSitesVO();
        uvo.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        uvo.setSiteName("Dummy Sites");
        uvo.setSiteRole("Admin");
                
        when(FacesContext.getCurrentInstance()).thenReturn(facesContext);
        when(FaultFrequenciesClient.getInstance()).thenReturn(faultFrequenciesClient);
        when(ServiceRegistrarClient.getInstance()).thenReturn(serviceRegistrarClient);
        when(facesContext.getExternalContext()).thenReturn(externalContext);
        when(externalContext.getSessionMap()).thenReturn(sessionMap);
        when(externalContext.getRequest()).thenReturn(request);
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("userManageBean")).thenReturn(userManageBean);
        when(session.getAttribute("presetsBean")).thenReturn(presetsBean);
        when(faultFrequenciesClient.createSiteFaultFrequencies(serviceRegistrarClient.getServiceHostURL(PRESET_SERVICE_NAME), ffList)).thenReturn(Boolean.TRUE);
        when(faultFrequenciesClient.createGlobalFaultFrequencies(serviceRegistrarClient.getServiceHostURL(PRESET_SERVICE_NAME), ffList)).thenReturn(Boolean.TRUE);
        when(userManageBean.getCurrentSite()).thenReturn(uvo);
        UserSitesVO usvo = new UserSitesVO();
        usvo.setCustomerAccount("77777");
        when(userManageBean.getCurrentSite()).thenReturn(usvo);
        
        instance = new CreateFaultFrequenciesBean();
        instance.setFrequenciesVOList(ffList);
    }

    /**
     * Test of init method, of class CreateFaultFrequenciesBean.
     */
    @Test
    public void testInit() {
        System.out.println("init");
        instance.setSetName("TEST SET");
        instance.init();
        assertEquals(instance.getSetName(), null);
    }

    /**
     * Test of resetPage method, of class CreateFaultFrequenciesBean.
     */
    @Test
    public void testResetPage() {
        System.out.println("resetPage");
        instance.setSetName("TEST SET");
        instance.resetPage();
        assertEquals(instance.getSetName(), null);
    }

    /**
     * Test of addFaultFrequencies method, of class CreateFaultFrequenciesBean.
     */
    @Test
    public void testAddFaultFrequencies() {
        System.out.println("addFaultFrequencies");
        int startCount = instance.getFrequenciesVOList().size();
        instance.addFaultFrequencies();
        assertEquals(startCount +1, instance.getFrequenciesVOList().size());
        System.out.println("addFaultFrequencies completed");
    }

    /**
     * Test of deleteFaultFrequencies method, of class CreateFaultFrequenciesBean.
     */
    @Test
    public void testdeleteFaultFrequencies() {
        System.out.println("deleteFaultFrequencies");
        FaultFrequenciesVO faultFrequenciesVO = new FaultFrequenciesVO();
        faultFrequenciesVO.setCustomerAccount("77777");
        faultFrequenciesVO.setSiteId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        faultFrequenciesVO.setSiteName("Test Site 1");
        faultFrequenciesVO.setFfName("Test FF Name");
        faultFrequenciesVO.setFfUnit("U/s");
        faultFrequenciesVO.setFfSetName("Test Set Name 1");
        faultFrequenciesVO.setFfSetDesc("Test Description 1");
        faultFrequenciesVO.setFfType("Site");
        faultFrequenciesVO.setFfStartHarmonic(2);
        faultFrequenciesVO.setFfEndHarmonic(4);
        
        FaultFrequenciesVO faultFrequenciesVO1 = new FaultFrequenciesVO();
        faultFrequenciesVO1.setCustomerAccount("77777");
        faultFrequenciesVO1.setSiteId(UUID.fromString("e3c66964-5a8c-451d-ae2e-9d04d83c8241"));
        faultFrequenciesVO1.setSiteName("Test Site 2");
        faultFrequenciesVO1.setFfName("Test FF Name 2");
        faultFrequenciesVO1.setFfUnit("U/s");
        faultFrequenciesVO1.setFfSetName("Test Set Name 2");
        faultFrequenciesVO1.setFfSetDesc("Test Description 2");
        faultFrequenciesVO1.setFfType("Site 2");
        faultFrequenciesVO1.setFfStartHarmonic(2);
        faultFrequenciesVO1.setFfEndHarmonic(4);

        List<FaultFrequenciesVO> faultFrequenciesVOs = new ArrayList<>();
        faultFrequenciesVOs.add(faultFrequenciesVO);
        faultFrequenciesVOs.add(faultFrequenciesVO1);
        instance.setFrequenciesVOList(faultFrequenciesVOs);
        int startCount = instance.getFrequenciesVOList().size();
        
        System.out.println("deleteFaultFrequencies startCount " + startCount);
        instance.deleteFaultFrequencies(faultFrequenciesVO);
        System.out.println("deleteFaultFrequencies getFrequenciesVOList().size() " + instance.getFrequenciesVOList().size());
        assertEquals(startCount - 1, instance.getFrequenciesVOList().size());
        System.out.println("deleteFaultFrequencies completed");
    }

    /**
     * Test of submit method, of class CreateFaultFrequenciesBean.
     */
    @Test
    public void testSubmit1_pass() {
        System.out.println("submit pass");
        instance.setSetName("Test SetName");
        when(PrimeFaces.current()).thenReturn(pf);
        when(pf.ajax()).thenReturn(ax);
        ax.addCallbackParam("presetsTabViewId", new Object());
        instance.submit();
        System.out.println("submit pass completed");
    }

        /**
     * Test of submit method, of class CreateFaultFrequenciesBean.
     */
    @Test
    public void testSubmit2_pass() {
        System.out.println("submit pass 2");
        instance.setSetName("Test SetName");
        when(PrimeFaces.current()).thenReturn(pf);
        when(pf.ajax()).thenReturn(ax);
        ax.addCallbackParam("presetsTabViewId", new Object());
        instance.submit();
        System.out.println("submit pass completed");
    }
    
    /**
     * Test of submit method, of class CreateFaultFrequenciesBean.
     */
    @Test
    public void testSubmit1_fail() {
        System.out.println("submit");
        instance.submit();
        System.out.println("submit completed");
    }

    /**
     * Test of submit method, of class CreateFaultFrequenciesBean.
     */
    @Test
    public void testSubmit2_fail() {
        System.out.println("submit pass 2");
        instance.setSetName("Test SetName");
        when(PrimeFaces.current()).thenReturn(pf);
        when(pf.ajax()).thenReturn(ax);
        ax.addCallbackParam("presetsTabViewId", new Object());
        instance.getFrequenciesVOList().get(0).setFfEndHarmonic(10);
        instance.submit();
        System.out.println("submit pass completed");
    }
    
    /**
     * Test of getFrequenciesVOList method, of class CreateFaultFrequenciesBean.
     */
    @Test
    public void testGetFrequenciesVOList() {
        System.out.println("getFrequenciesVOList");
        FaultFrequenciesVO ffVO = new FaultFrequenciesVO();
        ffVO.setFfName("Test FFName");
        List<FaultFrequenciesVO> ffList = new ArrayList();
        ffList.add(ffVO);
        instance.setFrequenciesVOList(ffList);
        List<FaultFrequenciesVO> expResult = new ArrayList();
        expResult.add(ffVO);
        List<FaultFrequenciesVO> result = instance.getFrequenciesVOList();
        assertEquals(expResult.get(0).getFfName(), result.get(0).getFfName());
    }

    /**
     * Test of getFrequencyUnitsList method, of class CreateFaultFrequenciesBean.
     */
    @Test
    public void testGetFrequencyUnitsList() {
        System.out.println("getFrequencyUnitsList");
        assertEquals(instance.getFrequencyUnitsList().size(), getFrequencyPlusUnits().size());
        System.out.println("getFrequencyUnitsList done");
    }
}
