/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import java.util.HashMap;
import java.util.Map;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import static org.powermock.api.mockito.PowerMockito.when;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.primefaces.PrimeFaces;
import org.primefaces.event.NodeCollapseEvent;
import org.primefaces.event.NodeExpandEvent;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.menu.DefaultMenuItem;

/**
 *
 * @author gsingh
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(PowerMockRunner.class)
@PowerMockIgnore("jdk.internal.reflect.*")
@PrepareForTest({FacesContext.class, ExternalContext.class, UserManageBean.class, ServiceRegistrarClient.class, NavigationBean.class, CreateAreaBean.class, AlarmsBean.class, PrimeFaces.class, PrimeFaces.Ajax.class})
public class NavigationBeanTest {

    @Mock
    ServiceRegistrarClient serviceRegistrarClient;
    @Mock
    FacesContext facesContext;
    @Mock
    ExternalContext externalContext;
    @Mock
    HttpServletRequest request;
    @Mock
    HttpSession session;
    @Mock
    UserManageBean userManageBean;
    @Mock
    CreateAreaBean createAreaBean;
    @Mock
    AlarmsBean alarmsBean;

    private final NavigationBean instance;
    Map<String, Object> sessionMap;

    public NavigationBeanTest() {
        PowerMockito.mockStatic(FacesContext.class);
        PowerMockito.mockStatic(ServiceRegistrarClient.class);

        facesContext = PowerMockito.mock(FacesContext.class);
        externalContext = PowerMockito.mock(ExternalContext.class);
        request = PowerMockito.mock(HttpServletRequest.class);
        session = PowerMockito.mock(HttpSession.class);
        userManageBean = PowerMockito.mock(UserManageBean.class);
        createAreaBean = PowerMockito.mock(CreateAreaBean.class);
        alarmsBean = PowerMockito.mock(AlarmsBean.class);
        serviceRegistrarClient = PowerMockito.mock(ServiceRegistrarClient.class);

        sessionMap = new HashMap();
        sessionMap.put("userManageBean", userManageBean);
        sessionMap.put("createAreaBean", createAreaBean);
        sessionMap.put("alarmsBean", alarmsBean);
        session.setAttribute("userManageBean", userManageBean);

        when(ServiceRegistrarClient.getInstance()).thenReturn(serviceRegistrarClient);
        when(FacesContext.getCurrentInstance()).thenReturn(facesContext);
        when(facesContext.getExternalContext()).thenReturn(externalContext);
        when(externalContext.getSessionMap()).thenReturn(sessionMap);
        when(facesContext.getExternalContext().getRequest()).thenReturn(request);
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("userManageBean")).thenReturn(userManageBean);

        PowerMockito.mockStatic(PrimeFaces.class);
        PowerMockito.mockStatic(PrimeFaces.Ajax.class);
        PrimeFaces pf = PowerMockito.mock(PrimeFaces.class);
        PrimeFaces.Ajax ax = PowerMockito.mock(PrimeFaces.Ajax.class);
        when(pf.current()).thenReturn(pf);
        when(pf.ajax()).thenReturn(ax);

        instance = new NavigationBean();
    }

    @Before
    public void setUp() {
    }

    /**
     * Test of changeMenuMode method, of class NavigationBean.
     */
    @Test
    public void test01ChangeMenuMode() {
        System.out.println("changeMenuMode");
        instance.changeMenuMode();
        assertEquals(instance.getMenuMode(), "layout-slim");
    }

    /**
     * Test of changeMenuMode method, of class NavigationBean.
     */
    @Test
    public void test02ChangeMenuMode() {
        System.out.println("changeMenuMode");
        Whitebox.setInternalState(instance, "menuMode", "layout-slim");
        instance.changeMenuMode();
        assertEquals(instance.getMenuMode(), "layout-static layout-static-active");
    }

    /**
     * Test of updatePage method, of class NavigationBean.
     */
    @Test
    public void test01UpdatePage() {
        System.out.println("updatePage");
        instance.updatePage("home");
        assertEquals(instance.getNavigationIcon(), "pi pi-fw pi-home");
        assertEquals(((DefaultMenuItem) instance.getBreadcrumbModel().getElements().get(0)).getIcon(), "pi pi-fw pi-home");
    }

    /**
     * Test of updatePage method, of class NavigationBean.
     */
    @Test
    public void test02UpdatePage() {
        System.out.println("updatePage");
        instance.updatePage(null);
        assertEquals(instance.getNavigationIcon(), "pi pi-fw pi-home");
        assertEquals(((DefaultMenuItem) instance.getBreadcrumbModel().getElements().get(0)).getIcon(), "pi pi-fw pi-home");
    }

    /**
     * Test of updateDialog method, of class NavigationBean.
     */
    @Test
    public void test01UpdateDialog() {
        System.out.println("updateDialog");
        instance.updateDialog("edit-area");
        assertEquals(instance.getDialogContent(), "/user/card/edit-area.xhtml");
    }

    /**
     * Test of updateDialog method, of class NavigationBean.
     */
    @Test
    public void test02UpdateDialog() {
        System.out.println("updateDialog");
        instance.updateDialog(null);
        assertEquals(instance.getDialogContent(), "/user/card/empty.xhtml");
    }

    /**
     * Test of updateSecondDialog method, of class NavigationBean.
     */
    @Test
    public void test02UpdateSecondDialog() {
        System.out.println("updateSecondDialog");
        instance.updateSecondDialog(null);
        assertEquals(instance.getSecondDialogContent(), "/user/card/empty.xhtml");
    }

    /**
     * Test of updateThirdDialog method, of class NavigationBean.
     */
    @Test
    public void test02UpdateThirdDialog() {
        System.out.println("updateThirdDialog");
        instance.updateThirdDialog(null);
        assertEquals(instance.getThirdDialogContent(), "/user/card/empty.xhtml");
    }

    /**
     * Test of closeDialog method, of class NavigationBean.
     */
    @Test
    public void testCloseDialog() {
        System.out.println("closeDialog");
        Whitebox.setInternalState(instance, "dialogCard", "create-area");
        instance.closeDialog();
    }

    /**
     * Test of closeSecondDialog method, of class NavigationBean.
     */
    @Test
    public void testCloseSecondDialog() {
        System.out.println("closeSecondDialog");
        instance.closeSecondDialog();
    }

    /**
     * Test of closeThirdDialog method, of class NavigationBean.
     */
    @Test
    public void testCloseThirdDialog() {
        System.out.println("closeThirdDialog");
        instance.closeThirdDialog();
    }

    /**
     * Test of onNodeExpand method, of class NavigationBean.
     */
    @Test
    public void testOnNodeExpand() {
        System.out.println("onNodeExpand");
        NodeExpandEvent nee = PowerMockito.mock(NodeExpandEvent.class);
        Whitebox.setInternalState(instance, "page", "alarms");
        instance.onNodeExpand(nee);
    }

    /**
     * Test of onNodeCollapse method, of class NavigationBean.
     */
    @Test
    public void testOnNodeCollapse() {
        System.out.println("onNodeCollapse");
        NodeCollapseEvent nce = PowerMockito.mock(NodeCollapseEvent.class);
        Whitebox.setInternalState(instance, "page", "alarms");
        instance.onNodeCollapse(nce);
    }

    /**
     * Test of onNodeSelect, of class NavigationBean.
     */
    @Test
    public void testOnNodeSelect() {
        System.out.println("onNodeSelect");
        NodeSelectEvent nse = PowerMockito.mock(NodeSelectEvent.class);
        Whitebox.setInternalState(instance, "page", "alarms");
        instance.onNodeSelect(nse);
    }

}
