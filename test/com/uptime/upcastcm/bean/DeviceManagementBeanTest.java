/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.dao.PrestoDAO;
import com.uptime.upcastcm.http.client.APALSetClient;
import com.uptime.upcastcm.http.client.ApALByPointClient;
import com.uptime.upcastcm.http.client.HwUnitToPointClient;
import com.uptime.upcastcm.http.client.ManageDeviceClient;
import com.uptime.upcastcm.http.client.PointLocationClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import com.uptime.upcastcm.utils.vo.ChannelVO;
import com.uptime.upcastcm.utils.vo.DeviceVO;
import com.uptime.upcastcm.utils.vo.HwUnitVO;
import com.uptime.upcastcm.utils.vo.PointLocationVO;
import com.uptime.upcastcm.utils.vo.UserSitesVO;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.junit.runners.MethodSorters;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import static org.powermock.api.mockito.PowerMockito.when;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;
import org.primefaces.PrimeFaces;

/**
 *
 * @author twilcox
 */
@RunWith(PowerMockRunner.class)
@PowerMockIgnore({"jdk.internal.reflect.*", "javax.net.ssl.*"})
@PrepareForTest({FacesContext.class, ExternalContext.class, NavigationBean.class, UserManageBean.class,PrestoDAO.class, PrimeFaces.class, PrimeFaces.Ajax.class, ServiceRegistrarClient.class, APALSetClient.class, ApALByPointClient.class, HwUnitToPointClient.class, ManageDeviceClient.class, PointLocationClient.class})
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@PowerMockRunnerDelegate(JUnit4.class)
public class DeviceManagementBeanTest {
    
    
    @Mock
    ServiceRegistrarClient serviceRegistrarClient;
    
    @Mock
    APALSetClient aPALSetClient;
            
    @Mock
    ApALByPointClient apALByPointClient;
    
    @Mock
    HwUnitToPointClient hwUnitToPointClient; 
    
    @Mock
    ManageDeviceClient manageDeviceClient; 
    
    @Mock
    PointLocationClient pointLocationClient;
    
    @Mock
    FacesContext facesContext;
    
    @Mock
    ExternalContext externalContext;
    
    @Mock
    NavigationBean navigationBean;
    
    @Mock
    UserManageBean userManageBean;
    
    @Mock
    PrimeFaces pf;
    
    @Mock
    PrimeFaces.Ajax ax;
    
    @Mock
    PrestoDAO prestoDAO; 
    
    
    private final DeviceManagementBean instance;
    private Map<String,Object> sessionMap;
    private List<ChannelVO> channelVOList;
    private List<DeviceVO> deviceVOList;
    
    public DeviceManagementBeanTest() {
        UserSitesVO usvo;
        
        PowerMockito.mockStatic(ServiceRegistrarClient.class);
        PowerMockito.mockStatic(APALSetClient.class);
        PowerMockito.mockStatic(ApALByPointClient.class);
        PowerMockito.mockStatic(HwUnitToPointClient.class);
        PowerMockito.mockStatic(ManageDeviceClient.class);
        PowerMockito.mockStatic(PointLocationClient.class);
        PowerMockito.mockStatic(FacesContext.class);
        PowerMockito.mockStatic(ExternalContext.class);
        PowerMockito.mockStatic(NavigationBean.class);
        PowerMockito.mockStatic(UserManageBean.class);
        PowerMockito.mockStatic(PrimeFaces.class);
        PowerMockito.mockStatic(PrimeFaces.Ajax.class);
        PowerMockito.mockStatic(PrestoDAO.class);
        
        serviceRegistrarClient = PowerMockito.mock(ServiceRegistrarClient.class);
        aPALSetClient = PowerMockito.mock(APALSetClient.class);
        apALByPointClient = PowerMockito.mock(ApALByPointClient.class);
        hwUnitToPointClient = PowerMockito.mock(HwUnitToPointClient.class);
        manageDeviceClient = PowerMockito.mock(ManageDeviceClient.class);
        pointLocationClient = PowerMockito.mock(PointLocationClient.class);
        facesContext = PowerMockito.mock(FacesContext.class);
        externalContext = PowerMockito.mock(ExternalContext.class);
        navigationBean = PowerMockito.mock(NavigationBean.class);
        userManageBean = PowerMockito.mock(UserManageBean.class);
        pf = PowerMockito.mock(PrimeFaces.class);
        ax = PowerMockito.mock(PrimeFaces.Ajax.class);
        prestoDAO=PowerMockito.mock(PrestoDAO.class);
        
        when(PrimeFaces.current()).thenReturn(pf);
        when(pf.ajax()).thenReturn(ax);
        
        sessionMap = new HashMap();
        sessionMap.put("navigationBean", navigationBean);
        sessionMap.put("userManageBean", userManageBean);
        sessionMap.put("prestoDAO", prestoDAO);
        
        when(FacesContext.getCurrentInstance()).thenReturn(facesContext);
        when(facesContext.getExternalContext()).thenReturn(externalContext);
        when(externalContext.getSessionMap()).thenReturn(sessionMap);
        
        usvo = new UserSitesVO();
        usvo.setCustomerAccount("FEDEX EXPRESS");
        usvo.setSiteId(UUID.fromString("8b086e15-371f-4a8d-bedb-c1b67b49be3e"));
        when(userManageBean.getCurrentSite()).thenReturn(usvo);
        
        when(ServiceRegistrarClient.getInstance()).thenReturn(serviceRegistrarClient);
        when(serviceRegistrarClient.getServiceHostURL(CONFIGURATION_SERVICE_NAME)).thenReturn("HOST");
        when(serviceRegistrarClient.getServiceHostURL(PRESET_SERVICE_NAME)).thenReturn("HOST");
        when(APALSetClient.getInstance()).thenReturn(aPALSetClient);
        when(ApALByPointClient.getInstance()).thenReturn(apALByPointClient);
        when(HwUnitToPointClient.getInstance()).thenReturn(hwUnitToPointClient);
        when(ManageDeviceClient.getInstance()).thenReturn(manageDeviceClient);
        when(PointLocationClient.getInstance()).thenReturn(pointLocationClient);
        
        instance = new DeviceManagementBean();
    }
    
    @Before
    public void setUp() {
        ChannelVO vo;
        
        vo = new ChannelVO();
        vo.setCustomerAccount("FEDEX EXPRESS");
        vo.setDeviceId("00100000");
        
        channelVOList = new ArrayList();
        channelVOList.add(vo);
    }

    /**
     * Test of init method, of class DeviceManagementBean.
     */
    @Test
    public void test00_Init() {
        System.out.println("init");
        instance.init();
        System.out.println("init completed");
    }

    /**
     * Test of resetPage method, of class DeviceManagementBean.
     */
    @Test
    public void test01_ResetPage() {
        System.out.println("resetPage");
        instance.resetPage();
        System.out.println("resetPage completed");
    }

    /**
     * Test of onSerialNumberChange method, of class DeviceManagementBean.
     */
    @Test
    public void test01_OnSerialNumberChange() {
        System.out.println("onSerialNumberChange");
        instance.onSerialNumberChange();
        System.out.println("onSerialNumberChange completed");
    }

    /**
     * Test of search method, of class DeviceManagementBean.
     */
    @Test
    public void test02_Search() {
        System.out.println("search");
        
        List<HwUnitVO> hwUnitVOList;
        
        when(manageDeviceClient.getDeviceManagementRowsByCustomerSiteIdDevice("HOST", "FEDEX EXPRESS", UUID.fromString("8b086e15-371f-4a8d-bedb-c1b67b49be3e"), "00100000", false)).thenReturn(channelVOList);
        instance.setSerialNumber("00100000");
        
        instance.search();
        
        assertEquals(instance.getChannelVOList().size(), 1);
        assertEquals(instance.getChannelVOList().get(0).getCustomerAccount(), "FEDEX EXPRESS");
        assertEquals(instance.getChannelVOList().get(0).getDeviceId(), "00100000");
        assertNull(instance.getSerialNumberNotInUse());
        
        when(manageDeviceClient.getDeviceManagementRowsByCustomerSiteIdDevice("HOST", "FEDEX EXPRESS", UUID.fromString("8b086e15-371f-4a8d-bedb-c1b67b49be3e"), "00100000", false)).thenReturn(new ArrayList());
        when(hwUnitToPointClient.getHwUnitToPointByDevice("HOST", "00100000")).thenReturn(new ArrayList());
        
        instance.search();
        
        assertTrue(instance.getSerialNumberNotInUse());
        
        hwUnitVOList = new ArrayList();
        hwUnitVOList.add(new HwUnitVO());
        when(manageDeviceClient.getDeviceManagementRowsByCustomerSiteIdDevice("HOST", "FEDEX EXPRESS", UUID.fromString("8b086e15-371f-4a8d-bedb-c1b67b49be3e"), "00100000", false)).thenReturn(new ArrayList());
        when(hwUnitToPointClient.getHwUnitToPointByDevice("HOST", "00100000")).thenReturn(hwUnitVOList);
        
        instance.search();
        
        assertFalse(instance.getSerialNumberNotInUse());
        
        System.out.println("search completed");
    }

    /**
     * Test of viewInfo method, of class DeviceManagementBean.
     */
    @Test
    public void test03_ViewInfo() {
        System.out.println("viewInfo");
        ChannelVO channelVO = new ChannelVO();
        channelVO.setCustomerAccount("FEDEX EXPRESS");
        channelVO.setSiteId(UUID.fromString("8b086e15-371f-4a8d-bedb-c1b67b49be3e"));
        channelVO.setAreaId(UUID.fromString("8b086e15-371f-4a8d-bedb-c1b67b49be3e"));
        channelVO.setAssetId(UUID.fromString("8b086e15-371f-4a8d-bedb-c1b67b49be3e"));
        channelVO.setPointLocationId(UUID.fromString("8b086e15-371f-4a8d-bedb-c1b67b49be3e"));
        List<PointLocationVO> pointLocationList = new ArrayList();
        PointLocationVO pointLocationVO = new PointLocationVO();
        pointLocationVO.setSampleInterval((short)0);
        pointLocationList.add(pointLocationVO);
        when(pointLocationClient.getPointLocationsByPK("HOST", channelVO.getCustomerAccount(), channelVO.getSiteId(), channelVO.getAreaId(), channelVO.getAssetId(), channelVO.getPointLocationId())).thenReturn(pointLocationList);
        instance.viewInfo(channelVO);
        System.out.println("viewInfo completed");
    }

    /**
     * Test of installDevice method, of class DeviceManagementBean.
     */
    @Test
    public void test04_InstallDevice() {
        System.out.println("installDevice");
        instance.installDevice();
        System.out.println("installDevice completed");
    }

    /**
     * Test of enableDevice method, of class DeviceManagementBean.
     */
    @Test
    public void test05_EnableDevice() {
        System.out.println("enableDevice");
        instance.enableDevice();
        System.out.println("enableDevice completed");
    }

    /**
     * Test of disableDevice method, of class DeviceManagementBean.
     */
    @Test
    public void test06_DisableDevice() {
        System.out.println("disableDevice");
        instance.disableDevice();
        System.out.println("disableDevice completed");
    }

    /**
     * Test of modifyDevice method, of class DeviceManagementBean.
     */
    @Test
    public void test07_ModifyDevice() {
        System.out.println("modifyDevice");
        instance.modifyDevice();
        System.out.println("modifyDevice completed");
    }

    /**
     * Test of removeDevice method, of class DeviceManagementBean.
     */
    @Test
    public void test08_RemoveDevice() {
        System.out.println("removeDevice");
        instance.removeDevice();
        System.out.println("removeDevice completed");
    }

    /**
     * Test of replaceDevice method, of class DeviceManagementBean.
     */
    @Test
    public void test09_ReplaceDevice() {
        System.out.println("replaceDevice");
        instance.replaceDevice();
        System.out.println("replaceDevice completed");
    }

    /**
     * Test of isSupportedDevice method, of class DeviceManagementBean.
     */
    @Test
    public void test10_IsSupportedDevice() {
        System.out.println("isSupportedDevice");
        
        instance.setSerialNumber("00100000");
        assertTrue(instance.isSupportedDevice("modify"));
        
        instance.setSerialNumber("BB123456");
        assertFalse(instance.isSupportedDevice("test"));
        
        System.out.println("isSupportedDevice completed");
    }

    /**
     * Test of getHwDisabled method, of class DeviceManagementBean.
     */
    @Test
    public void test11_GetHwDisabled() {
        System.out.println("getHwDisabled");
        
        instance.setSerialNumber("00100000");
        
        when(manageDeviceClient.getDeviceManagementRowsByCustomerSiteIdDevice("HOST", "FEDEX EXPRESS", UUID.fromString("8b086e15-371f-4a8d-bedb-c1b67b49be3e"), "00100000", false)).thenReturn(channelVOList);
        instance.search();
        assertNull(instance.getHwDisabled());
        
        channelVOList.get(0).setHwDisabled(true);
        when(manageDeviceClient.getDeviceManagementRowsByCustomerSiteIdDevice("HOST", "FEDEX EXPRESS", UUID.fromString("8b086e15-371f-4a8d-bedb-c1b67b49be3e"), "00100000", false)).thenReturn(channelVOList);
        instance.search();
        assertTrue(instance.getHwDisabled());
        
        channelVOList.get(0).setHwDisabled(false);
        when(manageDeviceClient.getDeviceManagementRowsByCustomerSiteIdDevice("HOST", "FEDEX EXPRESS", UUID.fromString("8b086e15-371f-4a8d-bedb-c1b67b49be3e"), "00100000", false)).thenReturn(channelVOList);
        instance.search();
        assertFalse(instance.getHwDisabled());
        
        System.out.println("getHwDisabled completed");
    }

    /**
     * Test of getPointsInUseString method, of class DeviceManagementBean.
     */
    @Test
    public void test12_GetPointsInUseString() {
        System.out.println("getPointsInUseString");
        
        instance.setSerialNumber("00100000");
        
        when(userManageBean.getResourceBundleString("label_points_in_use_what_to_do")).thenReturn("points in use. What would you like to do?");
        when(manageDeviceClient.getDeviceManagementRowsByCustomerSiteIdDevice("HOST", "FEDEX EXPRESS", UUID.fromString("8b086e15-371f-4a8d-bedb-c1b67b49be3e"), "00100000", false)).thenReturn(channelVOList);
        instance.search();
        
        assertEquals("(1) points in use. What would you like to do?", instance.getPointsInUseString());
        
        System.out.println("getPointsInUseString completed");
    }

    /**
     * Test of getSerialNumber method, of class DeviceManagementBean.
     */
    @Test
    public void test13_GetSerialNumber() {
        System.out.println("getSerialNumber");
        
        instance.setSerialNumber("00100000");
        
        assertEquals("00100000", instance.getSerialNumber());
        
        System.out.println("getSerialNumber completed");
    }

    /**
     * Test of setSerialNumber method, of class DeviceManagementBean.
     */
    @Test
    public void test14_SetSerialNumber() {
        System.out.println("setSerialNumber");
        
        instance.setSerialNumber("00100000");
        
        assertEquals("00100000", instance.getSerialNumber());
        
        System.out.println("setSerialNumber completed");
    }

    /**
     * Test of getChannelVOList method, of class DeviceManagementBean.
     */
    @Test
    public void test15_GetChannelVOList() {
        System.out.println("getChannelVOList");
        
        instance.setSerialNumber("00100000");
        when(manageDeviceClient.getDeviceManagementRowsByCustomerSiteIdDevice("HOST", "FEDEX EXPRESS", UUID.fromString("8b086e15-371f-4a8d-bedb-c1b67b49be3e"), "00100000", false)).thenReturn(channelVOList);
        instance.search();
        
        assertEquals("FEDEX EXPRESS", instance.getChannelVOList().get(0).getCustomerAccount());
        
        System.out.println("getChannelVOList completed");
    }

    /**
     * Test of getSerialNumberNotInUse method, of class DeviceManagementBean.
     */
    @Test
    public void test16_GetSerialNumberNotInUse() {
        System.out.println("getSerialNumberNotInUse");
        
        List<HwUnitVO> hwUnitVOList;
        
        instance.setSerialNumber("00100000");
        
        when(manageDeviceClient.getDeviceManagementRowsByCustomerSiteIdDevice("HOST", "FEDEX EXPRESS", UUID.fromString("8b086e15-371f-4a8d-bedb-c1b67b49be3e"), "00100000", false)).thenReturn(new ArrayList());
        when(hwUnitToPointClient.getHwUnitToPointByDevice("HOST", "00100000")).thenReturn(new ArrayList());
        
        instance.search();
        
        assertTrue(instance.getSerialNumberNotInUse());
        
        hwUnitVOList = new ArrayList();
        hwUnitVOList.add(new HwUnitVO());
        when(manageDeviceClient.getDeviceManagementRowsByCustomerSiteIdDevice("HOST", "FEDEX EXPRESS", UUID.fromString("8b086e15-371f-4a8d-bedb-c1b67b49be3e"), "00100000", false)).thenReturn(new ArrayList());
        when(hwUnitToPointClient.getHwUnitToPointByDevice("HOST", "00100000")).thenReturn(hwUnitVOList);
        
        instance.search();
        
        assertFalse(instance.getSerialNumberNotInUse());
        
        System.out.println("getSerialNumberNotInUse completed");
    }
    
     /**
     * Test of GetdeviceLocationsVOList method, of class DeviceManagementBean.
     */
    @Test
    public void test17_SearchForDeviceLocations() {
        System.out.println("searchForDeviceLocations");
        instance.searchForDeviceLocations();
        System.out.println("searchForDeviceLocations completed");
    }
}
