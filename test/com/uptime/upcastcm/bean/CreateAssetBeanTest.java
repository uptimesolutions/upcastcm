/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.APALSetClient;
import com.uptime.upcastcm.http.client.AreaClient;
import com.uptime.upcastcm.http.client.AssetClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import static com.uptime.upcastcm.utils.enums.ACSensorTypeEnum.getACSensorTypeItemList;
import static com.uptime.upcastcm.utils.enums.DCSensorTypeEnum.getDCSensorTypeItemList;
import com.uptime.upcastcm.utils.helperclass.Filters;
import com.uptime.upcastcm.utils.helperclass.PresetUtil;
import com.uptime.upcastcm.utils.vo.ApAlSetVO;
import com.uptime.upcastcm.utils.vo.AreaVO;
import com.uptime.upcastcm.utils.vo.AssetVO;
import com.uptime.upcastcm.utils.vo.PointLocationVO;
import com.uptime.upcastcm.utils.vo.PointVO;
import com.uptime.upcastcm.utils.vo.UserPreferencesVO;
import com.uptime.upcastcm.utils.vo.UserSitesVO;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.junit.runners.MethodSorters;
import org.mockito.Mock;
import static org.mockito.Mockito.mock;
import org.powermock.api.mockito.PowerMockito;
import static org.powermock.api.mockito.PowerMockito.when;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;
import org.powermock.reflect.Whitebox;

/**
 *
 * @author kpati
 */
@PowerMockIgnore("jdk.internal.reflect.*")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnit4.class)
@PrepareForTest({FacesContext.class, ExternalContext.class, HttpServletRequest.class, HttpSession.class,UserManageBean.class, ConfigurationBean.class, AreaClient.class, ServiceRegistrarClient.class, PresetUtil.class, AssetClient.class, APALSetClient.class})
public class CreateAssetBeanTest {

    private final CreateAssetBean instance;
    private final List<UserSitesVO> userSiteList;

    @Mock
    ServiceRegistrarClient serviceRegistrarClient;

    @Mock
    PresetUtil presetUtil;

    @Mock
    AssetClient assetClient;

    @Mock
    AreaClient areaClient;
    @Mock
    APALSetClient aPALSetClient;

    @Mock
    FacesContext facesContext;

    @Mock
    ExternalContext externalContext;

    @Mock
    HttpServletRequest mockRequest;

    @Mock
    HttpServletRequest request;

    @Mock
    HttpSession session;

    @Mock
    UserManageBean userManageBean;

    @Mock
    ConfigurationBean configurationBean;

    Map<String, Object> sessionMap;
    private List<UserSitesVO> siteList;
    private List<String> assetIdList;
    String customerAccount, defaultsite;
    AssetVO assetVO;
    UserPreferencesVO userVO;
    private final Filters filters;

    public CreateAssetBeanTest() {

        PowerMockito.mockStatic(FacesContext.class);
        PowerMockito.mockStatic(ServiceRegistrarClient.class);
        PowerMockito.mockStatic(AreaClient.class);
        PowerMockito.mockStatic(AssetClient.class);
        facesContext = PowerMockito.mock(FacesContext.class);
        externalContext = PowerMockito.mock(ExternalContext.class);
        serviceRegistrarClient = PowerMockito.mock(ServiceRegistrarClient.class);
        areaClient = PowerMockito.mock(AreaClient.class);
        assetClient = PowerMockito.mock(AssetClient.class);

        userManageBean = PowerMockito.mock(UserManageBean.class);
        configurationBean = PowerMockito.mock(ConfigurationBean.class);
        presetUtil = PowerMockito.mock(PresetUtil.class);
        mockRequest = PowerMockito.mock(HttpServletRequest.class);
       
        when(FacesContext.getCurrentInstance()).thenReturn(facesContext);
        when(facesContext.getExternalContext()).thenReturn(externalContext);

        when(externalContext.getRequest()).thenReturn(mockRequest);
        sessionMap = new HashMap();
        sessionMap.put("userManageBean", userManageBean);
        sessionMap.put("configurationBean", configurationBean);
        when(externalContext.getSessionMap()).thenReturn(sessionMap);

        userSiteList = new ArrayList<>();
        UserSitesVO uvo = new UserSitesVO();
        uvo.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        uvo.setSiteName("Test Site 1");
        userSiteList.add(uvo);

        when(userManageBean.getUserSiteList()).thenReturn(userSiteList);

        userVO = new UserPreferencesVO();
        userVO.setCustomerAccount("77777");
        userVO.setDefaultSite(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        userVO.setLocaleDisplayName("English");
        userVO.setTimezone("US EST");
        session = PowerMockito.mock(HttpSession.class);
        //mockRequest.getSession(true);

        when(mockRequest.getSession()).thenReturn(session);
        when(session.getAttribute("userVO")).thenReturn(userVO);
        UserSitesVO usvo = new UserSitesVO();
        usvo.setCustomerAccount("77777");
        when(userManageBean.getCurrentSite()).thenReturn(usvo);
        when(userManageBean.getAccountUserPreferencesVO()).thenReturn(userVO);
        when(userManageBean.getPresetUtil()).thenReturn(presetUtil);

        filters = new Filters();
        filters.setSite(new SelectItem(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), "Test Site 1"));
        filters.setArea(new SelectItem(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"), "Test Area 1"));
        filters.setAsset(new SelectItem(UUID.fromString("f33fcf5a-5f93-4504-bc09-74a61a460bf0"), "Test Asset 1"));

                
        HttpSession session = PowerMockito.mock(HttpSession.class);
        mockRequest.getSession(true);
        when(mockRequest.getSession()).thenReturn(session);
        
        when(ServiceRegistrarClient.getInstance()).thenReturn(serviceRegistrarClient);
        when(AreaClient.getInstance()).thenReturn(areaClient);
        when(AssetClient.getInstance()).thenReturn(assetClient);
        
        System.out.println("init 111");
        System.out.println("init 22");

        assetVO = new AssetVO();
        assetVO.setCustomerAccount("77777");
        assetVO.setSiteName("Test Site 1");
        assetVO.setSiteId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        assetVO.setAreaName("Test Area 1");
        assetVO.setAreaId(UUID.fromString("57e923d1-15ed-4993-8068-72a01f91a6d9"));
        assetVO.setAssetId(UUID.fromString("e5f2a849-e5cc-49c2-ac51-1c74866e42f9"));
        assetVO.setAssetType("Test Asset Type 1");
        assetVO.setAssetTypeId(UUID.fromString("4e2e46f7-e7e7-42be-bca3-205251863c65"));
        assetVO.setAssetName("Test Asset Name 1");
        assetVO.setDescription("Test Site 1 Desc");
        assetVO.setSampleInterval(5);
        assetVO.setPointLocationList(new ArrayList<PointLocationVO>());

        siteList = new ArrayList<>();
        UserSitesVO userSiteVO = new UserSitesVO();
        userSiteVO.setSiteName("Test Site 1");
        userSiteVO.setSiteId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        siteList.add(userSiteVO);

        userSiteVO = new UserSitesVO();
        userSiteVO.setSiteName("Test Site 2");
        userSiteVO.setSiteId(UUID.fromString("4b2e46f7-e7e7-42be-bca3-205251863c65"));
        siteList.add(userSiteVO);

        assetIdList = new ArrayList<>();
        assetIdList.add("Asset Id 1");
        assetIdList.add("Asset Id 2");
        assetIdList.add("Asset Id 3");

        defaultsite = "Site 2";
        instance = new CreateAssetBean();
        instance.setAssetVO(assetVO);
    }

    @BeforeClass
    public static void setUpClass() {
        
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {

    }

    @After
    public void tearDown() {
    }

    /**
     * Test of init method, of class CreateAssetBean.
     */
    @Test
    public void test01_Init() {
        try {
            System.out.println("init");
            when(userManageBean.getUserSiteList()).thenReturn(siteList);
            instance.init();
            assertTrue(instance.getAssetVO().getAssetName() == null);
            System.out.println("init 333");

        } catch (Exception ex) {
            System.out.println("testInit Exception - " + ex.toString());
        }
    }
    
    /**
     * Test of init method, of class CreateAssetBean.
     */
    @Test
    public void test02_Init() {
        try {
            System.out.println("init Not Equals");
//            MockitoAnnotations.initMocks(instance);

            //whenNew(UserManageBean.class).withNoArguments().thenReturn(userManageBean);
            when(userManageBean.getUserSiteList()).thenReturn(siteList);
            instance.init();
            List<UserSitesVO> expectedResult = null;
            assertNotEquals(expectedResult, userManageBean.getUserSiteList().get(0));
            System.out.println("init Not Equals Done");

        } catch (Exception ex) {
            System.out.println("testInit Exception - " + ex.toString());
        }
    }

    /**
     * Test of resetPage method, of class CreateAssetBean.
     */
    @Test
    public void test03_ResetPage() {
        System.out.println("resetPage");
        when(userManageBean.getUserSiteList()).thenReturn(siteList);
        instance.resetPage();
        assertTrue(instance.getAssetVO().getAssetName() == null);
    }
    
    /**
     * Test of resetPage method, of class CreateAssetBean.
     */
    @Test
    public void test04_ResetPage() {
        System.out.println("resetPage Not Equals");
        when(userManageBean.getUserSiteList()).thenReturn(siteList);
        instance.resetPage();
        List<UserSitesVO> expectedResult = null;
        assertNotEquals(expectedResult, userManageBean.getUserSiteList().get(0));
        System.out.println("resetPage Not Equals Done");
    }

    /**
     * Test of presetFields method, of class CreateAssetBean.
     */
    @Test
    public void testPresetFields05_Filters_Object() {
        System.out.println("presetFields");
        filters.setArea(new SelectItem(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"), "Test Area 2"));
        Filters presets = filters;
        Object object = null;

        instance.presetFields(presets, object);
    }
    
     /**
     * Test of presetFields method, of class CreateAssetBean.
     */
    @Test
    public void testPresetFields06_Filters_Object() {
        System.out.println("presetFields Not Equals");
        filters.setArea(new SelectItem(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"), "Test Area 2"));
        Filters presets = filters;
        Object object = null;
        String expectedResult = "";
        instance.presetFields(presets, object);
        System.out.println("presetFields Not Equals Done");
    }

    /**
     * Test of onSiteSelect method, of class CreateAssetBean.
     */
    @Test
    public void test07_OnSiteSelect() {
        System.out.println("onSiteSelect");
        List<AreaVO> areaList = new ArrayList();
        AreaVO areaVO = new AreaVO();
        areaVO.setAreaId(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"));
        areaVO.setAreaName("Test Area 3");
        areaList.add(areaVO);

        when(areaClient.getAreaSiteByCustomerSite(serviceRegistrarClient.getServiceHostURL(CONFIGURATION_SERVICE_NAME), customerAccount, assetVO.getSiteId())).thenReturn(areaList);
        List<SelectItem> tachoMeterList = new ArrayList();
        tachoMeterList.add(new SelectItem(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"), "Test Tachometer 1"));
        Whitebox.setInternalState(instance, "tachometerList", tachoMeterList);
        instance.onSiteSelect();
        assertTrue(tachoMeterList.isEmpty());
    }
    
    /**
     * Test of onSiteSelect method, of class CreateAssetBean.
     */
    @Test
    public void test08_OnSiteSelect() {
        System.out.println("onSiteSelect False");
        List<AreaVO> areaList = new ArrayList();
        AreaVO areaVO = new AreaVO();
        areaVO.setAreaId(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"));
        areaVO.setAreaName("Test Area 3");
        areaList.add(areaVO);

        when(areaClient.getAreaSiteByCustomerSite(serviceRegistrarClient.getServiceHostURL(CONFIGURATION_SERVICE_NAME), customerAccount, assetVO.getSiteId())).thenReturn(areaList);
        List<SelectItem> tachoMeterList = new ArrayList();
        tachoMeterList.add(new SelectItem(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"), "Test Tachometer 1"));
        Whitebox.setInternalState(instance, "tachometerList", tachoMeterList);
        instance.onSiteSelect();
        assertFalse(tachoMeterList.size() > 0);
        System.out.println("onSiteSelect False Done");
    }

    /**
     * Test of onApSetNameSelect method, of class CreateAssetBean.
     */
    @Test
    public void test09_OnApSetNameSelect() {
        System.out.println("onApSetNameSelect");
        PointVO pointVO = new PointVO();
        pointVO.setApSetId(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"));

        List<ApAlSetVO> siteApAlSetsVOList = new ArrayList();
        ApAlSetVO apAlSetVO = new ApAlSetVO();
        apAlSetVO.setApSetId(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"));
        apAlSetVO.setApSetName("Test ApAlSetName 1");
        siteApAlSetsVOList.add(apAlSetVO);

        Whitebox.setInternalState(instance, "apAlSet", apAlSetVO);
        Whitebox.setInternalState(instance, "siteApAlSetsVOList", siteApAlSetsVOList);

        aPALSetClient = mock(APALSetClient.class);
        
        Whitebox.setInternalState(APALSetClient.class, "instance", aPALSetClient);
        when(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME)).thenReturn("test");
        when(aPALSetClient.getSiteApAlSetsByCustomerSiteIdApSetId("test", assetVO.getCustomerAccount(), assetVO.getSiteId(), pointVO.getApSetId())).thenReturn(siteApAlSetsVOList);

        instance.onApSetNameSelect(pointVO);

        assertEquals(pointVO.getApSetName(), apAlSetVO.getApSetName());
        System.out.println("onApSetNameSelect Done");
    }
    
    /**
     * Test of onApSetNameSelect method, of class CreateAssetBean.
     */
    @Test
    public void test10_OnApSetNameSelect() {
        System.out.println("onApSetNameSelect Not Equals");
        PointVO pointVO = new PointVO();
        pointVO.setApSetId(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"));

        List<ApAlSetVO> siteApAlSetsVOList = new ArrayList();
        ApAlSetVO apAlSetVO = new ApAlSetVO();
        apAlSetVO.setApSetId(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"));
        apAlSetVO.setApSetName("Test ApAlSetName 1");
        siteApAlSetsVOList.add(apAlSetVO);

        Whitebox.setInternalState(instance, "apAlSet", apAlSetVO);
        Whitebox.setInternalState(instance, "siteApAlSetsVOList", siteApAlSetsVOList);

        aPALSetClient = mock(APALSetClient.class);
        
        Whitebox.setInternalState(APALSetClient.class, "instance", aPALSetClient);
        when(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME)).thenReturn("test");
        when(aPALSetClient.getSiteApAlSetsByCustomerSiteIdApSetId("test", assetVO.getCustomerAccount(), assetVO.getSiteId(), pointVO.getApSetId())).thenReturn(siteApAlSetsVOList);

        instance.onApSetNameSelect(pointVO);

        assertNotEquals(null, apAlSetVO.getApSetName());
        System.out.println("onApSetNameSelect Not Equals Done");
    }

    /**
     * Test of onAlSetNameSelect method, of class CreateAssetBean.
     */
    @Test
    public void test11_OnAlSetNameSelect() {
        System.out.println("onAlSetNameSelect");
        PointVO pointVO = new PointVO();
        pointVO.setAlSetId(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"));

        List<ApAlSetVO> siteApAlSetsVOList = new ArrayList();
        ApAlSetVO apAlSetVO = new ApAlSetVO();
        apAlSetVO.setAlSetId(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"));
        apAlSetVO.setAlSetName("Test ALSetName 1");
        siteApAlSetsVOList.add(apAlSetVO);

        Whitebox.setInternalState(instance, "apAlSet", apAlSetVO);
        Whitebox.setInternalState(instance, "siteApAlSetsVOList", siteApAlSetsVOList);

        instance.onAlSetNameSelect(pointVO);

        assertEquals(pointVO.getAlSetName(), apAlSetVO.getAlSetName());
        System.out.println("onAlSetNameSelect Done");
    }

    /**
     * Test of onAddNewPointLocation method, of class CreateAssetBean.
     */
    @Test
    public void test12_OnAddNewPointLocation() {
        System.out.println("onAddNewPointLocation");
        PointLocationVO plvo = new PointLocationVO();
        plvo.setCustomerAccount(customerAccount);
        assetVO.getPointLocationList().add(plvo);
        Whitebox.setInternalState(instance, "assetVO", assetVO);
        instance.onAddNewPointLocation();
        assertEquals(2, assetVO.getPointLocationList().size());
        System.out.println("onAddNewPointLocation Done");
    }
    
    /**
     * Test of onAddNewPointLocation method, of class CreateAssetBean.
     */
    @Test
    public void test13_OnAddNewPointLocation() {
        System.out.println("onAddNewPointLocation Not Equaal");
        PointLocationVO plvo = new PointLocationVO();
        plvo.setCustomerAccount(customerAccount);
        assetVO.getPointLocationList().add(plvo);
        Whitebox.setInternalState(instance, "assetVO", assetVO);
        instance.onAddNewPointLocation();
        assertNotEquals(3, assetVO.getPointLocationList().size());
        System.out.println("onAddNewPointLocation Not Equaal Done");
    }

    /**
     * Test of ondeletePointLocation method, of class CreateAssetBean.
     */
    @Test
    public void test14_OndeletePointLocation() {
        System.out.println("ondeletePointLocation");
        PointLocationVO plvo = new PointLocationVO();
        plvo.setCustomerAccount(customerAccount);
        assetVO.getPointLocationList().add(plvo);
        Whitebox.setInternalState(instance, "assetVO", assetVO);
        instance.ondeletePointLocation(plvo);
        assertTrue(assetVO.getPointLocationList().isEmpty());
        System.out.println("ondeletePointLocation Done");
    }
    
    /**
     * Test of ondeletePointLocation method, of class CreateAssetBean.
     */
    @Test
    public void test15_OndeletePointLocation() {
        System.out.println("ondeletePointLocation Not equal");
        PointLocationVO plvo = new PointLocationVO();
        plvo.setCustomerAccount(customerAccount);
        assetVO.getPointLocationList().add(plvo);
        Whitebox.setInternalState(instance, "assetVO", assetVO);
        instance.ondeletePointLocation(plvo);
        int expectedResult = assetVO.getPointLocationList().size();
        assertNotEquals(1, expectedResult);
        System.out.println("ondeletePointLocation Not equal Done");
    }
    

    /**
     * Test of onAddNewPoint method, of class CreateAssetBean.
     */
    @Test
    public void test16_OnAddNewPoint() {
        System.out.println("onAddNewPoint");
        PointLocationVO plvo = new PointLocationVO();
        instance.onAddNewPoint(plvo);
        assertEquals(1, plvo.getPointList().size());
        System.out.println("ondeletePointLocation Done");
    }
    
    /**
     * Test of onAddNewPoint method, of class CreateAssetBean.
     */
    @Test
    public void test17_OnAddNewPoint() {
        System.out.println("onAddNewPoint Not Equals");
        PointLocationVO plvo = new PointLocationVO();
        instance.onAddNewPoint(plvo);
        assertNotEquals(2, plvo.getPointList().size());
        System.out.println("ondeletePointLocation Not Equals Done");
    }
    

    /**
     * Test of ondeletePoints method, of class CreateAssetBean.
     */
    @Test
    public void test18_OndeletePoints() {
        System.out.println("ondeletePoints");
        PointLocationVO plvo = new PointLocationVO();
        plvo.setCustomerAccount(customerAccount);
        PointVO pvo = new PointVO();
        List<PointVO> pointList = new ArrayList();
        pointList.add(pvo);
        plvo.setPointList(pointList);
        instance.ondeletePoints(plvo, pvo);
        assertTrue(plvo.getPointList().isEmpty());
        System.out.println("ondeletePointLocation Done");
    }
    
    
    /**
     * Test of ondeletePoints method, of class CreateAssetBean.
     */
    @Test
    public void test19_OndeletePoints() {
        System.out.println("ondeletePoints False");
        PointLocationVO plvo = new PointLocationVO();
        plvo.setCustomerAccount(customerAccount);
        PointVO pvo = new PointVO();
        List<PointVO> pointList = new ArrayList();
        pointList.add(pvo);
        plvo.setPointList(pointList);
        instance.ondeletePoints(plvo, pvo);
        assertFalse(plvo.getPointList().size() > 0);
        System.out.println("ondeletePointLocation False Done");
    }

    /**
     * Test of createAsset method, of class CreateAssetBean.
     */
    @Test
    public void test20_CreateAsset() {
        System.out.println("createAsset");
        Whitebox.setInternalState(instance, "assetVO", assetVO);
        when(assetClient.createAsset(serviceRegistrarClient.getServiceHostURL(CONFIGURATION_SERVICE_NAME), assetVO)).thenReturn(true);
        when(externalContext.getSessionMap()).thenReturn(sessionMap);
        instance.createAsset();
        assertEquals(instance.getAssetVO().getAssetId(), new AssetVO().getAssetId());
    }

    /**
     * Test of onSensorSelect method, of class CreateAssetBean.
     */
    @Test
    public void test21_OnSensorSelect() {
        System.out.println("onSensorSelect");
        PointVO pvo = new PointVO();
        pvo.setSensorType("Acceleration");
        PointLocationVO plvo = new PointLocationVO();
        List<SelectItem> customerApSetVOList = new ArrayList();
        customerApSetVOList.add(new SelectItem(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"), "Test Tachometer 1"));
        Whitebox.setInternalState(instance, "customerApSetVOList", customerApSetVOList);
        when(presetUtil.populateApSetSensorTypeList(userManageBean, assetVO.getCustomerAccount(), assetVO.getSiteId(), pvo.getSensorType())).thenReturn(customerApSetVOList);
        instance.onSensorSelect(pvo, plvo);
        assertEquals(customerApSetVOList, instance.getCustomerApSetVOList());
        System.out.println("onSensorSelect Done");
    }
    
    
    /**
     * Test of onSensorSelect method, of class CreateAssetBean.
     */
    @Test
    public void test22_OnSensorSelect() {
        System.out.println("onSensorSelect Not Equals");
        PointVO pvo = new PointVO();
        pvo.setSensorType("Acceleration");
        PointLocationVO plvo = new PointLocationVO();
        List<SelectItem> customerApSetVOList = new ArrayList();
        customerApSetVOList.add(new SelectItem(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"), "Test Tachometer 1"));
        Whitebox.setInternalState(instance, "customerApSetVOList", customerApSetVOList);
        when(presetUtil.populateApSetSensorTypeList(userManageBean, assetVO.getCustomerAccount(), assetVO.getSiteId(), pvo.getSensorType())).thenReturn(customerApSetVOList);
        String expectedResult = "DC";
        instance.onSensorSelect(pvo, plvo);
        assertNotEquals(expectedResult, pvo.getPointType());
        System.out.println("onSensorSelect Not Equals Done");
    }

    /**
     * Test of setAssetVO method, of class CreateAssetBean.
     */
    @Test
    public void test23_SetAssetVO() {
        System.out.println("setAssetVO");
        AssetVO assetVO = new AssetVO();
        assetVO.setAssetName("Test Asset Name");
        instance.setAssetVO(assetVO);
        String expResult = "Test Asset Name";
        String result = instance.getAssetVO().getAssetName();
        assertEquals(expResult, result);
        System.out.println("setAssetVO Done");
    }
    
    /**
     * Test of setAssetVO method, of class CreateAssetBean.
     */
    @Test
    public void test24_SetAssetVO() {
        System.out.println("setAssetVO Not Equals");
        AssetVO assetVO = new AssetVO();
        assetVO.setAssetName("Test Asset Name");
        instance.setAssetVO(assetVO);
        String expResult = "";
        String result = instance.getAssetVO().getAssetName();
        assertNotEquals(expResult, result);
        System.out.println("setAssetVO Not Equals Done");
    }

    /**
     * Test of getAssetVO method, of class CreateAssetBean.
     */
    @Test
    public void test25_GetAssetVO() {
        System.out.println("getAssetVO");
        AssetVO expResult = assetVO;
        Whitebox.setInternalState(instance, "assetVO", assetVO);
        AssetVO result = instance.getAssetVO();
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getAssetVO method, of class CreateAssetBean.
     */
    @Test
    public void test26_GetAssetVO() {
        System.out.println("getAssetVO Not Equals");
        Whitebox.setInternalState(instance, "assetVO", assetVO);
        String result = instance.getAssetVO().getAssetName();
        String expResult = "";
        assertNotEquals(expResult, result);
        System.out.println("getAssetVO Not Equals Done");
    }

    /**
     * Test of getSiteList method, of class CreateAssetBean.
     */
    @Test
    public void test27_GetSiteList() {
        System.out.println("getSiteList");
        List<UserSitesVO> expResult = siteList;
        Whitebox.setInternalState(instance, "siteList", siteList);
        List<UserSitesVO> result = instance.getSiteList();
        assertEquals(expResult, result);
        System.out.println("getSiteList Done");
    }
    
    /**
     * Test of getSiteList method, of class CreateAssetBean.
     */
    @Test
    public void test28_GetSiteList() {
        System.out.println("getSiteList Not Equals");
        String expResult = "";
        Whitebox.setInternalState(instance, "siteList", siteList);
        String result = instance.getSiteList().get(0).getSiteName();
        assertNotEquals(expResult, result);
        System.out.println("getSiteList Not Equals Done");
    }

    /**
     * Test of getCustomerApSetVOList method, of class CreateAssetBean.
     */
    @Test
    public void test31_GetCustomerApSetVOList() {
        System.out.println("getCustomerApSetVOList");
        List<SelectItem> customerApSetVOList = new ArrayList();
        customerApSetVOList.add(new SelectItem(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"), "Test ApAl 1"));
        Whitebox.setInternalState(instance, "customerApSetVOList", customerApSetVOList);
        List<SelectItem> expResult = customerApSetVOList;
        List<SelectItem> result = instance.getCustomerApSetVOList();
        assertEquals(expResult, result);
        System.out.println("getCustomerApSetVOList Done");
    }
    
    /**
     * Test of getCustomerApSetVOList method, of class CreateAssetBean.
     */
    @Test
    public void test32_GetCustomerApSetVOList() {
        System.out.println("getCustomerApSetVOList Not Equals");
        List<SelectItem> customerApSetVOList = new ArrayList();
        customerApSetVOList.add(new SelectItem(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"), "Test ApAl 1"));
        Whitebox.setInternalState(instance, "customerApSetVOList", customerApSetVOList);
        String expResult = "";
        String result = instance.getCustomerApSetVOList().get(0).getLabel();
        assertNotEquals(expResult, result);
        System.out.println("getCustomerApSetVOList Not Equals Done");
    }

    /**
     * Test of getSiteApAlSetsVOList method, of class CreateAssetBean.
     */
    @Test
    public void test33_GetSiteApAlSetsVOList() {
        System.out.println("getSiteApAlSetsVOList");
        List<ApAlSetVO> siteApAlSetsVOList = new ArrayList();
        ApAlSetVO apAlSetVO = new ApAlSetVO();
        apAlSetVO.setAlSetName("Test ApAl set Name");
        siteApAlSetsVOList.add(apAlSetVO);
        Whitebox.setInternalState(instance, "siteApAlSetsVOList", siteApAlSetsVOList);
        List<ApAlSetVO> result = instance.getSiteApAlSetsVOList();
        assertEquals(siteApAlSetsVOList, result);
        System.out.println("getSiteApAlSetsVOList Done");
    }
    
    /**
     * Test of getSiteApAlSetsVOList method, of class CreateAssetBean.
     */
    @Test
    public void test34_GetSiteApAlSetsVOList() {
        System.out.println("getSiteApAlSetsVOList Not Equals");
        List<ApAlSetVO> siteApAlSetsVOList = new ArrayList();
        ApAlSetVO apAlSetVO = new ApAlSetVO();
        apAlSetVO.setAlSetName("Test ApAl set Name");
        siteApAlSetsVOList.add(apAlSetVO);
        Whitebox.setInternalState(instance, "siteApAlSetsVOList", siteApAlSetsVOList);
        List<ApAlSetVO> result = instance.getSiteApAlSetsVOList();
        List<ApAlSetVO> expResult = null;
        assertNotEquals(expResult, result);
        System.out.println("getSiteApAlSetsVOList Not Equals Done");
    }
    
    /**
     * Test of getAcSensorTypeList method, of class CreateAssetBean.
     */
    @Test
    public void test35_GetAcSensorTypeList() {
        System.out.println("getAcSensorTypeList");
        assertEquals(instance.getAcSensorTypeList().size(), getACSensorTypeItemList().size());
        System.out.println("getAcSensorTypeList Done");
    }
    
    /**
     * Test of getDcSensorTypeList method, of class CreateAssetBean.
     */
    @Test
    public void test36_GetDcSensorTypeList() {
        System.out.println("getDcSensorTypeList");
        assertEquals(instance.getDcSensorTypeList().size(), getDCSensorTypeItemList().size());
        System.out.println("getDcSensorTypeList Done");
    }

    /**
     * Test of getTachometerList method, of class CreateAssetBean.
     */
    @Test
    public void test39_GetTachometerList() {
        System.out.println("getTachometerList");
        
        List<SelectItem> tachometerList = new ArrayList();
        tachometerList.add(new SelectItem(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"), "Test Tachometer 1"));
        Whitebox.setInternalState(instance, "tachometerList", tachometerList);
        List<SelectItem> expResult = tachometerList;
        List<SelectItem> result = instance.getTachometerList();
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getTachometerList method, of class CreateAssetBean.
     */
    @Test
    public void test40_GetTachometerList() {
        System.out.println("getTachometerList Not Equals");
        
        List<SelectItem> tachometerList = new ArrayList();
        tachometerList.add(new SelectItem(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"), "Test Tachometer 1"));
        Whitebox.setInternalState(instance, "tachometerList", tachometerList);
        List<SelectItem> expResult = null;
        List<SelectItem> result = instance.getTachometerList();
        assertNotEquals(expResult, result);
        System.out.println("getTachometerList Not Equals Done");
    }

    /**
     * Test of getPtLoNamePresetList method, of class CreateAssetBean.
     */
    @Test
    public void test41_GetPtLoNamePresetList() {
        System.out.println("getPtLoNamePresetList");
        List<SelectItem> ptLoNamePresetList = new ArrayList();
        ptLoNamePresetList.add(new SelectItem(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"), "Test Tachometer 1"));
        Whitebox.setInternalState(instance, "ptLoNamePresetList", ptLoNamePresetList);
        List<SelectItem> expResult = ptLoNamePresetList;
        List<SelectItem> result = instance.getPtLoNamePresetList();
        assertEquals(expResult, result);
        System.out.println("getPtLoNamePresetList Done");
    }
    
    /**
     * Test of getPtLoNamePresetList method, of class CreateAssetBean.
     */
    @Test
    public void test42_GetPtLoNamePresetList() {
        System.out.println("getPtLoNamePresetList Not Equals");
        List<SelectItem> ptLoNamePresetList = new ArrayList();
        ptLoNamePresetList.add(new SelectItem(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"), "Test Tachometer 1"));
        Whitebox.setInternalState(instance, "ptLoNamePresetList", ptLoNamePresetList);
        List<SelectItem> expResult = null;
        List<SelectItem> result = instance.getPtLoNamePresetList();
        assertNotEquals(expResult, result);
        System.out.println("getPtLoNamePresetList Not Equals Done");
    }

    /**
     * Test of getFaultFrequenciesList method, of class CreateAssetBean.
     */
    @Test
    public void test43_GetFaultFrequenciesList() {
        System.out.println("getFaultFrequenciesList");
        List<SelectItem> faultFrequenciesList = new ArrayList();
        faultFrequenciesList.add(new SelectItem(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"), "Fault frequency 1"));
        Whitebox.setInternalState(instance, "faultFrequenciesList", faultFrequenciesList);
        List<SelectItem> expResult = faultFrequenciesList;
        List<SelectItem> result = instance.getFaultFrequenciesList();
        assertEquals(expResult, result);
        System.out.println("getPtLoNamePresetList Done");
    }
    
    /**
     * Test of getFaultFrequenciesList method, of class CreateAssetBean.
     */
    @Test
    public void test44_GetFaultFrequenciesList() {
        System.out.println("getFaultFrequenciesList Not Equals");
        List<SelectItem> faultFrequenciesList = new ArrayList();
        faultFrequenciesList.add(new SelectItem(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"), "Fault frequency 1"));
        Whitebox.setInternalState(instance, "faultFrequenciesList", faultFrequenciesList);
        List<SelectItem> expResult = null;
        List<SelectItem> result = instance.getFaultFrequenciesList();
        assertNotEquals(expResult, result);
        System.out.println("getPtLoNamePresetList Not Equals Done");
    }

    /**
     * Test of getAreaList method, of class CreateAssetBean.
     */
    @Test
    public void test45_GetAreaList() {
        System.out.println("getAreaList");
        List<AreaVO> areaList = new ArrayList();
        AreaVO areaVO = new AreaVO();
        areaVO.setAreaId(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"));
        areaVO.setAreaName("Test Area Name 1");
        Whitebox.setInternalState(instance, "areaList", areaList);
        List<AreaVO> expResult = areaList;
        List<AreaVO> result = instance.getAreaList();
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getAreaList method, of class CreateAssetBean.
     */
    @Test
    public void test46_GetAreaList() {
        System.out.println("getAreaList Not Equals");
        List<AreaVO> areaList = new ArrayList();
        AreaVO areaVO = new AreaVO();
        areaVO.setAreaId(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"));
        areaVO.setAreaName("Test Area Name 1");
        Whitebox.setInternalState(instance, "areaList", areaList);
        List<AreaVO> expResult = null;
        List<AreaVO> result = instance.getAreaList();
        assertNotEquals(expResult, result);
        System.out.println("getAreaList Not Equals Done");
    }
    
    /**
     * Test of createAsset method, of class CreateAssetBean.
     */
    @Test
    public void test47_CreateAsset() {
        System.out.println("createAsset");
        List<PointLocationVO> plVOList = new ArrayList();
        PointLocationVO plVO = new PointLocationVO();
        plVO.setPointLocationName("Test PoniLocation Name");
        plVOList.add(plVO);
        assetVO.setPointLocationList(plVOList);
        Whitebox.setInternalState(instance, "assetVO", assetVO);
        when(assetClient.createAsset(serviceRegistrarClient.getServiceHostURL(CONFIGURATION_SERVICE_NAME), assetVO)).thenReturn(Boolean.FALSE);
        when(externalContext.getSessionMap()).thenReturn(sessionMap);
        instance.createAsset();
        assertFalse(instance.getAssetVO().getPointLocationList().isEmpty());
    }
    
}
