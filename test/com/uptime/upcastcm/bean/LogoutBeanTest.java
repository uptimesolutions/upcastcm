/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package com.uptime.upcastcm.bean;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Vector;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.junit.runners.MethodSorters;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import static org.powermock.api.mockito.PowerMockito.when;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

/**
 *
 * @author joseph
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(PowerMockRunner.class)
@PowerMockIgnore("jdk.internal.reflect.*")
@PowerMockRunnerDelegate(JUnit4.class)
@PrepareForTest({FacesContext.class, ExternalContext.class, UserManageBean.class, ResourceBundle.class})
public class LogoutBeanTest {

    @Mock
    FacesContext facesContext;
    @Mock
    ExternalContext externalContext;
    @Mock
    HttpServletRequest request;
    @Mock
    HttpSession session;
    @Mock
    UserManageBean userManageBean;
    @Mock
    ResourceBundle resourceBundle;

    Map<String, Object> sessionMap;
    LogoutBean instance;
    Enumeration<String> enumeration;
    String resourceProperty = "application_header";

    public LogoutBeanTest() {
        PowerMockito.mockStatic(FacesContext.class);
        PowerMockito.mockStatic(ResourceBundle.class);
        facesContext = PowerMockito.mock(FacesContext.class);
        externalContext = PowerMockito.mock(ExternalContext.class);
        request = PowerMockito.mock(HttpServletRequest.class);
        session = PowerMockito.mock(HttpSession.class);
        userManageBean = PowerMockito.mock(UserManageBean.class);
        resourceBundle = PowerMockito.mock(ResourceBundle.class);

        sessionMap = new HashMap();
        sessionMap.put("userManageBean", userManageBean);
        session.setAttribute("userManageBean", userManageBean);
        Vector<String> rollno = new Vector();
        rollno.add("userManageBean");
        enumeration = rollno.elements();
        when(FacesContext.getCurrentInstance()).thenReturn(facesContext);
        when(facesContext.getExternalContext()).thenReturn(externalContext);
        when(externalContext.getSessionMap()).thenReturn(sessionMap);
        when(facesContext.getExternalContext().getRequest()).thenReturn(request);
        when(request.getSession()).thenReturn(session);
        when(request.getSession(false)).thenReturn(session);
        when(session.getAttribute("userManageBean")).thenReturn(userManageBean);
        when(request.getSession(false).getAttributeNames()).thenReturn(enumeration);
       

        instance = new LogoutBean();
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of doLogout method, of class LogoutBean.
     */
    @Test
    public void test00_DoLogout() {
        System.out.println("doLdapLogout");
        assertTrue(!sessionMap.isEmpty());
        instance.doLdapLogout();
        assertTrue(sessionMap.isEmpty());
        System.out.println("doLdapLogout completed");
    }
    
    /**
     * Test of doLogout method, of class LogoutBean.
     */
    @Test
    public void test01_DoSamlRedirect() {
        System.out.println("doSamlRedirect");
        instance.doSamlRedirect();
        assertTrue(!sessionMap.isEmpty());
        System.out.println("doSamlRedirect completed");
    }

    /**
     * Test of keepSessionAlive method, of class LogoutBean.
     */
    @Test
    public void test02_KeepSessionAlive() {
        System.out.println("keepSessionAlive");
        instance.keepSessionAlive();
        System.out.println("keepSessionAlive completed");
    }

}
