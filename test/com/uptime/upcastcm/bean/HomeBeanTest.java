/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.AlarmsClient;
import com.uptime.upcastcm.http.client.AreaClient;
import com.uptime.upcastcm.http.client.AssetClient;
import com.uptime.upcastcm.http.client.PointLocationClient;
import com.uptime.upcastcm.http.client.WorkOrderClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import com.uptime.upcastcm.utils.vo.AlarmsVO;
import com.uptime.upcastcm.utils.vo.AreaVO;
import com.uptime.upcastcm.utils.vo.AssetVO;
import com.uptime.upcastcm.utils.vo.PointLocationVO;
import com.uptime.upcastcm.utils.vo.UserSitesVO;
import com.uptime.upcastcm.utils.vo.WorkOrderVO;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.junit.runners.MethodSorters;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import static org.powermock.api.mockito.PowerMockito.when;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

/**
 *
 * @author twilcox
 */
@RunWith(PowerMockRunner.class)
@PowerMockIgnore("jdk.internal.reflect.*")
@PrepareForTest({FacesContext.class, ExternalContext.class, UserManageBean.class, ServiceRegistrarClient.class, AlarmsClient.class, AreaClient.class, AssetClient.class, PointLocationClient.class, WorkOrderClient.class})
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@PowerMockRunnerDelegate(JUnit4.class)
public class HomeBeanTest {
    
    @Mock
    ServiceRegistrarClient serviceRegistrarClient;

    @Mock
    FacesContext facesContext;

    @Mock
    ExternalContext externalContext;
    
    @Mock
    HttpServletRequest request;
    
    @Mock
    HttpSession session;

    @Mock
    UserManageBean userManageBean;
    
    @Mock
    AlarmsClient alarmsClient;
    
    @Mock
    AreaClient areaClient;
    
    @Mock
    AssetClient assetClient;
    
    @Mock
    PointLocationClient pointLocationClient;
    
    @Mock
    WorkOrderClient workOrderClient;
    
    private final HomeBean instance;
    Map<String, Object> sessionMap;
    UUID siteId;
    
    public HomeBeanTest() {
        UserSitesVO usvo;
        
        PowerMockito.mockStatic(FacesContext.class);
        PowerMockito.mockStatic(ServiceRegistrarClient.class);
        PowerMockito.mockStatic(AlarmsClient.class);
        PowerMockito.mockStatic(AreaClient.class);
        PowerMockito.mockStatic(AssetClient.class);
        PowerMockito.mockStatic(PointLocationClient.class);
        PowerMockito.mockStatic(WorkOrderClient.class);
        
        facesContext = PowerMockito.mock(FacesContext.class);
        externalContext = PowerMockito.mock(ExternalContext.class);
        request = PowerMockito.mock(HttpServletRequest.class);
        session = PowerMockito.mock(HttpSession.class);
        userManageBean = PowerMockito.mock(UserManageBean.class);
        
        serviceRegistrarClient = PowerMockito.mock(ServiceRegistrarClient.class);
        alarmsClient = PowerMockito.mock(AlarmsClient.class);
        areaClient = PowerMockito.mock(AreaClient.class);
        assetClient = PowerMockito.mock(AssetClient.class);
        pointLocationClient = PowerMockito.mock(PointLocationClient.class);
        workOrderClient = PowerMockito.mock(WorkOrderClient.class);
        
        sessionMap = new HashMap();
        sessionMap.put("userManageBean", userManageBean);
        session.setAttribute("userManageBean", userManageBean);
        
        when(ServiceRegistrarClient.getInstance()).thenReturn(serviceRegistrarClient);
        when(serviceRegistrarClient.getServiceHostURL(CONFIGURATION_SERVICE_NAME)).thenReturn("host");
        when(serviceRegistrarClient.getServiceHostURL(WORK_ORDER_SERVICE_NAME)).thenReturn("host");
        when(serviceRegistrarClient.getServiceHostURL(ALARM_SERVICE_NAME)).thenReturn("host");
        
        when(AlarmsClient.getInstance()).thenReturn(alarmsClient);
        when(AreaClient.getInstance()).thenReturn(areaClient);
        when(AssetClient.getInstance()).thenReturn(assetClient);
        when(PointLocationClient.getInstance()).thenReturn(pointLocationClient);
        when(WorkOrderClient.getInstance()).thenReturn(workOrderClient);
        
        when(FacesContext.getCurrentInstance()).thenReturn(facesContext);
        when(facesContext.getExternalContext()).thenReturn(externalContext);
        when(externalContext.getSessionMap()).thenReturn(sessionMap);
        when(facesContext.getExternalContext().getRequest()).thenReturn(request);
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("userManageBean")).thenReturn(userManageBean);
        
        siteId = UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0");
        
        usvo = new UserSitesVO();
        usvo.setCustomerAccount("77777");
        usvo.setSiteId(siteId);
        when(userManageBean.getCurrentSite()).thenReturn(usvo);
        
        instance = new HomeBean();
    }
    
    @Before
    public void setUp() {
        List<WorkOrderVO> workOrderVOList;
        List<AlarmsVO> alarmsVOList;
        List<AreaVO> areaVOList;
        List<AssetVO> assetVOList;
        List<PointLocationVO> pointLocationVOList;
        WorkOrderVO workOrderVO;
        AlarmsVO alarmsVO;
        AreaVO areaVO;
        UUID areaId;
        
        areaId = UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf2");
        
        areaVO = new AreaVO();
        areaVO.setAreaId(areaId);
        
        areaVOList = new ArrayList();
        areaVOList.add(areaVO);
        
        when(areaClient.getAreaSiteByCustomerSite("host", "77777", siteId)).thenReturn(areaVOList);
        
        assetVOList = new ArrayList();
        assetVOList.add(new AssetVO());
        
        when(assetClient.getAssetSiteAreaByCustomerSite("host", "77777", siteId.toString())).thenReturn(assetVOList);
        
        pointLocationVOList = new ArrayList();
        pointLocationVOList.add(new PointLocationVO());
        
        when(pointLocationClient.getPointLocationsByCustomerSiteArea("host", "77777", siteId, areaId)).thenReturn(pointLocationVOList);
        
        workOrderVO = new WorkOrderVO();
        workOrderVO.setCustomerAccount("77777");
        workOrderVO.setSiteId(siteId);
        workOrderVO.setTotalCount(4);
        workOrderVO.setPriority1(1);
        workOrderVO.setPriority2(1);
        workOrderVO.setPriority3(1);
        workOrderVO.setPriority4(1);
        
        workOrderVOList = new ArrayList();
        workOrderVOList.add(workOrderVO);
        
        when(workOrderClient.getWorkOrderCountsByCustomerSite("host", "77777", siteId)).thenReturn(workOrderVOList);
        
        alarmsVO = new AlarmsVO();
        alarmsVO.setCustomerAccount("77777");
        alarmsVO.setSiteId(siteId);
        alarmsVO.setAlarmCount(5);
        alarmsVO.setAlertCount(2);
        alarmsVO.setFaultCount(3);
        
        alarmsVOList = new ArrayList();
        alarmsVOList.add(alarmsVO);
        
        when(alarmsClient.getAlarmCountByCustomerSite("host", "77777", siteId)).thenReturn(alarmsVOList);
    }

    /**
     * Test of init method, of class HomeBean.
     */
    @Test
    public void test00_Init() {
        System.out.println("init");
        instance.init();
        assertEquals(5, instance.getAlarmCount());
        assertEquals(4, instance.getWorkOrderCount());
        System.out.println("init complete");
    }

    /**
     * Test of resetPage method, of class HomeBean.
     */
    @Test
    public void test01_ResetPage() {
        System.out.println("resetPage");
        instance.resetPage();
        assertEquals(5, instance.getAlarmCount());
        assertEquals(4, instance.getWorkOrderCount());
        System.out.println("resetPage complete");
    }

    /**
     * Test of getAreaCount method, of class HomeBean.
     */
    @Test
    public void test02_GetAreaCount() {
        System.out.println("getAreaCount");
        instance.resetPage();
        assertEquals(1, instance.getAreaCount());
        System.out.println("getAreaCount complete");
    }

    /**
     * Test of getAssetCount method, of class HomeBean.
     */
    @Test
    public void test03_GetAssetCount() {
        System.out.println("getAssetCount");
        instance.resetPage();
        assertEquals(1, instance.getAssetCount());
        System.out.println("getAssetCount complete");
    }

    /**
     * Test of getPointLocationCount method, of class HomeBean.
     */
    @Test
    public void test04_GetPointLocationCount() {
        System.out.println("getPointLocationCount");
        instance.resetPage();
        assertEquals(1, instance.getPointLocationCount());
        System.out.println("getPointLocationCount complete");
    }

    /**
     * Test of getAlarmCount method, of class HomeBean.
     */
    @Test
    public void test05_GetAlarmCount() {
        System.out.println("getAlarmCount");
        instance.resetPage();
        assertEquals(5, instance.getAlarmCount());
        System.out.println("getAlarmCount complete");
    }

    /**
     * Test of getAlertCount method, of class HomeBean.
     */
    @Test
    public void test06_GetAlertCount() {
        System.out.println("getAlertCount");
        instance.resetPage();
        assertEquals(2, instance.getAlertCount());
        System.out.println("getAlertCount complete");
    }

    /**
     * Test of getFaultCount method, of class HomeBean.
     */
    @Test
    public void test07_GetFaultCount() {
        System.out.println("getFaultCount");
        instance.resetPage();
        assertEquals(3, instance.getFaultCount());
        System.out.println("getFaultCount complete");
    }

    /**
     * Test of getWorkOrderCount method, of class HomeBean.
     */
    @Test
    public void test08_GetWorkOrderCount() {
        System.out.println("getWorkOrderCount");
        instance.resetPage();
        assertEquals(4, instance.getWorkOrderCount());
        System.out.println("getWorkOrderCount complete");
    }

    /**
     * Test of getPriority1Count method, of class HomeBean.
     */
    @Test
    public void test09_GetPriority1Count() {
        System.out.println("getPriority1Count");
        instance.resetPage();
        assertEquals(1, instance.getPriority1Count());
        System.out.println("getPriority1Count complete");
    }

    /**
     * Test of getPriority2Count method, of class HomeBean.
     */
    @Test
    public void test10_GetPriority2Count() {
        System.out.println("getPriority2Count");
        instance.resetPage();
        assertEquals(1, instance.getPriority2Count());
        System.out.println("getPriority2Count complete");
    }

    /**
     * Test of getPriority3Count method, of class HomeBean.
     */
    @Test
    public void test11_GetPriority3Count() {
        System.out.println("getPriority3Count");
        instance.resetPage();
        assertEquals(1, instance.getPriority3Count());
        System.out.println("getPriority3Count complete");
    }

    /**
     * Test of getPriority4Count method, of class HomeBean.
     */
    @Test
    public void test12_GetPriority4Count() {
        System.out.println("getPriority4Count");
        instance.resetPage();
        assertEquals(1, instance.getPriority4Count());
        System.out.println("getPriority4Count complete");
    }

    /**
     * Test of isSiteSelected method, of class HomeBean.
     */
    @Test
    public void test13_IsSiteSelected() {
        System.out.println("isSiteSelected");
        instance.resetPage();
        assertTrue(instance.isSiteSelected());
        System.out.println("isSiteSelected complete");
    }
}
