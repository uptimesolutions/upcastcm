/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.WorkOrderClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import com.uptime.upcastcm.utils.vo.UserSitesVO;
import com.uptime.upcastcm.utils.vo.WorkOrderVO;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.FixMethodOrder;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import static org.powermock.api.mockito.PowerMockito.when;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

/**
 *
 * @author twilcox
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(PowerMockRunner.class)
@PowerMockIgnore("jdk.internal.reflect.*")
@PrepareForTest({FacesContext.class, ExternalContext.class, ServiceRegistrarClient.class, UserManageBean.class, WorkOrderClient.class})
public class ClosedWorkOrderBeanTest {

    @Mock
    ServiceRegistrarClient serviceRegistrarClient;

    @Mock
    WorkOrderClient workOrderClient;

    @Mock
    FacesContext facesContext;

    @Mock
    ExternalContext externalContext;

    @Mock
    UserManageBean userManageBean;

    private final ClosedWorkOrderBean instance;
    private Map<String, Object> sessionMap;
    private List<WorkOrderVO> workOrderVOList;
    
    public ClosedWorkOrderBeanTest() {
        UserSitesVO usvo;
        WorkOrderVO workOrderVO;
        
        PowerMockito.mockStatic(FacesContext.class);
        PowerMockito.mockStatic(ServiceRegistrarClient.class);
        PowerMockito.mockStatic(WorkOrderClient.class);
        PowerMockito.mockStatic(ExternalContext.class);
        PowerMockito.mockStatic(UserManageBean.class);

        facesContext = PowerMockito.mock(FacesContext.class);
        externalContext = PowerMockito.mock(ExternalContext.class);
        userManageBean = PowerMockito.mock(UserManageBean.class);
        workOrderClient = PowerMockito.mock(WorkOrderClient.class);
        serviceRegistrarClient = PowerMockito.mock(ServiceRegistrarClient.class);
        sessionMap = new HashMap();
        sessionMap.put("userManageBean", userManageBean);

        when(FacesContext.getCurrentInstance()).thenReturn(facesContext);
        when(facesContext.getExternalContext()).thenReturn(externalContext);
        when(externalContext.getSessionMap()).thenReturn(sessionMap);
        when(WorkOrderClient.getInstance()).thenReturn(workOrderClient);
        when(ServiceRegistrarClient.getInstance()).thenReturn(serviceRegistrarClient);
        when(serviceRegistrarClient.getServiceHostURL(WORK_ORDER_SERVICE_NAME)).thenReturn("host");
        usvo = new UserSitesVO();
        usvo.setCustomerAccount("77777");
        usvo.setSiteId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        when(userManageBean.getCurrentSite()).thenReturn(usvo);
        when(userManageBean.getZoneId()).thenReturn(null);
     
        workOrderVO = new WorkOrderVO();
        workOrderVO.setCustomerAccount("77777");
        workOrderVO.setSiteId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        workOrderVO.setOpenedYear((short)2022);
        
        workOrderVOList = new ArrayList();
        workOrderVOList.add(workOrderVO);
        
        when(workOrderClient.getClosedWorkOrdersByCustomerSiteYear("host", "77777", UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"), (short)2020)).thenReturn(workOrderVOList);

        instance = new ClosedWorkOrderBean();
    }

    /**
     * Test of init method, of class ClosedWorkOrderBean.
     */
    @Test
    public void test00_Init() {
        System.out.println("init");
        //instance.init();
        //assertEquals(instance.getClosedWorkOrderList().size(), 1);
        System.out.println("init completed.");
    }

    /**
     * Test of resetPage method, of class ClosedWorkOrderBean.
     */
    @Test
    public void test01_ResetPage() {
        System.out.println("resetPage");
        //instance.resetPage();
        //assertEquals(instance.getClosedWorkOrderList().size(), 1);
        System.out.println("resetPage completed.");
    }

    /**
     * Test of incrementSelectedYear method, of class ClosedWorkOrderBean.
     */
    @Test
    public void test02_IncrementSelectedYear() {
        System.out.println("incrementSelectedYear");
        //instance.resetPage();
        //assertEquals(instance.getClosedWorkOrderList().size(), 1);
        //instance.incrementSelectedYear();
        //assertEquals(instance.getClosedWorkOrderList().size(), 0);
        System.out.println("incrementSelectedYear completed.");
    }

    /**
     * Test of decrementSelectedYear method, of class ClosedWorkOrderBean.
     */
    @Test
    public void test03_DecrementSelectedYear() {
        System.out.println("decrementSelectedYear");
        //instance.resetPage();
        //assertEquals(instance.getClosedWorkOrderList().size(), 1);
        //instance.decrementSelectedYear();
        //assertEquals(instance.getClosedWorkOrderList().size(), 0);
        System.out.println("decrementSelectedYear completed.");
    }

    /**
     * Test of getSelectedYear method, of class ClosedWorkOrderBean.
     */
    @Test
    public void test04_GetSelectedYear() {
        System.out.println("getSelectedYear");
        //instance.resetPage();
        //assertEquals(instance.getSelectedYear().shortValue(), (short)2022);
        System.out.println("getSelectedYear completed.");
    }

    /**
     * Test of getClosedWorkOrderList method, of class ClosedWorkOrderBean.
     */
    @Test
    public void test05_GetClosedWorkOrderList() {
        System.out.println("getClosedWorkOrderList");
        //instance.resetPage();
        //assertEquals(instance.getClosedWorkOrderList().size(), 1);
        System.out.println("getClosedWorkOrderList completed.");
    }

    /**
     * Test of getMaxSelectedYear method, of class ClosedWorkOrderBean.
     */
    @Test
    public void test06_GetMaxSelectedYear() {
        System.out.println("getMaxSelectedYear");
        //instance.resetPage();
        //assertEquals(instance.getMaxSelectedYear().shortValue(), (short)2022);
        System.out.println("getMaxSelectedYear completed.");
    }

    /**
     * Test of getMinSelectedYear method, of class ClosedWorkOrderBean.
     */
    @Test
    public void test07_GetMinSelectedYear() {
        System.out.println("getMinSelectedYear");
        //assertEquals(instance.getMinSelectedYear().shortValue(), (short)2022);
        System.out.println("getMinSelectedYear completed.");
    }

    /**
     * Test of getFilterBy method, of class ClosedWorkOrderBean.
     */
    @Test
    public void test08_GetFilterBy() {
        System.out.println("getFilterBy");
        //instance.setFilterBy(null);
        //assertNull(instance.getFilterBy());
        System.out.println("getFilterBy completed.");
    }

    /**
     * Test of setFilterBy method, of class ClosedWorkOrderBean.
     */
    @Test
    public void test09_SetFilterBy() {
        System.out.println("setFilterBy");
        //instance.setFilterBy(new ArrayList());
        //assertEquals(instance.getFilterBy().size(), 0);
        System.out.println("setFilterBy completed.");
    }

    /**
     * Test of getFilteredClosedWorkOrderList method, of class ClosedWorkOrderBean.
     */
    @Test
    public void test10_GetFilteredClosedWorkOrderList() {
        System.out.println("getFilteredClosedWorkOrderList");
        //instance.setFilteredClosedWorkOrderList(null);
        //assertNull(instance.getFilteredClosedWorkOrderList());
        System.out.println("getFilteredClosedWorkOrderList completed.");
    }

    /**
     * Test of setFilteredClosedWorkOrderList method, of class ClosedWorkOrderBean.
     */
    @Test
    public void test11_SetFilteredClosedWorkOrderList() {
        System.out.println("setFilteredClosedWorkOrderList");
        //instance.setFilteredClosedWorkOrderList(new ArrayList());
        //assertEquals(instance.getFilteredClosedWorkOrderList().size(), 0);
        System.out.println("setFilteredClosedWorkOrderList completed.");
    }
    
}
