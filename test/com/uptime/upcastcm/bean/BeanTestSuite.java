/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.upcastcm.bean;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author gopal
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
    ClosedWorkOrderBeanTest.class,
    ConfigurationBeanTest.class,
    CopyEditApAlSetBeanTest.class,
    CopyEditFaultFrequenciesBeanTest.class,
    CopyEditPointLocationNameBeanTest.class,
    CopyEditPresetsDeviceBeanTest.class,
    CopyEditTachometersBeanTest.class,
    CreateApAlSetBeanTest.class,
    CreateAreaBeanTest.class,
    CreateAssetBeanTest.class,
    CreateCustomFaultFrequecySetsBeanTest.class,
    CreateFaultFrequenciesBeanTest.class,
    CreatePointBeanTest.class,
    CreatePointLocationBeanTest.class,
    CreatePointLocationNameBeanTest.class,
    CreatePresetsDeviceBeanTest.class,
    CreateSiteBeanTest.class,
    CreateTachometerBeanTest.class,
    CreateTS1BeanTest.class,
    CreateUsersBeanTest.class,
    CreateWorkOrderBeanTest.class,
    DeviceManagementBeanTest.class,
    DeviceManagementInfoRowBeanTest.class,
    EditAreaBeanTest.class,
    EditAssetBeanTest.class,
    EditChannelBeanTest.class,
    EditPointBeanTest.class,
    EditPointLocationBeanTest.class,
    EditSiteBeanTest.class,
    EditTS1BeanTest.class,
    EditWorkOrderBeanTest.class,
    ForgotPasswordBeanTest.class,
    InstallDeviceBeanTest.class,
    LoginBeanTest.class,
    LogoutBeanTest.class,
    NavigationBeanTest.class,
    PresetsBeanTest.class,
    ReplaceDeviceBeanTest.class,
    UserManageBeanTest.class,
    UsersBeanTest.class,
    WorkOrderBeanTest.class,
    AddChannelBeanTest.class,
    AddTrendBeanTest.class,
    AlarmNotificationBeanTest.class,
    AlarmsBeanTest.class,
    AssetAnalysisBeanTest.class,
    AssetAnalysisWindowBeanTest.class,
    // Add more test classes here
})
public class BeanTestSuite {
    // This class doesn't need to have any code
}
