/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.AreaClient;
import com.uptime.upcastcm.http.client.AssetClient;
import com.uptime.upcastcm.http.client.UserClient;
import com.uptime.upcastcm.http.client.LdapClient;
import com.uptime.upcastcm.http.client.PointClient;
import com.uptime.upcastcm.http.client.PointLocationClient;
import com.uptime.upcastcm.http.client.SiteClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import com.uptime.upcastcm.utils.helperclass.PresetUtil;
import com.uptime.upcastcm.utils.vo.LdapUserVO;
import com.uptime.upcastcm.utils.vo.SiteUsersVO;
import com.uptime.upcastcm.utils.vo.UserPreferencesVO;
import com.uptime.upcastcm.utils.vo.UserSitesVO;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import static org.powermock.api.mockito.PowerMockito.when;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.primefaces.PrimeFaces;

/**
 *
 * @author gsingh
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(PowerMockRunner.class)
@PowerMockIgnore("jdk.internal.reflect.*")
@PrepareForTest({FacesContext.class, ExternalContext.class, ServiceRegistrarClient.class, NavigationBean.class, UserManageBean.class, CreateAssetBean.class, SiteClient.class, AreaClient.class, AssetClient.class, LdapClient.class, PointLocationClient.class, PointClient.class, UserClient.class, PresetUtil.class, PrimeFaces.class, PrimeFaces.Ajax.class})
public class UsersBeanTest {

    private final UsersBean instance;
    @Mock
    FacesContext facesContext;
    @Mock
    ExternalContext externalContext;
    @Mock
    UserManageBean userManageBean;
    @Mock
    ServiceRegistrarClient serviceRegistrarClient;
    @Mock
    UserClient userClient;
    @Mock
    HttpServletRequest request;
    @Mock
    HttpSession session;
    @Mock
    LdapClient ldapClient;
    Map<String, Object> sessionMap;
    List<UserSitesVO> userSiteList;
    UserPreferencesVO userPreferenceVO;
    List<SiteUsersVO> siteUsersVOList;

    public UsersBeanTest() {
        PowerMockito.mockStatic(FacesContext.class);
        PowerMockito.mockStatic(LdapClient.class);
        // HttpSession session = PowerMockito.mock(HttpSession.class);
        HttpServletRequest mockReq = PowerMockito.mock(HttpServletRequest.class);
        PowerMockito.mockStatic(ServiceRegistrarClient.class);
        PowerMockito.mockStatic(LdapClient.class);
        PowerMockito.mockStatic(UserClient.class);
        serviceRegistrarClient = PowerMockito.mock(ServiceRegistrarClient.class);
        userClient = PowerMockito.mock(UserClient.class);
        userManageBean = PowerMockito.mock(UserManageBean.class);
        mockReq.getSession(true);
        facesContext = PowerMockito.mock(FacesContext.class);
        externalContext = PowerMockito.mock(ExternalContext.class);
        ldapClient = PowerMockito.mock(LdapClient.class);

        request = PowerMockito.mock(HttpServletRequest.class);
        session = PowerMockito.mock(HttpSession.class);
//        FacesContext context = ContextMocker.mockFacesContext();
//        ExternalContext ext = PowerMockito.mock(ExternalContext.class);
//        when(context.getExternalContext()).thenReturn(ext);
//        when(ext.getRequest()).thenReturn(mockReq);
//        when(mockReq.getSession()).thenReturn(session);

        sessionMap = new HashMap();
        sessionMap.put("userManageBean", userManageBean);
        session.setAttribute("userManageBean", userManageBean);

        userSiteList = new ArrayList<>();
        UserSitesVO uvo = new UserSitesVO();
        uvo.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        uvo.setSiteName("Test Site 1");
        userSiteList.add(uvo);

        when(session.getAttribute("userManageBean")).thenReturn(userManageBean);
        when(ServiceRegistrarClient.getInstance()).thenReturn(serviceRegistrarClient);
        when(FacesContext.getCurrentInstance()).thenReturn(facesContext);
        when(LdapClient.getInstance()).thenReturn(ldapClient);
        when(UserClient.getInstance()).thenReturn(userClient);
        when(facesContext.getExternalContext()).thenReturn(externalContext);
        when(externalContext.getSessionMap()).thenReturn(sessionMap);
        when(facesContext.getExternalContext().getRequest()).thenReturn(request);
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("userManageBean")).thenReturn(userManageBean);
        when(userManageBean.getResourceBundleString("label_siteadministrator")).thenReturn("dummy1");
        when(userManageBean.getResourceBundleString("label_user")).thenReturn("label_user");
        when(userManageBean.getResourceBundleString("label_viewer")).thenReturn("label_viewer");
        userPreferenceVO = new UserPreferencesVO();
        userPreferenceVO.setCustomerAccount("77777");
        userPreferenceVO.setDefaultSite(UUID.fromString("e2cb8b2b-f5c1-4b7b-92d0-c20c7a94061e"));
        when(session.getAttribute("userVO")).thenReturn(userPreferenceVO);

        instance = new UsersBean();
        SiteUsersVO uvo1 = new SiteUsersVO();
        uvo1.setSiteId(UUID.fromString("e2cb8b2b-f5c1-4b7b-92d0-c20c7a94061e"));
        uvo1.setUserId("sam.sapde@uptime-solutions.us");
        uvo1.setFirstName("sam");
        uvo1.setLastName("Spade");
        uvo1.setUserId("sam.sapde@uptime-solutions.us");
        siteUsersVOList = new ArrayList();
        siteUsersVOList.add(uvo1);
        instance.setUsersList(siteUsersVOList);
    }

    /**
     * Test of init method, of class UsersBean.
     */
    @Test
    public void test1Init() {
        System.out.println("init");
        when(userClient.getSiteUsersListBySiteIdCustomerAccount(serviceRegistrarClient.getServiceHostURL(USER_PREFERENCE_SERVICE_NAME), userPreferenceVO.getDefaultSite().toString(), userPreferenceVO.getCustomerAccount())).thenReturn(new ArrayList());
        instance.init();
        System.out.println("init completed");
    }

    /**
     * Test of onCreateUser method, of class UsersBean.
     */
    @Test
    public void test2OnCreateUser() {
        System.out.println("onCreateUser");
        instance.onCreateUser();
        System.out.println("onCreateUser completed");
    }

    /**
     * Test of onUserRowEdit method, of class UsersBean.
     */
    @Test
    public void test3OnUserRowEdit() {
        System.out.println("onUserRowEdit");
        SiteUsersVO uvo = new SiteUsersVO();
        uvo.setSiteId(UUID.fromString("e2cb8b2b-f5c1-4b7b-92d0-c20c7a94061e"));
        uvo.setUserId("sam.sapde@uptime-solutions.us");
        uvo.setFirstName("sam");
        uvo.setLastName("Spade");
        uvo.setUserId("sam.sapde@uptime-solutions.us");
        List<SiteUsersVO> siteUsersVOs = new ArrayList();
        SiteUsersVO uvo1 = new SiteUsersVO();
        uvo1.setSiteId(UUID.fromString("e2cb8b2b-f5c1-4b7b-92d0-c20c7a94061e"));
        uvo1.setUserId("sam.sapde@uptime-solutions.us");
        uvo1.setFirstName("Sam ");
        uvo1.setLastName("Spade");
        uvo1.setUserId("sam.sapde@uptime-solutions.us");
        siteUsersVOs.add(uvo1);

        instance.setClonedUsersList(siteUsersVOs);
        when(userClient.updateUsers(serviceRegistrarClient.getServiceHostURL(USER_PREFERENCE_SERVICE_NAME), uvo)).thenReturn(Boolean.TRUE);
        when(ldapClient.updateUsers(serviceRegistrarClient.getServiceHostURL(LDAP_SERVICE_NAME), new LdapUserVO(), "updateUserAccount")).thenReturn(Boolean.TRUE);

        instance.onUserRowEdit(uvo);
        assertTrue(instance.getClonedUsersList().size() > 0);
        System.out.println("onUserRowEdit completed");
    }

    /**
     * Test of onUserRowEdit method, of class UsersBean.
     */
    @Test
    public void test4OnUserRowEdit() {
        System.out.println("onUserRowEdit");
        SiteUsersVO uvo = new SiteUsersVO();
        uvo.setSiteId(UUID.fromString("e2cb8b2b-f5c1-4b7b-92d0-c20c7a94061e"));
        uvo.setUserId("sam.sapde@uptime-solutions.us");
        uvo.setFirstName("sam");
        uvo.setLastName("Spade");
        uvo.setUserId("sam.sapde@uptime-solutions.us");
        List<SiteUsersVO> siteUsersVOs = new ArrayList();
        SiteUsersVO uvo1 = new SiteUsersVO();
        uvo1.setSiteId(UUID.fromString("e2cb8b2b-f5c1-4b7b-92d0-c20c7a94061e"));
        uvo1.setUserId("sam.sapde@uptime-solutions.us");
        uvo1.setFirstName("sam");
        uvo1.setLastName("Spade");
        uvo1.setUserId("sam.sapde@uptime-solutions.us");
        siteUsersVOs.add(uvo1);

        instance.setClonedUsersList(siteUsersVOs);

        when(userClient.updateUsers(serviceRegistrarClient.getServiceHostURL(USER_PREFERENCE_SERVICE_NAME), uvo)).thenReturn(Boolean.TRUE);
        when(ldapClient.updateUsers(serviceRegistrarClient.getServiceHostURL(LDAP_SERVICE_NAME), new LdapUserVO(), "updateUserAccount")).thenReturn(Boolean.TRUE);

        instance.onUserRowEdit(uvo);
        assertTrue(instance.getClonedUsersList().size() > 0);
        System.out.println("onUserRowEdit completed");
    }

    /**
     * Test of deleteUser method, of class UsersBean.
     */
    @Test
    public void test5DeleteUser() {
        System.out.println("deleteUser");
        instance.deleteUser();
        System.out.println("deleteUser completed");
    }

}
