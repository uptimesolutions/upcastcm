/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.AreaClient;
import com.uptime.upcastcm.http.client.SiteClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import static com.uptime.upcastcm.utils.enums.HourEnum.getHourItemList;
import com.uptime.upcastcm.utils.helperclass.Filters;
import com.uptime.upcastcm.utils.helperclass.PresetUtil;
import com.uptime.upcastcm.utils.vo.AreaConfigScheduleVO;
import com.uptime.upcastcm.utils.vo.AreaVO;
import com.uptime.upcastcm.utils.vo.SiteVO;
import com.uptime.upcastcm.utils.vo.UserPreferencesVO;
import com.uptime.upcastcm.utils.vo.UserSitesVO;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.faces.component.UIViewRoot;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.junit.runners.MethodSorters;
import org.mockito.Mock;
import static org.mockito.Mockito.mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import static org.powermock.api.mockito.PowerMockito.when;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;
import org.powermock.reflect.Whitebox;
import org.primefaces.PrimeFaces;
import org.primefaces.event.NodeExpandEvent;
import org.primefaces.model.TreeNode;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.MenuModel;

/**
 *
 * @author gsingh
 */
@PowerMockIgnore("jdk.internal.reflect.*")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnit4.class)
@PrepareForTest({FacesContext.class, ExternalContext.class, UserManageBean.class,PresetUtil.class, ServiceRegistrarClient.class, NavigationBean.class, SiteClient.class, AreaClient.class, PrimeFaces.class, PrimeFaces.Ajax.class})
public class CreateAreaBeanTest {

    @Mock
    private final UserManageBean userManageBean;

    @Mock
    ServiceRegistrarClient sc;

    @Mock
    SiteClient siteClient;
    
    @Mock
    AreaClient areaClient;

    @Mock
    FacesContext facesContext;
    
    @Mock
    ExternalContext externalContext;
    
    @Mock
    HttpServletRequest request;
    
    @Mock
    HttpSession session;
    
    @Mock
    UIViewRoot root;
    
    @Mock
    NavigationBean navigationBean;
    
    @Mock
    PresetUtil presetUtil;
    
    private final CreateAreaBean instance;
    Map<String, Object> sessionMap;
    String customerAccount;
    MenuModel breadcrumbModel;
    UserPreferencesVO userVO;
    private final List<UserSitesVO> userSiteList;
    private final Filters filters;
    List<AreaVO> areaList;
    
    public CreateAreaBeanTest() {
        PowerMockito.mockStatic(FacesContext.class);
        PowerMockito.mockStatic(ServiceRegistrarClient.class);
        PowerMockito.mockStatic(SiteClient.class);
        PowerMockito.mockStatic(AreaClient.class);

        facesContext = PowerMockito.mock(FacesContext.class);
        externalContext = PowerMockito.mock(ExternalContext.class);
        request = PowerMockito.mock(HttpServletRequest.class);
        session = PowerMockito.mock(HttpSession.class);
        root = PowerMockito.mock(UIViewRoot.class);
        navigationBean = PowerMockito.mock(NavigationBean.class);
        userManageBean = PowerMockito.mock(UserManageBean.class);
        presetUtil = PowerMockito.mock(PresetUtil.class);
        
        siteClient = PowerMockito.mock(SiteClient.class);
        areaClient = PowerMockito.mock(AreaClient.class);
        sc = PowerMockito.mock(ServiceRegistrarClient.class);

        sessionMap = new HashMap();
        sessionMap.put("navigationBean", navigationBean);
        sessionMap.put("userManageBean", userManageBean);
        session.setAttribute("navigationBean", navigationBean);
        session.setAttribute("userManageBean", userManageBean);

        userSiteList = new ArrayList<>();
        UserSitesVO uvo = new UserSitesVO();
        uvo.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        uvo.setSiteName("Test Site 1");
        userSiteList.add(uvo);
        
        userVO = new UserPreferencesVO();
        userVO.setCustomerAccount(customerAccount);
        userVO.setDefaultSite(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        userVO.setDefaultSiteName("Test Site 1");
        
        filters = new Filters();
         filters.setRegion("Region-1");
        filters.setCountry("USA");
        filters.setSite(new SelectItem(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), "Test Site 1"));
        
        AreaVO avo = new AreaVO();
        avo.setAreaName("Area Name 1");
        areaList = new ArrayList<>();
        areaList.add(avo);
        when(ServiceRegistrarClient.getInstance()).thenReturn(sc);
        when(SiteClient.getInstance()).thenReturn(siteClient);
        when(AreaClient.getInstance()).thenReturn(areaClient);
        when(FacesContext.getCurrentInstance()).thenReturn(facesContext);
        when(facesContext.getExternalContext()).thenReturn(externalContext);
        when(externalContext.getSessionMap()).thenReturn(sessionMap);
        when(facesContext.getExternalContext().getRequest()).thenReturn(request);
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("navigationBean")).thenReturn(navigationBean);
        when(session.getAttribute("userManageBean")).thenReturn(userManageBean);

        when(userManageBean.getUserSiteList()).thenReturn(userSiteList);
        when(userManageBean.getAccountUserPreferencesVO()).thenReturn(userVO);
        when(userManageBean.getCurrentSite()).thenReturn(uvo);
        when(facesContext.getViewRoot()).thenReturn(root);
        when(userManageBean.getSelectedFilters()).thenReturn(filters);
         when(userManageBean.getPresetUtil()).thenReturn(presetUtil);
        PowerMockito.mockStatic(PrimeFaces.class);
        PowerMockito.mockStatic(PrimeFaces.Ajax.class);
        PrimeFaces pf = PowerMockito.mock(PrimeFaces.class);
        PrimeFaces.Ajax ax = PowerMockito.mock(PrimeFaces.Ajax.class);
        when(pf.current()).thenReturn(pf);
        when(pf.ajax()).thenReturn(ax);

        customerAccount = "77777";
        UserPreferencesVO userVO = new UserPreferencesVO();
        userVO.setDefaultSite(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        userVO.setCustomerAccount("77777");
        when(session.getAttribute("userVO")).thenReturn(userVO);
        UserSitesVO usvo = new UserSitesVO();
        usvo.setCustomerAccount("77777");
        when(userManageBean.getCurrentSite()).thenReturn(usvo);
        this.instance = new CreateAreaBean();
        when(areaClient.getAreaSiteByCustomerSite("config", customerAccount, userVO.getDefaultSite())).thenReturn(areaList);
        
    }
    
    
    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        AreaVO avo = new AreaVO();
        avo.setSiteName("SITE_1");
        List<AreaConfigScheduleVO> scheduleVOList = new ArrayList();
        avo.setScheduleVOList(scheduleVOList);
        instance.setAreaVO(avo);
        breadcrumbModel = new DefaultMenuModel();
        when(navigationBean.getBreadcrumbModel()).thenReturn(breadcrumbModel);
        NodeExpandEvent nee = PowerMockito.mock(NodeExpandEvent.class);
        TreeNode tn = PowerMockito.mock(TreeNode.class);
        when(nee.getTreeNode()).thenReturn(tn);
        when(tn.getType()).thenReturn("region");
        when(tn.getData()).thenReturn("Region1");

        List<TreeNode> tnList = new ArrayList<>();
        tnList.add(PowerMockito.mock(TreeNode.class));
        when(sc.getServiceHostURL(CONFIGURATION_SERVICE_NAME)).thenReturn("config");
       
        when(navigationBean.getTreeNodeRoot()).thenReturn(tn);
        when(tn.getChildren()).thenReturn(tnList);
        
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of createArea method, of class CreateAreaBean.
     */
    @Test
    public void testCreateArea() throws Exception {

        /*    List<AreaConfigScheduleVO> acsVoList = new ArrayList<>();
        AreaConfigScheduleVO acsVO = new AreaConfigScheduleVO();
        acsVO.setCustomerAccount("77777");
        acsVO.setSiteName("Test Site");
        acsVO.setAreaName("area51");
        acsVO.setScheduleName("area51 sc1");
        acsVO.setDayOfWeek((byte) 2);
        acsVO.setSchDescription("area51 sc1 desc");
        acsVO.setHoursOfDay("01:00-2:00");
        acsVO.setContinuous(false);
        acsVoList.add(acsVO);

        AreaVO areaVO = new AreaVO();
        areaVO.setCustomerAccount("77777");
        areaVO.setSiteName("Test Site");
        areaVO.setAreaName("area51");
        areaVO.setClimateControlled(true);
        areaVO.setDescription("desc11");
        areaVO.setEnvironment("true,false");
        areaVO.setScheduleVOList(acsVoList); */
        HttpSession session = mock(HttpSession.class);
        HttpServletRequest mockReq = mock(HttpServletRequest.class);

        Whitebox.setInternalState(ServiceRegistrarClient.class, "instance", sc);
        Whitebox.setInternalState(AreaClient.class, "instance", areaClient);

        mockReq.getSession(true);

        FacesContext context = ContextMocker.mockFacesContext();
        ExternalContext ext = mock(ExternalContext.class);
        //    List<String> siteNames = new ArrayList();
        //    siteNames.add("Test Site");
        when(context.getExternalContext()).thenReturn(ext);
        when(ext.getRequest()).thenReturn(mockReq);
        when(mockReq.getSession()).thenReturn(session);
        when(session.getAttribute("userManageBean")).thenReturn(userManageBean);
        //    when(userManageBean.getSiteNames()).thenReturn(siteNames);
        
        instance.init();
         NodeExpandEvent nee = PowerMockito.mock(NodeExpandEvent.class);
        TreeNode tn = PowerMockito.mock(TreeNode.class);
        when(nee.getTreeNode()).thenReturn(tn);
        when(tn.getType()).thenReturn("region");
        when(tn.getData()).thenReturn("Region1");

        List<TreeNode> tnList = new ArrayList<>();
        tnList.add(PowerMockito.mock(TreeNode.class));
        when(sc.getServiceHostURL(CONFIGURATION_SERVICE_NAME)).thenReturn("config");
       
        when(navigationBean.getTreeNodeRoot()).thenReturn(tn);
        when(tn.getChildren()).thenReturn(tnList);
        
        when(areaClient.createArea("config", instance.getAreaVO())).thenReturn(true);
        instance.createArea();
        assertEquals(instance.getAreaVO(), new AreaVO());
        System.out.println("CreateArea Done");
    }

    /**
     * Test of init method, of class CreateAreaBean.
     */
    @Test
    public void test01_Init() {
        System.out.println("init");
        MockitoAnnotations.initMocks(instance);
        PowerMockito.mockStatic(PrimeFaces.class);
        PowerMockito.mockStatic(PrimeFaces.Ajax.class);
        PrimeFaces pf = PowerMockito.mock(PrimeFaces.class);
        PrimeFaces.Ajax ax = PowerMockito.mock(PrimeFaces.Ajax.class);
        when(pf.current()).thenReturn(pf);
        when(pf.ajax()).thenReturn(ax);
        when(sc.getServiceHostURL(CONFIGURATION_SERVICE_NAME)).thenReturn(CONFIGURATION_SERVICE_NAME);
        List<SiteVO> list = new ArrayList();
        SiteVO s1 = new SiteVO();
        s1.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        s1.setSiteName("Test Site 1");
        s1.setRegion("Region1");
        s1.setCountry("Country1");
        list.add(s1);
        when(siteClient.getSiteRegionCountryByCustomer(CONFIGURATION_SERVICE_NAME, customerAccount)).thenReturn(list);
        instance.setIndoor(true);
        instance.init();
        assertEquals(instance.isIndoor(), false);
    }

    /**
     * Test of resetPage method, of class CreateAreaBean.
     */
    @Test
    public void test02_ResetPage() {
        System.out.println("resetPage");
        instance.setOutdoor(true);
        instance.resetPage();
        assertEquals(instance.isOutdoor(), false);
        System.out.println("resetPage done");
    }

    /**
     * Test of presetFields method, of class CreateAreaBean.
     */
    @Test
    public void test03_PresetFields_Filters_Object() {
        System.out.println("presetFields");
        Object object = null;
        instance.presetFields(filters, object);
        assertEquals(instance.getSiteList().size(), 1);
        System.out.println("presetFields done");
    }

    /**
     * Test of presetFields method, of class CreateAreaBean.
     */
    @Test(expected = UnsupportedOperationException.class)
    public void test04_PresetFields_int_Object() {
        System.out.println("presetFields");
        int operationType = 0;
        Object object = null;
        instance.presetFields(operationType, object);
        System.out.println("presetFields int done");
    }

    /**
     * Test of presetFields method, of class CreateAreaBean.
     */
    @Test(expected = UnsupportedOperationException.class)
    public void test05_PresetFields_Object() {
        System.out.println("presetFields");
        Object object = null;
        instance.presetFields(object);
        System.out.println("presetFields Object done");
    }

    /**
     * Test of onSiteSelect method, of class CreateAreaBean.
     */
    @Test
    public void test06_OnSiteSelect() {
        System.out.println("onSiteSelect");
        instance.getAreaVO().setCustomerAccount(customerAccount);
        instance.setSelectedSite(userSiteList.get(0));
        instance.onSiteSelect();
        assertEquals(instance.getAreaNameList().size(), 1);
        System.out.println("onSiteSelect done");
    }

    /**
     * Test of onHrsChange method, of class CreateAreaBean.
     */
    @Test
    public void test07_OnHrsChange() {
        System.out.println("onHrsChange");
        AreaConfigScheduleVO acsVO = new AreaConfigScheduleVO();
        acsVO.setCustomerAccount("77777");
        acsVO.setSiteName("Test Site");
        acsVO.setAreaName("area51");
        acsVO.setScheduleName("area51 sc1");
        acsVO.setDayOfWeek((byte) 2);
        acsVO.setSchDescription("area51 sc1 desc");
        acsVO.setHoursOfDay("01:00-2:00");
        acsVO.setContinuous(false);       
         instance.isContinousChange(acsVO);
        instance.onHrsChange(acsVO);
        assertEquals(instance.getAreaVO().getSiteId(), null);
        System.out.println("onHrsChange done");
    }

    /**
     * Test of isContinousChange method, of class CreateAreaBean.
     */
    @Test
    public void test08_IsContinousChange() {
        System.out.println("isContinousChange");
        AreaConfigScheduleVO svo = new AreaConfigScheduleVO();
        instance.isContinousChange(svo);
        assertEquals(svo.getDayVOList().size(), 7);
        System.out.println("isContinousChange done");
    }

    /**
     * Test of validateAreaName method, of class CreateAreaBean.
     */
    @Test
    public void test09_ValidateAreaName() {
        System.out.println("validateAreaName");
        instance.validateAreaName();
        //No logic to test
        System.out.println("validateAreaName done");
    }

    /**
     * Test of updateBean method, of class CreateAreaBean.
     */
    @Test
    public void test10_UpdateBean() {
        System.out.println("updateBean");
        instance.updateBean();
        //No logic to test
        System.out.println("updateBean done");
    }

    /**
     * Test of updateCliemateControlled method, of class CreateAreaBean.
     */
    @Test
    public void test11_UpdateClimateControlled() {
        System.out.println("updateClimateControlled");
        instance.getAreaVO().setClimateControlled(true);
        instance.setIndoor(false);
        instance.updateClimateControlled();
        assertEquals(instance.getAreaVO().isClimateControlled(), false);
        System.out.println("updateClimateControlled done");
    }

    /**
     * Test of addSchedule method, of class CreateAreaBean.
     */
    @Test
    public void test12_AddSchedule() {
        System.out.println("addSchedule");
        List<AreaConfigScheduleVO> scheduleVOList = new ArrayList();
        instance.setScheduleVOList(scheduleVOList);
        instance.addSchedule();
        assertEquals(instance.getScheduleVOList().size(), 1);
        System.out.println("addSchedule done");
    }

    /**
     * Test of deleteSchedule method, of class CreateAreaBean.
     */
    @Test
    public void test13_DeleteSchedule() {
        System.out.println("deleteSchedule");
        AreaConfigScheduleVO svo = new AreaConfigScheduleVO();
        List<AreaConfigScheduleVO> scheduleVOList = new ArrayList();
        scheduleVOList.add(svo);
        instance.setScheduleVOList(scheduleVOList);
        instance.deleteSchedule(svo);
        assertEquals(instance.getScheduleVOList().size(), 0);
        System.out.println("deleteSchedule done");
    }

    /**
     * Test of getAreaVO method, of class CreateAreaBean.
     */
    @Test
    public void test14_GetAreaVO() {
        System.out.println("getAreaVO");
        AreaVO expResult = new AreaVO();
        expResult.setSiteName("SITE_1");
        List<AreaConfigScheduleVO> scheduleVOList = new ArrayList();
        expResult.setScheduleVOList(scheduleVOList);
        AreaVO result = instance.getAreaVO();
        assertEquals(expResult, result);
        System.out.println("getAreaVO done");
    }

    /**
     * Test of setAreaVO method, of class CreateAreaBean.
     */
    @Test
    public void test15_SetAreaVO() {
        System.out.println("setAreaVO");
        AreaVO areaVO = new AreaVO();
        instance.setAreaVO(areaVO);
        assertEquals(instance.getAreaVO(), areaVO);
        System.out.println("setAreaVO done");
    }

    /**
     * Test of getHrsList method, of class CreateAreaBean.
     */
    @Test
    public void test16_GetHrsList() {
        System.out.println("getHrsList");
        assertEquals(instance.getHrsList().size(), getHourItemList().size());
        System.out.println("getHrsList done");
    }

    /**
     * Test of isIndoor method, of class CreateAreaBean.
     */
    @Test
    public void test17_IsIndoor() {
        System.out.println("isIndoor");
        boolean expResult = false;
        boolean result = instance.isIndoor();
        assertEquals(expResult, result);
        System.out.println("isIndoor done");
    }

    /**
     * Test of setIndoor method, of class CreateAreaBean.
     */
    @Test
    public void testSetIndoor() {
        System.out.println("setIndoor");
        boolean indoor = false;
        instance.setIndoor(indoor);
        assertEquals(instance.isIndoor(), false);
        System.out.println("setIndoor done");
    }

    /**
     * Test of isOutdoor method, of class CreateAreaBean.
     */
    @Test
    public void testIsOutdoor() {
        System.out.println("isOutdoor");
        boolean expResult = false;
        boolean result = instance.isOutdoor();
        assertEquals(expResult, result);
        System.out.println("isOutdoor done");
    }

    /**
     * Test of setOutdoor method, of class CreateAreaBean.
     */
    @Test
    public void testSetOutdoor() {
        System.out.println("setOutdoor");
        boolean outdoor = false;
        instance.setOutdoor(outdoor);
        assertEquals(instance.isOutdoor(), false);
        System.out.println("setOutdoor done");
    }

    /**
     * Test of getAreaNameList method, of class CreateAreaBean.
     */
    @Test
    public void testGetAreaNameList() {
        System.out.println("getAreaNameList");
        List<String> expResult = null;
        List<String> result = instance.getAreaNameList();
        assertEquals(expResult, result);
        System.out.println("getAreaNameList done");
    }

    /**
     * Test of setAreaNameList method, of class CreateAreaBean.
     */
    @Test
    public void testSetAreaNameList() {
        System.out.println("setAreaNameList");
        List<String> areaNameList = null;
        instance.setAreaNameList(areaNameList);
        assertEquals(instance.getAreaNameList(), null);
        System.out.println("setAreaNameList done");
    }

    /**
     * Test of getSiteList method, of class CreateAreaBean.
     */
    @Test
    public void testGetSiteList() {
        System.out.println("getSiteList");
        List<UserSitesVO> expResult = null;
        List<UserSitesVO> result = instance.getSiteList();
        assertEquals(expResult, result);
        System.out.println("getSiteList done");
    }
}
