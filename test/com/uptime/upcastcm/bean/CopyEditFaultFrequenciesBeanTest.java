/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.FaultFrequenciesClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import com.uptime.upcastcm.utils.helperclass.Filters;
import com.uptime.upcastcm.utils.helperclass.PresetUtil;
import com.uptime.upcastcm.utils.vo.FaultFrequenciesVO;
import com.uptime.upcastcm.utils.vo.UserPreferencesVO;
import com.uptime.upcastcm.utils.vo.UserSitesVO;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.faces.component.UIViewRoot;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import static org.powermock.api.mockito.PowerMockito.when;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.primefaces.PrimeFaces;

/**
 *
 * @author Joseph
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(PowerMockRunner.class)
@PowerMockIgnore("jdk.internal.reflect.*")
@PrepareForTest({FacesContext.class, ServiceRegistrarClient.class, ExternalContext.class, UserManageBean.class, UserPreferencesVO.class, FaultFrequenciesClient.class, PrimeFaces.class, PrimeFaces.Ajax.class})
public class CopyEditFaultFrequenciesBeanTest {

    @Mock
    UserPreferencesVO userVO;

    @Mock
    FacesContext facesContext;

    @Mock
    ExternalContext externalContext;

    @Mock
    FaultFrequenciesClient faultFrequenciesClient;

    @Mock
    ServiceRegistrarClient serviceRegistrarClient;

    @Mock
    UserManageBean userManageBean;
    
    @Mock
    CreateCustomFaultFrequecySetsBean createCustomFaultFrequecySetsBean;

    @Mock
    HttpServletRequest request;

    @Mock
    HttpSession session;

    @Mock
    UIViewRoot root;

    @Mock
    NavigationBean navigationBean;

    @Mock
    PresetsBean presetsBean;

    private final CopyEditFaultFrequenciesBean instance;
    Map<String, Object> sessionMap;
    FaultFrequenciesVO faultFrequenciesVO;
    List<UserSitesVO> userSiteList;
    PrimeFaces pf;
    PrimeFaces.Ajax ax;

    public CopyEditFaultFrequenciesBeanTest() {
        PowerMockito.mockStatic(FacesContext.class);
        PowerMockito.mockStatic(FaultFrequenciesClient.class);
        PowerMockito.mockStatic(ServiceRegistrarClient.class);

        facesContext = PowerMockito.mock(FacesContext.class);
        externalContext = PowerMockito.mock(ExternalContext.class);
        userManageBean = PowerMockito.mock(UserManageBean.class);
        request = PowerMockito.mock(HttpServletRequest.class);
        session = PowerMockito.mock(HttpSession.class);
        root = PowerMockito.mock(UIViewRoot.class);
        navigationBean = PowerMockito.mock(NavigationBean.class);
        createCustomFaultFrequecySetsBean = PowerMockito.mock(CreateCustomFaultFrequecySetsBean.class);
        faultFrequenciesClient = PowerMockito.mock(FaultFrequenciesClient.class);
        serviceRegistrarClient = PowerMockito.mock(ServiceRegistrarClient.class);
        presetsBean = PowerMockito.mock(PresetsBean.class);

        PowerMockito.mockStatic(PrimeFaces.class);
        PowerMockito.mockStatic(PrimeFaces.Ajax.class);
        pf = PowerMockito.mock(PrimeFaces.class);
        ax = PowerMockito.mock(PrimeFaces.Ajax.class);
        when(PrimeFaces.current()).thenReturn(pf);
        when(pf.ajax()).thenReturn(ax);

        // Mockito.doNothing().when(presetUtil).clearGlobalFaultFrequenciesList();
        ax.addCallbackParam("presetsTabViewId:faultFrequenciesFormId", new Object());

        sessionMap = new HashMap();
        sessionMap.put("navigationBean", navigationBean);
        sessionMap.put("userManageBean", userManageBean);
        sessionMap.put("presetsBean", presetsBean);
        sessionMap.put("createCustomFaultFrequecySetsBean", createCustomFaultFrequecySetsBean);

        session.setAttribute("navigationBean", navigationBean);
        session.setAttribute("createCustomFaultFrequecySetsBean", createCustomFaultFrequecySetsBean);
        session.setAttribute("presetsBean", presetsBean);
        session.setAttribute("createCustomFaultFrequecySetsBean", createCustomFaultFrequecySetsBean);

        when(FaultFrequenciesClient.getInstance()).thenReturn(faultFrequenciesClient);
        when(ServiceRegistrarClient.getInstance()).thenReturn(serviceRegistrarClient);
        when(FacesContext.getCurrentInstance()).thenReturn(facesContext);
        when(facesContext.getExternalContext()).thenReturn(externalContext);
        when(externalContext.getSessionMap()).thenReturn(sessionMap);
        when(facesContext.getExternalContext().getRequest()).thenReturn(request);
        when(request.getSession()).thenReturn(session);

        when(userManageBean.getPresetUtil()).thenReturn(new PresetUtil());
        when(session.getAttribute("navigationBean")).thenReturn(navigationBean);
        when(session.getAttribute("createCustomFaultFrequecySetsBean")).thenReturn(createCustomFaultFrequecySetsBean);
        when(session.getAttribute("presetsBean")).thenReturn(presetsBean);

        UserSitesVO uvo = new UserSitesVO();
        uvo.setSiteId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        uvo.setSiteName("Test Site 1");
        uvo.setCustomerAccount("77777");
        when(userManageBean.getCurrentSite()).thenReturn(uvo);

        when(facesContext.getViewRoot()).thenReturn(root);

        userVO = new UserPreferencesVO();
        userVO.setDefaultSite(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        userVO.setCustomerAccount("77777");
        when(session.getAttribute("userVO")).thenReturn(userVO);

        instance = new CopyEditFaultFrequenciesBean();
        faultFrequenciesVO = new FaultFrequenciesVO();
        faultFrequenciesVO.setCustomerAccount("77777");
        faultFrequenciesVO.setSiteId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        faultFrequenciesVO.setSiteName("Test Site 1");
        faultFrequenciesVO.setFfSetName("Test Set Name 1");
        faultFrequenciesVO.setFfSetDesc("Test Description 1");
        faultFrequenciesVO.setFfType("Site");

        // Whitebox.setInternalState(instance, "tmpFaultFrequenciesVO", faultFrequenciesVO);
    }

    @Before
    public void setUp() {

    }

    /**
     * Test of resetPage method, of class CopyEditFaultFrequenciesBean.
     */
    @Test
    public void test0_ResetPage() {
        System.out.println("resetPage");
        instance.resetPage();
        assertSame(userVO.getCustomerAccount(), instance.getFrequenciesVO().getCustomerAccount());

    }

    /**
     * Test of resetPage method, of class CopyEditFaultFrequenciesBean.
     */
    @Test
    public void test01_ResetPage() {
        System.out.println("resetPage");
        instance.resetPage();
        assertNotSame(null, instance.getFrequenciesVO().getCustomerAccount());

    }

    /**
     * Test of presetFields method, of class CopyEditFaultFrequenciesBean.
     */
    @Test(expected = UnsupportedOperationException.class)
    public void test1_PresetFields_Filters_Object() {
        System.out.println("presetFields(Filters presets, Object object) Unsupported");
        Object object = null;
        Filters filters = null;
        instance.presetFields(filters, object);
    }

    /**
     * Test of presetFields method, of class CopyEditFaultFrequenciesBean.
     */
    @Test
    public void test2_PresetFields_int_Object() {
        System.out.println("Site presetFields");
        faultFrequenciesVO = new FaultFrequenciesVO();
        faultFrequenciesVO.setCustomerAccount("77777");
        faultFrequenciesVO.setSiteId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        faultFrequenciesVO.setFfSetId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8242"));
        faultFrequenciesVO.setSiteName("Test Site 1");
        faultFrequenciesVO.setFfSetName("Test Set Name 1");
        faultFrequenciesVO.setFfSetDesc("Test Description 1");
        faultFrequenciesVO.setFfType("Site");
        instance.presetFields(1, faultFrequenciesVO);
        System.out.println("Site presetFields completed");
    }

    /**
     * Test of presetFields method, of class CopyEditFaultFrequenciesBean.
     */
    @Test
    public void test20_PresetFields_int_Object() {
        System.out.println("Global presetFields");
        instance.getFrequenciesVOList().clear();
        List<FaultFrequenciesVO> list = new ArrayList();
        faultFrequenciesVO = new FaultFrequenciesVO();
        faultFrequenciesVO.setCustomerAccount("77777");
        faultFrequenciesVO.setSiteId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        faultFrequenciesVO.setSiteName("Test Site 1");
        faultFrequenciesVO.setFfSetName("Test Set Name 1");
        faultFrequenciesVO.setFfSetDesc("Test Description 1");
        faultFrequenciesVO.setFfType("Global");
        list.add(faultFrequenciesVO);

        when(faultFrequenciesClient.findByCustomerSetId(serviceRegistrarClient.getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), instance.getFrequenciesVO().getFfSetId())).thenReturn(list);

        instance.presetFields(1, faultFrequenciesVO);
        assertEquals(faultFrequenciesVO.getSiteName(), instance.getFrequenciesVOList().get(0).getSiteName());
        System.out.println("Global presetFields completed");
    }

    /**
     * Test of presetFields method, of class CopyEditFaultFrequenciesBean.
     */
    @Test
    public void test21_PresetFields_int_Object() {
        System.out.println("presetFields");
        instance.getFrequenciesVOList().clear();
        List<FaultFrequenciesVO> list = new ArrayList();
        faultFrequenciesVO = new FaultFrequenciesVO();
        faultFrequenciesVO.setCustomerAccount("77777");
        faultFrequenciesVO.setSiteId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        faultFrequenciesVO.setSiteName("Test Site 1");
        faultFrequenciesVO.setFfSetName("Test Set Name 1");
        faultFrequenciesVO.setFfSetDesc("Test Description 1");
        faultFrequenciesVO.setFfType("Site");
        list.add(faultFrequenciesVO);
        instance.setFrequenciesVOList(list);
        UserSitesVO usvo = new UserSitesVO();
        usvo.setCustomerAccount("77777");
        when(userManageBean.getCurrentSite()).thenReturn(usvo);
        when(faultFrequenciesClient.findByCustomerSiteSetId(serviceRegistrarClient.getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), instance.getFrequenciesVO().getSiteId(), instance.getFrequenciesVO().getFfSetId())).thenReturn(list);

        instance.presetFields(1, null);
        assertNotEquals(1, instance.getOperationType());
        System.out.println("presetFields completed");
    }

    /**
     * Test of addFaultFrequencies method, of class
     * CopyEditFaultFrequenciesBean.
     */
    @Test
    public void test3_AddFaultFrequencies() {
        System.out.println("addFaultFrequencies");
        int startCount = instance.getFrequenciesVOList().size();
        instance.addFaultFrequencies();
        int endCount = instance.getFrequenciesVOList().size();
        assertEquals(startCount + 1, endCount);
    }

    /**
     * Test of addFaultFrequencies method, of class
     * CopyEditFaultFrequenciesBean.
     */
    @Test
    public void test30_AddFaultFrequencies() {
        System.out.println("addFaultFrequencies");
        int startCount = instance.getFrequenciesVOList().size();
        instance.addFaultFrequencies();
        int endCount = instance.getFrequenciesVOList().size();
        assertNotEquals(startCount, endCount);
    }

    /**
     * Test of submit method, of class CopyEditFaultFrequenciesBean.
     */
    @Test
    public void test5_Submit() {
        System.out.println("submit- copy at site level");
        faultFrequenciesVO = new FaultFrequenciesVO();
        faultFrequenciesVO.setCustomerAccount("77777");
        faultFrequenciesVO.setSiteId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        faultFrequenciesVO.setSiteName("Test Site 1");
        faultFrequenciesVO.setFfName("Test FF Name");
        faultFrequenciesVO.setFfUnit("U/s");
        faultFrequenciesVO.setFfSetName("Test Set Name 1");
        faultFrequenciesVO.setFfSetDesc("Test Description 1");
        faultFrequenciesVO.setFfType("Site");
        faultFrequenciesVO.setFfStartHarmonic(2);
        faultFrequenciesVO.setFfEndHarmonic(4);
        List<FaultFrequenciesVO> faultFrequenciesVOs = new ArrayList<>();
        faultFrequenciesVOs.add(faultFrequenciesVO);
        instance.setFrequenciesVOList(faultFrequenciesVOs);
        instance.setSetName("Test Set Name 1");
        instance.setOperationType(2);
        when(faultFrequenciesClient.createSiteFaultFrequencies(serviceRegistrarClient.getServiceHostURL(PRESET_SERVICE_NAME), instance.getFrequenciesVOList())).thenReturn(Boolean.TRUE);

        instance.submit();
        assertEquals(Boolean.FALSE, instance.isValidationFailed());
        System.out.println("submit completed");
    }

    /**
     * Test of submit method, of class CopyEditFaultFrequenciesBean.
     */
    @Test
    public void test52_Submit() {
        System.out.println("submit - Edit at Site level");

        //To update existing record
        faultFrequenciesVO = new FaultFrequenciesVO();
        faultFrequenciesVO.setCustomerAccount("77777");
        faultFrequenciesVO.setSiteId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        faultFrequenciesVO.setFfSetId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        faultFrequenciesVO.setSiteName("Test Site 1");
        faultFrequenciesVO.setFfName("Test FF Name");
        faultFrequenciesVO.setFfUnit("U/s");
        faultFrequenciesVO.setFfSetName("Test Set Name 1");
        faultFrequenciesVO.setFfSetDesc("Test Description 1");
        faultFrequenciesVO.setFfType("Site");
        faultFrequenciesVO.setFfStartHarmonic(2);
        faultFrequenciesVO.setFfEndHarmonic(4);
        List<FaultFrequenciesVO> faultFrequenciesVOs = new ArrayList<>();
        faultFrequenciesVOs.add(faultFrequenciesVO);

        //To create new record
        faultFrequenciesVO = new FaultFrequenciesVO();
        faultFrequenciesVO.setCustomerAccount("77777");
        faultFrequenciesVO.setFfName("Test FF Name");
        faultFrequenciesVO.setFfUnit("U/s");
        faultFrequenciesVO.setFfSetName("Test Set Name 1");
        faultFrequenciesVO.setFfSetDesc("Test Description 1");
        faultFrequenciesVO.setFfType(null);
        faultFrequenciesVO.setFfStartHarmonic(2);
        faultFrequenciesVO.setFfEndHarmonic(4);
        faultFrequenciesVOs.add(faultFrequenciesVO);
        instance.setFrequenciesVOList(faultFrequenciesVOs);
        instance.setSetName("Test Set Name 1");
        instance.setOperationType(1);

        when(faultFrequenciesClient.createSiteFaultFrequencies(serviceRegistrarClient.getServiceHostURL(PRESET_SERVICE_NAME), instance.getFrequenciesVOList())).thenReturn(Boolean.TRUE);
        when(FaultFrequenciesClient.getInstance().updateSiteFaultFrequencies(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), instance.getFrequenciesVOList())).thenReturn(Boolean.TRUE);

        instance.submit();
        assertEquals(Boolean.FALSE, instance.isValidationFailed());
        System.out.println("submit completed");
    }

    @Test
    public void test54_Submit() {
        System.out.println("submit- FFset name empty");
        faultFrequenciesVO = new FaultFrequenciesVO();
        faultFrequenciesVO.setCustomerAccount("77777");
        faultFrequenciesVO.setSiteId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        faultFrequenciesVO.setSiteName("Test Site 1");
        faultFrequenciesVO.setFfName("Test FF Name");
        faultFrequenciesVO.setFfUnit("U/s");
        faultFrequenciesVO.setFfSetName("Test Set Name 1");
        faultFrequenciesVO.setFfSetDesc("Test Description 1");
        faultFrequenciesVO.setFfType("Site");
        faultFrequenciesVO.setFfStartHarmonic(2);
        faultFrequenciesVO.setFfEndHarmonic(4);
        List<FaultFrequenciesVO> faultFrequenciesVOs = new ArrayList<>();
        faultFrequenciesVOs.add(faultFrequenciesVO);
        instance.setFrequenciesVOList(faultFrequenciesVOs);
        instance.setSetName(null);
        instance.setOperationType(2);

        instance.submit();
        assertEquals(Boolean.TRUE, instance.isValidationFailed());
        System.out.println("submit completed");
    }

    @Test
    public void test56_Submit() {
        System.out.println("submit- setFfName name empty");
        faultFrequenciesVO = new FaultFrequenciesVO();
        faultFrequenciesVO.setCustomerAccount("77777");
        faultFrequenciesVO.setSiteId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        faultFrequenciesVO.setSiteName("Test Site 1");
        faultFrequenciesVO.setFfName(null);
        faultFrequenciesVO.setFfUnit("U/s");
        faultFrequenciesVO.setFfSetName("Test Set Name 1");
        faultFrequenciesVO.setFfSetDesc("Test Description 1");
        faultFrequenciesVO.setFfType("Site");
        faultFrequenciesVO.setFfStartHarmonic(2);
        faultFrequenciesVO.setFfEndHarmonic(4);
        List<FaultFrequenciesVO> faultFrequenciesVOs = new ArrayList<>();
        faultFrequenciesVOs.add(faultFrequenciesVO);
        instance.setFrequenciesVOList(faultFrequenciesVOs);
        instance.setSetName("Test SET Name 1");
        instance.setOperationType(2);

        instance.submit();
        assertEquals(Boolean.TRUE, instance.isValidationFailed());
        System.out.println("submit completed");
    }

    @Test
    public void test57_Submit() {
        System.out.println("submit- setFfUnit name empty");
        faultFrequenciesVO = new FaultFrequenciesVO();
        faultFrequenciesVO.setCustomerAccount("77777");
        faultFrequenciesVO.setSiteId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        faultFrequenciesVO.setSiteName("Test Site 1");
        faultFrequenciesVO.setFfName("Test FF Name");
        faultFrequenciesVO.setFfUnit(null);
        faultFrequenciesVO.setFfSetName("Test Set Name 1");
        faultFrequenciesVO.setFfSetDesc("Test Description 1");
        faultFrequenciesVO.setFfType("Site");
        faultFrequenciesVO.setFfStartHarmonic(2);
        faultFrequenciesVO.setFfEndHarmonic(4);
        List<FaultFrequenciesVO> faultFrequenciesVOs = new ArrayList<>();
        faultFrequenciesVOs.add(faultFrequenciesVO);
        instance.setFrequenciesVOList(faultFrequenciesVOs);
        instance.setSetName("Test SET Name 1");
        instance.setOperationType(2);

        instance.submit();
        assertEquals(Boolean.TRUE, instance.isValidationFailed());
        System.out.println("submit completed");
    }

    @Test
    public void test58_Submit() {
        System.out.println("submit- StartHarmonic and  EndHarmonic validation");
        faultFrequenciesVO = new FaultFrequenciesVO();
        faultFrequenciesVO.setCustomerAccount("77777");
        faultFrequenciesVO.setSiteId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        faultFrequenciesVO.setSiteName("Test Site 1");
        faultFrequenciesVO.setFfName("Test FF Name");
        faultFrequenciesVO.setFfUnit("U/s");
        faultFrequenciesVO.setFfSetName("Test Set Name 1");
        faultFrequenciesVO.setFfSetDesc("Test Description 1");
        faultFrequenciesVO.setFfType("Site");
        faultFrequenciesVO.setFfStartHarmonic(6);
        faultFrequenciesVO.setFfEndHarmonic(4);
        List<FaultFrequenciesVO> faultFrequenciesVOs = new ArrayList<>();
        faultFrequenciesVOs.add(faultFrequenciesVO);
        instance.setFrequenciesVOList(faultFrequenciesVOs);
        instance.setSetName("Test SET Name 1");
        instance.setOperationType(2);

        instance.submit();
        assertEquals(Boolean.FALSE, instance.isValidationFailed());
        System.out.println("submit completed");
    }

    /**
     * Test of deleteApAlRow method, of class CopyEditFaultFrequenciesBean.
     */
    @Test
    public void test9_DeleteApAlRow() {
        System.out.println("deleteApAlRow");
        faultFrequenciesVO = new FaultFrequenciesVO();
        faultFrequenciesVO.setCustomerAccount("77777");
        faultFrequenciesVO.setSiteId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        faultFrequenciesVO.setSiteName("Test Site 1");
        faultFrequenciesVO.setFfName("Test FF Name");
        faultFrequenciesVO.setFfUnit("U/s");
        faultFrequenciesVO.setFfSetName("Test Set Name 1");
        faultFrequenciesVO.setFfSetDesc("Test Description 1");
        faultFrequenciesVO.setFfType("Site");
        faultFrequenciesVO.setFfStartHarmonic(2);
        faultFrequenciesVO.setFfEndHarmonic(4);

        FaultFrequenciesVO faultFrequenciesVO1 = new FaultFrequenciesVO();
        faultFrequenciesVO1.setCustomerAccount("77777");
        faultFrequenciesVO1.setSiteId(UUID.fromString("e3c66964-5a8c-451d-ae2e-9d04d83c8241"));
        faultFrequenciesVO1.setSiteName("Test Site 2");
        faultFrequenciesVO1.setFfName("Test FF Name 2");
        faultFrequenciesVO1.setFfUnit("U/s");
        faultFrequenciesVO1.setFfSetName("Test Set Name 2");
        faultFrequenciesVO1.setFfSetDesc("Test Description 2");
        faultFrequenciesVO1.setFfType("Site 2");
        faultFrequenciesVO1.setFfStartHarmonic(2);
        faultFrequenciesVO1.setFfEndHarmonic(4);

        List<FaultFrequenciesVO> faultFrequenciesVOs = new ArrayList<>();
        faultFrequenciesVOs.add(faultFrequenciesVO);
        faultFrequenciesVOs.add(faultFrequenciesVO1);
        instance.setFrequenciesVOList(faultFrequenciesVOs);
        int startCount = instance.getFrequenciesVOList().size();
        instance.deleteApAlRow(faultFrequenciesVO);
        assertSame(startCount - 1, instance.getFrequenciesVOList().size());
        System.out.println("deleteApAlRow completed");
    }

    /**
     * Test of deleteApAlRow method, of class CopyEditFaultFrequenciesBean.
     */
    @Test
    public void test90_DeleteApAlRow() {
        System.out.println("deleteApAlRow");
        faultFrequenciesVO = new FaultFrequenciesVO();
        faultFrequenciesVO.setCustomerAccount("77777");
        faultFrequenciesVO.setSiteId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        faultFrequenciesVO.setSiteName("Test Site 1");
        faultFrequenciesVO.setFfName("Test FF Name");
        faultFrequenciesVO.setFfUnit("U/s");
        faultFrequenciesVO.setFfSetName("Test Set Name 1");
        faultFrequenciesVO.setFfSetDesc("Test Description 1");
        faultFrequenciesVO.setFfType("Site");
        faultFrequenciesVO.setFfStartHarmonic(2);
        faultFrequenciesVO.setFfEndHarmonic(4);

        List<FaultFrequenciesVO> faultFrequenciesVOs = new ArrayList<>();
        faultFrequenciesVOs.add(faultFrequenciesVO);
        instance.setFrequenciesVOList(faultFrequenciesVOs);
        int startCount = instance.getFrequenciesVOList().size();
        instance.deleteApAlRow(faultFrequenciesVO);
        assertNotEquals(startCount - 1, instance.getFrequenciesVOList().size());
        System.out.println("deleteApAlRow completed");
    }

    /**
     * Test of getFrequenciesVO method, of class CopyEditFaultFrequenciesBean.
     */
    @Test
    public void test11_GetFrequenciesVO() {
        System.out.println("getFrequenciesVO");
        FaultFrequenciesVO ffVO = new FaultFrequenciesVO();
        ffVO.setFfName("Test FFName");
        instance.setFrequenciesVO(ffVO);
        FaultFrequenciesVO expResult = ffVO;
        FaultFrequenciesVO result = instance.getFrequenciesVO();
        assertEquals(expResult.getFfName(), result.getFfName());
    }

    /**
     * Test of getOperationType method, of class CopyEditFaultFrequenciesBean.
     */
    @Test
    public void test12_GetOperationType() {
        System.out.println("getOperationType");
        int expResult = 0;
        int result = instance.getOperationType();
        assertEquals(expResult, result);
    }

    /**
     * Test of getFrequenciesVOList method, of class
     * CopyEditFaultFrequenciesBean.
     */
    @Test
    public void test13_GetFrequenciesVOList() {
        System.out.println("getFrequenciesVOList");
        FaultFrequenciesVO ffVO = new FaultFrequenciesVO();
        ffVO.setFfName("Test FFName");
        List<FaultFrequenciesVO> ffList = new ArrayList();
        ffList.add(ffVO);
        instance.setFrequenciesVOList(ffList);
        List<FaultFrequenciesVO> expResult = new ArrayList();
        expResult.add(ffVO);
        List<FaultFrequenciesVO> result = instance.getFrequenciesVOList();
        assertEquals(expResult.get(0).getFfName(), result.get(0).getFfName());
    }

}
