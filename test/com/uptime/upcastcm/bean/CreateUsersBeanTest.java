/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.LdapClient;
import com.uptime.upcastcm.http.client.UserClient;
import com.uptime.upcastcm.utils.vo.SiteUsersVO;
import com.uptime.upcastcm.utils.vo.UserPreferencesVO;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import static org.powermock.api.mockito.PowerMockito.when;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.primefaces.PrimeFaces;

/**
 *
 * @author joseph
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(PowerMockRunner.class)
@PowerMockIgnore("jdk.internal.reflect.*")
@PrepareForTest({FacesContext.class, ExternalContext.class, HttpServletRequest.class, HttpSession.class, UserManageBean.class, ServiceRegistrarClient.class, UserClient.class, LdapClient.class,UsersBean.class, PrimeFaces.class, PrimeFaces.Ajax.class})
public class CreateUsersBeanTest {

    @Mock
    FacesContext facesContext;

    @Mock
    ExternalContext externalContext;

    @Mock
    UserManageBean userManageBean;

    @Mock
    HttpServletRequest request;

    @Mock
    HttpSession session;

    @Mock
    ServiceRegistrarClient serviceRegistrarClient;

    @Mock
    UserClient userClient;

    @Mock
    LdapClient ldapClient;
            
    @Mock
    UsersBean usersBean;

    CreateUsersBean instance;
    Map<String, Object> sessionMap;
    PrimeFaces pf;
    PrimeFaces.Ajax ax;
    SiteUsersVO siteUsersVO;
    UserPreferencesVO userPreferenceVO;

    public CreateUsersBeanTest() {
        PowerMockito.mockStatic(FacesContext.class);
        PowerMockito.mockStatic(ServiceRegistrarClient.class);
        PowerMockito.mockStatic(UserClient.class);
        PowerMockito.mockStatic(LdapClient.class);

        facesContext = PowerMockito.mock(FacesContext.class);
        externalContext = PowerMockito.mock(ExternalContext.class);
        userManageBean = PowerMockito.mock(UserManageBean.class);
        request = PowerMockito.mock(HttpServletRequest.class);
        userClient = PowerMockito.mock(UserClient.class);
        session = PowerMockito.mock(HttpSession.class);
        serviceRegistrarClient = PowerMockito.mock(ServiceRegistrarClient.class);
        ldapClient = PowerMockito.mock(LdapClient.class);
        usersBean = PowerMockito.mock(UsersBean.class);

        when(ServiceRegistrarClient.getInstance()).thenReturn(serviceRegistrarClient);
        when(UserClient.getInstance()).thenReturn(userClient);
        when(FacesContext.getCurrentInstance()).thenReturn(facesContext);

        when(facesContext.getExternalContext()).thenReturn(externalContext);
        when(externalContext.getRequest()).thenReturn(request);
        when(LdapClient.getInstance()).thenReturn(ldapClient);
        
        siteUsersVO = new SiteUsersVO();
        siteUsersVO.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        siteUsersVO.setCustomerAccount("77777");
        siteUsersVO.setFirstName("Sam");
        siteUsersVO.setLastName("Spade");
        siteUsersVO.setSiteRole("User");
        siteUsersVO.setUserId("sam.spade@uptime-solutions.us");
        List<SiteUsersVO> usersVO = new ArrayList();
        usersVO.add(siteUsersVO);
      //  usersBean.setUsersList(usersVO);
        sessionMap = new HashMap();
        sessionMap.put("userManageBean", userManageBean);
        sessionMap.put("usersBean", usersBean);
        session.setAttribute("userManageBean", userManageBean);
        session.setAttribute("usersBean", usersBean);
        when(externalContext.getSessionMap()).thenReturn(sessionMap);
        when(usersBean.getUsersList()).thenReturn(usersVO);

        when(request.getSession()).thenReturn(session);

        PowerMockito.mockStatic(PrimeFaces.class);
        PowerMockito.mockStatic(PrimeFaces.Ajax.class);
        pf = PowerMockito.mock(PrimeFaces.class);
        ax = PowerMockito.mock(PrimeFaces.Ajax.class);
        when(PrimeFaces.current()).thenReturn(pf);
        when(pf.ajax()).thenReturn(ax);

        instance = new CreateUsersBean();
        siteUsersVO = new SiteUsersVO();
        siteUsersVO.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        siteUsersVO.setCustomerAccount("77777");
        siteUsersVO.setFirstName("Sam");
        siteUsersVO.setLastName("Spade");
        siteUsersVO.setSiteRole("User");
        siteUsersVO.setUserId("sam.spade@uptime-solutions.us");
        instance.setSiteUsersVO(siteUsersVO);

        userPreferenceVO = new UserPreferencesVO();
        userPreferenceVO.setDefaultSite(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        userPreferenceVO.setCustomerAccount("77777");
    }

    /**
     * Test of init method, of class CreateUsersBean.
     */
    @Test
    public void test1_Init() {
        System.out.println("init");
        instance.init();
        assertTrue(instance.getUserRoles().size() > 0);
        System.out.println("init completed");
    }

    /**
     * Test of resetPage method, of class CreateUsersBean.
     */
    @Test
    public void test2_ResetPage() {
        System.out.println("resetPage");
        instance.resetPage();
        System.out.println("resetPage completed");
    }

    /**
     * Test of isUserExist method, of class CreateUsersBean.
     */
    @Test
    public void test3_IsUserExist() {
        System.out.println("isUserExist");
        instance.isUserExist();
        System.out.println("isUserExist completed");
    }

    /**
     * Test of createUser method, of class CreateUsersBean.
     */
    @Test
    public void test4_CreateUser() {
        System.out.println("createUser");
        instance.createUser();
        System.out.println("createUser completed");

    }
}
