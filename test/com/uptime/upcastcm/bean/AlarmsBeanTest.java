/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.AlarmsClient;
import com.uptime.upcastcm.http.client.PointClient;
import com.uptime.upcastcm.http.client.SiteClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import com.uptime.upcastcm.utils.helperclass.Filters;
import com.uptime.upcastcm.utils.vo.AlarmsVO;
import com.uptime.upcastcm.utils.vo.PointVO;
import com.uptime.upcastcm.utils.vo.SiteVO;
import com.uptime.upcastcm.utils.vo.UserPreferencesVO;
import com.uptime.upcastcm.utils.vo.UserSitesVO;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.faces.component.UIViewRoot;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.junit.runners.MethodSorters;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import static org.powermock.api.mockito.PowerMockito.when;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;
import org.primefaces.PrimeFaces;
import org.primefaces.event.NodeCollapseEvent;
import org.primefaces.event.NodeExpandEvent;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.primefaces.model.TreeNode;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.MenuModel;

/**
 *
 * @author gsingh
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(PowerMockRunner.class)
@PowerMockIgnore("jdk.internal.reflect.*")
@PowerMockRunnerDelegate(JUnit4.class)
@PrepareForTest({FacesContext.class, ExternalContext.class, UserManageBean.class, ServiceRegistrarClient.class, NavigationBean.class, AlarmsClient.class, SiteClient.class, PointClient.class, PrimeFaces.class, PrimeFaces.Ajax.class})
public class AlarmsBeanTest {

    @Mock
    ServiceRegistrarClient serviceRegistrarClient;
    @Mock
    AlarmsClient alarmsClient;
    @Mock
    SiteClient siteClient;
    @Mock
    PointClient pointClient;
    @Mock
    FacesContext facesContext;
    @Mock
    ExternalContext externalContext;
    @Mock
    HttpServletRequest request;
    @Mock
    HttpSession session;
    @Mock
    UIViewRoot root;
    @Mock
    NavigationBean navigationBean;
    @Mock
    UserManageBean userManageBean;

    private final AlarmsBean instance;
    Map<String, Object> sessionMap;
    String customerAccount;
    List<AlarmsVO> alarmsVOList;
    MenuModel breadcrumbModel;
    private final List<UserSitesVO> userSiteList;
    PointVO tmp;
    private final Filters filters;

    public AlarmsBeanTest() {
        PowerMockito.mockStatic(FacesContext.class);
        PowerMockito.mockStatic(ServiceRegistrarClient.class);
        PowerMockito.mockStatic(SiteClient.class);
        PowerMockito.mockStatic(AlarmsClient.class);
        PowerMockito.mockStatic(PointClient.class);

        facesContext = PowerMockito.mock(FacesContext.class);
        externalContext = PowerMockito.mock(ExternalContext.class);
        request = PowerMockito.mock(HttpServletRequest.class);
        session = PowerMockito.mock(HttpSession.class);
        root = PowerMockito.mock(UIViewRoot.class);
        navigationBean = PowerMockito.mock(NavigationBean.class);
        userManageBean = PowerMockito.mock(UserManageBean.class);

        siteClient = PowerMockito.mock(SiteClient.class);
        pointClient = PowerMockito.mock(PointClient.class);
        alarmsClient = PowerMockito.mock(AlarmsClient.class);
        serviceRegistrarClient = PowerMockito.mock(ServiceRegistrarClient.class);

        sessionMap = new HashMap();
        sessionMap.put("navigationBean", navigationBean);
        sessionMap.put("userManageBean", userManageBean);
        session.setAttribute("navigationBean", navigationBean);
        session.setAttribute("userManageBean", userManageBean);

        userSiteList = new ArrayList<>();
        UserSitesVO uvo = new UserSitesVO();
        uvo.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        uvo.setSiteName("Test Site 1");
        userSiteList.add(uvo);

        when(ServiceRegistrarClient.getInstance()).thenReturn(serviceRegistrarClient);
        when(SiteClient.getInstance()).thenReturn(siteClient);
        when(AlarmsClient.getInstance()).thenReturn(alarmsClient);
        when(PointClient.getInstance()).thenReturn(pointClient);
        when(FacesContext.getCurrentInstance()).thenReturn(facesContext);
        when(facesContext.getExternalContext()).thenReturn(externalContext);
        when(externalContext.getSessionMap()).thenReturn(sessionMap);
        when(facesContext.getExternalContext().getRequest()).thenReturn(request);
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("navigationBean")).thenReturn(navigationBean);
        when(session.getAttribute("userManageBean")).thenReturn(userManageBean);

        when(userManageBean.getUserSiteList()).thenReturn(userSiteList);
        when(facesContext.getViewRoot()).thenReturn(root);
        PowerMockito.mockStatic(PrimeFaces.class);
        PowerMockito.mockStatic(PrimeFaces.Ajax.class);
        PrimeFaces pf = PowerMockito.mock(PrimeFaces.class);
        PrimeFaces.Ajax ax = PowerMockito.mock(PrimeFaces.Ajax.class);
        when(pf.current()).thenReturn(pf);
        when(pf.ajax()).thenReturn(ax);

        filters = new Filters();
        filters.setCountry("Test Country 1");
        filters.setRegion("Test Region 1");
        filters.setSite(new SelectItem(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), "Test Site 1"));
        filters.setArea(new SelectItem(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"), "Test Area 1"));
        filters.setAsset(new SelectItem(UUID.fromString("f33fcf5a-5f93-4504-bc09-74a61a460bf0"), "Test Asset 1"));
        when(userManageBean.getSelectedFilters()).thenReturn(filters);
        
        customerAccount = "77777";
        UserPreferencesVO userVO = new UserPreferencesVO();
        userVO.setDefaultSite(UUID.randomUUID());
        userVO.setCustomerAccount("77777");
        when(session.getAttribute("userVO")).thenReturn(userVO);

        UserSitesVO usvo = new UserSitesVO();
        usvo.setCustomerAccount(customerAccount);
        when(userManageBean.getCurrentSite()).thenReturn(usvo);

        instance = new AlarmsBean();
        tmp = new PointVO();
        //Whitebox.setInternalState(instance, "tmpPointVO", tmp);
    }

    @Before
    public void setUp() {

        AlarmsVO avo = new AlarmsVO();
        avo.setSiteName("SITE_1");
        alarmsVOList = new ArrayList();
        alarmsVOList.add(avo);
        UserSitesVO uvo = new UserSitesVO();
        uvo.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        uvo.setSiteName("Test Site 1");
        uvo.setCustomerAccount(customerAccount);
        when(userManageBean.getCurrentSite()).thenReturn(uvo);
        breadcrumbModel = new DefaultMenuModel();
        when(navigationBean.getBreadcrumbModel()).thenReturn(breadcrumbModel);
        NodeExpandEvent nee = PowerMockito.mock(NodeExpandEvent.class);
        TreeNode tn = PowerMockito.mock(TreeNode.class);
        when(tn.getData()).thenReturn(new String());
        List<TreeNode> tnList = new ArrayList<>();
        tnList.add(tn);
        when(navigationBean.getTreeNodeRoot()).thenReturn(tn);
        when(navigationBean.getTreeNodeRoot().getChildren()).thenReturn(tnList);
        instance.setSelectedFilters();
        instance.getSelectedFilters().setCountry("Country 1");
        instance.getSelectedFilters().setRegion("Region 1");
        instance.getSelectedFilters().setSite(new SelectItem(UUID.randomUUID(), "site"));
        instance.getSelectedFilters().setArea(new SelectItem(UUID.randomUUID(), "area"));
        instance.getSelectedFilters().setAsset(new SelectItem(UUID.randomUUID(), "asset"));
        instance.getSelectedFilters().setPointLocation(new SelectItem(UUID.randomUUID(), "ptlo"));
        instance.getSelectedFilters().setPoint(new SelectItem(UUID.randomUUID(), "point"));

    }

    /**
     * Test of init method, of class AlarmsBean.
     */
    @Test
    public void test01_Init() {
        System.out.println("test01_Init");
        MockitoAnnotations.initMocks(instance);
        List<SiteVO> list = new ArrayList();
        SiteVO s1 = new SiteVO();
        s1.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        s1.setSiteName("Test Site 1");
        s1.setRegion("Region1");
        s1.setCountry("Country1");
        list.add(s1);
        PowerMockito.mockStatic(PrimeFaces.class);
        PowerMockito.mockStatic(PrimeFaces.Ajax.class);
        PrimeFaces pf = PowerMockito.mock(PrimeFaces.class);
        PrimeFaces.Ajax ax = PowerMockito.mock(PrimeFaces.Ajax.class);
        when(pf.current()).thenReturn(pf);
        when(pf.ajax()).thenReturn(ax);
        when(serviceRegistrarClient.getServiceHostURL(CONFIGURATION_SERVICE_NAME)).thenReturn(CONFIGURATION_SERVICE_NAME);
        when(siteClient.getSiteRegionCountryByCustomer(CONFIGURATION_SERVICE_NAME, customerAccount)).thenReturn(list);
        instance.init();
        System.out.println("test01_Init completed");
    }

    @Test
    public void test02_Init() {
        System.out.println("test02_Init");
        PowerMockito.mockStatic(PrimeFaces.class);
        PowerMockito.mockStatic(PrimeFaces.Ajax.class);
        PrimeFaces pf = PowerMockito.mock(PrimeFaces.class);
        PrimeFaces.Ajax ax = PowerMockito.mock(PrimeFaces.Ajax.class);
        when(pf.current()).thenReturn(pf);
        when(pf.ajax()).thenReturn(ax);
        instance.init();
        assertEquals(instance.getSelectedFilters().getRegion(), "Test Region 1");
        System.out.println("test02_Init completed");
    }

    /**
     * Test of deleteSelectedAlarms method, of class AlarmsBean. Removed 'Delete Alarm' functionality from UpCastCM.
     */
    @Ignore
    public void test011_deleteSelectedAlarms() {
        System.out.println("test011_deleteSelectedAlarms");
        when(serviceRegistrarClient.getServiceHostURL(ALARM_SERVICE_NAME)).thenReturn(ALARM_SERVICE_NAME);
        instance.getSelectedAlarms().addAll(alarmsVOList);

        assertTrue(instance.getSelectedAlarms().isEmpty());
        System.out.println("test011_deleteSelectedAlarms completed.");
    }

    /**
     * Test of acknowledgeAlarm method, of class AlarmsBean.
     */
    @Test
    public void test131_acknowledgeAlarm() {
        System.out.println("test131_acknowledgeAlarm");
        AlarmsVO alarmVO = new AlarmsVO();
        when(serviceRegistrarClient.getServiceHostURL(ALARM_SERVICE_NAME)).thenReturn(ALARM_SERVICE_NAME);
        instance.getSelectedAlarms().addAll(alarmsVOList);
        when(alarmsClient.updateAlarm(ALARM_SERVICE_NAME, alarmVO)).thenReturn(Boolean.TRUE);
        instance.acknowledgeAlarm();
        assertFalse(instance.getSelectedAlarms().isEmpty());
        System.out.println("test0131_acknowledgeAlarm completed.");
    }

    /**
     * Test of acknowledgeAlarm method, of class AlarmsBean.
     */
    @Test
    public void test132_acknowledgeAlarm() {
        System.out.println("test132_acknowledgeAlarm");
        AlarmsVO alarmVO = new AlarmsVO();
        when(serviceRegistrarClient.getServiceHostURL(ALARM_SERVICE_NAME)).thenReturn(ALARM_SERVICE_NAME);
        instance.getSelectedAlarms().addAll(alarmsVOList);
        when(alarmsClient.updateAlarm(ALARM_SERVICE_NAME, alarmVO)).thenReturn(Boolean.FALSE);
        instance.acknowledgeAlarm();
        assertEquals(instance.getSelectedAlarms().size(), 1);
        System.out.println("test0132_acknowledgeAlarm completed.");
    }

    /**
     * Test of acknowledgeSelectedAlarms method, of class AlarmsBean.
     */
    @Test
    public void test141_acknowledgeSelectedAlarms() {
        System.out.println("test0141_acknowledgeSelectedAlarms");
        when(serviceRegistrarClient.getServiceHostURL(ALARM_SERVICE_NAME)).thenReturn(ALARM_SERVICE_NAME);
        instance.getSelectedAlarms().addAll(alarmsVOList);
        when(alarmsClient.updateAlarmList(ALARM_SERVICE_NAME, alarmsVOList)).thenReturn(Boolean.TRUE);
        instance.acknowledgeSelectedAlarms();
        instance.getSelectedAlarms().clear();
        assertTrue(instance.getSelectedAlarms().isEmpty());
        System.out.println("test0141_acknowledgeSelectedAlarms completed.");
    }

    /**
     * Test of acknowledgeSelectedAlarms method, of class AlarmsBean.
     */
    @Test
    public void test142_acknowledgeSelectedAlarms() {
        System.out.println("test0142_acknowledgeSelectedAlarms");
        when(serviceRegistrarClient.getServiceHostURL(ALARM_SERVICE_NAME)).thenReturn(ALARM_SERVICE_NAME);
        instance.getSelectedAlarms().addAll(alarmsVOList);
        when(alarmsClient.updateAlarmList(ALARM_SERVICE_NAME, alarmsVOList)).thenReturn(Boolean.FALSE);
        instance.acknowledgeSelectedAlarms();
        assertEquals(instance.getSelectedAlarms().size(), 1);
        System.out.println("test0142_acknowledgeSelectedAlarms completed.");
    }

    /**
     * Test of acknowledgeButtonMessage method, of class AlarmsBean.
     */
    @Test
    public void test15_acknowledgeButtonMessage() {
        System.out.println("test15_acknowledgeButtonMessage");
        String expected = "Acknowledge selected";
        when(userManageBean.getResourceBundleString("label_acknowledge")).thenReturn("Acknowledge selected");
        String actual = instance.acknowledgeButtonMessage();
        assertEquals(expected, actual);
        System.out.println("test015_acknowledgeButtonMessage completed.");
    }

    /**
     * Test of resetPage method, of class AlarmsBean.
     */
    @Test
    public void test021_ResetPage() {
        System.out.println("test021_ResetPage");
        instance.setSelectedFilters();
        instance.setSelectedAlarms(null);
        instance.resetPage();
        assertNotNull(instance.getSelectedAlarms());
        System.out.println("test021_ResetPage completed");
    }

    /**
     * Test of onYearChange method, of class AlarmsBean.
     */
    @Test(expected = UnsupportedOperationException.class)
    public void test03_OnYearChange() {
        System.out.println("test03_OnYearChange");
        instance.onYearChange(true);
        System.out.println("test03_OnYearChange completed");
    }

    /**
     * Test of onRegionChange method, of class AlarmsBean.
     */
    @Test
    public void test041_OnRegionChange() {
        System.out.println("test041_OnRegionChange");
        PowerMockito.mockStatic(PrimeFaces.class);
        PowerMockito.mockStatic(PrimeFaces.Ajax.class);
        PrimeFaces pf = PowerMockito.mock(PrimeFaces.class);
        PrimeFaces.Ajax ax = PowerMockito.mock(PrimeFaces.Ajax.class);
        when(pf.current()).thenReturn(pf);
        when(pf.ajax()).thenReturn(ax);
        alarmsVOList.clear();
        instance.getSelectedAlarms().addAll(alarmsVOList);
        instance.onRegionChange(true);
        assertTrue(instance.getSelectedAlarms().isEmpty());
        System.out.println("test041_OnRegionChange completed");
    }

    /**
     * Test of onCountryChange method, of class AlarmsBean.
     */
    @Test
    public void test051_OnCountryChange() {
        System.out.println("test051_OnCountryChange");
        PowerMockito.mockStatic(PrimeFaces.class);
        PowerMockito.mockStatic(PrimeFaces.Ajax.class);
        PrimeFaces pf = PowerMockito.mock(PrimeFaces.class);
        PrimeFaces.Ajax ax = PowerMockito.mock(PrimeFaces.Ajax.class);
        when(pf.current()).thenReturn(pf);
        when(pf.ajax()).thenReturn(ax);
        ax.addCallbackParam("alarm-form:create-workorder-button", new Object());
        alarmsVOList.clear();
        instance.getSelectedAlarms().addAll(alarmsVOList);
        instance.onCountryChange(true);
        assertTrue(instance.getSelectedAlarms().isEmpty());
        System.out.println("test051_OnCountryChange completed");
    }

    /**
     * Test of onSiteChange method, of class AlarmsBean.
     */
    @Test
    public void test061_OnSiteChange() {
        System.out.println("test061_OnSiteChange");
        PowerMockito.mockStatic(PrimeFaces.class);
        PowerMockito.mockStatic(PrimeFaces.Ajax.class);
        PrimeFaces pf = PowerMockito.mock(PrimeFaces.class);
        PrimeFaces.Ajax ax = PowerMockito.mock(PrimeFaces.Ajax.class);
        when(pf.current()).thenReturn(pf);
        when(pf.ajax()).thenReturn(ax);
        alarmsVOList.clear();
        instance.getSelectedAlarms().addAll(alarmsVOList);
        instance.onSiteChange(true);
        assertTrue(instance.getSelectedAlarms().isEmpty());
        System.out.println("test061_OnSiteChange completed");
    }

    /**
     * Test of onAreaChange method, of class AlarmsBean.
     */
    @Test
    public void test071_OnAreaChange() {
        System.out.println("test071_OnAreaChange");
        PowerMockito.mockStatic(PrimeFaces.class);
        PowerMockito.mockStatic(PrimeFaces.Ajax.class);
        PrimeFaces pf = PowerMockito.mock(PrimeFaces.class);
        PrimeFaces.Ajax ax = PowerMockito.mock(PrimeFaces.Ajax.class);
        when(pf.current()).thenReturn(pf);
        when(pf.ajax()).thenReturn(ax);
        alarmsVOList.clear();
        instance.getSelectedAlarms().addAll(alarmsVOList);
        instance.onAreaChange(true);
        assertTrue(instance.getSelectedAlarms().isEmpty());
        System.out.println("test071_OnAreaChange completed");
    }

    /**
     * Test of onAssetChange method, of class ConfigurationBean.
     */
    @Test
    public void test071_OnAssetChange() {
        System.out.println("test071_OnAssetChange");

        PowerMockito.mockStatic(PrimeFaces.class);
        PowerMockito.mockStatic(PrimeFaces.Ajax.class);
        PrimeFaces pf = PowerMockito.mock(PrimeFaces.class);
        PrimeFaces.Ajax ax = PowerMockito.mock(PrimeFaces.Ajax.class);
        when(pf.current()).thenReturn(pf);
        when(pf.ajax()).thenReturn(ax);
        ax.addCallbackParam("alarm-form:create-workorder-button", new Object());
        alarmsVOList.clear();
        instance.getSelectedAlarms().addAll(alarmsVOList);
        instance.onAssetChange(true);
        assertTrue(instance.getSelectedAlarms().isEmpty());
        System.out.println("test071_OnAssetChange completed");
    }

    /**
     * Test of onPointLocationChange method, of class ConfigurationBean.
     */
    @Test
    public void test08_OnPointLocationChange() {
        System.out.println("test08_OnPointLocationChange");
        PowerMockito.mockStatic(PrimeFaces.class);
        PowerMockito.mockStatic(PrimeFaces.Ajax.class);
        PrimeFaces pf = PowerMockito.mock(PrimeFaces.class);
        PrimeFaces.Ajax ax = PowerMockito.mock(PrimeFaces.Ajax.class);
        when(pf.current()).thenReturn(pf);
        when(pf.ajax()).thenReturn(ax);
        alarmsVOList.clear();
        instance.getSelectedAlarms().addAll(alarmsVOList);
        instance.onPointLocationChange(true);
        assertTrue(instance.getSelectedAlarms().isEmpty());
        System.out.println("test08_OnPointLocationChange completed");
    }

    /**
     * Test of onPointChange method, of class ConfigurationBean.
     */
    @Test
    public void test09_OnPointChange() {
        System.out.println("test09_OnPointChange");
        instance.getSelectedAlarms().addAll(alarmsVOList);
        instance.onPointChange(true);
        assertEquals(instance.getSelectedAlarms().size(), 0);
        System.out.println("test09_OnPointChange completed");
    }

    /**
     * Test of updateUI method, of class AlarmsBean.
     */
    @Test
    public void test10_UpdateUI() {
        System.out.println("test10_UpdateUI");
        PowerMockito.mockStatic(PrimeFaces.class);
        PowerMockito.mockStatic(PrimeFaces.Ajax.class);
        PrimeFaces pf = PowerMockito.mock(PrimeFaces.class);
        PrimeFaces.Ajax ax = PowerMockito.mock(PrimeFaces.Ajax.class);
        when(pf.current()).thenReturn(pf);
        when(pf.ajax()).thenReturn(ax);

        ax.addCallbackParam("alarm-form", new Object());
        ax.addCallbackParam("alarmsFilterFormId", new Object());

        instance.updateUI("content");
        System.out.println("test10_UpdateUI completed");
    }

    /**
     * Test of updateBreadcrumbModel method, of class AlarmsBean.
     */
    @Test
    public void test11_UpdateBreadcrumbModel() {
        System.out.println("test11_UpdateBreadcrumbModel");
        instance.updateBreadcrumbModel();
        DefaultMenuItem item = (DefaultMenuItem) breadcrumbModel.getElements().get(0);
        assertEquals(item.getIcon(), "pi pi-fw pi-bell");
        System.out.println("test11_UpdateBreadcrumbModel completed");
    }

    /**
     * Test of treeContentMenuBuilder method, of class AlarmsBean.
     */
    @Test
    public void test12_TreeContentMenuBuilder() {
        System.out.println("test12_TreeContentMenuBuilder");
        test01_Init();
        PowerMockito.mockStatic(PrimeFaces.class);
        PowerMockito.mockStatic(PrimeFaces.Ajax.class);
        PrimeFaces pf = PowerMockito.mock(PrimeFaces.class);
        PrimeFaces.Ajax ax = PowerMockito.mock(PrimeFaces.Ajax.class);
        when(pf.current()).thenReturn(pf);
        when(pf.ajax()).thenReturn(ax);
        ax.addCallbackParam("leftTreeFormId", new Object());

        instance.treeContentMenuBuilder(ROOT, "", null, null);
        TreeNode tn = PowerMockito.mock(TreeNode.class);
        instance.treeContentMenuBuilder("site", "", tn, null);
        System.out.println("test12_TreeContentMenuBuilder completed");
    }

    /**
     * Test of onNodeExpand method, of class AlarmsBean.
     */
    @Test
    public void test13_OnNodeExpand() {
        System.out.println("test13_OnNodeExpand");

        NodeExpandEvent nee = PowerMockito.mock(NodeExpandEvent.class);
        TreeNode tn = PowerMockito.mock(TreeNode.class);

        when(nee.getTreeNode()).thenReturn(tn);
        when(tn.getType()).thenReturn("region");
        when(tn.getData()).thenReturn("Region1");

        List<TreeNode> tnList = new ArrayList<>();
        tnList.add(PowerMockito.mock(TreeNode.class));
        when(tn.getChildren()).thenReturn(tnList);

        test01_Init();

        instance.onNodeExpand(nee);
        assertEquals(nee.getTreeNode().getChildren().size(), 1);

        System.out.println("test13_OnNodeExpand completed");
    }

    /**
     * Test of onNodeCollapse method, of class AlarmsBean.
     */
    @Test
    public void test14_OnNodeCollapse() {
        System.out.println("test14_OnNodeCollapse");
        NodeCollapseEvent nce = PowerMockito.mock(NodeCollapseEvent.class);
        TreeNode tn = PowerMockito.mock(TreeNode.class);

        when(nce.getTreeNode()).thenReturn(tn);
        when(tn.getType()).thenReturn("region");
        when(tn.getData()).thenReturn("Region1");

        List<TreeNode> tnList = new ArrayList<>();
        tnList.add(PowerMockito.mock(TreeNode.class));
        when(tn.getChildren()).thenReturn(tnList);

        instance.onNodeCollapse(nce);
        //assertEquals(nce.getTreeNode().getChildren().size(), 0);

        System.out.println("test14_OnNodeCollapse completed");
    }

    /**
     * Test of onNodeSelect method, of class AlarmsBean.
     */
    @Test
    public void test15_OnNodeSelect() {
        System.out.println("test15_OnNodeSelect");
        NodeSelectEvent nse = PowerMockito.mock(NodeSelectEvent.class);
        TreeNode tn = PowerMockito.mock(TreeNode.class);

        when(nse.getTreeNode()).thenReturn(tn);
        when(tn.getType()).thenReturn("region");
        when(tn.getData()).thenReturn("region");

        List<TreeNode> tnList = new ArrayList<>();
        tnList.add(PowerMockito.mock(TreeNode.class));
        when(tn.getChildren()).thenReturn(tnList);
        when(tn.getParent()).thenReturn(tn);

        instance.onNodeSelect(nse);
        System.out.println("test15_OnNodeSelect completed");
    }

    /**
     * Test of dateFormatter method, of class AlarmsBean.
     */
    @Test
    public void test15_DateFormatter() {
        System.out.println("test15_DateFormatter");
        Instant instant;
        when(userManageBean.getZoneId()).thenReturn(null);
        instant = LocalDateTime.now().atZone(ZoneOffset.UTC).toInstant();
        instance.dateFormatter(instant);
        assertEquals(LocalDateTime.ofInstant(instant, ZoneOffset.UTC).format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")), instance.dateFormatter(instant));
        System.out.println("test15_DateFormatter completed");
    }

    /**
     * Test of setRegionCountrySiteMap method, of class AlarmsBean.
     */
    @Test
    public void test16_SetRegionCountrySiteMap() {
        System.out.println("test16_SetRegionCountrySiteMap");
        List<SiteVO> list = new ArrayList();
        SiteVO s1 = new SiteVO();
        s1.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        s1.setSiteName("Test Site 1");
        s1.setRegion("Region1");
        s1.setCountry("Country1");
        list.add(s1);
        when(serviceRegistrarClient.getServiceHostURL(CONFIGURATION_SERVICE_NAME)).thenReturn(CONFIGURATION_SERVICE_NAME);
        when(siteClient.getSiteRegionCountryByCustomer(CONFIGURATION_SERVICE_NAME, customerAccount)).thenReturn(list);
        instance.setRegionCountrySiteMap();
        System.out.println("test16_SetRegionCountrySiteMap completed");
    }

    /**
     * Test of updateAlarmsVOListByAreaId method, of class AlarmsBean.
     */
    @Test
    public void test17_UpdateAlarmsVOListByAreaId() {
        System.out.println("testUpdateAlarmsVOListByAreaId");

        List<AlarmsVO> list = new ArrayList<>();
        AlarmsVO avo = new AlarmsVO();
        avo.setCustomerAccount("CUST_ACC");
        avo.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        avo.setSiteName("Test Site 1");
        avo.setAreaId(UUID.randomUUID());
        avo.setAssetId(UUID.randomUUID());
        avo.setPointLocationId(UUID.randomUUID());
        avo.setPointId(UUID.randomUUID());
        avo.setParamName("PARAM_NAME");
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ssZ");
        avo.setSampleTimestamp(df.format(new Date()));
        list.add(avo);
        instance.setAlarmType(null);
        when(serviceRegistrarClient.getServiceHostURL(ALARM_SERVICE_NAME)).thenReturn(ALARM_SERVICE_NAME);
        when(alarmsClient.getAlarmsByCustomerSiteArea(serviceRegistrarClient.getServiceHostURL(ALARM_SERVICE_NAME), customerAccount, (UUID) instance.getSelectedFilters().getSite().getValue(), (UUID) instance.getSelectedFilters().getArea().getValue())).thenReturn(list);

        instance.updateAlarmsVOListByAreaId();
        assertTrue(!instance.getAlarmsVOList().isEmpty());
        //assertEquals(instance.getSelectedFilters().getArea().getLabel(), "area");
        //assertEquals(list.get(0).getAreaName(), "area");
    }

    /**
     * Test of updateAlarmsVOListByAreaId method, of class AlarmsBean.
     */
    @Test
    public void test18_UpdateAlarmsVOListByAreaId() {
        System.out.println("testUpdateAlarmsVOListByAreaId");

       
        List<AlarmsVO> list = new ArrayList<>();
        AlarmsVO avo = new AlarmsVO();
        avo.setCustomerAccount("CUST_ACC");
        avo.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        avo.setSiteName("Test Site 1");
        avo.setAreaId(UUID.randomUUID());
        avo.setAssetId(UUID.randomUUID());
        avo.setPointLocationId(UUID.randomUUID());
        avo.setPointId(UUID.randomUUID());
        avo.setParamName("PARAM_NAME");
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ssZ");
        avo.setSampleTimestamp(df.format(new Date()));
        avo.setSensorType("Acceleration");
        list.add(avo);
        instance.setAlarmType("alerts");
         
        when(serviceRegistrarClient.getServiceHostURL(ALARM_SERVICE_NAME)).thenReturn(ALARM_SERVICE_NAME);
        when(alarmsClient.getAlarmsByCustomerSiteArea(serviceRegistrarClient.getServiceHostURL(ALARM_SERVICE_NAME), customerAccount, (UUID) instance.getSelectedFilters().getSite().getValue(), (UUID) instance.getSelectedFilters().getArea().getValue())).thenReturn(list);
       
        instance.updateAlarmsVOListByAreaId();
        assertTrue(!instance.getAlarmsVOList().isEmpty());
    }

    /**
     * Test of updateAlarmsVOListByAreaId method, of class AlarmsBean.
     */
    @Test
    public void test19_UpdateAlarmsVOListByAreaId() {
        System.out.println("testUpdateAlarmsVOListByAreaId");

        List<AlarmsVO> list = new ArrayList<>();
        AlarmsVO avo = new AlarmsVO();
        avo.setCustomerAccount("CUST_ACC");
        avo.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        avo.setSiteName("Test Site 1");
        avo.setAreaId(UUID.randomUUID());
        avo.setAssetId(UUID.randomUUID());
        avo.setPointLocationId(UUID.randomUUID());
        avo.setPointId(UUID.randomUUID());
        avo.setParamName("PARAM_NAME");
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ssZ");
        avo.setSampleTimestamp(df.format(new Date()));
        avo.setSensorType("Acceleration");
        list.add(avo);
        instance.setAlarmType("faults");
        when(serviceRegistrarClient.getServiceHostURL(ALARM_SERVICE_NAME)).thenReturn(ALARM_SERVICE_NAME);
        when(alarmsClient.getAlarmsByCustomerSiteArea(serviceRegistrarClient.getServiceHostURL(ALARM_SERVICE_NAME), customerAccount, (UUID) instance.getSelectedFilters().getSite().getValue(), (UUID) instance.getSelectedFilters().getArea().getValue())).thenReturn(list);

        instance.updateAlarmsVOListByAreaId();
        assertTrue(!instance.getAlarmsVOList().isEmpty());
    }

    /**
     * Test of updateAlarmsVOListByAreaId method, of class AlarmsBean.
     */
    @Test
    public void test21_UpdateAlarmsVOListByAreaId() {
        System.out.println("testUpdateAlarmsVOListByAreaId");

        List<AlarmsVO> list = new ArrayList<>();
        AlarmsVO avo = new AlarmsVO();
        avo.setCustomerAccount("CUST_ACC");
        avo.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        avo.setSiteName("Test Site 1");
        avo.setAreaId(UUID.randomUUID());
        avo.setAssetId(UUID.randomUUID());
        avo.setPointLocationId(UUID.randomUUID());
        avo.setPointId(UUID.randomUUID());
        avo.setParamName("PARAM_NAME");
        avo.setAlarmType("LOW ALERT");
        avo.setSensorType("Temperature");

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ssZ");
        avo.setSampleTimestamp(df.format(new Date()));
        list.add(avo);
        instance.setAlarmType("alerts");
        when(serviceRegistrarClient.getServiceHostURL(ALARM_SERVICE_NAME)).thenReturn(ALARM_SERVICE_NAME);
        when(alarmsClient.getAlarmsByCustomerSiteArea(serviceRegistrarClient.getServiceHostURL(ALARM_SERVICE_NAME), customerAccount, (UUID) instance.getSelectedFilters().getSite().getValue(), (UUID) instance.getSelectedFilters().getArea().getValue())).thenReturn(list);

        instance.updateAlarmsVOListByAreaId();
        assertTrue(!instance.getAlarmsVOList().isEmpty());
    }

    
    /**
     * Test of updateAlarmsVOListByAreaId method, of class AlarmsBean.
     */
    @Test
    public void test20_UpdateAlarmsVOListByAreaId() {
        System.out.println("testUpdateAlarmsVOListByAreaId");

        List<AlarmsVO> list = new ArrayList<>();
        AlarmsVO avo = new AlarmsVO();
        avo.setCustomerAccount("CUST_ACC");
        avo.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        avo.setSiteName("Test Site 1");
        avo.setAreaId(UUID.randomUUID());
        avo.setAssetId(UUID.randomUUID());
        avo.setPointLocationId(UUID.randomUUID());
        avo.setPointId(UUID.randomUUID());
        avo.setParamName("PARAM_NAME");
        avo.setAlarmType("LOW FAULT");
        avo.setSensorType("Temperature");

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ssZ");
        avo.setSampleTimestamp(df.format(new Date()));
        list.add(avo);
        instance.setAlarmType("faults");
        when(serviceRegistrarClient.getServiceHostURL(ALARM_SERVICE_NAME)).thenReturn(ALARM_SERVICE_NAME);
        when(alarmsClient.getAlarmsByCustomerSiteArea(serviceRegistrarClient.getServiceHostURL(ALARM_SERVICE_NAME), customerAccount, (UUID) instance.getSelectedFilters().getSite().getValue(), (UUID) instance.getSelectedFilters().getArea().getValue())).thenReturn(list);

        instance.updateAlarmsVOListByAreaId();
        assertTrue(!instance.getAlarmsVOList().isEmpty());
    }

    /**
     * Test of updateAlarmsVOListByAssetId method, of class AlarmsBean.
     */
    @Test
    public void testUpdateAlarmsVOListByAssetId() {
        System.out.println("testUpdateAlarmsVOListByAssetId");

        List<AlarmsVO> list = new ArrayList<>();
        AlarmsVO avo = new AlarmsVO();
        avo.setCustomerAccount("CUST_ACC");
        avo.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        avo.setSiteName("Test Site 1");
        avo.setAreaId(UUID.randomUUID());
        avo.setAssetId(UUID.randomUUID());
        avo.setPointLocationId(UUID.randomUUID());
        avo.setPointId(UUID.randomUUID());
        avo.setParamName("PARAM_NAME");
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ssZ");
        avo.setSampleTimestamp(df.format(new Date()));
        list.add(avo);
        when(serviceRegistrarClient.getServiceHostURL(ALARM_SERVICE_NAME)).thenReturn(ALARM_SERVICE_NAME);
        when(alarmsClient.getAlarmsByCustomerSiteAreaAsset(ServiceRegistrarClient.getInstance().getServiceHostURL(ALARM_SERVICE_NAME), customerAccount, (UUID) instance.getSelectedFilters().getSite().getValue(), (UUID) instance.getSelectedFilters().getArea().getValue(), (UUID) instance.getSelectedFilters().getAsset().getValue())).thenReturn(list);

        instance.updateAlarmsVOListByAssetId();
        //assertEquals(instance.getSelectedFilters().getAsset().getLabel(), "asset");
        //assertEquals(list.get(0).getAssetName(), "asset");
    }

    /**
     * Test of updateAlarmsVOListByPointId method, of class AlarmsBean.
     */
    @Test
    public void testUpdateAlarmsVOListByPointId() {
        System.out.println("testUpdateAlarmsVOListByPointId");

        List<AlarmsVO> list = new ArrayList<>();
        AlarmsVO avo = new AlarmsVO();
        avo.setCustomerAccount("CUST_ACC");
        avo.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        avo.setSiteName("Test Site 1");
        avo.setAreaId(UUID.randomUUID());
        avo.setAssetId(UUID.randomUUID());
        avo.setPointLocationId(UUID.randomUUID());
        avo.setPointId(UUID.randomUUID());
        avo.setParamName("PARAM_NAME");
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ssZ");
        avo.setSampleTimestamp(df.format(new Date()));
        list.add(avo);
        when(serviceRegistrarClient.getServiceHostURL(ALARM_SERVICE_NAME)).thenReturn(ALARM_SERVICE_NAME);
        when(alarmsClient.getAlarmsByCustomerSiteAreaAssetPointLocationPoint(ServiceRegistrarClient.getInstance().getServiceHostURL(ALARM_SERVICE_NAME), customerAccount, (UUID) instance.getSelectedFilters().getSite().getValue(), (UUID) instance.getSelectedFilters().getArea().getValue(), (UUID) instance.getSelectedFilters().getAsset().getValue(), (UUID) instance.getSelectedFilters().getPointLocation().getValue(), (UUID) instance.getSelectedFilters().getPoint().getValue())).thenReturn(list);

        instance.updateAlarmsVOListByPointId();
        //assertEquals(instance.getSelectedFilters().getPoint().getLabel(), "point");
        //assertEquals(list.get(0).getPointName(), "point");
    }

    /**
     * Test of viewAlarmHistory method, of class AlarmsBean.
     */
    // @Test
    public void testViewAlarmHistory() {
        System.out.println("testViewAlarmHistory");
        int currentYear = LocalDate.now().getYear();
        List<AlarmsVO> list = new ArrayList<>();
        AlarmsVO avo = new AlarmsVO();
        avo.setCustomerAccount("CUST_ACC");
        avo.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        avo.setSiteName("Test Site 1");
        avo.setAreaId(UUID.randomUUID());
        avo.setAssetId(UUID.randomUUID());
        avo.setPointLocationId(UUID.randomUUID());
        avo.setPointId(UUID.randomUUID());
        avo.setParamName("PARAM_NAME");
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ssZ");
        avo.setSampleTimestamp(df.format(new Date()));
        list.add(avo);
        when(serviceRegistrarClient.getServiceHostURL(ALARM_SERVICE_NAME)).thenReturn(ALARM_SERVICE_NAME);
        when(alarmsClient.getAlarmsHistory(ServiceRegistrarClient.getInstance().getServiceHostURL(ALARM_SERVICE_NAME), avo.getCustomerAccount(), avo.getSiteId(), currentYear, avo.getAreaId(), avo.getAssetId(), avo.getPointLocationId(), avo.getPointId(), avo.getParamName())).thenReturn(list);
        // instance.viewAlarmHistory(avo);
        // assertEquals(instance.getTimeLineModel().getEvents().size(), 1);
    }

    @Test
    public void testOnRowSelect() {
        System.out.println("testOnRowSelect");
        SelectEvent<AlarmsVO> event = PowerMockito.mock(SelectEvent.class);
        when(event.getObject()).thenReturn(new AlarmsVO());
        instance.onRowSelect(event);
        assertEquals(instance.getSelectedAlarms().size(), 1);
    }

    @Test
    public void testOnRowUnselect() {
        System.out.println("testOnRowUnselect");

        AlarmsVO avo = new AlarmsVO();
        instance.getSelectedAlarms().add(avo);

        UnselectEvent<AlarmsVO> event = PowerMockito.mock(UnselectEvent.class);
        when(event.getObject()).thenReturn(avo);
        instance.onRowUnselect(event);
        assertTrue(instance.getSelectedAlarms().isEmpty());
    }

    @Test
    public void testDeleteButtonMessage() {
        System.out.println("testDeleteButtonMessage");
        instance.getSelectedAlarms().add(new AlarmsVO());
        instance.getSelectedAlarms().add(new AlarmsVO());
        String expected = "Delete selected (2)";
        when(userManageBean.getResourceBundleString("label_delete_button_message")).thenReturn("Delete selected");
        String actual = instance.deleteButtonMessage();
        assertEquals(expected, actual);
    }

    @Test
    public void testIsSelectedAcknowledgedAlarms() {
        System.out.println("testIsSelectedAcknowledgedAlarms");
        AlarmsVO avo = new AlarmsVO();
        avo.setAcknowledged(true);
        instance.getSelectedAlarms().add(avo);
        instance.getSelectedAlarms().add(new AlarmsVO());
        // assertEquals(instance.isSelectedAcknowledgedAlarms(), true);
        avo.setAcknowledged(false);
        // assertEquals(instance.isSelectedAcknowledgedAlarms(), false);
    }

    @Test
    public void testInfoOfSelectedAlarm() {
        System.out.println("testIsSelectedAcknowledgedAlarms");
        AlarmsVO avo = new AlarmsVO();
        avo.setCustomerAccount("77777");
        avo.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        avo.setSiteName("Test Site 1");
        avo.setAreaId(UUID.randomUUID());
        avo.setAssetId(UUID.randomUUID());
        avo.setPointLocationId(UUID.randomUUID());
        avo.setPointId(UUID.randomUUID());
        avo.setApSetId(UUID.randomUUID());
        avo.setParamName("PARAM_NAME");
        PointVO pvo = new PointVO();
        pvo.setApSetId(avo.getApSetId());
        pvo.setAlSetId(UUID.randomUUID());
        List<PointVO> pointVOList = new ArrayList<>();
        pointVOList.add(pvo);
        when(serviceRegistrarClient.getServiceHostURL(CONFIGURATION_SERVICE_NAME)).thenReturn(CONFIGURATION_SERVICE_NAME);
        when(pointClient.getApALByPoints_bySiteAreaAssetPointLocationPointApSet(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), customerAccount, avo.getSiteId(), avo.getAreaId(), avo.getAssetId(), avo.getPointLocationId(), avo.getPointId(), avo.getApSetId())).thenReturn(pointVOList);
        instance.infoOfSelectedAlarm();
        //assertEquals(pvo.getSiteName(), avo.getSiteName());
    }

    @Test
    public void testClearRowSelection() {
        System.out.println("testClearRowSelection");
        instance.getSelectedAlarms().addAll(alarmsVOList);
        PowerMockito.mockStatic(PrimeFaces.class);
        PowerMockito.mockStatic(PrimeFaces.Ajax.class);
        PrimeFaces pf = PowerMockito.mock(PrimeFaces.class);
        PrimeFaces.Ajax ax = PowerMockito.mock(PrimeFaces.Ajax.class);
        when(pf.current()).thenReturn(pf);
        when(pf.ajax()).thenReturn(ax);
        ax.addCallbackParam("dtAlarms", new Object());
        ax.addCallbackParam("alarm-form", new Object());
        ax.addCallbackParam("alarm-form:dt-alarms", new Object());
        ax.addCallbackParam("alarm-form:delete-alarm-button", new Object());
        ax.addCallbackParam("alarm-form:acknowledge-alarm-button", new Object());
        instance.clearRowSelection();
        assertTrue(instance.getSelectedAlarms().isEmpty());
    }

}
