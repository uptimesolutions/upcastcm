/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.bean;

import com.uptime.upcastcm.utils.vo.ChannelVO;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.junit.runners.MethodSorters;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

/**
 *
 * @author twilcox
 */
@RunWith(PowerMockRunner.class)
@PowerMockIgnore("jdk.internal.reflect.*")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@PowerMockRunnerDelegate(JUnit4.class)
public class DeviceManagementInfoRowBeanTest {
    
    private final DeviceManagementInfoRowBean instance;
    
    public DeviceManagementInfoRowBeanTest() {
        instance = new DeviceManagementInfoRowBean();
    }
    
    @Before
    public void setUp() {
        ChannelVO channelVO;
        
        channelVO = new ChannelVO();
        channelVO.setCustomerAccount("FEDEX EXPRESS");
        
        instance.presetFields(channelVO);
    }

    /**
     * Test of resetPage method, of class DeviceManagementInfoRowBean.
     */
    @Test
    public void test0_ResetPage() {
        System.out.println("resetPage");
        
        instance.resetPage();
        assertNull(instance.getChannelVO());
        
        System.out.println("resetPage completed");
    }

    /**
     * Test of presetFields method, of class DeviceManagementInfoRowBean.
     */
    @Test
    public void test1_PresetFields() {
        System.out.println("presetFields");
        
        ChannelVO channelVO;
        
        channelVO = new ChannelVO();
        channelVO.setCustomerAccount("UPTIME");
        
        instance.presetFields(channelVO);
        assertEquals("UPTIME", instance.getChannelVO().getCustomerAccount());
        
        System.out.println("presetFields completed");
    }

    /**
     * Test of getChannelVO method, of class DeviceManagementInfoRowBean.
     */
    @Test
    public void test2_GetChannelVO() {
        System.out.println("getChannelVO");
        
        assertEquals("FEDEX EXPRESS", instance.getChannelVO().getCustomerAccount());
        
        System.out.println("getChannelVO completed");
    }
}
