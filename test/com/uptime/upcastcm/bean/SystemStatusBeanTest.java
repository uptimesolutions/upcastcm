/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.AreaClient;
import com.uptime.upcastcm.http.client.SiteClient;
import com.uptime.upcastcm.utils.vo.AirbaseStatusVO;
import com.uptime.upcastcm.utils.vo.EchoDiagnosticsVO;
import com.uptime.upcastcm.utils.vo.MistDiagnosticsVO;
import com.uptime.upcastcm.utils.vo.UserPreferencesVO;
import com.uptime.upcastcm.utils.vo.UserSitesVO;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import static org.powermock.api.mockito.PowerMockito.when;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.primefaces.PrimeFaces;
import org.primefaces.event.ItemSelectEvent;
import org.primefaces.model.charts.donut.DonutChartDataSet;

/**
 *
 * @author gopal
 */
@RunWith(PowerMockRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@PowerMockIgnore("javax.net.ssl.*")
@PrepareForTest({FacesContext.class, ExternalContext.class, UserManageBean.class, ServiceRegistrarClient.class, NavigationBean.class, SiteClient.class, AreaClient.class, PrimeFaces.class, PrimeFaces.Ajax.class})
public class SystemStatusBeanTest {

    @Mock
    private final UserManageBean userManageBean;
    @Mock
    NavigationBean navigationBean;
    @Mock
    ServiceRegistrarClient sc;
    @Mock
    private final FacesContext facesContext;
    @Mock
    private final ExternalContext externalContext;
    private final SystemStatusBean instance;
    @Mock
    HttpServletRequest request;
    @Mock
    HttpSession session;
    Map<String, Object> sessionMap;
    private final String DATE_TIME_ZONE_ID = "America/New_York";

    public SystemStatusBeanTest() throws Exception {

        PowerMockito.mockStatic(FacesContext.class);
        PowerMockito.mockStatic(ServiceRegistrarClient.class);
        PowerMockito.mockStatic(SiteClient.class);
        PowerMockito.mockStatic(AreaClient.class);

        facesContext = PowerMockito.mock(FacesContext.class);
        externalContext = PowerMockito.mock(ExternalContext.class);
        request = PowerMockito.mock(HttpServletRequest.class);
        session = PowerMockito.mock(HttpSession.class);
        navigationBean = PowerMockito.mock(NavigationBean.class);
        userManageBean = PowerMockito.mock(UserManageBean.class);

        sc = PowerMockito.mock(ServiceRegistrarClient.class);

        sessionMap = new HashMap();
        sessionMap.put("navigationBean", navigationBean);
        sessionMap.put("userManageBean", userManageBean);
        session.setAttribute("navigationBean", navigationBean);
        session.setAttribute("userManageBean", userManageBean);

        when(ServiceRegistrarClient.getInstance()).thenReturn(sc);
        when(FacesContext.getCurrentInstance()).thenReturn(facesContext);
        when(facesContext.getExternalContext()).thenReturn(externalContext);
        when(externalContext.getSessionMap()).thenReturn(sessionMap);
        when(facesContext.getExternalContext().getRequest()).thenReturn(request);
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("navigationBean")).thenReturn(navigationBean);
        when(session.getAttribute("userManageBean")).thenReturn(userManageBean);

        PowerMockito.mockStatic(PrimeFaces.class);
        PowerMockito.mockStatic(PrimeFaces.Ajax.class);
        PrimeFaces pf = PowerMockito.mock(PrimeFaces.class);
        PrimeFaces.Ajax ax = PowerMockito.mock(PrimeFaces.Ajax.class);
        when(pf.current()).thenReturn(pf);
        when(pf.ajax()).thenReturn(ax);

        UserPreferencesVO userVO = new UserPreferencesVO();
        userVO.setDefaultSite(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        userVO.setCustomerAccount("77777");
        when(session.getAttribute("userVO")).thenReturn(userVO);
        UserSitesVO usvo = new UserSitesVO();
        usvo.setCustomerAccount("77777");
        usvo.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        when(userManageBean.getCurrentSite()).thenReturn(usvo);
        when(userManageBean.getZoneId()).thenReturn(ZoneId.of(DATE_TIME_ZONE_ID));
        this.instance = new SystemStatusBean();
        Whitebox.setInternalState(instance, "today", new DateTime().withZone(DateTimeZone.forID(DATE_TIME_ZONE_ID)));

        List<MistDiagnosticsVO> mistDiagnosticsList = new ArrayList();
        MistDiagnosticsVO MistDiagnosticsVO = new MistDiagnosticsVO();
        MistDiagnosticsVO.setCustomerAccount("FEDEX EXPRESS");
        MistDiagnosticsVO.setLastSample(Instant.EPOCH);
        MistDiagnosticsVO.setLastSampleWithSpecfZone(MistDiagnosticsVO.getLastSample().atZone(ZoneId.of(DATE_TIME_ZONE_ID)).truncatedTo(ChronoUnit.SECONDS));
        mistDiagnosticsList.add(MistDiagnosticsVO);
//        Whitebox.setInternalState(instance, "mistDiagnosticsList", mistDiagnosticsList);
//        Whitebox.setInternalState(instance, "lowBatteryMistDiagnosticsList", mistDiagnosticsList);

        List<AirbaseStatusVO> airbaseStatusVOList = new ArrayList();
        AirbaseStatusVO airbaseStatusVO = new AirbaseStatusVO();
        airbaseStatusVO.setCustomerId("FEDEX EXPRESS");
        airbaseStatusVO.setBaseStationId("BC0001FA");
        airbaseStatusVO.setLastSampleReceived(Instant.EPOCH);
        airbaseStatusVO.setLastSampleReceivedWithSpecfZone(airbaseStatusVO.getLastSampleReceived().atZone(ZoneId.of(DATE_TIME_ZONE_ID)).truncatedTo(ChronoUnit.SECONDS));
        airbaseStatusVOList.add(airbaseStatusVO);
        Whitebox.setInternalState(instance, "airbaseStatusVOList", airbaseStatusVOList);

        List<EchoDiagnosticsVO> echoDiagnosticsList = new ArrayList();
        EchoDiagnosticsVO echoDiagnosticsVo = new EchoDiagnosticsVO();
        echoDiagnosticsVo.setCustomerAccount("FEDEX EXPRESS");
        echoDiagnosticsVo.setLastUpdate(Instant.EPOCH);
        echoDiagnosticsVo.setLastUpdateWithSpecfZone(echoDiagnosticsVo.getLastUpdate().atZone(ZoneId.of(DATE_TIME_ZONE_ID)).truncatedTo(ChronoUnit.SECONDS));
        echoDiagnosticsList.add(echoDiagnosticsVo);
        Whitebox.setInternalState(instance, "echoDiagnosticsList", echoDiagnosticsList);

        //    Map<String, MistDiagnosticsVO> deviceLastSampleMap = new HashMap();
        //    Whitebox.setInternalState(instance, "deviceLastSampleMap", deviceLastSampleMap);
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {

    }

    @After
    public void tearDown() {
    }

    /**
     * Test of init method, of class SystemStatusBean.
     */
    @Test
    public void testInit() {
        System.out.println("init");
        instance.init();
    }

    /**
     * Test of createAirbaseStatusDonutModel method, of class SystemStatusBean.
     */
    @Test
    public void testCreateAirbaseStatusDonutModel() {
        System.out.println("createAirbaseStatusDonutModel");
        instance.createAirbaseStatusDonutModel();
        DonutChartDataSet obj = (DonutChartDataSet) instance.getAirbaseStatusDonutModel().getData().getDataSet().get(0);
        assertEquals(1, obj.getData().get(2));
    }

    /**
     * Test of createEchobaseStatusDonutModel method, of class SystemStatusBean.
     */
    @Test
    public void testCreateEchobaseStatusDonutModel() {
        System.out.println("createEchobaseStatusDonutModel");
        instance.createEchobaseStatusDonutModel();
        DonutChartDataSet obj = (DonutChartDataSet) instance.getEchobaseStatusDonutModel().getData().getDataSet().get(0);
        assertEquals(1, obj.getData().get(2));
    }

    /**
     * Test of createSensorStatusDonutModel method, of class SystemStatusBean.
     */
    @Test
    public void testCreateSensorStatusDonutModel() {
        System.out.println("createSensorStatusDonutModel");
        instance.createSensorStatusDonutModel();
        DonutChartDataSet obj = (DonutChartDataSet) instance.getSensorStatusDonutModel().getData().getDataSet().get(0);
        assertEquals(0, obj.getData().get(2));
    }

    /**
     * Test of handleAirbaseDonutClick method, of class SystemStatusBean.
     */
    @Test
    public void testHandleAirbaseDonutClick() {
        System.out.println("handleAirbaseDonutClick");
        ItemSelectEvent ise = PowerMockito.mock(ItemSelectEvent.class);
        when(ise.getItemIndex()).thenReturn(2);
        instance.handleAirbaseDonutClick(ise);
        assertEquals(1, instance.getFilteredAirbaseStatusVOList().size());
    }

    /**
     * Test of handleEchobaseDonutClick method, of class SystemStatusBean.
     */
    @Test
    public void testHandleEchobaseDonutClick() {
        System.out.println("handleEchobaseDonutClick");
        ItemSelectEvent ise = PowerMockito.mock(ItemSelectEvent.class);
        when(ise.getItemIndex()).thenReturn(2);
        instance.handleEchobaseDonutClick(ise);
        assertEquals(1, instance.getFilteredEchoDiagnosticsList().size());
    }

    /**
     * Test of handleSensorStatusDonutClick method, of class SystemStatusBean.
     */
    @Test
    public void testHandleSensorStatusDonutClick() {
        System.out.println("handleSensorStatusDonutClick");
        ItemSelectEvent ise = PowerMockito.mock(ItemSelectEvent.class);
        when(ise.getItemIndex()).thenReturn(2);
        instance.handleSensorStatusDonutClick(ise);
        assertEquals(0, instance.getFilteredDeviceStatusList().size());
    }

    /**
     * Test of handleLowBetteryClick method, of class SystemStatusBean.
     */
    @Test
    public void testHandleLowBetteryClick() {
        System.out.println("handleLowBetteryClick");
        instance.handleLowBetteryClick();
        assertEquals(true, instance.isRenderLowBattery());
    }

    /**
     * Test of handleDisabledSensorClick method, of class SystemStatusBean.
     */
    @Test
    public void testhandleDisabledSensorClick() {
        System.out.println("handleDisabledSensorClick");
        instance.handleDisabledSensorClick();
        assertEquals(true, instance.isRenderDisabledSensor());
    }

    /**
     * Test of compareDates method, of class SystemStatusBean.
     */
    @Test
    public void testCompareDates_3args() {
        System.out.println("compareDates");
        DateTime today = new DateTime().withZone(DateTimeZone.forID(DATE_TIME_ZONE_ID));
        DateTime yesterday = today.minusDays(2);
        DateTime sampleTime = today.minusDays(1);
        boolean expResult = true;
        instance.init();
        boolean result = instance.compareDates(sampleTime, today, yesterday);
        assertEquals(expResult, result);
    }

    /**
     * Test of compareDates method, of class SystemStatusBean.
     */
    @Test
    public void testCompareDates_DateTime() {
        System.out.println("compareDates");
        boolean expResult = true;
        DateTime today = new DateTime().withZone(DateTimeZone.forID(DATE_TIME_ZONE_ID));
        DateTime yesterday = today.minusDays(1);
        instance.init();
        boolean result = instance.compareDates(yesterday, today, false);
        assertEquals(expResult, result);
    }

    /**
     * Test of toDateTime method, of class SystemStatusBean.
     */
    @Test
    public void testToDateTime() {
        System.out.println("toDateTime");
        ZonedDateTime input = Instant.now().atZone(ZoneId.of(DATE_TIME_ZONE_ID)).truncatedTo(ChronoUnit.SECONDS);
        instance.init();
        DateTime result = instance.toDateTime(input);
        assertEquals(input.getMinute(), result.getMinuteOfHour());
    }

}
