/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.utils.vo.UserSitesVO;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.junit.runners.MethodSorters;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import static org.powermock.api.mockito.PowerMockito.when;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;
import org.powermock.reflect.Whitebox;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author twilcox
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(PowerMockRunner.class)
@PowerMockIgnore("jdk.internal.reflect.*")
@PowerMockRunnerDelegate(JUnit4.class)
@PrepareForTest({FacesContext.class, ExternalContext.class, ServiceRegistrarClient.class, NavigationBean.class, UserManageBean.class})
public class ImportConfigUploadBeanTest {

    @Mock
    ServiceRegistrarClient serviceRegistrarClient;
    @Mock
    FacesContext facesContext;
    @Mock
    ExternalContext externalContext;
    @Mock
    NavigationBean navigationBean;
    @Mock
    UserManageBean userManageBean;
    @Mock
    HttpSession session;

    private static ImportConfigUploadBean instance;

    Map<String, Object> sessionMap;

    public ImportConfigUploadBeanTest() {
        PowerMockito.mockStatic(FacesContext.class);
        PowerMockito.mockStatic(ServiceRegistrarClient.class);

        facesContext = PowerMockito.mock(FacesContext.class);
        externalContext = PowerMockito.mock(ExternalContext.class);
        navigationBean = PowerMockito.mock(NavigationBean.class);
        userManageBean = PowerMockito.mock(UserManageBean.class);
        serviceRegistrarClient = PowerMockito.mock(ServiceRegistrarClient.class);
        session = PowerMockito.mock(HttpSession.class);

        sessionMap = new HashMap();
        sessionMap.put("navigationBean", navigationBean);
        sessionMap.put("userManageBean", userManageBean);

        session.setAttribute("navigationBean", navigationBean);
        session.setAttribute("userManageBean", userManageBean);

        when(FacesContext.getCurrentInstance()).thenReturn(facesContext);
        when(facesContext.getExternalContext()).thenReturn(externalContext);
        when(externalContext.getSessionMap()).thenReturn(sessionMap);
        when(session.getAttribute("navigationBean")).thenReturn(navigationBean);
        when(session.getAttribute("userManageBean")).thenReturn(userManageBean);
        when(ServiceRegistrarClient.getInstance()).thenReturn(serviceRegistrarClient);

        UserSitesVO usvo = new UserSitesVO();
        usvo.setCustomerAccount("77777");
        when(userManageBean.getCurrentSite()).thenReturn(usvo);
        instance = new ImportConfigUploadBean();
    }

    @BeforeClass
    public static void setUpClass() {

    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of downloadCsvTemplate method, of class ImportConfigUploadBean.
     */
    @Test
    public void test0_DownloadCsvTemplate() {
        Whitebox.setInternalState(instance, "uploadedFileName", "template.csv");
        StreamedContent content = instance.downloadCsvTemplate();
        // Assert that the file name is correct
        assertEquals("Bulk_Upload_Template.csv", content.getName());
        InputStream inputStream = content.getStream();
        String fileContent = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8)).lines().collect(Collectors.joining("\n"));
        String expectedContent = "Area_Name,Asset_Name,Asset_Template_Name";
        assertEquals(expectedContent, fileContent);
    }

    /**
     * Test of downloadCsv method, of class ImportConfigUploadBean.
     */
    @Test
    public void test1_DownloadCsv() {
        Whitebox.setInternalState(instance, "uploadedFileName", "template.csv");
        List<String[]> failedRows = new ArrayList();
        String[] row = new String[3];
        row[0] = "Test Area1";
        row[1] = "Test @$$et";
        row[2] = "Test asset Template";
        failedRows.add(row);
        Whitebox.setInternalState(instance, "failedRows", failedRows);
        StreamedContent content = instance.downloadCsv();
        // Assert that the file name is correct
        assertEquals("template_Errors.csv", content.getName());
        InputStream inputStream = content.getStream();
        String fileContent = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8))
                .lines()
                .collect(Collectors.joining("\n"));
        String expectedContent = "Area_Name,Asset_Name,Asset_Template_Name\n"
                + "Test Area1,Test @$$et,Test asset Template";
        assertEquals(expectedContent, fileContent);
    }

    /**
     * Test of uploadFile method, of class ImportConfigUploadBean.
     */
    @Test
    public void test2_UploadFile() {
        System.out.println("uploadFile");
        instance.uploadFile();
        // TODO review the generated test code and remove the default call to fail.
        //  fail("The test case is a prototype.");
    }

    /**
     * Test of clearFile method, of class ImportConfigUploadBean.
     */
    @Test
    public void test3_ClearFile() {
        System.out.println("clearFile");
        instance.clearFile();
        // TODO review the generated test code and remove the default call to fail.
        // fail("The test case is a prototype.");
    }

    /**
     * Test of upload method, of class ImportConfigUploadBean.
     */
    @Test
    public void test4_Upload() {
        System.out.println("upload");
        instance.upload();
        // TODO review the generated test code and remove the default call to fail.
        // fail("The test case is a prototype.");
    }

}
