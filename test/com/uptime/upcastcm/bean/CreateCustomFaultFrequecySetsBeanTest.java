/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.FaultFrequenciesClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import com.uptime.upcastcm.utils.helperclass.PresetUtil;
import com.uptime.upcastcm.utils.vo.FaultFrequenciesVO;
import com.uptime.upcastcm.utils.vo.UserPreferencesVO;
import com.uptime.upcastcm.utils.vo.UserSitesVO;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.faces.component.UIViewRoot;
import javax.faces.component.behavior.BehaviorBase;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.FixMethodOrder;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.junit.runners.MethodSorters;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import static org.powermock.api.mockito.PowerMockito.when;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;
import org.primefaces.PrimeFaces;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.ToggleSelectEvent;
import org.primefaces.event.UnselectEvent;

/**
 *
 * @author twilcox
 */
@RunWith(PowerMockRunner.class)
@PowerMockIgnore("jdk.internal.reflect.*")
@PrepareForTest({FacesContext.class, ExternalContext.class, NavigationBean.class, UserManageBean.class, ServiceRegistrarClient.class, FaultFrequenciesClient.class, PresetUtil.class, CreatePresetsDeviceBean.class,  PresetsBean.class,PrimeFaces.class, PrimeFaces.Ajax.class})
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@PowerMockRunnerDelegate(JUnit4.class)
public class CreateCustomFaultFrequecySetsBeanTest {
    @Mock
    ServiceRegistrarClient serviceRegistrarClient;
    
    @Mock
    FacesContext facesContext;
    
    @Mock
    ExternalContext externalContext;
    
    @Mock
    NavigationBean navigationBean;
    
    @Mock
    UserManageBean userManageBean;
    
    @Mock
    CreatePresetsDeviceBean createPresetsDeviceBean;
    
    @Mock
    PresetsBean presetsBean;
    
    @Mock
    PresetUtil presetUtil;
    
    @Mock
    FaultFrequenciesClient faultFrequenciesClient;
    
    @Mock
    UIViewRoot root;
    
    @Mock
    PrimeFaces pf;
    
    @Mock
    PrimeFaces.Ajax ax;
    
    private final CreateCustomFaultFrequecySetsBean instance;
    Map<String,Object> sessionMap;
    String customerAccount, defaultsite;
    UserPreferencesVO userVO;
    private final List<UserSitesVO> userSiteList;
    List<FaultFrequenciesVO> faultFrequenciesVOs, faultFrequenciesVOList, siteFaultFrequenciesVOList;
    
    public CreateCustomFaultFrequecySetsBeanTest() {
        PowerMockito.mockStatic(FacesContext.class);
        PowerMockito.mockStatic(ServiceRegistrarClient.class);
        PowerMockito.mockStatic(ExternalContext.class);
        PowerMockito.mockStatic(PresetUtil.class);
        PowerMockito.mockStatic(FaultFrequenciesClient.class);
        PowerMockito.mockStatic(PresetsBean.class);
        
        facesContext = PowerMockito.mock(FacesContext.class);
        externalContext = PowerMockito.mock(ExternalContext.class);
        navigationBean = PowerMockito.mock(NavigationBean.class);
        userManageBean = PowerMockito.mock(UserManageBean.class);
        createPresetsDeviceBean = PowerMockito.mock(CreatePresetsDeviceBean.class);
        presetUtil = PowerMockito.mock(PresetUtil.class);
        serviceRegistrarClient = PowerMockito.mock(ServiceRegistrarClient.class);
        faultFrequenciesClient = PowerMockito.mock(FaultFrequenciesClient.class);
        presetsBean = PowerMockito.mock(PresetsBean.class);
        root = PowerMockito.mock(UIViewRoot.class);
        
        PowerMockito.mockStatic(PrimeFaces.class);
        PowerMockito.mockStatic(PrimeFaces.Ajax.class);
        pf = PowerMockito.mock(PrimeFaces.class);
        ax = PowerMockito.mock(PrimeFaces.Ajax.class);
        
        sessionMap = new HashMap();
        sessionMap.put("navigationBean", navigationBean);
        sessionMap.put("userManageBean", userManageBean);
        sessionMap.put("createPresetsDeviceBean", createPresetsDeviceBean);
        sessionMap.put("presetsBean", presetsBean);
        
        when(FacesContext.getCurrentInstance()).thenReturn(facesContext);
        when(facesContext.getExternalContext()).thenReturn(externalContext);
        when(externalContext.getSessionMap()).thenReturn(sessionMap);
        
        customerAccount = "77777";
        defaultsite = "TEST SITE 001";
        
        userSiteList = new ArrayList<>();
        UserSitesVO uvo = new UserSitesVO();
        uvo.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        uvo.setSiteName("TEST SITE 001");
        userSiteList.add(uvo);
        
        userVO = new UserPreferencesVO();
        userVO.setCustomerAccount(customerAccount);
        userVO.setDefaultSite(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        userVO.setDefaultSiteName("TEST SITE 001");
        
        faultFrequenciesVOs = new ArrayList();
        faultFrequenciesVOList = new ArrayList();
        FaultFrequenciesVO ffvo = new FaultFrequenciesVO();
        ffvo.setCustomerAccount(customerAccount);
        ffvo.setFfName("TEST FF");
        ffvo.setFfSetId(UUID.randomUUID());
        ffvo.setSiteId(UUID.randomUUID());
        faultFrequenciesVOList.add(ffvo);
        
        siteFaultFrequenciesVOList= new ArrayList();
        FaultFrequenciesVO ffvo1 = new FaultFrequenciesVO();
        ffvo1.setCustomerAccount(customerAccount);
        ffvo1.setSiteId(userVO.getDefaultSite());
        ffvo1.setFfSetId(UUID.randomUUID());
        ffvo1.setFfName("TEST FF Site");
        faultFrequenciesVOList.add(ffvo);
        
        UserSitesVO usvo = new UserSitesVO();
        usvo.setCustomerAccount(customerAccount);
        
        when(userManageBean.getPresetUtil()).thenReturn(presetUtil);
        when(userManageBean.getCurrentSite()).thenReturn(usvo);
        when(presetUtil.getGlobalFaultFrequenciesByCustomer(customerAccount)).thenReturn(faultFrequenciesVOList);
        when(presetUtil.getSiteFaultFrequenciesList(customerAccount, userVO.getDefaultSite())).thenReturn(siteFaultFrequenciesVOList);
        
        PowerMockito.doNothing().when(presetUtil).clearSiteFaultFrequenciesList();
        when(ServiceRegistrarClient.getInstance()).thenReturn(serviceRegistrarClient);
        when(FaultFrequenciesClient.getInstance()).thenReturn(faultFrequenciesClient);
        when(serviceRegistrarClient.getServiceHostURL(PRESET_SERVICE_NAME)).thenReturn(PRESET_SERVICE_NAME);
        when(faultFrequenciesClient.deleteSiteFaultFrequencies(PRESET_SERVICE_NAME, faultFrequenciesVOs)).thenReturn(true);
        when(facesContext.getViewRoot()).thenReturn(root);
        instance = new CreateCustomFaultFrequecySetsBean();
        
        PowerMockito.mockStatic(PrimeFaces.class);
        PowerMockito.mockStatic(PrimeFaces.Ajax.class);
        pf = PowerMockito.mock(PrimeFaces.class);
        PrimeFaces.Ajax ax = PowerMockito.mock(PrimeFaces.Ajax.class);
        PowerMockito.mockStatic(PrimeFaces.class);
        PowerMockito.mockStatic(PrimeFaces.Ajax.class);
        when(pf.current()).thenReturn(pf);
        when(pf.ajax()).thenReturn(ax);       
    }

    /**
     * Test of init method, of class PresetsBean.
     */
    @Test
    public void test01_Init() {
        System.out.println("init");
        instance.init();
        System.out.println("init completed");
    }

    /**
     * Test of resetPage method, of class PresetsBean.
     */
    @Test
    public void test02_ResetPage() {
        System.out.println("resetPage");
        UserSitesVO usvo = new UserSitesVO();
        usvo.setSiteName("WWWWW");
        instance.resetPage();
        System.out.println("resetPage completed");
    }

    /**
     * Test of onRowUnselect method, of class CreateCustomFaultFrequecySetsBean.
     */
    @Test
    public void test03_OnRowUnselect() {
        System.out.println("onRowUnselect");
        FaultFrequenciesVO vo = new FaultFrequenciesVO();
        instance.setSelectedFaultFrequencyList(new ArrayList());
        instance.getSelectedFaultFrequencyList().add(vo);
        instance.onRowUnselect(new UnselectEvent<>(new DataTable(), new BehaviorBase(), vo));
        assertEquals(0, instance.getSelectedFaultFrequencyList().size());
        System.out.println("onRowUnselect completed");
    }

    /**
     * Test of onRowSelect method, of class CreateCustomFaultFrequecySetsBean.
     */
    @Test
    public void test04_OnRowSelect() {
        System.out.println("onRowSelect");
        instance.onRowSelect(null);
        FaultFrequenciesVO vo = new FaultFrequenciesVO();
        instance.onRowSelect(new SelectEvent<>(new DataTable(), new BehaviorBase(), vo));
        assertEquals(1, instance.getSelectedFaultFrequencyList().size());
        System.out.println("onRowSelect completed");
    }

    /**
     * Test of dataTableSelectAllRows method, of class CreateCustomFaultFrequecySetsBean.
     */
    @Test
    public void test05_DataTableSelectAllRows() {
        System.out.println("dataTableSelectAllRows");
        instance.setSelectedFaultFrequencyList(null);
        instance.dataTableSelectAllRows(new ToggleSelectEvent(new DataTable(), new BehaviorBase(), false));
        assertEquals(0, instance.getSelectedFaultFrequencyList().size());
        System.out.println("dataTableSelectAllRows completed");
    }

    /**
     * Test of submit method, of class CreateCustomFaultFrequecySetsBean.
     */
    @Test
    public void test06_Submit() {
        System.out.println("submit");

        FaultFrequenciesVO faultFrequenciesVO = new FaultFrequenciesVO();
        faultFrequenciesVO.setCustomerAccount(userManageBean.getCurrentSite().getCustomerAccount());
        faultFrequenciesVO.setFfId(UUID.randomUUID());
        faultFrequenciesVO.setFfName("FF Name 1");
        faultFrequenciesVO.setFfSetDesc("FF Set Desc");
        faultFrequenciesVO.setFfSetId(UUID.randomUUID());
        faultFrequenciesVO.setFfSetName("FF Set Name");
        faultFrequenciesVO.setFfUnit("FF Unit");
        faultFrequenciesVO.setFfValue(0.01f);
        faultFrequenciesVO.setGlobal(true);
        faultFrequenciesVO.setSiteId(userManageBean.getCurrentSite().getSiteId());
        instance.getFaultFrequencyList().add(faultFrequenciesVO);
        instance.setSelectedFaultFrequencyList(instance.getFaultFrequencyList());
        instance.setEdited(true);
      
        when(faultFrequenciesClient.updateSiteFFSetFavoritesVOList(instance.getFaultFrequencyList(), null)).thenReturn(true);
        instance.submit();
        assertEquals(false, instance.isEdited());
        System.out.println("submit completed");
    }
    
    /**
     * Test of submit method, of class CreateCustomFaultFrequecySetsBean.
     */
    @Test
    public void test061_Submit() {
        System.out.println("submit");

        FaultFrequenciesVO faultFrequenciesVO = new FaultFrequenciesVO();
        faultFrequenciesVO.setCustomerAccount(userManageBean.getCurrentSite().getCustomerAccount());
        faultFrequenciesVO.setFfId(UUID.randomUUID());
        faultFrequenciesVO.setFfName("FF Name 1");
        faultFrequenciesVO.setFfSetDesc("FF Set Desc");
        faultFrequenciesVO.setFfSetId(UUID.randomUUID());
        faultFrequenciesVO.setFfSetName("FF Set Name");
        faultFrequenciesVO.setFfUnit("FF Unit");
        faultFrequenciesVO.setFfValue(0.01f);
        faultFrequenciesVO.setGlobal(true);
        faultFrequenciesVO.setSiteId(userManageBean.getCurrentSite().getSiteId());
        instance.getFaultFrequencyList().add(faultFrequenciesVO);
        instance.setSelectedFaultFrequencyList(instance.getFaultFrequencyList());
        instance.setEdited(true);
      
        when(faultFrequenciesClient.updateSiteFFSetFavoritesVOList(instance.getFaultFrequencyList(), null)).thenReturn(false);
        instance.submit();
        assertEquals(true, instance.isEdited());
        System.out.println("submit completed");
    }

    /**
     * Test of delete method, of class PresetsBean.
     */
    @Test
    public void test07_Delete() {
        System.out.println("delete");
        instance.delete(null);
        System.out.println("delete completed");
    }

    /**
     * Test of delete method, of class PresetsBean.
     */
    @Test
    public void test08_Delete_ff() {
        System.out.println("delete");
        FaultFrequenciesVO faultFrequencies = new FaultFrequenciesVO();
        faultFrequencies.setCustomerAccount(customerAccount);
        faultFrequencies.setFfSetId(UUID.randomUUID());
        faultFrequencies.setSiteId(UUID.randomUUID());
        instance.getFaultFrequencyList().add(faultFrequencies);
        when(faultFrequenciesClient.deleteSiteFaultFrequencies(PRESET_SERVICE_NAME, instance.getFaultFrequencyList())).thenReturn(true);

        assertEquals(instance.getFaultFrequencyList().size(), 1);
        instance.delete(instance.getFaultFrequencyList().get(0));
        assertEquals(instance.getFaultFrequencyList().size(), 0);
        System.out.println("delete faultFrequencies completed");
    }
     /**
     * Test of delete method, of class PresetsBean.
     */
    @Test
    public void test081_Delete_ff() {
        System.out.println("delete");
        FaultFrequenciesVO faultFrequencies = new FaultFrequenciesVO();
        faultFrequencies.setCustomerAccount(customerAccount);
        faultFrequencies.setFfSetId(UUID.randomUUID());
        faultFrequencies.setSiteId(UUID.randomUUID());
        instance.getFaultFrequencyList().add(faultFrequencies);
        when(faultFrequenciesClient.deleteSiteFaultFrequencies(PRESET_SERVICE_NAME, instance.getFaultFrequencyList())).thenReturn(false);

        assertEquals(instance.getFaultFrequencyList().size(), 1);
        instance.delete(instance.getFaultFrequencyList().get(0));
        assertEquals(instance.getFaultFrequencyList().size(), 1);
        System.out.println("delete faultFrequencies completed");
    }

    /**
     * Test of setFaultFrequenciesDialog method, of class PresetsBean.
     */
    @Test
    public void test09_SetFaultFrequenciesDialog() {
        System.out.println("setApAlSetDialog");
        assertEquals(sessionMap.size(), 4);
        instance.setFaultFrequenciesDialog(null, 0);
        assertEquals(sessionMap.size(), 5);
        System.out.println("setApAlSetDialog completed");
    }

    /**
     * Test of getFaultFrequencyList method, of class PresetsBean.
     */
    @Test
    public void test10_GetFaultFrequencyList() {
        System.out.println("getFaultFrequencyList");
        assertEquals(new ArrayList(), instance.getFaultFrequencyList());
        System.out.println("getFaultFrequencyList completed");
    }

    /**
     * Test of getFaultFrequenciesByRowToggle method, of class PresetsBean.
     */
    @Test
    public void test11_GetFaultFrequenciesByRowToggle() {
        System.out.println("getFaultFrequenciesByRowToggle");
        FaultFrequenciesVO faultFrequenciesVO = new FaultFrequenciesVO();
        faultFrequenciesVO.setFfSetId(faultFrequenciesVOList.get(0).getFfSetId());
        instance.getFaultFrequenciesByRowToggle(faultFrequenciesVO);
        assertEquals(instance.getFaultFrequencyList().size(), 0);
    }

    /**
     * Test of faultFrequencieUpload method, of class PresetsBean.
     */
    @Test
    public void test12_FaultFrequencieUpload() {
        System.out.println("faultFrequencieUpload");
        FileUploadEvent event = null;
        instance.faultFrequencieUpload(event);
        assertEquals(instance.getFaultFrequencyList().size(), 0);
    }

    /**
     * Test of getFilteredFaultFrequencyList method, of class PresetsBean.
     */
    @Test
    public void test13_GetFilteredFaultFrequencyList() {
        System.out.println("getFilteredFaultFrequencyList");
        List<FaultFrequenciesVO> expResult = new ArrayList<>();
        List<FaultFrequenciesVO> result = instance.getFilteredFaultFrequencyList();
        assertEquals(expResult, result);
    }

    /**
     * Test of getDistinctFaultFrequencies method, of class PresetsBean.
     */
    @Test
    public void test14_GetDistinctFaultFrequencies() {
        System.out.println("getDistinctFaultFrequencies");
        List<FaultFrequenciesVO> expResult = new ArrayList<>();
        List<FaultFrequenciesVO> result = instance.getDistinctFaultFrequencies();
        assertEquals(expResult, result);
    }

    /**
     * Test of customFaultFrequenciesButtonMessage method, of class CreateCustomFaultFrequecySetsBean.
     */
    @Test
    public void test15_CustomFaultFrequenciesButtonMessage() {
        System.out.println("customFaultFrequenciesButtonMessage");
        when(userManageBean.getResourceBundleString("btn_save_preferred")).thenReturn("Save Preferred");
        assertEquals("Save Preferred", instance.customFaultFrequenciesButtonMessage());
        instance.setSelectedFaultFrequencyList(new ArrayList());
        instance.getSelectedFaultFrequencyList().add(new FaultFrequenciesVO());
        assertEquals("Save Preferred (1)", instance.customFaultFrequenciesButtonMessage());
        System.out.println("customFaultFrequenciesButtonMessage completed");
    }

    /**
     * Test of getParamUnitLabelByValue method, of class CreateCustomFaultFrequecySetsBean.
     */
    @Test
    public void test16_GetParamUnitLabelByValue() {
        System.out.println("getParamUnitLabelByValue");
        assertNull(instance.getParamUnitLabelByValue(null));
        System.out.println("getParamUnitLabelByValue completed");
    }

    /**
     * Test of getSelectedFaultFrequencyList method, of class CreateCustomFaultFrequecySetsBean.
     */
    @Test
    public void test17_GetSelectedFaultFrequencyList() {
        System.out.println("getSelectedFaultFrequencyList");
        instance.setSelectedFaultFrequencyList(new ArrayList());
        assertEquals(0, instance.getSelectedFaultFrequencyList().size());
        instance.setSelectedFaultFrequencyList(null);
        assertNull(instance.getSelectedFaultFrequencyList());
        System.out.println("getSelectedFaultFrequencyList completed");
    }

    /**
     * Test of setSelectedFaultFrequencyList method, of class CreateCustomFaultFrequecySetsBean.
     */
    @Test
    public void test18_SetSelectedFaultFrequencyList() {
        System.out.println("setSelectedFaultFrequencyList");
        instance.setSelectedFaultFrequencyList(new ArrayList());
        assertEquals(0, instance.getSelectedFaultFrequencyList().size());
        instance.setSelectedFaultFrequencyList(null);
        assertNull(instance.getSelectedFaultFrequencyList());
        System.out.println("setSelectedFaultFrequencyList completed");
    }

    /**
     * Test of isEdited method, of class CreateCustomFaultFrequecySetsBean.
     */
    @Test
    public void test19_IsEdited() {
        System.out.println("isEdited");
        instance.setEdited(false);
        assertFalse(instance.isEdited());
        instance.setEdited(true);
        assertTrue(instance.isEdited());
        System.out.println("isEdited completed");
    }

    /**
     * Test of setEdited method, of class CreateCustomFaultFrequecySetsBean.
     */
    @Test
    public void test20_SetEdited() {
        System.out.println("setEdited");
        instance.setEdited(false);
        assertFalse(instance.isEdited());
        instance.setEdited(true);
        assertTrue(instance.isEdited());
        System.out.println("setEdited completed");
    }
}
