/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.AssetPresetClient;
import com.uptime.upcastcm.http.client.DevicePresetClient;
import com.uptime.upcastcm.http.client.TachometerClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.DEFAULT_UPTIME_ACCOUNT;
import static com.uptime.upcastcm.utils.ApplicationConstants.PRESET_SERVICE_NAME;
import com.uptime.upcastcm.utils.helperclass.Filters;
import com.uptime.upcastcm.utils.helperclass.PresetUtil;
import com.uptime.upcastcm.utils.vo.AssetPresetPointLocationVO;
import com.uptime.upcastcm.utils.vo.AssetPresetVO;
import com.uptime.upcastcm.utils.vo.DevicePresetVO;
import com.uptime.upcastcm.utils.vo.TachometerVO;
import com.uptime.upcastcm.utils.vo.UserSitesVO;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import static org.powermock.api.mockito.PowerMockito.when;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.primefaces.PrimeFaces;

/**
 *
 * @author joseph
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(PowerMockRunner.class)
@PowerMockIgnore("jdk.internal.reflect.*")
@PrepareForTest({FacesContext.class, ExternalContext.class, ServiceRegistrarClient.class, UserManageBean.class, AssetPresetClient.class, DevicePresetClient.class, TachometerClient.class, PresetUtil.class, PresetsBean.class, PrimeFaces.class, PrimeFaces.Ajax.class})
public class AddPresetsPointLocationBeanTest {

    @Mock
    ServiceRegistrarClient serviceRegistrarClient;

    @Mock
    FacesContext facesContext;

    @Mock
    ExternalContext externalContext;

    @Mock
    UserManageBean userManageBean;

    @Mock
    AssetPresetClient assetPresetClient;

    @Mock
    DevicePresetClient devicePresetClient;

    @Mock
    TachometerClient tachometerClient;

    @Mock
    HttpServletRequest request;

    @Mock
    HttpSession session;

    @Mock
    PresetUtil presetUtil;

    @Mock
    PresetsBean presetsBean;

    @Mock
    PrimeFaces pf;

    @Mock
    PrimeFaces.Ajax ax;

    @Mock
    NavigationBean navigationBean;

    Map<String, Object> sessionMap;
    private final AddPresetsPointLocationBean instance;
    AssetPresetVO assetPresetsVO;
    UUID siteId;

    public AddPresetsPointLocationBeanTest() {
        UserSitesVO uvo;

        PowerMockito.mockStatic(FacesContext.class);
        PowerMockito.mockStatic(DevicePresetClient.class);
        PowerMockito.mockStatic(ServiceRegistrarClient.class);
        PowerMockito.mockStatic(AssetPresetClient.class);
        PowerMockito.mockStatic(TachometerClient.class);

        facesContext = PowerMockito.mock(FacesContext.class);
        serviceRegistrarClient = PowerMockito.mock(ServiceRegistrarClient.class);
        assetPresetClient = PowerMockito.mock(AssetPresetClient.class);
        devicePresetClient = PowerMockito.mock(DevicePresetClient.class);
        tachometerClient = PowerMockito.mock(TachometerClient.class);
        externalContext = PowerMockito.mock(ExternalContext.class);
        request = PowerMockito.mock(HttpServletRequest.class);
        session = PowerMockito.mock(HttpSession.class);
        userManageBean = PowerMockito.mock(UserManageBean.class);
        presetUtil = PowerMockito.mock(PresetUtil.class);
        presetsBean = PowerMockito.mock(PresetsBean.class);
        navigationBean = PowerMockito.mock(NavigationBean.class);
        PowerMockito.mockStatic(PrimeFaces.class);
        PowerMockito.mockStatic(PrimeFaces.Ajax.class);
        pf = PowerMockito.mock(PrimeFaces.class);
        ax = PowerMockito.mock(PrimeFaces.Ajax.class);
        sessionMap = new HashMap();
        sessionMap.put("userManageBean", userManageBean);
        sessionMap.put("navigationBean", navigationBean);
        sessionMap.put("presetsBean", presetsBean);
        session.setAttribute("userManageBean", userManageBean);
        session.setAttribute("navigationBean", navigationBean);

        uvo = new UserSitesVO();
        uvo.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        uvo.setSiteName("Test Site 1");

        siteId = UUID.randomUUID();
        assetPresetsVO = new AssetPresetVO();
        assetPresetsVO.setType("site");
        assetPresetsVO.setAssetPresetId(UUID.randomUUID());
        assetPresetsVO.setAssetPresetName("Asset Preset 1");
        assetPresetsVO.setAssetTypeId(UUID.randomUUID());
        assetPresetsVO.setAssetTypeName("Asset Type name 1");
        assetPresetsVO.setCustomerAccount("77777");
        assetPresetsVO.setDescription("Test Description");
        assetPresetsVO.setDevicePresetId(UUID.randomUUID());
        assetPresetsVO.setDevicePresetName("Device Preset Name 1");
        assetPresetsVO.setDevicePresetType("MistLX");
        assetPresetsVO.setPointLocationName("Point Location 1");
        assetPresetsVO.setSampleInterval(720);
        assetPresetsVO.setSiteId(siteId);
        assetPresetsVO.setSiteName("Test Site 1");
        assetPresetsVO.setSpeedRatio(2.0f);
        when(FacesContext.getCurrentInstance()).thenReturn(facesContext);
        when(ServiceRegistrarClient.getInstance()).thenReturn(serviceRegistrarClient);
        when(AssetPresetClient.getInstance()).thenReturn(assetPresetClient);
        when(DevicePresetClient.getInstance()).thenReturn(devicePresetClient);
        when(TachometerClient.getInstance()).thenReturn(tachometerClient);
        when(facesContext.getExternalContext()).thenReturn(externalContext);
        when(externalContext.getSessionMap()).thenReturn(sessionMap);
        when(facesContext.getExternalContext().getRequest()).thenReturn(request);
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("userManageBean")).thenReturn(userManageBean);
        when(userManageBean.getPresetUtil()).thenReturn(presetUtil);
        when(session.getAttribute("navigationBean")).thenReturn(navigationBean);
        when(userManageBean.getCurrentSite()).thenReturn(uvo);
        UserSitesVO usvo = new UserSitesVO();
        usvo.setCustomerAccount("77777");
        usvo.setSiteId(siteId);
        when(userManageBean.getCurrentSite()).thenReturn(usvo);
        instance = new AddPresetsPointLocationBean();
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of init method, of class AddPresetsPointLocationBean.
     */
    @Test
    public void testInit() {
        System.out.println("init");
        instance.setSelectedDevicePresetId(UUID.randomUUID());
        instance.init();
        assertEquals(instance.getSelectedDevicePresetId(), null);
        System.out.println("init completed");
    }

    /**
     * Test of resetPage method, of class AddPresetsPointLocationBean.
     */
    @Test
    public void testResetPage() {
        System.out.println("resetPage");
        instance.setSelectedDevicePresetId(UUID.randomUUID());
        instance.resetPage();
        assertEquals(instance.getSelectedDevicePresetId(), null);
        System.out.println("resetPage completed");
    }

    /**
     * Test of presetFields method, of class AddPresetsPointLocationBean.
     */
    @Test(expected = UnsupportedOperationException.class)
    public void testPresetFields_Filters_Object() {
        System.out.println("presetFields");
        Filters presets = null;
        Object object = null;
        instance.presetFields(presets, object);
    }

    /**
     * Test of presetFields method, of class AddPresetsPointLocationBean.
     */
    @Test(expected = UnsupportedOperationException.class)
    public void testPresetFields_int_Object() {
        System.out.println("presetFields");
        int operationType = 0;
        Object object = null;
        instance.presetFields(operationType, object);
    }

    /**
     * Test of presetFields method, of class AddPresetsPointLocationBean.
     */
    @Test
    public void testPresetFields_Object() {
        System.out.println("presetFields");
        AssetPresetVO assetPresetVO = new AssetPresetVO();
        Whitebox.setInternalState(instance, "assetPresetVO", assetPresetVO);
        AssetPresetVO apVO = new AssetPresetVO(assetPresetsVO);
        apVO.setType("site");
        List<AssetPresetPointLocationVO> list = new ArrayList();
        apVO.setAssetPresetPointLocationVOList(list);
        instance.presetFields(apVO);
        assertEquals(instance.getAssetPresetVO().getType(), "site");
    }

    /**
     * Test of onDeviceTypeChange method, of class AddPresetsPointLocationBean.
     */
    @Test
    public void testOnDeviceTypeChange() {
        System.out.println("onDeviceTypeChange");

        List<AssetPresetPointLocationVO> list = new ArrayList();
        assetPresetsVO.setAssetPresetPointLocationVOList(list);
        assetPresetsVO.setType("site");
        Whitebox.setInternalState(instance, "assetPresetVO", assetPresetsVO);
        instance.setSelectedDeviceType("MistLX");
        List<DevicePresetVO> dpvos = new ArrayList();
        DevicePresetVO dpvo = new DevicePresetVO();
        dpvo.setPresetId(UUID.fromString("5dc46b36-0718-4ec5-9a38-b9322e788a87"));
        dpvo.setDeviceId("5dc46b36-0718-4ec5-9a38-b9322e788a87");
        dpvo.setDeviceType("MistLX");
        dpvos.add(dpvo);
        when(devicePresetClient.getGlobalDevicePresetsByCustomerDeviceType(serviceRegistrarClient.getServiceHostURL(PRESET_SERVICE_NAME), DEFAULT_UPTIME_ACCOUNT, "MistLX")).thenReturn(new ArrayList());
        when(devicePresetClient.getGlobalDevicePresetsByCustomerDeviceType(serviceRegistrarClient.getServiceHostURL(PRESET_SERVICE_NAME), "77777", "MistLX")).thenReturn(new ArrayList());
        when(devicePresetClient.getSiteDeviceByCustomerSiteDeviceType(serviceRegistrarClient.getServiceHostURL(PRESET_SERVICE_NAME), "77777", siteId.toString(), "MistLX")).thenReturn(dpvos);
        instance.onDeviceTypeChange();
        assertTrue(instance.getDevicePresetList().size() > 0);
        System.out.println("onDeviceTypeChange completed");
    }

    /**
     * Test of onDeviceTemplateChange method, of class AddPresetsPointLocationBean.
     */
    @Test
    public void testOnDeviceTemplateChange() {
        System.out.println("onDeviceTemplateChange");
        // instance.onDeviceTemplateChange();
        AssetPresetPointLocationVO assetPresetPointLocationVO = new AssetPresetPointLocationVO();
        assetPresetPointLocationVO.setCustomerAccount("77777");
        assetPresetPointLocationVO.setPointLocationName("Test Point Location1");
        assetPresetPointLocationVO.setDeviceType("MistLX");
        assetPresetPointLocationVO.setDeviceId(UUID.fromString("5dc46b36-0718-4ec5-9a38-b9322e788a87"));
        instance.setAssetPresetPointLocationVO(assetPresetPointLocationVO);
        instance.setSelectedDevicePresetId(UUID.fromString("5dc46b36-0718-4ec5-9a38-b9322e788a87"));
        testOnDeviceTypeChange();
        instance.onDeviceTemplateChange();
        assertTrue(instance.getDistinctDevicePresetList().size() > 0);
    }

    /**
     * Test of onTachChange method, of class AddPresetsPointLocationBean.
     */
    @Test
    public void testOnTachChange() {
        System.out.println("onTachChange");
        UUID tachId = UUID.randomUUID();
        AssetPresetPointLocationVO assetPresetPointLocationVO = new AssetPresetPointLocationVO();
        assetPresetPointLocationVO.setCustomerAccount("77777");
        assetPresetPointLocationVO.setPointLocationName("Test Point Location1");
        assetPresetPointLocationVO.setDeviceType("MistLX");
        assetPresetPointLocationVO.setDeviceId(UUID.fromString("5dc46b36-0718-4ec5-9a38-b9322e788a87"));
        assetPresetPointLocationVO.setTachId(tachId);
        assetPresetPointLocationVO.setSpeedRatio(10);
        instance.setAssetPresetPointLocationVO(assetPresetPointLocationVO);
        List<TachometerVO> tachList = new ArrayList();
        TachometerVO tachometerVO = new TachometerVO();
        tachometerVO.setTachReferenceUnits("inches");
        when(tachometerClient.getGlobalTachometersByPK(serviceRegistrarClient.getServiceHostURL(PRESET_SERVICE_NAME), DEFAULT_UPTIME_ACCOUNT, tachId)).thenReturn(tachList);
        instance.onTachChange();
        assertTrue(instance.getAssetPresetPointLocationVO().getSpeedRatio() == 0f);
    }

    /**
     * Test of submit method, of class AddPresetsPointLocationBean.
     */
    @Test
    public void testSubmit() {
        System.out.println("submit");
        when(PrimeFaces.current()).thenReturn(pf);
        when(pf.ajax()).thenReturn(ax);
        ax.addCallbackParam("createAssetsFormId", new Object());
        ax.addCallbackParam("copyEditPresetsAssetFormId", new Object());
        List<AssetPresetPointLocationVO> list = new ArrayList();
        assetPresetsVO.setAssetPresetPointLocationVOList(list);
        assetPresetsVO.setType("site");
        Whitebox.setInternalState(instance, "assetPresetVO", assetPresetsVO);
        AssetPresetPointLocationVO assetPresetPointLocationVO = new AssetPresetPointLocationVO();
        assetPresetPointLocationVO.setCustomerAccount("77777");
        assetPresetPointLocationVO.setPointLocationName("Test Point Location1");
        assetPresetPointLocationVO.setDeviceType("MistLX");
        assetPresetPointLocationVO.setDeviceId(UUID.fromString("5dc46b36-0718-4ec5-9a38-b9322e788a87"));
        assetPresetPointLocationVO.setSpeedRatio(10);
        instance.setAssetPresetPointLocationVO(assetPresetPointLocationVO);
        instance.setAssetPresetPointLocationVOList(list);
        assertTrue(instance.getAssetPresetVO().getAssetPresetPointLocationVOList().isEmpty());
        instance.submit();
        assertTrue(instance.getAssetPresetVO().getAssetPresetPointLocationVOList().size() > 0);
    }

}
