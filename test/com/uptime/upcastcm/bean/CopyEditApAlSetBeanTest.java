/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.APALSetClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import com.uptime.upcastcm.utils.vo.ApAlSetVO;
import com.uptime.upcastcm.utils.vo.UserPreferencesVO;
import com.uptime.upcastcm.utils.vo.UserSitesVO;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import static org.powermock.api.mockito.PowerMockito.when;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.primefaces.PrimeFaces;

/**
 *
 * @author gsingh
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(PowerMockRunner.class)
@PowerMockIgnore("jdk.internal.reflect.*")
@PrepareForTest({FacesContext.class, ExternalContext.class, UserManageBean.class, ServiceRegistrarClient.class, NavigationBean.class, APALSetClient.class, PrimeFaces.class, PrimeFaces.Ajax.class})
public class CopyEditApAlSetBeanTest {

    @Mock
    ServiceRegistrarClient serviceRegistrarClient;
    @Mock
    APALSetClient aPALSetClient;
    @Mock
    FacesContext facesContext;
    @Mock
    ExternalContext externalContext;
    @Mock
    HttpServletRequest request;
    @Mock
    HttpSession session;
    @Mock
    UserManageBean userManageBean;

    private final CopyEditApAlSetBean instance;
    Map<String, Object> sessionMap;
    private final List<UserSitesVO> userSiteList;

    public CopyEditApAlSetBeanTest() {
        PowerMockito.mockStatic(FacesContext.class);
        PowerMockito.mockStatic(ServiceRegistrarClient.class);
        PowerMockito.mockStatic(APALSetClient.class);

        facesContext = PowerMockito.mock(FacesContext.class);
        externalContext = PowerMockito.mock(ExternalContext.class);
        request = PowerMockito.mock(HttpServletRequest.class);
        session = PowerMockito.mock(HttpSession.class);
        userManageBean = PowerMockito.mock(UserManageBean.class);

        aPALSetClient = PowerMockito.mock(APALSetClient.class);
        serviceRegistrarClient = PowerMockito.mock(ServiceRegistrarClient.class);

        sessionMap = new HashMap();
        sessionMap.put("userManageBean", userManageBean);
        session.setAttribute("userManageBean", userManageBean);

        userSiteList = new ArrayList<>();
        UserSitesVO uvo = new UserSitesVO();
        uvo.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        uvo.setSiteName("Test Site 1");
        uvo.setCustomerAccount("77777");
        userSiteList.add(uvo);

        when(ServiceRegistrarClient.getInstance()).thenReturn(serviceRegistrarClient);
        when(APALSetClient.getInstance()).thenReturn(aPALSetClient);
        when(FacesContext.getCurrentInstance()).thenReturn(facesContext);
        when(facesContext.getExternalContext()).thenReturn(externalContext);
        when(externalContext.getSessionMap()).thenReturn(sessionMap);
        when(facesContext.getExternalContext().getRequest()).thenReturn(request);
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("userManageBean")).thenReturn(userManageBean);

        when(userManageBean.getUserSiteList()).thenReturn(userSiteList);
        PowerMockito.mockStatic(PrimeFaces.class);
        PowerMockito.mockStatic(PrimeFaces.Ajax.class);
        PrimeFaces pf = PowerMockito.mock(PrimeFaces.class);
        PrimeFaces.Ajax ax = PowerMockito.mock(PrimeFaces.Ajax.class);
        when(pf.current()).thenReturn(pf);
        when(pf.ajax()).thenReturn(ax);

        UserPreferencesVO userVO = new UserPreferencesVO();
        userVO.setDefaultSite(UUID.randomUUID());
        userVO.setCustomerAccount("77777");
        when(session.getAttribute("userVO")).thenReturn(userVO);
        UserSitesVO usvo = new UserSitesVO();
        usvo.setCustomerAccount("77777");
        when(userManageBean.getCurrentSite()).thenReturn(usvo);
        instance = new CopyEditApAlSetBean();
    }

    @Before
    public void setUp() {
        UserSitesVO uvo = new UserSitesVO();
        uvo.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        uvo.setSiteName("Test Site 1");
        when(userManageBean.getCurrentSite()).thenReturn(uvo);
    }

    /**
     * Test of resetPage method, of class CopyEditApAlSetBean.
     */
    @Test
    public void testResetPage() {
        System.out.println("resetPage");
        List<ApAlSetVO> apAlSetVOList = new ArrayList<>();
        ApAlSetVO apAlSetVO = new ApAlSetVO();
        apAlSetVOList.add(apAlSetVO);
        Whitebox.setInternalState(instance, "apAlSetVOList", apAlSetVOList);
        instance.resetPage();
        assertEquals(apAlSetVOList.size(), 0);
    }

    /**
     * Test of presetFields method, of class CopyEditApAlSetBean.
     */
    @Test(expected = UnsupportedOperationException.class)
    public void testPresetFields_Filters_Object() {
        System.out.println("presetFields");
        instance.presetFields(null, null);
    }

    /**
     * Test of presetFields method, of class CopyEditApAlSetBean.
     */
    @Test
    public void testPresetFields_int_Object() {
        System.out.println("presetFields");
        List<ApAlSetVO> apAlSetVOList = new ArrayList<>();
        Whitebox.setInternalState(instance, "apAlSetVOList", apAlSetVOList);
        ApAlSetVO apAlSetVO = new ApAlSetVO();
        apAlSetVOList.add(apAlSetVO);
        int operationType = 1;
        when(APALSetClient.getInstance()).thenReturn(aPALSetClient);
        when(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME)).thenReturn("presets");
        when(APALSetClient.getInstance().getGlobalApAlSetsByCustomerApSetId(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), "77777", apAlSetVO.getApSetId())).thenReturn(apAlSetVOList);
        instance.presetFields(operationType, apAlSetVO);
        assertEquals(apAlSetVOList.size(), 1);
    }

    /**
     * Test of presetFields method, of class CopyEditApAlSetBean.
     */
    @Test
    public void testPresetFields_Object() {
        System.out.println("presetFields");
        UserSitesVO uvo = new UserSitesVO();
        uvo.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        uvo.setSiteName("Test Site 1");
        ApAlSetVO apAlSetVO = new ApAlSetVO();
        Whitebox.setInternalState(instance, "apAlSetVO", apAlSetVO);
        instance.presetFields(uvo);
        assertEquals(uvo.getSiteId(), apAlSetVO.getSiteId());
    }

    /**
     * Test of submit method, of class CopyEditApAlSetBean.
     */
    @Test
    public void testSubmit() {
        System.out.println("submit");
        instance.submit();
    }

}
