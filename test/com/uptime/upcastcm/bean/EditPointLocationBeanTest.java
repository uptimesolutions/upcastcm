/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.PointLocationClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import com.uptime.upcastcm.utils.helperclass.Filters;
import com.uptime.upcastcm.utils.helperclass.PresetUtil;
import com.uptime.upcastcm.utils.vo.PointLocationVO;
import com.uptime.upcastcm.utils.vo.UserSitesVO;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import static org.powermock.api.mockito.PowerMockito.when;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

/**
 *
 * @author joseph
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(PowerMockRunner.class)
@PowerMockIgnore("jdk.internal.reflect.*")
@PrepareForTest({FacesContext.class, ExternalContext.class, HttpServletRequest.class, HttpSession.class, UserManageBean.class, ServiceRegistrarClient.class, PresetUtil.class, PointLocationClient.class,ConfigurationBean.class})

public class EditPointLocationBeanTest {

    @Mock
    FacesContext facesContext;

    @Mock
    ExternalContext externalContext;

    @Mock
    UserManageBean userManageBean;

    @Mock
    HttpServletRequest request;

    @Mock
    HttpSession session;

    @Mock
    ServiceRegistrarClient serviceRegistrarClient;

    @Mock
    PresetUtil presetUtil;

    @Mock
    PointLocationClient pointLocationClient;

    @Mock
    ConfigurationBean configurationBean;

    private final Filters filters;
    EditPointLocationBean instance;
    String customerAccount;
    Map<String, Object> sessionMap;

    public EditPointLocationBeanTest() {
        PowerMockito.mockStatic(FacesContext.class);
        PowerMockito.mockStatic(ServiceRegistrarClient.class);
        PowerMockito.mockStatic(PresetUtil.class);
        PowerMockito.mockStatic(PointLocationClient.class);

        facesContext = PowerMockito.mock(FacesContext.class);
        externalContext = PowerMockito.mock(ExternalContext.class);
        userManageBean = PowerMockito.mock(UserManageBean.class);
        request = PowerMockito.mock(HttpServletRequest.class);
        presetUtil = PowerMockito.mock(PresetUtil.class);
        session = PowerMockito.mock(HttpSession.class);
        serviceRegistrarClient = PowerMockito.mock(ServiceRegistrarClient.class);
        pointLocationClient = PowerMockito.mock(PointLocationClient.class);
        configurationBean = PowerMockito.mock(ConfigurationBean.class);

        when(ServiceRegistrarClient.getInstance()).thenReturn(serviceRegistrarClient);
        when(FacesContext.getCurrentInstance()).thenReturn(facesContext);

        when(facesContext.getExternalContext()).thenReturn(externalContext);
        when(externalContext.getRequest()).thenReturn(request);
        
        customerAccount = "77777";
        
        sessionMap = new HashMap();
        sessionMap.put("userManageBean", userManageBean);
        sessionMap.put("configurationBean", configurationBean);
        
        when(externalContext.getSessionMap()).thenReturn(sessionMap);
        UserSitesVO usvo = new UserSitesVO();
        usvo.setCustomerAccount(customerAccount);
        when(userManageBean.getCurrentSite()).thenReturn(usvo);
        when(userManageBean.getPresetUtil()).thenReturn(presetUtil);
        when(PointLocationClient.getInstance()).thenReturn(pointLocationClient);

        filters = new Filters();

        filters.setCountry("Test Country 1");
        filters.setRegion("Test Region 1");
        filters.setSite(new SelectItem(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"), "Test site 1"));
        filters.setArea(new SelectItem(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"), "Test Area 1"));
        filters.setAsset(new SelectItem(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"), "Test Asset 1"));
        filters.setPointLocation(new SelectItem(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"), "Test Point Location 1"));
        filters.setPoint(new SelectItem(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"), "Test point 1"));

        instance = new EditPointLocationBean();
    }

    /**
     * Test of init method, of class EditPointLocationBean.
     */
    @Test
    public void test1_Init() {
        System.out.println("init");
        instance.init();
        assertSame(customerAccount, instance.getPointLocationVO().getCustomerAccount());
        System.out.println("init completed");
    }

    /**
     * Test of resetPage method, of class EditPointLocationBean.
     */
    @Test
    public void test2_ResetPage() {
        System.out.println("resetPage");
        instance.resetPage();
        assertSame(customerAccount, instance.getPointLocationVO().getCustomerAccount());
        System.out.println("resetPage completed");
    }

    /**
     * Test of presetFields method, of class EditPointLocationBean.
     */
    @Test
    public void test3_PresetFields_Filters_Object() {
        System.out.println("presetFields");
        instance.setSiteNames(new ArrayList());
        instance.setAreaNameList(new ArrayList());
        instance.presetFields(filters, new PointLocationVO());
        System.out.println("presetFields completed");
    }

    /**
     * Test of updateBean method, of class EditPointLocationBean.
     */
    @Test
    public void test4_UpdateBean() {
        System.out.println("updateBean");
        instance.setPointLocationVO(new PointLocationVO());
        instance.updateBean();
        System.out.println("updateBean completed");
    }

    /**
     * Test of updatePointLocation method, of class EditPointLocationBean.
     */
    @Test
    public void test5_UpdatePointLocation() {
        System.out.println("updatePointLocation");
        PointLocationVO pointLocationVO = new PointLocationVO();
        pointLocationVO.setCustomerAccount("77777");
        Set<String> ffSetName = new HashSet();
        ffSetName.add("Test FF set Name");
        pointLocationVO.setFfSetName(ffSetName);
        instance.setPointLocationVO(pointLocationVO);
        when(pointLocationClient.updatePointLocation(serviceRegistrarClient.getServiceHostURL(CONFIGURATION_SERVICE_NAME), instance.getPointLocationVO())).thenReturn(Boolean.TRUE);
        instance.updatePointLocation();
        assertSame(null, instance.getPointLocationVO().getCustomerAccount());
        System.out.println("updatePointLocation completed");
    }
    

    /**
     * Test of updatePointLocation method, of class EditPointLocationBean.
     */
    @Test
    public void test51_UpdatePointLocation() {
        System.out.println("updatePointLocation");
        PointLocationVO pointLocationVO = new PointLocationVO();
        pointLocationVO.setCustomerAccount("77777");
        pointLocationVO.setSiteName("Test Site Name");
        Set<String> ffSetName = new HashSet();
        ffSetName.add("Test FF set Name");
        pointLocationVO.setFfSetName(ffSetName);
        instance.setPointLocationVO(pointLocationVO);
        when(pointLocationClient.updatePointLocation(serviceRegistrarClient.getServiceHostURL(CONFIGURATION_SERVICE_NAME), instance.getPointLocationVO())).thenReturn(Boolean.FALSE);
        instance.updatePointLocation();
        assertSame("77777", instance.getPointLocationVO().getCustomerAccount());
        System.out.println("updatePointLocation completed");
    }

    /**
     * Test of presetFields method, of class EditPointLocationBean.
     */
    @Test(expected = UnsupportedOperationException.class)
    public void test6_PresetFields_Object() {
        System.out.println("presetFields");
        Object object = null;
        instance.presetFields(object);
    }

    /**
     * Test of presetFields method, of class EditPointLocationBean.
     */
    @Test(expected = UnsupportedOperationException.class)
    public void test7_PresetFields_int_Object() {
        System.out.println("presetFields");
        int operationType = 0;
        Object object = null;
        instance.presetFields(operationType, object);
    }

}
