/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.bean;

import static com.uptime.upcastcm.utils.enums.ACSensorTypeEnum.getACSensorTypeItemList;
import static com.uptime.upcastcm.utils.enums.DCSensorTypeEnum.getDCSensorTypeItemList;
import static com.uptime.upcastcm.utils.enums.DemodHighAndLowPassEnum.getDemodItemList;
import static com.uptime.upcastcm.utils.enums.DemodHighAndLowPassEnum.getLowPassOptionsGreaterThanSelectedHighPass;
import static com.uptime.upcastcm.utils.enums.DwellEnum.getDwellItemList;
import static com.uptime.upcastcm.utils.enums.FmaxEnum.getFmaxItemList;
import static com.uptime.upcastcm.utils.enums.ParamTypeEnum.getParamTypeItemList;
import static com.uptime.upcastcm.utils.enums.ParamUnitEnum.getFrequencyPlusUnits;
import static com.uptime.upcastcm.utils.enums.ResolutionEnum.getResolutionItemList;
import com.uptime.upcastcm.utils.vo.AlSetVO;
import com.uptime.upcastcm.utils.vo.ApAlSetVO;
import com.uptime.upcastcm.utils.vo.UserSitesVO;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import static org.powermock.api.mockito.PowerMockito.when;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

/**
 *
 * @author twilcox
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(PowerMockRunner.class)
@PowerMockIgnore("jdk.internal.reflect.*")
@PrepareForTest({FacesContext.class, ExternalContext.class, UserManageBean.class})
public class CreateApAlSetBeanTest {
    
    @Mock
    FacesContext facesContext;
    
    @Mock
    ExternalContext externalContext;
    
    @Mock
    UserManageBean userManageBean;
    
    @Mock
    HttpServletRequest request;
    
    @Mock
    HttpSession session;
    
    private final CreateApAlSetBean instance;
    Map<String,Object> sessionMap;
    ApAlSetVO apAlRow;
    AlSetVO alRow;
    
    public CreateApAlSetBeanTest() {

        PowerMockito.mockStatic(FacesContext.class);
        
        facesContext = PowerMockito.mock(FacesContext.class);
        externalContext = PowerMockito.mock(ExternalContext.class);
        userManageBean = PowerMockito.mock(UserManageBean.class);
        request = PowerMockito.mock(HttpServletRequest.class);
        session = PowerMockito.mock(HttpSession.class);

        sessionMap = new HashMap();
        sessionMap.put("userManageBean", userManageBean);
        session.setAttribute("userManageBean", userManageBean);
        
        UserSitesVO uvo = new UserSitesVO();
        uvo.setSiteId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        uvo.setSiteName("Test Site 1");
        uvo.setCustomerAccount("77777");
        
        when(FacesContext.getCurrentInstance()).thenReturn(facesContext);
        when(facesContext.getExternalContext()).thenReturn(externalContext);
        when(externalContext.getSessionMap()).thenReturn(sessionMap);
        when(facesContext.getExternalContext().getRequest()).thenReturn(request);
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("userManageBean")).thenReturn(userManageBean);
        when(userManageBean.getCurrentSite()).thenReturn(uvo);
        
        ApAlSetVO apAlSetVO = new ApAlSetVO();
        instance = new CreateApAlSetBean();
        instance.setApAlSetVO(apAlSetVO);
        
        apAlRow = new ApAlSetVO();
        apAlRow.setParamType("peak amplitude");
        
        alRow = new AlSetVO();
        alRow.setAlSetList(new ArrayList());
        alRow.getAlSetList().add(new ApAlSetVO(apAlRow));
    }

    /**
     * Test of init method, of class CreateApAlSetBean.
     */
    @Test
    public void test00_Init() {
        System.out.println("init");
        ApAlSetVO avo = new ApAlSetVO();
        avo.setCustomerAccount(userManageBean.getCurrentSite().getCustomerAccount());
        assertEquals(instance.getParamList().size(), 0);
        instance.init();
        assertEquals(instance.getParamList().size(), 1);
        System.out.println("init completed");
    }

    /**
     * Test of resetPage method, of class CreateApAlSetBean.
     */
    @Test
    public void test01_ResetPage() {
        System.out.println("resetPage");
        System.out.println("resetPage completed");
        instance.resetPage();
        assertEquals(instance.getParamList().size(), 1);
        System.out.println("resetPage completed");
    }

    /**
     * Test of submit method, of class CreateApAlSetBean.
     */
    @Test
    public void test02_Submit() {
        System.out.println("submit");
        instance.submit();
        System.out.println("submit completed");
    }

    /**
     * Test of deleteParamRow method, of class CreateApAlSetBean.
     */
    @Test
    public void test03_DeleteParamRow() {
        System.out.println("deleteParamRow****");
    //    instance.deleteParamRow(null);
        instance.getParamList().add(apAlRow);
        ApAlSetVO aRow = new ApAlSetVO();
        aRow.setCustomerAccount(userManageBean.getCurrentSite().getCustomerAccount());
        instance.getParamList().add(apAlRow);
        assertEquals(instance.getParamList().size(), 2);
//        instance.deleteParamRow(apAlRow);
//        assertEquals(instance.getParamList().size(), 1);
        System.out.println("deleteParamRow completed");
    }

    /**
     * Test of addParamRow method, of class CreateApAlSetBean.
     */
    @Test
    public void test03_AddParamRow() {
        System.out.println("addParamRow");
        instance.addParamRow();
        assertEquals(instance.getParamList().size(), 1);
        System.out.println("addParamRow completed");
    }

    /**
     * Test of onSensorTypeChange method, of class CreateApAlSetBean.
     */
    @Test
    public void test04_OnSensorTypeChange() {
        System.out.println("onSensorTypeChange");
        instance.onSensorTypeChange();
        assertEquals(instance.getParamList().size(), 1);
        System.out.println("onSensorTypeChange completed");
    }

    /**
     * Test of onParamTypeChange method, of class CreateApAlSetBean.
     */
    @Test
    public void test05_OnParamTypeChange() {
        System.out.println("onParamTypeChange");
        ApAlSetVO aRow = new ApAlSetVO();
        aRow.setParamType("waveform peak-to-peak");
        instance.onParamTypeChange(aRow);
        assertEquals(aRow.getParamAmpFactor(), "Peak-to-Peak");
        System.out.println("onParamTypeChange completed");
    }

    /**
     * Test of onDemodChange method, of class CreateApAlSetBean.
     */
    @Test
    public void test06_OnDemodChange() {
        System.out.println("onDemodChange");
        instance.getApAlSetVO().setDemodHighPass(20);
        instance.onDemodChange();
        assertEquals(0, instance.getApAlSetVO().getDemodHighPass(), 0);
        System.out.println("onDemodChange completed");
    }

    /**
     * Test of onFmaxChange method, of class CreateApAlSetBean.
     */
    @Test
    public void test07_OnFmaxChange() {
        System.out.println("onFmaxChange");
        instance.getApAlSetVO().setFmax(244);
        instance.onFmaxChange();
        assertEquals(625, instance.getApAlSetVO().getSampleRate(), 0);
        System.out.println("onFmaxChange completed");
    }

    /**
     * Test of onResolutionChange method, of class CreateApAlSetBean.
     */
    @Test
    public void test08_OnResolutionChange() {
        System.out.println("onResolutionChange");
        instance.getApAlSetVO().setResolution(200);
        instance.onResolutionChange();
        assertEquals(1/200*512, instance.getApAlSetVO().getPeriod(), 0);
        System.out.println("onResolutionChange completed");
    }

    /**
     * Test of onMinOrMaxFrequencyChange method, of class CreateApAlSetBean.
     */
    @Test
    public void test09_OnMinOrMaxFrequencyChange() {
        System.out.println("onMinOrMaxFrequencyChange");
        ApAlSetVO aRow = new ApAlSetVO();
        aRow.setParamType("waveform peak-to-peak");
        aRow.setMinFrequency(-1);
        aRow.setMaxFrequency(-2);
        instance.onMinOrMaxFrequencyChange(aRow);
        assertEquals(0, aRow.getMinFrequency(), 0);
        assertEquals(0.0091, aRow.getMaxFrequency(), 0.1);
        System.out.println("onMinOrMaxFrequencyChange completed");
    }

    /**
     * Test of onDwellChange method, of class CreateApAlSetBean.
     */
    @Test
    public void test10_OnDwellChange() {
        System.out.println("onDwellChange");
        instance.resetPage();
        apAlRow.setDwell(30);
        List<ApAlSetVO> alSetList = new ArrayList<>();
        alSetList.add(apAlRow);
        AlSetVO aRow = new AlSetVO();
        aRow.setAlSetName("Test AL set name");
        alRow.setAlSetList(alSetList);
        instance.getAlSetVOList().add(alRow);
        instance.getAlSetVOList().add(aRow);
        instance.getTmpAlSetVOList().add(alRow);
        instance.getTmpAlSetVOList().add(aRow);
        instance.onDwellChange(alRow, apAlRow);
        assertEquals(30, instance.getApAlSetVOFromAlSetVOListBasedOnTmpAlSetVOListObjects(alRow, apAlRow).getDwell());
        System.out.println("onDwellChange completed");
    }

    /**
     * Test of highFaultValidation method, of class CreateApAlSetBean.
     */
    @Test
    public void test18_HighFaultValidation() {
        System.out.println("highFaultValidation");
        instance.resetPage();
        apAlRow.setLowAlertActive(true);
        apAlRow.setHighAlertActive(true);
        apAlRow.setHighAlert(20);
        apAlRow.setHighFaultActive(true);
        apAlRow.setHighFault(30);
        apAlRow.setLowAlert(10);
        List<ApAlSetVO> alSetList = new ArrayList<>();
        alSetList.add(apAlRow);
        AlSetVO aRow = new AlSetVO();
        aRow.setAlSetName("Test AL set name");
        aRow.setAlSetList(new ArrayList());
        alRow.setAlSetList(alSetList);
        instance.getAlSetVOList().add(alRow);
        instance.getAlSetVOList().add(aRow);
        instance.getTmpAlSetVOList().add(alRow);
        instance.getTmpAlSetVOList().add(aRow);
        instance.alarmLimitValidation(alRow, apAlRow, 0);
        assertEquals(30, instance.getApAlSetVOFromAlSetVOListBasedOnTmpAlSetVOListObjects(alRow, apAlRow).getHighFault(), 0);
        System.out.println("highFaultValidation completed");
    }

    /**
     * Test of paramNameValidation method, of class CreateApAlSetBean.
     */
    @Test
    public void test20_ParamNameValidation() {
        System.out.println("paramNameValidation");
        ApAlSetVO aRow = new ApAlSetVO();
        aRow.setCustomerAccount(userManageBean.getCurrentSite().getCustomerAccount());
        aRow.setParamName("TEST PARAM");
        apAlRow.setParamName("TEST PARAM");
        instance.getParamList().add(aRow);
        instance.getParamList().add(apAlRow);
        instance.paramNameValidation(apAlRow);
        assertEquals(apAlRow.getParamName(), null);
        System.out.println("paramNameValidation completed");
    }

    /**
     * Test of submitValidation method, of class CreateApAlSetBean.
     */
    @Test
    public void test21_SubmitValidation_fail() {
        System.out.println("submitValidation");
        instance.getApAlSetVO().setCustomerAccount(userManageBean.getCurrentSite().getCustomerAccount());
        instance.setSiteOrGlobal("site");
        instance.getApAlSetVO().setSensorType("Acceleration");
        instance.getApAlSetVO().setApSetName("TEST AP SET");
        instance.getApAlSetVO().setFmax(0);
        instance.getApAlSetVO().setResolution(0);
        instance.getApAlSetVO().setPeriod(0);
        instance.getApAlSetVO().setSampleRate(0);
        assertFalse(instance.submitValidation());
        System.out.println("submitValidation completed");
    }

    /**
     * Test of submitValidation method, of class CreateApAlSetBean.
     */
    @Test
    public void test21_SubmitValidation_pass() {
        System.out.println("submitValidation");
        instance.getApAlSetVO().setCustomerAccount(userManageBean.getCurrentSite().getCustomerAccount());
        instance.setSiteOrGlobal("site");
        instance.getApAlSetVO().setSensorType("Acceleration");
        instance.getApAlSetVO().setApSetName("TEST AP SET");
        instance.getApAlSetVO().setFmax(20);
        instance.getApAlSetVO().setResolution(30);
        instance.getApAlSetVO().setPeriod(40);
        instance.getApAlSetVO().setSampleRate(50);
        assertTrue(instance.submitValidation());
        System.out.println("submitValidation completed");
    }

    /**
     * Test of disableParamUnits method, of class CreateApAlSetBean.
     */
    @Test
    public void test22_DisableParamUnits() {
        System.out.println("disableParamUnits");
        assertFalse(instance.disableParamUnits(apAlRow));
        apAlRow.setParamType("waveform crest factor");
        assertTrue(instance.disableFrequency(apAlRow));
        System.out.println("disableParamUnits completed");
    }

    /**
     * Test of disableParamAmpFactor method, of class CreateApAlSetBean.
     */
    @Test
    public void test23_DisableParamAmpFactor() {
        System.out.println("disableParamAmpFactor");
        assertFalse(instance.disableParamAmpFactor(apAlRow));
        apAlRow.setParamType("waveform true peak");
        assertTrue(instance.disableFrequency(apAlRow));
        System.out.println("disableParamAmpFactor completed");
    }

    /**
     * Test of disableFrequency method, of class CreateApAlSetBean.
     */
    @Test
    public void test24_DisableFrequency() {
        System.out.println("disableFrequency");
        assertTrue(instance.disableFrequency(apAlRow));
        apAlRow.setParamType("spectral band peak amplitude");
        assertFalse(instance.disableFrequency(apAlRow));
        System.out.println("disableFrequency completed");
    }

    /**
     * Test of getApAlSetVOFromAlSetVOListBasedOnTmpAlSetVOListObjects method, of class CreateApAlSetBean.
     */
    @Test
    public void test25_GetApAlSetVOFromAlSetVOListBasedOnTmpAlSetVOListObjects() {
        System.out.println("getApAlSetVOFromAlSetVOListBasedOnTmpAlSetVOListObjects");
        assertNull(instance.getApAlSetVOFromAlSetVOListBasedOnTmpAlSetVOListObjects(alRow, apAlRow));
        System.out.println("getApAlSetVOFromAlSetVOListBasedOnTmpAlSetVOListObjects completed");
    }

    /**
     * Test of getACParamUnitList method, of class CreateApAlSetBean.
     */
    @Test
    public void test26_GetACParamUnitList() {
        System.out.println("getACParamUnitList");
        List<String> list = new ArrayList<>();
        assertEquals(instance.getACParamUnitList(apAlRow), list);
        System.out.println("getACParamUnitList completed");
    }

    /**
     * Test of getParamAmpFactorList method, of class CreateApAlSetBean.
     */
    @Test
    public void test27_GetParamAmpFactorList() {
        System.out.println("getParamAmpFactorList");
        assertEquals("Peak",instance.getParamAmpFactorList(apAlRow).get(0).getValue());
        assertEquals("Peak-to-Peak",instance.getParamAmpFactorList(apAlRow).get(1).getValue());
        assertEquals("RMS",instance.getParamAmpFactorList(apAlRow).get(2).getValue());
        System.out.println("getParamAmpFactorList completed");
    }

    /**
     * Test of getFrequencyUnitsList method, of class CreateApAlSetBean.
     */
    @Test
    public void test28_GetFrequencyUnitsList() {
        System.out.println("getFrequencyUnitsList");
        assertEquals("CPM",instance.getFrequencyUnitsList().get(0).getLabel());
        assertEquals("Hz",instance.getFrequencyUnitsList().get(1).getLabel());
        assertEquals("Orders",instance.getFrequencyUnitsList().get(2).getLabel());
        System.out.println("getFrequencyUnitsList completed");
    }

    /**
     * Test of getApAlSetVO method, of class CreateApAlSetBean.
     */
    @Test
    public void test29_GetApAlSetVO() {
        System.out.println("getApAlSetVO");
        assertEquals(instance.getApAlSetVO(), new ApAlSetVO());
        System.out.println("getApAlSetVO completed");
    }

    /**
     * Test of deleteAlSet method, of class CreateApAlSetBean.
     */
    @Test
    public void test37_DeleteAlSet() {
        System.out.println("deleteAlSet");
        instance.getTmpAlSetVOList().add(alRow);
        instance.getAlSetVOList().add(alRow);
        AlSetVO alSet = new AlSetVO();
        alSet.setAlSetName("TEST NAME");
        instance.getTmpAlSetVOList().add(alSet);
        instance.getAlSetVOList().add(alSet);
        assertEquals(instance.getTmpAlSetVOList().size(), 2);
        instance.deleteAlSet(alSet);
        assertEquals(instance.getTmpAlSetVOList().size(), 1);
        System.out.println("deleteAlSet completed");
    }

    /**
     * Test of addAlSet method, of class CreateApAlSetBean.
     */
    @Test
    public void test32_AddAlSet() {
        System.out.println("addAlSet");
        assertEquals(instance.getTmpAlSetVOList().size(), 0);
        instance.addAlSet();
        assertEquals(instance.getTmpAlSetVOList().size(), 1);
        System.out.println("addAlSet completed");
    }

    /**
     * Test of alSetNameValidation method, of class CreateApAlSetBean.
     */
    @Test
    public void test33_AlSetNameValidation() {
        System.out.println("alSetNameValidation");
        instance.getTmpAlSetVOList().add(alRow);
        instance.getAlSetVOList().add(alRow);
        AlSetVO alSet = new AlSetVO();
        alRow.setAlSetName("TEST NAME");
        alSet.setAlSetName("TEST NAME");
        instance.getTmpAlSetVOList().add(alSet);
        instance.getAlSetVOList().add(alSet);
        assertEquals(instance.getTmpAlSetVOList().size(), 2);
        instance.alSetNameValidation(alSet);
        assertEquals(alSet.getAlSetName(), null);
        System.out.println("alSetNameValidation completed");
    }

    /**
     * Test of getParamList method, of class CreateApAlSetBean.
     */
    @Test
    public void test34_GetParamList() {
        System.out.println("getParamList");
        assertEquals(new ArrayList(), instance.getParamList());
        System.out.println("getParamList completed");
    }

    /**
     * Test of getTmpAlSetVOList method, of class CreateApAlSetBean.
     */
    @Test
    public void test35_GetTmpAlSetVOList() {
        System.out.println("getTmpAlSetVOList");
        assertEquals(new ArrayList(), instance.getTmpAlSetVOList());
        System.out.println("getTmpAlSetVOList completed");
    }

    /**
     * Test of onApSetTypeChange method, of class CreateApAlSetBean.
     */
    @Test
    public void test36_OnAcSensorChange() {
        System.out.println("onApSetTypeChange");
        instance.getTmpAlSetVOList().add(alRow);
        instance.getAlSetVOList().add(alRow);
        AlSetVO alSet = new AlSetVO();
        alRow.setAlSetName("TEST NAME 1");
        alSet.setAlSetName("TEST NAME 2");
        instance.getTmpAlSetVOList().add(alSet);
        instance.getAlSetVOList().add(alSet);
        instance.onApSetTypeChange();
        assertEquals(1, instance.getTmpAlSetVOList().size());
        System.out.println("onAcSensorChange done");
    }

    /**
     * Test of getACParamUnitList method, of class CreateApAlSetBean.
     */
    @Test
    public void test38_GetACParamUnitList() {
        System.out.println("getACParamUnitList");
        assertEquals(instance.getACParamUnitList(null), null);
        System.out.println("getACParamUnitList done");
    }

    /**
     * Test of getDCParamUnitList method, of class CreateApAlSetBean.
     */
    @Test
    public void test39_GetDCParamUnitList() {
        System.out.println("getDCParamUnitList");
        assertEquals(instance.getDCParamUnitList(), new ArrayList());
        System.out.println("getDCParamUnitList done");
    }

    /**
     * Test of getParamAmpFactorList method, of class CreateApAlSetBean.
     */
    @Test
    public void test40_GetParamAmpFactorList() {
        System.out.println("getParamAmpFactorList");
        assertEquals(instance.getParamAmpFactorList(null).size(), 0);
        System.out.println("getParamAmpFactorList done");
    }

    /**
     * Test of getDemodLowPassList method, of class CreateApAlSetBean.
     */
    @Test
    public void test41_GetDemodLowPassList() {
        System.out.println("getDemodLowPassList");
        assertEquals(instance.getDemodLowPassList().size(), getLowPassOptionsGreaterThanSelectedHighPass(0).size());
        System.out.println("getDemodLowPassList done");
    }

    /**
     * Test of getFrequencyUnitsList method, of class CreateApAlSetBean.
     */
    @Test
    public void test42_GetFrequencyUnitsList() {
        System.out.println("getFrequencyUnitsList");
        assertEquals(instance.getFrequencyUnitsList().size(), getFrequencyPlusUnits().size());
        System.out.println("getFrequencyUnitsList done");
    }

    /**
     * Test of getDcSensorTypeList method, of class CreateApAlSetBean.
     */
    @Test
    public void test43_GetDcSensorTypeList() {
        System.out.println("getDcSensorTypeList");
        assertEquals(instance.getDcSensorTypeList().size(), getDCSensorTypeItemList().size());
        System.out.println("getDcSensorTypeList done");
    }

    /**
     * Test of getAcSensorTypeList method, of class CreateApAlSetBean.
     */
    @Test
    public void test44_GetAcSensorTypeList() {
        System.out.println("getAcSensorTypeList");
        assertEquals(instance.getAcSensorTypeList().size(), getACSensorTypeItemList().size());
        System.out.println("getAcSensorTypeList done");
    }

    /**
     * Test of getParamTypeList method, of class CreateApAlSetBean.
     */
    @Test
    public void test45_GetParamTypeList() {
        System.out.println("getParamTypeList");
        assertEquals(instance.getParamTypeList().size(), getParamTypeItemList().size());
        System.out.println("getParamTypeList done");
    }

    /**
     * Test of getDwellList method, of class CreateApAlSetBean.
     */
    @Test
    public void test46_GetDwellList() {
        System.out.println("getDwellList");
        assertEquals(instance.getDwellList().size(), getDwellItemList().size());
        System.out.println("getDwellList done");
    }

    /**
     * Test of getFmaxList method, of class CreateApAlSetBean.
     */
    @Test
    public void test47_GetFmaxList() {
        System.out.println("getFmaxList");
        assertEquals(instance.getFmaxList().size(), getFmaxItemList().size());
        System.out.println("getFmaxList done");
    }

    /**
     * Test of getResolutionList method, of class CreateApAlSetBean.
     */
    @Test
    public void test48_GetResolutionList() {
        System.out.println("getResolutionList");
        assertEquals(instance.getResolutionList().size(), getResolutionItemList().size());
        System.out.println("getResolutionList done");
    }

    /**
     * Test of getDemodHighPassList method, of class CreateApAlSetBean.
     */
    @Test
    public void test49_GetDemodHighPassList() {
        System.out.println("getDemodHighPassList");
        assertEquals(instance.getDemodHighPassList().size(), getDemodItemList().size());
        System.out.println("getDemodHighPassList done");
    }
}
