/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.AreaClient;
import com.uptime.upcastcm.http.client.AssetClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import com.uptime.upcastcm.utils.vo.AreaVO;
import com.uptime.upcastcm.utils.vo.AssetVO;
import com.uptime.upcastcm.utils.vo.UserPreferencesVO;
import com.uptime.upcastcm.utils.vo.UserSitesVO;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.junit.runners.MethodSorters;
import org.mockito.Mock;
import static org.mockito.Mockito.mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import static org.powermock.api.mockito.PowerMockito.when;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;
import org.powermock.reflect.Whitebox;
import org.primefaces.PrimeFaces;

/**
 *
 * @author Joseph
 */
@PowerMockIgnore("jdk.internal.reflect.*")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnit4.class)
@PrepareForTest({FacesContext.class, ExternalContext.class, HttpServletRequest.class, HttpSession.class, UserManageBean.class, AreaClient.class, ServiceRegistrarClient.class, AssetClient.class, PrimeFaces.class, PrimeFaces.Ajax.class})
public class EditAssetBeanTest {
    private final EditAssetBean instance;
    
    @Mock
    ServiceRegistrarClient sc;
    
    @Mock
    AssetClient assetClient;
    
    @Mock
    AreaClient areaClient;
    
    @Mock
    FacesContext facesContext;
    
    @Mock
    ExternalContext externalContext;
    
    @Mock
    HttpServletRequest mockRequest;
    
    @Mock
    UserManageBean userManageBean;
    
    @Mock
    PrimeFaces pf;
    
    @Mock
    PrimeFaces.Ajax ax;
     
    Map<String,Object> sessionMap;
    private final List<UserSitesVO> siteList;
    String customerAccount, defaultsite;
    AssetVO assetVO;
    UserPreferencesVO userVO;
    
    public EditAssetBeanTest() {
        HttpSession session;
        UserSitesVO userSiteVO;
        
        PowerMockito.mockStatic(FacesContext.class);
        PowerMockito.mockStatic(PrimeFaces.class);
        PowerMockito.mockStatic(PrimeFaces.Ajax.class);
        pf = PowerMockito.mock(PrimeFaces.class);
        ax = PowerMockito.mock(PrimeFaces.Ajax.class);
        
        facesContext = PowerMockito.mock(FacesContext.class);
        externalContext = PowerMockito.mock(ExternalContext.class);
    
        userManageBean = PowerMockito.mock(UserManageBean.class);
        mockRequest = PowerMockito.mock(HttpServletRequest.class);
        sessionMap = new HashMap();
        sessionMap.put("userManageBean", userManageBean );
        
            
        userVO = new UserPreferencesVO();
        userVO.setCustomerAccount("77777");
        userVO.setDefaultSite(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        userVO.setLocaleDisplayName("English");
        userVO.setTimezone("US EST");
        session = PowerMockito.mock(HttpSession.class);
        session.setAttribute("userVO", userVO);
        
        when(FacesContext.getCurrentInstance()).thenReturn(facesContext);
        when(facesContext.getExternalContext()).thenReturn(externalContext);  
        when(externalContext.getRequest()).thenReturn(mockRequest);  
        when(externalContext.getSessionMap()).thenReturn(sessionMap);
        when(mockRequest.getSession()).thenReturn(session);
        when(userManageBean.getAccountUserPreferencesVO()).thenReturn(userVO);
        
        siteList = new ArrayList<>();
        userSiteVO = new UserSitesVO();
        userSiteVO.setSiteName("Test Site 1");
        userSiteVO.setSiteId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        siteList.add(userSiteVO);
        userSiteVO = new UserSitesVO();
        userSiteVO.setSiteName("Test Site 2");
        userSiteVO.setSiteId(UUID.fromString("4b2e46f7-e7e7-42be-bca3-205251863c65"));
        siteList.add(userSiteVO);
        
        assetVO = new AssetVO();
        assetVO.setCustomerAccount("77777");
        assetVO.setSiteName("Test Site 1");
        assetVO.setSiteId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        assetVO.setAreaName("Test Area 1");
        assetVO.setAreaId(UUID.fromString("57e923d1-15ed-4993-8068-72a01f91a6d9"));
        assetVO.setAssetId(UUID.fromString("e5f2a849-e5cc-49c2-ac51-1c74866e42f9"));
        assetVO.setAssetType("Test Asset Type 1");
        assetVO.setAssetTypeId(UUID.fromString("4e2e46f7-e7e7-42be-bca3-205251863c65"));
        assetVO.setAssetName("Test Asset Name 1");
        assetVO.setDescription("Test Site 1 Desc");
        assetVO.setSampleInterval(5);
        
        instance = new EditAssetBean();
    }

    /**
     * Test of init method, of class EditAssetBean.
     */
    @Test
    public void test01_Init() {
        try {
            System.out.println("init");
            instance.init();
            assertEquals(userManageBean.getUserSiteList().get(0), siteList.get(0));
            System.out.println("init Done");
        } catch (Exception ex) {
            System.out.println("testInit Exception - " + ex.toString());
        }
    }

    /**
     * Test of resetPage method, of class EditAssetBean.
     */
    @Test
    public void test02_ResetPage() {
        System.out.println("resetPage");
        instance.resetPage();
        System.out.println("resetPage Done");
    }

    /**
     * Test of presetFields method, of class EditAssetBean.
     */
    @Test
    public void test03_PresetFields_Filters_Object() {
        System.out.println("presetFields");
        instance.presetFields(null, null);
    }

    /**
     * Test of presetFields method, of class EditAssetBean.
     */
    @Test
    public void test04_PresetFields_int_Object() {
        System.out.println("presetFields");
        
    }

    /**
     * Test of presetFields method, of class EditAssetBean.
     */
    @Test
    public void test05_PresetFields_Object() {
        System.out.println("presetFields");
    }

    /**
     * Test of updateBean method, of class EditAssetBean.
     */
   @Test
    public void test06_UpdateBean() {
        System.out.println("updateBean");       
    }

    /**
     * Test of getAreasBySite method, of class EditAssetBean.
     */
    @Test
    public void test07_OnSiteSelect() {
         try {
            System.out.println("onSiteSelect - " + assetVO);
            MockitoAnnotations.initMocks(instance);
            instance.setAssetVO(assetVO);
            customerAccount = "77777";
            System.out.println("onSiteSelect 1 - " + instance.getAssetVO());
            areaClient = mock(AreaClient.class);
            sc = mock(ServiceRegistrarClient.class);
            Whitebox.setInternalState(ServiceRegistrarClient.class, "instance", sc);
            Whitebox.setInternalState(AreaClient.class, "instance", areaClient);
            System.out.println("onSiteSelect 2");
            List<AreaVO> areaList =new ArrayList<>();
            AreaVO avo = new AreaVO();
            avo.setAreaName("Test Area 1");
            avo.setAreaId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241")); 
            areaList.add(avo);
            avo = new AreaVO();
            avo.setAreaName("Test Area 2");
            avo.setAreaId(UUID.fromString("53a1bc71-94c5-4ab9-acfa-07304e74c4df"));
            areaList.add(avo);
            when(sc.getServiceHostURL(CONFIGURATION_SERVICE_NAME)).thenReturn("aaa");
            
            System.out.println("onSiteSelect 2  1- " + customerAccount);
            System.out.println("onSiteSelect 2 2 - " + assetVO.getSiteName());
            
            when(areaClient.getAreaSiteByCustomerSite("aaa", customerAccount, assetVO.getSiteId())).thenReturn(areaList);
            System.out.println("onSiteSelect 4");
            List<String> expResult = new ArrayList<>();
            expResult.add("Test Area 1");
            expResult.add("Test Area 2");
            instance.init();
            List<AreaVO> result = instance.getAreaList();
            System.out.println("instance.onSiteSelect() 5");
            assertEquals(expResult, result.stream().map(AreaVO::getAreaName).collect(Collectors.toList()));
        }
        catch (Exception ex) {
            System.out.println("testOnSiteSelect Exception - " + ex.toString());
        }
    }

   
    /**
     * Test of updateAsset method, of class EditAssetBean.
     */
    @Test
    public void test08_UpdateAsset() {
        System.out.println("updateAsset");
        try {
            instance.setAssetVO(assetVO);
            customerAccount = "77777";
            assetClient = mock(AssetClient.class);
            Whitebox.setInternalState(AssetClient.class, "instance", assetClient);
            sc = mock(ServiceRegistrarClient.class);
            Whitebox.setInternalState(ServiceRegistrarClient.class, "instance", sc);
            when(sc.getServiceHostURL(CONFIGURATION_SERVICE_NAME)).thenReturn("aaa");
            when(assetClient.updateAsset("aaa", assetVO)).thenReturn(true);
            when(PrimeFaces.current()).thenReturn(pf);
        when(pf.ajax()).thenReturn(ax);
        ax.addCallbackParam("configurationFilterFormId", new Object());
        ax.addCallbackParam("configurationResultFormId", new Object());
            instance.updateAsset();
            assertEquals(instance.getAssetVO().getAssetId(), new AssetVO().getAssetId());
        }
        catch (Exception ex) {
            System.out.println("test6_CreateAsset Exception - " + ex.toString());
        }
    }      
}
