/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package com.uptime.upcastcm.bean;

import com.sun.faces.context.RequestParameterMap;
import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.ApALByPointClient;
import com.uptime.upcastcm.http.client.AreaClient;
import com.uptime.upcastcm.http.client.FaultFrequenciesClient;
import com.uptime.upcastcm.http.client.PointClient;
import com.uptime.upcastcm.http.client.PointLocationClient;
import com.uptime.upcastcm.http.client.SiteClient;
import com.uptime.upcastcm.http.client.TrendDataClient;
import com.uptime.upcastcm.http.client.WorkOrderClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import com.uptime.upcastcm.utils.helperclass.DefaultUnits;
import com.uptime.upcastcm.utils.helperclass.Filters;
import com.uptime.upcastcm.utils.vo.ApALIdVO;
import com.uptime.upcastcm.utils.vo.AreaVO;
import com.uptime.upcastcm.utils.vo.SiteVO;
import com.uptime.upcastcm.utils.vo.UserPreferencesVO;
import com.uptime.upcastcm.utils.vo.UserSitesVO;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.faces.component.UIViewRoot;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import static org.powermock.api.mockito.PowerMockito.when;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.primefaces.PrimeFaces;
import org.primefaces.event.NodeCollapseEvent;
import org.primefaces.event.NodeExpandEvent;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.TreeNode;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.MenuModel;

/**
 *
 * @author gsingh
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(PowerMockRunner.class)
@PowerMockIgnore("jdk.internal.reflect.*")
@PrepareForTest({FacesContext.class, ExternalContext.class, UserManageBean.class, ServiceRegistrarClient.class, NavigationBean.class, SiteClient.class, PointLocationClient.class, PointClient.class, PointClient.class, AreaClient.class, TrendDataClient.class, FaultFrequenciesClient.class, ApALByPointClient.class, WorkOrderClient.class, PrimeFaces.class, PrimeFaces.Ajax.class})
public class AssetAnalysisBeanTest {

    @Mock
    ServiceRegistrarClient serviceRegistrarClient;
    @Mock
    SiteClient siteClient;
    @Mock
    PointLocationClient pointLocationClient;
    @Mock
    PointClient pointClient;
    @Mock
    AreaClient areaClient;
    @Mock
    TrendDataClient trendDataClient;
    @Mock
    FaultFrequenciesClient faultFrequenciesClient;
    @Mock
    ApALByPointClient apALByPointClient;
    @Mock
    WorkOrderClient workOrderClient;
    @Mock
    FacesContext facesContext;
    @Mock
    ExternalContext externalContext;
    @Mock
    HttpServletRequest request;
    @Mock
    HttpSession session;
    @Mock
    UIViewRoot root;
    @Mock
    NavigationBean navigationBean;
    @Mock
    UserManageBean userManageBean;

    private final AssetAnalysisBean instance;
    Map<String, Object> sessionMap;
    private final String customerAccount;
    MenuModel breadcrumbModel;
    private final List<UserSitesVO> userSiteList;
    private final Filters filters;
    RequestParameterMap requestParameterMap;

    public AssetAnalysisBeanTest() {
        customerAccount = "77777";
        PowerMockito.mockStatic(FacesContext.class);
        PowerMockito.mockStatic(ServiceRegistrarClient.class);
        PowerMockito.mockStatic(SiteClient.class);
        PowerMockito.mockStatic(PointLocationClient.class);
        PowerMockito.mockStatic(PointClient.class);
        PowerMockito.mockStatic(AreaClient.class);
        PowerMockito.mockStatic(TrendDataClient.class);
        PowerMockito.mockStatic(FaultFrequenciesClient.class);
        PowerMockito.mockStatic(ApALByPointClient.class);
        PowerMockito.mockStatic(WorkOrderClient.class);

        facesContext = PowerMockito.mock(FacesContext.class);
        externalContext = PowerMockito.mock(ExternalContext.class);
        request = PowerMockito.mock(HttpServletRequest.class);
        session = PowerMockito.mock(HttpSession.class);
        root = PowerMockito.mock(UIViewRoot.class);
        navigationBean = PowerMockito.mock(NavigationBean.class);
        userManageBean = PowerMockito.mock(UserManageBean.class);

        siteClient = PowerMockito.mock(SiteClient.class);
        pointLocationClient = PowerMockito.mock(PointLocationClient.class);
        pointClient = PowerMockito.mock(PointClient.class);
        areaClient = PowerMockito.mock(AreaClient.class);
        serviceRegistrarClient = PowerMockito.mock(ServiceRegistrarClient.class);
        trendDataClient = PowerMockito.mock(TrendDataClient.class);
        faultFrequenciesClient = PowerMockito.mock(FaultFrequenciesClient.class);
        apALByPointClient = PowerMockito.mock(ApALByPointClient.class);
        workOrderClient = PowerMockito.mock(WorkOrderClient.class);
        requestParameterMap = PowerMockito.mock(RequestParameterMap.class);

        sessionMap = new HashMap();
        sessionMap.put("navigationBean", navigationBean);
        sessionMap.put("userManageBean", userManageBean);
        session.setAttribute("navigationBean", navigationBean);
        session.setAttribute("userManageBean", userManageBean);

        userSiteList = new ArrayList<>();
        UserSitesVO uvo = new UserSitesVO();
        uvo.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        uvo.setSiteName("Test Site 1");
        uvo.setCustomerAccount("77777");
        userSiteList.add(uvo);

        when(ServiceRegistrarClient.getInstance()).thenReturn(serviceRegistrarClient);
        when(SiteClient.getInstance()).thenReturn(siteClient);
        when(PointLocationClient.getInstance()).thenReturn(pointLocationClient);
        when(PointClient.getInstance()).thenReturn(pointClient);
        when(AreaClient.getInstance()).thenReturn(areaClient);
        when(TrendDataClient.getInstance()).thenReturn(trendDataClient);
        when(FaultFrequenciesClient.getInstance()).thenReturn(faultFrequenciesClient);
        when(WorkOrderClient.getInstance()).thenReturn(workOrderClient);
        when(ApALByPointClient.getInstance()).thenReturn(apALByPointClient);
        when(FacesContext.getCurrentInstance()).thenReturn(facesContext);
        when(facesContext.getExternalContext()).thenReturn(externalContext);
        when(externalContext.getSessionMap()).thenReturn(sessionMap);
        when(facesContext.getExternalContext().getRequest()).thenReturn(request);

        when(facesContext.getExternalContext().getRequestParameterMap()).thenReturn(requestParameterMap);
        when(requestParameterMap.get("graphDataWaveForm:iph")).thenReturn("Wave Graph");
        when(requestParameterMap.get("graphDataSpecForm:iph")).thenReturn("Spectrum Graph");
        when(requestParameterMap.get("hiddenForm1:windowId")).thenReturn("window1");
        when(requestParameterMap.get("trendGraphActionBarFormId:windowId")).thenReturn("window1");
        when(requestParameterMap.get("trendGraphActionBarFormId:windowUrlId")).thenReturn("window1");
        when(requestParameterMap.get("createWorkOrderAnalysisFormId:windowUrlId")).thenReturn("window1");
        when(requestParameterMap.get("actionTopBarFormId:windowUrlId")).thenReturn("window1");
        when(requestParameterMap.get("editAlarmLimitFormId:windowUrlId")).thenReturn("window1");

        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("navigationBean")).thenReturn(navigationBean);
        when(session.getAttribute("userManageBean")).thenReturn(userManageBean);
        when(userManageBean.getUserSiteList()).thenReturn(userSiteList);
        when(userManageBean.getCurrentSite()).thenReturn(uvo);
        when(userManageBean.getZoneId()).thenReturn(ZoneOffset.UTC);
        when(userManageBean.getAccountDefaultUnits()).thenReturn(new DefaultUnits());
        filters = new Filters();
        filters.setCountry("Test Country 1");
        filters.setRegion("Test Region 1");
        filters.setSite(new SelectItem(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), "Test Site 1"));
        filters.setArea(new SelectItem(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"), "Test Area 1"));
        filters.setAsset(new SelectItem(UUID.fromString("f33fcf5a-5f93-4504-bc09-74a61a460bf0"), "Test Asset 1"));
        when(userManageBean.getSelectedFilters()).thenReturn(filters);
        when(facesContext.getViewRoot()).thenReturn(root);
        PowerMockito.mockStatic(PrimeFaces.class);
        PowerMockito.mockStatic(PrimeFaces.Ajax.class);
        PrimeFaces pf = PowerMockito.mock(PrimeFaces.class);
        PrimeFaces.Ajax ax = PowerMockito.mock(PrimeFaces.Ajax.class);
        when(pf.current()).thenReturn(pf);
        when(pf.ajax()).thenReturn(ax);

        UserPreferencesVO userVO = new UserPreferencesVO();
        userVO.setDefaultSite(UUID.randomUUID());
        userVO.setCustomerAccount("77777");
        when(session.getAttribute("userVO")).thenReturn(userVO);
        UserSitesVO usvo = new UserSitesVO();
        usvo.setCustomerAccount(customerAccount);
        when(userManageBean.getCurrentSite()).thenReturn(usvo);

        instance = new AssetAnalysisBean();

    }

    @Before
    public void setUp() {

        UserSitesVO uvo = new UserSitesVO();
        uvo.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        uvo.setSiteName("Test Site 1");
        uvo.setCustomerAccount(customerAccount);
        when(userManageBean.getCurrentSite()).thenReturn(uvo);
        breadcrumbModel = new DefaultMenuModel();
        when(navigationBean.getBreadcrumbModel()).thenReturn(breadcrumbModel);
        NodeExpandEvent nee = PowerMockito.mock(NodeExpandEvent.class);
        TreeNode tn = PowerMockito.mock(TreeNode.class);
        when(tn.getData()).thenReturn(new String());
        List<TreeNode> tnList = new ArrayList<>();
        tnList.add(tn);
        when(navigationBean.getTreeNodeRoot()).thenReturn(tn);
        when(navigationBean.getTreeNodeRoot().getChildren()).thenReturn(tnList);
        instance.setSelectedFilters();
        instance.getSelectedFilters().setCountry("Test Country 1");
        instance.getSelectedFilters().setRegion("Test Region 1");
        instance.getSelectedFilters().setSite(new SelectItem(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), "site"));
        instance.getSelectedFilters().setArea(new SelectItem(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), "area"));
        instance.getSelectedFilters().setAsset(new SelectItem(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), "asset"));
        instance.getSelectedFilters().setPointLocation(new SelectItem(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), "Test Point Location 1"));
        instance.getSelectedFilters().setPoint(new SelectItem(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), "1 Acceleration"));
        instance.getSelectedFilters().setApSet(new SelectItem(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), "Ap Set 1"));
        instance.getSelectedFilters().setAlSetParam(new SelectItem(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), "Overall"));
    }

    /**
     * Test of init method, of class AssetAnalysisBean.
     */
    @Test
    public void test01_Init() {
        System.out.println("init");
        MockitoAnnotations.initMocks(instance);
        List<SiteVO> list = new ArrayList();
        SiteVO s1 = new SiteVO();
        s1.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        s1.setSiteName("Test Site 1");
        s1.setCountry("Test Country 1");
        s1.setRegion("Test Region 1");
        s1.setCustomerAccount(customerAccount);
        list.add(s1);
        List<AreaVO> areaList = new ArrayList();
        AreaVO avo = new AreaVO();
        avo.setAreaId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        avo.setAreaName("Test Area 1");
        avo.setClimateControlled(false);
        avo.setCustomerAccount(customerAccount);
        avo.setDescription("Test Description 1");
        avo.setEnvironment("Test Environment 1");
        avo.setScheduleVOList(new ArrayList());
        avo.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        avo.setSiteName("Test Site 1");
        areaList.add(avo);
        PowerMockito.mockStatic(PrimeFaces.class);
        PowerMockito.mockStatic(PrimeFaces.Ajax.class);
        PrimeFaces pf = PowerMockito.mock(PrimeFaces.class);
        PrimeFaces.Ajax ax = PowerMockito.mock(PrimeFaces.Ajax.class);
        when(pf.current()).thenReturn(pf);
        when(pf.ajax()).thenReturn(ax);
        when(serviceRegistrarClient.getServiceHostURL(CONFIGURATION_SERVICE_NAME)).thenReturn(CONFIGURATION_SERVICE_NAME);
        when(siteClient.getSiteRegionCountryByCustomer(CONFIGURATION_SERVICE_NAME, customerAccount)).thenReturn(list);
        when(areaClient.getAreaSiteByCustomerSite(CONFIGURATION_SERVICE_NAME, customerAccount, UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"))).thenReturn(areaList);
        instance.init();
        assertEquals(instance.getSelectedFilters().getRegion(), s1.getRegion());
    }

    @Test
    public void test02_Init() {
        System.out.println("test02_Init");
        PowerMockito.mockStatic(PrimeFaces.class);
        PowerMockito.mockStatic(PrimeFaces.Ajax.class);
        PrimeFaces pf = PowerMockito.mock(PrimeFaces.class);
        PrimeFaces.Ajax ax = PowerMockito.mock(PrimeFaces.Ajax.class);
        when(pf.current()).thenReturn(pf);
        when(pf.ajax()).thenReturn(ax);
        instance.init();
        assertNotEquals(instance.getSelectedFilters().getRegion(), null);
        System.out.println("test02_Init completed");
    }

    /**
     * Test of resetPage method, of class AssetAnalysisBean.
     */
    @Test
    public void testResetPage() {
        System.out.println("testResetPage");
        instance.resetPage();
    }

    /**
     * Test of onRegionChange method, of class AssetAnalysisBean.
     */
    @Test
    public void testOnRegionChange() {
        System.out.println("testOnRegionChange");
        instance.onRegionChange(true);
    }

    /**
     * Test of onCountryChange method, of class AssetAnalysisBean.
     */
    @Test
    public void testOnCountryChange() {
        System.out.println("testOnCountryChange");
        instance.onCountryChange(true);
    }

    /**
     * Test of onSiteChange method, of class AssetAnalysisBean.
     */
    @Test
    public void testOnSiteChange() {
        System.out.println("testOnSiteChange");
        instance.onSiteChange(true);
    }

    /**
     * Test of onAreaChange method, of class AssetAnalysisBean.
     */
    @Test
    public void testOnAreaChange() {
        System.out.println("testOnAreaChange");
        instance.onAreaChange(true);
    }

    /**
     * Test of onAssetChange method, of class AssetAnalysisBean.
     */
    @Test
    public void testOnAssetChange() {
        System.out.println("testOnAssetChange");
        instance.onAssetChange(true);
    }

    /**
     * Test of onPointLocationChange method, of class AssetAnalysisBean.
     */
    @Test
    public void testOnPointLocationChange() {
        System.out.println("testOnPointLocationChange");
        instance.onPointLocationChange(true);
    }

    /**
     * Test of onPointChange method, of class AssetAnalysisBean.
     */
    @Test
    public void testOnPointChange() {
        System.out.println("testOnPointChange");
        instance.onPointChange(true);
    }

    /**
     * Test of ApSetChange method, of class AssetAnalysisBean.
     */
    @Test
    public void testOnApSetChange() {
        System.out.println("testOnApSetChange");
        instance.onApSetChange(true);
    }

    /**
     * Test of onAlSetParamChange method, of class AssetAnalysisBean.
     */
    @Test
    public void testAlSetParamChange() {
        System.out.println("testOnAlSetParamChange");
        instance.onAlSetParamChange(true);
    }

    /**
     * Test of updateUI method, of class AssetAnalysisBean.
     */
    @Test
    public void testUpdateUI() {
        System.out.println("testUpdateUI");
        PowerMockito.mockStatic(PrimeFaces.class);
        PowerMockito.mockStatic(PrimeFaces.Ajax.class);
        PrimeFaces pf = PowerMockito.mock(PrimeFaces.class);
        PrimeFaces.Ajax ax = PowerMockito.mock(PrimeFaces.Ajax.class);
        when(pf.current()).thenReturn(pf);
        when(pf.ajax()).thenReturn(ax);
        ax.addCallbackParam("trendGraphActionBarFormId", new Object());
        instance.updateUI("content");
        System.out.println("test10_UpdateUI completed");
    }

    /**
     * Test of updateBreadcrumbModel method, of class AssetAnalysisBean.
     */
    @Test
    public void testUpdateBreadcrumbModel() {
        System.out.println("testUpdateBreadcrumbModel");
        instance.updateBreadcrumbModel();
        DefaultMenuItem item = (DefaultMenuItem) breadcrumbModel.getElements().get(0);
        assertEquals(item.getIcon(), "pi pi-fw pi-chart-line");
        System.out.println("test11_UpdateBreadcrumbModel completed");
    }

    /**
     * Test of treeContentMenuBuilder method, of class AssetAnalysisBean.
     */
    @Test
    public void testTreeContentMenuBuilder() {
        System.out.println("testTreeContentMenuBuilder");
        test01_Init();
        PowerMockito.mockStatic(PrimeFaces.class);
        PowerMockito.mockStatic(PrimeFaces.Ajax.class);
        PrimeFaces pf = PowerMockito.mock(PrimeFaces.class);
        PrimeFaces.Ajax ax = PowerMockito.mock(PrimeFaces.Ajax.class);
        when(pf.current()).thenReturn(pf);
        when(pf.ajax()).thenReturn(ax);
        ax.addCallbackParam("leftTreeFormId", new Object());

        instance.treeContentMenuBuilder(ROOT, "", null, null);
        TreeNode tn = PowerMockito.mock(TreeNode.class);
        instance.treeContentMenuBuilder("site", "", tn, null);
        System.out.println("test12_TreeContentMenuBuilder completed");
    }

    /**
     * Test of onNodeExpand method, of class AssetAnalysisBean.
     */
    @Test
    public void testOnNodeExpand() {
        System.out.println("testOnNodeExpand");

        NodeExpandEvent nee = PowerMockito.mock(NodeExpandEvent.class);
        TreeNode tn = PowerMockito.mock(TreeNode.class);

        when(nee.getTreeNode()).thenReturn(tn);
        when(tn.getType()).thenReturn("region");
        when(tn.getData()).thenReturn("Test Region 1");

        List<TreeNode> tnList = new ArrayList<>();
        tnList.add(PowerMockito.mock(TreeNode.class));
        when(tn.getChildren()).thenReturn(tnList);

        test01_Init();

        instance.onNodeExpand(nee);
        assertEquals(nee.getTreeNode().getChildren().size(), 1);

        System.out.println("test13_OnNodeExpand completed");
    }

    /**
     * Test of onNodeCollapse method, of class AssetAnalysisBean.
     */
    @Test
    public void testOnNodeCollapse() {
        System.out.println("testOnNodeCollapse");
        NodeCollapseEvent nce = PowerMockito.mock(NodeCollapseEvent.class);
        TreeNode tn = PowerMockito.mock(TreeNode.class);

        when(nce.getTreeNode()).thenReturn(tn);
        when(tn.getType()).thenReturn("region");
        when(tn.getData()).thenReturn("Test Region 1");

        List<TreeNode> tnList = new ArrayList<>();
        tnList.add(PowerMockito.mock(TreeNode.class));
        when(tn.getChildren()).thenReturn(tnList);

        instance.onNodeCollapse(nce);
        assertEquals(nce.getTreeNode().getChildren().size(), 1);

        System.out.println("test14_OnNodeCollapse completed");
    }

    /**
     * Test of onNodeSelect method, of class AssetAnalysisBean.
     */
    @Test
    public void testOnNodeSelect() {
        System.out.println("testOnNodeSelect");
        NodeSelectEvent nse = PowerMockito.mock(NodeSelectEvent.class);
        TreeNode tn = PowerMockito.mock(TreeNode.class);

        when(nse.getTreeNode()).thenReturn(tn);
        when(tn.getType()).thenReturn("region");
        when(tn.getData()).thenReturn("region");

        List<TreeNode> tnList = new ArrayList<>();
        tnList.add(PowerMockito.mock(TreeNode.class));
        when(tn.getChildren()).thenReturn(tnList);
        when(tn.getParent()).thenReturn(tn);

        instance.onNodeSelect(nse);
        assertEquals(nse.getTreeNode().getChildren().size(), 1);

        System.out.println("test15_OnNodeSelect completed");
    }

    /**
     * Test of setRegionCountrySiteMap method, of class AssetAnalysisBean.
     */
    @Test
    public void testSetRegionCountrySiteMap() {
        System.out.println("testSetRegionCountrySiteMap");
        List<SiteVO> list = new ArrayList();
        SiteVO s1 = new SiteVO();
        s1.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        s1.setSiteName("Test Site 1");
        s1.setRegion("Test Region 1");
        s1.setCountry("Test Country 1");
        s1.setCustomerAccount(customerAccount);
        list.add(s1);
        when(serviceRegistrarClient.getServiceHostURL(CONFIGURATION_SERVICE_NAME)).thenReturn(CONFIGURATION_SERVICE_NAME);
        when(siteClient.getSiteRegionCountryByCustomer(CONFIGURATION_SERVICE_NAME, customerAccount)).thenReturn(list);
        instance.setRegionCountrySiteMap();
        System.out.println("test16_SetRegionCountrySiteMap completed");
    }
}
