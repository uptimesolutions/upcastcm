/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.APALSetClient;
import com.uptime.upcastcm.http.client.ApALByPointClient;
import com.uptime.upcastcm.http.client.AreaClient;
import com.uptime.upcastcm.http.client.AssetClient;
import com.uptime.upcastcm.http.client.HwUnitToPointClient;
import com.uptime.upcastcm.http.client.PointLocationClient;
import com.uptime.upcastcm.http.client.TachometerClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import com.uptime.upcastcm.utils.helperclass.PresetUtil;
import com.uptime.upcastcm.utils.singletons.CommonUtilSingleton;
import com.uptime.upcastcm.utils.vo.ApAlSetVO;
import com.uptime.upcastcm.utils.vo.AreaVO;
import com.uptime.upcastcm.utils.vo.AssetVO;
import com.uptime.upcastcm.utils.vo.ChannelVO;
import com.uptime.upcastcm.utils.vo.PointLocationVO;
import com.uptime.upcastcm.utils.vo.PointVO;
import com.uptime.upcastcm.utils.vo.TachometerVO;
import com.uptime.upcastcm.utils.vo.UserSitesVO;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.junit.runners.MethodSorters;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import static org.powermock.api.mockito.PowerMockito.when;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;
import org.powermock.reflect.Whitebox;
import org.primefaces.PrimeFaces;

/**
 *
 * @author madhavi
 */

@RunWith(PowerMockRunner.class)
@PowerMockIgnore("jdk.internal.reflect.*")
@PrepareForTest({FacesContext.class, ExternalContext.class, UserManageBean.class, PrimeFaces.class, PrimeFaces.Ajax.class, ServiceRegistrarClient.class, APALSetClient.class, ApALByPointClient.class, HwUnitToPointClient.class, AssetClient.class, AreaClient.class, PointLocationClient.class, PresetUtil.class, CommonUtilSingleton.class, TachometerClient.class})
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@PowerMockRunnerDelegate(JUnit4.class)
public class AddChannelBeanTest {
    
    @Mock
    ServiceRegistrarClient serviceRegistrarClient;
    
    @Mock
    APALSetClient aPALSetClient;
            
    @Mock
    ApALByPointClient apALByPointClient;
    
    @Mock
    HwUnitToPointClient hwUnitToPointClient; 
    
    @Mock
    AssetClient assetClient; 
    
    @Mock
    AreaClient areaClient;
    
    @Mock
    PointLocationClient pointLocationClient;
    
    @Mock
    FacesContext facesContext;
    
    @Mock
    ExternalContext externalContext;
    
    @Mock
    UserManageBean userManageBean;
    
    @Mock
    PrimeFaces pf;
    
    @Mock
    PrimeFaces.Ajax ax;
    
    @Mock
    PresetUtil presetUtil;
    
    @Mock
    CommonUtilSingleton commonUtilSingleton;
    
    @Mock
    TachometerClient tachometerClient;
    
    private static AddChannelBean instance;
    private Map<String,Object> sessionMap;
    private static List<ApAlSetVO> globalApAlSetBySensorList;
    private static ChannelVO channelVO;
    
    public AddChannelBeanTest() {
        UserSitesVO usvo;
        List<SelectItem> apSetSensorTypeList = new ArrayList(), alSetVOList = new ArrayList();
        
        List<ApAlSetVO> siteApAlSetBySensorList = new ArrayList();
        List<AreaVO> areaList = new ArrayList();
        List<AssetVO> assetList = new ArrayList();
        List<TachometerVO> globalTachList = new ArrayList();
        List<TachometerVO> siteTachList = new ArrayList();
        List<PointLocationVO> plList = new ArrayList();
        List<PointVO> ptList = new ArrayList();
        
        SelectItem si = new SelectItem();
        si.setLabel("Test AP Set Name");
        si.setValue(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"));
        apSetSensorTypeList.add(si);
        
        globalApAlSetBySensorList = new ArrayList();
        ApAlSetVO apalVO = new ApAlSetVO();
        apalVO.setApSetId(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"));
        apalVO.setAlSetId(UUID.fromString("b9c92679-9376-40fc-9498-083c104edca6"));
        apalVO.setApSetName("Test Ap Set Name");
        apalVO.setAlSetName("Test Al Set Name");
        globalApAlSetBySensorList.add(apalVO);
        
        AreaVO avo = new AreaVO();
        avo.setAreaId(UUID.fromString("2ef129fa-2d05-4dec-a84d-ed8f386ac1d0"));
        avo.setAreaName("Test Area Name");
        areaList.add(avo);
        
        TachometerVO tvo = new TachometerVO();
        tvo.setTachId(UUID.fromString("435f1a2d-1c81-4abf-a370-43dcecfde963"));
        tvo.setTachName("Test Tach Name");
        tvo.setTachReferenceUnits("RPM");
        globalTachList.add(tvo);
        
        SelectItem alset = new SelectItem();
        alset.setValue("b9c92679-9376-40fc-9498-083c104edca6");
        alset.setLabel("Test Al Set Name");
        alSetVOList.add(alset);
        
        AssetVO assetVO = new AssetVO();
        assetVO.setAreaId(UUID.fromString("2ef129fa-2d05-4dec-a84d-ed8f386ac1d0"));
        assetVO.setAreaName("Test Area Name");
        assetVO.setAssetId(UUID.fromString("0361880c-d0d8-4eb7-91be-a436eb735c75"));
        assetVO.setAssetName("Test Asset Name");
        
        PointLocationVO plvo = new PointLocationVO();
        plvo.setPointLocationName("Test PL Name");
        
        PointVO pvo = new PointVO();
        pvo.setPointName("Test PL Name VIBE");
        pvo.setSensorType("Acceleration");
        pvo.setApSetId(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"));
        pvo.setAlSetId(UUID.fromString("b9c92679-9376-40fc-9498-083c104edca6"));
        pvo.setApSetName("Test Ap Set Name");
        pvo.setAlSetName("Test Al Set Name");
        pvo.setApAlSetVOs(globalApAlSetBySensorList);
        ptList.add(pvo);
        plvo.setPointList(ptList);
        plList.add(plvo);
        assetVO.setPointLocationList(plList);
        assetVO.setAssetName("Test Asset Name");
        assetList.add(assetVO);
        
        PowerMockito.mockStatic(ServiceRegistrarClient.class);
        PowerMockito.mockStatic(APALSetClient.class);
        PowerMockito.mockStatic(ApALByPointClient.class);
        PowerMockito.mockStatic(HwUnitToPointClient.class);
        PowerMockito.mockStatic(AssetClient.class);
        PowerMockito.mockStatic(AreaClient.class);        
        PowerMockito.mockStatic(PointLocationClient.class);
        PowerMockito.mockStatic(FacesContext.class);
        PowerMockito.mockStatic(ExternalContext.class);
        PowerMockito.mockStatic(UserManageBean.class);
        PowerMockito.mockStatic(PrimeFaces.class);
        PowerMockito.mockStatic(PrimeFaces.Ajax.class);
        PowerMockito.mockStatic(PresetUtil.class);
        PowerMockito.mockStatic(CommonUtilSingleton.class);
        PowerMockito.mockStatic(TachometerClient.class);
        
        serviceRegistrarClient = PowerMockito.mock(ServiceRegistrarClient.class);
        aPALSetClient = PowerMockito.mock(APALSetClient.class);
        apALByPointClient = PowerMockito.mock(ApALByPointClient.class);
        hwUnitToPointClient = PowerMockito.mock(HwUnitToPointClient.class);
        assetClient = PowerMockito.mock(AssetClient.class);
        areaClient = PowerMockito.mock(AreaClient.class);
        pointLocationClient = PowerMockito.mock(PointLocationClient.class);
        facesContext = PowerMockito.mock(FacesContext.class);
        externalContext = PowerMockito.mock(ExternalContext.class);
        userManageBean = PowerMockito.mock(UserManageBean.class);
        presetUtil = PowerMockito.mock(PresetUtil.class);
        commonUtilSingleton = PowerMockito.mock(CommonUtilSingleton.class);
        tachometerClient = PowerMockito.mock(TachometerClient.class);
        
        pf = PowerMockito.mock(PrimeFaces.class);
        ax = PowerMockito.mock(PrimeFaces.Ajax.class);
        
        when(PrimeFaces.current()).thenReturn(pf);
        when(pf.ajax()).thenReturn(ax);
        
        sessionMap = new HashMap();
        sessionMap.put("userManageBean", userManageBean);
        
        when(FacesContext.getCurrentInstance()).thenReturn(facesContext);
        when(facesContext.getExternalContext()).thenReturn(externalContext);
        when(externalContext.getSessionMap()).thenReturn(sessionMap);
        
        usvo = new UserSitesVO();
        usvo.setCustomerAccount("FEDEX EXPRESS");
        usvo.setSiteId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        when(userManageBean.getCurrentSite()).thenReturn(usvo);
        
        when(ServiceRegistrarClient.getInstance()).thenReturn(serviceRegistrarClient);
        when(serviceRegistrarClient.getServiceHostURL(CONFIGURATION_SERVICE_NAME)).thenReturn("HOST");
        when(serviceRegistrarClient.getServiceHostURL(PRESET_SERVICE_NAME)).thenReturn("HOST");
        when(APALSetClient.getInstance()).thenReturn(aPALSetClient);
        when(ApALByPointClient.getInstance()).thenReturn(apALByPointClient);
        when(HwUnitToPointClient.getInstance()).thenReturn(hwUnitToPointClient);
        when(AssetClient.getInstance()).thenReturn(assetClient);
        when(AreaClient.getInstance()).thenReturn(areaClient);
        when(PointLocationClient.getInstance()).thenReturn(pointLocationClient);
        when(userManageBean.getPresetUtil()).thenReturn(presetUtil);
        when(CommonUtilSingleton.getInstance()).thenReturn(commonUtilSingleton);
        when(TachometerClient.getInstance()).thenReturn(tachometerClient);
        
        when(presetUtil.populateApSetSensorTypeList(userManageBean, userManageBean.getCurrentSite().getCustomerAccount(), userManageBean.getCurrentSite().getSiteId(), "Acceleration")).thenReturn(apSetSensorTypeList);
        when(presetUtil.getGlobalApAlSetsByCustomerSensorType(userManageBean.getCurrentSite().getCustomerAccount(), "Acceleration")).thenReturn(globalApAlSetBySensorList);
        when(presetUtil.getSiteApAlSetsByCustomerSiteIdSensorType(userManageBean.getCurrentSite().getCustomerAccount(), userManageBean.getCurrentSite().getSiteId(), "Acceleration")).thenReturn(siteApAlSetBySensorList);
        when(presetUtil.populateAlSetByApAlsetList(globalApAlSetBySensorList)).thenReturn(alSetVOList);
        when(areaClient.getAreaSiteByCustomerSite("HOST", userManageBean.getCurrentSite().getCustomerAccount(), userManageBean.getCurrentSite().getSiteId())).thenReturn(areaList);
        when(assetClient.getAssetSiteAreaByCustomerSiteArea("HOST", userManageBean.getCurrentSite().getCustomerAccount(), userManageBean.getCurrentSite().getSiteId(), avo.getAreaId())).thenReturn(assetList);
        when(commonUtilSingleton.getPointNameBySensorType("Acceleration")).thenReturn("VIBE");
        when(tachometerClient.getGlobalTachometersByPK("HOST", userManageBean.getCurrentSite().getCustomerAccount(), UUID.fromString("435f1a2d-1c81-4abf-a370-43dcecfde963"))).thenReturn(globalTachList);
        when(tachometerClient.getSiteTachometersByPK("HOST", userManageBean.getCurrentSite().getCustomerAccount(), userManageBean.getCurrentSite().getSiteId(), UUID.fromString("435f1a2d-1c81-4abf-a370-43dcecfde963"))).thenReturn(siteTachList);
        when(aPALSetClient.getSiteApAlSetsByCustomerSiteIdApSetId("HOST", userManageBean.getCurrentSite().getCustomerAccount(), userManageBean.getCurrentSite().getSiteId(), UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"))).thenReturn(siteApAlSetBySensorList);
        when(aPALSetClient.getGlobalApAlSetsByCustomerApSetId("HOST", userManageBean.getCurrentSite().getCustomerAccount(), UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"))).thenReturn(globalApAlSetBySensorList);
        
        instance = new AddChannelBean();
    }
    
    @Before
    public void setUp() {
        channelVO = new ChannelVO();
        channelVO.setCustomerAccount("FEDEX EXPRESS");
        channelVO.setSiteId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        instance.setChannelVO(channelVO);
        instance.presetFields(new ArrayList<>());
    }

    /**
     * Test of init method, of class AddChannelBean.
     */
    @Test
    public void test01_Init() {
        System.out.println("init");
        instance.init();
        System.out.println("init done");
    }

    /**
     * Test of resetPage method, of class AddChannelBean.
     */
    @Test
    public void test02_ResetPage() {
        System.out.println("resetPage");
        instance.resetPage();
        System.out.println("resetPage done");
    }

    /**
     * Test of updateBean method, of class CreateTS1Bean.
     */
    @Test
    public void test03_UpdateBean() {
        System.out.println("updateBean");
        instance.updateBean();
        System.out.println("updateBean done");
    }

    /**
     * Test of onPointTypeChange method, of class CreateTS1Bean.
     */
    @Test
    public void test04_OnPointTypeChange() {
        System.out.println("onPointTypeChange");
        instance.getChannelVO().setCustomized(true);
        instance.onPointTypeChange();
        assertEquals(instance.getChannelVO().isCustomized(), false);
        System.out.println("onPointTypeChange done");
    }

    /**
     * Test of onSensorSelect method, of class CreateTS1Bean.
     */
    @Test
    public void test05_OnSensorSelect() {
        System.out.println("onSensorSelect");
        instance.getChannelVO().setSensorType("Acceleration");
        instance.onSensorSelect();
        assertEquals(1, instance.getCustomerApSetVOList().size());
        System.out.println("onSensorSelect done");
    }

    /**
     * Test of onAreaChange method, of class CreateTS1Bean.
     */
    @Test
    public void test06_OnAreaChange() {
        System.out.println("onAreaChange");
        instance.getChannelVO().setAreaId(UUID.fromString("2ef129fa-2d05-4dec-a84d-ed8f386ac1d0"));
        instance.onAreaChange();
        assertEquals(instance.getChannelVO().getAreaName(), "Test Area Name");
        System.out.println("onAreaChange done");
    }

    /**
     * Test of onAssetChange method, of class CreateTS1Bean.
     */
    @Test
    public void test07_OnAssetChange() {
        System.out.println("onAssetChange");
        List<SelectItem> newAssetVOList= new ArrayList<>();
        newAssetVOList.add(new SelectItem());
        newAssetVOList.get(0).setValue(UUID.fromString("0361880c-d0d8-4eb7-91be-a436eb735c75"));
        newAssetVOList.get(0).setLabel("Test Asset Name");
        Whitebox.setInternalState(instance, "assetList", newAssetVOList);
        instance.getChannelVO().setAssetId(UUID.fromString("0361880c-d0d8-4eb7-91be-a436eb735c75"));
        instance.onAssetChange();
        assertEquals(instance.getChannelVO().getAssetName(), "Test Asset Name");
        System.out.println("onAssetChange done");
    }

    /**
     * Test of onPointLocationChange method, of class CreateTS1Bean.
     */
    @Test
    public void test08_OnPointLocationChange() {
        System.out.println("onPointLocationChange");
        instance.getChannelVO().setSensorType("Acceleration");
        instance.getChannelVO().setPointLocationName("Test PL Name");
        instance.onPointLocationChange();
        assertEquals(instance.getChannelVO().getPointName(), "Test PL Name VIBE");
        System.out.println("onPointLocationChange done");
    }

    /**
     * Test of onTachChange method, of class CreateTS1Bean.
     */
    @Test
    public void test09_OnTachChange() {
        System.out.println("onTachChange");
        instance.getChannelVO().setSensorType("Acceleration");
        instance.getChannelVO().setTachId(UUID.fromString("435f1a2d-1c81-4abf-a370-43dcecfde963"));
        instance.onTachChange();
        assertEquals(instance.getChannelVO().getTachReferenceUnits(), "RPM");
        System.out.println("onPointLocationChange done");
    }

    /**
     * Test of onApSetNameSelect method, of class CreateTS1Bean.
     */
    @Test
    public void test10_OnApSetNameSelect() {
        System.out.println("onApSetNameSelect");
        instance.getChannelVO().setSensorType("Acceleration");
        instance.getChannelVO().setApSetId(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"));
        instance.onApSetNameSelect();
        assertEquals(instance.getChannelVO().getApSetName(), "Test Ap Set Name");
        System.out.println("onApSetNameSelect done");
    }
    
    /**
     * Test of onAlSetNameSelect method, of class CreateTS1Bean.
     */
    @Test
    public void test11_OnAlSetNameSelect() {
        System.out.println("onAlSetNameSelect");
        instance.getChannelVO().setSensorType("Acceleration");
        instance.getChannelVO().setApSetId(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"));
        instance.getChannelVO().setAlSetId(UUID.fromString("b9c92679-9376-40fc-9498-083c104edca6"));
        instance.onAlSetNameSelect();
        assertNull(instance.getChannelVO().getAlSetName());
        System.out.println("onAlSetNameSelect done");
    }

    /**
     * Test of submit method, of class CreateTS1Bean.
     */
    @Test
    public void test13_Submit() {
        System.out.println("submit");
        instance.resetPage();
        instance.getChannelVO().setAreaId(UUID.fromString("2ef129fa-2d05-4dec-a84d-ed8f386ac1d0"));
        List<SelectItem> areaList = new ArrayList();
        AreaVO areaVO = new AreaVO();
        areaVO.setAreaId(instance.getChannelVO().getAreaId());
        areaVO.setAreaName("Test Area Name");
        areaList.add(new SelectItem(areaVO.getAreaId(),areaVO.getAreaName()));
        Whitebox.setInternalState(instance, "areaList", areaList);
        instance.onAreaChange();
        instance.getChannelVO().setAssetId(UUID.fromString("0361880c-d0d8-4eb7-91be-a436eb735c75"));
        assertEquals(instance.getChannelVOList().size(), 0);
        instance.submit();
        assertEquals(instance.getChannelVOList().size(), 1);
        System.out.println("submit done");
    }
    
}
