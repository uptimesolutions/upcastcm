/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.LdapClient;
import com.uptime.upcastcm.http.client.UserClient;
import com.uptime.upcastcm.utils.SessionUtils;
import com.uptime.upcastcm.utils.vo.LdapUserVO;
import com.uptime.upcastcm.utils.vo.UserPreferencesVO;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import static org.powermock.api.mockito.PowerMockito.when;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.primefaces.PrimeFaces;

/**
 *
 * @author kpati
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(PowerMockRunner.class)
@PowerMockIgnore("jdk.internal.reflect.*")
@PrepareForTest({FacesContext.class, ExternalContext.class, UserManageBean.class, ServiceRegistrarClient.class, SessionUtils.class, LdapUserVO.class, PrimeFaces.class, PrimeFaces.Ajax.class, UserClient.class, LdapClient.class})
public class LoginBeanTest {

    private final LoginBean instance;

    @Mock
    ServiceRegistrarClient serviceRegistrarClient;
    @Mock
    SessionUtils sessionUtils;
    @Mock
    FacesContext facesContext;
    @Mock
    ExternalContext externalContext;
    @Mock
    HttpServletRequest request;
    @Mock
    HttpSession session;
    @Mock
    UserManageBean userManageBean;
    @Mock
    UserClient userClient;
    @Mock
    LdapClient ldapClient;
    @Mock
    LdapUserVO userVO, currentUserVO;

    Map<String, Object> sessionMap;

    public LoginBeanTest() {
        PowerMockito.mockStatic(FacesContext.class);
        PowerMockito.mockStatic(ServiceRegistrarClient.class);
        PowerMockito.mockStatic(SessionUtils.class);
        PowerMockito.mockStatic(LdapUserVO.class);
        PowerMockito.mockStatic(UserClient.class);
        PowerMockito.mockStatic(LdapClient.class);

        facesContext = PowerMockito.mock(FacesContext.class);
        externalContext = PowerMockito.mock(ExternalContext.class);
        request = PowerMockito.mock(HttpServletRequest.class);
        session = PowerMockito.mock(HttpSession.class);
        userManageBean = PowerMockito.mock(UserManageBean.class);
        serviceRegistrarClient = PowerMockito.mock(ServiceRegistrarClient.class);
        sessionUtils = PowerMockito.mock(SessionUtils.class);
        userClient = PowerMockito.mock(UserClient.class);
        ldapClient = PowerMockito.mock(LdapClient.class);

        sessionMap = new HashMap();
        sessionMap.put("userManageBean", userManageBean);
        session.setAttribute("userManageBean", userManageBean);

        when(ServiceRegistrarClient.getInstance()).thenReturn(serviceRegistrarClient);
        when(UserClient.getInstance()).thenReturn(userClient);
        when(LdapClient.getInstance()).thenReturn(ldapClient);
        when(FacesContext.getCurrentInstance()).thenReturn(facesContext);
        when(facesContext.getExternalContext()).thenReturn(externalContext);
        when(externalContext.getSessionMap()).thenReturn(sessionMap);
        when(facesContext.getExternalContext().getRequest()).thenReturn(request);
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("userManageBean")).thenReturn(userManageBean);
        when(SessionUtils.getSession()).thenReturn(session);
        when(SessionUtils.getRequest()).thenReturn(request);
        when(SessionUtils.getRequest().getSession().getId()).thenReturn("65F452AF4AF6147D46A23FDE1056DEDE");
        PowerMockito.mockStatic(PrimeFaces.class);
        PowerMockito.mockStatic(PrimeFaces.Ajax.class);
        PrimeFaces pf = PowerMockito.mock(PrimeFaces.class);
        PrimeFaces.Ajax ax = PowerMockito.mock(PrimeFaces.Ajax.class);
        when(pf.current()).thenReturn(pf);
        when(pf.ajax()).thenReturn(ax);

        UserPreferencesVO userPreferenceVO = new UserPreferencesVO();
        userPreferenceVO.setCustomerAccount("77777");
        userPreferenceVO.setDefaultSite(UUID.fromString("e2cb8b2b-f5c1-4b7b-92d0-c20c7a94061e"));
        when(session.getAttribute("userVO")).thenReturn(userPreferenceVO);

        Map<String, String> hm = new HashMap<>();
        hm.put("email", "gopalsingh0707@gmail.com");
        hm.put("uuid", "e2cb8b2b-f5c1-4b7b-92d0-c20c7a94061e");

        when(externalContext.getRequestParameterMap()).thenReturn(hm);

        instance = new LoginBean();
        userVO = new LdapUserVO();
        currentUserVO = new LdapUserVO();
    }

    /**
     * Test of login method, of class LoginBean.
     */
    @Test
    public void test01_Login() {
        System.out.println("Login");
        assertEquals("", instance.login());
    }

    /**
     * Test of isLoggedIn method, of class LoginBean.
     */
    @Test
    public void test03_IsLoggedIn_True() {
        System.out.println("IsLoggedIn_True");
        instance.setCurrentUserVO(currentUserVO);
        boolean expResult = true;
        boolean result = instance.isLoggedIn();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }
    
    
    /**
     * Test of isLoggedIn method when currentUserVO is null, of class LoginBean.
     * currentUserVO will be null if LdapClient returns Null value for an invalid user
     */
    @Test
    public void test04_IsLoggedIn_False() {
        System.out.println("IsLoggedIn_False");
        //When currentUserVO is NULL
        instance.setCurrentUserVO(null);
        boolean expResult = false;
        boolean result = instance.isLoggedIn();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }
    

}
