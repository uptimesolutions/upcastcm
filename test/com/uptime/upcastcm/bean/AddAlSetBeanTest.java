/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.APALSetClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import com.uptime.upcastcm.utils.vo.ApAlSetVO;
import com.uptime.upcastcm.utils.vo.UserSitesVO;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import static org.powermock.api.mockito.PowerMockito.when;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.primefaces.PrimeFaces;

/**
 *
 * @author twilcox
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(PowerMockRunner.class)
@PowerMockIgnore("jdk.internal.reflect.*")
@PrepareForTest({FacesContext.class, ExternalContext.class, UserManageBean.class, ServiceRegistrarClient.class, APALSetClient.class, PrimeFaces.class, PrimeFaces.Ajax.class})
public class AddAlSetBeanTest {
    
    @Mock
    ServiceRegistrarClient serviceRegistrarClient;
    
    @Mock
    FacesContext facesContext;
    
    @Mock
    ExternalContext externalContext;
    
    @Mock
    HttpServletRequest request;
    
    @Mock
    HttpSession session;
    
    @Mock
    UserManageBean userManageBean;
    
    @Mock
    APALSetClient aPALSetClient;
    
    Map<String, Object> sessionMap;
    ApAlSetVO apAlSetVO;
    
    private final AddAlSetBean instance;
    
    public AddAlSetBeanTest() {
        List<ApAlSetVO> list;
        UserSitesVO usvo;
        ApAlSetVO avo;
                
        PowerMockito.mockStatic(FacesContext.class);
        PowerMockito.mockStatic(ServiceRegistrarClient.class);
        PowerMockito.mockStatic(APALSetClient.class);
        
        facesContext = PowerMockito.mock(FacesContext.class);
        externalContext = PowerMockito.mock(ExternalContext.class);
        request = PowerMockito.mock(HttpServletRequest.class);
        session = PowerMockito.mock(HttpSession.class);
        userManageBean = PowerMockito.mock(UserManageBean.class);
        
        serviceRegistrarClient = PowerMockito.mock(ServiceRegistrarClient.class);
        aPALSetClient = PowerMockito.mock(APALSetClient.class);
        
        sessionMap = new HashMap();
        sessionMap.put("userManageBean", userManageBean);
        session.setAttribute("userManageBean", userManageBean);
        
        when(ServiceRegistrarClient.getInstance()).thenReturn(serviceRegistrarClient);
        when(serviceRegistrarClient.getServiceHostURL(PRESET_SERVICE_NAME)).thenReturn("HOST");
        when(APALSetClient.getInstance()).thenReturn(aPALSetClient);
        when(FacesContext.getCurrentInstance()).thenReturn(facesContext);
        when(facesContext.getExternalContext()).thenReturn(externalContext);
        when(externalContext.getSessionMap()).thenReturn(sessionMap);
        when(facesContext.getExternalContext().getRequest()).thenReturn(request);
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("userManageBean")).thenReturn(userManageBean);
        when(userManageBean.getResourceBundleString("label_sensorType_acceleration")).thenReturn("Acceleration");
        
        usvo = new UserSitesVO();
        usvo.setCustomerAccount("77777");
        when(userManageBean.getCurrentSite()).thenReturn(usvo);
        
        avo = new ApAlSetVO();
        avo.setAlSetId(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf1"));
        avo.setApSetId(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"));
        avo.setCustomerAccount("77777");
        avo.setParamName("Param 1");
        avo.setSensorType("Acceleration");
        
        list = new ArrayList();
        list.add(avo);
        when(aPALSetClient.getGlobalApAlSetsByCustomerApSetId("HOST", "77777", UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"))).thenReturn(list);
        
        instance = new AddAlSetBean();
    }
    
    @Before
    public void setUp() {
        apAlSetVO = new ApAlSetVO();
        apAlSetVO.setSensorType("Acceleration");
        apAlSetVO.setCustomerAccount("77777");
        apAlSetVO.setApSetId(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"));
    }

    /**
     * Test of init method, of class AddAlSetBean.
     */
    @Test
    public void test00_Init() {
        System.out.println("init");
        instance.init();
        assertEquals(0, instance.getAlSetList().size());
        System.out.println("init Complete");
    }

    /**
     * Test of resetPage method, of class AddAlSetBean.
     */
    @Test
    public void test01_ResetPage() {
        System.out.println("resetPage");
        instance.resetPage();
        assertEquals(0, instance.getAlSetList().size());
        System.out.println("iresetPage Complete");
    }

    /**
     * Test of presetFields method, of class AddAlSetBean.
     */
    @Test
    public void test02_PresetFields_Object() {
        System.out.println("presetFields");
        
        instance.resetPage();
        assertEquals(0, instance.getAlSetList().size());
        
        instance.presetFields(apAlSetVO);
        assertEquals(1, instance.getAlSetList().size());
        assertEquals("[ApAlSetVO{customerAccount=77777, siteName=null, siteId=null, apSetName=null, apSetId=f33fcf5a-5f93-4504-bc08-74a61a460bf0, alSetName=null, alSetId=null, paramName=Param 1, sensorType=Acceleration, active=false, fmax=0, resolution=0, period=0.0, sampleRate=0.0, demod=false, demodHighPass=0.0, demodLowPass=0.0, paramType=null, frequencyUnits=null, paramUnits=null, paramAmpFactor=null, minFrequency=0.0, maxFrequency=0.0, dwell=0, lowFaultActive=false, lowAlertActive=false, highAlertActive=false, highFaultActive=false, lowFault=0.0, lowAlert=0.0, highAlert=0.0, highFault=0.0, selectedAlSet=null, lowFaultFailure=false, lowAlertFailure=false, highAlertFailure=false, highFaultFailure=false, customized=false, readOnly=false, lowFaultActiveValue=null, lowAlertActiveValue=null, highAlertActiveValue=null, highFaultActiveValue=null}]", instance.getAlSetList().toString());
        
        System.out.println("presetFields Complete");
    }

    /**
     * Test of submit method, of class AddAlSetBean.
     */
    @Test
    public void test03_Submit() {
        System.out.println("submit");
        instance.submit();
        System.out.println("submit Complete");
    }

    /**
     * Test of alarmLimitValidation method, of class AddAlSetBean.
     */
    @Test
    public void test04_AlarmLimitValidation() {
        System.out.println("alarmLimitValidation");
        ApAlSetVO alSetVO;
        
        alSetVO = new ApAlSetVO();
        
        instance.alarmLimitValidation(alSetVO);
        assertFalse(alSetVO.isLowFaultFailure());
        assertFalse(alSetVO.isLowAlertFailure());
        assertFalse(alSetVO.isHighAlertFailure());
        assertFalse(alSetVO.isHighFaultFailure());
        
        alSetVO.setLowFaultActive(true);
        alSetVO.setLowAlertActive(true);
        instance.alarmLimitValidation(alSetVO);
        assertTrue(alSetVO.isLowFaultFailure());
        assertTrue(alSetVO.isLowAlertFailure());
        assertFalse(alSetVO.isHighAlertFailure());
        assertFalse(alSetVO.isHighFaultFailure());
        
        System.out.println("alarmLimitValidation Complete");
    }

    /**
     * Test of renderErrorMessage method, of class AddAlSetBean.
     */
    @Test
    public void test05_RenderErrorMessage() {
        System.out.println("renderErrorMessage");
        instance.presetFields(apAlSetVO);
        
        instance.alarmLimitValidation(instance.getAlSetList().get(0));
        instance.renderErrorMessage();
        assertFalse(instance.isRenderAlertMsg());
        
        instance.getAlSetList().get(0).setLowFaultActive(true);
        instance.getAlSetList().get(0).setLowAlertActive(true);
        instance.alarmLimitValidation(instance.getAlSetList().get(0));
        instance.renderErrorMessage();
        assertTrue(instance.isRenderAlertMsg());
        
        System.out.println("renderErrorMessage Complete");
    }

    /**
     * Test of getAlSetList method, of class AddAlSetBean.
     */
    @Test
    public void test06_GetAlSetList() {
        System.out.println("getAlSetList");
        instance.presetFields(apAlSetVO);
        
        assertEquals(1, instance.getAlSetList().size());
        assertEquals("[ApAlSetVO{customerAccount=77777, siteName=null, siteId=null, apSetName=null, apSetId=f33fcf5a-5f93-4504-bc08-74a61a460bf0, alSetName=null, alSetId=null, paramName=Param 1, sensorType=Acceleration, active=false, fmax=0, resolution=0, period=0.0, sampleRate=0.0, demod=false, demodHighPass=0.0, demodLowPass=0.0, paramType=null, frequencyUnits=null, paramUnits=null, paramAmpFactor=null, minFrequency=0.0, maxFrequency=0.0, dwell=0, lowFaultActive=false, lowAlertActive=false, highAlertActive=false, highFaultActive=false, lowFault=0.0, lowAlert=0.0, highAlert=0.0, highFault=0.0, selectedAlSet=null, lowFaultFailure=false, lowAlertFailure=false, highAlertFailure=false, highFaultFailure=false, customized=false, readOnly=false, lowFaultActiveValue=null, lowAlertActiveValue=null, highAlertActiveValue=null, highFaultActiveValue=null}]", instance.getAlSetList().toString());
        System.out.println("getAlSetList Complete");
    }

    /**
     * Test of getDwellList method, of class AddAlSetBean.
     */
    @Test
    public void test07_GetDwellList() {
        System.out.println("getDwellList");
        assertEquals(6, instance.getDwellList().size());
        System.out.println("getDwellList Complete");
    }

    /**
     * Test of isRenderAlertMsg method, of class AddAlSetBean.
     */
    @Test
    public void test08_IsRenderAlertMsg() {
        System.out.println("isRenderAlertMsg");
        instance.presetFields(apAlSetVO);
        
        instance.alarmLimitValidation(instance.getAlSetList().get(0));
        instance.renderErrorMessage();
        assertFalse(instance.isRenderAlertMsg());
        
        instance.getAlSetList().get(0).setLowFaultActive(true);
        instance.getAlSetList().get(0).setLowAlertActive(true);
        instance.alarmLimitValidation(instance.getAlSetList().get(0));
        instance.renderErrorMessage();
        assertTrue(instance.isRenderAlertMsg());
        
        System.out.println("isRenderAlertMsg Complete");
    }

    /**
     * Test of getApAlSetVO method, of class AddAlSetBean.
     */
    @Test
    public void test09_GetApAlSetVO() {
        System.out.println("getApAlSetVO");
        instance.presetFields(apAlSetVO);
        assertEquals("ApAlSetVO{customerAccount=77777, siteName=null, siteId=null, apSetName=null, apSetId=f33fcf5a-5f93-4504-bc08-74a61a460bf0, alSetName=null, alSetId=null, paramName=null, sensorType=Acceleration, active=false, fmax=0, resolution=0, period=0.0, sampleRate=0.0, demod=false, demodHighPass=0.0, demodLowPass=0.0, paramType=null, frequencyUnits=null, paramUnits=null, paramAmpFactor=null, minFrequency=0.0, maxFrequency=0.0, dwell=0, lowFaultActive=false, lowAlertActive=false, highAlertActive=false, highFaultActive=false, lowFault=0.0, lowAlert=0.0, highAlert=0.0, highFault=0.0, selectedAlSet=null, lowFaultFailure=false, lowAlertFailure=false, highAlertFailure=false, highFaultFailure=false, customized=false, readOnly=false, lowFaultActiveValue=null, lowAlertActiveValue=null, highAlertActiveValue=null, highFaultActiveValue=null}", instance.getApAlSetVO().toString());
        
        System.out.println("getApAlSetVO Complete");
    }

    /**
     * Test of getAcSensor method, of class AddAlSetBean.
     */
    @Test
    public void test10_GetAcSensor() {
        System.out.println("getAcSensor");
        instance.presetFields(apAlSetVO);
        assertTrue(instance.getAcSensor());
        System.out.println("getAcSensor Complete");
    }
    
}
