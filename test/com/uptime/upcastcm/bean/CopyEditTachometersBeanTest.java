/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.TachometerClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import com.uptime.upcastcm.utils.helperclass.PresetUtil;
import com.uptime.upcastcm.utils.vo.TachometerVO;
import com.uptime.upcastcm.utils.vo.UserSitesVO;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import static org.powermock.api.mockito.PowerMockito.when;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.primefaces.PrimeFaces;

/**
 *
 * @author twilcox
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(PowerMockRunner.class)
@PowerMockIgnore("jdk.internal.reflect.*")
@PrepareForTest({FacesContext.class, ExternalContext.class, ServiceRegistrarClient.class, UserManageBean.class, TachometerClient.class, PrimeFaces.class, PrimeFaces.Ajax.class})
public class CopyEditTachometersBeanTest {

    @Mock
    ServiceRegistrarClient serviceRegistrarClient;

    @Mock
    TachometerClient tachometerClient;

    @Mock
    FacesContext facesContext;

    @Mock
    ExternalContext externalContext;

    @Mock
    UserManageBean userManageBean;
    @Mock
    PresetsBean presetsBean;

    @Mock
    HttpServletRequest request;
    @Mock
    HttpSession session;

    private final CopyEditTachometersBean instance;
    Map<String, Object> sessionMap;

    public CopyEditTachometersBeanTest() {
        TachometerVO tachometerVO;
        
        PowerMockito.mockStatic(FacesContext.class);
        PowerMockito.mockStatic(TachometerClient.class);
        PowerMockito.mockStatic(ServiceRegistrarClient.class);

        facesContext = PowerMockito.mock(FacesContext.class);
        externalContext = PowerMockito.mock(ExternalContext.class);
        request = PowerMockito.mock(HttpServletRequest.class);
        session = PowerMockito.mock(HttpSession.class);
        userManageBean = PowerMockito.mock(UserManageBean.class);
        presetsBean = PowerMockito.mock(PresetsBean.class);
        tachometerClient = PowerMockito.mock(TachometerClient.class);
        serviceRegistrarClient = PowerMockito.mock(ServiceRegistrarClient.class);

        sessionMap = new HashMap();
        sessionMap.put("userManageBean", userManageBean);
        sessionMap.put("presetsBean", presetsBean);
        session.setAttribute("userManageBean", userManageBean);
        session.setAttribute("presetsBean", presetsBean);
        
        UserSitesVO uvo = new UserSitesVO();
        uvo.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        uvo.setSiteName("Test Site 1");
        uvo.setCustomerAccount("77777");

        when(TachometerClient.getInstance()).thenReturn(tachometerClient);
        when(ServiceRegistrarClient.getInstance()).thenReturn(serviceRegistrarClient);
        when(FacesContext.getCurrentInstance()).thenReturn(facesContext);
        when(facesContext.getExternalContext()).thenReturn(externalContext);
        when(externalContext.getSessionMap()).thenReturn(sessionMap);
        when(facesContext.getExternalContext().getRequest()).thenReturn(request);
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("userManageBean")).thenReturn(userManageBean);
        when(session.getAttribute("presetsBean")).thenReturn(presetsBean);
        when(userManageBean.getPresetUtil()).thenReturn(new PresetUtil());
        when(userManageBean.getCurrentSite()).thenReturn(uvo);

        instance = new CopyEditTachometersBean();
        
        tachometerVO = new TachometerVO();
        tachometerVO.setSiteName("Test Site 1");
        tachometerVO.setCustomerAccount("77777");
        tachometerVO.setTachName("Tach Name 1");
        instance.setTachometerVO(tachometerVO);
    }

    /**
     * Test of resetPage method, of class CopyEditTachometersBean.
     */
    @Test
    public void test0_ResetPage() {
        System.out.println("resetPage");
        instance.resetPage();
        System.out.println("resetPage completed");
    }

    /**
     * Test of presetFields method, of class CopyEditTachometersBean.
     */
    @Test
    public void test1_PresetFields_Filters_Object() {
        System.out.println("presetFields(Filters presets, Object object) Unsupported");
    }

    /**
     * Test of presetFields method, of class CopyEditTachometersBean.
     */
    @Test
    public void test2_PresetFields_int_Object() {
        System.out.println("presetFields");
        instance.presetFields(1, instance.getTachometerVO());
        assertSame(instance.getTachometerVO().getSiteName(), userManageBean.getCurrentSite().getSiteName());
        System.out.println("presetFields completed");
    }

    /**
     * Test of presetFields method, of class CopyEditTachometersBean.
     */
    @Test
    public void test3_PresetFields_Object() {
        System.out.println("presetFields(Object object) Unsupported");
    }

    /**
     * Test of submit method, of class CopyEditTachometersBean.
     */
    @Test
    public void test4_Submit() {
        System.out.println("submit");

        TachometerVO tachometerVO = new TachometerVO();
        tachometerVO.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        tachometerVO.setSiteName("Test Site 1");
        tachometerVO.setCustomerAccount("77777");
        tachometerVO.setTachName("Tach Name 1");
        instance.setTachometerVO(tachometerVO);
        when(tachometerClient.createGlobalTachometers(serviceRegistrarClient.getServiceHostURL(PRESET_SERVICE_NAME), instance.getTachometerVO())).thenReturn(Boolean.TRUE);
        PowerMockito.mockStatic(PrimeFaces.class);
        PowerMockito.mockStatic(PrimeFaces.Ajax.class);
        PrimeFaces pf = PowerMockito.mock(PrimeFaces.class);
        PrimeFaces.Ajax ax = PowerMockito.mock(PrimeFaces.Ajax.class);
        when(PrimeFaces.current()).thenReturn(pf);
        when(pf.ajax()).thenReturn(ax);
        ax.addCallbackParam("presetsTabViewId:tachometersFormId", new Object());
        instance.submit();
        assertEquals(Boolean.FALSE,instance.isValidationFailed());
        System.out.println("submit completed");
    }

    /**
     * Test of submit method, of class CopyEditTachometersBean.
     */
    @Test
    public void test41_Submit() {
        System.out.println("submit");

        TachometerVO tachometerVO = new TachometerVO();
        tachometerVO.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        tachometerVO.setSiteName("Test Site 1");
        tachometerVO.setCustomerAccount("77777");
        tachometerVO.setTachName("Tach Name 1");
        instance.setTachometerVO(tachometerVO);
        when(tachometerClient.createSiteTachometers(serviceRegistrarClient.getServiceHostURL(PRESET_SERVICE_NAME), instance.getTachometerVO())).thenReturn(Boolean.TRUE);
        PowerMockito.mockStatic(PrimeFaces.class);
        PowerMockito.mockStatic(PrimeFaces.Ajax.class);
        PrimeFaces pf = PowerMockito.mock(PrimeFaces.class);
        PrimeFaces.Ajax ax = PowerMockito.mock(PrimeFaces.Ajax.class);
        when(pf.current()).thenReturn(pf);
        when(pf.ajax()).thenReturn(ax);
        ax.addCallbackParam("presetsTabViewId:tachometersFormId", new Object());
        instance.submit();
        assertEquals(Boolean.FALSE,instance.isValidationFailed());
        System.out.println("submit completed");
    }

    /**
     * Test of submit method, of class CopyEditTachometersBean.
     */
    @Test
    public void test42_Submit() {
        System.out.println("submit");
        instance.setOperationType(1);
        TachometerVO tachometerVO = new TachometerVO();
        tachometerVO.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        tachometerVO.setSiteName("Test Site 1");
        tachometerVO.setCustomerAccount("77777");
        tachometerVO.setTachName("Tach Name 1");
        instance.setTachometerVO(tachometerVO);
        when(tachometerClient.updateSiteTachometers(serviceRegistrarClient.getServiceHostURL(PRESET_SERVICE_NAME), instance.getTachometerVO())).thenReturn(Boolean.TRUE);
        PowerMockito.mockStatic(PrimeFaces.class);
        PowerMockito.mockStatic(PrimeFaces.Ajax.class);
        PrimeFaces pf = PowerMockito.mock(PrimeFaces.class);
        PrimeFaces.Ajax ax = PowerMockito.mock(PrimeFaces.Ajax.class);
        when(pf.current()).thenReturn(pf);
        when(pf.ajax()).thenReturn(ax);
        ax.addCallbackParam("presetsTabViewId:tachometersFormId", new Object());
        instance.submit();
        assertEquals(Boolean.FALSE,instance.isValidationFailed());
        System.out.println("submit completed");
    }

    /**
     * Test of submit method, of class CopyEditTachometersBean.
     */
    @Test
    public void test43_Submit() {
        System.out.println("submit");
        instance.setOperationType(1);
        TachometerVO tachometerVO = new TachometerVO();
        tachometerVO.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        tachometerVO.setSiteName("Test Site 1");
        tachometerVO.setCustomerAccount("77777");
        tachometerVO.setTachName("Tach Name 1");
        instance.setTachometerVO(tachometerVO);
        when(tachometerClient.updateGlobalTachometers(serviceRegistrarClient.getServiceHostURL(PRESET_SERVICE_NAME), instance.getTachometerVO())).thenReturn(Boolean.TRUE);
        PowerMockito.mockStatic(PrimeFaces.class);
        PowerMockito.mockStatic(PrimeFaces.Ajax.class);
        PrimeFaces pf = PowerMockito.mock(PrimeFaces.class);
        PrimeFaces.Ajax ax = PowerMockito.mock(PrimeFaces.Ajax.class);
        when(pf.current()).thenReturn(pf);
        when(pf.ajax()).thenReturn(ax);
        ax.addCallbackParam("presetsTabViewId:tachometersFormId", new Object());
        instance.submit();
        assertEquals(Boolean.FALSE,instance.isValidationFailed());
        System.out.println("submit completed");
    }
    
    
    /**
     * Test of submit method, of class CopyEditTachometersBean.
     */
    @Test
    public void test44_Submit() {
        System.out.println("submit");
        
        instance.setTachometerVO(null);        
        instance.submit();
        assertEquals(Boolean.TRUE,instance.isValidationFailed());
        System.out.println("submit completed");
    }
    
    /**
     * Test of submit method, of class CopyEditTachometersBean.
     */
    @Test
    public void test45_Submit() {
        System.out.println("submit");
        instance.setOperationType(1);
        TachometerVO tachometerVO = new TachometerVO();
        tachometerVO.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        tachometerVO.setSiteName("Test Site 1");
        tachometerVO.setCustomerAccount(null);
        tachometerVO.setTachName("Tach Name 1");
        instance.setTachometerVO(tachometerVO);        
        instance.submit();
        assertEquals(Boolean.TRUE,instance.isValidationFailed());
        System.out.println("submit completed");
    }
    /**
     * Test of submit method, of class CopyEditTachometersBean.
     */
    @Test
    public void test46_Submit() {
        System.out.println("submit");
        instance.setOperationType(1);
        TachometerVO tachometerVO = new TachometerVO();
        tachometerVO.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        tachometerVO.setSiteName("Test Site 1");
        tachometerVO.setCustomerAccount("77777");
        tachometerVO.setTachName(null);
        instance.setTachometerVO(tachometerVO);        
        instance.submit();
        assertEquals(Boolean.TRUE,instance.isValidationFailed());
        System.out.println("submit completed");
    }
   

}
