/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.AssetClient;
import com.uptime.upcastcm.http.client.PointClient;
import com.uptime.upcastcm.http.client.PointLocationClient;
import com.uptime.upcastcm.http.client.WorkOrderClient;
import com.uptime.upcastcm.utils.vo.UserSitesVO;
import com.uptime.upcastcm.utils.vo.WorkOrderVO;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import static org.powermock.api.mockito.PowerMockito.when;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.primefaces.PrimeFaces;

/**
 *
 * @author twilcox
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(PowerMockRunner.class)
@PowerMockIgnore("jdk.internal.reflect.*")
@PrepareForTest({FacesContext.class, ExternalContext.class, UserManageBean.class, ServiceRegistrarClient.class, PointClient.class, PointLocationClient.class, PrimeFaces.class, PrimeFaces.Ajax.class})
public class AddTrendBeanTest {
    
    @Mock
    ServiceRegistrarClient serviceRegistrarClient;
    
    @Mock
    FacesContext facesContext;
    
    @Mock
    ExternalContext externalContext;
    
    @Mock
    HttpServletRequest request;
    
    @Mock
    HttpSession session;
    
    @Mock
    UserManageBean userManageBean;
    
    @Mock
    PointClient pointClient;
    
    @Mock
    PointLocationClient pointLocationClient;
    
    Map<String, Object> sessionMap;
    WorkOrderVO workOrderVO;
    
    private final AddTrendBean instance;
    
    public AddTrendBeanTest() {
        UserSitesVO usvo;
                
        PowerMockito.mockStatic(FacesContext.class);
        PowerMockito.mockStatic(ServiceRegistrarClient.class);
        PowerMockito.mockStatic(WorkOrderClient.class);
        PowerMockito.mockStatic(PointClient.class);
        PowerMockito.mockStatic(PointLocationClient.class);
        PowerMockito.mockStatic(AssetClient.class);
        
        facesContext = PowerMockito.mock(FacesContext.class);
        externalContext = PowerMockito.mock(ExternalContext.class);
        request = PowerMockito.mock(HttpServletRequest.class);
        session = PowerMockito.mock(HttpSession.class);
        userManageBean = PowerMockito.mock(UserManageBean.class);
        
        serviceRegistrarClient = PowerMockito.mock(ServiceRegistrarClient.class);
        pointClient = PowerMockito.mock(PointClient.class);
        pointLocationClient = PowerMockito.mock(PointLocationClient.class);
        
        sessionMap = new HashMap();
        sessionMap.put("userManageBean", userManageBean);
        session.setAttribute("userManageBean", userManageBean);
        
        when(ServiceRegistrarClient.getInstance()).thenReturn(serviceRegistrarClient);
        when(PointClient.getInstance()).thenReturn(pointClient);
        when(PointLocationClient.getInstance()).thenReturn(pointLocationClient);
        when(FacesContext.getCurrentInstance()).thenReturn(facesContext);
        when(facesContext.getExternalContext()).thenReturn(externalContext);
        when(externalContext.getSessionMap()).thenReturn(sessionMap);
        when(facesContext.getExternalContext().getRequest()).thenReturn(request);
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("userManageBean")).thenReturn(userManageBean);
        
        usvo = new UserSitesVO();
        usvo.setCustomerAccount("77777");
        when(userManageBean.getCurrentSite()).thenReturn(usvo);
        
        
        instance = new AddTrendBean();
    }
    
    @Before
    public void setUp() {
        workOrderVO = new WorkOrderVO();
        workOrderVO.setTrendDetails(new ArrayList());
    }

    /**
     * Test of init method, of class AddTrendBean.
     */
    @Test
    public void testInit() {
        System.out.println("init");
        instance.setSelectedParam("test");
        instance.init();
        assertNull(instance.getSelectedParam());
        System.out.println("init completed");
    }

    /**
     * Test of resetPage method, of class AddTrendBean.
     */
    @Test
    public void testResetPage() {
        System.out.println("resetPage");
        instance.setSelectedParam("test");
        instance.resetPage();
        assertNull(instance.getSelectedParam());
        System.out.println("resetPage completed");
    }

    /**
     * Test of presetFields method, of class AddTrendBean.
     */
    @Test
    public void testPresetFields_Object() {
        System.out.println("presetFields");
        instance.presetFields(workOrderVO);
        instance.presetFields("f33fcf5a-5f93-4504-bc08-74a61a460bf0");
        System.out.println("presetFields completed");
    }

    /**
     * Test of addTrend method, of class AddTrendBean.
     */
    @Test
    public void testAddTrend() {
        System.out.println("addTrend");
        instance.addTrend();
        System.out.println("addTrend completed");
    }

    /**
     * Test of onPointLocationChange method, of class AddTrendBean.
     */
    @Test
    public void testOnPointLocationChange() {
        System.out.println("onPointLocationChange");
        instance.onPointLocationChange();
        System.out.println("onPointLocationChange completed");
    }

    /**
     * Test of onPointChange method, of class AddTrendBean.
     */
    @Test
    public void testOnPointChange() {
        System.out.println("onPointChange");
        instance.onPointChange();
        System.out.println("onPointChange completed");
    }

    /**
     * Test of getPointLocationList method, of class AddTrendBean.
     */
    @Test
    public void testGetPointLocationList() {
        System.out.println("getPointLocationList");
        assertEquals(0, instance.getPointLocationList().size());
        System.out.println("getPointLocationList completed");
    }

    /**
     * Test of getPointList method, of class AddTrendBean.
     */
    @Test
    public void testGetPointList() {
        System.out.println("getPointList");
        assertEquals(0, instance.getPointList().size());
        System.out.println("getPointList completed");
    }

    /**
     * Test of getParamList method, of class AddTrendBean.
     */
    @Test
    public void testGetParamList() {
        System.out.println("getParamList");
        assertEquals(0, instance.getParamList().size());
        System.out.println("getParamList completed");
    }

    /**
     * Test of getSelectedPointLocation method, of class AddTrendBean.
     */
    @Test
    public void testGetSelectedPointLocation() {
        System.out.println("getSelectedPointLocation");
        instance.setSelectedPointLocation(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"));
        assertEquals(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"), instance.getSelectedPointLocation());
        System.out.println("getSelectedPointLocation completed");
    }

    /**
     * Test of setSelectedPointLocation method, of class AddTrendBean.
     */
    @Test
    public void testSetSelectedPointLocation() {
        System.out.println("setSelectedPointLocation");
        instance.setSelectedPointLocation(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"));
        assertEquals(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"), instance.getSelectedPointLocation());
        System.out.println("setSelectedPointLocation completed");
    }

    /**
     * Test of getSelectedPoint method, of class AddTrendBean.
     */
    @Test
    public void testGetSelectedPoint() {
        System.out.println("getSelectedPoint");
        instance.setSelectedPoint(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"));
        assertEquals(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"), instance.getSelectedPoint());
        System.out.println("getSelectedPoint completed");
    }

    /**
     * Test of setSelectedPoint method, of class AddTrendBean.
     */
    @Test
    public void testSetSelectedPoint() {
        System.out.println("setSelectedPoint");
        instance.setSelectedPoint(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"));
        assertEquals(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"), instance.getSelectedPoint());
        System.out.println("setSelectedPoint completed");
    }

    /**
     * Test of getSelectedParam method, of class AddTrendBean.
     */
    @Test
    public void testGetSelectedParam() {
        System.out.println("getSelectedParam");
        instance.setSelectedParam("test");
        assertEquals("test", instance.getSelectedParam());
        System.out.println("getSelectedParam completed");
    }

    /**
     * Test of setSelectedParam method, of class AddTrendBean.
     */
    @Test
    public void testSetSelectedParam() {
        System.out.println("setSelectedParam");
        instance.setSelectedParam("test");
        assertEquals("test", instance.getSelectedParam());
        System.out.println("setSelectedParam completed");
    }
    
}
