/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.APALSetClient;
import com.uptime.upcastcm.http.client.AreaClient;
import com.uptime.upcastcm.http.client.PointLocationClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import com.uptime.upcastcm.utils.helperclass.Filters;
import com.uptime.upcastcm.utils.helperclass.PresetUtil;
import com.uptime.upcastcm.utils.vo.FaultFrequenciesVO;
import com.uptime.upcastcm.utils.vo.PointLocationVO;
import com.uptime.upcastcm.utils.vo.UserSitesVO;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.junit.runners.MethodSorters;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import static org.powermock.api.mockito.PowerMockito.when;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

/**
 *
 * @author kpati
 */
@PowerMockIgnore("jdk.internal.reflect.*")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnit4.class)
@PrepareForTest({FacesContext.class, ExternalContext.class, HttpServletRequest.class, HttpSession.class, UserManageBean.class, ConfigurationBean.class, AreaClient.class, ServiceRegistrarClient.class, PresetUtil.class, PointLocationClient.class, APALSetClient.class})
public class CreatePointLocationBeanTest {

    private final CreatePointLocationBean instance;

    @Mock
    ServiceRegistrarClient serviceRegistrarClient;

    @Mock
    ConfigurationBean configurationBean;

    @Mock
    PresetUtil presetUtil;

    @Mock
    PointLocationClient pointLocationClient;

    @Mock
    FacesContext facesContext;

    @Mock
    ExternalContext externalContext;

    @Mock
    UserManageBean userManageBean;

    Map<String, Object> sessionMap;
    private PointLocationVO pointLocationVO;
    String customerAccount, defaultsite;
    private final Filters filters;

    public CreatePointLocationBeanTest() {

        PowerMockito.mockStatic(FacesContext.class);
        PowerMockito.mockStatic(PointLocationClient.class);
        PowerMockito.mockStatic(ServiceRegistrarClient.class);

        facesContext = PowerMockito.mock(FacesContext.class);
        externalContext = PowerMockito.mock(ExternalContext.class);

        userManageBean = PowerMockito.mock(UserManageBean.class);
        configurationBean = PowerMockito.mock(ConfigurationBean.class);
        presetUtil = PowerMockito.mock(PresetUtil.class);
        serviceRegistrarClient = PowerMockito.mock(ServiceRegistrarClient.class);
        pointLocationClient = PowerMockito.mock(PointLocationClient.class);

        when(FacesContext.getCurrentInstance()).thenReturn(facesContext);
        when(facesContext.getExternalContext()).thenReturn(externalContext);
        when(ServiceRegistrarClient.getInstance()).thenReturn(serviceRegistrarClient);
        when(PointLocationClient.getInstance()).thenReturn(pointLocationClient);
        customerAccount = "77777";
        sessionMap = new HashMap();
        sessionMap.put("userManageBean", userManageBean);
        sessionMap.put("configurationBean", configurationBean);
        when(externalContext.getSessionMap()).thenReturn(sessionMap);
        UserSitesVO usvo = new UserSitesVO();
        usvo.setCustomerAccount(customerAccount);
        when(userManageBean.getCurrentSite()).thenReturn(usvo);
        when(userManageBean.getPresetUtil()).thenReturn(presetUtil);

        filters = new Filters();
        filters.setSite(new SelectItem(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), "Test Site 1"));
        filters.setArea(new SelectItem(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"), "Test Area 1"));
        filters.setAsset(new SelectItem(UUID.fromString("f33fcf5a-5f93-4504-bc09-74a61a460bf0"), "Test Asset 1"));

        instance = new CreatePointLocationBean();

    }

    @Before
    public void setUp() {
        pointLocationVO = new PointLocationVO();
        pointLocationVO.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        instance.setPointLocationVO(pointLocationVO);
    }

    /**
     * Test of init method, of class CreatePointLocationBean.
     */
    @Test
    public void test01_Init() {
        System.out.println("init");
        String expectedResult = customerAccount;
        instance.init();
        String result = instance.getPointLocationVO().getCustomerAccount();
        assertEquals(expectedResult, result);
        System.out.println("init Done");
    }

    /**
     * Test of init method, of class CreatePointLocationBean.
     */
    @Test
    public void test02_Init() {
        System.out.println("init");
        String expectedResult = "88888";
        instance.init();
        String result = instance.getPointLocationVO().getCustomerAccount();
        assertNotEquals(expectedResult, result);
        System.out.println("init Done");
    }

    /**
     * Test of resetPage method, of class CreatePointLocationBean.
     */
    @Test
    public void test03_ResetPage() {
        System.out.println("resetPage");
        String expectedResult = customerAccount;
        instance.resetPage();
        String result = instance.getPointLocationVO().getCustomerAccount();
        assertEquals(expectedResult, result);
        System.out.println("resetPage Done");
    }

    /**
     * Test of resetPage method, of class CreatePointLocationBean.
     */
    @Test
    public void test04_ResetPage() {
        System.out.println("resetPage");
        String expectedResult = "88888";
        instance.resetPage();
        String result = instance.getPointLocationVO().getCustomerAccount();
        assertNotEquals(expectedResult, result);
        System.out.println("resetPage Done");
    }

    /**
     * Test of presetFields method, of class CreatePointLocationBean.
     */
    @Test
    public void test05_PresetFields_Filters_Object() {
        System.out.println("presetFields");
        filters.setArea(new SelectItem(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"), "Test Area 2"));
        Filters presets = filters;
        Object object = null;
        String expectedResult = "Test Area 2";
        instance.presetFields(presets, object);
        String result = instance.getPointLocationVO().getAreaName();
        assertEquals(expectedResult, result);
        System.out.println("presetFields Done");
    }

    /**
     * Test of presetFields method, of class CreatePointLocationBean.
     */
    @Test
    public void test06_PresetFields_Filters_Object() {
        System.out.println("presetFields");
        filters.setArea(new SelectItem(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"), "Test Area 2"));
        Filters presets = filters;
        Object object = null;
        String expectedResult = "Test Area 1";
        instance.presetFields(presets, object);
        String result = instance.getPointLocationVO().getAreaName();
        assertNotEquals(expectedResult, result);
        assertEquals(instance.getPointLocationVO().getAreaName(), "Test Area 2");
        System.out.println("presetFields Done");
    }

    /**
     * Test of submit method, of class CreatePointLocationBean.
     */
    @Test
    public void test07_Submit() {
        System.out.println("submit");
        pointLocationVO.setCustomerAccount(customerAccount);
        pointLocationVO.setAreaId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        pointLocationVO.setAssetId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        pointLocationVO.setPointLocationName("Test PointLocation Name");
        pointLocationVO.setRollDiameter(0);
        pointLocationVO.setRollDiameterUnits("Inches");
        pointLocationVO.setFfSetName(new HashSet());
        instance.setPointLocationVO(pointLocationVO);
        when(serviceRegistrarClient.getServiceHostURL(CONFIGURATION_SERVICE_NAME)).thenReturn(CONFIGURATION_SERVICE_NAME);
        when(pointLocationClient.createPointLocation(CONFIGURATION_SERVICE_NAME, pointLocationVO)).thenReturn(true);
        UUID expectedResult = pointLocationVO.getAreaId();
        instance.submit();
        UUID result = instance.getPointLocationVO().getAreaId();
        assertEquals(null, result);
        System.out.println("submit Done");
    }

    /**
     * Test of submit method, of class CreatePointLocationBean.
     */
    @Test
    public void test08_Submit() {
        System.out.println("submit");
        pointLocationVO.setCustomerAccount(customerAccount);
        pointLocationVO.setAreaId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        pointLocationVO.setAssetId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        pointLocationVO.setPointLocationName("Test PointLocation Name");
        pointLocationVO.setPointLocationId(UUID.fromString("f44fcf5a-5f93-4504-bc07-74a61a460bf0"));
        pointLocationVO.setFfSetName(new HashSet());
        instance.setPointLocationVO(pointLocationVO);
        when(serviceRegistrarClient.getServiceHostURL(CONFIGURATION_SERVICE_NAME)).thenReturn(CONFIGURATION_SERVICE_NAME);
        when(pointLocationClient.createPointLocation(CONFIGURATION_SERVICE_NAME, pointLocationVO)).thenReturn(false);
        UUID expectedResult = new PointLocationVO().getPointLocationId();
        instance.submit();
        UUID result = instance.getPointLocationVO().getPointLocationId();
        assertNotEquals(expectedResult, result);
        System.out.println("submit Done");
    }

    /**
     * Test of getPointLocationVO method, of class CreatePointLocationBean.
     */
    @Test
    public void test09_GetPointLocationVO() {
        System.out.println("getPointLocationVO");
        PointLocationVO expResult = pointLocationVO;
        PointLocationVO result = instance.getPointLocationVO();
        assertEquals(expResult, result);
        System.out.println("getPointLocationVO Done");
    }

    /**
     * Test of getPointLocationVO method, of class CreatePointLocationBean.
     */
    @Test
    public void test10_GetPointLocationVO() {
        System.out.println("getPointLocationVO");
        PointLocationVO expResult = new PointLocationVO();
        PointLocationVO result = instance.getPointLocationVO();
        assertNotEquals(expResult, result);
        System.out.println("getPointLocationVO Done");
    }

    /**
     * Test of setPointLocationVO method, of class CreatePointLocationBean.
     */
    @Test
    public void test11_SetPointLocationVO() {
        System.out.println("setPointLocationVO");
        PointLocationVO pointLocationVO = this.pointLocationVO;
        PointLocationVO expectedResult = pointLocationVO;
        instance.setPointLocationVO(pointLocationVO);
        PointLocationVO result = instance.getPointLocationVO();
        assertEquals(expectedResult, result);
        System.out.println("setPointLocationVO Done");
    }

    /**
     * Test of setPointLocationVO method, of class CreatePointLocationBean.
     */
    @Test
    public void test12_SetPointLocationVO() {
        System.out.println("setPointLocationVO");
        PointLocationVO pointLocationVO = this.pointLocationVO;
        PointLocationVO expectedResult = pointLocationVO;
        instance.setPointLocationVO(pointLocationVO);
        PointLocationVO result = instance.getPointLocationVO();
        assertEquals(expectedResult, result);
        System.out.println("setPointLocationVO Done");
    }

    /**
     * Test of getSiteNames method, of class CreatePointLocationBean.
     */
    @Test
    public void test13_GetSiteNames() {
        System.out.println("getSiteNames");
        filters.setSite(new SelectItem(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), "Test Site Name 1"));
        test05_PresetFields_Filters_Object();
        List<String> expResult = new ArrayList();
        expResult.add(filters.getSite().getLabel());
        List<String> result = instance.getSiteNames();
        assertEquals(expResult, result);
        System.out.println("getSiteNames Done");
    }

    /**
     * Test of getSiteNames method, of class CreatePointLocationBean.
     */
    @Test
    public void test14_GetSiteNames() {
        System.out.println("getSiteNames");
        filters.setSite(new SelectItem(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), "Test Site Name 1"));
        test05_PresetFields_Filters_Object();
        List<String> expResult = new ArrayList();
        List<String> result = instance.getSiteNames();
        assertNotEquals(expResult, result);
        System.out.println("getSiteNames Done");
    }

    /**
     * Test of getAssetNameList method, of class CreatePointLocationBean.
     */
    @Test
    public void test15_GetAssetNameList() {
        System.out.println("getAssetNameList");
        filters.setAsset(new SelectItem(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), "Test Asset Name 1"));
        test05_PresetFields_Filters_Object();
        List<String> expResult = new ArrayList();
        expResult.add(filters.getAsset().getLabel());
        List<String> result = instance.getAssetNameList();
        assertEquals(expResult, result);
        System.out.println("getAssetNameList Done");
    }

    /**
     * Test of getAssetNameList method, of class CreatePointLocationBean.
     */
    @Test
    public void test16_GetAssetNameList() {
        System.out.println("getAssetNameList");
        filters.setAsset(new SelectItem(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), "Test Asset Name 1"));
        test05_PresetFields_Filters_Object();
        List<String> expResult = new ArrayList();
        List<String> result = instance.getAssetNameList();
        assertNotEquals(expResult, result);
        System.out.println("getAssetNameList Done");
    }

    /**
     * Test of getFaultFrequenciesList method, of class CreatePointLocationBean.
     */
    @Test
    public void test19_GetFaultFrequenciesList() {
        System.out.println("getFaultFrequenciesList");
        List<SelectItem> ffList = new ArrayList();
        List<FaultFrequenciesVO> newFFList = new ArrayList();
        ffList.add(new SelectItem(UUID.fromString("f44fcf5a-5f93-4504-bc07-74a61a460bf0"), "Test FF Set Name1"));
        when(presetUtil.populateFFSetNameList(userManageBean, newFFList)).thenReturn(ffList);
        test05_PresetFields_Filters_Object();
        List<SelectItem> expResult = ffList;
        List<SelectItem> result = instance.getFaultFrequenciesList();
        assertEquals(expResult, result);
        System.out.println("getFaultFrequenciesList Done");
    }

    /**
     * Test of getFaultFrequenciesList method, of class CreatePointLocationBean.
     */
    @Test
    public void test20_GetFaultFrequenciesList() {
        System.out.println("getFaultFrequenciesList");
        List<SelectItem> ffList = new ArrayList();
        List<FaultFrequenciesVO> newFfList = new ArrayList();
        ffList.add(new SelectItem(UUID.fromString("f44fcf5a-5f93-4504-bc07-74a61a460bf0"), "Test FF Set Name1"));
        when(presetUtil.populateFFSetNameList(userManageBean, newFfList)).thenReturn(ffList);
        test05_PresetFields_Filters_Object();
        List<SelectItem> expResult = new ArrayList();
        List<SelectItem> result = instance.getFaultFrequenciesList();
        assertNotEquals(expResult, result);
        System.out.println("getFaultFrequenciesList Done");
    }

    /**
     * Test of getTachometersList method, of class CreatePointLocationBean.
     */
    @Test
    public void test21_GetTachometersList() {
        System.out.println("getTachometersList");
        List<SelectItem> tachometerList = new ArrayList();
        tachometerList.add(new SelectItem(UUID.fromString("f44fcf5a-5f93-4504-bc07-74a61a460bf0"), "Test FF Set Name1"));
        when(presetUtil.populateTachometerList(userManageBean, customerAccount, pointLocationVO.getSiteId())).thenReturn(tachometerList);
        test05_PresetFields_Filters_Object();
        List<SelectItem> expResult = tachometerList;
        List<SelectItem> result = instance.getTachometersList();
        assertEquals(expResult, result);
        System.out.println("getTachometersList Done");
    }

    /**
     * Test of getTachometersList method, of class CreatePointLocationBean.
     */
    @Test
    public void test22_GetTachometersList() {
        System.out.println("getTachometersList");
        List<SelectItem> tachometerList = new ArrayList();
        tachometerList.add(new SelectItem(UUID.fromString("f44fcf5a-5f93-4504-bc07-74a61a460bf0"), "Test FF Set Name1"));
        when(presetUtil.populateTachometerList(userManageBean, customerAccount, pointLocationVO.getSiteId())).thenReturn(tachometerList);
        test05_PresetFields_Filters_Object();
        List<SelectItem> expResult = new ArrayList();
        List<SelectItem> result = instance.getTachometersList();
        assertNotEquals(expResult, result);
        System.out.println("getTachometersList Done");
    }

    /**
     * Test of getPtLoNamePresetList method, of class CreatePointLocationBean.
     */
    @Test
    public void test23_GetPtLoNamePresetList() {
        System.out.println("getPtLoNamePresetList");
        List<SelectItem> ptLoNameList = new ArrayList();
        ptLoNameList.add(new SelectItem(UUID.fromString("f44fcf5a-5f93-4504-bc07-74a61a460bf0"), "Test FF Set Name1"));
        when(presetUtil.populatePtLoNamesList(userManageBean, customerAccount, pointLocationVO.getSiteId())).thenReturn(ptLoNameList);
        test05_PresetFields_Filters_Object();
        List<SelectItem> expResult = ptLoNameList;
        List<SelectItem> result = instance.getPtLoNamePresetList();
        assertEquals(expResult, result);
        System.out.println("getPtLoNamePresetList Done");

    }

    /**
     * Test of getPtLoNamePresetList method, of class CreatePointLocationBean.
     */
    @Test
    public void test24_GetPtLoNamePresetList() {
        System.out.println("getPtLoNamePresetList");
        List<SelectItem> ptLoNameList = new ArrayList();
        ptLoNameList.add(new SelectItem(UUID.fromString("f44fcf5a-5f93-4504-bc07-74a61a460bf0"), "Test FF Set Name1"));
        when(presetUtil.populatePtLoNamesList(userManageBean, customerAccount, pointLocationVO.getSiteId())).thenReturn(ptLoNameList);
        test05_PresetFields_Filters_Object();
        List<SelectItem> expResult = new ArrayList();
        List<SelectItem> result = instance.getPtLoNamePresetList();
        assertNotEquals(expResult, result);
        System.out.println("getPtLoNamePresetList Done");

    }

    /**
     * Test of getAreaNameList method, of class CreatePointLocationBean.
     */
    @Test
    public void test25_GetAreaNameList() {
        System.out.println("getAreaNameList");
        test05_PresetFields_Filters_Object();
        List<String> expResult = new ArrayList();
        expResult.add(filters.getArea().getLabel());
        List<String> result = instance.getAreaNameList();
        assertEquals(expResult, result);
        System.out.println("getAreaNameList Done");
    }

    /**
     * Test of getAreaNameList method, of class CreatePointLocationBean.
     */
    @Test
    public void test26_GetAreaNameList() {
        System.out.println("getAreaNameList");
        test05_PresetFields_Filters_Object();
        List<String> expResult = new ArrayList();
        List<String> result = instance.getAreaNameList();
        assertNotEquals(expResult, result);
        System.out.println("getAreaNameList Done");
    }

}
