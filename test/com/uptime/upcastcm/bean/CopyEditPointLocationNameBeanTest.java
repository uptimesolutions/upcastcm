/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.FaultFrequenciesClient;
import com.uptime.upcastcm.http.client.PointLocationNamesClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import com.uptime.upcastcm.utils.helperclass.PresetUtil;
import com.uptime.upcastcm.utils.vo.PointLocationNamesVO;
import com.uptime.upcastcm.utils.vo.UserSitesVO;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import javax.faces.component.UIViewRoot;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.junit.Test;
import org.junit.FixMethodOrder;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import static org.powermock.api.mockito.PowerMockito.when;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.primefaces.PrimeFaces;

/**
 *
 * @author Joseph
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(PowerMockRunner.class)
@PowerMockIgnore("jdk.internal.reflect.*")
@PrepareForTest({FacesContext.class, ExternalContext.class, ServiceRegistrarClient.class, PointLocationNamesClient.class, PresetsBean.class, UserManageBean.class, PointLocationNamesClient.class, PrimeFaces.class, PrimeFaces.Ajax.class})
public class CopyEditPointLocationNameBeanTest {

    @Mock
    ServiceRegistrarClient serviceRegistrarClient;

    @Mock
    FaultFrequenciesClient faultFrequenciesClient;
    @Mock
    PointLocationNamesClient pointLocationNamesClient;

    @Mock
    FacesContext facesContext;

    @Mock
    ExternalContext externalContext;

    @Mock
    UserManageBean userManageBean;
    @Mock
    PresetsBean presetsBean;
    @Mock
    HttpServletRequest request;
    @Mock
    HttpSession session;
    @Mock
    UIViewRoot root;
    @Mock
    NavigationBean navigationBean;
  

    private final CopyEditPointLocationNameBean instance;
    Map<String, Object> sessionMap;

    public CopyEditPointLocationNameBeanTest() {
        PointLocationNamesVO plnvo;
        UserSitesVO uvo;
                
        PowerMockito.mockStatic(FacesContext.class);
        PowerMockito.mockStatic(PointLocationNamesClient.class);
        PowerMockito.mockStatic(ServiceRegistrarClient.class);

        facesContext = PowerMockito.mock(FacesContext.class);
        externalContext = PowerMockito.mock(ExternalContext.class);
        userManageBean = PowerMockito.mock(UserManageBean.class);
        request = PowerMockito.mock(HttpServletRequest.class);
        session = PowerMockito.mock(HttpSession.class);
        root = PowerMockito.mock(UIViewRoot.class);
        navigationBean = PowerMockito.mock(NavigationBean.class);
        presetsBean = PowerMockito.mock(PresetsBean.class);
      

        pointLocationNamesClient = PowerMockito.mock(PointLocationNamesClient.class);
        serviceRegistrarClient = PowerMockito.mock(ServiceRegistrarClient.class);

        sessionMap = new HashMap();
        sessionMap.put("presetsBean", presetsBean);
        sessionMap.put("userManageBean", userManageBean);
       
        session.setAttribute("navigationBean", navigationBean);
        session.setAttribute("userManageBean", userManageBean);
        
        uvo = new UserSitesVO();
        uvo.setSiteId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        uvo.setSiteName("Test Site 1");
     
        when(PointLocationNamesClient.getInstance()).thenReturn(pointLocationNamesClient);
        when(ServiceRegistrarClient.getInstance()).thenReturn(serviceRegistrarClient);
        when(FacesContext.getCurrentInstance()).thenReturn(facesContext);
        when(facesContext.getExternalContext()).thenReturn(externalContext);
        when(externalContext.getSessionMap()).thenReturn(sessionMap);
        when(facesContext.getExternalContext().getRequest()).thenReturn(request);
        when(request.getSession()).thenReturn(session);
        when(userManageBean.getPresetUtil()).thenReturn(new PresetUtil());
        when(session.getAttribute("navigationBean")).thenReturn(navigationBean);
        when(session.getAttribute("presetsBean")).thenReturn(presetsBean);
        when(session.getAttribute("userManageBean")).thenReturn(userManageBean);
        when(userManageBean.getCurrentSite()).thenReturn(uvo);
        UserSitesVO usvo = new UserSitesVO();
        usvo.setCustomerAccount("77777");
        when(userManageBean.getCurrentSite()).thenReturn(usvo);

        request = PowerMockito.mock(HttpServletRequest.class);
        session = PowerMockito.mock(HttpSession.class);
        root = PowerMockito.mock(UIViewRoot.class);
        navigationBean = PowerMockito.mock(NavigationBean.class);

        instance = new CopyEditPointLocationNameBean();
        
        plnvo = new PointLocationNamesVO();
        plnvo.setPointLocationName("Test Point Location");
        plnvo.setSiteName("Test Site 1");
        plnvo.setSiteId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        plnvo.setCustomerAccount("77777");
        plnvo.setDescription("Test Description 1");
        instance.setPointLocationNamesVO(plnvo);
    }

    /**
     * Test of presetFields method, of class CopyEditPointLocationNameBean.
     */
    @Test
    public void test1_PresetFields_int_Object() {
        System.out.println("presetFields");
        instance.presetFields(1, instance.getPointLocationNamesVO());
        System.out.println("presetFields completed");
    }

    /**
     * Test of submit method, of class CopyEditPointLocationNameBean.
     */
    @Test
    public void test2_Submit() {
        System.out.println("submit- copy at site level");
        PowerMockito.mockStatic(PrimeFaces.class);
        PowerMockito.mockStatic(PrimeFaces.Ajax.class);
        
        PrimeFaces pf = PowerMockito.mock(PrimeFaces.class);
        PrimeFaces.Ajax ax = PowerMockito.mock(PrimeFaces.Ajax.class);
        when(pf.current()).thenReturn(pf);
        when(pf.ajax()).thenReturn(ax);
        ax.addCallbackParam("presetsTabViewId:pointLocationNamesFormId", new Object());
        instance.setOperationType(2);
        instance.getPointLocationNamesVO().setSiteName("Test Site 1");
        
        when(pointLocationNamesClient.createSitePointLocationNames(serviceRegistrarClient.getServiceHostURL(PRESET_SERVICE_NAME), instance.getPointLocationNamesVO())).thenReturn(Boolean.TRUE);
        instance.submit();
        System.out.println("submit completed");
    }

    /**
     * Test of submit method, of class CopyEditPointLocationNameBean.
     */
    @Test
    public void test3_Submit() {
        System.out.println("submit- edit at site level");
        PowerMockito.mockStatic(PrimeFaces.class);
        PowerMockito.mockStatic(PrimeFaces.Ajax.class);
        
        PrimeFaces pf = PowerMockito.mock(PrimeFaces.class);
        PrimeFaces.Ajax ax = PowerMockito.mock(PrimeFaces.Ajax.class);
        when(pf.current()).thenReturn(pf);
        when(pf.ajax()).thenReturn(ax);
        ax.addCallbackParam("presetsTabViewId:pointLocationNamesFormId", new Object());
        instance.setOperationType(1);
        instance.getPointLocationNamesVO().setSiteName("Test Site 1");
        
        when(pointLocationNamesClient.updateSitePointLocationNames(serviceRegistrarClient.getServiceHostURL(PRESET_SERVICE_NAME), instance.getPointLocationNamesVO())).thenReturn(Boolean.TRUE);
        instance.submit();
        System.out.println("submit completed");
    }

}
