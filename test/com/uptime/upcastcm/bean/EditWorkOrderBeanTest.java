/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.PointClient;
import com.uptime.upcastcm.http.client.PointLocationClient;
import com.uptime.upcastcm.http.client.WorkOrderClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import static com.uptime.upcastcm.utils.enums.WorkOrderActionEnum.getActionItemList;
import static com.uptime.upcastcm.utils.enums.WorkOrderIssueEnum.getIssueItemList;
import static com.uptime.upcastcm.utils.enums.WorkOrderPriorityEnum.getPriorityItemList;
import com.uptime.upcastcm.utils.helperclass.Filters;
import com.uptime.upcastcm.utils.vo.PointLocationVO;
import com.uptime.upcastcm.utils.vo.PointVO;
import com.uptime.upcastcm.utils.vo.TrendDetailsVO;
import com.uptime.upcastcm.utils.vo.UserPreferencesVO;
import com.uptime.upcastcm.utils.vo.UserSitesVO;
import com.uptime.upcastcm.utils.vo.WorkOrderVO;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.faces.component.UIViewRoot;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import static org.powermock.api.mockito.PowerMockito.when;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.primefaces.PrimeFaces;
import org.primefaces.event.NodeExpandEvent;
import org.primefaces.model.TreeNode;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.MenuModel;

/**
 *
 * @author kpati
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(PowerMockRunner.class)
@PowerMockIgnore("jdk.internal.reflect.*")
@PrepareForTest({FacesContext.class, ExternalContext.class, UserManageBean.class, ServiceRegistrarClient.class, NavigationBean.class, WorkOrderClient.class, PointLocationClient.class, PointClient.class, PrimeFaces.class, PrimeFaces.Ajax.class})
public class EditWorkOrderBeanTest {

    @Mock
    ServiceRegistrarClient serviceRegistrarClient;
    @Mock
    WorkOrderClient workOrderClient;
    @Mock
    FacesContext facesContext;
    @Mock
    ExternalContext externalContext;
    @Mock
    HttpServletRequest request;
    @Mock
    HttpSession session;
    @Mock
    UIViewRoot root;
    @Mock
    NavigationBean navigationBean;
    @Mock
    UserManageBean userManageBean;
    @Mock
    PointLocationClient pointLocationClient;
    @Mock
    PointClient pointClient;

    Map<String, Object> sessionMap;
    String customerAccount;
    MenuModel breadcrumbModel;
    private final EditWorkOrderBean instance;
    private final Filters filters;
    WorkOrderVO workOrderVO;

    public EditWorkOrderBeanTest() {
        PowerMockito.mockStatic(FacesContext.class);
        PowerMockito.mockStatic(ServiceRegistrarClient.class);
        PowerMockito.mockStatic(WorkOrderClient.class);
        PowerMockito.mockStatic(PointLocationClient.class);
        PowerMockito.mockStatic(PointClient.class);

        facesContext = PowerMockito.mock(FacesContext.class);
        externalContext = PowerMockito.mock(ExternalContext.class);
        request = PowerMockito.mock(HttpServletRequest.class);
        session = PowerMockito.mock(HttpSession.class);
        root = PowerMockito.mock(UIViewRoot.class);
        navigationBean = PowerMockito.mock(NavigationBean.class);
        userManageBean = PowerMockito.mock(UserManageBean.class);

        workOrderClient = PowerMockito.mock(WorkOrderClient.class);
        serviceRegistrarClient = PowerMockito.mock(ServiceRegistrarClient.class);
        pointLocationClient = PowerMockito.mock(PointLocationClient.class);
        pointClient = PowerMockito.mock(PointClient.class);

        sessionMap = new HashMap();
        sessionMap.put("navigationBean", navigationBean);
        sessionMap.put("userManageBean", userManageBean);
        session.setAttribute("navigationBean", navigationBean);
        session.setAttribute("userManageBean", userManageBean);
      
        filters = new Filters();
       
        filters.setCountry("Test Country 1");
        filters.setRegion("Test Region 1");
        filters.setSite(new SelectItem(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"), "Test Site 1"));
        filters.setArea(new SelectItem(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"), "Test Area 1"));
        filters.setAsset(new SelectItem(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"), "Test Asset 1"));
        when(userManageBean.getSelectedFilters()).thenReturn(filters);

        when(ServiceRegistrarClient.getInstance()).thenReturn(serviceRegistrarClient);
        when(WorkOrderClient.getInstance()).thenReturn(workOrderClient);
        when(PointLocationClient.getInstance()).thenReturn(pointLocationClient);
        when(PointClient.getInstance()).thenReturn(pointClient);
        when(FacesContext.getCurrentInstance()).thenReturn(facesContext);
        when(facesContext.getExternalContext()).thenReturn(externalContext);
        when(externalContext.getSessionMap()).thenReturn(sessionMap);
        when(facesContext.getExternalContext().getRequest()).thenReturn(request);
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("navigationBean")).thenReturn(navigationBean);
        when(session.getAttribute("userManageBean")).thenReturn(userManageBean);

        when(facesContext.getViewRoot()).thenReturn(root);

        customerAccount = "77777";
        UserPreferencesVO userVO = new UserPreferencesVO();
        userVO.setDefaultSite(UUID.randomUUID());
        userVO.setCustomerAccount("77777");
        when(session.getAttribute("userVO")).thenReturn(userVO);
        UserSitesVO usvo = new UserSitesVO();
        usvo.setCustomerAccount(customerAccount);
        usvo.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"));
        usvo.setSiteName("Test Site 1");
        when(userManageBean.getCurrentSite()).thenReturn(usvo);

        instance = new EditWorkOrderBean();
    }

    @Before
    public void setUp() {
        TreeNode tn;
        List<TreeNode> tnList;
        
        workOrderVO = new WorkOrderVO();
        breadcrumbModel = new DefaultMenuModel();
        when(navigationBean.getBreadcrumbModel()).thenReturn(breadcrumbModel);
        tn = PowerMockito.mock(TreeNode.class);
        
        tnList = new ArrayList<>();
        tnList.add(PowerMockito.mock(TreeNode.class));
        
        when(navigationBean.getTreeNodeRoot()).thenReturn(tn);
        when(navigationBean.getTreeNodeRoot().getChildren()).thenReturn(tnList);
    }

    /**
     * Test of init method, of class EditWorkOrderBean.
     */
    @Test
    public void test01_Init() {
        System.out.println("init");

        List<SelectItem> expectedResult = new ArrayList();
        instance.init();
        List<SelectItem> result = instance.getAssetList();
        assertEquals(expectedResult, result);
        System.out.println("init Done");
    }

    /**
     * Test of init method, of class CreateWorkOrderBean.
     */
    @Test
    public void test02_Init() {
        System.out.println("init");
        List<SelectItem> expectedResult = new ArrayList();
        expectedResult.add(filters.getAsset());
        instance.init();
        List<SelectItem> result = instance.getAssetList();
        assertNotEquals(expectedResult, result);
        System.out.println("init Done");
    }

    /**
     * Test of resetPage method, of class EditWorkOrderBean.
     */
    @Test
    public void test02_ResetPage() {
        System.out.println("resetPage");
        List<SelectItem> expectedResult = new ArrayList();
        instance.resetPage();
        List<SelectItem> result = instance.getAssetList();
        assertEquals(expectedResult, result);
        System.out.println("resetPage Done");
    }

    /**
     * Test of resetPage method, of class CreateWorkOrderBean.
     */
    @Test
    public void test021_ResetPage() {
        System.out.println("resetPage");
        List<SelectItem> expectedResult = new ArrayList();
        expectedResult.add(filters.getAsset());
        instance.resetPage();
        List<SelectItem> result = instance.getAssetList();
        assertNotEquals(expectedResult, result);
        System.out.println("resetPage Done");

    }

    /**
     * Test of presetFields method, of class EditWorkOrderBean.
     */
    @Test
    public void test03_PresetFields_Filters_Object() {
        System.out.println("presetFields");
        workOrderVO.setChannelType("Test Channel Type");
        workOrderVO.setCustomerAccount("77777");
        workOrderVO.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"));
        workOrderVO.setAreaId(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"));
        workOrderVO.setAssetId(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"));
        workOrderVO.setPointLocationId(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"));
        workOrderVO.setPointId(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"));
        workOrderVO.setApSetId(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"));

        workOrderVO.setPriority((byte) 4);
        workOrderVO.setCreatedDate(Instant.now());

        Whitebox.setInternalState(instance, "workOrderVO", workOrderVO);
        List<WorkOrderVO> workOrderVOs = new ArrayList();
        WorkOrderVO wovo = new WorkOrderVO();
        wovo.setApSetId(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"));
        wovo.setChannelType("Test Channel Type");
        wovo.setParamName("Overall");
        wovo.setPointId(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"));
        wovo.setPointLocationId(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"));
        wovo.setRowId(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"));
        workOrderVOs.add(wovo);
        when(workOrderClient.getWorkOrderTrendsByCustomerSiteAreaAssetPriorityDate(serviceRegistrarClient.getServiceHostURL(WORK_ORDER_SERVICE_NAME), instance.getWorkOrderVO().getCustomerAccount(), instance.getWorkOrderVO().getSiteId(), instance.getWorkOrderVO().getAreaId(), instance.getWorkOrderVO().getAssetId(), instance.getWorkOrderVO().getPriority(), instance.getWorkOrderVO().getCreatedDate())).thenReturn(workOrderVOs);
       
        List<PointVO> pointVOList = new ArrayList();
        PointVO pointVO = new PointVO();
        pointVO.setPointId(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"));
        pointVO.setPointName("Test Point 1");
        pointVO.setApSetId(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"));
        pointVO.setAlSetId(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"));
        pointVO.setParamType("Test Param 1");

        pointVOList.add(pointVO);
        when(pointClient.getPointsByPK(serviceRegistrarClient.getServiceHostURL(CONFIGURATION_SERVICE_NAME), instance.getWorkOrderVO().getCustomerAccount(), instance.getWorkOrderVO().getSiteId(), instance.getWorkOrderVO().getAreaId(), instance.getWorkOrderVO().getAssetId(), instance.getWorkOrderVO().getPointLocationId(), instance.getWorkOrderVO().getPointId(), instance.getWorkOrderVO().getApSetId())).thenReturn(pointVOList);
       
        List<PointLocationVO> pointLocationVOs = new ArrayList();
        PointLocationVO plvo = new PointLocationVO();
        plvo.setPointLocationId(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"));
        plvo.setPointLocationName("Test Point Location 1");
        pointLocationVOs.add(plvo);
        when(pointLocationClient.getPointLocationsByPK(serviceRegistrarClient.getServiceHostURL(CONFIGURATION_SERVICE_NAME), instance.getWorkOrderVO().getCustomerAccount(), instance.getWorkOrderVO().getSiteId(), instance.getWorkOrderVO().getAreaId(), instance.getWorkOrderVO().getAssetId(), instance.getWorkOrderVO().getPointLocationId())).thenReturn(pointLocationVOs);
        
        instance.presetFields(filters, workOrderVO);

        assertTrue(!instance.getWorkOrderVO().getTrendDetails().isEmpty());
    }

    /**
     * Test of submit method, of class EditWorkOrderBean.
     */
    @Test
    public void test04_Submit() {
        System.out.println("submit");
       
        Whitebox.setInternalState(instance, "navigationFilter", filters);
        PowerMockito.mockStatic(PrimeFaces.class);
        PowerMockito.mockStatic(PrimeFaces.Ajax.class);
        PrimeFaces pf = PowerMockito.mock(PrimeFaces.class);
        PrimeFaces.Ajax ax = PowerMockito.mock(PrimeFaces.Ajax.class);
        when(pf.current()).thenReturn(pf);
        when(pf.ajax()).thenReturn(ax);
        UserSitesVO usvo = new UserSitesVO();
        usvo.setCustomerAccount(customerAccount);
        when(userManageBean.getCurrentSite()).thenReturn(usvo);
      
         NodeExpandEvent nee = PowerMockito.mock(NodeExpandEvent.class);
        TreeNode tn = PowerMockito.mock(TreeNode.class);
        when(tn.getData()).thenReturn(new String());
        List<TreeNode> tnList = new ArrayList<>();
        tnList.add(tn);
        when(navigationBean.getTreeNodeRoot()).thenReturn(tn);
        when(navigationBean.getTreeNodeRoot().getChildren()).thenReturn(tnList);
        when(workOrderClient.updateWorkOrder(ServiceRegistrarClient.getInstance().getServiceHostURL(WORK_ORDER_SERVICE_NAME), instance.getWorkOrderVO())).thenReturn(Boolean.TRUE);
        workOrderVO.setChannelType("Test Channel Type");
        workOrderVO.setCustomerAccount("77777");
        workOrderVO.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"));
        workOrderVO.setAreaId(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"));
        workOrderVO.setAssetId(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"));

        workOrderVO.setPriority((byte) 4);
        workOrderVO.setNewPriority((byte)3);
        workOrderVO.setCreatedDate(Instant.now());

        Whitebox.setInternalState(instance, "workOrderVO", workOrderVO);
        instance.submit();
        String expectedResult = "Priority changed from 4 to 3.";
        assertEquals(expectedResult, instance.getWorkOrderVO().getNotes());
    }

    /**
     * Test of presetFields method, of class EditWorkOrderBean.
     */
    @Test(expected = UnsupportedOperationException.class)
    public void test05_PresetFields_int_Object() {
        System.out.println("presetFields");
        int operationType = 0;
        Object object = null;
        instance.presetFields(operationType, object);
    }

    /**
     * Test of presetFields method, of class EditWorkOrderBean.
     */
    @Test(expected = UnsupportedOperationException.class)
    public void test06_PresetFields_Object() {
        System.out.println("presetFields");
        Object object = null;
        instance.presetFields(object);
    }

    /**
     * Test of getPriorities method, of class EditWorkOrderBean.
     */
    @Test
    public void test08_GetPriorities() {
        System.out.println("getPriorities");
        assertEquals(instance.getPriorities().size(), getPriorityItemList().size());
        System.out.println("getPriorities Done");
    }

    /**
     * Test of getIssueList method, of class EditWorkOrderBean.
     */
    @Test
    public void test09_GetIssueList() {
        System.out.println("getIssueList");
        assertEquals(instance.getIssueList().size(), getIssueItemList().size());
        System.out.println("getIssueList Done");
    }

    /**
     * Test of getActionList method, of class EditWorkOrderBean.
     */
    @Test
    public void test10_GetActionList() {
        System.out.println("getActionList");
        assertEquals(instance.getActionList().size(), getActionItemList().size());
        System.out.println("getActionList Done");
    }

    /**
     * Test of updateBean method, of class EditWorkOrderBean.
     */
    @Test
    public void testUpdateBean() {
        System.out.println("updateBean");
        instance.updateBean();
        System.out.println("updateBean Done");
    }

    /**
     * Test of removeTrend method, of class EditWorkOrderBean.
     */
    @Test
    public void test01_RemoveTrend() {
        System.out.println("removeTrend");
        List<TrendDetailsVO> trendDetails = new ArrayList();
        TrendDetailsVO trendDetailsVO = new TrendDetailsVO();
        trendDetailsVO.setParamName("Param 1");
        trendDetails.add(trendDetailsVO);
        instance.getWorkOrderVO().setTrendDetails(trendDetails);
        instance.removeTrend(null);
        assertTrue(!instance.getWorkOrderVO().getTrendDetails().isEmpty());
        System.out.println("RemoveTrend Done");
    }

    /**
     * Test of removeTrend method, of class CreateWorkOrderBean.
     */
    @Test
    public void test02_RemoveTrend() {
        System.out.println("removeTrend");
        List<TrendDetailsVO> trendDetails = new ArrayList();
        TrendDetailsVO trendDetailsVO = new TrendDetailsVO();
        trendDetailsVO.setParamName("Param 1");
        trendDetails.add(trendDetailsVO);
        instance.getWorkOrderVO().setTrendDetails(trendDetails);
        instance.removeTrend(trendDetailsVO);
        assertTrue(instance.getWorkOrderVO().getTrendDetails().isEmpty());
        System.out.println("RemoveTrend Done");
    }

    /**
     * Test of viewTrendGraph method, of class EditWorkOrderBean.
     */
    //@Test
    public void testViewTrendGraph() {
        System.out.println("viewTrendGraph");
        TrendDetailsVO trendDetailsVO = null;
        instance.viewTrendGraph(trendDetailsVO);
    }

    /**
     * Test of openAddTrendOverlay method, of class EditWorkOrderBean.
     */
    @Test
    public void testOpenAddTrendOverlay() {
        System.out.println("openAddTrendOverlay");
        instance.openAddTrendOverlay();
    }
}
