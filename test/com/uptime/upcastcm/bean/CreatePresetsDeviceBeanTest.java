/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.featuresflag.enums.UptimeFeaturesEnum;
import com.uptime.upcastcm.http.client.APALSetClient;
import com.uptime.upcastcm.http.client.DevicePresetClient;
import static com.uptime.upcastcm.utils.enums.ACSensorTypeEnum.getACSensorTypeItemList;
import static com.uptime.upcastcm.utils.enums.DCSensorTypeEnum.getDCSensorTypeItemList;
import static com.uptime.upcastcm.utils.enums.DeviceTypeEnum.getDeviceTypeItemList;
import static com.uptime.upcastcm.utils.enums.ParamUnitEnum.getFrequencyPlusUnits;
import com.uptime.upcastcm.utils.helperclass.PresetUtil;
import com.uptime.upcastcm.utils.vo.ApAlSetVO;
import com.uptime.upcastcm.utils.vo.DevicePresetVO;
import com.uptime.upcastcm.utils.vo.UserSitesVO;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import static org.powermock.api.mockito.PowerMockito.when;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.primefaces.PrimeFaces;
import org.togglz.junit.TogglzRule;

/**
 *
 * @author Joseph
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(PowerMockRunner.class)
@PowerMockIgnore("jdk.internal.reflect.*")
@PrepareForTest({FacesContext.class, ExternalContext.class, ServiceRegistrarClient.class, UserManageBean.class, APALSetClient.class, DevicePresetClient.class, PresetUtil.class, PresetsBean.class, PrimeFaces.class, PrimeFaces.Ajax.class})
public class CreatePresetsDeviceBeanTest {

    @Rule
    public TogglzRule togglzRule = TogglzRule.allEnabled(UptimeFeaturesEnum.UptimeFeatures.class);

    @Mock
    ServiceRegistrarClient serviceRegistrarClient;

    @Mock
    APALSetClient aPALSetClient;

    @Mock
    FacesContext facesContext;

    @Mock
    ExternalContext externalContext;

    @Mock
    UserManageBean userManageBean;

    @Mock
    DevicePresetClient devicePresetClient;

    @Mock
    HttpServletRequest request;

    @Mock
    HttpSession session;

    @Mock
    PresetUtil presetUtil;

    @Mock
    PresetsBean presetsBean;

    @Mock
    PrimeFaces pf;

    @Mock
    PrimeFaces.Ajax ax;

    Map<String, Object> sessionMap;
    DevicePresetVO devicePresetVO;
    private final CreatePresetsDeviceBean instance;
    List<ApAlSetVO> apalVOs;

    public CreatePresetsDeviceBeanTest() {
        UserSitesVO uvo;

        PowerMockito.mockStatic(FacesContext.class);

        facesContext = PowerMockito.mock(FacesContext.class);
        externalContext = PowerMockito.mock(ExternalContext.class);
        request = PowerMockito.mock(HttpServletRequest.class);
        session = PowerMockito.mock(HttpSession.class);
        userManageBean = PowerMockito.mock(UserManageBean.class);
        presetUtil = PowerMockito.mock(PresetUtil.class);
        presetsBean = PowerMockito.mock(PresetsBean.class);

        PowerMockito.mockStatic(PrimeFaces.class);
        PowerMockito.mockStatic(PrimeFaces.Ajax.class);
        pf = PowerMockito.mock(PrimeFaces.class);
        ax = PowerMockito.mock(PrimeFaces.Ajax.class);
        sessionMap = new HashMap();
        sessionMap.put("userManageBean", userManageBean);
        sessionMap.put("presetsBean", presetsBean);
        session.setAttribute("userManageBean", userManageBean);

        uvo = new UserSitesVO();
        uvo.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        uvo.setSiteName("Test Site 1");

        devicePresetVO = new DevicePresetVO();
        devicePresetVO.setApSetId(UUID.fromString("5ee8fbf1-6d1e-4ac7-afa8-e1db3d8920c7"));
        ApAlSetVO apalVo = new ApAlSetVO();
        apalVo.setApSetId(UUID.fromString("5ee8fbf1-6d1e-4ac7-afa8-e1db3d8920c7"));
        apalVOs = new ArrayList();
        apalVOs.add(apalVo);
        devicePresetVO.setApAlSetVOList(apalVOs);
        devicePresetVO.setSensorType("Test Sensor");

        when(FacesContext.getCurrentInstance()).thenReturn(facesContext);
        when(facesContext.getExternalContext()).thenReturn(externalContext);
        when(externalContext.getSessionMap()).thenReturn(sessionMap);
        when(facesContext.getExternalContext().getRequest()).thenReturn(request);
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("userManageBean")).thenReturn(userManageBean);
        when(userManageBean.getPresetUtil()).thenReturn(presetUtil);
        when(presetUtil.getGlobalApAlSetsByCustomerSensorType("77777", "Test Sensor")).thenReturn(apalVOs);
        when(userManageBean.getCurrentSite()).thenReturn(uvo);
        UserSitesVO usvo = new UserSitesVO();
        usvo.setCustomerAccount("77777");
        when(userManageBean.getCurrentSite()).thenReturn(usvo);

        instance = new CreatePresetsDeviceBean();

    }

    /**
     * Test of init method, of class CreatePresetsDeviceBean.
     */
    @Test
    public void test00_Init() {
        System.out.println("init");
        instance.init();
        assertEquals(instance.getSetName(), null);
        assertEquals(instance.getSelectedDeviceType(), null);
        assertEquals(instance.getDevicePresetsVOList().size(), 0);
        System.out.println("init completed");
    }

    /**
     * Test of resetPage method, of class CreatePresetsDeviceBean.
     */
    @Test
    public void test01_ResetPage() {
        System.out.println("resetPage");
        instance.resetPage();
        assertEquals(instance.getSetName(), null);
        assertEquals(instance.getSelectedDeviceType(), null);
        assertEquals(instance.getDevicePresetsVOList().size(), 0);
        System.out.println("resetPage completed");
    }

    /**
     * Test of updateBean method, of class CreatePresetsDeviceBean.
     */
    @Test
    public void test02_UpdateBean() {
        System.out.println("updateBean");
        instance.updateBean();
        System.out.println("updateBean completed");
    }

    /**
     * Test of onSiteChange method, of class CreatePresetsDeviceBean.
     */
    @Test
    public void test03_OnSiteChange() {
        System.out.println("onSiteChange");
        instance.onSiteChange();
        System.out.println("onSiteChange completed");
    }

    /**
     * Test of onDeviceTypeChange method, of class CreatePresetsDeviceBean.
     */
    @Test
    public void test04_OnDeviceTypeChange() {
        System.out.println("onDeviceTypeChange");
        instance.setSelectedDeviceType("MistLX");
        instance.onDeviceTypeChange();
        assertEquals(instance.getDevicePresetsVOList().get(0).getSensorType(), "Ultrasonic");
        System.out.println("onDeviceTypeChange completed");
    }

    /**
     * Test of onApSetNameSelect method, of class CreatePresetsDeviceBean.
     */
    @Test
    public void test05_OnApSetNameSelect() {
        System.out.println("onApSetNameSelect");
        instance.onApSetNameSelect(devicePresetVO);
        assertEquals(1, devicePresetVO.getAlSetVOList().size());
        System.out.println("onApSetNameSelect completed");
    }

    /**
     * Test of submit method, of class CreatePresetsDeviceBean.
     */
    @Test
    public void test060_Submit() {
        System.out.println("submit");

        when(PrimeFaces.current()).thenReturn(pf);
        when(pf.ajax()).thenReturn(ax);

        ax.addCallbackParam("presetsTabViewId", new Object());
        ax.addCallbackParam("devicesFormId", new Object());
        instance.setSelectedDeviceType("MistLX");
        instance.setSiteOrGlobal("site");
        instance.onDeviceTypeChange();
        instance.submit();
        assertEquals(Boolean.TRUE, instance.isValidationFailed());
        System.out.println("submit completed");
    }

    /**
     * Test of submit method, of class CreatePresetsDeviceBean.
     */
    @Test
    public void test061_Submit() {
        System.out.println("submit");

        when(PrimeFaces.current()).thenReturn(pf);
        when(pf.ajax()).thenReturn(ax);

        ax.addCallbackParam("presetsTabViewId", new Object());
        ax.addCallbackParam("devicesFormId", new Object());
        instance.setSelectedDeviceType("MistLX");
        instance.onDeviceTypeChange();
        instance.setSetName("Test Set 1");
        instance.setSiteOrGlobal("site");

        List<DevicePresetVO> devicePresetsVOList = new ArrayList();
        DevicePresetVO dpvo = new DevicePresetVO();
        dpvo.setAlSetId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        dpvo.setAlSetName("Al Set Name 1");
        dpvo.setApSetId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        dpvo.setApSetName("Ap Set Name 1");
        dpvo.setCustomerAccount("77777");
        dpvo.setSiteId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        devicePresetsVOList.add(dpvo);
        instance.setDevicePresetsVOList(devicePresetsVOList);
        instance.submit();
        assertEquals(Boolean.FALSE, instance.isValidationFailed());
        System.out.println("submit completed");
    }

    /**
     * Test of submit method, of class CreatePresetsDeviceBean.
     */
    @Test
    public void test062_Submit() {
        System.out.println("submit");

        when(PrimeFaces.current()).thenReturn(pf);
        when(pf.ajax()).thenReturn(ax);

        ax.addCallbackParam("presetsTabViewId", new Object());
        ax.addCallbackParam("devicesFormId", new Object());

        instance.setSelectedDeviceType("MistLX");
        instance.onDeviceTypeChange();
        instance.setSetName("Test Set 1");
        instance.submit();
        assertEquals(Boolean.TRUE, instance.isValidationFailed());
        System.out.println("submit completed");
    }

    /**
     * Test of getFrequencyUnitsList method, of class CreatePresetsDeviceBean.
     */
    @Test
    public void test07_GetFrequencyUnitsList() {
        System.out.println("getFrequencyUnitsList");
        assertEquals(instance.getFrequencyUnitsList().size(), getFrequencyPlusUnits().size());
        System.out.println("getFrequencyUnitsList completed");
    }

    /**
     * Test of addDevicePreset method, of class CreatePresetsDeviceBean.
     */
    @Test
    public void test08_AddDevicePreset() {
        System.out.println("addDevicePreset");
        instance.addDevicePreset();
        System.out.println("addDevicePreset completed");
    }

    /**
     * Test of deleteDevicePresetVO method, of class CreatePresetsDeviceBean.
     */
    @Test
    public void test10_DeleteApAlRow() {
        System.out.println("deleteApAlRow");
        instance.deleteDevicePresetVO(devicePresetVO);
        System.out.println("deleteApAlRow completed");
    }

    /**
     * Test of onSiteChange method, of class CreatePresetsDeviceBean.
     */
    @Test
    public void testOnSiteChange() {
        System.out.println("onSiteChange");
        instance.onSiteChange();
    }

    /**
     * Test of getDevicePresetsVOList method, of class CreatePresetsDeviceBean.
     */
    @Test
    public void testGetDevicePresetsVOList() {
        System.out.println("getDevicePresetsVOList");
        List<DevicePresetVO> expResult = new ArrayList<>();
        List<DevicePresetVO> result = instance.getDevicePresetsVOList();
        assertEquals(expResult, result);
    }

    /**
     * Test of getDeviceTypeList method, of class CreatePresetsDeviceBean.
     */
//    @Test
    public void testGetDeviceTypeList() {
        System.out.println("getDeviceTypeList");
        assertEquals(instance.getDeviceTypeList().size(), 2);
        System.out.println("getDeviceTypeList completed");
    }

    /**
     * Test of isDisable method, of class CreatePresetsDeviceBean.
     */
    @Test
    public void testIsDisable() {
        System.out.println("isDisable");
        boolean expResult = false;
        boolean result = instance.isDisable();
        assertEquals(expResult, result);
    }

    /**
     * Test of setDisable method, of class CreatePresetsDeviceBean.
     */
    @Test
    public void testSetDisable() {
        System.out.println("setDisable");
        boolean disable = false;
        instance.setDisable(disable);
    }

    /**
     * Test of isValidationFailed method, of class CreatePresetsDeviceBean.
     */
    @Test
    public void testIsValidationFailed() {
        System.out.println("isValidationFailed");
        boolean expResult = false;
        boolean result = instance.isValidationFailed();
        assertEquals(expResult, result);
    }

    /**
     * Test of getSelectedDeviceType method, of class CreatePresetsDeviceBean.
     */
    @Test
    public void testGetSelectedDeviceType() {
        System.out.println("getSelectedDeviceType");
        String expResult = null;
        String result = instance.getSelectedDeviceType();
        assertEquals(expResult, result);
    }

    /**
     * Test of getDcSensorTypeList method, of class CreatePresetsDeviceBean.
     */
    @Test
    public void testGetDcSensorTypeList() {
        System.out.println("getDcSensorTypeList");
        assertEquals(instance.getDcSensorTypeList().size(), getDCSensorTypeItemList().size());
        System.out.println("getDcSensorTypeList completed");
    }

    /**
     * Test of getAcSensorTypeList method, of class CreatePresetsDeviceBean.
     */
    @Test
    public void testGetAcSensorTypeList() {
        System.out.println("getAcSensorTypeList");
        assertEquals(instance.getAcSensorTypeList().size(), getACSensorTypeItemList().size());
        System.out.println("getAcSensorTypeList completed");
    }

}
