/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.TachometerClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import com.uptime.upcastcm.utils.helperclass.PresetUtil;
import com.uptime.upcastcm.utils.vo.ChannelVO;
import com.uptime.upcastcm.utils.vo.UserSitesVO;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.junit.runners.MethodSorters;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import static org.powermock.api.mockito.PowerMockito.when;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;
import org.powermock.reflect.Whitebox;
import org.primefaces.PrimeFaces;

/**
 *
 * @author twilcox
 */
@RunWith(PowerMockRunner.class)
@PowerMockIgnore("jdk.internal.reflect.*")
@PrepareForTest({FacesContext.class, ExternalContext.class, UserManageBean.class, PrimeFaces.class, PrimeFaces.Ajax.class, ServiceRegistrarClient.class, TachometerClient.class})
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@PowerMockRunnerDelegate(JUnit4.class)
public class EditChannelBeanTest {
    
    @Mock
    ServiceRegistrarClient serviceRegistrarClient;
    
    @Mock
    TachometerClient tachometerClient; 
    
    @Mock
    FacesContext facesContext;
    
    @Mock
    ExternalContext externalContext;
    
    @Mock
    UserManageBean userManageBean;
    
    @Mock
    PrimeFaces pf;
    
    @Mock
    PrimeFaces.Ajax ax;
    
    private final EditChannelBean instance;
    private Map<String,Object> sessionMap;
    private List<ChannelVO> channelVOList;
    private ChannelVO channelVO;
    
    public EditChannelBeanTest() {
        UserSitesVO usvo;
        
        PowerMockito.mockStatic(ServiceRegistrarClient.class);
        PowerMockito.mockStatic(TachometerClient.class);
        PowerMockito.mockStatic(FacesContext.class);
        PowerMockito.mockStatic(ExternalContext.class);
        PowerMockito.mockStatic(UserManageBean.class);
        PowerMockito.mockStatic(PrimeFaces.class);
        PowerMockito.mockStatic(PrimeFaces.Ajax.class);
        
        serviceRegistrarClient = PowerMockito.mock(ServiceRegistrarClient.class);
        tachometerClient = PowerMockito.mock(TachometerClient.class);
        facesContext = PowerMockito.mock(FacesContext.class);
        externalContext = PowerMockito.mock(ExternalContext.class);
        userManageBean = PowerMockito.mock(UserManageBean.class);
        pf = PowerMockito.mock(PrimeFaces.class);
        ax = PowerMockito.mock(PrimeFaces.Ajax.class);
        
        when(PrimeFaces.current()).thenReturn(pf);
        when(pf.ajax()).thenReturn(ax);
        
        sessionMap = new HashMap();
        sessionMap.put("userManageBean", userManageBean);
        
        when(FacesContext.getCurrentInstance()).thenReturn(facesContext);
        when(facesContext.getExternalContext()).thenReturn(externalContext);
        when(externalContext.getSessionMap()).thenReturn(sessionMap);
        
        usvo = new UserSitesVO();
        usvo.setCustomerAccount("FEDEX EXPRESS");
        usvo.setSiteId(UUID.fromString("8b086e15-371f-4a8d-bedb-c1b67b49be3e"));
        when(userManageBean.getCurrentSite()).thenReturn(usvo);
        when(userManageBean.getPresetUtil()).thenReturn(new PresetUtil());
        
        when(ServiceRegistrarClient.getInstance()).thenReturn(serviceRegistrarClient);
        when(serviceRegistrarClient.getServiceHostURL(PRESET_SERVICE_NAME)).thenReturn("HOST");
        when(TachometerClient.getInstance()).thenReturn(tachometerClient);
        
        instance = new EditChannelBean();
    }
    
    @Before
    public void setUp() {
        channelVO = new ChannelVO();
        channelVO.setCustomerAccount("FEDEX EXPRESS");
        channelVO.setDeviceId("00100000");
        
        channelVOList = new ArrayList();
        channelVOList.add(channelVO);
        Whitebox.setInternalState(instance, "channelVO", channelVO);
    }

    /**
     * Test of init method, of class EditChannelBean.
     */
    @Test
    public void test00_Init() {
        System.out.println("init");
        Whitebox.setInternalState(instance, "channelVO", channelVO);
        instance.presetFields(channelVO, channelVOList);
        instance.init();
        assertEquals(instance.getChannelVO(), new ChannelVO());
        
        System.out.println("init Completed");
    }

    /**
     * Test of resetPage method, of class EditChannelBean.
     */
    @Test
    public void test01_ResetPage() {
        System.out.println("resetPage");
        Whitebox.setInternalState(instance, "channelVO", channelVO);
        instance.presetFields(channelVO, channelVOList);
        instance.resetPage();
        assertEquals(instance.getChannelVO(), new ChannelVO());
        
        System.out.println("resetPage Completed");
    }

    /**
     * Test of presetFields method, of class EditChannelBean.
     */
    @Test
    public void test02_PresetFields() {
        System.out.println("presetFields");
        Whitebox.setInternalState(instance, "channelVO", channelVO);
        instance.presetFields(channelVO, channelVOList);
        assertEquals(instance.getChannelVO(), channelVO);
        
        System.out.println("presetFields Completed");
    }

    /**
     * Test of updateBean method, of class EditChannelBean.
     */
    @Test
    public void test03_UpdateBean() {
        System.out.println("updateBean");
        
        // There is nothing to check with this method.
        instance.updateBean();
        
        System.out.println("updateBean Completed");
    }

    /**
     * Test of onTachChange method, of class EditChannelBean.
     */
    @Test
    public void test07_OnTachChange() {
        System.out.println("onTachChange");
        
        when(TachometerClient.getInstance().getGlobalTachometersByPK("HOST", userManageBean.getCurrentSite().getCustomerAccount(), null)).thenReturn(null);
        when(TachometerClient.getInstance().getSiteTachometersByPK("HOST", userManageBean.getCurrentSite().getCustomerAccount(), channelVO.getSiteId(), null)).thenReturn(null);
        Whitebox.setInternalState(instance, "channelVO", channelVO);
        instance.presetFields(channelVO, channelVOList);
        assertEquals(instance.getChannelVO().getSpeedRatio(), (float)0.0, (float)0.0);
        instance.onTachChange();
        assertEquals(instance.getChannelVO().getSpeedRatio(), (float)1.0, (float)0.0);
        
        System.out.println("onTachChange Completed");
    }

    /**
     * Test of submit method, of class EditChannelBean.
     */
    @Test
    public void test08_Submit() {
        System.out.println("submit");
        Whitebox.setInternalState(instance, "channelVOList", channelVOList);
        Whitebox.setInternalState(instance, "channelVO", channelVO);
        ChannelVO originalChannelVO = channelVO;
        Whitebox.setInternalState(instance, "originalChannelVO", originalChannelVO);
        // There is nothing to check with this method.
        instance.submit();
        
        System.out.println("submit Completed");
    }

    /**
     * Test of getChannelVO method, of class EditChannelBean.
     */
    @Test
    public void test10_GetChannelVO() {
        System.out.println("getChannelVO");
        Whitebox.setInternalState(instance, "channelVO", channelVO);
        instance.presetFields(channelVO, channelVOList);
        assertEquals(instance.getChannelVO(), channelVO);
        
        System.out.println("getChannelVO Completed");
    }

    /**
     * Test of getFaultFrequenciesList method, of class EditChannelBean.
     */
    @Test
    public void test11_GetFaultFrequenciesList() {
        System.out.println("getFaultFrequenciesList");
        Whitebox.setInternalState(instance, "channelVO", channelVO);
        instance.presetFields(channelVO, channelVOList);
        assertEquals(instance.getFaultFrequenciesList(), new ArrayList());
        
        System.out.println("getFaultFrequenciesList Completed");
    }

    /**
     * Test of getTachometersList method, of class EditChannelBean.
     */
    @Test
    public void test12_GetTachometersList() {
        System.out.println("getTachometersList");
        Whitebox.setInternalState(instance, "channelVO", channelVO);
        instance.presetFields(channelVO, channelVOList);
        assertEquals(instance.getTachometersList(), new ArrayList());

        System.out.println("getTachometersList Completed");
    }

    /**
     * Test of getRollDiameterUnitsList method, of class EditChannelBean.
     */
    @Test
    public void test13_GetRollDiameterUnitsList() {
        System.out.println("getRollDiameterUnitsList");
        
        assertEquals(2, instance.getRollDiameterUnitsList().size());
        instance.getRollDiameterUnitsList();
        
        System.out.println("getRollDiameterUnitsList Completed");
    }

    /**
     * Test of isTachValidationFailed method, of class EditChannelBean.
     */
    @Test
    public void test14_IsTachValidationFailed() {
        System.out.println("isTachValidationFailed");
        
        assertFalse(instance.isTachValidationFailed());

        System.out.println("isTachValidationFailed Completed");
    }
    
}
