/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.SiteClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import static com.uptime.upcastcm.utils.enums.ContactRoleEnum.getRoleItemList;
import static com.uptime.upcastcm.utils.enums.CountryRegionEnum.getCountryList;
import static com.uptime.upcastcm.utils.enums.IndustryEnum.getIndustryItemList;
import com.uptime.upcastcm.utils.helperclass.Filters;
import com.uptime.upcastcm.utils.vo.SiteContactVO;
import com.uptime.upcastcm.utils.vo.SiteVO;
import com.uptime.upcastcm.utils.vo.UserSitesVO;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import static org.powermock.api.mockito.PowerMockito.when;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

/**
 *
 * @author kpati
 */
@RunWith(PowerMockRunner.class)
@PowerMockIgnore("jdk.internal.reflect.*")
@PrepareForTest({FacesContext.class, ExternalContext.class, HttpServletRequest.class, HttpSession.class, UserManageBean.class, ConfigurationBean.class, SiteClient.class, ServiceRegistrarClient.class})
public class EditSiteBeanTest {
    
    private final EditSiteBean instance;

    @Mock
    private final UserManageBean userManageBean;

    @Mock
    ServiceRegistrarClient serviceRegistrarClient;

    @Mock
    SiteClient siteClient;

    @Mock
    FacesContext facesContext;

    @Mock
    ExternalContext externalContext;

    @Mock
    HttpServletRequest mockRequest;
    
    @Mock
    ConfigurationBean configurationBean;

    Map<String, Object> sessionMap;
    SiteVO siteVO;
    String customerAccount;
    private final Filters filters;
    private final List<SiteContactVO> contactList;
    private List<String> industries;
    private List<String> countryList;
    private List<String> roleList;
    
    public EditSiteBeanTest() {
        PowerMockito.mockStatic(FacesContext.class);
        PowerMockito.mockStatic(ServiceRegistrarClient.class);
        PowerMockito.mockStatic(SiteClient.class);
        
        serviceRegistrarClient = PowerMockito.mock(ServiceRegistrarClient.class);
        siteClient = PowerMockito.mock(SiteClient.class);
        userManageBean = PowerMockito.mock(UserManageBean.class);
        configurationBean = PowerMockito.mock(ConfigurationBean.class);
        
        facesContext = PowerMockito.mock(FacesContext.class);
        externalContext = PowerMockito.mock(ExternalContext.class);
        
        sessionMap = new HashMap();
        sessionMap.put("userManageBean", userManageBean);
        sessionMap.put("configurationBean", configurationBean);
        
        when(ServiceRegistrarClient.getInstance()).thenReturn(serviceRegistrarClient);
        when(SiteClient.getInstance()).thenReturn(siteClient);
        when(FacesContext.getCurrentInstance()).thenReturn(facesContext);
        when(facesContext.getExternalContext()).thenReturn(externalContext);
        when(externalContext.getSessionMap()).thenReturn(sessionMap);
        when(serviceRegistrarClient.getServiceHostURL(CONFIGURATION_SERVICE_NAME)).thenReturn(CONFIGURATION_SERVICE_NAME);
        
        filters = new Filters();
        filters.setSite(new SelectItem(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), "Test Site 1"));
        filters.setArea(new SelectItem(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"), "Test Area 1"));
        filters.setAsset(new SelectItem(UUID.fromString("f33fcf5a-5f93-4504-bc09-74a61a460bf0"), "Test Asset 1"));
        
        customerAccount = "77777";
        instance = new EditSiteBean();
        siteVO = new SiteVO();
        
        contactList = new ArrayList();
        SiteContactVO scontactVO = new SiteContactVO();
        scontactVO.setSiteName("Test Site 201");
        scontactVO.setContactName("Test Contact Name");
        scontactVO.setContactTitle("Test Title");
        scontactVO.setRole("Test Role");
        scontactVO.setCustomerAccount(customerAccount);
        scontactVO.setContactEmail("test.contact@text.com");
        scontactVO.setContactPhone("1234567890");
        contactList.add(scontactVO);
        Whitebox.setInternalState(instance, "contactList", contactList);
        
        UserSitesVO usvo = new UserSitesVO();
        usvo.setCustomerAccount(customerAccount);
        when(userManageBean.getCurrentSite()).thenReturn(usvo);
        
        
    }
    
    @Before
    public void setUp() {
        
        siteVO.setSiteId(UUID.fromString("8b086e15-371f-4a8d-bedb-c1b67b49be3e"));
        siteVO.setCustomerAccount("Test 77777");
        siteVO.setSiteName("Test Site 201");
        siteVO.setPhysicalAddress1("4820 Executive Park Ct");
        siteVO.setPhysicalAddress2("Test Physical Address 2");
        siteVO.setPhysicalCity("Jacksonville");
        siteVO.setPhysicalPostalCode("32216");
        siteVO.setPhysicalStateProvince("FL");
        siteVO.setCountry("UNITED STATES");
        siteVO.setRegion("NORTH AMERICA");
        siteVO.setLatitude("30.252254");
        siteVO.setLongitude("-81.602805");
        siteVO.setIndustry("Production");
        siteVO.setTimezone("Pacific/Niue (UTC -11:00)");
        siteVO.setBillingAddress1("4820 Executive Park Ct");
        siteVO.setBillingCity("Jacksonville");
        siteVO.setBillingPostalCode("32216");
        siteVO.setBillingStateProvince("FL");
        siteVO.setShipAddress1("4820 Executive Park Ct");
        siteVO.setShipAddress2("Test Physical Address 2");
        siteVO.setShipCity("Jacksonville");
        siteVO.setShipPostalCode("32216");
        siteVO.setShipStateProvince("FL");
        siteVO.setSiteContactList(contactList);
        instance.setSiteVo(siteVO);
        
        industries = new ArrayList();
        industries.add("Automotive");
        industries.add("Production");
        industries.add("Mechanical");
        industries.add("Chemical");
        
        countryList = new ArrayList();
        countryList.add("Test Country 1");
        countryList.add("Test Country 2");
        
        roleList = new ArrayList();
        roleList.add("Test Role 1");
        roleList.add("Test Role 2");
        
        
    }

    /**
     * Test of init method, of class EditSiteBean.
     */
    @Test
    public void test01_Init() {
        System.out.println("init");
        instance.init();
        assertEquals(instance.getSiteVo().getCustomerAccount(), customerAccount);
        System.out.println("init Done");
    }

    /**
     * Test of resetPage method, of class EditSiteBean.
     */
    @Test
    public void test03_ResetPage() {
        System.out.println("resetPage");
        instance.resetPage();
        assertEquals(instance.getContactList().size(), 0);
        System.out.println("resetPage Done");
    }

    /**
     * Test of presetFields method, of class EditSiteBean.
     */
    @Test
    public void test05_PresetFields_Filters_Object() {
        System.out.println("presetFields");
        filters.setCountry("United States");
        filters.setRegion("Test Region");
        Filters presets = filters;
        Object object = (SiteVO)siteVO;
        List<SiteContactVO> filterContactList = new ArrayList();
        SiteContactVO scontactVO = new SiteContactVO();
        scontactVO.setSiteName("Test PresetFields Filters Site");
        scontactVO.setContactName("Test PresetFields Filters Contact Name");
        scontactVO.setContactTitle("Test PresetFields Filters Role");
        scontactVO.setCustomerAccount(customerAccount);
        filterContactList.add(scontactVO);
        when(serviceRegistrarClient.getServiceHostURL(CONFIGURATION_SERVICE_NAME)).thenReturn(CONFIGURATION_SERVICE_NAME);
        when(siteClient.getSiteContactsByCustomerSite(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), siteVO.getCustomerAccount(), siteVO.getSiteId())).thenReturn(filterContactList);
        String expectedResult = scontactVO.getSiteName();
        instance.presetFields(presets, object);
        //String result = siteVO.getSiteContactList().get(0).getSiteName();
        //assertEquals(expectedResult, result);
        System.out.println("presetFields Done");
    }



    /**
     * Test of onCountryChange method, of class EditSiteBean.
     */
    @Test
    public void test07_OnCountryChange() {
        System.out.println("onCountryChange");
        siteVO.setCountry("Algeria");
        String expectedResult = "ASIA";
        instance.onCountryChange();
        String result = instance.getSiteVo().getRegion();
        assertNotEquals(expectedResult, result);
        System.out.println("onCountryChange Done");
    }

    /**
     * Test of getLatLongByGeoLocation method, of class EditSiteBean.
     */
    @Test
    public void test08_GetLatLongByGeoLocation() {
        System.out.println("getLatLongByGeoLocation");
        SiteVO latLongSiteVo = new SiteVO();
        latLongSiteVo.setLatitude("30.222254");
        latLongSiteVo.setLongitude("-81.602305");
        when(siteClient.getLatLongByPhysicalAddress(siteVO.getPhysicalAddress1(), siteVO.getPhysicalCity(), siteVO.getPhysicalStateProvince(), siteVO.getPhysicalPostalCode())).thenReturn(latLongSiteVo);
        String expectedResult = latLongSiteVo.getLatitude();
        instance.getLatLongByGeoLocation();
        String result = instance.getSiteVo().getLatitude();
        assertEquals(expectedResult, result);
        System.out.println("getLatLongByGeoLocation Done");
    }

    /**
     * Test of populatePhyscAddForShipp method, of class EditSiteBean.
     */
    @Test
    public void test10_PopulatePhyscAddForShipp() {
        System.out.println("populatePhyscAddForShipp");
        instance.setSamePhyscAddShipChk(true);
        String expectedResult = siteVO.getShipAddress1();
        instance.populatePhyscAddForShipp();
        String result = instance.getSiteVo().getShipAddress1();
        assertEquals(expectedResult, result);
        System.out.println("populatePhyscAddForShipp Done");
    }

    /**
     * Test of populatePhyscAddForBill method, of class EditSiteBean.
     */
    @Test
    public void test12_PopulatePhyscAddForBill() {
        System.out.println("populatePhyscAddForBill");
        instance.setSamePhyscAddBillChk(true);
        String expectedResult = siteVO.getBillingAddress1();
        instance.populatePhyscAddForBill();
        String result = instance.getSiteVo().getBillingAddress1();
        assertEquals(expectedResult, result);
        System.out.println("populatePhyscAddForBill Done");

    }

    /**
     * Test of updateBean method, of class EditSiteBean.
     */
    @Test
    public void test14_UpdateBean() {
        System.out.println("updateBean");
        test08_GetLatLongByGeoLocation();
        String expectdResult = "30.222254";
        instance.updateBean();
        String result= instance.getSiteVo().getLatitude();
        assertEquals(expectdResult, result);
        System.out.println("updateBean Done");
    }

    /**
     * Test of onAddNewRow method, of class EditSiteBean.
     */
    @Test
    public void test17_OnAddNewRow() {
        System.out.println("onAddNewRow");
        int expectedResult = 2;
        instance.onAddNewRow();
        int result = instance.getSiteVo().getSiteContactList().size();
        assertEquals(expectedResult, result);
        System.out.println("onAddNewRow Done");
    }

    /**
     * Test of onDeleteRow method, of class EditSiteBean.
     */
    @Test
    public void test19_OnDeleteRow() {
        System.out.println("onDeleteRow");
        SiteContactVO siteContactVO = siteVO.getSiteContactList().get(0);
        int expectedResult = 0;
        instance.onDeleteRow(siteContactVO);
        int result = instance.getSiteVo().getSiteContactList().size();
        assertEquals(expectedResult, result);
        System.out.println("onDeleteRow Done");
    }

    /**
     * Test of submit method, of class EditSiteBean.
     */
    @Test
    public void test21_Submit() {
        System.out.println("submit");
        when(SiteClient.getInstance().updateSite(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), siteVO)).thenReturn(true);
        try {
            instance.submit();
            assertEquals(null, new SiteVO().getSiteId());
            System.out.println("submit Done");
        }
        catch (Exception ex) {
            System.out.println("Exception in Update- " + ex.toString());
        }
    }
    
    @Test
    public void test22_Submit() {
        System.out.println("submit");
        when(SiteClient.getInstance().updateSite(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), siteVO)).thenReturn(false);
        try {
            instance.submit();
            assertEquals(null, new SiteVO().getSiteId());
            System.out.println("submit Done");
        }
        catch (Exception ex) {
            System.out.println("Exception in Update- " + ex.toString());
        }
    }

    /**
     * Test of getCountries method, of class EditSiteBean.
     */
    @Test
    public void test23_GetCountries() {
        System.out.println("getCountries");
        assertEquals(instance.getCountries().size(), getCountryList(true).size());
        System.out.println("getCountries Done");
    }

    /**
     * Test of isSamePhyscAddShipChk method, of class EditSiteBean.
     */
    @Test
    public void test25_IsSamePhyscAddShipChk() {
        System.out.println("isSamePhyscAddShipChk");
        instance.setSamePhyscAddShipChk(false);
        boolean expResult = false;
        boolean result = instance.isSamePhyscAddShipChk();
        assertEquals(expResult, result);
        System.out.println("isSamePhyscAddShipChk Done");
    }

    /**
     * Test of setSamePhyscAddShipChk method, of class EditSiteBean.
     */
    @Test
    public void test27_SetSamePhyscAddShipChk() {
        System.out.println("setSamePhyscAddShipChk");
        boolean samePhyscAddShipChk = false;
        boolean expectedResult = false;
        instance.setSamePhyscAddShipChk(samePhyscAddShipChk);
        boolean result = instance.isSamePhyscAddShipChk();
        assertEquals(expectedResult, result);
        System.out.println("setSamePhyscAddShipChk Done");
    }

    /**
     * Test of isSamePhyscAddBillChk method, of class EditSiteBean.
     */
    @Test
    public void test29_IsSamePhyscAddBillChk() {
        System.out.println("isSamePhyscAddBillChk");
        instance.setSamePhyscAddBillChk(false);
        boolean expResult = false;
        boolean result = instance.isSamePhyscAddBillChk();
        assertEquals(expResult, result);
        System.out.println("isSamePhyscAddBillChk Done");
    }

    /**
     * Test of setSamePhyscAddBillChk method, of class EditSiteBean.
     */
    @Test
    public void test31_SetSamePhyscAddBillChk() {
        System.out.println("setSamePhyscAddBillChk");
        boolean samePhyscAddBillChk = false;
        boolean expectedResult = false;
        instance.setSamePhyscAddBillChk(samePhyscAddBillChk);
        boolean result = instance.isSamePhyscAddBillChk();
        assertEquals(expectedResult, result);
        System.out.println("setSamePhyscAddBillChk Done");
    }

    /**
     * Test of getTimezoneList method, of class EditSiteBean.
     */
    @Test
    public void test33_GetTimezoneList() {
        System.out.println("getTimezoneList");
        assertEquals(instance.getTimezoneList().size(), 0);
        System.out.println("getTimezoneList Done");
    }

    /**
     * Test of getSiteVo method, of class EditSiteBean.
     */
    @Test
    public void test35_GetSiteVo() {
        System.out.println("getSiteVo");
        instance.setSiteVo(siteVO);
        SiteVO expResult = siteVO;
        SiteVO result = instance.getSiteVo();
        assertEquals(expResult, result);
        System.out.println("getSiteVo Done");
    }

    /**
     * Test of setSiteVo method, of class EditSiteBean.
     */
    @Test
    public void test37_SetSiteVo() {
        System.out.println("setSiteVo");
        SiteVO siteVo = siteVO;
        SiteVO expectedResult = siteVO;
        instance.setSiteVo(siteVo);
        SiteVO result = instance.getSiteVo();
        assertEquals(expectedResult, result);
        System.out.println("setSiteVo Done");
    }

    /**
     * Test of getSiteContactVO method, of class EditSiteBean.
     */
    @Test
    public void test39_GetSiteContactVO() {
        System.out.println("getSiteContactVO");
        SiteContactVO siteContactVO = siteVO.getSiteContactList().get(0);
        SiteContactVO expResult = siteContactVO;
        instance.setSiteContactVO(siteContactVO);
        SiteContactVO result = instance.getSiteContactVO();
        assertEquals(expResult, result);
        System.out.println("getSiteContactVO Done");
    }

    /**
     * Test of setSiteContactVO method, of class EditSiteBean.
     */
    @Test
    public void test41_SetSiteContactVO() {
        System.out.println("setSiteContactVO");
        SiteContactVO siteContactVO = siteVO.getSiteContactList().get(0);
        SiteContactVO expectedResult = siteContactVO;
        instance.setSiteContactVO(siteContactVO);
        SiteContactVO result = instance.getSiteContactVO();
        assertEquals(expectedResult, result);
        System.out.println("setSiteContactVO Done");
        
    }

    /**
     * Test of getIndustries method, of class EditSiteBean.
     */
    @Test
    public void test43_GetIndustries() {
        System.out.println("getIndustries");
        assertEquals(instance.getIndustries().size(), getIndustryItemList().size());
        System.out.println("getIndustries Done");
    }
    
    /**
     * Test of getRoles method, of class EditSiteBean.
     */
    @Test
    public void test45_GetRoles() {
        System.out.println("getRoles");
        assertEquals(instance.getRoles().size(), getRoleItemList().size());
        System.out.println("getRoles Done");
    }

    /**
     * Test of getContactList method, of class EditSiteBean.
     */
    @Test
    public void test47_GetContactList() {
        System.out.println("getContactList");
        List<SiteContactVO> contactList1 = new ArrayList();
        SiteContactVO siteContactVO = siteVO.getSiteContactList().get(0);
        contactList1.add(siteContactVO);
        Whitebox.setInternalState(instance, "contactList", contactList1);
        List<SiteContactVO> expResult = contactList1;
        List<SiteContactVO> result = instance.getContactList();
        assertEquals(expResult, result);
        System.out.println("getContactList Done");
    }
}
