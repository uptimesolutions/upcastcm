package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.UserClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import com.uptime.upcastcm.utils.vo.UserPreferencesVO;
import com.uptime.upcastcm.utils.vo.UserSitesVO;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.junit.runners.MethodSorters;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import static org.powermock.api.mockito.PowerMockito.when;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;
import org.powermock.reflect.Whitebox;
import org.primefaces.PrimeFaces;

/**
 *
 * @author aswani
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(PowerMockRunner.class)
@PowerMockIgnore("jdk.internal.reflect.*")
@PowerMockRunnerDelegate(JUnit4.class)
@PrepareForTest({FacesContext.class, ExternalContext.class, ServiceRegistrarClient.class, NavigationBean.class, UserManageBean.class, PrimeFaces.class, PrimeFaces.Ajax.class, UserClient.class})
public class UserManageBeanTest {

    @Mock
    FacesContext facesContext;
    @Mock
    ExternalContext externalContext;
    @Mock
    HttpServletRequest request;
    @Mock
    HttpSession session;
    @Mock
    ServiceRegistrarClient serviceRegistrarClient;
    @Mock
    PrimeFaces pf;
    @Mock
    PrimeFaces.Ajax ax;
    @Mock
    NavigationBean navigationBean;
    @Mock
    UserManageBean userManageBean;
    @Mock
    UserClient userClient;

    private final List<UserSitesVO> userSiteList;
    UserManageBean instance;
    Map<String, Object> sessionMap;

    public UserManageBeanTest() {
        PowerMockito.mockStatic(FacesContext.class);
        PowerMockito.mockStatic(PrimeFaces.class);
        PowerMockito.mockStatic(PrimeFaces.Ajax.class);
        PowerMockito.mockStatic(ServiceRegistrarClient.class);
        PrimeFaces pf = PowerMockito.mock(PrimeFaces.class);
        PrimeFaces.Ajax ax = PowerMockito.mock(PrimeFaces.Ajax.class);
        when(PrimeFaces.current()).thenReturn(pf);
        when(pf.ajax()).thenReturn(ax);
        PowerMockito.mockStatic(UserClient.class);
        userClient = PowerMockito.mock(UserClient.class);
        facesContext = PowerMockito.mock(FacesContext.class);
        externalContext = PowerMockito.mock(ExternalContext.class);
        request = PowerMockito.mock(HttpServletRequest.class);
        session = PowerMockito.mock(HttpSession.class);
        navigationBean = PowerMockito.mock(NavigationBean.class);
        userManageBean = PowerMockito.mock(UserManageBean.class);
        serviceRegistrarClient = PowerMockito.mock(ServiceRegistrarClient.class);

        sessionMap = new HashMap();
        sessionMap.put("navigationBean", navigationBean);
        sessionMap.put("userManageBean", userManageBean);

        session.setAttribute("navigationBean", navigationBean);
        session.setAttribute("userManageBean", userManageBean);

        userSiteList = new ArrayList<>();
        UserSitesVO uvo = new UserSitesVO();
        uvo.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        uvo.setSiteName("Test Site 1");
        userSiteList.add(uvo);
        when(FacesContext.getCurrentInstance()).thenReturn(facesContext);
        when(facesContext.getExternalContext()).thenReturn(externalContext);
        when(externalContext.getSessionMap()).thenReturn(sessionMap);
        when(facesContext.getExternalContext().getRequest()).thenReturn(request);
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("navigationBean")).thenReturn(navigationBean);
        when(session.getAttribute("userManageBean")).thenReturn(userManageBean);
        when(userManageBean.getUserSiteList()).thenReturn(userSiteList);
        when(ServiceRegistrarClient.getInstance()).thenReturn(serviceRegistrarClient);
        when(UserClient.getInstance()).thenReturn(userClient);

        instance = new UserManageBean();
    }

    /**
     * Test of init method, of class UserManageBean.
     */
    @Test
    public void test00_Init() {
        System.out.println("init");
        instance.init();
        assertSame(null, instance.getUserName());
        System.out.println("init completed");
    }

    /**
     * Test of init method, of class UserManageBean.
     */
    @Test
    public void test01_Init() {
        System.out.println("init");

        when(session.getAttribute("accountNumber")).thenReturn("77777");
        when(session.getAttribute("displayName")).thenReturn("Sam Spade");
        when(session.getAttribute("company")).thenReturn("Uptime Solutions");
        when(session.getAttribute("nameId")).thenReturn("sam.spade@uptime-solutions.us");
        when(session.getAttribute("siteList")).thenReturn(userSiteList);
        instance.init();
        System.out.println("init completed");
    }

    /**
     * Test of submit method, of class UserManageBean.
     */
    @Test
    public void test11Submit() {
        System.out.println("submit");
        UserPreferencesVO accountUserPreferencesVO = new UserPreferencesVO();
        accountUserPreferencesVO.setLocaleDisplayName("English (United States)");
        accountUserPreferencesVO.setDefaultSite(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        accountUserPreferencesVO.setDefaultUnits("{\"AC Units\":[{\"acceleration_waveform\":\"Gs\",\"acceleration_spectrum\":\"m/s/s\",\"acceleration_amp_factor\":\"rms\",\"acceleration_log_y_axis\": false,\"velocity_waveform\":\"in/s\",\"velocity_spectrum\":\"Gs\",\"velocity_amp_factor\":\"rms\",\"velocity_log_y_axis\": false,\"displacement_waveform\":\"mils\",\"displacement_spectrum\":\"mils\",\"displacement_amp_factor\":\"rms\",\"displacement_log_y_axis\": false,\"ultrasonic_waveform\":\"dB SPL\",\"ultrasonic_spectrum\":\"dB SPL\",\"ultrasonic_amp_factor\":\"rms\",\"ultrasonic_log_y_axis\": true,\"time_unit\":\"s\",\"frequency_unit\":\"Hz\"}],\"DC Units\":[{\"temperature\":\"F\",\"frequency\":\"RPM\",\"pressure\":\"Pascals\",\"voltage\":\"V\",\"current\":\"A\",\"phase\":\"Degrees\",\"count\":\"Count\",\"relative_humidity\":\"%RH\",\"temperature_rtd\":\"F\"}]}");
        Whitebox.setInternalState(instance, "accountUserPreferencesVO", accountUserPreferencesVO);
        when(userClient.createUserPreferences(serviceRegistrarClient.getServiceHostURL(USER_PREFERENCE_SERVICE_NAME), accountUserPreferencesVO)).thenReturn(Boolean.TRUE);

        instance.submit();
        
         UserPreferencesVO expResult = accountUserPreferencesVO;
        UserPreferencesVO result = instance.getAccountUserPreferencesVO();
        assertEquals(expResult, result);
        System.out.println("Submit completed");
    }
    /**
     * Test of submit method, of class UserManageBean.
     */
    @Test
    public void test11SubmitFail() {
        System.out.println("submit");
        UserPreferencesVO accountUserPreferencesVO = new UserPreferencesVO();
        accountUserPreferencesVO.setLocaleDisplayName("English (United States)");
        accountUserPreferencesVO.setDefaultSite(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        accountUserPreferencesVO.setDefaultUnits("{\"AC Units\":[{\"acceleration_waveform\":\"Gs\",\"acceleration_spectrum\":\"m/s/s\",\"acceleration_amp_factor\":\"rms\",\"acceleration_log_y_axis\": false,\"velocity_waveform\":\"in/s\",\"velocity_spectrum\":\"Gs\",\"velocity_amp_factor\":\"rms\",\"velocity_log_y_axis\": false,\"displacement_waveform\":\"mils\",\"displacement_spectrum\":\"mils\",\"displacement_amp_factor\":\"rms\",\"displacement_log_y_axis\": false,\"ultrasonic_waveform\":\"dB SPL\",\"ultrasonic_spectrum\":\"dB SPL\",\"ultrasonic_amp_factor\":\"rms\",\"ultrasonic_log_y_axis\": true,\"time_unit\":\"s\",\"frequency_unit\":\"Hz\"}],\"DC Units\":[{\"temperature\":\"F\",\"frequency\":\"RPM\",\"pressure\":\"Pascals\",\"voltage\":\"V\",\"current\":\"A\",\"phase\":\"Degrees\",\"count\":\"Count\",\"relative_humidity\":\"%RH\",\"temperature_rtd\":\"F\"}]}");

        Whitebox.setInternalState(instance, "accountUserPreferencesVO", accountUserPreferencesVO);
        when(userClient.createUserPreferences(serviceRegistrarClient.getServiceHostURL(USER_PREFERENCE_SERVICE_NAME), accountUserPreferencesVO)).thenReturn(Boolean.FALSE);

        instance.submit();
        
        UserPreferencesVO expResult = accountUserPreferencesVO;
        UserPreferencesVO result = instance.getAccountUserPreferencesVO();
        assertEquals(expResult, result);
        
        System.out.println("SubmitFail completed");
    }

    /**
     * Test of getResourceBundleString method, of class UserManageBean.
     */
    @Test
    public void test03GetResourceBundleString() {
        System.out.println("getResourceBundleString");
        String property = "label_acknowledge";
        String expResult = "NULL";
        String result = instance.getResourceBundleString(property);
        assertEquals(expResult, result);
    }

    /**
     * Test of getTimezoneList method, of class UserManageBean.
     */
    @Test
    public void test04GetTimezoneList() {
        System.out.println("getTimezoneList");
        List<String> result = instance.getTimezoneList();
        assertTrue(result.isEmpty());
        System.out.println("getTimezoneList completed");
    }

    /**
     * Test of updateBean method, of class UserManageBean.
     */
    @Test
    public void test06UpdateBean() {
        System.out.println("updateBean");
        instance.updateBean();
        System.out.println("updateBean completed");
    }

    /**
     * Test of getTzOffsetString method, of class UserManageBean.
     */
    @Test
    public void test05GetTzOffsetString() {
        System.out.println("getTzOffsetString");
        Instant instant = Instant.parse("2023-03-07T11:19:42.12Z");
        int format = 2;
        ZoneId zoneId = ZoneOffset.UTC;
        Whitebox.setInternalState(instance, "zoneId", zoneId);
        String expResult = "2023-03-07T11:19:42+0000";
        String result = instance.getTzOffsetString(instant, format);
        assertEquals(expResult, result);
        System.out.println("getTzOffsetString completed");
    }

}
