/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.HwUnitToPointClient;
import com.uptime.upcastcm.http.client.ManageDeviceClient;
import com.uptime.upcastcm.http.client.PointClient;
import com.uptime.upcastcm.http.client.PointLocationClient;
import com.uptime.upcastcm.http.client.PointToHwUnitClient;
import com.uptime.upcastcm.http.client.SiteClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import com.uptime.upcastcm.utils.helperclass.Filters;
import com.uptime.upcastcm.utils.helperclass.PresetUtil;
import com.uptime.upcastcm.utils.vo.DeviceVO;
import com.uptime.upcastcm.utils.vo.HwUnitVO;
import com.uptime.upcastcm.utils.vo.PointLocationVO;
import com.uptime.upcastcm.utils.vo.PointVO;
import com.uptime.upcastcm.utils.vo.UserSitesVO;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import static org.powermock.api.mockito.PowerMockito.when;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.primefaces.PrimeFaces;

/**
 *
 * @author kpati
 */
@RunWith(PowerMockRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@PowerMockIgnore("jdk.internal.reflect.*")
@PrepareForTest({FacesContext.class, ExternalContext.class, UserManageBean.class, ServiceRegistrarClient.class, NavigationBean.class, SiteClient.class, PointLocationClient.class, PointClient.class, PointToHwUnitClient.class, ManageDeviceClient.class, HwUnitToPointClient.class, ConfigurationBean.class, PrimeFaces.class, PrimeFaces.Ajax.class})
public class InstallDeviceBeanTest {
    
    @Mock
    ServiceRegistrarClient serviceRegistrarClient;
    
    @Mock
    ConfigurationBean configurationBean;
    
    @Mock
    PointLocationClient pointLocationClient;
    
    @Mock
    PointClient pointClient;
    
    @Mock
    PointToHwUnitClient pointToHwUnitClient;
    
    @Mock
    HwUnitToPointClient hwUnitToPointClient;
    
    @Mock
    ManageDeviceClient manageDeviceClient;

    @Mock
    FacesContext facesContext;

    @Mock
    ExternalContext externalContext;

    @Mock
    UserManageBean userManageBean;

    @Mock
    HttpServletRequest request;
    
    @Mock
    HttpSession session;
    
    private final InstallDeviceBean instance;
    private final Filters filters;
    Map<String, Object> sessionMap;
    PrimeFaces pf;
    List<DeviceVO> deviceTypeList;
    List<PointVO> pointVOList;
    String serialNumber;
    DeviceVO installDevice;
    
    public InstallDeviceBeanTest() {
        UserSitesVO uvo;
        
        PrimeFaces.Ajax ax;
        PowerMockito.mockStatic(FacesContext.class);
        PowerMockito.mockStatic(ServiceRegistrarClient.class);
        PowerMockito.mockStatic(PointClient.class);
        PowerMockito.mockStatic(PointLocationClient.class);
        PowerMockito.mockStatic(PointToHwUnitClient.class);
        PowerMockito.mockStatic(HwUnitToPointClient.class);
        PowerMockito.mockStatic(ManageDeviceClient.class);
        PowerMockito.mockStatic(PrimeFaces.class);
        PowerMockito.mockStatic(PrimeFaces.Ajax.class);

        pf = PowerMockito.mock(PrimeFaces.class);
        facesContext = PowerMockito.mock(FacesContext.class);
        externalContext = PowerMockito.mock(ExternalContext.class);
        request = PowerMockito.mock(HttpServletRequest.class);
        session = PowerMockito.mock(HttpSession.class);
        userManageBean = PowerMockito.mock(UserManageBean.class);
        configurationBean = PowerMockito.mock(ConfigurationBean.class);
        serviceRegistrarClient = PowerMockito.mock(ServiceRegistrarClient.class);
        pointClient = PowerMockito.mock(PointClient.class);
        pointLocationClient = PowerMockito.mock(PointLocationClient.class);
        pointToHwUnitClient = PowerMockito.mock(PointToHwUnitClient.class);
        hwUnitToPointClient = PowerMockito.mock(HwUnitToPointClient.class);
        manageDeviceClient = PowerMockito.mock(ManageDeviceClient.class);

        sessionMap = new HashMap();
        sessionMap.put("userManageBean", userManageBean);
        sessionMap.put("configurationBean", configurationBean);
        session.setAttribute("userManageBean", userManageBean);
        session.setAttribute("configurationBean", configurationBean);
        

        uvo = new UserSitesVO();
        uvo.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        uvo.setSiteName("Test Site 1");
        
        UserSitesVO usvo = new UserSitesVO();
        usvo.setCustomerAccount("77777");
        usvo.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        usvo.setSiteName("Test Site 1");
        
        ax = PowerMockito.mock(PrimeFaces.Ajax.class);

        when(ServiceRegistrarClient.getInstance()).thenReturn(serviceRegistrarClient);
        when(PointClient.getInstance()).thenReturn(pointClient);
        when(PointLocationClient.getInstance()).thenReturn(pointLocationClient);
        when(PointToHwUnitClient.getInstance()).thenReturn(pointToHwUnitClient);
        when(HwUnitToPointClient.getInstance()).thenReturn(hwUnitToPointClient);
        when(ManageDeviceClient.getInstance()).thenReturn(manageDeviceClient);
        when(FacesContext.getCurrentInstance()).thenReturn(facesContext);
        when(facesContext.getExternalContext()).thenReturn(externalContext);
        when(externalContext.getSessionMap()).thenReturn(sessionMap);
        when(facesContext.getExternalContext().getRequest()).thenReturn(request);
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("userManageBean")).thenReturn(userManageBean);
        when(userManageBean.getCurrentSite()).thenReturn(uvo);
        when(userManageBean.getCurrentSite()).thenReturn(usvo);
        when(userManageBean.getPresetUtil()).thenReturn(new PresetUtil());

        when(PrimeFaces.current()).thenReturn(pf);
        when(pf.ajax()).thenReturn(ax);
        
        when(serviceRegistrarClient.getServiceHostURL(CONFIGURATION_SERVICE_NAME)).thenReturn(CONFIGURATION_SERVICE_NAME);

        filters = new Filters();
        filters.setSite(new SelectItem(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), "Test Site 1"));
        filters.setArea(new SelectItem(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"), "Test Area 1"));
        filters.setAsset(new SelectItem(UUID.fromString("f33fcf5a-5f93-4504-bc09-74a61a460bf0"), "Test Asset 1"));
        filters.setPointLocation(new SelectItem(UUID.fromString("f33fcf5a-5f93-4504-bc09-74a61a460bf0"), "Test Pointlocation 1"));

        instance = new InstallDeviceBean();
        installDevice = new DeviceVO();
        installDevice.setSiteId((UUID)filters.getSite().getValue());
        installDevice.setAreaId((UUID)filters.getArea().getValue());
        installDevice.setAssetId((UUID)filters.getAsset().getValue());
        installDevice.setPointLocationId((UUID)filters.getPointLocation().getValue());
        installDevice.setNewDeviceId("BB001788");

        Whitebox.setInternalState(instance, "installDevice", installDevice);
        deviceTypeList = new ArrayList();
        pointVOList = new ArrayList();

    }

    /**
     * Test of init method, of class InstallDeviceBean.
     */
    @Test
    public void test01_Init() {
        System.out.println("init");
        instance.setExistDeviceSerialNumber("BB0090001");
        instance.init();
        assertEquals(instance.getExistDeviceSerialNumber(), null);
        System.out.println("init Done");
    }
    
    /**
     * Test of init method, of class InstallDeviceBean.
     */
    @Test
    public void test02_Init() {
        System.out.println("init");
        instance.setExistDeviceSerialNumber("BB0090001");
        instance.init();
        assertNotEquals(instance.getExistDeviceSerialNumber(), "BB0090001");
        System.out.println("init Done");
    }

    /**
     * Test of resetPage method, of class InstallDeviceBean.
     */
    @Test
    public void test03_ResetPage() {
        System.out.println("resetPage");
        instance.resetPage();
        assertEquals(instance.getSelectedSiteDevicePresetList().size(), 0);
        System.out.println("resetPage Done");
    }

    /**
     * Test of presetFields method, of class InstallDeviceBean.
     */
    @Test
    public void test05_PresetFields_Filters_Object() {
        System.out.println("presetFields");
        Filters presets = filters;
        
        List<PointLocationVO> pointLocationVOList = new ArrayList();
        PointLocationVO pointLocationVO = new PointLocationVO();
        pointLocationVO.setCustomerAccount(installDevice.getCustomerAccount());
        pointLocationVO.setSiteId(installDevice.getSiteId());
        pointLocationVO.setAreaId(installDevice.getAreaId());
        pointLocationVO.setAssetId(installDevice.getAssetId());
        pointLocationVO.setPointLocationId(installDevice.getPointLocationId());
        pointLocationVO.setPointLocationName("Test PL Name");
        pointLocationVO.setBasestationPortNum((short)10);
        pointLocationVO.setDeviceSerialNumber("BB009001");
        pointLocationVOList.add(pointLocationVO);
        
        when(pointLocationClient.getPointLocationsByPK(CONFIGURATION_SERVICE_NAME, installDevice.getCustomerAccount(), installDevice.getSiteId(), installDevice.getAreaId(), installDevice.getAssetId(), installDevice.getPointLocationId())).thenReturn(pointLocationVOList);
        
        PointVO pointVO = new PointVO();
        pointVO.setCustomerAccount(installDevice.getCustomerAccount());
        pointVO.setSiteId(installDevice.getSiteId());
        pointVO.setAreaId(installDevice.getAreaId());
        pointVO.setAssetId(installDevice.getAssetId());
        pointVO.setPointLocationId(installDevice.getPointLocationId());
        pointVO.setPointId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        pointVO.setApSetId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        pointVO.setAlSetId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        pointVO.setPointType("AC");
        pointVO.setSensorChannelNum(0);
        pointVO.setPointName("Test Point Name1");
        pointVOList.add(pointVO);
        
        when(pointClient.getPointsByCustomerSiteAreaAssetPointLocation(CONFIGURATION_SERVICE_NAME, installDevice.getCustomerAccount(), installDevice.getSiteId(), installDevice.getAreaId(), installDevice.getAssetId(), installDevice.getPointLocationId(), true)).thenReturn(pointVOList);
        
        instance.presetFields(presets, pointLocationVO);
    }


    /**
     * Test of getPointVOList method, of class InstallDeviceBean.
     */
    @Test
    public void test07_GetPointVOList() {
        System.out.println("getPointList");
        assertEquals(instance.getPointVOList().size(), 0);
    }

    /**
     * Test of validateSerialNum method, of class InstallDeviceBean.
     */
    @Test
    public void test11_ValidateSerialNum() {
        System.out.println("validateSerialNum");
        List<HwUnitVO> list ;
        
        instance.setSerialNumberInput("BB009001");
        
        list = new ArrayList();
        HwUnitVO hwUnitVO = new HwUnitVO();
        hwUnitVO.setExistingDeviceId(instance.getSerialNumberInput());
        list.add(hwUnitVO);
        
        when(hwUnitToPointClient.getHwUnitToPointByDevice(CONFIGURATION_SERVICE_NAME, serialNumber)).thenReturn(list);
        instance.validateSerialNum();
        
        assertEquals(instance.isDisplayPoint(), false);
    }

    
    /**
     * Test of deletePointList method, of class InstallDeviceBean.
     */
    @Test
    public void test13_DeletePointList() {
        System.out.println("deletePointList");
        instance.deletePointList();
        System.out.println("deletePointList Done");
    }
    

    /**
     * Test of submit method, of class InstallDeviceBean.
     */
    @Test
    public void test15_Submit() {
        System.out.println("submit");
        when(manageDeviceClient.installDevice(CONFIGURATION_SERVICE_NAME, installDevice)).thenReturn(true);
        instance.setSerialNumberInput("BB009004");
        instance.onSerialNumberChange();
        instance.submit();
        assertEquals(instance.getPointVOList().size(), 0);
        assertEquals(instance.isDisplayPoint(), false);
        System.out.println("submit Done");
    }
}
