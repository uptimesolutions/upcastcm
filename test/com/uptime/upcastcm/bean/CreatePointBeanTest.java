/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.APALSetClient;
import com.uptime.upcastcm.http.client.AreaClient;
import com.uptime.upcastcm.http.client.AssetClient;
import com.uptime.upcastcm.http.client.PointClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.CONFIGURATION_SERVICE_NAME;
import static com.uptime.upcastcm.utils.ApplicationConstants.PRESET_SERVICE_NAME;
import static com.uptime.upcastcm.utils.enums.ACSensorTypeEnum.getACSensorTypeItemList;
import static com.uptime.upcastcm.utils.enums.DCSensorTypeEnum.getDCSensorTypeItemList;
import static com.uptime.upcastcm.utils.enums.DwellEnum.getDwellItemList;
import static com.uptime.upcastcm.utils.enums.FmaxEnum.getFmaxItemList;
import static com.uptime.upcastcm.utils.enums.ResolutionEnum.getResolutionItemList;
import com.uptime.upcastcm.utils.helperclass.Filters;
import com.uptime.upcastcm.utils.helperclass.PresetUtil;
import com.uptime.upcastcm.utils.vo.ApAlSetVO;
import com.uptime.upcastcm.utils.vo.DevicePresetVO;
import com.uptime.upcastcm.utils.vo.PointVO;
import com.uptime.upcastcm.utils.vo.UserSitesVO;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.junit.runners.MethodSorters;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import static org.powermock.api.mockito.PowerMockito.when;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;
import org.powermock.reflect.Whitebox;
import org.primefaces.PrimeFaces;

/**
 *
 * @author madhavi
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(PowerMockRunner.class)
@PowerMockIgnore("jdk.internal.reflect.*")
@PowerMockRunnerDelegate(JUnit4.class)
@PrepareForTest({FacesContext.class, ExternalContext.class, HttpServletRequest.class, HttpSession.class, UserManageBean.class, ConfigurationBean.class, AreaClient.class, ServiceRegistrarClient.class, AssetClient.class, PointClient.class, PresetUtil.class, APALSetClient.class, PrimeFaces.class, PrimeFaces.Ajax.class})
public class CreatePointBeanTest {

    @Mock
    ServiceRegistrarClient sc;

    @Mock
    AssetClient assetClient;

    @Mock
    AreaClient areaClient;

    @Mock
    PointClient pointClient;

    @Mock
    APALSetClient apALSetClient;

    @Mock
    FacesContext facesContext;

    @Mock
    ExternalContext externalContext;

    @Mock
    UserManageBean userManageBean;

    @Mock
    ConfigurationBean configurationBean;
    @Mock
    HttpServletRequest request;

    @Mock
    HttpSession session;

    @Mock
    PresetUtil presetUtil;

    Map<String, Object> sessionMap;
    PointVO pointVO;
    List<String> siteNames, assetNameList, sensorTypeList, pointTypeList, apSetList, alSetList;
    String customerAccount;
    List<UserSitesVO> userSiteList;
    CreatePointBean instance;
    private final Filters filters;
    ApAlSetVO apAlSetVO;
    PrimeFaces pf;
    PrimeFaces.Ajax ax;

    public CreatePointBeanTest() {
        PowerMockito.mockStatic(FacesContext.class);
        PowerMockito.mockStatic(ServiceRegistrarClient.class);
        PowerMockito.mockStatic(AreaClient.class);
        PowerMockito.mockStatic(PointClient.class);
        PowerMockito.mockStatic(APALSetClient.class);
        PowerMockito.mockStatic(ConfigurationBean.class);

        facesContext = PowerMockito.mock(FacesContext.class);
        externalContext = PowerMockito.mock(ExternalContext.class);
        userManageBean = PowerMockito.mock(UserManageBean.class);
        configurationBean = PowerMockito.mock(ConfigurationBean.class);
        request = PowerMockito.mock(HttpServletRequest.class);
        presetUtil = PowerMockito.mock(PresetUtil.class);
        pointClient = PowerMockito.mock(PointClient.class);
        apALSetClient = PowerMockito.mock(APALSetClient.class);
        sc = PowerMockito.mock(ServiceRegistrarClient.class);
        areaClient = PowerMockito.mock(AreaClient.class);
        session = PowerMockito.mock(HttpSession.class);

        when(ServiceRegistrarClient.getInstance()).thenReturn(sc);
        when(AreaClient.getInstance()).thenReturn(areaClient);
        when(PointClient.getInstance()).thenReturn(pointClient);
        when(APALSetClient.getInstance()).thenReturn(apALSetClient);
        when(FacesContext.getCurrentInstance()).thenReturn(facesContext);

        when(facesContext.getExternalContext()).thenReturn(externalContext);
        when(externalContext.getRequest()).thenReturn(request);
        sessionMap = new HashMap();
        sessionMap.put("userManageBean", userManageBean);
        sessionMap.put("configurationBean", configurationBean);
        when(externalContext.getSessionMap()).thenReturn(sessionMap);

        when(request.getSession()).thenReturn(session);
        when(userManageBean.getPresetUtil()).thenReturn(presetUtil);

        customerAccount = "7777";

        UserSitesVO usvo = new UserSitesVO();
        usvo.setCustomerAccount(customerAccount);
        when(userManageBean.getCurrentSite()).thenReturn(usvo);

        PowerMockito.mockStatic(PrimeFaces.class);
        PowerMockito.mockStatic(PrimeFaces.Ajax.class);
        pf = PowerMockito.mock(PrimeFaces.class);
        ax = PowerMockito.mock(PrimeFaces.Ajax.class);
        when(PrimeFaces.current()).thenReturn(pf);
        when(pf.ajax()).thenReturn(ax);

        instance = new CreatePointBean();

        filters = new Filters();

        filters.setCountry("Test Country 1");
        filters.setRegion("Test Region 1");
        filters.setSite(new SelectItem(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"), "Test site 1"));
        filters.setArea(new SelectItem(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"), "Test Area 1"));
        filters.setAsset(new SelectItem(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"), "Test Asset 1"));
        filters.setPointLocation(new SelectItem(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"), "Test Point Location 1"));
        filters.setPoint(new SelectItem(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"), "Test point 1"));
        when(userManageBean.getSelectedFilters()).thenReturn(filters);
        when(configurationBean.getSelectedFilters()).thenReturn(filters);
        userSiteList = new ArrayList<>();
        UserSitesVO uvo = new UserSitesVO();
        uvo.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        uvo.setSiteName("Test Site 1");
        userSiteList.add(uvo);
        uvo = new UserSitesVO();
        uvo.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf1"));
        uvo.setSiteName("Test Site 2");
        userSiteList.add(uvo);

        pointVO = new PointVO();
        pointVO.setCustomerAccount(customerAccount);
        pointVO.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        pointVO.setSiteName("Test Site 1");
        pointVO.setAreaId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf1"));
        pointVO.setAreaName("Test Area 1");
        pointVO.setSensorType("Acceleration");
        pointVO.setAlSetId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        pointVO.setAlSetName("Test Al Set Name");
        pointVO.setApSetId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8242"));
        pointVO.setApSetName("Test Ap Set Name");

        apAlSetVO = new ApAlSetVO();
        apAlSetVO.setAlSetId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        apAlSetVO.setAlSetName("Test Al Set Name");
        apAlSetVO.setApSetId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8242"));
        apAlSetVO.setApSetName("Test Ap Set Name");

    }

    /**
     * Test of init method, of class CreatePointBean.
     */
    @Test
    public void test1_Init() {
        System.out.println("init");
        instance.init();
        assertEquals(null, instance.getPointVO().getCustomerAccount());
        System.out.println("init completed");
    }

    /**
     * Test of resetPage method, of class CreatePointBean.
     */
    @Test
    public void test2_ResetPage() {
        System.out.println("resetPage");
        instance.resetPage();
        assertEquals(null, instance.getPointVO().getCustomerAccount());
        System.out.println("resetPage completed");
    }

    /**
     * Test of presetFields method, of class CreatePointBean.
     */
    @Test
    public void test3_PresetFields_Filters_Object() {
        System.out.println("presetFields");
        instance.resetPage();
        instance.presetFields(filters, null);
        System.out.println("presetFields completed");
    }

    /**
     * Test of updateBean method, of class CreatePointBean.
     */
    @Test
    public void test6_UpdateBean() {
        System.out.println("updateBean");
        instance.updateBean();
        System.out.println("updateBean completed");
    }

    /**
     * Test of onSwitchChange method, of class CreatePointBean.
     */
    @Test
    public void test7_OnSwitchChange() {
        System.out.println("onSwitchChange");
        instance.resetPage();
        instance.onSwitchChange();
        System.out.println("onSwitchChange completed");
    }

    /**
     * Test of onDevicePresetSelect method, of class CreatePointBean.
     */
    @Test
    public void test8_OnDevicePresetSelect() {
        System.out.println("onDevicePresetSelect");
        UUID devicePresetId = UUID.randomUUID();
        instance.setSelectedPresetId(devicePresetId);
        List<DevicePresetVO> devicePresetsList = new ArrayList();
        DevicePresetVO dpvo = new DevicePresetVO();
        dpvo.setPresetId(devicePresetId);
        dpvo.setCustomerAccount(pointVO.getCustomerAccount());
        dpvo.setSiteId(pointVO.getSiteId());
        dpvo.setAlSetId(UUID.randomUUID());
        dpvo.setAlSetName("Al Test 1");
        dpvo.setAlarmEnabled(false);
        dpvo.setApSetId(UUID.randomUUID());
        dpvo.setApSetName("AP Test 1");
        dpvo.setAreaId(pointVO.getAreaId());
        dpvo.setAssetId(pointVO.getAssetId());
        dpvo.setPointLocationId(pointVO.getPointLocationId());
        dpvo.setDisabled(false);
        dpvo.setPointName("Point 1");
        dpvo.setPointType("Point TYpe 1 ");
        dpvo.setSensorChannelNum(2);
        dpvo.setSensorOffset(2.0f);
        dpvo.setSensorSensitivity(4.0f);
        dpvo.setSensorType("AC");
        dpvo.setSensorUnits("m/s");
        dpvo.setAlSetVOList(new ArrayList());
        devicePresetsList.add(dpvo);
        Whitebox.setInternalState(instance, "devicePresetsList", devicePresetsList);
        Whitebox.setInternalState(instance, "pointVO", pointVO);
        when(apALSetClient.getSiteApAlSetsByCustomerSiteIdApSetIdAlSetId(sc.getServiceHostURL(PRESET_SERVICE_NAME), dpvo.getCustomerAccount(), dpvo.getSiteId(), dpvo.getApSetId(), dpvo.getAlSetId())).thenReturn(new ArrayList());
        when(apALSetClient.getGlobalApAlSetsByCustomerApSetIdAlSetId(sc.getServiceHostURL(PRESET_SERVICE_NAME), dpvo.getCustomerAccount(), dpvo.getApSetId(), dpvo.getAlSetId())).thenReturn(new ArrayList());

        instance.onDevicePresetSelect();
        assertTrue(instance.getSelectedSiteDevicePresetList().isEmpty());
        System.out.println("onDevicePresetSelect completed");
    }

    /**
     * Test of onSensorSelect method, of class CreatePointBean.
     */
    @Test
    public void test9_OnSensorSelect() {
        System.out.println("onSensorSelect");
        Whitebox.setInternalState(instance, "pointVO", pointVO);
        instance.onSensorSelect();
        System.out.println("onSensorSelect completed");
    }

    /**
     * Test of createPoint method, of class CreatePointBean.
     */
    @Test
    public void test10_CreatePoint() {
        System.out.println("createPoint");
        instance.resetPage();
        instance.setFromTemplate(false);
        Whitebox.setInternalState(instance, "pointVO", pointVO);
        instance.createPoint();
        System.out.println("createPoint completed");
    }

    /**
     * Test of createPoint method, of class CreatePointBean.
     */
    @Test
    public void test11_CreatePoint() {
        System.out.println("createPoint");
        instance.resetPage();
        Whitebox.setInternalState(instance, "pointVO", pointVO);
        List<DevicePresetVO> selectedSiteDevicePresetList = new ArrayList();
        DevicePresetVO dpvo = new DevicePresetVO();
        dpvo.setCustomerAccount(pointVO.getCustomerAccount());
        dpvo.setSiteId(pointVO.getSiteId());
        dpvo.setAlSetId(UUID.randomUUID());
        dpvo.setAlSetName("Al Test 1");
        dpvo.setAlarmEnabled(false);
        dpvo.setApSetId(UUID.randomUUID());
        dpvo.setApSetName("AP Test 1");
        dpvo.setAreaId(pointVO.getAreaId());
        dpvo.setAssetId(pointVO.getAssetId());
        dpvo.setPointLocationId(pointVO.getPointLocationId());
        dpvo.setDisabled(false);
        dpvo.setPointName("Point 1");
        dpvo.setPointType("Point TYpe 1 ");
        dpvo.setSensorChannelNum(2);
        dpvo.setSensorOffset(2.0f);
        dpvo.setSensorSensitivity(4.0f);
        dpvo.setSensorType("AC");
        dpvo.setSensorUnits("m/s");
        dpvo.setAlSetVOList(new ArrayList());
        selectedSiteDevicePresetList.add(dpvo);
        Whitebox.setInternalState(instance, "selectedSiteDevicePresetList", selectedSiteDevicePresetList);
        List<PointVO> pointVOs = new ArrayList();
        selectedSiteDevicePresetList.stream().forEachOrdered(dpVO -> {
            PointVO pvo = new PointVO();
            pvo.setCustomerAccount(pointVO.getCustomerAccount());
            pvo.setSiteId(pointVO.getSiteId());
            pvo.setAlSetId(dpVO.getAlSetId());
            pvo.setAlSetName(dpVO.getAlSetName());
            pvo.setAlarmEnabled(dpVO.isAlarmEnabled());
            pvo.setApSetId(dpVO.getApSetId());
            pvo.setApSetName(dpVO.getApSetName());
            pvo.setAreaId(pointVO.getAreaId());
            pvo.setAssetId(pointVO.getAssetId());
            pvo.setPointLocationId(pointVO.getPointLocationId());
            pvo.setDisabled(dpVO.isDisabled());
            pvo.setPointName(dpVO.getPointName());
            pvo.setPointType(dpVO.getChannelType());
            pvo.setSensorChannelNum(dpVO.getChannelNumber());
            pvo.setSensorOffset(dpVO.getSensorOffset());
            pvo.setSensorSensitivity(dpVO.getSensorSensitivity());
            pvo.setSensorType(dpVO.getSensorType());
            pvo.setSensorUnits(dpVO.getSensorUnits());
            pvo.setApAlSetVOs(dpVO.getAlSetVOList());
            pointVOs.add(pvo);
        });
        when(pointClient.createPoint(sc.getServiceHostURL(CONFIGURATION_SERVICE_NAME), pointVOs)).thenReturn(Boolean.TRUE);
        instance.createPoint();
        assertNull(instance.getPointVO().getPointName());
        System.out.println("createPoint completed");
    }

    /**
     * Test of onApSetNameSelect method, of class CreatePointBean.
     */
    @Test
    public void test12_OnApSetNameSelect() {
        System.out.println("onApSetNameSelect");
        Whitebox.setInternalState(instance, "pointVO", pointVO);
        instance.onApSetNameSelect();
        System.out.println("onApSetNameSelect completed");
    }

    /**
     * Test of onAlSetNameSelect method, of class CreatePointBean.
     */
    @Test
    public void test13_OnAlSetNameSelect() {
        System.out.println("onAlSetNameSelect");
        instance.onAlSetNameSelect();
        System.out.println("onAlSetNameSelect completed");
    }

    /**
     * Test of onLowAlertActiveChange method, of class CreatePointBean.
     */
    @Test
    public void test15_alarmLimitValidation() {
        System.out.println("onLowAlertActiveChange");
        instance.alarmLimitValidation(apAlSetVO, 1);
        System.out.println("onLowAlertActiveChange completed");
    }

    /**
     * Test of getDcSensorTypeList method, of class CreatePointBean.
     */
    @Test
    public void test24_GetDcSensorTypeList() {
        System.out.println("getDcSensorTypeList");
        assertEquals(instance.getDcSensorTypeList().size(), getDCSensorTypeItemList().size());
        System.out.println("getDcSensorTypeList completed");
    }

    /**
     * Test of getAcSensorTypeList method, of class CreatePointBean.
     */
    @Test
    public void test25_GetAcSensorTypeList() {
        System.out.println("getAcSensorTypeList");
        assertEquals(instance.getAcSensorTypeList().size(), getACSensorTypeItemList().size());
        System.out.println("getAcSensorTypeList completed");
    }

    /**
     * Test of getFmaxList method, of class CreatePointBean.
     */
    @Test
    public void test26_GetFmaxList() {
        System.out.println("getFmaxList");
        assertEquals(instance.getFmaxList().size(), getFmaxItemList().size());
        System.out.println("getFmaxList completed");
    }

    /**
     * Test of getDwellList method, of class CreatePointBean.
     */
    @Test
    public void test27_GetDwellList() {
        System.out.println("getDwellList");
        assertEquals(instance.getDwellList().size(), getDwellItemList().size());
        System.out.println("getDwellList completed");
    }

    /**
     * Test of getResolutionList method, of class CreatePointBean.
     */
    @Test
    public void test28_GetResolutionList() {
        System.out.println("getResolutionList");
        assertEquals(instance.getResolutionList().size(), getResolutionItemList().size());
        System.out.println("getResolutionList completed");
    }

    /**
     * Test of onCustomLimitsAlSetNameSelect method, of class CreatePointBean.
     */
    @Test
    public void test29_OnCustomLimitsAlSetNameSelect() {
        System.out.println("onCustomLimitsAlSetNameSelect");
        instance.onCustomLimitsAlSetNameSelect();
        System.out.println("onCustomLimitsAlSetNameSelect completed");
    }

    /**
     * Test of removeApAlset method, of class CreatePointBean.
     */
    @Test
    public void test30_RemoveApAlset() {
        System.out.println("removeApAlset");
        Whitebox.setInternalState(instance, "pointVO", pointVO);
        instance.removeApAlset(apAlSetVO);
        System.out.println("removeApAlset completed");
    }

}
