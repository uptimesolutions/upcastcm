/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.AssetPresetClient;
import com.uptime.upcastcm.http.client.DevicePresetClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.DEFAULT_UPTIME_ACCOUNT;
import static com.uptime.upcastcm.utils.ApplicationConstants.PRESET_SERVICE_NAME;
import com.uptime.upcastcm.utils.helperclass.PresetUtil;
import com.uptime.upcastcm.utils.vo.AssetPresetPointLocationVO;
import com.uptime.upcastcm.utils.vo.AssetPresetVO;
import com.uptime.upcastcm.utils.vo.UserSitesVO;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import static org.powermock.api.mockito.PowerMockito.when;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.primefaces.PrimeFaces;

/**
 *
 * @author joseph
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(PowerMockRunner.class)
@PowerMockIgnore("jdk.internal.reflect.*")
@PrepareForTest({FacesContext.class, ExternalContext.class, ServiceRegistrarClient.class, UserManageBean.class, AssetPresetClient.class, DevicePresetClient.class, PresetUtil.class, PresetsBean.class, PrimeFaces.class, PrimeFaces.Ajax.class})
public class CreatePresetsAssetBeanTest {

    @Mock
    ServiceRegistrarClient serviceRegistrarClient;

    @Mock
    FacesContext facesContext;

    @Mock
    ExternalContext externalContext;

    @Mock
    UserManageBean userManageBean;

    @Mock
    AssetPresetClient assetPresetClient;

    @Mock
    DevicePresetClient devicePresetClient;

    @Mock
    HttpServletRequest request;

    @Mock
    HttpSession session;

    @Mock
    PresetUtil presetUtil;

    @Mock
    PresetsBean presetsBean;

    @Mock
    PrimeFaces pf;

    @Mock
    PrimeFaces.Ajax ax;

    @Mock
    NavigationBean navigationBean;

    Map<String, Object> sessionMap;
    AssetPresetVO assetPresetVO;
    private final CreatePresetsAssetBean instance;
    UUID siteId;

    public CreatePresetsAssetBeanTest() {
        UserSitesVO uvo;

        PowerMockito.mockStatic(FacesContext.class);
        PowerMockito.mockStatic(DevicePresetClient.class);
        PowerMockito.mockStatic(ServiceRegistrarClient.class);
        PowerMockito.mockStatic(AssetPresetClient.class);

        facesContext = PowerMockito.mock(FacesContext.class);
        serviceRegistrarClient = PowerMockito.mock(ServiceRegistrarClient.class);
        assetPresetClient = PowerMockito.mock(AssetPresetClient.class);
        devicePresetClient = PowerMockito.mock(DevicePresetClient.class);
        externalContext = PowerMockito.mock(ExternalContext.class);
        request = PowerMockito.mock(HttpServletRequest.class);
        session = PowerMockito.mock(HttpSession.class);
        userManageBean = PowerMockito.mock(UserManageBean.class);
        presetUtil = PowerMockito.mock(PresetUtil.class);
        presetsBean = PowerMockito.mock(PresetsBean.class);
        navigationBean = PowerMockito.mock(NavigationBean.class);
        PowerMockito.mockStatic(PrimeFaces.class);
        PowerMockito.mockStatic(PrimeFaces.Ajax.class);
        pf = PowerMockito.mock(PrimeFaces.class);
        ax = PowerMockito.mock(PrimeFaces.Ajax.class);
        sessionMap = new HashMap();
        sessionMap.put("userManageBean", userManageBean);
        sessionMap.put("navigationBean", navigationBean);
        sessionMap.put("presetsBean", presetsBean);
        session.setAttribute("userManageBean", userManageBean);
        session.setAttribute("navigationBean", navigationBean);

        uvo = new UserSitesVO();
        uvo.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        uvo.setSiteName("Test Site 1");

        siteId = UUID.randomUUID();
        assetPresetVO = new AssetPresetVO();
        assetPresetVO.setType("site");
        assetPresetVO.setAssetPresetId(UUID.randomUUID());
        assetPresetVO.setAssetPresetName("Asset Preset 1");
        assetPresetVO.setAssetTypeId(UUID.randomUUID());
        assetPresetVO.setAssetTypeName("Asset Type name 1");
        assetPresetVO.setCustomerAccount("77777");
        assetPresetVO.setDescription("Test Description");
        assetPresetVO.setDevicePresetId(UUID.randomUUID());
        assetPresetVO.setDevicePresetName("Device Preset Name 1");
        assetPresetVO.setDevicePresetType("MistLX");
        assetPresetVO.setPointLocationName("Point Location 1");
        assetPresetVO.setSampleInterval(720);
        assetPresetVO.setSiteId(siteId);
        assetPresetVO.setSiteName("Test Site 1");
        assetPresetVO.setSpeedRatio(2.0f);

//        devicePresetVO.setApSetId(UUID.fromString("5ee8fbf1-6d1e-4ac7-afa8-e1db3d8920c7"));
//        ApAlSetVO apalVo = new ApAlSetVO();
//        apalVo.setApSetId(UUID.fromString("5ee8fbf1-6d1e-4ac7-afa8-e1db3d8920c7"));
//        apalVOs = new ArrayList();
//        apalVOs.add(apalVo);
//        devicePresetVO.setApAlSetVOList(apalVOs);
//        devicePresetVO.setSensorType("Test Sensor");
        when(FacesContext.getCurrentInstance()).thenReturn(facesContext);
        when(ServiceRegistrarClient.getInstance()).thenReturn(serviceRegistrarClient);
        when(AssetPresetClient.getInstance()).thenReturn(assetPresetClient);
        when(DevicePresetClient.getInstance()).thenReturn(devicePresetClient);
        when(facesContext.getExternalContext()).thenReturn(externalContext);
        when(externalContext.getSessionMap()).thenReturn(sessionMap);
        when(facesContext.getExternalContext().getRequest()).thenReturn(request);
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("userManageBean")).thenReturn(userManageBean);
        when(userManageBean.getPresetUtil()).thenReturn(presetUtil);
        when(session.getAttribute("navigationBean")).thenReturn(navigationBean);
        // when(presetUtil.getGlobalApAlSetsByCustomerSensorType("77777", "Test Sensor")).thenReturn(apalVOs);
        when(userManageBean.getCurrentSite()).thenReturn(uvo);
        UserSitesVO usvo = new UserSitesVO();
        usvo.setCustomerAccount("77777");
        usvo.setSiteId(siteId);
        when(userManageBean.getCurrentSite()).thenReturn(usvo);

        instance = new CreatePresetsAssetBean();

    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of init method, of class CreatePresetsAssetBean.
     */
    @Test
    public void test1_Init() {
        System.out.println("init");
        instance.setSiteOrGlobal("site");
        instance.init();
        assertEquals(instance.getSiteOrGlobal(), null);
        System.out.println("init completed");
    }

    /**
     * Test of resetPage method, of class CreatePresetsAssetBean.
     */
    @Test
    public void test2_ResetPage() {
        System.out.println("resetPage");
        instance.setSiteOrGlobal("site");
        instance.resetPage();
        assertEquals(instance.getSiteOrGlobal(), null);
        System.out.println("resetPage completed");
    }

    /**
     * Test of openPointLocationOverlay method, of class CreatePresetsAssetBean.
     */
    @Test
    public void test3_OpenPointLocationOverlay() {
        System.out.println("openPointLocationOverlay");
       
        List<AssetPresetPointLocationVO> list = new ArrayList();
        assetPresetVO.setAssetPresetPointLocationVOList(list);
        instance.setSiteOrGlobal("site");
        Whitebox.setInternalState(instance, "assetPresetVO", assetPresetVO);
        assertEquals(sessionMap.size(), 3);
        instance.openPointLocationOverlay();
        assertEquals(sessionMap.size(), 4);
        System.out.println("openPointLocationOverlay completed");
    }

    /**
     * Test of setPointLocationDialog method, of class CreatePresetsAssetBean.
     */
    @Test
    public void test4_SetPointLocationDialog() {
        System.out.println("setPointLocationDialog");
        AssetPresetPointLocationVO assetPresetPointLocationVO = new AssetPresetPointLocationVO();
        assetPresetPointLocationVO.setCustomerAccount("77777");
        assetPresetPointLocationVO.setPointLocationName("Test Point Location1");
        List<AssetPresetPointLocationVO> list = new ArrayList();
        assetPresetVO.setAssetPresetPointLocationVOList(list);
        instance.setSiteOrGlobal("site");
        Whitebox.setInternalState(instance, "assetPresetVO", assetPresetVO);
        when(devicePresetClient.getGlobalDevicePresetsByCustomerDeviceType(serviceRegistrarClient.getServiceHostURL(PRESET_SERVICE_NAME), DEFAULT_UPTIME_ACCOUNT, "MistLX")).thenReturn(new ArrayList());
        when(devicePresetClient.getGlobalDevicePresetsByCustomerDeviceType(serviceRegistrarClient.getServiceHostURL(PRESET_SERVICE_NAME), "77777", "MistLX")).thenReturn(new ArrayList());
        when(devicePresetClient.getSiteDeviceByCustomerSiteDeviceType(serviceRegistrarClient.getServiceHostURL(PRESET_SERVICE_NAME), "77777", siteId.toString(), "MistLX")).thenReturn(new ArrayList());

        int operationType = 2;
        assertEquals(sessionMap.size(), 3);
        instance.setPointLocationDialog(assetPresetPointLocationVO, operationType);
        assertEquals(sessionMap.size(), 4);
        System.out.println("setPointLocationDialog completed");
    }

    /**
     * Test of delete method, of class CreatePresetsAssetBean.
     */
    @Test
    public void test5_Delete() {
        System.out.println("delete");
        when(PrimeFaces.current()).thenReturn(pf);
        when(pf.ajax()).thenReturn(ax);
        ax.addCallbackParam("createAssetsFormId", new Object());
        ax.addCallbackParam("copyEditPresetsAssetFormId", new Object());
        AssetPresetPointLocationVO pointLocationNamesVO = new AssetPresetPointLocationVO();
        pointLocationNamesVO.setCustomerAccount("77777");
        pointLocationNamesVO.setPointLocationName("Test Point Location1");
      
        assetPresetVO.setAssetPresetPointLocationVOList(new ArrayList());
        instance.setSiteOrGlobal("site");
        Whitebox.setInternalState(instance, "assetPresetVO", assetPresetVO);
        String tab = "pointLocationNames";
        instance.delete(pointLocationNamesVO, tab);
        assertEquals(instance.getAssetPresetVO().getAssetPresetPointLocationVOList().size(), 0);
        System.out.println("delete completed");
    }

    /**
     * Test of submit method, of class CreatePresetsAssetBean.
     */
    @Test
    public void test6_Submit() {
        System.out.println("site submit");
        when(PrimeFaces.current()).thenReturn(pf);
        when(pf.ajax()).thenReturn(ax);
        ax.addCallbackParam("presetsTabViewId:assetsFormId", new Object());
        AssetPresetPointLocationVO pointLocationNamesVO = new AssetPresetPointLocationVO();
        pointLocationNamesVO.setCustomerAccount("77777");
        pointLocationNamesVO.setPointLocationName("Test Point Location1");
        List<AssetPresetPointLocationVO> list = new ArrayList();
        list.add(pointLocationNamesVO);
        assetPresetVO.setAssetPresetPointLocationVOList(list);
        instance.setSiteOrGlobal("site");
        Whitebox.setInternalState(instance, "assetPresetVO", assetPresetVO);
        instance.submit();
        System.out.println("site submit completed");
    }

    /**
     * Test of submit method, of class CreatePresetsAssetBean.
     */
    @Test
    public void test7_Submit() {
        System.out.println("global submit");
        when(PrimeFaces.current()).thenReturn(pf);
        when(pf.ajax()).thenReturn(ax);
        ax.addCallbackParam("presetsTabViewId:assetsFormId", new Object());
        // ax.addCallbackParam("copyEditPresetsAssetFormId", new Object());
        AssetPresetPointLocationVO pointLocationNamesVO = new AssetPresetPointLocationVO();
        pointLocationNamesVO.setCustomerAccount("77777");
        pointLocationNamesVO.setPointLocationName("Test Point Location1");
        List<AssetPresetPointLocationVO> list = new ArrayList();
        list.add(pointLocationNamesVO);
        assetPresetVO.setAssetPresetPointLocationVOList(list);
        instance.setSiteOrGlobal("global");
        Whitebox.setInternalState(instance, "assetPresetVO", assetPresetVO);
        instance.submit();
        System.out.println("global submit completed");
    }

}
