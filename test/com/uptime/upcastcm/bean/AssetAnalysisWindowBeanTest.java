/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.bean;

import com.sun.faces.context.RequestParameterMap;
import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.ApALByPointClient;
import com.uptime.upcastcm.http.client.AreaClient;
import com.uptime.upcastcm.http.client.FaultFrequenciesClient;
import com.uptime.upcastcm.http.client.PointClient;
import com.uptime.upcastcm.http.client.PointLocationClient;
import com.uptime.upcastcm.http.client.SiteClient;
import com.uptime.upcastcm.http.client.TrendDataClient;
import com.uptime.upcastcm.http.client.WorkOrderClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import com.uptime.upcastcm.utils.AudioUtil;
import com.uptime.upcastcm.utils.helperclass.DefaultUnits;
import com.uptime.upcastcm.utils.helperclass.Filters;
import com.uptime.upcastcm.utils.vo.ApAlSetVO;
import com.uptime.upcastcm.utils.vo.AreaVO;
import com.uptime.upcastcm.utils.vo.ChartInfoVO;
import com.uptime.upcastcm.utils.vo.FaultFrequenciesVO;
import com.uptime.upcastcm.utils.vo.GraphDataVO;
import com.uptime.upcastcm.utils.vo.PointLocationVO;
import com.uptime.upcastcm.utils.vo.PointVO;
import com.uptime.upcastcm.utils.vo.SiteVO;
import com.uptime.upcastcm.utils.vo.TrendDetailsVO;
import com.uptime.upcastcm.utils.vo.TrendValuesVO;
import com.uptime.upcastcm.utils.vo.UserPreferencesVO;
import com.uptime.upcastcm.utils.vo.UserSitesVO;
import com.uptime.upcastcm.utils.vo.WaveDataVO;
import com.uptime.upcastcm.utils.vo.WaveformVO;
import com.uptime.upcastcm.utils.vo.WorkOrderVO;
import java.time.Instant;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.component.UIViewRoot;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import static org.powermock.api.mockito.PowerMockito.when;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.primefaces.PrimeFaces;
import org.primefaces.event.NodeExpandEvent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.TreeNode;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.MenuModel;

/**
 *
 * @author gsingh
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(PowerMockRunner.class)
@PowerMockIgnore("jdk.internal.reflect.*")
@PrepareForTest({FacesContext.class, ExternalContext.class, UserManageBean.class, ServiceRegistrarClient.class, NavigationBean.class, SiteClient.class, PointLocationClient.class, PointClient.class, PointClient.class, AreaClient.class, TrendDataClient.class, FaultFrequenciesClient.class, ApALByPointClient.class, WorkOrderClient.class, PrimeFaces.class, PrimeFaces.Ajax.class, AudioUtil.class,})
public class AssetAnalysisWindowBeanTest {

    @Mock
    ServiceRegistrarClient serviceRegistrarClient;
    @Mock
    SiteClient siteClient;
    @Mock
    PointLocationClient pointLocationClient;
    @Mock
    PointClient pointClient;
    @Mock
    AreaClient areaClient;
    @Mock
    TrendDataClient trendDataClient;
    @Mock
    FaultFrequenciesClient faultFrequenciesClient;
    @Mock
    ApALByPointClient apALByPointClient;
    @Mock
    WorkOrderClient workOrderClient;
    @Mock
    FacesContext facesContext;
    @Mock
    ExternalContext externalContext;
    @Mock
    HttpServletRequest request;
    @Mock
    HttpSession session;
    @Mock
    UIViewRoot root;
    @Mock
    NavigationBean navigationBean;
    @Mock
    UserManageBean userManageBean;
    
    @Mock
    private AudioUtil audioUtil;

    private final AssetAnalysisWindowBean instance;
    Map<String, Object> sessionMap;
    private String customerAccount;
    MenuModel breadcrumbModel;
    private final List<UserSitesVO> userSiteList;
    private final Filters filters;
    private Filters selectedFilters;
    RequestParameterMap requestParameterMap;

    public AssetAnalysisWindowBeanTest() {
        customerAccount = "77777";
        PowerMockito.mockStatic(FacesContext.class);
        PowerMockito.mockStatic(ServiceRegistrarClient.class);
        PowerMockito.mockStatic(SiteClient.class);
        PowerMockito.mockStatic(PointLocationClient.class);
        PowerMockito.mockStatic(PointClient.class);
        PowerMockito.mockStatic(AreaClient.class);
        PowerMockito.mockStatic(TrendDataClient.class);
        PowerMockito.mockStatic(FaultFrequenciesClient.class);
        PowerMockito.mockStatic(ApALByPointClient.class);
        PowerMockito.mockStatic(WorkOrderClient.class);

        facesContext = PowerMockito.mock(FacesContext.class);
        externalContext = PowerMockito.mock(ExternalContext.class);
        request = PowerMockito.mock(HttpServletRequest.class);
        session = PowerMockito.mock(HttpSession.class);
        root = PowerMockito.mock(UIViewRoot.class);
        navigationBean = PowerMockito.mock(NavigationBean.class);
        userManageBean = PowerMockito.mock(UserManageBean.class);
        audioUtil = PowerMockito.mock(AudioUtil.class);
        

        siteClient = PowerMockito.mock(SiteClient.class);
        pointLocationClient = PowerMockito.mock(PointLocationClient.class);
        pointClient = PowerMockito.mock(PointClient.class);
        areaClient = PowerMockito.mock(AreaClient.class);
        serviceRegistrarClient = PowerMockito.mock(ServiceRegistrarClient.class);
        trendDataClient = PowerMockito.mock(TrendDataClient.class);
        faultFrequenciesClient = PowerMockito.mock(FaultFrequenciesClient.class);
        apALByPointClient = PowerMockito.mock(ApALByPointClient.class);
        workOrderClient = PowerMockito.mock(WorkOrderClient.class);
        requestParameterMap = PowerMockito.mock(RequestParameterMap.class);

        sessionMap = new HashMap();
        sessionMap.put("navigationBean", navigationBean);
        sessionMap.put("userManageBean", userManageBean);
        session.setAttribute("navigationBean", navigationBean);
        session.setAttribute("userManageBean", userManageBean);

        userSiteList = new ArrayList<>();
        UserSitesVO uvo = new UserSitesVO();
        uvo.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        uvo.setSiteName("Test Site 1");
        uvo.setCustomerAccount("77777");
        userSiteList.add(uvo);

        when(ServiceRegistrarClient.getInstance()).thenReturn(serviceRegistrarClient);
        when(SiteClient.getInstance()).thenReturn(siteClient);
        when(PointLocationClient.getInstance()).thenReturn(pointLocationClient);
        when(PointClient.getInstance()).thenReturn(pointClient);
        when(AreaClient.getInstance()).thenReturn(areaClient);
        when(TrendDataClient.getInstance()).thenReturn(trendDataClient);
        when(FaultFrequenciesClient.getInstance()).thenReturn(faultFrequenciesClient);
        when(WorkOrderClient.getInstance()).thenReturn(workOrderClient);
        when(ApALByPointClient.getInstance()).thenReturn(apALByPointClient);
        when(FacesContext.getCurrentInstance()).thenReturn(facesContext);
        when(facesContext.getExternalContext()).thenReturn(externalContext);
        when(externalContext.getSessionMap()).thenReturn(sessionMap);
        when(facesContext.getExternalContext().getRequest()).thenReturn(request);
        try {
            PowerMockito.whenNew(AudioUtil.class).withNoArguments().thenReturn(audioUtil);
            //when(new AudioUtil()).thenReturn(audioUtil);
        } catch (Exception ex) {
            Logger.getLogger(AssetAnalysisWindowBeanTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        when(facesContext.getExternalContext().getRequestParameterMap()).thenReturn(requestParameterMap);
        when(requestParameterMap.get("graphDataWaveForm:iph")).thenReturn("Wave Graph");
        when(requestParameterMap.get("graphDataSpecForm:iph")).thenReturn("Spectrum Graph");
        when(requestParameterMap.get("hiddenForm1:windowId")).thenReturn("window1");
        when(requestParameterMap.get("trendGraphActionBarFormId:windowId")).thenReturn("window1");
        when(requestParameterMap.get("trendGraphActionBarFormId:windowUrlId")).thenReturn("window1");
        when(requestParameterMap.get("createWorkOrderAnalysisFormId:windowUrlId")).thenReturn("window1");
        when(requestParameterMap.get("actionTopBarFormId:windowUrlId")).thenReturn("window1");
        when(requestParameterMap.get("editAlarmLimitFormId:windowUrlId")).thenReturn("window1");

        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("navigationBean")).thenReturn(navigationBean);
        when(session.getAttribute("userManageBean")).thenReturn(userManageBean);
        when(userManageBean.getUserSiteList()).thenReturn(userSiteList);
        when(userManageBean.getCurrentSite()).thenReturn(uvo);
        when(userManageBean.getZoneId()).thenReturn(ZoneOffset.UTC);
        DefaultUnits du = new DefaultUnits();
        du.setTimeUnit("s");
        du.setAccelerationAmpFactor("Peak");
        du.setAccelerationlogYaxis(true);
        du.setAccelerationWaveform("in/s");
        du.setAccelerationSpectrum("in/s/s");
        when(userManageBean.getAccountDefaultUnits()).thenReturn(du);
        filters = new Filters();
        filters.setCountry("Test Country 1");
        filters.setRegion("Test Region 1");
        filters.setSite(new SelectItem(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), "Test Site 1"));
        filters.setArea(new SelectItem(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"), "Test Area 1"));
        filters.setAsset(new SelectItem(UUID.fromString("f33fcf5a-5f93-4504-bc09-74a61a460bf0"), "Test Asset 1"));
        when(userManageBean.getSelectedFilters()).thenReturn(filters);
        when(facesContext.getViewRoot()).thenReturn(root);
        PowerMockito.mockStatic(PrimeFaces.class);
        PowerMockito.mockStatic(PrimeFaces.Ajax.class);
        PrimeFaces pf = PowerMockito.mock(PrimeFaces.class);
        PrimeFaces.Ajax ax = PowerMockito.mock(PrimeFaces.Ajax.class);
        when(pf.current()).thenReturn(pf);
        when(pf.ajax()).thenReturn(ax);

        UserPreferencesVO userVO = new UserPreferencesVO();
        userVO.setDefaultSite(UUID.randomUUID());
        userVO.setCustomerAccount("77777");
        when(session.getAttribute("userVO")).thenReturn(userVO);
        UserSitesVO usvo = new UserSitesVO();
        usvo.setCustomerAccount(customerAccount);
        when(userManageBean.getCurrentSite()).thenReturn(usvo);

        instance = new AssetAnalysisWindowBean();

    }

    @Before
    public void setUp() {

        UserSitesVO uvo = new UserSitesVO();
        uvo.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        uvo.setSiteName("Test Site 1");
        uvo.setCustomerAccount(customerAccount);
        when(userManageBean.getCurrentSite()).thenReturn(uvo);
        breadcrumbModel = new DefaultMenuModel();
        when(navigationBean.getBreadcrumbModel()).thenReturn(breadcrumbModel);
        NodeExpandEvent nee = PowerMockito.mock(NodeExpandEvent.class);
        TreeNode tn = PowerMockito.mock(TreeNode.class);
        when(tn.getData()).thenReturn(new String());
        List<TreeNode> tnList = new ArrayList<>();
        tnList.add(tn);
        when(navigationBean.getTreeNodeRoot()).thenReturn(tn);
        when(navigationBean.getTreeNodeRoot().getChildren()).thenReturn(tnList);

        selectedFilters = new Filters();
        selectedFilters.setCountry("Test Country 1");
        selectedFilters.setRegion("Test Region 1");
        selectedFilters.setSite(new SelectItem(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), "site"));
        selectedFilters.setArea(new SelectItem(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), "area"));
        selectedFilters.setAsset(new SelectItem(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), "asset"));
        selectedFilters.setPointLocation(new SelectItem(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), "Test Point Location 1"));
        selectedFilters.setPoint(new SelectItem(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), "1 Acceleration"));
        selectedFilters.setApSet(new SelectItem(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), "Ap Set 1"));
        selectedFilters.setAlSetParam(new SelectItem(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), "Overall"));
    }

    /**
     * Test of init method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void test01_Init() {
        System.out.println("init");
        MockitoAnnotations.initMocks(instance);
        List<SiteVO> list = new ArrayList();
        SiteVO s1 = new SiteVO();
        s1.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        s1.setSiteName("Test Site 1");
        s1.setCountry("Test Country 1");
        s1.setRegion("Test Region 1");
        s1.setCustomerAccount(customerAccount);
        list.add(s1);
        List<AreaVO> areaList = new ArrayList();
        AreaVO avo = new AreaVO();
        avo.setAreaId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        avo.setAreaName("Test Area 1");
        avo.setClimateControlled(false);
        avo.setCustomerAccount(customerAccount);
        avo.setDescription("Test Description 1");
        avo.setEnvironment("Test Environment 1");
        avo.setScheduleVOList(new ArrayList());
        avo.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        avo.setSiteName("Test Site 1");
        areaList.add(avo);
        PowerMockito.mockStatic(PrimeFaces.class);
        PowerMockito.mockStatic(PrimeFaces.Ajax.class);
        PrimeFaces pf = PowerMockito.mock(PrimeFaces.class);
        PrimeFaces.Ajax ax = PowerMockito.mock(PrimeFaces.Ajax.class);
        when(pf.current()).thenReturn(pf);
        when(pf.ajax()).thenReturn(ax);
        when(serviceRegistrarClient.getServiceHostURL(CONFIGURATION_SERVICE_NAME)).thenReturn(CONFIGURATION_SERVICE_NAME);
        when(siteClient.getSiteRegionCountryByCustomer(CONFIGURATION_SERVICE_NAME, customerAccount)).thenReturn(list);
        when(areaClient.getAreaSiteByCustomerSite(CONFIGURATION_SERVICE_NAME, customerAccount, UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"))).thenReturn(areaList);
        instance.init();
        assertEquals(selectedFilters.getRegion(), s1.getRegion());
    }

    @Test
    public void test02_Init() {
        System.out.println("test02_Init");
        PowerMockito.mockStatic(PrimeFaces.class);
        PowerMockito.mockStatic(PrimeFaces.Ajax.class);
        PrimeFaces pf = PowerMockito.mock(PrimeFaces.class);
        PrimeFaces.Ajax ax = PowerMockito.mock(PrimeFaces.Ajax.class);
        when(pf.current()).thenReturn(pf);
        when(pf.ajax()).thenReturn(ax);
        instance.init();
        assertNotEquals(selectedFilters.getRegion(), null);
        System.out.println("test02_Init completed");
    }

    /**
     * Test of configureHighChart method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testConfigureHighChart() {
        System.out.println("testConfigureHighChart");
        List<TrendValuesVO> trendDataList = new ArrayList<>();
        TrendValuesVO tvo = new TrendValuesVO();
        tvo.setSampleTime(Instant.ofEpochMilli(Long.valueOf("1672088340000")));
        tvo.setValue(5f);
        tvo.setUnits("KMS");
        trendDataList.add(tvo);
        String containerId = "window1";
        setupChartMap();
        String expResult = "{ \"chart\" : { marginTop:70, \"zoomType\" : \"x\" , \"height\": 280,events: { load() { updateRemoveWindowDetails(), this.series[0].points[0].select() } } }, time: { timezoneOffset: 0 }, \"series\" : [ { lineWidth: 1, states: { hover: { lineWidth: 1 }  }, \"data\" :[[1672088340000, 5.0]], color: \"#003399\", \"name\" : \" \", cropThreshold: 9999 } ], plotOptions: { series: {  point: { events: { select: function () {var date = new Date(this.x).toLocaleString(\"en-US\", {timeZone: \"GMT\", weekday:\"short\", year:\"numeric\", month:\"short\", day:\"numeric\", hour:\"2-digit\", minute:\"2-digit\", second:\"2-digit\", hourCycle: 'h23'}); var text = date + '</br>' + Highcharts.numberFormat(this.y, 4); var chart = this.series.chart; if (!chart.lbl) { chart.lbl = chart.renderer.label(text, 75, 7).attr({ padding: 10,r: 5,zIndex: 0 }).css({color: 'red'}).add();} else {chart.lbl.attr({text: text}); } } , click: function (event) { this.select(!this.selected, false); } } }, marker: { enabled: true, radius: 0.1,  states: { select: { fillColor: 'red', lineWidth: 0, radius: 4  }, hover: { fillColor: '#4da6ff', lineWidth: 1, radius: 6 } } } } }, \"title\" : { \"text\" : \" Overall \" },exporting: appendExportButtonsToTrenGraph(),  \"legend\" : { \"enabled\" : false },\"yAxis\" : [ { min:0, max: 7.975,  \"title\" : { \"text\" : \" KMS - null \" } ,\"plotLines\": [{ \"color\": \"red\", \"width\": 2, \"dashStyle\": \"dash\", \"value\": 0.0, \"label\": { \"text\": \"null\", \"align\": \"right\"} },{ \"color\": \"orange\", \"width\": 2, \"dashStyle\": \"dash\", \"value\": 1.0, \"label\": { \"text\": \"null\", \"align\": \"right\"} },{ \"color\": \"orange\", \"width\": 2, \"dashStyle\": \"dash\", \"value\": 6.85, \"label\": { \"text\": \"null\", \"align\": \"right\"} },{ \"color\": \"red\", \"width\": 2, \"dashStyle\": \"dash\", \"value\": 7.25, \"label\": { \"text\": \"null\", \"align\": \"right\"} }] } ],\"xAxis\": [{ \"title\": { \"text\": \" null \" }, \"type\": \"datetime\" } ],tooltip: { positioner: function() { return { x: this.chart.plotSizeX-135, y: this.chart.plotTop-45 }; }, formatter() { return new Date(this.x).toLocaleString(\"en-US\", { timeZone: \"GMT\", weekday:\"short\", year:\"numeric\", month:\"short\", day:\"numeric\", hour:\"2-digit\", minute:\"2-digit\", second:\"2-digit\", hourCycle: 'h23'}) +'</br>' + Highcharts.numberFormat(this.y, 4)},shared: true, shadow: false, borderWidth: 0, backgroundColor: \"rgba(255,255,255,0.8)\" }  ,navigator: { top: 35, margin: 10, enabled: true, series: {  dataGrouping: { enabled: false } } ,xAxis: { type: 'linear' } }, scrollbar: { enabled: true }}";
        instance.configureHighChart(trendDataList, containerId, true);
        assertNotEquals(instance.getChartOptionsJs(), expResult);
    }

    /**
     * Test of updateAlarmLimits method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testUpdateAlarmLimits() {
        System.out.println("testUpdateAlarmLimits");

        String containerId = "window1";
        setupChartMap();
        instance.setWindowId(containerId);
        when(apALByPointClient.updateApALByPoint(serviceRegistrarClient.getServiceHostURL(CONFIGURATION_SERVICE_NAME), instance.getChartInfoMap().get(containerId).getSelectedAlarmLimitPoint(), "growlMessages")).thenReturn(true);
        instance.updateAlarmLimits();
        assertEquals(instance.getChartInfoMap().get(containerId).getSelectedApAlByPoint().getApAlSetVOs().get(0).getLowAlert(), 1f, 0);
    }

    /**
     * Test of loadTrendGraph method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void test01_LoadTrendGraph() {
        System.out.println("loadTrendGraph");
        boolean isLoadedFromAlarms = false;

        List<PointLocationVO> pointLocationVOList = setUpPointLocationVOList();
        when(pointLocationClient.getPointLocationsByCustomerSiteAreaAsset(serviceRegistrarClient.getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"))).thenReturn(pointLocationVOList);

        List<PointVO> pointVOList = setUpPointVOList();
        when(pointClient.getPointsByCustomerSiteAreaAssetPointLocationPoint(serviceRegistrarClient.getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), false)).thenReturn(pointVOList);

        List<PointVO> apAlByPointList = setUpApAlByPointList();
        when(pointClient.getApALByPoints_bySiteAreaAssetPointLocationPointApSetAlSet(serviceRegistrarClient.getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"))).thenReturn(apAlByPointList);
        List<TrendValuesVO> trendDataList = setUpTrendDataList();
        when(trendDataClient.getTrendsByCustomerSiteAreaAssetPointLocationPointApSetParam(ServiceRegistrarClient.getInstance().getServiceHostURL(WAVEFORM_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), "Overall")).thenReturn(trendDataList);
        List<FaultFrequenciesVO> ffSetListTmp = setUpffSetList();
        when(faultFrequenciesClient.findByCustomerSetId(serviceRegistrarClient.getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), UUID.fromString("b9c92679-9376-40fc-9498-083c104edca6"))).thenReturn(ffSetListTmp);

        WaveDataVO waveDataVO = setUpWaveDataVO();
        when(trendDataClient.getByCustomerSiteAreaAssetPointLocationPointSampleYearSampleMonthSampleTime(serviceRegistrarClient.getServiceHostURL(WAVEFORM_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), (short) 2023, (byte) 1, Instant.parse("2023-01-13T04:17:03Z"))).thenReturn(waveDataVO);
        instance.loadTrendGraph(selectedFilters, isLoadedFromAlarms);
        assertTrue(instance.isGenerateGraph());
    }

    /**
     * Test of loadTrendGraph method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void test02_LoadTrendGraph() {
        System.out.println("loadTrendGraph");
        boolean isLoadedFromAlarms = false;

        List<PointLocationVO> pointLocationVOList = setUpPointLocationVOList();
        when(pointLocationClient.getPointLocationsByCustomerSiteAreaAsset(serviceRegistrarClient.getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"))).thenReturn(pointLocationVOList);

        List<PointVO> pointVOList = setUpPointVOList();
        pointVOList.get(0).setPointType("DC");
        when(pointClient.getPointsByCustomerSiteAreaAssetPointLocationPoint(serviceRegistrarClient.getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), false)).thenReturn(pointVOList);

        List<PointVO> apAlByPointList = setUpApAlByPointList();
        apAlByPointList.get(0).setPointType("DC");
        when(pointClient.getApALByPoints_bySiteAreaAssetPointLocationPointApSetAlSet(serviceRegistrarClient.getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"))).thenReturn(apAlByPointList);
        List<TrendValuesVO> trendDataList = setUpTrendDataList();
        when(trendDataClient.getTrendsByCustomerSiteAreaAssetPointLocationPointApSet(ServiceRegistrarClient.getInstance().getServiceHostURL(WAVEFORM_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"))).thenReturn(trendDataList);
        List<FaultFrequenciesVO> ffSetListTmp = setUpffSetList();
        when(faultFrequenciesClient.findByCustomerSetId(serviceRegistrarClient.getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), UUID.fromString("b9c92679-9376-40fc-9498-083c104edca6"))).thenReturn(ffSetListTmp);

        WaveDataVO waveDataVO = setUpWaveDataVO();
        when(trendDataClient.getByCustomerSiteAreaAssetPointLocationPointSampleYearSampleMonthSampleTime(serviceRegistrarClient.getServiceHostURL(WAVEFORM_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), (short) 2023, (byte) 1, Instant.parse("2023-01-13T04:17:03Z"))).thenReturn(waveDataVO);
        instance.loadTrendGraph(selectedFilters, isLoadedFromAlarms);
        assertTrue(instance.isGenerateGraph());
    }

    /**
     * Test of displayAssociatedWaveAndSpectrum method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void test01_DisplayAssociatedWaveOrSpectrum() {
        System.out.println("displayAssociatedWaveOrSpectrum");
        boolean isCalledFromUI = false;
        setupChartMap();
        instance.setWindowId("window1");
        String timestamp = String.valueOf(Instant.parse("2023-01-13T04:17:03Z").toEpochMilli());
        instance.getChartInfoMap().get("window1").setGraphTimestamp(timestamp);

        Whitebox.setInternalState(instance, "generateWaveAndSpecGraph", false);
        WaveDataVO waveDataVO = setUpWaveDataVO();
        when(trendDataClient.getByCustomerSiteAreaAssetPointLocationPointSampleYearSampleMonthSampleTime(serviceRegistrarClient.getServiceHostURL(WAVEFORM_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), (short) 2023, (byte) 1, Instant.parse("2023-01-13T04:17:03Z"))).thenReturn(waveDataVO);
        instance.displayAssociatedWaveAndSpectrum(isCalledFromUI);
        assertTrue(instance.isGenerateWaveAndSpecGraph());
    }

    /**
     * Test of displayAssociatedWaveAndSpectrum method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void test02_DisplayAssociatedWaveOrSpectrum() {
        System.out.println("displayAssociatedWaveOrSpectrum");
        boolean isCalledFromUI = true;
        setupChartMap();
        instance.setWindowId("window1");
        String timestamp = String.valueOf(Instant.parse("2023-01-13T04:17:03Z").toEpochMilli());
        instance.getChartInfoMap().get("window1").setGraphTimestamp(timestamp);

        Whitebox.setInternalState(instance, "generateWaveAndSpecGraph", false);
        WaveDataVO waveDataVO = setUpWaveDataVO();
        when(trendDataClient.getByCustomerSiteAreaAssetPointLocationPointSampleYearSampleMonthSampleTime(serviceRegistrarClient.getServiceHostURL(WAVEFORM_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), (short) 2023, (byte) 1, Instant.parse("2023-01-13T04:17:03Z"))).thenReturn(waveDataVO);
        instance.displayAssociatedWaveAndSpectrum(isCalledFromUI);
        assertTrue(instance.isGenerateWaveAndSpecGraph());
    }

    /**
     * Test of getWaveChartJson method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testGetWaveChartJson01() {
        System.out.println("getWaveChartJson01");
        String values = "";
        String yAxisTitle = "";
        String xAxisTitle = "";
        String xAxisType = "";
        String graphHeader = "";
        String expResult = "{ \"chart\" : { events: { load() { updateRemoveWindowDetails() }, render() { let chart = this; if (chart.customImg) { chart.customImg.destroy(); } chart.customImg =  chart.renderer.image(\"../javax.faces.resource/images/volume.png.xhtml?ln=roma-layout\", chart.plotWidth + chart.plotLeft-54, 12, 25, 20).on(\"click\", function() { generateWaveAudio(); }).attr({ zIndex: 100  }).css({ cursor: \"pointer\" }).add(); } }, \"zoomType\" : \"x\" , \"height\": 242},  \"boost\" : { useGPUTranslations: true }, \"series\" : [ { showInNavigator: true, boostThreshold: 5000, lineWidth: 1, states: { hover: { lineWidth: 1 }  }, \"data\" :, color: \"#003399\", \"name\" : \" \" } ], \"title\" : { \"text\" : \"\" },plotOptions: { series: { showInNavigator: true, point: { events: { select: function () { var text = 'Ampl: ' + Highcharts.numberFormat(this.y, 4) + '</br>' + 'Time: ' + Highcharts.numberFormat(this.x, 4); var chart = this.series.chart; if (!chart.lbl) { chart.lbl = chart.renderer.label(text, 75, 7).attr({ padding: 10,r: 5,zIndex: 0 }).css({color: 'red'}).add();} else {chart.lbl.attr({text: text}); } } , click: function (event) { this.select(!this.selected, false); } } }, marker: { enabled: true, radius: 0.1, states: { select: { fillColor: 'red', lineWidth: 0, radius: 4  }, hover: { fillColor: '#4da6ff', lineWidth: 1, radius: 6 } } } } },exporting: appendExportButtonsToWaveGraph(), \"legend\" : { \"enabled\" : false },\"yAxis\" : [ { \"title\" : { \"text\" : \"  \" }  } ],\"xAxis\": [{ min:0, \"title\": { \"text\": \"  \" }, \"type\": \"\"   } ],tooltip: { formatter: function(){ return 'Ampl: ' + Highcharts.numberFormat(this.y, 4) + '</br>' + 'Time: ' + Highcharts.numberFormat(this.x, 4) }, positioner: function() { return { x: this.chart.plotSizeX-120, y: this.chart.plotTop-45 }; }, shared: true, shadow: false, borderWidth: 0, backgroundColor: \"rgba(255,255,255,0.8)\" }  ,navigator: { margin:10, enabled: true, series: {  dataGrouping: { enabled: false } } ,xAxis: { type: 'linear', labels: { formatter: function() { return Highcharts.numberFormat(this.value, 2); } } } }, scrollbar: { enabled: true }}";
        String result = instance.getWaveChartJson(values, yAxisTitle, xAxisTitle, xAxisType, graphHeader);
        assertNotEquals(expResult, result);
    }

    /**
     * Test of getWaveChartJson method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testGetWaveChartJson02() {
        System.out.println("getWaveChartJson02");
        String values = "[{1,2.3},{2,1.3},{3,1.5},{4,5.3}]";
        String yAxisTitle = "Pascals";
        String xAxisTitle = "s";
        String xAxisType = "Time";
        String graphHeader = "Total Non Sync";
        String expResult = "{ \"chart\" : { events: { load() { updateRemoveWindowDetails() }, render() { let chart = this; if (chart.customImg) { chart.customImg.destroy(); } chart.customImg =  chart.renderer.image(\"../javax.faces.resource/images/volume.png.xhtml?ln=roma-layout\", chart.plotWidth + chart.plotLeft-54, 12, 25, 20).on(\"click\", function() { generateWaveAudio(); }).attr({ zIndex: 100  }).css({ cursor: \"pointer\" }).add(); } }, \"zoomType\" : \"x\" , \"height\": 242},  \"boost\" : { useGPUTranslations: true }, \"series\" : [ { showInNavigator: true, boostThreshold: 5000, lineWidth: 1, states: { hover: { lineWidth: 1 }  }, \"data\" :[{1,2.3},{2,1.3},{3,1.5},{4,5.3}], color: \"#003399\", \"name\" : \" \" } ], \"title\" : { \"text\" : \"Total Non Sync\" },plotOptions: { series: { showInNavigator: true, point: { events: { select: function () { var text = 'Ampl: ' + Highcharts.numberFormat(this.y, 4) + '</br>' + 'Time: ' + Highcharts.numberFormat(this.x, 4); var chart = this.series.chart; if (!chart.lbl) { chart.lbl = chart.renderer.label(text, 75, 7).attr({ padding: 10,r: 5,zIndex: 0 }).css({color: 'red'}).add();} else {chart.lbl.attr({text: text}); } } , click: function (event) { this.select(!this.selected, false); } } }, marker: { enabled: true, radius: 0.1, states: { select: { fillColor: 'red', lineWidth: 0, radius: 4  }, hover: { fillColor: '#4da6ff', lineWidth: 1, radius: 6 } } } } },exporting: appendExportButtonsToWaveGraph(), \"legend\" : { \"enabled\" : false },\"yAxis\" : [ { \"title\" : { \"text\" : \" Pascals \" }  } ],\"xAxis\": [{ min:0, \"title\": { \"text\": \" s \" }, \"type\": \"Time\"   } ],tooltip: { formatter: function(){ return 'Ampl: ' + Highcharts.numberFormat(this.y, 4) + '</br>' + 'Time: ' + Highcharts.numberFormat(this.x, 4) }, positioner: function() { return { x: this.chart.plotSizeX-120, y: this.chart.plotTop-45 }; }, shared: true, shadow: false, borderWidth: 0, backgroundColor: \"rgba(255,255,255,0.8)\" }  ,navigator: { margin:10, enabled: true, series: {  dataGrouping: { enabled: false } } ,xAxis: { type: 'linear', labels: { formatter: function() { return Highcharts.numberFormat(this.value, 2); } } } }, scrollbar: { enabled: true }}";
        String result = instance.getWaveChartJson(values, yAxisTitle, xAxisTitle, xAxisType, graphHeader);
        assertNotEquals(expResult, result);
    }

    /**
     * Test of getSpectrumChartJson method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testGetSpectrumChartJson01() {
        System.out.println("getSpectrumChartJson01");
        String values = "";
        String yAxisTitle = "";
        String xAxisTitle = "";
        String ampFactor = "";
        String graphHeader = "";
        String expResult = "{ \"chart\" : { \"zoomType\" : \"x\" , \"height\": 242, events: { load() { updateRemoveWindowDetails() } } }, \"series\" : [ { lineWidth: 1, states: { hover: { lineWidth: 1 }  }, \"data\" :, color: \"#003399\", \"name\" : \" \" } ], \"title\" : { \"text\" : \"\" },plotOptions: { series: { point: { events: { select: function () { var text = 'Ampl: ' + Highcharts.numberFormat(this.y, 4) + '</br>' + 'Freq: ' + Highcharts.numberFormat(this.x, 4); var chart = this.series.chart; if (!chart.lbl) { chart.lbl = chart.renderer.label(text, 75, 7).attr({ padding: 10,r: 5,zIndex: 0 }).css({color: 'red'}).add();} else {chart.lbl.attr({text: text}); } }  , click: function (event) { this.select(!this.selected, false); } } }, marker: { enabled: true, radius: 0.1, states: { select: { fillColor: 'red', lineWidth: 0, radius: 4  }, hover: { fillColor: '#4da6ff', lineWidth: 1, radius: 6 } } } } },exporting: appendExportButtonsToSpectrumGraph(), \"legend\" : { \"enabled\" : false },\"yAxis\" : [ { type: \"logarithmic\" , minorTickInterval: 0.1 , \"title\" : { \"text\" : \"  -  \" }  } ],\"xAxis\": [{ min:0, \"title\": { \"text\": \"  \" }, \"type\": \"none\"    } ],tooltip: { formatter: function(){ return 'Ampl: ' + Highcharts.numberFormat(this.y, 4) + '</br>' + 'Freq: ' + Highcharts.numberFormat(this.x, 4) }, positioner: function() { return { x: this.chart.plotSizeX-120, y: this.chart.plotTop-45 }; }, shared: true, shadow: false, borderWidth: 0, backgroundColor: \"rgba(255,255,255,0.8)\" }  ,navigator: { yAxis: {type: 'logarithmic'}, margin:10, enabled: true, series: {  dataGrouping: { enabled: false } } ,xAxis: { type: 'linear', labels: { formatter: function() { return Highcharts.numberFormat(this.value, 2); } } } }, scrollbar: { enabled: true }}";
        String result = instance.getSpectrumChartJson(values, yAxisTitle, xAxisTitle, ampFactor, graphHeader, true);
        assertNotEquals(expResult, result);
    }

    /**
     * Test of getSpectrumChartJson method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testGetSpectrumChartJson02() {
        System.out.println("getSpectrumChartJson02");
        String values = "[{1,2.3},{2,1.3},{3,1.5},{4,5.3}]";
        String yAxisTitle = "in/s/s";
        String xAxisTitle = "min";
        String ampFactor = "RMS";
        String graphHeader = "Overall";
        String expResult = "{ \"chart\" : { \"zoomType\" : \"x\" , \"height\": 242, events: { load() { updateRemoveWindowDetails() } } }, \"series\" : [ { lineWidth: 1, states: { hover: { lineWidth: 1 }  }, \"data\" :[{1,2.3},{2,1.3},{3,1.5},{4,5.3}], color: \"#003399\", \"name\" : \" \" } ], \"title\" : { \"text\" : \"Overall\" },plotOptions: { series: { point: { events: { select: function () { var text = 'Ampl: ' + Highcharts.numberFormat(this.y, 4) + '</br>' + 'Freq: ' + Highcharts.numberFormat(this.x, 4); var chart = this.series.chart; if (!chart.lbl) { chart.lbl = chart.renderer.label(text, 75, 7).attr({ padding: 10,r: 5,zIndex: 0 }).css({color: 'red'}).add();} else {chart.lbl.attr({text: text}); } }  , click: function (event) { this.select(!this.selected, false); } } }, marker: { enabled: true, radius: 0.1, states: { select: { fillColor: 'red', lineWidth: 0, radius: 4  }, hover: { fillColor: '#4da6ff', lineWidth: 1, radius: 6 } } } } },exporting: appendExportButtonsToSpectrumGraph(), \"legend\" : { \"enabled\" : false },\"yAxis\" : [ { type: \"logarithmic\" , minorTickInterval: 0.1 , \"title\" : { \"text\" : \" in/s/s - RMS \" }  } ],\"xAxis\": [{ min:0, \"title\": { \"text\": \" min \" }, \"type\": \"none\"    } ],tooltip: { formatter: function(){ return 'Ampl: ' + Highcharts.numberFormat(this.y, 4) + '</br>' + 'Freq: ' + Highcharts.numberFormat(this.x, 4) }, positioner: function() { return { x: this.chart.plotSizeX-120, y: this.chart.plotTop-45 }; }, shared: true, shadow: false, borderWidth: 0, backgroundColor: \"rgba(255,255,255,0.8)\" }  ,navigator: { yAxis: {type: 'logarithmic'}, margin:10, enabled: true, series: {  dataGrouping: { enabled: false } } ,xAxis: { type: 'linear', labels: { formatter: function() { return Highcharts.numberFormat(this.value, 2); } } } }, scrollbar: { enabled: true }}";
        String result = instance.getSpectrumChartJson(values, yAxisTitle, xAxisTitle, ampFactor, graphHeader, true);
        assertNotEquals(expResult, result);
    }

    /**
     * Test of updateBean method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testUpdateBean() {
        System.out.println("updateBean");
        instance.updateBean();
    }

    /**
     * Test of onParamChange method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void test01_OnParamChange() {
        System.out.println("onParamChange");
        setupChartMap();
        instance.setWindowId("window1");
        instance.setParamName("All");
        List<TrendValuesVO> trendDataList = setUpTrendDataList();
        when(trendDataClient.getTrendsByCustomerSiteAreaAssetPointLocationPointApSetParam(ServiceRegistrarClient.getInstance().getServiceHostURL(WAVEFORM_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), "Overall")).thenReturn(trendDataList);
        List<String> chartOptionsJsList = new ArrayList();
        Whitebox.setInternalState(instance, "chartOptionsJsList", chartOptionsJsList);
        instance.onParamChange();
        assertTrue(!chartOptionsJsList.isEmpty());
    }

    /**
     * Test of onParamChange method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void test02_OnParamChange() {
        System.out.println("onParamChange");
        setupChartMap();
        instance.setWindowId("window1");
        instance.setParamName("Overall");
        List<TrendValuesVO> trendDataList = setUpTrendDataList();
        when(trendDataClient.getTrendsByCustomerSiteAreaAssetPointLocationPointApSetParam(ServiceRegistrarClient.getInstance().getServiceHostURL(WAVEFORM_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), "Overall")).thenReturn(trendDataList);
        List<String> chartOptionsJsList = new ArrayList();
        Whitebox.setInternalState(instance, "chartOptionsJsList", chartOptionsJsList);
        instance.onParamChange();
        assertTrue(!chartOptionsJsList.isEmpty());
    }

    /**
     * Test of onParamChange method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void test03_OnParamChange() {
        System.out.println("onParamChange");
        setupChartMap();
        instance.getChartInfoMap().get("window1").setPointType("DC");
        instance.setWindowId("window1");
        instance.setParamName("All");
        List<TrendValuesVO> trendDataList = setUpTrendDataList();
        when(trendDataClient.getTrendsByCustomerSiteAreaAssetPointLocationPointApSet(ServiceRegistrarClient.getInstance().getServiceHostURL(WAVEFORM_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"))).thenReturn(trendDataList);
        List<String> chartOptionsJsList = new ArrayList();
        Whitebox.setInternalState(instance, "chartOptionsJsList", chartOptionsJsList);
        instance.onParamChange();
        assertTrue(!chartOptionsJsList.isEmpty());
    }

    /**
     * Test of onParamChange method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void test04_OnParamChange() {
        System.out.println("onParamChange");
        setupChartMap();
        instance.getChartInfoMap().get("window1").setPointType("DC");
        instance.setWindowId("window1");
        instance.setParamName("Overall");
        List<TrendValuesVO> trendDataList = setUpTrendDataList();
        when(trendDataClient.getTrendsByCustomerSiteAreaAssetPointLocationPointApSet(ServiceRegistrarClient.getInstance().getServiceHostURL(WAVEFORM_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"))).thenReturn(trendDataList);
        List<String> chartOptionsJsList = new ArrayList();
        Whitebox.setInternalState(instance, "chartOptionsJsList", chartOptionsJsList);
        instance.onParamChange();
        assertTrue(!chartOptionsJsList.isEmpty());
    }

    /**
     * Test of loadPlotDataOptions method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void test01_LoadPlotDataOptions() {
        System.out.println("loadPlotDataOptions");
        instance.setGraphType("wave");
        instance.setWindowId("window1");
        setupChartMap();
        instance.loadPlotDataOptions();
        assertEquals(instance.getChartInfoMap().get("window1").getTimeUnit(), "s");
        assertEquals(instance.getChartInfoMap().get("window1").getAmplitudeUnit(), "m/s/s");
    }

    /**
     * Test of loadPlotDataOptions method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void test02_LoadPlotDataOptions() {
        System.out.println("loadPlotDataOptions");
        instance.setGraphType("spectrum");
        instance.setWindowId("window1");
        setupChartMap();
        instance.loadPlotDataOptions();
    }

    /**
     * Test of savePlotDataOptions method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void test01_SavePlotDataOptions() {
        System.out.println("savePlotDataOptions");
        instance.setGraphType("wave");
        instance.setWindowId("window1");
        setupChartMap();
        float[] convertedSamplesArray = new float[instance.getChartInfoMap().get("window1").getWaveDataVO().getWave().getSamples().size()];
        instance.setTimeUnit("ms");
        instance.setAmplitudeUnit(instance.getChartInfoMap().get("window1").getWaveGraphData().getSelectedAmplitudeUnit());
        instance.savePlotDataOptions(true);
        assertTrue(convertedSamplesArray.length > 0);
    }

    /**
     * Test of savePlotDataOptions method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void test02_SavePlotDataOptions() {
        System.out.println("savePlotDataOptions");
        instance.setGraphType("spectrum");
        instance.setWindowId("window1");
        setupChartMap();
        float[] convertedSamplesArray = new float[instance.getChartInfoMap().get("window1").getWaveDataVO().getWave().getSamples().size()];
        instance.setTimeUnit("ms");
        instance.setAmplitudeUnit(instance.getChartInfoMap().get("window1").getWaveGraphData().getSelectedAmplitudeUnit());
        instance.setFrequencyUnit(instance.getChartInfoMap().get("window1").getSpectrumGraphData().getSelectedFrequencyUnit());
        instance.setAmplitudeUnit(instance.getChartInfoMap().get("window1").getSpectrumGraphData().getSelectedAmplitudeUnit());
        instance.setAmpFactor(instance.getChartInfoMap().get("window1").getSpectrumGraphData().getSelectedAmpFactor());

        instance.savePlotDataOptions(true);
        assertTrue(convertedSamplesArray.length > 0);
    }

    /**
     * Test of calculateDeltaTime method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testCalculateDeltaTime() {
        System.out.println("calculateDeltaTime");
        String timeUnit = "s";
        int noOfRecords = 5;
        float xAxisLength = 100.0F;
        float expResult = 20.0F;
        float result = instance.calculateDeltaTime(timeUnit, noOfRecords, xAxisLength);
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of calculateDeltaFrequency method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testCalculateDeltaFrequency01() {
        System.out.println("calculateDeltaFrequency01");
        GraphDataVO graphDataVO = new GraphDataVO();
        graphDataVO.setSelectedFrequencyUnit("Hz");
        graphDataVO.setTachSpeedUnit("RPM");
        int noOfRecords = 5;
        float xAxisLength = 100.0F;
        float expResult = 20.0F;
        float result = instance.calculateDeltaFrequency(graphDataVO, noOfRecords, xAxisLength);
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of calculateDeltaFrequency method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testCalculateDeltaFrequency02() {
        System.out.println("calculateDeltaFrequency02");
        GraphDataVO graphDataVO = new GraphDataVO();
        graphDataVO.setSelectedFrequencyUnit("Orders");
        graphDataVO.setTachSpeed(2f);
        graphDataVO.setTachSpeedUnit("RPM");
        int noOfRecords = 5;
        float xAxisLength = 100.0F;
        float expResult = 10.0F;
        float result = instance.calculateDeltaFrequency(graphDataVO, noOfRecords, xAxisLength);
        //assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of getUnitConvertedSamplesAsString method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testGetUnitConvertedSamplesAsString() throws Exception {
        System.out.println("getUnitConvertedSamplesAsString");
        float[] sampleArray = new float[5];
        sampleArray[0] = 0.4765278f;
        sampleArray[1] = 0.82874405f;
        sampleArray[2] = 1.2016788f;
        sampleArray[3] = 1.3881462f;
        sampleArray[4] = 1.48138f;

        String yAxisUnit = "Gs";
        float timeDelta = 0.5F;
        GraphDataVO graphDataVO = new GraphDataVO();
        graphDataVO.setSelectedAmplitudeUnit("Gs");
        String expResult = "[[0, 0.4765278], [0.5, 0.82874405], [1, 1.2016788], [1.5, 1.3881462], [2, 1.48138]]";
        String result = instance.getUnitConvertedSamplesAsString(sampleArray, yAxisUnit, timeDelta, graphDataVO);
        assertEquals(expResult, result);
    }

    /**
     * Test of getSpectralUnitConvertedSamplesAsString method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testGetSamplesStringWithSpectralUnitConversion() throws Exception {
        System.out.println("getSamplesStringWithSpectralUnitConversion");
        float[] sampleArray = new float[5];
        sampleArray[0] = 0.4765278f;
        sampleArray[1] = 0.82874405f;
        sampleArray[2] = 1.2016788f;
        sampleArray[3] = 1.3881462f;
        sampleArray[4] = 1.48138f;
        String yAxisUnit = "Gs";
        float frequencyDelta = 0.2F;
        GraphDataVO gdVo = new GraphDataVO();
        gdVo.setSelectedAmplitudeUnit("Gs");
        ChartInfoVO ciVo = new ChartInfoVO();
        ciVo.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        ciVo.setSiteName("Test Site 1");
        ciVo.setPointType("AC");
        ciVo.setPointLocationName("SGA-1");
        ciVo.setPointName("Ultrasonic");
        ciVo.setSelectedApAlByPoint(setUpApAlByPointList().get(0));
        ciVo.setApAlByPointList(setUpApAlByPointList());
        String expResult = "[[0, 0.4765278], [0.2, 0.82874405], [0.40000001, 1.2016788], [0.60000002, 1.3881462], [0.80000001, 1.48138]]";
        String result = instance.getSpectralUnitConvertedSamplesAsString(sampleArray, yAxisUnit, frequencyDelta, gdVo, ciVo);
        assertEquals(expResult, result);
    }

    /**
     * Test of getSamplesAsString method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testGetSamplesAsString() throws Exception {
        System.out.println("getSamplesAsString");
        float[] sampleArray = new float[5];
        sampleArray[0] = 0.4765278f;
        sampleArray[1] = 0.82874405f;
        sampleArray[2] = 1.2016788f;
        sampleArray[3] = 1.3881462f;
        sampleArray[4] = 1.48138f;
        float delta = 0.2F;
        GraphDataVO gdVo = new GraphDataVO();
        gdVo.setSelectedAmplitudeUnit("Gs");
        String expResult = "[[0, 0.4765278], [0.2, 0.82874405], [0.40000001, 1.2016788], [0.60000002, 1.3881462], [0.80000001, 1.48138]]";
        String result = instance.getSamplesAsString(sampleArray, delta, gdVo);
        assertEquals(expResult, result);
    }

    /**
     * Test of getFloatArrayFromSampleList method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testGetFloatArray() {
        System.out.println("getFloatArray");
        float[] sampleArray = new float[5];
        sampleArray[0] = 0.4765278f;
        sampleArray[1] = 0.82874405f;
        sampleArray[2] = 1.2016788f;
        sampleArray[3] = 1.3881462f;
        sampleArray[4] = 1.48138f;
        List samples = new ArrayList();
        for (float f : sampleArray) {
            samples.add(f);
        }
        float[] result = instance.getFloatArrayFromSampleList(samples);
        assertEquals(sampleArray[1], result[1], 0.0);
    }

    /**
     * Test of onFFSetSelect method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void test01_OnFFSetSelect() {
        System.out.println("onFFSetSelect");
        setupChartMap();
        instance.setWindowId("window1");
        List<UUID> selectedFfIds = new ArrayList();
        selectedFfIds.add(UUID.fromString("b9c92679-9376-40fc-9498-083c104edca6"));
        instance.setSelectedFfIds(selectedFfIds);
        instance.onFFSetSelect();
        assertTrue(instance.getSelectedFfIds() == null);
    }

    /**
     * Test of onFFSetSelect method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void test02_OnFFSetSelect() {
        System.out.println("onFFSetSelect");
        setupChartMap();
        instance.setWindowId("window1");
        List<UUID> selectedFfIds = new ArrayList();
        selectedFfIds.add(UUID.fromString("b9c92679-9376-40fc-9498-083c104edca6"));
        instance.setSelectedFfIds(selectedFfIds);
        instance.onFFSetSelect();
        assertTrue(instance.getSelectedFfIds() == null);
    }

    /**
     * Test of onFFSelect method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void test01_OnFFSelect() {
        System.out.println("onFFSelect");
        when(requestParameterMap.get("graphDataWaveForm:iph")).thenReturn("window1");
        List<UUID> selectedFfIds = new ArrayList();
        selectedFfIds.add(UUID.fromString("7318b307-f8dc-4a5c-888f-d9a2c59324ba"));
        selectedFfIds.add(UUID.fromString("435f1a2d-1c81-4abf-a370-43dcecfde963"));
        selectedFfIds.add(UUID.fromString("2ef129fa-2d05-4dec-a84d-ed8f386ac1d0"));
        selectedFfIds.add(UUID.fromString("0361880c-d0d8-4eb7-91be-a436eb735c75"));
        instance.setSelectedFfIds(selectedFfIds);
        setupChartMap();

        instance.setWindowId("window1");
        test01_GetFfVoList();
        instance.getChartInfoMap().get("window1").getSpectrumGraphData().setEndingHarmonic(40);
        instance.onFFSelect();
        assertEquals(instance.getChartInfoMap().get("window1").getSpectrumGraphData().getEndingHarmonic(), null);
    }

    /**
     * Test of getFfSetItemsList method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void test01_GetFfSetItemsList() {
        System.out.println("getFfSetItemsList");
        when(requestParameterMap.get("graphDataWaveForm:iph")).thenReturn("window1");
        setupChartMap();
        String expResult = "GSR-Test-100723";
        List<SelectItem> result = instance.getFfSetItemsList();
        assertEquals(expResult, result.get(0).getLabel());
    }

    /**
     * Test of getFfSetItemsList method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void test02_GetFfSetItemsList() {
        System.out.println("getFfSetItemsList");
        when(requestParameterMap.get("graphDataSpecForm:iph")).thenReturn("window1");
        setupChartMap();
        String expResult = "GSR-Test-100723";
        List<SelectItem> result = instance.getFfSetItemsList();
        assertEquals(expResult, result.get(0).getLabel());
    }

    /**
     * Test of getFfVoList method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void test01_GetFfVoList() {
        System.out.println("getFfVoList");
        when(requestParameterMap.get("graphDataWaveForm:iph")).thenReturn("window1");
        setupChartMap();
        List<FaultFrequenciesVO> result = instance.getFfVoList();
        assertTrue(result.size() == 4);
    }

    /**
     * Test of getFfVoList method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void test02_GetFfVoList() {
        System.out.println("getFfVoList");
        when(requestParameterMap.get("graphDataSpecForm:iph")).thenReturn("window1");
        setupChartMap();
        List<FaultFrequenciesVO> result = instance.getFfVoList();
        assertTrue(result.size() == 4);
    }

    /**
     * Test of getWindowParams method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testGetWindowParams() {
        System.out.println("getWindowParams");
        String containerId = "window1";
        ChartInfoVO ciVo = new ChartInfoVO();
        ciVo.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        ciVo.setSiteName("Test Site 1");
        ciVo.setPointType("AC");
        ciVo.setPointLocationName("SGA-1");
        ciVo.setPointName("Ultrasonic");
        ciVo.setSelectedApAlByPoint(setUpApAlByPointList().get(0));
        ciVo.setApAlByPointList(setUpApAlByPointList());
        List<String> paramNameList = new ArrayList();
        paramNameList.add("Overall");
        ciVo.setParamNameList(paramNameList);
        Map<String, ChartInfoVO> chartInfoMap = new HashMap<>();
        chartInfoMap.put(containerId, ciVo);
        Whitebox.setInternalState(instance, "chartInfoMap", chartInfoMap);
        instance.setWindowId(containerId);
        List<String> expResult = paramNameList;
        List<String> result = instance.getWindowParams();
        assertEquals(expResult, result);
    }

    /**
     * Test of showAllTrendPoints method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testShowAllTrendPoints() {
        System.out.println("showAllTrendPoints");
        instance.setParamName("Overall");
        instance.setGraphType("Overall");
        setupChartMap();
        List<TrendValuesVO> trendValuesVOs = setUpTrendDataList();
        when(trendDataClient.getTrendsByCustomerSiteAreaAssetPointLocationPointApSetParamDateRange(serviceRegistrarClient.getServiceHostURL(WAVEFORM_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), instance.getGraphType(), Instant.EPOCH.toString(), Instant.now().toString())).thenReturn(trendValuesVOs);
        WaveDataVO waveDataVO = setUpWaveDataVO();
        when(trendDataClient.getByCustomerSiteAreaAssetPointLocationPointSampleYearSampleMonthSampleTime(serviceRegistrarClient.getServiceHostURL(WAVEFORM_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), (short) 2023, (byte) 1, Instant.parse("2023-01-13T04:17:03Z"))).thenReturn(waveDataVO);

        instance.showAllTrendPoints();
        assertFalse(instance.isGenerateGraph());
    }

    /**
     * Test of downloadTrendGraph method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testDownloadTrendGraph() {
        System.out.println("downloadTrendGraph");
        setupChartMap();
        String expResult = "TrendData_null.csv";
        instance.setXaxis("m/s/s");
        instance.setYaxis("Peak");
        instance.setYaxisTitle("TrendGraph");
        StreamedContent result = instance.downloadTrendGraph();
        assertEquals(expResult, result.getName());
    }

    private void setupChartMap() {
        String containerId = "window1";

        ChartInfoVO ciVo = new ChartInfoVO();
        ciVo.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        ciVo.setAreaId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        ciVo.setAssetId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        ciVo.setPointLocationId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        ciVo.setPointId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        ciVo.setSiteName("Test Site 1");
        ciVo.setPointType("AC");
        ciVo.setPointLocationName("SGA-1");
        ciVo.setPointName("1 Acceleration");
        ciVo.setSelectedApAlByPoint(setUpApAlByPointList().get(0));
        ciVo.setApAlByPointList(setUpApAlByPointList());
        ciVo.setGraphType("wave");
        List<String> paramNameList = new ArrayList();
        paramNameList.add("Overall");
        ciVo.setParamNameList(paramNameList);
        Map<String, ChartInfoVO> chartInfoMap = new HashMap<>();
        Map<String, List<TrendValuesVO>> trendDataMap = new HashMap<>();
        trendDataMap.put("Overall", setUpTrendDataList());
        //ciVo.setTrendDataMap(trendDataMap);
        for (PointVO pvo : setUpApAlByPointList()) {
            if (pvo.getPointName().equals(ciVo.getPointName())) {
                ciVo.setResolution(pvo.getResolution());
                break;
            }
        }
        List<FaultFrequenciesVO> ffSetVOList = setUpffSetList();
        ciVo.setFfSetVOList(ffSetVOList);
        ciVo.setWaveDataVO(setUpWaveDataVO());

        GraphDataVO graphDataVO = new GraphDataVO();
        graphDataVO.setSelectedFfSetId(UUID.fromString("b9c92679-9376-40fc-9498-083c104edca6"));
        graphDataVO.setFrequencyList(ffSetVOList);
        graphDataVO.setSelectedTimeUnit("s");
        graphDataVO.setSelectedAmplitudeUnit("Gs");
        graphDataVO.setSelectedFrequencyUnit("Hz");
        graphDataVO.setSelectedAmplitudeUnit("m/s/s");
        graphDataVO.setSelectedAmpFactor("RMS");
        ciVo.setWaveGraphData(graphDataVO);
        ciVo.setSpectrumGraphData(graphDataVO);

        WorkOrderVO workOrderVO = new WorkOrderVO();
        workOrderVO.setCustomerAccount("77777");
        workOrderVO.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"));
        workOrderVO.setAreaId(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"));
        workOrderVO.setAssetId(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"));
        workOrderVO.setPointLocationId(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"));
        workOrderVO.setPointId(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"));
        workOrderVO.setApSetId(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"));
        workOrderVO.setDescription("Test Description");
        workOrderVO.setPriority((byte) 4);
        workOrderVO.setIssue("Battery Failed");
        workOrderVO.setAction("Battery Replace");
        TrendDetailsVO trendDetailsVO = new TrendDetailsVO();
        trendDetailsVO.setApSetId(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"));
        trendDetailsVO.setChannelType("AC");
        trendDetailsVO.setParamName("Overall");
        trendDetailsVO.setPointId(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"));
        trendDetailsVO.setPointLocationId(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"));
        trendDetailsVO.setPointLocationName("Test Point Location");
        trendDetailsVO.setPointName("Test Point");
        trendDetailsVO.setRowId(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"));
        List<TrendDetailsVO> trendDetailsVOs = new ArrayList();
        trendDetailsVOs.add(trendDetailsVO);
        workOrderVO.setTrendDetails(trendDetailsVOs);
        ciVo.setWorkOrderVO(workOrderVO);
        ciVo.setSelectedAlarmLimitPoint(setUpPointVOList().get(0));
        ciVo.setSelectedApAlByPoint(setUpApAlByPointList().get(0));
        chartInfoMap.put(containerId, ciVo);
        Whitebox.setInternalState(instance, "chartInfoMap", chartInfoMap);
        instance.setWindowId(containerId);
    }

    /**
     * Test of alarmLimitValidation method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void test01_AlarmLimitValidation() {
        System.out.println("alarmLimitValidation");
        setupChartMap();
        instance.getChartInfoMap().get("window1").setLowFaultValidationFailed(false);
        instance.getChartInfoMap().get("window1").setLowAlertValidationFailed(false);
        instance.getChartInfoMap().get("window1").setHighAlertValidationFailed(true);
        instance.getChartInfoMap().get("window1").setHighFaultValidationFailed(true);
        instance.setWindowId("window1");
        boolean reset = false;
        instance.alarmLimitValidation(reset);
        assertEquals(true, instance.getChartInfoMap().get("window1").isLowFaultValidationFailed());
        assertEquals(true, instance.getChartInfoMap().get("window1").isLowAlertValidationFailed());
        assertEquals(false, instance.getChartInfoMap().get("window1").isHighAlertValidationFailed());
        assertEquals(false, instance.getChartInfoMap().get("window1").isHighFaultValidationFailed());
    }

    /**
     * Test of alarmLimitValidation method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void test02_AlarmLimitValidation() {
        System.out.println("alarmLimitValidation");
        setupChartMap();
        instance.getChartInfoMap().get("window1").setLowFaultValidationFailed(false);
        instance.getChartInfoMap().get("window1").setLowAlertValidationFailed(false);
        instance.getChartInfoMap().get("window1").setHighAlertValidationFailed(true);
        instance.getChartInfoMap().get("window1").setHighFaultValidationFailed(true);
        instance.setWindowId("window1");
        boolean reset = true;
        instance.alarmLimitValidation(reset);
        assertEquals(true, instance.getChartInfoMap().get("window1").isLowFaultValidationFailed());
        assertEquals(false, instance.getChartInfoMap().get("window1").isLowAlertValidationFailed());
        assertEquals(false, instance.getChartInfoMap().get("window1").isHighAlertValidationFailed());
        assertEquals(false, instance.getChartInfoMap().get("window1").isHighFaultValidationFailed());
    }

    /**
     * Test of toggleCreateWorkOrder method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void test01_ToggleCreateWorkOrder() {
        System.out.println("toggleCreateWorkOrder");
        setupChartMap();
        instance.getChartInfoMap().get("window1").setSelectedAlarmLimitPoint(new PointVO());
        instance.setWindowId("window1");
        instance.toggleCreateWorkOrder();
        assertTrue(instance.getChartInfoMap().get("window1").getWorkOrderVO().getTrendDetails().isEmpty());
    }

    /**
     * Test of toggleCreateWorkOrder method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void test02_ToggleCreateWorkOrder() {
        System.out.println("toggleCreateWorkOrder");
        setupChartMap();
        instance.getChartInfoMap().get("window1").setSelectedAlarmLimitPoint(new PointVO());
        instance.setWindowId("window1");
        when(userManageBean.getCurrentSite()).thenReturn(null);
        instance.toggleCreateWorkOrder();
        assertEquals(new WorkOrderVO(), instance.getChartInfoMap().get("window1").getWorkOrderVO());
    }

    /**
     * Test of submitWorkOrder method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testSubmitWorkOrder() {
        System.out.println("submitWorkOrder");
        setupChartMap();
        instance.getChartInfoMap().get("window1").setSelectedAlarmLimitPoint(new PointVO());
        instance.setWindowId("window1");
        when(workOrderClient.createOpenWorkOrder(serviceRegistrarClient.getServiceHostURL(WORK_ORDER_SERVICE_NAME), instance.getChartInfoMap().get("window1").getWorkOrderVO())).thenReturn(Boolean.TRUE);
        instance.submitWorkOrder();
        assertTrue(instance.getChartInfoMap().get("window1").getWorkOrderVO().getTrendDetails().isEmpty());

    }

    /**
     * Test of autoCorrelate method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void test01_AutoCorrelate() {
        System.out.println("autoCorrelate");
        setupChartMap();
        instance.getChartInfoMap().get("window1").setWorkOrderVO(new WorkOrderVO());
        instance.getChartInfoMap().get("window1").setSelectedAlarmLimitPoint(new PointVO());
        instance.setWindowId("window1");
        instance.setGenericStr("apply-correlation");
        instance.autoCorrelate();
        assertEquals(0.3944000005722046, instance.getChartInfoMap().get("window1").getWaveGraphData().getRmsWave(), 0.0);
    }

    /**
     * Test of autoCorrelate method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void test02_AutoCorrelate() {
        System.out.println("autoCorrelate");
        setupChartMap();
        instance.getChartInfoMap().get("window1").setWorkOrderVO(new WorkOrderVO());
        instance.getChartInfoMap().get("window1").setSelectedAlarmLimitPoint(new PointVO());
        instance.setWindowId("window1");
        instance.setGenericStr("remove-correlation");
        instance.autoCorrelate();
        assertEquals(20.212799072265625, instance.getChartInfoMap().get("window1").getWaveGraphData().getRmsWave(), 0.0);
    }

    /**
     * Test of downloadWaveformGraph method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testDownloadWaveformGraph() {
        System.out.println("downloadWaveformGraph");
        setupChartMap();
        //instance.getChartInfoMap().get("window1").setSelectedAlarmLimitPoint(new PointVO());
        instance.setWindowId("window1");
        instance.setXaxis("m/s/s");
        instance.setYaxis("Peak");
        instance.setYaxisTitle("WaveformGraph");
        StreamedContent result = instance.downloadWaveformGraph();
        assertTrue(result.getName().contains("WaveForm"));
    }

    /**
     * Test of downloadSpectrumGraph method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testDownloadSpectrumGraph() {
        System.out.println("downloadSpectrumGraph");
        setupChartMap();
        // instance.getChartInfoMap().get("window1").setSelectedAlarmLimitPoint(new PointVO());
        instance.setWindowId("window1");
        instance.setXaxis("m/s/s");
        instance.setYaxis("Peak");
        instance.setYaxisTitle("SpectrumGraph");
        StreamedContent result = instance.downloadSpectrumGraph();
        assertTrue(result.getName().contains("Spectrum"));
    }

    /**
     * Test of setEditAlarmLimitLowFaultActive method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testSetEditAlarmLimitLowFaultActive() {
        System.out.println("setEditAlarmLimitLowFaultActive");
        setupChartMap();
        instance.getChartInfoMap().get("window1").setWorkOrderVO(new WorkOrderVO());
        instance.getChartInfoMap().get("window1").setSelectedAlarmLimitPoint(new PointVO());
        instance.setWindowId("window1");
        boolean value = false;
        instance.setEditAlarmLimitLowFaultActive(value);
        boolean result = instance.isEditAlarmLimitLowFaultActive();
        assertEquals(value, result);
    }

    /**
     * Test of isEditAlarmLimitLowFaultActive method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testIsEditAlarmLimitLowFaultActive() {
        System.out.println("isEditAlarmLimitLowFaultActive");
        testSetEditAlarmLimitLowFaultActive();
        boolean expResult = false;
        boolean result = instance.isEditAlarmLimitLowFaultActive();
        assertEquals(expResult, result);
    }

    /**
     * Test of setEditAlarmLimitLowAlertActive method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testSetEditAlarmLimitLowAlertActive() {
        System.out.println("setEditAlarmLimitLowAlertActive");
        setupChartMap();
        instance.getChartInfoMap().get("window1").setWorkOrderVO(new WorkOrderVO());
        instance.getChartInfoMap().get("window1").setSelectedAlarmLimitPoint(new PointVO());
        instance.setWindowId("window1");
        boolean value = false;
        instance.setEditAlarmLimitLowAlertActive(value);
        boolean result = instance.isEditAlarmLimitLowAlertActive();
        assertEquals(value, result);
    }

    /**
     * Test of isEditAlarmLimitLowAlertActive method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testIsEditAlarmLimitLowAlertActive() {
        System.out.println("isEditAlarmLimitLowAlertActive");
        testSetEditAlarmLimitLowAlertActive();
        boolean expResult = false;
        boolean result = instance.isEditAlarmLimitLowAlertActive();
        assertEquals(expResult, result);
    }

    /**
     * Test of setEditAlarmLimitHighAlertActive method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testSetEditAlarmLimitHighAlertActive() {
        System.out.println("setEditAlarmLimitHighAlertActive");
        setupChartMap();
        instance.getChartInfoMap().get("window1").setWorkOrderVO(new WorkOrderVO());
        instance.getChartInfoMap().get("window1").setSelectedAlarmLimitPoint(new PointVO());
        instance.setWindowId("window1");
        boolean value = false;
        instance.setEditAlarmLimitHighAlertActive(value);
        boolean result = instance.isEditAlarmLimitHighAlertActive();
        assertEquals(value, result);
    }

    /**
     * Test of isEditAlarmLimitHighAlertActive method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testIsEditAlarmLimitHighAlertActive() {
        System.out.println("isEditAlarmLimitHighAlertActive");
        testSetEditAlarmLimitHighAlertActive();
        boolean expResult = false;
        boolean result = instance.isEditAlarmLimitHighAlertActive();
        assertEquals(expResult, result);
    }

    /**
     * Test of setEditAlarmLimitHighFaultActive method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testSetEditAlarmLimitHighFaultActive() {
        System.out.println("setEditAlarmLimitHighFaultActive");
        setupChartMap();
        instance.getChartInfoMap().get("window1").setWorkOrderVO(new WorkOrderVO());
        instance.getChartInfoMap().get("window1").setSelectedAlarmLimitPoint(new PointVO());
        instance.setWindowId("window1");
        boolean value = true;
        instance.setEditAlarmLimitHighFaultActive(value);
        boolean result = instance.isEditAlarmLimitHighFaultActive();
        assertEquals(value, result);
    }

    /**
     * Test of isEditAlarmLimitHighFaultActive method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testIsEditAlarmLimitHighFaultActive() {
        System.out.println("isEditAlarmLimitHighFaultActive");
        testSetEditAlarmLimitHighFaultActive();
        boolean expResult = true;
        boolean result = instance.isEditAlarmLimitHighFaultActive();
        assertEquals(expResult, result);
    }

    /**
     * Test of setEditAlarmLimitLowFault method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testSetEditAlarmLimitLowFault() {
        System.out.println("setEditAlarmLimitLowFault");
        setupChartMap();
        instance.getChartInfoMap().get("window1").setWorkOrderVO(new WorkOrderVO());
        instance.getChartInfoMap().get("window1").setSelectedAlarmLimitPoint(new PointVO());
        instance.setWindowId("window1");
        float value = 0.1F;
        instance.setEditAlarmLimitLowFault(value);
        float result = instance.getEditAlarmLimitLowFault();
        assertEquals(value, result, 0.0);
    }

    /**
     * Test of getEditAlarmLimitLowFault method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testGetEditAlarmLimitLowFault() {
        System.out.println("getEditAlarmLimitLowFault");
        testSetEditAlarmLimitLowFault();
        float expResult = 0.1F;
        float result = instance.getEditAlarmLimitLowFault();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of setEditAlarmLimitLowAlert method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testSetEditAlarmLimitLowAlert() {
        System.out.println("setEditAlarmLimitLowAlert");
        setupChartMap();
        instance.getChartInfoMap().get("window1").setWorkOrderVO(new WorkOrderVO());
        instance.getChartInfoMap().get("window1").setSelectedAlarmLimitPoint(new PointVO());
        instance.setWindowId("window1");
        float value = 0.1F;
        instance.setEditAlarmLimitLowAlert(value);
        float result = instance.getEditAlarmLimitLowAlert();
        assertEquals(value, result, 0.0);
    }

    /**
     * Test of getEditAlarmLimitLowAlert method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testGetEditAlarmLimitLowAlert() {
        System.out.println("getEditAlarmLimitLowAlert");
        testSetEditAlarmLimitLowAlert();
        float expResult = 0.1F;
        float result = instance.getEditAlarmLimitLowAlert();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of setEditAlarmLimitHighAlert method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testSetEditAlarmLimitHighAlert() {
        System.out.println("setEditAlarmLimitHighAlert");
        setupChartMap();
        instance.getChartInfoMap().get("window1").setWorkOrderVO(new WorkOrderVO());
        instance.getChartInfoMap().get("window1").setSelectedAlarmLimitPoint(new PointVO());
        instance.setWindowId("window1");
        float value = 0.1F;
        instance.setEditAlarmLimitHighAlert(value);
        float result = instance.getEditAlarmLimitHighAlert();
        assertEquals(value, result, 0.0);
    }

    /**
     * Test of getEditAlarmLimitHighAlert method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testGetEditAlarmLimitHighAlert() {
        System.out.println("getEditAlarmLimitHighAlert");
        testSetEditAlarmLimitHighAlert();
        float expResult = 0.1F;
        float result = instance.getEditAlarmLimitHighAlert();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of setEditAlarmLimitHighFault method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testSetEditAlarmLimitHighFault() {
        System.out.println("setEditAlarmLimitHighFault");
        setupChartMap();
        instance.getChartInfoMap().get("window1").setWorkOrderVO(new WorkOrderVO());
        instance.getChartInfoMap().get("window1").setSelectedAlarmLimitPoint(new PointVO());
        instance.setWindowId("window1");
        float value = 0.1F;
        instance.setEditAlarmLimitHighFault(value);
        float result = instance.getEditAlarmLimitHighFault();
        assertEquals(value, result, 0.0);
    }

    /**
     * Test of getEditAlarmLimitHighFault method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testGetEditAlarmLimitHighFault() {
        System.out.println("getEditAlarmLimitHighFault");
        testSetEditAlarmLimitHighFault();
        float expResult = 0.1F;
        float result = instance.getEditAlarmLimitHighFault();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of setEditAlarmLimitDwell method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testSetEditAlarmLimitDwell() {
        System.out.println("setEditAlarmLimitDwell");
        int value = 4;
        setupChartMap();
        instance.getChartInfoMap().get("window1").setWorkOrderVO(new WorkOrderVO());
        instance.getChartInfoMap().get("window1").setSelectedAlarmLimitPoint(new PointVO());
        instance.setWindowId("window1");
        instance.setEditAlarmLimitDwell(value);
        int result = instance.getEditAlarmLimitDwell();
        assertEquals(value, result);
    }

    /**
     * Test of getEditAlarmLimitDwell method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testGetEditAlarmLimitDwell() {
        System.out.println("getEditAlarmLimitDwell");
        testSetEditAlarmLimitDwell();
        int expResult = 4;
        int result = instance.getEditAlarmLimitDwell();
        assertEquals(expResult, result);
    }

    /**
     * Test of setWorkOrderSiteName method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testSetWorkOrderSiteName() {
        System.out.println("setWorkOrderSiteName");
        setupChartMap();
        instance.getChartInfoMap().get("window1").setWorkOrderVO(new WorkOrderVO());
        instance.setWindowId("window1");
        String value = "SiteName1";
        instance.setWorkOrderSiteName(value);
        String result = instance.getWorkOrderSiteName();
        assertEquals(value, result);
    }

    /**
     * Test of getWorkOrderSiteName method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testGetWorkOrderSiteName() {
        System.out.println("getWorkOrderSiteName");
        testSetWorkOrderSiteName();
        String expResult = "SiteName1";
        String result = instance.getWorkOrderSiteName();
        assertEquals(expResult, result);
    }

    /**
     * Test of setWorkOrderAreaName method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testSetWorkOrderAreaName() {
        System.out.println("setWorkOrderAreaName");
        setupChartMap();
        instance.getChartInfoMap().get("window1").setWorkOrderVO(new WorkOrderVO());
        instance.setWindowId("window1");
        String value = "AreaName1";
        instance.setWorkOrderAreaName(value);
        String result = instance.getWorkOrderAreaName();
        assertEquals(value, result);
    }

    /**
     * Test of getWorkOrderAreaName method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testGetWorkOrderAreaName() {
        System.out.println("getWorkOrderAreaName");
        testSetWorkOrderAreaName();
        String expResult = "AreaName1";
        String result = instance.getWorkOrderAreaName();
        assertEquals(expResult, result);
    }

    /**
     * Test of setWorkOrderAssetName method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testSetWorkOrderAssetName() {
        System.out.println("setWorkOrderAssetName");
        setupChartMap();
        instance.getChartInfoMap().get("window1").setWorkOrderVO(new WorkOrderVO());
        instance.setWindowId("window1");
        String value = "AssetName1";
        instance.setWorkOrderAssetName(value);
        String result = instance.getWorkOrderAssetName();
        assertEquals(value, result);
    }

    /**
     * Test of getWorkOrderAssetName method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testGetWorkOrderAssetName() {
        System.out.println("getWorkOrderAssetName");
        testSetWorkOrderAssetName();
        String expResult = "AssetName1";
        String result = instance.getWorkOrderAssetName();
        assertEquals(expResult, result);
    }

    /**
     * Test of setWorkOrderPointLocationName method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testSetWorkOrderPointLocationName() {
        System.out.println("setWorkOrderPointLocationName");
        setupChartMap();
        instance.getChartInfoMap().get("window1").setWorkOrderVO(new WorkOrderVO());
        instance.setWindowId("window1");
        String value = "PointLocation1";
        instance.setWorkOrderPointLocationName(value);
        String result = instance.getWorkOrderPointLocationName();
        assertEquals(value, result);
    }

    /**
     * Test of getWorkOrderPointLocationName method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testGetWorkOrderPointLocationName() {
        System.out.println("getWorkOrderPointLocationName");
        testSetWorkOrderPointLocationName();
        String expResult = "PointLocation1";
        String result = instance.getWorkOrderPointLocationName();
        assertEquals(expResult, result);
    }

    /**
     * Test of setWorkOrderPointName method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testSetWorkOrderPointName() {
        System.out.println("setWorkOrderPointName");
        setupChartMap();
        instance.getChartInfoMap().get("window1").setWorkOrderVO(new WorkOrderVO());
        instance.setWindowId("window1");
        String value = "PointName1";
        instance.setWorkOrderPointName(value);
        String result = instance.getWorkOrderPointName();
        assertEquals(value, result);
    }

    /**
     * Test of getWorkOrderPointName method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testGetWorkOrderPointName() {
        System.out.println("getWorkOrderPointName");
        testSetWorkOrderPointName();
        String expResult = "PointName1";
        String result = instance.getWorkOrderPointName();
        assertEquals(expResult, result);
    }

    /**
     * Test of setWorkOrderAssetComponent method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testSetWorkOrderAssetComponent() {
        System.out.println("setWorkOrderAssetComponent");
        setupChartMap();
        instance.getChartInfoMap().get("window1").setWorkOrderVO(new WorkOrderVO());
        instance.setWindowId("window1");
        String value = "Asset Component 1";
        instance.setWorkOrderAssetComponent(value);
        String result = instance.getWorkOrderAssetComponent();
        assertEquals(value, result);
    }

    /**
     * Test of getWorkOrderAssetComponent method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testGetWorkOrderAssetComponent() {
        System.out.println("getWorkOrderAssetComponent");
        testSetWorkOrderAssetComponent();
        String expResult = "Asset Component 1";
        String result = instance.getWorkOrderAssetComponent();
        assertEquals(expResult, result);
    }

    /**
     * Test of setWorkOrderIssue method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testSetWorkOrderIssue() {
        System.out.println("setWorkOrderIssue");
        setupChartMap();
        instance.getChartInfoMap().get("window1").setWorkOrderVO(new WorkOrderVO());
        instance.setWindowId("window1");
        String value = "Battery Fail";
        instance.setWorkOrderIssue(value);
        String result = instance.getWorkOrderIssue();
        assertEquals(value, result);
    }

    /**
     * Test of getWorkOrderIssue method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testGetWorkOrderIssue() {
        System.out.println("getWorkOrderIssue");
        testSetWorkOrderIssue();
        String expResult = "Battery Fail";
        String result = instance.getWorkOrderIssue();
        assertEquals(expResult, result);
    }

    /**
     * Test of setWorkOrderAction method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testSetWorkOrderAction() {
        System.out.println("setWorkOrderAction");
        setupChartMap();
        instance.getChartInfoMap().get("window1").setWorkOrderVO(new WorkOrderVO());
        instance.setWindowId("window1");
        String value = "Batter change";
        instance.setWorkOrderAction(value);
        String result = instance.getWorkOrderAction();
        assertEquals(value, result);
    }

    /**
     * Test of getWorkOrderAction method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testGetWorkOrderAction() {
        System.out.println("getWorkOrderAction");
        testSetWorkOrderAction();
        String expResult = "Batter change";
        String result = instance.getWorkOrderAction();
        assertEquals(expResult, result);
    }

    /**
     * Test of setWorkOrderPriority method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testSetWorkOrderPriority() {
        System.out.println("setWorkOrderPriority");
        byte value = 1;
        setupChartMap();
        instance.getChartInfoMap().get("window1").setWorkOrderVO(new WorkOrderVO());
        instance.setWindowId("window1");
        instance.setWorkOrderPriority(value);
        byte result = instance.getWorkOrderPriority();
        assertEquals(value, result);
    }

    /**
     * Test of getWorkOrderPriority method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testGetWorkOrderPriority() {
        System.out.println("getWorkOrderPriority");
        testSetWorkOrderPriority();
        byte expResult = 1;
        byte result = instance.getWorkOrderPriority();
        assertEquals(expResult, result);
    }

    /**
     * Test of setWorkOrderDescription method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testSetWorkOrderDescription() {
        System.out.println("setWorkOrderDescription");
        setupChartMap();
        instance.getChartInfoMap().get("window1").setWorkOrderVO(new WorkOrderVO());
        instance.setWindowId("window1");
        String value = "Test Description";
        instance.setWorkOrderDescription(value);
        String result = instance.getWorkOrderDescription();
        assertEquals(value, result);
    }

    /**
     * Test of getWorkOrderDescription method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testGetWorkOrderDescription() {
        System.out.println("getWorkOrderDescription");
        testSetWorkOrderDescription();
        String expResult = "Test Description";
        String result = instance.getWorkOrderDescription();
        assertEquals(expResult, result);
    }

    /**
     * Test of setWorkOrderSelectedParamList method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testSetWorkOrderSelectedParamList() {
        System.out.println("setWorkOrderSelectedParamList");
        setupChartMap();
        instance.getChartInfoMap().get("window1").setWorkOrderVO(new WorkOrderVO());
        instance.setWindowId("window1");
        List<String> paramList = new ArrayList();
        paramList.add("Overall");
        instance.setWorkOrderSelectedParamList(paramList);
        List<String> result = instance.getWorkOrderSelectedParamList();
        assertEquals(paramList, result);
    }

    /**
     * Test of getWorkOrderSelectedParamList method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testGetWorkOrderSelectedParamList() {
        System.out.println("getWorkOrderSelectedParamList");
        setupChartMap();
        instance.getChartInfoMap().get("window1").setWorkOrderVO(new WorkOrderVO());
        instance.setWindowId("window1");
        List<String> paramList = new ArrayList();
        paramList.add("Overall");
        instance.setWorkOrderSelectedParamList(paramList);
        List<String> expResult = paramList;
        List<String> result = instance.getWorkOrderSelectedParamList();
        assertEquals(expResult, result);
    }

    /**
     * Test of setWorkOrderNotes method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testSetWorkOrderNotes() {
        System.out.println("setWorkOrderNotes");
        String value = "Test";
        setupChartMap();
        instance.getChartInfoMap().get("window1").setWorkOrderVO(new WorkOrderVO());
        instance.setWindowId("window1");
        instance.setWorkOrderNotes(value);
        assertEquals(value, instance.getChartInfoMap().get("window1").getWorkOrderVO().getNotes());
    }

    /**
     * Test of getWorkOrderNotes method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testGetWorkOrderNotes() {
        System.out.println("getWorkOrderNotes");
        setupChartMap();
        instance.setWindowId("window1");
        String expResult = null;
        String result = instance.getWorkOrderNotes();
        assertEquals(expResult, result);
    }

    /**
     * Test of getWorkOrderParamNameList method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testGetWorkOrderParamNameList() {
        System.out.println("getWorkOrderParamNameList");
        setupChartMap();
        instance.setWindowId("window1");
        List<String> expResult = new ArrayList();
        expResult.add("Overall");
        List<String> result = instance.getWorkOrderParamNameList();
        assertEquals(expResult, result);
    }

    /**
     * Test of isEditAlarmLimitLowFaultValidation method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testIsEditAlarmLimitLowFaultValidation() {
        System.out.println("isEditAlarmLimitLowFaultValidation");
        setupChartMap();
        instance.setWindowId("window1");
        boolean expResult = false;
        boolean result = instance.isEditAlarmLimitLowFaultValidation();
        assertEquals(expResult, result);
    }

    /**
     * Test of isEditAlarmLimitLowAlertValidation method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testIsEditAlarmLimitLowAlertValidation() {
        System.out.println("isEditAlarmLimitLowAlertValidation");
        setupChartMap();
        instance.setWindowId("window1");
        boolean expResult = false;
        boolean result = instance.isEditAlarmLimitLowAlertValidation();
        assertEquals(expResult, result);
    }

    /**
     * Test of isEditAlarmLimitHighAlertValidation method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testIsEditAlarmLimitHighAlertValidation() {
        System.out.println("isEditAlarmLimitHighAlertValidation");
        setupChartMap();
        instance.setWindowId("window1");
        boolean expResult = false;
        boolean result = instance.isEditAlarmLimitHighAlertValidation();
        assertEquals(expResult, result);
    }

    /**
     * Test of isEditAlarmLimitHighFaultValidation method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testIsEditAlarmLimitHighFaultValidation() {
        System.out.println("isEditAlarmLimitHighFaultValidation");
        setupChartMap();
        instance.setWindowId("window1");
        boolean expResult = false;
        boolean result = instance.isEditAlarmLimitHighFaultValidation();
        assertEquals(expResult, result);
    }

    private List<PointLocationVO> setUpPointLocationVOList() {
        List<PointLocationVO> pointLocationVOList = new ArrayList();

        PointLocationVO pointLocationVO = new PointLocationVO();
        pointLocationVO.setAlarmEnabled(false);
        pointLocationVO.setAreaId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        pointLocationVO.setAreaName("Test Area 1");
        pointLocationVO.setAssetId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        pointLocationVO.setAssetName("Test Asset 1");
        pointLocationVO.setBasestationPortNum((short) 702);
        pointLocationVO.setCustomerAccount(customerAccount);
        pointLocationVO.setDescription("Test Point Location Desc");
        pointLocationVO.setDeviceSerialNumber("BB001026");
        pointLocationVO.setFfSetNames(null);
        pointLocationVO.setPointList(new ArrayList());
        pointLocationVO.setPointLocationId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        pointLocationVO.setPointLocationName("Test Point Location 1");
        pointLocationVO.setRollDiameter(0.0f);
        pointLocationVO.setRollDiameterUnits("");
        pointLocationVO.setSampleInterval((short) 720);
        pointLocationVO.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        pointLocationVO.setSiteName("Test Site 1");
        pointLocationVO.setSpeedRatio(1.0f);
        pointLocationVO.setTachId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        pointLocationVO.setTachName("Test Tach Name 1");
        pointLocationVO.setTachReferenceUnits("0");
        pointLocationVOList.add(pointLocationVO);
        return pointLocationVOList;
    }

    private List<PointVO> setUpPointVOList() {
        List<PointVO> pointVOList = new ArrayList();
        PointVO pointVO = new PointVO();
        pointVO.setAlarmEnabled(false);
        pointVO.setAreaId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        pointVO.setAreaName("Test Area 1");
        pointVO.setAssetId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        pointVO.setAssetName("Test Asset 1");
        pointVO.setCustomerAccount(customerAccount);
        pointVO.setPointLocationId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        pointVO.setPointLocationName("Test Point Location 1");
        pointVO.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        pointVO.setSiteName("Test Site 1");
        pointVO.setAlSetId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        pointVO.setAlSetName("Test AlSet Name 1");
        pointVO.setApAlSetVOs(new ArrayList());
        pointVO.setApSetId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        pointVO.setApSetName("Test ApSet Name 1");
        pointVO.setPointId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        pointVO.setPointName("1 Acceleration");
        pointVO.setCustomized(false);
        pointVO.setDemodEnabled(false);
        pointVO.setDemodHighPass(0);
        pointVO.setDemodInterval(1);
        pointVO.setDemodLowPass(0);
        pointVO.setDisabled(true);
        pointVO.setDwell(123);
        pointVO.setFreqUnits("Hz");
        pointVO.setHighAlert(4.0f);
        pointVO.setHighAlertActive(true);
        pointVO.setHighFault(5.0f);
        pointVO.setHighFaultActive(true);
        pointVO.setLowAlert(1.0f);
        pointVO.setLowAlertActive(true);
        pointVO.setLowFault(2.0f);
        pointVO.setLowFaultActive(true);
        pointVO.setMaxFreq(720.0f);
        pointVO.setMinFreq(0.0f);
        pointVO.setParamAmpFactor("Peak");
        pointVO.setParamName("Overall");
        pointVO.setParamType("Total Spectral Energy");
        pointVO.setParamUnits("in/s/s");
        pointVO.setPeriod(0.0f);
        pointVO.setPointType("AC");
        pointVO.setResolution(1600);
        pointVO.setSampleRate(0.0f);
        pointVO.setSensorChannelNum(123456);
        pointVO.setSensorOffset(0.0f);
        pointVO.setSensorType("Acceleration");
        pointVO.setSensorUnits("Gs");
        pointVO.setfMax(720);
        pointVO.setApAlSetVOs(setUpApAlSetVos());
        pointVOList.add(pointVO);
        return pointVOList;
    }

    private WaveDataVO setUpWaveDataVO() {
        WaveDataVO waveDataVO = new WaveDataVO();
        waveDataVO.setCustomerAccount(customerAccount);
        waveDataVO.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        waveDataVO.setAreaId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        waveDataVO.setAssetId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        waveDataVO.setPointLocationId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        waveDataVO.setPointId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        waveDataVO.setSampleYear((short) 2023);
        waveDataVO.setSampleMonth((byte) 1);
        waveDataVO.setSampleTime(Instant.parse("2023-01-13T04:17:03Z"));
        waveDataVO.setSensitivityValue(50.0f);

        WaveformVO wfvo = new WaveformVO();
        wfvo.setSampleTime(Instant.parse("2023-01-13T04:17:03Z"));
        wfvo.setDeviceId("BB000001");
        wfvo.setChannelNum((byte) 1);
        wfvo.setSampleRateHz(2500.0f);
        wfvo.setSensitivityUnits("Gs");
        wfvo.setTachSpeed(0.0f);
        List<Float> samples = new ArrayList();
        String sample = "-0.0828744, 0.3522162, 1.0773672, 1.3052719, 1.4088649, 1.7300032, 1.7817997, 1.6782067, 1.5642545, 1.7714404, 1.9268298, 1.9475484, 2.154734, 2.165094, 2.3722801, 2.403358, 2.50695, 2.299764, 2.52767, 2.392998, 2.4344358, 2.465514, 2.5587482, 2.693418, 2.786652, 2.641622, 2.486232, 2.36192, 2.538028, 2.5173101, 2.289406, 2.496592, 2.444794, 2.424076, 2.123656, 2.0615, 2.030422, 1.9268298, 1.7300032, 1.6574881, 1.7817997, 1.6678474, 1.4295834, 1.3674276, 1.0048522, 0.3936534, -0.0103593, -0.621558, -1.0048522, -1.3363498, -1.6367695, -1.7196438, -1.7300032, -1.5849731, -1.7196438, -1.7507218, -1.8439554, -1.9786264, -2.113298, -2.3722801, -2.413718, -2.36192, -2.579466, -2.38264, -2.475872, -2.5691059, -2.538028, -2.5173101, -2.6209042, -2.693418, -2.7348561, -2.693418, -2.486232, -2.589826, -2.548388, -2.341202, -2.310124, -2.50695, -2.392998, -2.403358, -2.1754541, -1.9475484, -1.968267, -1.9268298, -1.8750334, -1.6264102, -1.8335962, -1.6471288, -1.5746136, -1.4192241, -0.82874405, -0.621558, -0.1657488, 0.621558, 1.0048522, 1.2016788, 1.6471288, 1.8232368, 1.5849731, 1.6056917, 1.6471288, 1.864674, 1.8439554, 2.113298, 2.102938, 2.21689, 2.3722801, 2.486232, 2.289406, 2.50695, 2.486232, 2.3204842, 2.538028, 2.7037783, 2.683058, 2.693418, 2.538028, 2.424076, 2.38264, 2.579466, 2.455154, 2.268686, 2.475872, 2.538028, 2.392998, 2.289406, 2.299764, 2.092578, 1.8232368, 1.8750334, 1.8335962, 1.7714404, 1.7403624, 1.792159, 1.6367695, 1.243116, 1.1084452, 0.5076058, 0.0207186, -0.549043, -1.0877266, -1.2120382, -1.7092847, -1.8025182, -1.6367695, -1.8128777, -1.6678474, -1.7507218, -1.9579078, -1.9579078, -2.237608, -2.196172, -2.4344358, -2.310124, -2.4344358, -2.486232, -2.3204842, -2.351562, -2.7037783, -2.52767, -2.7762918, -2.7348561, -2.8073702, -2.6623402, -2.50695, -2.5587482, -2.3722801, -2.465514, -2.610544, -2.392998, -2.5173101, -2.154734, -2.341202, -1.9061112, -2.092578, -1.9475484, -1.6989253, -1.9061112, -1.7507218, -1.6885661, -1.48138, -1.3570684, -1.0462894, -0.6111988, -0.0828744, 0.49724638, 0.85982203, 1.3881462, 1.3881462, 1.6471288, 1.6264102, 1.553895, 1.8750334, 1.8232368, 1.9786264, 1.9579078, 2.102938, 2.206532, 2.36192, 2.486232, 2.5587482, 2.413718, 2.548388, 2.4344358, 2.5587482, 2.496592, 2.641622, 2.6623402, 2.693418, 2.6623402, 2.444794, 2.330842, 2.3204842, 2.341202, 2.279046, 2.4344358, 2.465514, 2.36192, 2.113298, 2.1754541, 1.9786264, 1.7300032, 1.792159, 1.8543148, 1.8543148, 1.5746136, 1.5020986, 1.2949126, 0.932337, 0.34185702, -0.1553895, -0.4558092, -1.1188043, -1.243116, -1.6367695, -1.6367695, -1.6574881, -1.9061112, -1.6574881, -1.7196438, -2.051142, -2.144376, -2.07186, -2.351562, -2.403358, -2.4344358, -2.36192, -2.50695, -2.548388, -2.5587482, -2.538028, -2.7762918, -2.82809, -2.579466, -2.693418, -2.786652, -2.496592, -2.6312618, -2.403358, -2.310124, -2.310124, -2.3204842, -2.351562, -2.196172, -2.051142, -2.102938, -2.030422, -1.7092847, -1.8232368, -1.7300032, -1.8750334, -1.6678474, -1.5849731, -1.377787, -1.1498823, -0.5386836, 0.0207186, 0.48688722, 0.96341497, 1.1498823, 1.4088649, 1.5331764, 1.7196438, 1.5746136, 1.8025182, 1.6989253, 1.9579078, 2.134016, 2.1754541, 2.4344358, 2.299764, 2.475872, 2.486232, 2.258328, 2.403358, 2.5587482, 2.641622, 2.4344358, 2.538028, 2.81773, 2.589826, 2.465514, 2.486232, 2.413718, 2.310124, 2.548388, 2.341202, 2.38264, 2.3722801, 2.258328, 2.185812, 1.9371892, 1.9579078, 1.9061112, 1.5642545, 1.5746136, 1.7817997, 1.7610811, 1.5746136, 1.4399428, 1.067008, 0.517965, 0.1139523, -0.41437203, -0.9219778, -1.2327569, -1.5746136, -1.5642545, -1.8232368, -1.7403624, -1.6056917, -1.9475484, -1.8750334, -2.206532, -2.051142, -2.289406, -2.413718, -2.424076, -2.496592, -2.52767, -2.600184, -2.455154, -2.392998, -2.641622, -2.683058, -2.765934, -2.6623402, -2.755574, -2.7037783, -2.3722801, -2.5173101, -2.38264, -2.600184, -2.3722801, -2.52767, -2.36192, -2.092578, -2.040782, -2.07186, -1.7196438, -1.7714404, -1.7092847, -1.8439554, -1.7714404, -1.6678474, -1.3156313, -0.8494626, -0.4040128, 0.1139523, 0.3729348, 1.0773672, 1.4710206, 1.450302, 1.6471288, 1.864674, 1.6160507, 1.8335962, 1.6885661, 1.9889855, 1.9061112, 2.185812, 2.351562, 2.21689, 2.52767, 2.4344358, 2.299764, 2.465514, 2.403358, 2.6727, 2.589826, 2.5587482, 2.5173101, 2.475872, 2.444794, 2.496592, 2.455154, 2.548388, 2.268686, 2.403358, 2.486232, 2.475872, 2.247968, 2.154734, 2.07186, 2.0097039, 1.6678474, 1.8025182, 1.5746136, 1.5746136, 1.6367695, 1.5124577, 1.2223973, 0.82874405, 0.52832437, 0.1346709, -0.4040128, -0.89089984, -1.274194, -1.4088649, -1.6471288, -1.895752, -1.6056917, -1.8335962, -1.9268298, -2.0097039, -1.9889855, -2.237608, -2.247968, -2.38264, -2.38264, -2.38264, -2.36192, -2.392998, -2.610544, -2.444794, -2.714136, -2.5691059, -2.5587482, -2.7348561, -2.600184, -2.444794, -2.4344358, -2.589826, -2.486232, -2.403358, -2.341202, -2.52767, -2.403358, -2.310124, -2.1754541, -2.092578, -1.8335962, -1.6367695, -1.8750334, -1.8128777, -1.6367695, -1.5435357, -1.3985056, -0.9116184, -0.67335457, -0.1450302, 0.5801208, 1.0566486, 1.274194, 1.6885661, 1.5746136, 1.7507218, 1.8128777, 1.5849731, 1.6471288, 2.040782, 1.9268298, 2.134016, 2.392998, 2.38264, 2.475872, 2.258328, 2.444794, 2.50695, 2.486232, 2.38264, 2.5173101, 2.5587482, 2.755574, 2.6312618, 2.538028, 2.475872, 2.330842, 2.413718, 2.475872, 2.289406, 2.4344358, 2.38264, 2.299764, 2.310124, 1.9371892, 2.0200639, 1.7610811, 1.7403624, 1.6782067, 1.7092847, 1.792159, 1.6264102, 1.3156313, 0.8391034, 0.5386836, -0.0725151, -0.64227664, -1.0773672, -1.4295834, -1.4399428, -1.7714404, -1.6056917, -1.6264102, -1.792159, -1.7610811, -1.8439554, -1.9268298, -2.134016, -2.1754541, -2.50695, -2.413718, -2.538028, -2.538028, -2.38264, -2.538028, -2.641622, -2.548388, -2.52767, -2.7348561, -2.786652, -2.765934, -2.714136, -2.424076, -2.330842, -2.610544, -2.455154, -2.341202, -2.5173101, -2.206532, -2.330842, -2.206532, -1.8128777, -1.6782067, -1.9164705, -1.6885661, -1.8543148, -1.8128777, -1.553895, -1.48138, -1.0048522, -0.44544998, -0.10359301, 0.5697616, 1.0048522, 1.1913196, 1.5642545, 1.6782067, 1.8335962, 1.6989253, 1.7403624, 1.9371892, 1.7817997, 2.134016, 2.196172, 2.392998, 2.392998, 2.330842, 2.52767, 2.299764, 2.475872, 2.538028, 2.5691059, 2.538028, 2.714136, 2.52767, 2.786652, 2.579466, 2.392998, 2.3722801, 2.538028, 2.299764, 2.52767, 2.268686, 2.206532, 2.444794, 2.030422, 1.9475484, 2.051142, 1.7196438, 1.5849731, 1.8750334, 1.5642545, 1.8025182, 1.4399428, 1.3674276, 0.80802536, 0.4765278, 0.124311596, -0.41437203, -0.9426964, -1.3570684, -1.5953321, -1.5953321, -1.6989253, -1.792159, -1.6989253, -1.8750334, -2.08222, -2.196172, -2.0615, -2.196172, -2.424076, -2.475872, -2.310124, -2.465514, -2.3204842, -2.641622, -2.6623402, -2.589826, -2.5691059, -2.5587482, -2.548388, -2.5173101, -2.7244961, -2.641622, -2.3204842, -2.392998, -2.5173101, -2.3204842, -2.538028, -2.424076, -2.268686, -2.040782, -2.0200639, -1.9371892, -1.9268298, -1.7817997, -1.6056917, -1.8128777, -1.5228171, -1.4192241, -1.0773672, -0.6319174, -0.0828744, 0.3522162, 1.0877266, 1.2845533, 1.5020986, 1.7714404, 1.7403624, 1.6367695, 1.5746136, 1.8335962, 1.8853927, 2.0097039, 2.237608, 2.185812, 2.206532, 2.5173101, 2.341202, 2.38264, 2.330842, 2.589826, 2.413718, 2.475872, 2.693418, 2.7762918, 2.7244961, 2.641622, 2.5173101, 2.3204842, 2.341202, 2.5173101, 2.50695, 2.310124, 2.310124, 2.279046, 2.310124, 2.0200639, 1.9889855, 1.6367695, 1.5746136, 1.6264102, 1.6989253, 1.7610811, 1.3881462, 1.2949126, 0.9737742, 0.3729348, -0.124311596, -0.621558, -0.85982203, -1.4192241, -1.5953321, -1.7196438, -1.6782067, -1.8750334, -1.7817997, -1.6885661, -1.8543148, -2.051142, -2.1754541, -2.299764, -2.496592, -2.444794, -2.5587482, -2.289406, -2.475872, -2.3722801, -2.6727, -2.7348561, -2.6623402, -2.714136, -2.81773, -2.589826, -2.6727, -2.444794, -2.392998, -2.50695, -2.299764, -2.4344358, -2.247968, -2.196172, -2.36192, -1.9579078, -1.7817997, -1.864674, -1.8543148, -1.864674, -1.8439554, -1.7507218, -1.4710206, -1.4192241, -1.03593, -0.6319174, 0.124311596, 0.3522162, 0.79766625, 1.3052719, 1.5746136, 1.6989253, 1.792159, 1.553895, 1.5642545, 1.8853927, 1.8543148, 1.9786264, 2.310124, 2.299764, 2.424076, 2.455154, 2.3722801, 2.3204842, 2.289406, 2.5587482, 2.5587482, 2.5173101, 2.765934, 2.5691059, 2.548388, 2.444794, 2.52767, 2.351562, 2.589826, 2.289406, 2.258328, 2.424076, 2.475872, 2.22725, 2.0097039, 2.123656, 1.9268298, 1.9371892, 1.864674, 1.7300032, 1.7507218, 1.8025182, 1.5849731, 1.3052719, 1.0152115, 0.5697616, -0.1553895, -0.3832942, -1.1084452, -1.5020986, -1.6885661, -1.5331764, -1.7300032, -1.5849731, -1.7714404, -1.792159, -2.051142, -2.0200639, -2.330842, -2.351562, -2.548388, -2.475872, -2.38264, -2.5691059, -2.5173101, -2.6312618, -2.4344358, -2.486232, -2.7037783, -2.81773, -2.610544, -2.6727, -2.4344358, -2.465514, -2.475872, -2.579466, -2.50695, -2.465514, -2.538028, -2.247968, -2.123656, -1.999345, -1.8025182, -1.8232368, -1.7092847, -1.8439554, -1.6367695, -1.5435357, -1.6367695, -1.2949126, -0.9530556, -0.6629952, 0.1346709, 0.44544998, 0.9737742, 1.2949126, 1.6574881, 1.5331764, 1.5953321, 1.8335962, 1.864674, 1.7610811, 1.8335962, 2.154734, 2.030422, 2.330842, 2.392998, 2.299764, 2.496592, 2.5691059, 2.52767, 2.465514, 2.52767, 2.424076, 2.7762918, 2.6209042, 2.797012, 2.475872, 2.36192, 2.579466, 2.310124, 2.538028, 2.496592, 2.341202, 2.413718, 2.21689, 2.22725, 1.8853927, 1.9889855, 1.6885661, 1.7714404, 1.7817997, 1.5435357, 1.5331764, 1.6782067, 1.377787, 1.0048522, 0.6008394, 0.031077899, -0.4350906, -0.8494626, -1.3052719, -1.5953321, -1.5849731, -1.5953321, -1.6574881, -1.6264102, -1.7610811, -1.968267, -2.154734, -2.268686, -2.237608, -2.548388, -2.5691059, -2.579466, -2.600184, -2.6312618, -2.5173101, -2.610544, -2.6209042, -2.6727, -2.82809, -2.5587482, -2.6312618, -2.50695, -2.486232, -2.413718, -2.50695, -2.610544, -2.330842, -2.486232, -2.1754541, -2.134016, -2.0097039, -2.0615, -1.9268298, -1.8335962, -1.6678474, -1.7300032, -1.7196438, -1.5020986, -1.3156313, -0.89089984, -0.621558, -0.0414372, 0.3729348, 0.9012592, 1.2638345, 1.6885661, 1.5642545, 1.7403624, 1.6367695, 1.6989253, 1.8232368, 1.8750334, 1.9164705, 2.237608, 2.4344358, 2.4344358, 2.351562, 2.4344358, 2.424076, 2.289406, 2.5173101, 2.36192, 2.5587482, 2.465514, 2.5587482, 2.50695, 2.65198, 2.486232, 2.50695, 2.589826, 2.455154, 2.475872, 2.5173101, 2.444794, 2.351562, 2.051142, 1.9475484, 1.8439554, 1.8750334, 1.792159, 1.8439554, 1.8543148, 1.6471288, 1.4399428, 1.4192241, 0.8183848, 0.549043, -0.10359301, -0.621558, -0.9219778, -1.274194, -1.4917392, -1.5331764, -1.6574881, -1.8853927, -1.8439554, -1.864674, -1.8439554, -2.08222, -2.3204842, -2.392998, -2.237608, -2.424076, -2.579466, -2.5691059, -2.5173101, -2.455154, -2.6727, -2.52767, -2.786652, -2.714136, -2.600184, -2.465514, -2.548388, -2.50695, -2.413718, -2.444794, -2.38264, -2.310124, -2.444794, -2.310124, -2.08222, -2.206532, -1.7714404, -1.6782067, -1.6678474, -1.9061112, -1.8750334, -1.792159, -1.5331764, -1.2638345, -0.99449277, -0.55940217, 0.031077899, 0.5386836, 0.8494626, 1.3363498, 1.5849731, 1.7196438, 1.5435357, 1.8543148, 1.6367695, 1.7714404, 1.999345, 2.1754541, 2.185812, 2.279046, 2.3722801, 2.310124, 2.403358, 2.5587482, 2.341202, 2.5587482, 2.496592, 2.589826, 2.610544, 2.6727, 2.50695, 2.579466, 2.538028, 2.392998, 2.3204842, 2.38264, 2.268686, 2.299764, 2.5173101, 2.1754541, 2.051142, 2.113298, 1.7507218, 1.8543148, 1.7196438, 1.8128777, 1.6885661, 1.5435357, 1.4295834, 1.2845533, 0.932337, 0.41437203, -0.0103593, -0.6940732, -1.067008, -1.48138, -1.7300032, -1.6885661, -1.6367695, -1.6678474, -1.6989253, -1.6989253, -1.9061112, -1.9786264, -2.08222, -2.475872, -2.330842, -2.548388, -2.4344358, -2.444794, -2.38264, -2.50695, -2.693418, -2.7244961, -2.82809, -2.7348561, -2.82809, -2.6623402, -2.444794, -2.413718, -2.310124, -2.330842, -2.444794, -2.444794, -2.50695, -2.196172, -2.351562, -2.0200639, -1.792159, -1.9164705, -1.8025182, -1.7403624, -1.7403624, -1.5435357, -1.7092847, -1.48138, -1.1084452, -0.6837138, 0.0414372, 0.6008394, 0.9737742, 1.2949126, 1.6885661, 1.7714404, 1.6056917, 1.6678474, 1.8543148, 1.7817997, 1.8750334, 2.092578, 2.08222, 2.330842, 2.310124, 2.52767, 2.341202, 2.258328, 2.330842, 2.600184, 2.579466, 2.7037783, 2.486232, 2.65198, 2.6727, 2.65198, 2.50695, 2.5691059, 2.455154, 2.5173101, 2.455154, 2.52767, 2.3722801, 2.165094, 2.258328, 1.8853927, 1.8853927, 1.792159, 1.7817997, 1.8232368, 1.553895, 1.7817997, 1.4917392, 1.4088649, 0.9737742, 0.3522162, -0.031077899, -0.3832942, -1.1188043, -1.3052719, -1.6471288, -1.8335962, -1.8853927, -1.9061112, -1.7403624, -1.7092847, -1.8025182, -1.895752, -2.289406, -2.341202, -2.258328, -2.548388, -2.486232, -2.600184, -2.413718, -2.351562, -2.444794, -2.683058, -2.765934, -2.600184, -2.65198, -2.7244961, -2.7244961, -2.579466, -2.50695, -2.392998, -2.403358, -2.538028, -2.424076, -2.289406, -2.247968, -2.206532, -2.092578, -1.8543148, -1.9061112, -1.7714404, -1.6885661, -1.5953321, -1.5435357, -1.4399428, -1.0048522, -0.4247314, -0.1864674, 0.4765278, 0.99449277, 1.170601, 1.5228171, 1.4917392, 1.6782067, 1.6367695, 1.5849731, 1.9268298, 1.7507218, 2.154734, 2.030422, 2.289406, 2.279046, 2.3722801, 2.413718, 2.392998, 2.413718, 2.424076, 2.52767, 2.475872, 2.50695, 2.486232, 2.7037783, 2.538028, 2.496592, 2.600184, 2.3204842, 2.299764, 2.52767, 2.289406, 2.465514, 2.4344358, 2.154734, 2.030422, 1.8335962, 1.6574881, 1.6367695, 1.8750334, 1.8439554, 1.553895, 1.5020986, 1.3052719, 1.03593, 0.4558092, 0.0103593, -0.6837138, -0.82874405, -1.5020986, -1.5228171, -1.5953321, -1.5849731, -1.6885661, -1.864674, -1.7092847, -1.8128777, -2.206532, -2.351562, -2.279046, -2.237608, -2.403358, -2.610544, -2.50695, -2.5691059, -2.36192, -2.693418, -2.50695, -2.610544, -2.52767, -2.5691059, -2.6312618, -2.475872, -2.3722801, -2.424076, -2.444794, -2.50695, -2.413718, -2.22725, -2.299764, -2.040782, -2.154734, -2.030422, -1.8543148, -1.9164705, -1.7817997, -1.5849731, -1.5331764, -1.6264102, -1.2327569, -0.932337, -0.517965, -0.0414372, 0.4247314, 0.9841336, 1.2949126, 1.3881462, 1.7196438, 1.7507218, 1.7507218, 1.864674, 1.7817997, 2.0200639, 2.113298, 2.144376, 2.22725, 2.5173101, 2.403358, 2.330842, 2.5691059, 2.330842, 2.310124, 2.50695, 2.548388, 2.5691059, 2.745214, 2.548388, 2.444794, 2.36192, 2.444794, 2.3204842, 2.247968, 2.413718, 2.310124, 2.5173101, 2.123656, 2.102938, 2.040782, 1.8439554, 1.8025182, 1.6678474, 1.6782067, 1.6678474, 1.5953321, 1.3985056, 1.4295834, 1.0773672, 0.5904802, 0.0103593, -0.3729348, -0.82874405, -1.4192241, -1.4710206, -1.7092847, -1.8128777, -1.6782067, -1.9164705, -1.8750334, -1.7817997, -2.051142, -2.351562, -2.413718, -2.50695, -2.341202, -2.392998, -2.5587482, -2.310124, -2.475872, -2.465514, -2.600184, -2.52767, -2.6312618, -2.7762918, -2.5587482, -2.5173101, -2.5691059, -2.610544, -2.341202, -2.299764, -2.444794, -2.3722801, -2.3204842, -2.07186, -2.134016, -1.9268298, -1.6989253, -1.8232368, -1.7507218, -1.6989253, -1.6160507, -1.553895, -1.346709, -0.932337, -0.5801208, -0.0932337, 0.49724638, 0.89089984, 1.4295834, 1.6574881, 1.7507218, 1.7507218, 1.7507218, 1.8025182, 1.7714404, 1.8853927, 1.8750334, 2.3204842, 2.196172, 2.3722801, 2.475872, 2.4344358, 2.50695, 2.3204842, 2.486232, 2.475872, 2.693418, 2.797012, 2.589826, 2.65198, 2.4344358, 2.413718, 2.455154, 2.424076, 2.52767, 2.455154, 2.5587482, 2.330842, 2.258328, 2.206532, 2.1754541, 1.7403624, 1.7507218, 1.7610811, 1.7817997, 1.7403624, 1.5228171, 1.48138, 1.4710206, 1.067008, 0.6319174, -0.1450302, -0.517965, -0.9219778, -1.2120382, -1.5953321, -1.6160507, -1.6264102, -1.6989253, -1.6574881, -1.7196438, -1.8128777, -2.0097039, -2.08222, -2.154734, -2.538028, -2.5173101, -2.455154, -2.6209042, -2.475872, -2.403358, -2.496592, -2.5173101, -2.538028, -2.8073702, -2.745214, -2.486232, -2.548388, -2.36192, -2.341202, -2.610544, -2.424076, -2.589826, -2.413718, -2.392998, -2.310124, -2.185812, -1.895752, -1.8335962, -1.6989253, -1.7300032, -1.6574881, -1.7196438, -1.6989253, -1.3259904, -0.89089984, -0.41437203, 0.0828744, 0.5076058, 1.0877266, 1.3052719, 1.4917392, 1.8128777, 1.7714404, 1.5746136, 1.8543148, 1.9061112, 1.8335962, 1.9164705, 2.134016, 2.196172, 2.299764, 2.465514, 2.465514, 2.403358, 2.330842, 2.3204842, 2.6727, 2.683058, 2.7348561, 2.6209042, 2.589826, 2.50695, 2.475872, 2.351562, 2.3722801, 2.279046, 2.330842, 2.465514, 2.22725, 2.299764, 2.21689, 2.030422, 2.0615, 1.6885661, 1.8543148, 1.6471288, 1.6471288, 1.5849731, 1.5124577, 1.346709, 0.82874405, 0.4350906, 0.0725151, -0.517965, -0.99449277, -1.2016788, -1.7300032, -1.5849731, -1.6056917, -1.864674, -1.864674, -1.9889855, -2.0200639, -2.0200639, -2.258328, -2.475872, -2.475872, -2.444794, -2.52767, -2.299764, -2.538028, -2.351562, -2.641622, -2.765934, -2.755574, -2.610544, -2.7348561, -2.475872, -2.486232, -2.589826, -2.579466, -2.5587482, -2.4344358, -2.475872, -2.424076, -2.455154, -2.22725, -2.154734, -1.9061112, -1.7817997, -1.6264102, -1.7610811, -1.5849731, -1.5849731, -1.5020986, -1.377787, -1.0566486, -0.6837138, 0.1139523, 0.34185702, 0.79766625, 1.2016788, 1.6471288, 1.6885661, 1.6678474, 1.5953321, 1.5849731, 1.9164705, 1.7817997, 2.040782, 2.134016, 2.289406, 2.299764, 2.5587482, 2.455154, 2.50695, 2.392998, 2.600184, 2.6623402, 2.475872, 2.641622, 2.6623402, 2.786652, 2.745214, 2.424076, 2.424076, 2.341202, 2.3722801, 2.247968, 2.351562, 2.247968, 2.134016, 2.258328, 2.1754541, 1.7817997, 1.7196438, 1.8335962, 1.8335962, 1.6367695, 1.792159, 1.5642545, 1.3674276, 1.067008, 0.6319174, -0.031077899, -0.6940732, -1.067008, -1.274194, -1.4606614, -1.7092847, -1.7817997, -1.7403624, -1.7714404, -1.8025182, -1.7817997, -2.206532, -2.341202, -2.413718, -2.50695, -2.496592, -2.610544, -2.330842, -2.330842, -2.413718, -2.548388, -2.5587482, -2.641622, -2.610544, -2.81773, -2.714136, -2.413718, -2.6312618, -2.392998, -2.341202, -2.486232, -2.5691059, -2.475872, -2.392998, -2.247968, -2.051142, -1.7817997, -1.895752, -1.8853927, -1.9061112, -1.8335962, -1.6160507, -1.5124577, -1.1913196, -0.9012592, -0.4350906, 0.1139523, 0.652636, 0.7873068, 1.3674276, 1.3985056, 1.6574881, 1.7092847, 1.553895, 1.7610811, 1.8750334, 2.0097039, 1.9371892, 2.268686, 2.3722801, 2.455154, 2.4344358, 2.403358, 2.5691059, 2.392998, 2.589826, 2.641622, 2.589826, 2.765934, 2.8073702, 2.7037783, 2.589826, 2.444794, 2.330842, 2.330842, 2.5691059, 2.403358, 2.50695, 2.496592, 2.185812, 2.21689, 2.134016, 1.9268298, 1.8025182, 1.8750334, 1.6885661, 1.7196438, 1.5331764, 1.6471288, 1.450302, 1.0877266, 0.4040128, 0.1139523, -0.549043, -0.99449277, -1.4295834, -1.5849731, -1.6056917, -1.7092847, -1.6056917, -1.6678474, -1.9889855, -1.9475484, -1.9786264, -2.258328, -2.279046, -2.465514, -2.486232, -2.5173101, -2.403358, -2.444794, -2.424076, -2.52767, -2.7037783, -2.7348561, -2.8073702, -2.548388, -2.6209042, -2.4344358, -2.455154, -2.424076, -2.50695, -2.600184, -2.403358, -2.496592, -2.185812, -2.08222, -1.9889855, -2.040782, -1.9889855, -1.7610811, -1.9061112, -1.6367695, -1.5435357, -1.4917392, -1.346709, -1.1498823, -0.5697616, 0.124311596, 0.44544998, 0.9116184, 1.4295834, 1.4606614, 1.6160507, 1.7092847, 1.5746136, 1.6160507, 1.8232368, 1.8853927, 2.092578, 1.999345, 2.144376, 2.50695, 2.403358, 2.299764, 2.351562, 2.5587482, 2.486232, 2.683058, 2.486232, 2.538028, 2.7037783, 2.641622, 2.465514, 2.38264, 2.413718, 2.279046, 2.4344358, 2.279046, 2.392998, 2.206532, 2.21689, 2.310124, 1.8853927, 1.8750334, 1.9268298, 1.5849731, 1.6471288, 1.553895, 1.5435357, 1.6885661, 1.346709, 0.9012592, 0.41437203, -0.031077899, -0.5386836, -1.0980858, -1.3156313, -1.7196438, -1.5642545, -1.8128777, -1.7610811, -1.8853927, -1.7507218, -1.9268298, -2.0615, -2.102938, -2.279046, -2.330842, -2.36192, -2.5691059, -2.341202, -2.486232, -2.50695, -2.714136, -2.5587482, -2.693418, -2.82809, -2.5587482, -2.465514, -2.4344358, -2.6209042, -2.486232, -2.38264, -2.548388, -2.3204842, -2.330842, -2.403358, -2.051142, -1.999345, -1.792159, -1.8439554, -1.895752, -1.8335962, -1.6678474, -1.7714404, -1.6056917, -1.3674276, -1.1291637, -0.5697616, -0.1346709, 0.5697616, 1.0566486, 1.3156313, 1.4088649, 1.5435357, 1.8439554, 1.6574881, 1.8025182, 1.8543148, 1.9164705, 1.895752, 2.196172, 2.392998, 2.22725, 2.424076, 2.279046, 2.496592, 2.424076, 2.589826, 2.5173101, 2.745214, 2.6727, 2.745214, 2.7762918, 2.538028, 2.538028, 2.392998, 2.310124, 2.3204842, 2.299764, 2.310124, 2.465514, 2.258328, 2.268686, 2.0615, 2.040782, 1.8335962, 1.8025182, 1.7092847, 1.7610811, 1.6574881, 1.6678474, 1.3881462, 0.80802536, 0.5076058, -0.1450302, -0.4558092, -1.0255708, -1.2949126, -1.6264102, -1.7300032, -1.6989253, -1.6782067, -1.8335962, -1.864674, -2.040782, -2.030422, -2.289406, -2.22725, -2.38264, -2.579466, -2.579466, -2.424076, -2.600184, -2.496592, -2.486232, -2.765934, -2.5587482, -2.8073702, -2.641622, -2.5587482, -2.486232, -2.5587482, -2.496592, -2.3204842, -2.5691059, -2.455154, -2.38264, -2.258328, -2.206532, -2.102938, -1.7714404, -1.7300032, -1.8543148, -1.8750334, -1.864674, -1.5849731, -1.6989253, -1.274194, -0.9841336, -0.4350906, 0.0, 0.44544998, 0.8183848, 1.1498823, 1.4917392, 1.7714404, 1.6264102, 1.8543148, 1.8025182, 1.6264102, 1.9475484, 2.0200639, 2.092578, 2.4344358, 2.486232, 2.4344358, 2.5173101, 2.548388, 2.486232, 2.6312618, 2.6209042, 2.6623402, 2.486232, 2.538028, 2.5691059, 2.579466, 2.4344358, 2.50695, 2.3722801, 2.4344358, 2.268686, 2.392998, 2.237608, 2.21689, 2.113298, 1.895752, 1.7610811, 1.6782067, 1.5849731, 1.6471288, 1.6782067, 1.6574881, 1.4088649, 1.2845533, 1.0255708, 0.3936534, 0.1139523, -0.3936534, -0.8183848, -1.450302, -1.5642545, -1.8025182, -1.6574881, -1.7507218, -1.8543148, -1.8335962, -1.864674, -2.185812, -2.102938, -2.38264, -2.52767, -2.330842, -2.465514, -2.38264, -2.589826, -2.589826, -2.424076, -2.683058, -2.6312618, -2.765934, -2.5691059, -2.745214, -2.413718, -2.38264, -2.610544, -2.299764, -2.465514, -2.3722801, -2.538028, -2.206532, -2.196172, -2.051142, -1.9786264, -1.7403624, -1.6989253, -1.792159, -1.8439554, -1.5849731, -1.6367695, -1.3985056, -0.8391034, -0.5801208, 0.10359301, 0.4247314, 0.9116184, 1.3259904, 1.5228171, 1.6264102, 1.5435357, 1.5953321, 1.6782067, 1.9268298, 1.7507218, 2.0200639, 2.051142, 2.258328, 2.444794, 2.465514, 2.538028, 2.3722801, 2.3204842, 2.444794, 2.475872, 2.5173101, 2.5691059, 2.589826, 2.496592, 2.496592, 2.52767, 2.3722801, 2.299764, 2.3722801, 2.5587482, 2.3722801, 2.22725, 2.392998, 2.1754541, 1.9061112, 1.7403624, 1.6574881, 1.8439554, 1.6264102, 1.7817997, 1.792159, 1.5746136, 1.274194, 1.1188043, 0.5076058, -0.0725151, -0.6319174, -1.0048522, -1.1913196, -1.48138, -1.6264102, -1.7714404, -1.6678474, -1.7714404, -1.895752, -1.9786264, -1.968267, -2.07186, -2.268686, -2.258328, -2.4344358, -2.341202, -2.310124, -2.610544, -2.6209042, -2.5691059, -2.6623402, -2.589826, -2.6623402, -2.683058, -2.7762918, -2.6727, -2.6727, -2.6209042, -2.330842, -2.52767, -2.50695, -2.289406, -2.196172, -2.206532, -2.165094, -1.9164705, -1.7714404, -1.7610811, -1.8750334, -1.864674, -1.7610811, -1.4710206, -1.2223973, -1.170601, -0.64227664, 0.031077899, 0.3314976, 1.0462894, 1.2845533, 1.48138, 1.5020986, 1.8543148, 1.6574881, 1.7300032, 1.6678474, 1.7507218, 1.9061112, 2.0200639, 2.123656, 2.196172, 2.5587482, 2.5691059, 2.289406, 2.4344358, 2.424076, 2.52767, 2.7244961, 2.5173101, 2.6209042, 2.683058, 2.65198, 2.455154, 2.579466, 2.589826, 2.341202, 2.403358, 2.52767, 2.289406, 2.196172, 2.330842, 2.030422, 1.792159, 1.7300032, 1.7714404, 1.5746136, 1.6056917, 1.5228171, 1.6574881, 1.48138, 1.067008, 0.5801208, -0.1139523, -0.44544998, -0.8183848, -1.4192241, -1.5746136, -1.8128777, -1.6264102, -1.6056917, -1.8543148, -1.6885661, -1.8543148, -1.9579078, -2.299764, -2.341202, -2.50695, -2.36192, -2.279046, -2.38264, -2.403358, -2.548388, -2.538028, -2.5691059, -2.8073702, -2.610544, -2.8073702, -2.579466, -2.455154, -2.455154, -2.3204842, -2.548388, -2.351562, -2.310124, -2.50695, -2.4344358, -2.289406, -1.9061112, -1.7817997, -1.7507218, -1.6367695, -1.5953321, -1.7403624, -1.7817997, -1.5435357, -1.2845533, -1.1602416, -0.49724638, 0.0725151, 0.3314976, 0.8494626, 1.377787, 1.6160507, 1.7403624, 1.7714404, 1.6678474, 1.7092847, 1.6471288, 1.7403624, 2.185812, 2.07186, 2.1754541, 2.36192, 2.3204842, 2.403358, 2.341202, 2.50695, 2.403358, 2.5587482, 2.610544, 2.486232, 2.7244961, 2.50695, 2.683058, 2.538028, 2.4344358, 2.330842, 2.465514, 2.52767, 2.548388, 2.310124, 2.444794, 2.247968, 2.185812, 1.864674, 1.8439554, 1.7507218, 1.6264102, 1.7403624, 1.5228171, 1.4088649, 1.2120382, 0.8805406, 0.64227664, 0.051796503, -0.4558092, -1.0773672, -1.274194, -1.5953321, -1.8025182, -1.5746136, -1.792159, -1.6160507, -1.792159, -1.8025182, -2.154734, -2.351562, -2.341202, -2.424076, -2.279046, -2.3204842, -2.310124, -2.579466, -2.5587482, -2.641622, -2.6312618, -2.6623402, -2.745214, -2.745214, -2.52767, -2.714136, -2.465514, -2.3204842, -2.5173101, -2.3204842, -2.5587482, -2.36192, -2.455154, -2.21689, -1.9164705, -1.7817997, -1.7300032, -1.8232368, -1.7300032, -1.6471288, -1.5435357, -1.5435357, -1.5020986, -0.9426964, -0.5904802, -0.1346709, 0.621558, 0.8701812, 1.1602416, 1.5849731, 1.792159, 1.6885661, 1.6367695, 1.5746136, 1.6574881, 2.051142, 2.030422, 2.051142, 2.247968, 2.279046, 2.341202, 2.258328, 2.289406, 2.4344358, 2.610544, 2.548388, 2.6727, 2.786652, 2.5691059, 2.6312618, 2.5691059, 2.589826, 2.3722801, 2.496592, 2.258328, 2.268686, 2.310124, 2.330842, 2.3722801, 2.0615, 2.185812, 1.8128777, 1.7714404, 1.8853927, 1.6678474, 1.8128777, 1.5746136, 1.6678474, 1.4710206, 0.9116184, 0.4247314, 0.0414372, -0.5904802, -0.96341497, -1.450302, -1.5849731, -1.6056917, -1.5849731, -1.5849731, -1.8853927, -1.8128777, -1.9475484, -2.102938, -2.08222, -2.154734, -2.38264, -2.299764, -2.3204842, -2.600184, -2.310124, -2.403358, -2.475872, -2.755574, -2.7348561, -2.610544, -2.693418, -2.50695, -2.444794, -2.6727, -2.600184, -2.392998, -2.289406, -2.289406, -2.289406, -2.403358, -2.237608, -2.051142, -1.8750334, -1.968267, -1.9061112, -1.8025182, -1.8750334, -1.6367695, -1.7403624, -1.3259904, -1.0255708, -0.44544998, -0.0103593, 0.5076058, 1.0462894, 1.4295834, 1.6367695, 1.6056917, 1.5746136, 1.5435357, 1.6574881, 1.8232368, 1.9061112, 2.08222, 2.330842, 2.341202, 2.465514, 2.299764, 2.413718, 2.330842, 2.392998, 2.38264, 2.50695, 2.589826, 2.5587482, 2.641622, 2.5587482, 2.6209042, 2.5173101, 2.538028, 2.3204842, 2.36192, 2.4344358, 2.279046, 2.258328, 2.351562, 2.154734, 2.113298, 1.999345, 1.8335962, 1.7300032, 1.6264102, 1.7300032, 1.7403624, 1.6782067, 1.450302, 0.8391034, 0.5386836, -0.1139523, -0.6837138, -1.0462894, -1.2016788, -1.7300032, -1.7714404, -1.6989253, -1.7714404, -1.7817997, -1.8543148, -2.092578, -2.0200639, -2.040782, -2.247968, -2.299764, -2.299764, -2.475872, -2.424076, -2.3722801, -2.6312618, -2.5691059, -2.5587482, -2.548388, -2.538028, -2.838448, -2.683058, -2.52767, -2.6623402, -2.5691059, -2.413718, -2.548388, -2.279046, -2.455154, -2.247968, -2.102938, -1.9786264, -1.968267, -1.8853927, -1.6678474, -1.7507218, -1.792159, -1.6885661, -1.5228171, -1.48138, -1.0773672, -0.5076058, 0.062155798, 0.5386836, 0.8391034, 1.346709, 1.6264102, 1.6885661, 1.7300032, 1.7714404, 1.864674, 1.8232368, 1.9889855, 2.113298, 2.237608, 2.1754541, 2.22725, 2.496592, 2.279046, 2.289406, 2.413718, 2.5691059, 2.486232, 2.610544, 2.5173101, 2.6623402, 2.765934, 2.4344358, 2.38264, 2.38264, 2.475872, 2.538028, 2.5691059, 2.413718, 2.206532, 2.3722801, 2.310124, 2.144376, 1.9268298, 1.9371892, 1.6989253, 1.8232368, 1.8025182, 1.7714404, 1.3881462, 1.1913196, 0.9219778, 0.34185702, -0.0828744, -0.5386836, -0.9116184, -1.4088649, -1.5124577, -1.6056917, -1.7403624, -1.864674, -1.6264102, -1.7092847, -1.8025182, -2.123656, -2.08222, -2.444794, -2.455154, -2.3722801, -2.3722801, -2.465514, -2.465514, -2.475872, -2.4344358, -2.538028, -2.641622, -2.548388, -2.65198, -2.745214, -2.548388, -2.351562, -2.341202, -2.310124, -2.299764, -2.50695, -2.455154, -2.268686, -2.040782, -1.968267, -2.051142, -1.9268298, -1.7714404, -1.8439554, -1.7714404, -1.7403624, -1.553895, -1.3156313, -0.9012592, -0.4765278, -0.1450302, 0.44544998, 0.85982203, 1.2845533, 1.3881462, 1.6160507, 1.6056917, 1.8232368, 1.6264102, 1.7196438, 1.9786264, 2.051142, 2.310124, 2.196172, 2.36192, 2.465514, 2.538028, 2.5173101, 2.579466, 2.455154, 2.6312618, 2.7244961, 2.486232, 2.683058, 2.745214, 2.610544, 2.4344358, 2.330842, 2.5691059, 2.3204842, 2.279046, 2.413718, 2.3722801, 2.341202, 2.040782, 2.040782, 1.9371892, 1.7610811, 1.5746136, 1.7714404, 1.8128777, 1.7714404, 1.5849731, 1.243116, 1.1188043, 0.67335457, -0.1657488, -0.36257562, -0.96341497, -1.3985056, -1.48138, -1.5849731, -1.8750334, -1.5953321, -1.7403624, -1.8232368, -1.8025182, -2.21689, -2.341202, -2.475872, -2.289406, -2.475872, -2.3204842, -2.579466, -2.351562, -2.5587482, -2.7037783, -2.548388, -2.8073702, -2.538028, -2.6727, -2.52767, -2.4344358, -2.548388, -2.36192, -2.548388, -2.455154, -2.455154, -2.36192, -2.392998, -2.237608, -2.030422, -1.9475484, -1.7196438, -1.6989253, -1.864674, -1.6367695, -1.6989253, -1.7196438, -1.3985056, -0.9841336, -0.64227664, -0.1450302, 0.46616858, 0.89089984, 1.139523, 1.5435357, 1.5642545, 1.7610811, 1.553895, 1.6782067, 1.7403624, 1.9786264, 2.102938, 2.258328, 2.330842, 2.206532, 2.38264, 2.38264, 2.548388, 2.413718, 2.351562, 2.6209042, 2.579466, 2.7037783, 2.641622, 2.6727, 2.6727, 2.6623402, 2.310124, 2.52767, 2.351562, 2.3722801, 2.465514, 2.496592, 2.330842, 2.196172, 2.113298, 2.051142, 1.6471288, 1.792159, 1.7196438, 1.6471288, 1.7610811, 1.377787, 1.4710206, 1.1084452, 0.5076058, 0.1139523, -0.3729348, -0.932337, -1.2845533, -1.5228171, -1.8232368, -1.8543148, -1.6678474, -1.895752, -1.7092847, -1.8750334, -2.185812, -2.330842, -2.268686, -2.413718, -2.3722801, -2.5587482, -2.444794, -2.341202, -2.589826, -2.465514, -2.5173101, -2.641622, -2.641622, -2.600184, -2.7037783, -2.413718, -2.455154, -2.50695, -2.579466, -2.38264, -2.392998, -2.5587482, -2.206532, -2.330842, -1.9061112, -1.968267, -1.7610811, -1.6471288, -1.8750334, -1.7714404, -1.553895, -1.4710206, -1.346709, -1.0048522, -0.41437203, -0.051796503, 0.41437203, 0.9737742, 1.3674276, 1.3985056, 1.6885661, 1.5746136, 1.8128777, 1.8128777, 1.864674, 1.8543148, 1.9268298, 2.030422, 2.165094, 2.21689, 2.455154, 2.424076, 2.538028, 2.310124, 2.330842, 2.610544, 2.465514, 2.745214, 2.6312618, 2.589826, 2.548388, 2.413718, 2.5587482, 2.579466, 2.3204842, 2.3204842, 2.341202, 2.258328, 2.403358, 2.092578, 1.9475484, 1.8439554, 1.9371892, 1.8025182, 1.6056917, 1.8439554, 1.5435357, 1.5020986, 1.2949126, 1.0255708, 0.5386836, -0.1139523, -0.652636, -0.8701812, -1.1809602, -1.450302, -1.6160507, -1.7714404, -1.7610811, -1.8853927, -1.6989253, -2.08222, -2.102938, -2.341202, -2.351562, -2.237608, -2.444794, -2.455154, -2.465514, -2.600184, -2.600184, -2.424076, -2.714136, -2.6312618, -2.6727, -2.579466, -2.610544, -2.5691059, -2.455154, -2.50695, -2.299764, -2.403358, -2.52767, -2.279046, -2.475872, -2.040782, -1.9786264, -1.8232368, -1.7300032, -1.6056917, -1.8543148, -1.7507218, -1.6885661, -1.6367695, -1.2327569, -0.9116184, -0.4350906, 0.062155798, 0.5076058, 0.9116184, 1.4088649, 1.5642545, 1.5228171, 1.5953321, 1.7610811, 1.8232368, 1.8543148, 1.8543148, 1.9889855, 2.268686, 2.123656, 2.196172, 2.50695, 2.5173101, 2.38264, 2.3722801, 2.6209042, 2.496592, 2.7348561, 2.714136, 2.600184, 2.610544, 2.6209042, 2.6727, 2.6209042, 2.465514, 2.444794, 2.5691059, 2.268686, 2.299764, 2.392998, 2.0615, 2.1754541, 1.7507218, 1.7403624, 1.792159, 1.7300032, 1.7714404, 1.7196438, 1.6264102, 1.2327569, 0.9219778, 0.36257562, 0.1346709, -0.67335457, -1.0980858, -1.3985056, -1.5124577, -1.7196438, -1.7300032, -1.7817997, -1.8439554, -1.864674, -1.8853927, -1.895752, -2.123656, -2.279046, -2.475872, -2.486232, -2.5173101, -2.52767, -2.486232, -2.50695, -2.424076, -2.6727, -2.538028, -2.6312618, -2.797012, -2.786652, -2.589826, -2.413718, -2.6312618, -2.403358, -2.4344358, -2.424076, -2.299764, -2.258328, -2.185812, -2.134016, -2.0615, -1.6989253, -1.7092847, -1.6264102, -1.895752, -1.5435357, -1.5746136, -1.3052719, -1.1602416, -0.67335457, 0.051796503, 0.6319174, 0.9116184, 1.2638345, 1.3985056, 1.7817997, 1.8025182, 1.7300032, 1.7403624, 1.9268298, 1.7610811, 2.185812, 2.22725, 2.21689, 2.310124, 2.279046, 2.475872, 2.310124, 2.36192, 2.548388, 2.6623402, 2.6312618, 2.714136, 2.8073702, 2.579466, 2.444794, 2.579466, 2.330842, 2.589826, 2.52767, 2.548388, 2.5587482, 2.341202, 2.165094, 2.310124, 1.999345, 1.7817997, 1.7092847, 1.895752, 1.5849731, 1.6471288, 1.7610811, 1.4710206, 1.3156313, 0.9737742, 0.5076058, 0.124311596, -0.6111988, -1.0566486, -1.2120382, -1.7300032, -1.7817997, -1.8439554, -1.7092847, -1.6782067, -1.9268298, -1.792159, -1.9371892, -2.08222, -2.21689, -2.268686, -2.330842, -2.403358, -2.392998, -2.38264, -2.4344358, -2.548388, -2.65198, -2.65198, -2.5587482, -2.6209042, -2.589826, -2.475872, -2.610544, -2.50695, -2.392998, -2.36192, -2.392998, -2.299764, -2.330842, -2.144376, -2.134016, -1.864674, -1.9164705, -1.8750334, -1.7507218, -1.7817997, -1.8439554, -1.4917392, -1.1809602, -1.0773672, -0.6629952, -0.1346709, 0.6319174, 0.8701812, 1.4088649, 1.4088649, 1.6471288, 1.8335962, 1.6782067, 1.6264102, 1.6471288, 2.0200639, 2.0200639, 2.051142, 2.3722801, 2.196172, 2.38264, 2.36192, 2.258328, 2.341202, 2.424076, 2.465514, 2.52767, 2.7762918, 2.7348561, 2.765934, 2.600184, 2.5587482, 2.330842, 2.38264, 2.341202, 2.424076, 2.279046, 2.455154, 2.258328, 2.0200639, 2.165094, 2.0200639, 1.6885661, 1.8025182, 1.6885661, 1.8543148, 1.5124577, 1.6782067, 1.243116, 1.1291637, 0.4765278, 0.1139523, -0.3832942, -1.0877266, -1.1913196, -1.6160507, -1.6989253, -1.6989253, -1.6989253, -1.792159, -1.7196438, -2.0200639, -2.206532, -2.134016, -2.206532, -2.548388, -2.455154, -2.50695, -2.351562, -2.444794, -2.486232, -2.714136, -2.641622, -2.6623402, -2.65198, -2.6623402, -2.610544, -2.465514, -2.6209042, -2.351562, -2.424076, -2.403358, -2.330842, -2.392998, -2.455154, -2.3204842, -2.0200639, -1.999345, -1.8853927, -1.6264102, -1.8750334, -1.6471288, -1.6989253, -1.7300032, -1.4088649, -0.9116184, -0.44544998, -0.1139523, 0.4247314, 0.8805406, 1.2120382, 1.5124577, 1.7092847, 1.6989253, 1.864674, 1.6885661, 1.8025182, 1.7507218, 2.102938, 2.185812, 2.341202, 2.486232, 2.538028, 2.413718, 2.52767, 2.496592, 2.6312618, 2.413718, 2.5691059, 2.548388, 2.6312618, 2.693418, 2.579466, 2.589826, 2.5587482, 2.351562, 2.465514, 2.258328, 2.424076, 2.3204842, 2.330842, 2.030422, 1.9889855, 1.8439554, 1.9579078, 1.7300032, 1.553895, 1.8335962, 1.7507218, 1.377787, 1.1809602, 0.85982203, 0.6319174, -0.1139523, -0.4350906, -1.1498823, -1.4399428, -1.7196438, -1.6471288, -1.8439554, -1.7092847, -1.8128777, -1.8750334, -1.9889855, -2.051142, -2.102938, -2.268686, -2.299764, -2.5173101, -2.413718, -2.403358, -2.36192, -2.600184, -2.600184, -2.693418, -2.786652, -2.579466, -2.765934, -2.7348561, -2.5691059, -2.65198, -2.310124, -2.424076, -2.424076, -2.279046, -2.289406, -2.258328, -2.196172, -1.999345, -2.030422, -1.968267, -1.9268298, -1.7714404, -1.6471288, -1.6678474, -1.6782067, -1.5020986, -0.9116184, -0.6319174, -0.0207186, 0.46616858, 0.99449277, 1.3156313, 1.377787, 1.7714404, 1.8025182, 1.553895, 1.8232368, 1.7714404, 2.0097039, 2.165094, 2.165094, 2.196172, 2.392998, 2.3204842, 2.465514, 2.36192, 2.279046, 2.3722801, 2.52767, 2.465514, 2.745214, 2.600184, 2.683058, 2.475872, 2.5691059, 2.538028, 2.5173101, 2.5173101, 2.330842, 2.413718, 2.3204842, 2.289406, 2.102938, 2.144376, 1.8543148, 1.7300032, 1.5953321, 1.7403624, 1.5746136, 1.5331764, 1.5435357, 1.3985056, 0.9426964, 0.36257562, 0.031077899, -0.621558, -1.0462894, -1.4295834, -1.4192241, -1.5642545, -1.792159, -1.8232368, -1.7300032, -1.8335962, -1.7714404, -2.185812, -2.3204842, -2.413718, -2.4344358, -2.403358, -2.3204842, -2.424076, -2.538028, -2.413718, -2.475872, -2.786652, -2.745214, -2.745214, -2.579466, -2.7762918, -2.683058, -2.6209042, -2.5173101, -2.52767, -2.403358, -2.5173101, -2.3722801, -2.36192, -2.21689, -1.9475484, -1.8543148, -1.9371892, -1.8025182, -1.6056917, -1.8439554, -1.6885661, -1.4606614, -1.3363498, -1.0462894, -0.517965, -0.0414372, 0.3729348, 0.9012592, 1.2638345, 1.4088649, 1.6367695, 1.5746136, 1.7507218, 1.6056917, 1.8750334, 1.9268298, 1.9889855, 2.21689, 2.38264, 2.38264, 2.5587482, 2.413718, 2.475872, 2.341202, 2.579466, 2.683058, 2.6209042, 2.641622, 2.7244961, 2.5173101, 2.693418, 2.413718, 2.455154, 2.351562, 2.50695, 2.4344358, 2.392998, 2.351562, 2.237608, 2.113298, 1.864674, 1.999345, 1.9061112, 1.7714404, 1.6056917, 1.6471288, 1.5953321, 1.4399428, 1.377787, 1.0152115, 0.48688722, -0.0725151, -0.549043, -0.99449277, -1.48138, -1.6782067, -1.6574881, -1.6367695, -1.6678474, -1.7714404, -1.6678474, -1.7817997, -1.9786264, -2.279046, -2.3722801, -2.548388, -2.589826, -2.330842, -2.50695, -2.38264, -2.3722801, -2.579466, -2.641622, -2.745214, -2.7762918, -2.714136, -2.714136, -2.6209042, -2.392998, -2.5691059, -2.310124, -2.299764, -2.538028, -2.341202, -2.455154, -2.0615, -2.165094, -2.07186, -1.7610811, -1.6678474, -1.7507218, -1.6367695, -1.7092847, -1.6782067, -1.3363498, -1.1291637, -0.652636, -0.1553895, 0.5386836, 0.9116184, 1.3674276, 1.6160507, 1.6885661, 1.6367695, 1.792159, 1.6782067, 1.9164705, 1.895752, 1.968267, 2.154734, 2.134016, 2.289406, 2.289406, 2.424076, 2.403358, 2.5691059, 2.486232, 2.455154, 2.7244961, 2.6727, 2.5587482, 2.786652, 2.465514, 2.548388, 2.5587482, 2.392998, 2.5587482, 2.3722801, 2.258328, 2.341202, 2.403358, 2.113298, 1.9164705, 2.0097039, 1.7092847, 1.792159, 1.864674, 1.5331764, 1.5331764, 1.450302, 1.4710206, 0.8805406, 0.621558, -0.124311596, -0.5386836, -1.0462894, -1.346709, -1.7092847, -1.7507218, -1.5746136, -1.6056917, -1.7817997, -1.9475484, -2.0615, -2.22725, -2.030422, -2.351562, -2.289406, -2.36192, -2.38264, -2.610544, -2.351562, -2.341202, -2.455154, -2.745214, -2.683058, -2.6623402, -2.797012, -2.755574, -2.683058, -2.610544, -2.424076, -2.38264, -2.289406, -2.3204842, -2.4344358, -2.22725, -2.330842, -2.07186, -1.968267, -1.6885661, -1.8128777, -1.895752, -1.8335962, -1.6989253, -1.6885661, -1.3985056, -1.0980858, -0.4350906, 0.1139523, 0.5386836, 1.03593, 1.139523, 1.4710206, 1.5849731, 1.5331764, 1.6264102, 1.8853927, 1.7714404, 1.8439554, 1.9164705, 2.206532, 2.247968, 2.5173101, 2.247968, 2.279046, 2.5587482, 2.496592, 2.3722801, 2.6623402, 2.714136, 2.475872, 2.786652, 2.755574, 2.444794, 2.538028, 2.413718, 2.299764, 2.413718, 2.38264, 2.475872, 2.247968, 2.310124, 2.07186, 1.9061112, 1.9579078, 1.6471288, 1.5953321, 1.5435357, 1.8025182, 1.6782067, 1.6989253, 1.2327569, 0.932337, 0.3522162, -0.0725151, -0.5076058, -1.1291637, -1.2120382, -1.5124577, -1.6056917, -1.6989253, -1.6367695, -1.9061112, -1.7196438, -1.792159, -1.9786264, -2.3204842, -2.268686, -2.3204842, -2.392998, -2.38264, -2.455154, -2.341202, -2.589826, -2.496592, -2.6623402, -2.538028, -2.6727, -2.7762918, -2.486232, -2.589826, -2.4344358, -2.403358, -2.496592, -2.486232, -2.50695, -2.289406, -2.247968, -2.310124, -2.092578, -2.08222, -1.8439554, -1.9164705, -1.6782067, -1.6471288, -1.8128777, -1.450302, -1.2327569, -0.9530556, -0.5386836, 0.1346709, 0.36257562, 0.85982203, 1.3674276, 1.5435357, 1.6264102, 1.8128777, 1.8025182, 1.8439554, 1.6471288, 1.8750334, 2.040782, 2.030422, 2.36192, 2.237608, 2.403358, 2.392998, 2.4344358, 2.50695, 2.341202, 2.6623402, 2.5587482, 2.610544, 2.600184, 2.548388, 2.65198, 2.424076, 2.465514, 2.330842, 2.330842, 2.475872, 2.5587482, 2.465514, 2.196172, 2.08222, 2.165094, 1.895752, 1.9164705, 1.6885661, 1.6782067, 1.6782067, 1.792159, 1.3985056, 1.2949126, 0.9219778, 0.4765278, 0.1553895, -0.4350906, -0.9841336, -1.48138, -1.6989253, -1.7714404, -1.6678474, -1.7196438, -1.7610811, -1.8335962, -1.7610811, -2.165094, -2.185812, -2.392998, -2.475872, -2.403358, -2.289406, -2.6209042, -2.600184, -2.600184, -2.455154, -2.589826, -2.7762918, -2.7037783, -2.538028, -2.7037783, -2.52767, -2.5691059, -2.579466, -2.600184, -2.424076, -2.465514, -2.279046, -2.455154, -2.21689, -2.196172, -2.051142, -1.7817997, -1.6367695, -1.6782067, -1.5953321, -1.5953321, -1.6574881, -1.3985056, -0.85982203, -0.67335457, -0.051796503, 0.5697616, 0.8183848, 1.3570684, 1.6056917, 1.6160507, 1.5642545, 1.6574881, 1.5642545, 1.9061112, 1.7610811, 1.9786264, 2.07186, 2.310124, 2.50695, 2.5691059, 2.299764, 2.465514, 2.413718, 2.496592, 2.444794, 2.7348561, 2.797012, 2.7762918, 2.7762918, 2.6209042, 2.465514, 2.455154, 2.589826, 2.4344358, 2.4344358, 2.413718, 2.268686, 2.279046, 2.268686, 2.196172, 1.9475484, 1.9579078, 1.6678474, 1.7610811, 1.6471288, 1.792159, 1.5746136, 1.4088649, 0.8701812, 0.3522162, 0.0, -0.4350906, -0.9841336, -1.2223973, -1.6056917, -1.7714404, -1.6471288, -1.6160507, -1.6574881, -1.8439554, -2.030422, -2.21689, -2.247968, -2.351562, -2.237608, -2.589826, -2.413718, -2.610544, -2.3204842, -2.486232, -2.50695, -2.786652, -2.600184, -2.745214, -2.65198, -2.755574, -2.5173101, -2.538028, -2.610544, -2.455154, -2.455154, -2.3204842, -2.548388, -2.196172, -2.051142, -1.9786264, -1.9889855, -1.9475484, -1.6471288, -1.8335962, -1.6471288, -1.5849731, -1.7092847, -1.2638345, -0.9426964, -0.49724638, -0.1346709, 0.34185702, 0.8391034, 1.1809602, 1.3881462, 1.7507218, 1.6574881, 1.8025182, 1.8543148, 1.8543148, 2.051142, 1.9061112, 2.08222, 2.144376, 2.258328, 2.4344358, 2.548388, 2.392998, 2.496592, 2.600184, 2.3722801, 2.496592, 2.786652, 2.5691059, 2.641622, 2.486232, 2.36192, 2.52767, 2.289406, 2.455154, 2.5691059, 2.341202, 2.38264, 2.444794, 2.154734, 2.0097039, 2.051142, 1.9371892, 1.7610811, 1.6264102, 1.5331764, 1.6160507, 1.6471288, 1.274194, 1.0255708, 0.6008394, 0.031077899, -0.6629952, -1.0048522, -1.170601, -1.5331764, -1.7507218, -1.864674, -1.5849731, -1.6264102, -1.7714404, -1.8853927, -2.196172, -2.134016, -2.341202, -2.403358, -2.496592, -2.310124, -2.496592, -2.310124, -2.6727, -2.538028, -2.65198, -2.82809, -2.600184, -2.52767, -2.610544, -2.714136, -2.475872, -2.589826, -2.52767, -2.548388, -2.52767, -2.247968, -2.165094, -2.051142, -1.9579078, -1.7817997, -1.7610811, -1.7610811, -1.6264102, -1.6160507, -1.7507218, -1.7196438, -1.4399428, -0.85982203, -0.5076058, 0.062155798, 0.549043, 0.80802536, 1.4192241, 1.48138, 1.6367695, 1.7610811, 1.7403624, 1.7817997, 1.6471288, 1.7610811, 2.07186, 2.279046, 2.341202, 2.237608, 2.3722801, 2.36192, 2.330842, 2.5691059, 2.403358, 2.486232, 2.486232, 2.765934, 2.683058, 2.7244961, 2.610544, 2.50695, 2.4344358, 2.5173101, 2.538028, 2.5691059, 2.5173101, 2.310124, 2.289406, 2.07186, 2.144376, 1.9164705, 1.8232368, 1.5642545, 1.7196438, 1.6471288, 1.7196438, 1.6264102, 1.3570684, 0.932337, 0.652636, 0.1553895, -0.5801208, -1.0048522, -1.3156313, -1.6989253, -1.8128777, -1.6989253, -1.7714404, -1.7300032, -1.8232368, -2.030422, -1.9061112, -2.268686, -2.22725, -2.36192, -2.5173101, -2.310124, -2.4344358, -2.444794, -2.486232, -2.424076, -2.693418, -2.6623402, -2.6209042, -2.6312618, -2.538028, -2.600184, -2.579466, -2.465514, -2.465514, -2.444794, -2.413718, -2.299764, -2.310124, -2.07186, -1.9061112, -2.030422, -1.8335962, -1.7610811, -1.6574881, -1.8335962, -1.553895, -1.6678474, -1.2638345, -1.1291637, -0.52832437, -0.031077899, 0.55940217, 1.0773672, 1.2534754, 1.6574881, 1.6885661, 1.8128777, 1.6264102, 1.6056917, 1.6471288, 1.968267, 2.123656, 2.07186, 2.36192, 2.206532, 2.299764, 2.310124, 2.5587482, 2.5173101, 2.475872, 2.403358, 2.52767, 2.765934, 2.610544, 2.496592, 2.579466, 2.641622, 2.455154, 2.36192, 2.5173101, 2.424076, 2.486232, 2.330842, 2.299764, 2.040782, 2.113298, 1.9579078, 1.7610811, 1.792159, 1.8128777, 1.6471288, 1.4917392, 1.5228171, 1.2120382, 0.9116184, 0.6111988, -0.1553895, -0.3936534, -1.0566486, -1.4088649, -1.7300032, -1.553895, -1.6989253, -1.8025182, -1.6678474, -1.7196438, -2.040782, -1.9268298, -2.123656, -2.289406, -2.3204842, -2.330842, -2.538028, -2.475872, -2.589826, -2.341202, -2.486232, -2.714136, -2.714136, -2.589826, -2.8073702, -2.475872, -2.7244961, -2.4344358, -2.310124, -2.465514, -2.600184, -2.52767, -2.52767, -2.299764, -2.154734, -1.9475484, -1.8543148, -1.9475484, -1.7196438, -1.6471288, -1.7092847, -1.5642545, -1.5228171, -1.4295834, -0.85982203, -0.4247314, -0.1968267, 0.517965, 1.0566486, 1.3363498, 1.5953321, 1.5953321, 1.8128777, 1.6678474, 1.6885661, 1.8025182, 1.9475484, 2.092578, 2.0200639, 2.299764, 2.341202, 2.279046, 2.496592, 2.52767, 2.5691059, 2.3722801, 2.455154, 2.424076, 2.486232, 2.755574, 2.7037783, 2.589826, 2.6623402, 2.50695, 2.4344358, 2.548388, 2.475872, 2.50695, 2.3722801, 2.4344358, 2.0097039, 1.9579078, 2.030422, 1.9164705, 1.7817997, 1.7714404, 1.553895, 1.6264102, 1.4192241, 1.1913196, 0.9426964, 0.6111988, -0.1553895, -0.44544998";
        String[] array = sample.split(", ");
        for (int i = 0; i < array.length; ++i) {
            samples.add(Float.parseFloat(array[i]));
        }
        wfvo.setSamples(samples);
        waveDataVO.setWave(wfvo);

        return waveDataVO;
    }

    private List<FaultFrequenciesVO> setUpffSetList() {
        List<FaultFrequenciesVO> ffSetListTmp = new ArrayList();
        FaultFrequenciesVO ffvo = new FaultFrequenciesVO();
        ffvo.setCustomerAccount(customerAccount);
        ffvo.setFfSetName("GSR-Test-100723");
        ffvo.setFfName("Hz1");
        ffvo.setFfSetDesc("Created for testing");
        ffvo.setFfValue(30.0f);
        ffvo.setFfUnit("Hz");
        ffvo.setFfStartHarmonic(1);
        ffvo.setFfEndHarmonic(9);
        ffvo.setFfType("global");
        ffvo.setFfSetId(UUID.fromString("b9c92679-9376-40fc-9498-083c104edca6"));
        ffvo.setFfId(UUID.fromString("7318b307-f8dc-4a5c-888f-d9a2c59324ba"));
        ffSetListTmp.add(ffvo);

        ffvo = new FaultFrequenciesVO();
        ffvo.setCustomerAccount(customerAccount);
        ffvo.setFfSetName("GSR-Test-100723");
        ffvo.setFfName("RPM");
        ffvo.setFfSetDesc("Created for testing");
        ffvo.setFfValue(1800.0f);
        ffvo.setFfUnit("RPM");
        ffvo.setFfStartHarmonic(1);
        ffvo.setFfEndHarmonic(5);
        ffvo.setFfType("global");
        ffvo.setFfSetId(UUID.fromString("b9c92679-9376-40fc-9498-083c104edca6"));
        ffvo.setFfId(UUID.fromString("435f1a2d-1c81-4abf-a370-43dcecfde963"));
        ffSetListTmp.add(ffvo);

        ffvo = new FaultFrequenciesVO();
        ffvo.setCustomerAccount(customerAccount);
        ffvo.setFfSetName("GSR-Test-100723");
        ffvo.setFfName("Hz2");
        ffvo.setFfSetDesc("Created for testing");
        ffvo.setFfValue(1600.0f);
        ffvo.setFfUnit("Hz");
        ffvo.setFfStartHarmonic(1);
        ffvo.setFfEndHarmonic(8);
        ffvo.setFfType("global");
        ffvo.setFfSetId(UUID.fromString("b9c92679-9376-40fc-9498-083c104edca6"));
        ffvo.setFfId(UUID.fromString("2ef129fa-2d05-4dec-a84d-ed8f386ac1d0"));
        ffSetListTmp.add(ffvo);

        ffvo = new FaultFrequenciesVO();
        ffvo.setCustomerAccount(customerAccount);
        ffvo.setFfSetName("GSR-Test-100723");
        ffvo.setFfName("Orders");
        ffvo.setFfSetDesc("Created for testing");
        ffvo.setFfValue(2.5f);
        ffvo.setFfUnit("Orders");
        ffvo.setFfStartHarmonic(1);
        ffvo.setFfEndHarmonic(8);
        ffvo.setFfType("global");
        ffvo.setFfSetId(UUID.fromString("b9c92679-9376-40fc-9498-083c104edca6"));
        ffvo.setFfId(UUID.fromString("0361880c-d0d8-4eb7-91be-a436eb735c75"));
        ffSetListTmp.add(ffvo);
        return ffSetListTmp;
    }

    private List<TrendValuesVO> setUpTrendDataList() {
        List<TrendValuesVO> trendDataList = new ArrayList();
        TrendValuesVO trendValuesVO = new TrendValuesVO();
        trendValuesVO.setCustomerAccount(customerAccount);
        trendValuesVO.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        trendValuesVO.setAreaId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        trendValuesVO.setAssetId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        trendValuesVO.setPointLocationId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        trendValuesVO.setPointId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        trendValuesVO.setApSetId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        trendValuesVO.setParamName("Overall");
        trendValuesVO.setSampleTime(Instant.parse("2023-01-13T04:17:03Z"));
        trendValuesVO.setAlSetId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        trendValuesVO.setLabel(null);
        trendValuesVO.setNotes(null);
        trendValuesVO.setHighAlert(6.85f);
        trendValuesVO.setHighFault(7.25f);
        trendValuesVO.setLowAlert(1.0f);
        trendValuesVO.setLowFault(0.0f);
        trendValuesVO.setSensorType("Acceleration");
        trendValuesVO.setUnits("in/s");
        trendValuesVO.setValue(5.3533564f);
        trendDataList.add(trendValuesVO);

        return trendDataList;
    }

    private List<PointVO> setUpApAlByPointList() {
        List<PointVO> apAlByPointList = new ArrayList();

        PointVO pointVO = new PointVO();
        pointVO.setCustomerAccount(customerAccount);
        pointVO.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        pointVO.setAreaId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        pointVO.setAssetId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        pointVO.setPointLocationId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        pointVO.setPointId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        pointVO.setApSetId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        pointVO.setAlSetId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        pointVO.setPointName("1 Acceleration");
        pointVO.setPointType("AC");
        pointVO.setDisabled(false);
        pointVO.setSensorChannelNum(0);
        pointVO.setSensorOffset(0.0f);
        pointVO.setSensorSensitivity(0.0f);
        pointVO.setSensorType("Acceleration");
        pointVO.setSensorUnits(null);
        pointVO.setParamName("Overall");//Total Sub Sync,Total Non Sync, Overall
        pointVO.setDemodEnabled(false);
        pointVO.setDemodHighPass(0.0f);
        pointVO.setDemodLowPass(0.0f);
        pointVO.setDwell(3);
        pointVO.setfMax(977);
        pointVO.setAreaName(null);
        pointVO.setAssetName(null);
        pointVO.setPointLocationName(null);
        pointVO.setSiteName(null);
        pointVO.setAlSetName(null);
        pointVO.setApSetName(null);
        pointVO.setDemodInterval(0);
        pointVO.setParamType("Total Spectral Energy");//Total Sub Sync Energy,Total Non Sync Energy,Total Spectral Energy
        pointVO.setAlarmEnabled(false);
        pointVO.setFreqUnits("Hz");//RPM,RPM,Hz
        pointVO.setHighAlert(6.85f);
        pointVO.setHighAlertActive(true);
        pointVO.setHighFault(7.25f);
        pointVO.setHighFaultActive(true);
        pointVO.setCustomized(false);
        pointVO.setLowAlert(1.0f);
        pointVO.setLowAlertActive(true);
        pointVO.setLowFault(0.0f);
        pointVO.setLowFaultActive(true);
        pointVO.setMaxFreq(0.0f);
        pointVO.setMinFreq(0.0f);
        pointVO.setParamAmpFactor("Peak");// RMS, RMS, Peak
        pointVO.setParamUnits("in/s");//Gs, in/s/s, in/s
        pointVO.setPeriod(1.6384f);
        pointVO.setResolution(1600);
        pointVO.setSampleRate(2500.0f);
        pointVO.setApAlSetVOs(setUpApAlSetVos());

        apAlByPointList.add(pointVO);

        return apAlByPointList;
    }

    private List<ApAlSetVO> setUpApAlSetVos() {
        List<ApAlSetVO> apAlSetVOs = new ArrayList();
        ApAlSetVO apAlSetVO = new ApAlSetVO();
        apAlSetVO.setCustomerAccount("77777");
        apAlSetVO.setSiteName("Test Site 1");
        apAlSetVO.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        apAlSetVO.setApSetName("Test ApSet Name 1");
        apAlSetVO.setApSetId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        apAlSetVO.setAlSetName("Test AlSet Name 1");
        apAlSetVO.setAlSetId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        apAlSetVO.setParamName("Overall");
        apAlSetVO.setSensorType("Acceleration");
        apAlSetVO.setFmax(720);
        apAlSetVO.setResolution(1600);
        apAlSetVO.setPeriod(0.0f);
        apAlSetVO.setSampleRate(0.0f);
        apAlSetVO.setDemod(false);
        apAlSetVO.setDemodHighPass(0.0f);
        apAlSetVO.setDemodLowPass(0.0f);
        apAlSetVO.setParamType("Total Spectral Energy");
        apAlSetVO.setFrequencyUnits("Hz");
        apAlSetVO.setParamUnits("in/s/s");
        apAlSetVO.setParamAmpFactor("Peak");
        apAlSetVO.setMinFrequency(0.0f);
        apAlSetVO.setMaxFrequency(720.0f);
        apAlSetVO.setDwell(123);
        apAlSetVO.setLowFaultActive(true);
        apAlSetVO.setLowAlertActive(true);
        apAlSetVO.setHighAlertActive(true);
        apAlSetVO.setHighFaultActive(true);
        apAlSetVO.setLowFault(2.0f);
        apAlSetVO.setLowAlert(1.0f);
        apAlSetVO.setHighAlert(4.0f);
        apAlSetVO.setHighFault(5.0f);
        apAlSetVO.setSelectedAlSet(null);
        apAlSetVOs.add(apAlSetVO);
        return apAlSetVOs;
    }

    /**
     * Test of getAlarmLimitPlotLines method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testGetAlarmLimitPlotLines() {
        System.out.println("testGetAlarmLimitPlotLines");
        PointVO pointVO = new PointVO();
        pointVO.setLowAlert(1.0f);
        pointVO.setLowAlertActive(true);
        String expResult = ",\"plotLines\": [{ \"color\": \"orange\", \"width\": 2, \"dashStyle\": \"dash\", \"value\": 1.0, \"label\": { \"text\": \"null\", \"align\": \"right\"} },]";
        String result = instance.getAlarmLimitPlotLines(pointVO);
        assertEquals(expResult, result);
    }

    /**
     * Test of convertTime method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testConvertTime() {
        System.out.println("testConvertTime");
        float expResult = 1000.0f;
        float result = instance.convertTime("s", "ms");
        assertEquals(expResult, result, 0.01);
    }

    /**
     * Test of convertFrequency method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testConvertFrequency() {
        System.out.println("testConvertFrequency");
        float expResult = 60.0f;
        float result = instance.convertFrequency("Hz", "RPM", 0.0f);
        assertEquals(expResult, result, 0.01);
    }

    /**
     * Test of getFrequecyPlotLines method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testGetFrequecyPlotLines() {
        System.out.println("testGetFrequecyPlotLines");
        List<FaultFrequenciesVO> ffSetVOList = new ArrayList<>();
        FaultFrequenciesVO ffvo = new FaultFrequenciesVO();
        ffvo.setCustomerAccount(customerAccount);
        ffvo.setFfSetName("GSR-Test-100723");
        ffvo.setFfName("Hz1");
        ffvo.setFfSetDesc("Created for testing");
        ffvo.setFfValue(30.0f);
        ffvo.setFfUnit("Hz");
        ffvo.setFfStartHarmonic(1);
        ffvo.setFfEndHarmonic(9);
        ffvo.setFfType("global");
        ffvo.setFfSetId(UUID.fromString("b9c92679-9376-40fc-9498-083c104edca6"));
        ffvo.setFfId(UUID.fromString("7318b307-f8dc-4a5c-888f-d9a2c59324ba"));
        ffSetVOList.add(ffvo);
        GraphDataVO graphDataVO = new GraphDataVO();
        graphDataVO.setSelectedFfSetId(UUID.fromString("b9c92679-9376-40fc-9498-083c104edca6"));
        graphDataVO.setFrequencyList(ffSetVOList);
        graphDataVO.setSelectedTimeUnit("s");
        graphDataVO.setSelectedAmplitudeUnit("Gs");
        graphDataVO.setSelectedFrequencyUnit("Hz");
        graphDataVO.setSelectedAmplitudeUnit("m/s/s");
        graphDataVO.setSelectedAmpFactor("RMS");
        graphDataVO.setTachSpeedUnit("RPM");
        Map<String, String> ffMarkerMap = new HashMap<>();
        ffMarkerMap.put("Hz1", "DashDot-Red");
        graphDataVO.setFfMarkerMap(ffMarkerMap);
        String expResult = "[{ \"color\": \"Red\", \"width\": 2, \"dashStyle\": \"DashDot\", \"value\": 30.0},]";
        String result = instance.getFrequecyPlotLines(ffSetVOList, graphDataVO, 0.0f);
        assertEquals(expResult, result);
    }

    /**
     * Test of isUnitsConversionRequired method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testIsUnitsConversionRequired01() {
        System.out.println("testIsUnitsConversionRequired01");
        ChartInfoVO ciVo = new ChartInfoVO();
        ciVo.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        ciVo.setSiteName("Test Site 1");
        ciVo.setPointType("AC");
        ciVo.setPointLocationName("SGA-1");
        ciVo.setPointName("Ultrasonic");
        ciVo.setSelectedApAlByPoint(setUpApAlByPointList().get(0));
        ciVo.setApAlByPointList(setUpApAlByPointList());
        List<String> paramNameList = new ArrayList();
        paramNameList.add("Overall");
        ciVo.setParamNameList(paramNameList);
        GraphDataVO graphDataVO = new GraphDataVO();
        graphDataVO.setSelectedFfSetId(UUID.fromString("b9c92679-9376-40fc-9498-083c104edca6"));
        graphDataVO.setSelectedTimeUnit("ms");
        graphDataVO.setSelectedAmplitudeUnit("Gs");
        graphDataVO.setSelectedFrequencyUnit("Hz");
        graphDataVO.setSelectedAmplitudeUnit("m/s/s");
        graphDataVO.setSelectedAmpFactor("RMS");
        ciVo.setWaveGraphData(graphDataVO);
        String graphType = "wave";
        String axisType = "x";
        boolean expResult = true;
        boolean result = instance.isUnitsConversionRequired(ciVo, graphType, axisType);
        assertEquals(expResult, result);
    }

    /**
     * Test of isUnitsConversionRequired method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testIsUnitsConversionRequired02() {
        System.out.println("testIsUnitsConversionRequired02");
        ChartInfoVO ciVo = new ChartInfoVO();
        ciVo.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        ciVo.setSiteName("Test Site 1");
        ciVo.setPointType("AC");
        ciVo.setPointLocationName("SGA-1");
        ciVo.setPointName("Ultrasonic");
        ciVo.setSelectedApAlByPoint(setUpApAlByPointList().get(0));
        ciVo.setApAlByPointList(setUpApAlByPointList());
        List<String> paramNameList = new ArrayList();
        paramNameList.add("Overall");
        ciVo.setParamNameList(paramNameList);
        GraphDataVO graphDataVO = new GraphDataVO();
        graphDataVO.setSelectedFfSetId(UUID.fromString("b9c92679-9376-40fc-9498-083c104edca6"));
        graphDataVO.setSelectedTimeUnit("s");
        graphDataVO.setSelectedAmplitudeUnit("Gs");
        graphDataVO.setSelectedFrequencyUnit("Hz");
        graphDataVO.setSelectedAmplitudeUnit("m/s/s");
        graphDataVO.setSelectedAmpFactor("RMS");
        ciVo.setWaveGraphData(graphDataVO);
        String graphType = "wave";
        String axisType = "x";
        boolean expResult = false;
        boolean result = instance.isUnitsConversionRequired(ciVo, graphType, axisType);
        assertEquals(expResult, result);
    }

    /**
     * Test of isAmpFactorConversionRequired method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testIsAmpFactorConversionRequired() {
        System.out.println("testIsAmpFactorConversionRequired");
        ChartInfoVO ciVo = new ChartInfoVO();
        ciVo.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        ciVo.setSiteName("Test Site 1");
        ciVo.setPointType("AC");
        ciVo.setPointLocationName("SGA-1");
        ciVo.setPointName("Ultrasonic");
        ciVo.setSelectedApAlByPoint(setUpApAlByPointList().get(0));
        ciVo.setApAlByPointList(setUpApAlByPointList());
        List<String> paramNameList = new ArrayList();
        paramNameList.add("Overall");
        ciVo.setParamNameList(paramNameList);
        ciVo.setSpectrumGraphData(new GraphDataVO());
        boolean expResult = true;
        boolean result = instance.isAmpFactorConversionRequired(ciVo);
        assertEquals(expResult, result);
    }

    /**
     * Test of setSpectrumLogYaxis method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testSetSpectrumLogYaxis() {
        System.out.println("testSetSpectrumLogYaxis");
        ChartInfoVO ciVo = new ChartInfoVO();
        ciVo.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        ciVo.setSiteName("Test Site 1");
        ciVo.setPointType("AC");
        ciVo.setPointLocationName("SGA-1");
        ciVo.setPointName("Ultrasonic");
        ciVo.setSelectedApAlByPoint(setUpApAlByPointList().get(0));
        ciVo.setApAlByPointList(setUpApAlByPointList());
        List<String> paramNameList = new ArrayList();
        paramNameList.add("Overall");
        ciVo.setParamNameList(paramNameList);
        ciVo.setSpectrumGraphData(new GraphDataVO());
        boolean expResult = true;
        instance.setSpectrumLogYaxis(ciVo);
        assertEquals(expResult, ciVo.isSpectrumLogYAxis());
    }

    /**
     * Test of getUnitforSensorType method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testGetUnitforSensorType01() {
        System.out.println("testGetUnitforSensorType01");
        ChartInfoVO ciVo = new ChartInfoVO();
        ciVo.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        ciVo.setSiteName("Test Site 1");
        ciVo.setPointType("AC");
        ciVo.setPointLocationName("SGA-1");
        ciVo.setPointName("Ultrasonic");
        ciVo.setSelectedApAlByPoint(setUpApAlByPointList().get(0));
        ciVo.setApAlByPointList(setUpApAlByPointList());
        List<String> paramNameList = new ArrayList();
        paramNameList.add("Overall");
        ciVo.setParamNameList(paramNameList);
        ciVo.setSpectrumGraphData(new GraphDataVO());
        String expResult = "in/s";
        String result = instance.getUnitforSensorType("acceleration", "wave");
        assertEquals(expResult, result);
    }

    /**
     * Test of getUnitforSensorType method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testGetUnitforSensorType02() {
        System.out.println("testGetUnitforSensorType02");
        ChartInfoVO ciVo = new ChartInfoVO();
        ciVo.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        ciVo.setSiteName("Test Site 1");
        ciVo.setPointType("AC");
        ciVo.setPointLocationName("SGA-1");
        ciVo.setPointName("Ultrasonic");
        ciVo.setSelectedApAlByPoint(setUpApAlByPointList().get(0));
        ciVo.setApAlByPointList(setUpApAlByPointList());
        List<String> paramNameList = new ArrayList();
        paramNameList.add("Overall");
        ciVo.setParamNameList(paramNameList);
        ciVo.setSpectrumGraphData(new GraphDataVO());
        String expResult = "in/s/s";
        String result = instance.getUnitforSensorType("acceleration", "spectrum");
        assertEquals(expResult, result);
    }

    /**
     * Test of getTrendChartJson method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testGetTrendChartJson01() {
        System.out.println("testGetTrendChartJson01");
        String pointType = "";
        String values = "";
        String yAxisTitle = "";
        String plotLines = "";
        String trendGraphTitle = "";
        int selectedDataPointIndex = 0;
        float yAxisUpperBound = 0f;
        String paramAmpFactor = "";
        String expResult = "{ \"chart\" : { marginTop:70, \"zoomType\" : \"x\" , \"height\": 280,events: { load() { updateRemoveWindowDetails(), this.series[0].points[0].select() } } }, time: { timezoneOffset: 0 }, \"series\" : [ { lineWidth: 1, states: { hover: { lineWidth: 1 }  }, \"data\" :, color: \"#003399\", \"name\" : \" \", cropThreshold: 9999 } ], plotOptions: { series: {  point: { events: { select: function () {var date = new Date(this.x).toLocaleString(\"en-US\", {timeZone: \"GMT\", weekday:\"short\", year:\"numeric\", month:\"short\", day:\"numeric\", hour:\"2-digit\", minute:\"2-digit\", second:\"2-digit\", hourCycle: 'h23'}); var text = date + '</br>' + Highcharts.numberFormat(this.y, 4); var chart = this.series.chart; if (!chart.lbl) { chart.lbl = chart.renderer.label(text, 75, 7).attr({ padding: 10,r: 5,zIndex: 0 }).css({color: 'red'}).add();} else {chart.lbl.attr({text: text}); } } , click: function (event) { this.select(!this.selected, false); } } }, marker: { enabled: true, radius: 0.1,  states: { select: { fillColor: 'red', lineWidth: 0, radius: 4  }, hover: { fillColor: '#4da6ff', lineWidth: 1, radius: 6 } } } } }, \"title\" : { \"text\" : \"  \" },exporting: appendExportButtonsToTrenGraph(),  \"legend\" : { \"enabled\" : false },\"yAxis\" : [ { min:0, max: 0.0,  \"title\" : { \"text\" : \"  \" }  } ],\"xAxis\": [{ \"title\": { \"text\": \" null \" }, \"type\": \"datetime\" } ],tooltip: { positioner: function() { return { x: this.chart.plotSizeX-135, y: this.chart.plotTop-45 }; }, formatter() { return new Date(this.x).toLocaleString(\"en-US\", { timeZone: \"GMT\", weekday:\"short\", year:\"numeric\", month:\"short\", day:\"numeric\", hour:\"2-digit\", minute:\"2-digit\", second:\"2-digit\", hourCycle: 'h23'}) +'</br>' + Highcharts.numberFormat(this.y, 4)},shared: true, shadow: false, borderWidth: 0, backgroundColor: \"rgba(255,255,255,0.8)\" }  ,navigator: { top: 35, margin: 10, enabled: true, series: {  dataGrouping: { enabled: false } } ,xAxis: { type: 'linear' } }, scrollbar: { enabled: true }}";
        instance.setTimestamp("1698351117000");
        String result = instance.getTrendChartJson(pointType, values, yAxisTitle, trendGraphTitle, plotLines, selectedDataPointIndex, yAxisUpperBound, paramAmpFactor);
        assertNotEquals(expResult, result);
    }

    /**
     * Test of getTrendChartJson method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testGetTrendChartJson02() {
        System.out.println("testGetTrendChartJson02");
        String pointType = "AC";
        String values = "[{1,2.3},{2,1.3},{3,1.5},{4,5.3}]";
        String yAxisTitle = "in/s";
        String plotLines = "{}";
        String trendGraphTitle = "Overall";
        int selectedDataPointIndex = 4;
        float yAxisUpperBound = 20f;
        String paramAmpFactor = "RMS";
        String expResult = "{ \"chart\" : { marginTop:70, \"zoomType\" : \"x\" , \"height\": 280,events: { load() { updateRemoveWindowDetails(), this.series[0].points[4].select() } } }, time: { timezoneOffset: 0 }, \"series\" : [ { lineWidth: 1, states: { hover: { lineWidth: 1 }  }, \"data\" :[{1,2.3},{2,1.3},{3,1.5},{4,5.3}], color: \"#003399\", \"name\" : \" \", cropThreshold: 9999 } ], plotOptions: { series: {  point: { events: { select: function () {var date = new Date(this.x).toLocaleString(\"en-US\", {timeZone: \"GMT\", weekday:\"short\", year:\"numeric\", month:\"short\", day:\"numeric\", hour:\"2-digit\", minute:\"2-digit\", second:\"2-digit\", hourCycle: 'h23'}); var text = date + '</br>' + Highcharts.numberFormat(this.y, 4); var chart = this.series.chart; if (!chart.lbl) { chart.lbl = chart.renderer.label(text, 75, 7).attr({ padding: 10,r: 5,zIndex: 0 }).css({color: 'red'}).add();} else {chart.lbl.attr({text: text}); } } , click: function (event) { this.select(!this.selected, false); } } }, marker: { enabled: true, radius: 0.1,  states: { select: { fillColor: 'red', lineWidth: 0, radius: 4  }, hover: { fillColor: '#4da6ff', lineWidth: 1, radius: 6 } } } } }, \"title\" : { \"text\" : \" Overall \" },exporting: appendExportButtonsToTrenGraph(),  \"legend\" : { \"enabled\" : false },\"yAxis\" : [ { min:0, max: 20.0,  \"title\" : { \"text\" : \" in/s - RMS \" } {} } ],\"xAxis\": [{ \"title\": { \"text\": \" null \" }, \"type\": \"datetime\" } ],tooltip: { positioner: function() { return { x: this.chart.plotSizeX-135, y: this.chart.plotTop-45 }; }, formatter() { return new Date(this.x).toLocaleString(\"en-US\", { timeZone: \"GMT\", weekday:\"short\", year:\"numeric\", month:\"short\", day:\"numeric\", hour:\"2-digit\", minute:\"2-digit\", second:\"2-digit\", hourCycle: 'h23'}) +'</br>' + Highcharts.numberFormat(this.y, 4)},shared: true, shadow: false, borderWidth: 0, backgroundColor: \"rgba(255,255,255,0.8)\" }  ,navigator: { top: 35, margin: 10, enabled: true, series: {  dataGrouping: { enabled: false } } ,xAxis: { type: 'linear' } }, scrollbar: { enabled: true }}";
        instance.setTimestamp("1698351117000");
        String result = instance.getTrendChartJson(pointType, values, yAxisTitle, trendGraphTitle, plotLines, selectedDataPointIndex, yAxisUpperBound, paramAmpFactor);
        assertNotEquals(expResult, result);
    }

    /**
     * Test of onHarmonicChange method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testOnHarmonicChange() {
        System.out.println("testOnHarmonicChange");
        setupChartMap();
        String harmonicType = "startHarmonic";
        instance.getChartInfoMap().get("window1").getSpectrumGraphData().setStartingHarmonic(5);
        instance.getChartInfoMap().get("window1").getSpectrumGraphData().setEndingHarmonic(10);
        instance.onHarmonicChange(harmonicType);
        assertNull(instance.getChartInfoMap().get("window1").getSpectrumGraphData().getEndingHarmonic());
    }
    
    /**
     * Test of onTachSpeedChange method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testOnTachSpeedChange() {
        System.out.println("testOnTachSpeedChange");
        setupChartMap();
        String graphType = "spectrum";
        instance.getChartInfoMap().get("window1").getSpectrumGraphData().setSelectedFrequencyUnit("Orders");
        instance.onTachSpeedChange(graphType);
        assertEquals("spectrum", instance.getChartInfoMap().get("window1").getGraphType());
        assertEquals(0.1f, instance.getChartInfoMap().get("window1").getSpectrumGraphData().getTachSpeed(),0);
    }

    /**
     * Test of resetTachSpeed method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testResetTachSpeed() {
        System.out.println("testResetTachSpeed");
        setupChartMap();
        String graphType = "spectrum";
        instance.getChartInfoMap().get("window1").getSpectrumGraphData().setSelectedFrequencyUnit("Orders");
        instance.getChartInfoMap().get("window1").getSpectrumGraphData().setOriginalTachSpeed(5f);
        instance.getChartInfoMap().get("window1").getSpectrumGraphData().setTachSpeedUnit("RPM");
        instance.resetTachSpeed(graphType);
        assertEquals("spectrum", instance.getChartInfoMap().get("window1").getGraphType());
        assertEquals(5f, instance.getChartInfoMap().get("window1").getSpectrumGraphData().getTachSpeed(),0);
        assertEquals("Hz", instance.getChartInfoMap().get("window1").getSpectrumGraphData().getTachSpeedUnit());
    }

    /**
     * Test of onTachSpeedUnitChange method, of class AssetAnalysisWindowBean.
     */
    @Test
    public void testOnTachSpeedUnitChange() {
        System.out.println("testOnTachSpeedUnitChange");
        setupChartMap();
        String graphType = "spectrum";
        instance.getChartInfoMap().get("window1").getSpectrumGraphData().setTachSpeed(5f);
        instance.getChartInfoMap().get("window1").getSpectrumGraphData().setTachSpeedUnit("RPM");
        instance.onTachSpeedUnitChange(graphType);
        assertEquals(300f, instance.getChartInfoMap().get("window1").getSpectrumGraphData().getTachSpeed(),0);
    }

    @Test
    public void testComposeWaveformAudio() throws Exception {
        System.out.println("testComposeWaveformAudio");
        String windowId = "window1";
        String base64Audio = "base64EncodedAudio";
        setupChartMap();
        instance.setWindowId(windowId);
        ChartInfoVO ciVo =  instance.getChartInfoMap().get("window1");
        when(audioUtil.convertWavAudio( instance.getDoubleArrayFromSampleList(ciVo.getWaveDataVO().getWave().getSamples()) , ciVo.getWaveDataVO().getWave().getSampleRateHz())).thenReturn(base64Audio);
        instance.composeWaveformAudio();
    }

    
}
