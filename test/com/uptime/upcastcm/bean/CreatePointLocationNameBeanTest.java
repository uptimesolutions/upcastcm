/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.FaultFrequenciesClient;
import com.uptime.upcastcm.http.client.PointLocationNamesClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import com.uptime.upcastcm.utils.helperclass.PresetUtil;
import com.uptime.upcastcm.utils.vo.PointLocationNamesVO;
import com.uptime.upcastcm.utils.vo.UserSitesVO;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.faces.component.UIViewRoot;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import static junit.framework.Assert.assertEquals;
import org.junit.Test;
import org.junit.FixMethodOrder;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.primefaces.PrimeFaces;

/**
 *
 * @author Joseph
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(PowerMockRunner.class)
@PowerMockIgnore("jdk.internal.reflect.*")
@PrepareForTest({FacesContext.class, ExternalContext.class, ServiceRegistrarClient.class, UserManageBean.class, PointLocationNamesClient.class, PrimeFaces.class, PrimeFaces.Ajax.class})
public class CreatePointLocationNameBeanTest {

    @Mock
    ServiceRegistrarClient serviceRegistrarClient;

    @Mock
    FaultFrequenciesClient faultFrequenciesClient;

    @Mock
    PointLocationNamesClient pointLocationNamesClient;

    @Mock
    FacesContext facesContext;

    @Mock
    ExternalContext externalContext;

    @Mock
    UserManageBean userManageBean;

    @Mock
    PresetsBean presetsBean;

    @Mock
    HttpServletRequest request;

    @Mock
    HttpSession session;

    @Mock
    UIViewRoot root;

    @Mock
    NavigationBean navigationBean;
    private final CreatePointLocationNameBean instance;
    Map<String, Object> sessionMap;

    public CreatePointLocationNameBeanTest() {
        UserSitesVO uvo;
        
        PowerMockito.mockStatic(FacesContext.class);
        PowerMockito.mockStatic(PointLocationNamesClient.class);
        PowerMockito.mockStatic(ServiceRegistrarClient.class);

        facesContext = PowerMockito.mock(FacesContext.class);
        externalContext = PowerMockito.mock(ExternalContext.class);
        userManageBean = PowerMockito.mock(UserManageBean.class);

        request = PowerMockito.mock(HttpServletRequest.class);
        session = PowerMockito.mock(HttpSession.class);
        root = PowerMockito.mock(UIViewRoot.class);
        navigationBean = PowerMockito.mock(NavigationBean.class);
        presetsBean = PowerMockito.mock(PresetsBean.class);
        sessionMap = new HashMap();

        pointLocationNamesClient = PowerMockito.mock(PointLocationNamesClient.class);
        serviceRegistrarClient = PowerMockito.mock(ServiceRegistrarClient.class);

        sessionMap = new HashMap();
        sessionMap.put("presetsBean", presetsBean);
        sessionMap.put("userManageBean", userManageBean);

        session.setAttribute("navigationBean", navigationBean);
        session.setAttribute("userManageBean", userManageBean);

        uvo = new UserSitesVO();
        uvo.setSiteId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        uvo.setSiteName("Test Site 1");
        uvo.setSiteRole("Site Administrator");
        
        when(PointLocationNamesClient.getInstance()).thenReturn(pointLocationNamesClient);
        when(ServiceRegistrarClient.getInstance()).thenReturn(serviceRegistrarClient);
        when(serviceRegistrarClient.getServiceHostURL(PRESET_SERVICE_NAME)).thenReturn("host");
        when(FacesContext.getCurrentInstance()).thenReturn(facesContext);
        when(facesContext.getExternalContext()).thenReturn(externalContext);
        when(externalContext.getSessionMap()).thenReturn(sessionMap);
        when(facesContext.getExternalContext().getRequest()).thenReturn(request);
        when(request.getSession()).thenReturn(session);
        when(userManageBean.getPresetUtil()).thenReturn(new PresetUtil());
        when(session.getAttribute("navigationBean")).thenReturn(navigationBean);
        when(session.getAttribute("presetsBean")).thenReturn(presetsBean);
        when(session.getAttribute("userManageBean")).thenReturn(userManageBean);
        when(userManageBean.getCurrentSite()).thenReturn(uvo);
        UserSitesVO usvo = new UserSitesVO();
        usvo.setCustomerAccount("77777");
        when(userManageBean.getCurrentSite()).thenReturn(usvo);

        instance = new CreatePointLocationNameBean();

        PointLocationNamesVO plnvo = new PointLocationNamesVO();
        plnvo.setPointLocationName("Test Point Location");
        plnvo.setSiteName("Test Site 1");
        plnvo.setSiteId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        plnvo.setCustomerAccount("77777");
        plnvo.setDescription("Test Description 1");
        
        instance.setPointLocationNamesVO(plnvo);
    }

    /**
     * Test of init method, of class CreatePointLocationNameBean.
     */
    @Test
    public void test1_Init() {
        System.out.println("init");
        PointLocationNamesVO vo = new PointLocationNamesVO();
        vo.setCustomerAccount("77777");
        instance.init();
        assertEquals(instance.getPointLocationNamesVO(), vo);
        System.out.println("init completed");
    }

    /**
     * Test of resetPage method, of class CreatePointLocationNameBean.
     */
    @Test
    public void test2_ResetPage() {
        System.out.println("resetPage");
        PointLocationNamesVO vo = new PointLocationNamesVO();
        vo.setCustomerAccount("77777");
        instance.resetPage();
        assertEquals(instance.getPointLocationNamesVO(), vo);
        System.out.println("resetPage completed");
    }

    /**
     * Test of onSiteChange method, of class CreatePointLocationNameBean.
     */
    @Test
    public void test3_OnSiteChange() {
        System.out.println("onSiteChange");
        instance.onSiteChange();
        System.out.println("onSiteChange completed");
    }

    /**
     * Test of onPointLocationNameChange method, of class CreatePointLocationNameBean.
     */
    @Test
    public void test4_onPointLocationNameChange() {
        System.out.println("onPointLocationNameChange");
        List<PointLocationNamesVO> sitePtLocationNameList = new ArrayList();
        sitePtLocationNameList.add(new PointLocationNamesVO());
        when(pointLocationNamesClient.getSitePointLocationNamesByPK("host", "77777", UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"), "Test Point Location")).thenReturn(sitePtLocationNameList);
        instance.setSiteOrGlobal("site");
        instance.onPointLocationNameChange();
        assertEquals(instance.getPointLocationNamesVO().getPointLocationName(), "Test Point Location");
        System.out.println("onPointLocationNameChange completed");
    }

    /**
     * Test of submit method, of class CreatePointLocationNameBean.
     */
    @Test
    public void test99_Submit() {
        System.out.println("submit");
        PowerMockito.mockStatic(PrimeFaces.class);
        PowerMockito.mockStatic(PrimeFaces.Ajax.class);
        PrimeFaces pf = PowerMockito.mock(PrimeFaces.class);
        PrimeFaces.Ajax ax = PowerMockito.mock(PrimeFaces.Ajax.class);
        when(pf.current()).thenReturn(pf);
        when(pf.ajax()).thenReturn(ax);
        ax.addCallbackParam("presetsTabViewId:pointLocationNamesFormId", new Object());
        instance.getPointLocationNamesVO().setSiteName("Test Site 1");
        instance.setSiteOrGlobal("site");
        when(pointLocationNamesClient.createSitePointLocationNames(serviceRegistrarClient.getServiceHostURL(PRESET_SERVICE_NAME), instance.getPointLocationNamesVO())).thenReturn(Boolean.TRUE);
        instance.submit();
        System.out.println("submit completed");
    }

    /**
     * Test of submit method, of class CreatePointLocationNameBean.
     */
    @Test
    public void test98_Submit() {
        System.out.println("submit- at global level");
        PowerMockito.mockStatic(PrimeFaces.class);
        PowerMockito.mockStatic(PrimeFaces.Ajax.class);
        PrimeFaces pf = PowerMockito.mock(PrimeFaces.class);
        PrimeFaces.Ajax ax = PowerMockito.mock(PrimeFaces.Ajax.class);
        when(pf.current()).thenReturn(pf);
        when(pf.ajax()).thenReturn(ax);
        ax.addCallbackParam("presetsTabViewId:pointLocationNamesFormId", new Object());
        instance.getPointLocationNamesVO().setSiteName(null);
        instance.setSiteOrGlobal("global");
        when(pointLocationNamesClient.createGlobalPointLocationNames(serviceRegistrarClient.getServiceHostURL(PRESET_SERVICE_NAME), instance.getPointLocationNamesVO())).thenReturn(Boolean.TRUE);
        instance.submit();
        System.out.println("submit global completed");
    }

    /**
     * Test of getPointLocationNamesVO method, of class CreatePointLocationNameBean.
     */
    @Test
    public void test14_GetPointLocationNamesVO() {
        System.out.println("getPointLocationNamesVO");
        CreatePointLocationNameBean instance = new CreatePointLocationNameBean();
        PointLocationNamesVO expResult = null;
        PointLocationNamesVO result = instance.getPointLocationNamesVO();
        assertEquals(expResult, result);
    }

    /**
     * Test of setPointLocationNamesVO method, of class CreatePointLocationNameBean.
     */
    @Test
    public void test15_SetPointLocationNamesVO() {
        System.out.println("setPointLocationNamesVO");
        PointLocationNamesVO pointLocationNamesVO = new PointLocationNamesVO();
        instance.setPointLocationNamesVO(pointLocationNamesVO);
    }

}
