/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.AssetPresetClient;
import com.uptime.upcastcm.http.client.DevicePresetClient;
import com.uptime.upcastcm.http.client.FaultFrequenciesClient;
import com.uptime.upcastcm.http.client.PointLocationNamesClient;
import com.uptime.upcastcm.http.client.TachometerClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import com.uptime.upcastcm.utils.helperclass.PresetUtil;
import com.uptime.upcastcm.utils.singletons.CommonUtilSingleton;
import com.uptime.upcastcm.utils.vo.ApAlSetVO;
import com.uptime.upcastcm.utils.vo.AssetPresetVO;
import com.uptime.upcastcm.utils.vo.DevicePresetVO;
import com.uptime.upcastcm.utils.vo.DeviceVO;
import com.uptime.upcastcm.utils.vo.FaultFrequenciesVO;
import com.uptime.upcastcm.utils.vo.PointLocationNamesVO;
import com.uptime.upcastcm.utils.vo.TachometerVO;
import com.uptime.upcastcm.utils.vo.UserPreferencesVO;
import com.uptime.upcastcm.utils.vo.UserSitesVO;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.FixMethodOrder;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.junit.runners.MethodSorters;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;
import org.primefaces.PrimeFaces;

/**
 *
 * @author twilcox
 */
@RunWith(PowerMockRunner.class)
@PowerMockIgnore("jdk.internal.reflect.*")
@PrepareForTest({FacesContext.class, ExternalContext.class, NavigationBean.class, UserManageBean.class, ServiceRegistrarClient.class, FaultFrequenciesClient.class, DevicePresetClient.class, TachometerClient.class, PointLocationNamesClient.class, AssetPresetClient.class, PresetUtil.class, CreatePresetsDeviceBean.class, CommonUtilSingleton.class, PrimeFaces.class, PrimeFaces.Ajax.class})
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@PowerMockRunnerDelegate(JUnit4.class)
public class PresetsBeanTest {

    @Mock
    ServiceRegistrarClient serviceRegistrarClient;

    @Mock
    FacesContext facesContext;

    @Mock
    ExternalContext externalContext;

    @Mock
    NavigationBean navigationBean;

    @Mock
    UserManageBean userManageBean;

    @Mock
    CreatePresetsDeviceBean createPresetsDeviceBean;

    @Mock
    PresetUtil presetUtil;

    @Mock
    DevicePresetClient devicePresetClient;

    @Mock
    FaultFrequenciesClient faultFrequenciesClient;

    @Mock
    TachometerClient tachometerClient;

    @Mock
    AssetPresetClient assetPresetClient;

    @Mock
    CommonUtilSingleton commonUtilSingleton;

    @Mock
    PointLocationNamesClient pointLocationNamesClient;

    @Mock
    PrimeFaces pf;

    @Mock
    PrimeFaces.Ajax ax;

    private final PresetsBean instance;
    Map<String, Object> sessionMap;
    String customerAccount, defaultsite;
    UserPreferencesVO userVO;
    private final List<UserSitesVO> userSiteList;
    List<ApAlSetVO> apalVOs;
    List<DevicePresetVO> devicePresetVOList, uptimeDevicePresetVOList, siteDevicePresetVOList;
    List<FaultFrequenciesVO> faultFrequenciesVOs, faultFrequenciesVOList, siteFaultFrequenciesVOList;
    List<TachometerVO> customerTachometerVOList, globalTachometerVOList, siteTachometerVOList;
    TachometerVO tachometerVO;
    List<DevicePresetVO> devicePresetVOs;
    List<PointLocationNamesVO> pointLocationNamesVOs, sitePointLocationNamesVOs;

    public PresetsBeanTest() {
        PowerMockito.mockStatic(FacesContext.class);
        PowerMockito.mockStatic(ServiceRegistrarClient.class);
        PowerMockito.mockStatic(DevicePresetClient.class);
        PowerMockito.mockStatic(ExternalContext.class);
        PowerMockito.mockStatic(PresetUtil.class);
        PowerMockito.mockStatic(FaultFrequenciesClient.class);
        PowerMockito.mockStatic(TachometerClient.class);
        PowerMockito.mockStatic(PointLocationNamesClient.class);
        PowerMockito.mockStatic(CommonUtilSingleton.class);
        PowerMockito.mockStatic(AssetPresetClient.class);

        facesContext = PowerMockito.mock(FacesContext.class);
        externalContext = PowerMockito.mock(ExternalContext.class);
        navigationBean = PowerMockito.mock(NavigationBean.class);
        userManageBean = PowerMockito.mock(UserManageBean.class);
        createPresetsDeviceBean = PowerMockito.mock(CreatePresetsDeviceBean.class);
        presetUtil = PowerMockito.mock(PresetUtil.class);
        serviceRegistrarClient = PowerMockito.mock(ServiceRegistrarClient.class);
        devicePresetClient = PowerMockito.mock(DevicePresetClient.class);
        faultFrequenciesClient = PowerMockito.mock(FaultFrequenciesClient.class);
        tachometerClient = PowerMockito.mock(TachometerClient.class);
        commonUtilSingleton = PowerMockito.mock(CommonUtilSingleton.class);
        pointLocationNamesClient = PowerMockito.mock(PointLocationNamesClient.class);
        assetPresetClient = PowerMockito.mock(AssetPresetClient.class);

        PowerMockito.mockStatic(PrimeFaces.class);
        PowerMockito.mockStatic(PrimeFaces.Ajax.class);
        pf = PowerMockito.mock(PrimeFaces.class);
        ax = PowerMockito.mock(PrimeFaces.Ajax.class);

        sessionMap = new HashMap();
        sessionMap.put("navigationBean", navigationBean);
        sessionMap.put("userManageBean", userManageBean);
        sessionMap.put("createPresetsDeviceBean", createPresetsDeviceBean);

        when(FacesContext.getCurrentInstance()).thenReturn(facesContext);
        when(facesContext.getExternalContext()).thenReturn(externalContext);
        when(externalContext.getSessionMap()).thenReturn(sessionMap);

        customerAccount = "77777";
        defaultsite = "TEST SITE 001";

        userSiteList = new ArrayList<>();
        UserSitesVO uvo = new UserSitesVO();
        uvo.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        uvo.setSiteName("TEST SITE 001");
        userSiteList.add(uvo);

        userVO = new UserPreferencesVO();
        userVO.setCustomerAccount(customerAccount);
        userVO.setDefaultSite(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        userVO.setDefaultSiteName("TEST SITE 001");

        ApAlSetVO apalVo = new ApAlSetVO();
        apalVo.setApSetId(UUID.fromString("5ee8fbf1-6d1e-4ac7-afa8-e1db3d8920c7"));
        apalVo.setFmax(20);
        apalVo.setResolution(2);
        apalVo.setPeriod(25);
        apalVo.setSampleRate(85);
        apalVOs = new ArrayList();
        apalVOs.add(apalVo);

        devicePresetVOList = new ArrayList();
        DevicePresetVO dvo = new DevicePresetVO();
        dvo.setCustomerAccount(customerAccount);
        dvo.setPresetId(UUID.randomUUID());
        devicePresetVOList.add(dvo);

        siteDevicePresetVOList = new ArrayList();
        dvo = new DevicePresetVO();
        dvo.setCustomerAccount("AAAAA");
        dvo.setSiteId(userVO.getDefaultSite());
        dvo.setPresetId(UUID.randomUUID());
        siteDevicePresetVOList.add(dvo);

        uptimeDevicePresetVOList = new ArrayList();
        dvo = new DevicePresetVO();
        dvo.setCustomerAccount(DEFAULT_UPTIME_ACCOUNT);
        dvo.setPresetId(UUID.randomUUID());
        uptimeDevicePresetVOList.add(dvo);

        faultFrequenciesVOs = new ArrayList();
        faultFrequenciesVOList = new ArrayList();
        FaultFrequenciesVO ffvo = new FaultFrequenciesVO();
        ffvo.setCustomerAccount(customerAccount);
        ffvo.setFfName("TEST FF");
        ffvo.setFfSetId(UUID.randomUUID());
        ffvo.setSiteId(UUID.randomUUID());
        faultFrequenciesVOList.add(ffvo);

        siteFaultFrequenciesVOList = new ArrayList();
        FaultFrequenciesVO ffvo1 = new FaultFrequenciesVO();
        ffvo1.setCustomerAccount(customerAccount);
        ffvo1.setSiteId(userVO.getDefaultSite());
        ffvo1.setFfSetId(UUID.randomUUID());
        ffvo1.setFfName("TEST FF Site");
        faultFrequenciesVOList.add(ffvo);

        tachometerVO = new TachometerVO();
        tachometerVO.setCustomerAccount(customerAccount);
        tachometerVO.setSiteId(userVO.getDefaultSite());
        tachometerVO.setSiteName(userVO.getDefaultSiteName());

        devicePresetVOs = new ArrayList<>();
        DevicePresetVO devicePresetVO = new DevicePresetVO();
        devicePresetVO.setCustomerAccount(customerAccount);
        devicePresetVO.setPresetId(UUID.randomUUID());
        devicePresetVOs.add(devicePresetVO);

        pointLocationNamesVOs = new ArrayList<>();
        PointLocationNamesVO pvo = new PointLocationNamesVO();
        pvo.setCustomerAccount(customerAccount);
        pvo.setSiteId(userVO.getDefaultSite());
        pvo.setPointLocationName("Test PL 1");
        pointLocationNamesVOs.add(pvo);

        sitePointLocationNamesVOs = new ArrayList<>();
        pvo = new PointLocationNamesVO();
        pvo.setCustomerAccount(customerAccount);
        pvo.setSiteId(userVO.getDefaultSite());
        pvo.setPointLocationName("Test PL 2");
        sitePointLocationNamesVOs.add(pvo);

        globalTachometerVOList = new ArrayList<>();
        customerTachometerVOList = new ArrayList<>();
        siteTachometerVOList = new ArrayList<>();

        TachometerVO tachometerVO1 = new TachometerVO();
        tachometerVO1.setCustomerAccount(customerAccount);
        tachometerVO1.setSiteId(userVO.getDefaultSite());
        tachometerVO1.setSiteName(userVO.getDefaultSiteName());
        tachometerVO1.setTachName("SITE TACH");
        siteTachometerVOList.add(tachometerVO1);

        TachometerVO tachometerVO2 = new TachometerVO();
        tachometerVO1.setCustomerAccount(customerAccount);
        tachometerVO2.setTachName("CUST TACH");
        customerTachometerVOList.add(tachometerVO2);

        TachometerVO tachometerVO3 = new TachometerVO();
        tachometerVO3.setCustomerAccount(DEFAULT_UPTIME_ACCOUNT);
        tachometerVO3.setTachName("SITE TACH");
        globalTachometerVOList.add(tachometerVO3);

        List<DeviceVO> deviceTypeList = new ArrayList();
        deviceTypeList.add(new DeviceVO(0, "0", "AC", "Waveform"));
        deviceTypeList.add(new DeviceVO(1, "1", "AC", "Waveform"));
        deviceTypeList.add(new DeviceVO(2, "2", "AC", "Waveform"));
        deviceTypeList.add(new DeviceVO(3, "3", "AC", "Waveform"));
        deviceTypeList.add(new DeviceVO(4, "4", "AC", "Waveform"));
        deviceTypeList.add(new DeviceVO(5, "5", "AC", "Waveform"));
        deviceTypeList.add(new DeviceVO(6, "6", "AC", "Waveform"));
        deviceTypeList.add(new DeviceVO(7, "7", "AC", "Waveform"));
        deviceTypeList.add(new DeviceVO(0, "0", "DC", "Single-Measurement"));
        deviceTypeList.add(new DeviceVO(1, "1", "DC", "Single-Measurement"));
        deviceTypeList.add(new DeviceVO(2, "2", "DC", "Single-Measurement"));
        deviceTypeList.add(new DeviceVO(3, "3", "DC", "Single-Measurement"));
        deviceTypeList.add(new DeviceVO(4, "4", "DC", "Single-Measurement"));
        deviceTypeList.add(new DeviceVO(5, "5", "DC", "Single-Measurement"));
        deviceTypeList.add(new DeviceVO(6, "6", "DC", "Single-Measurement"));

        UserSitesVO usvo = new UserSitesVO();
        usvo.setCustomerAccount(customerAccount);

        when(userManageBean.getPresetUtil()).thenReturn(presetUtil);
        when(userManageBean.getCurrentSite()).thenReturn(usvo);
        //when(presetUtil.getCustomerApAlSetList(customerAccount)).thenReturn(apalVOs);
        when(presetUtil.getGlobalptLoNameList(customerAccount)).thenReturn(pointLocationNamesVOs);
        when(presetUtil.getGlobalFaultFrequenciesByCustomer(customerAccount)).thenReturn(faultFrequenciesVOList);
        when(presetUtil.getSiteFaultFrequenciesList(customerAccount, userVO.getDefaultSite())).thenReturn(siteFaultFrequenciesVOList);
        when(presetUtil.getSitePtLoNameList(customerAccount, userVO.getDefaultSite())).thenReturn(sitePointLocationNamesVOs);

        when(presetUtil.getGlobalTachList(customerAccount)).thenReturn(customerTachometerVOList);
        when(presetUtil.getSiteTachList(customerAccount, userVO.getDefaultSite())).thenReturn(siteTachometerVOList);
        PowerMockito.doNothing().when(presetUtil).clearSiteFaultFrequenciesList();
        when(ServiceRegistrarClient.getInstance()).thenReturn(serviceRegistrarClient);
        when(CommonUtilSingleton.getInstance()).thenReturn(commonUtilSingleton);
        when(commonUtilSingleton.populateDeviceTypeList("TS1X")).thenReturn(deviceTypeList);
        when(DevicePresetClient.getInstance()).thenReturn(devicePresetClient);
        when(FaultFrequenciesClient.getInstance()).thenReturn(faultFrequenciesClient);
        when(TachometerClient.getInstance()).thenReturn(tachometerClient);
        when(PointLocationNamesClient.getInstance()).thenReturn(pointLocationNamesClient);
        when(AssetPresetClient.getInstance()).thenReturn(assetPresetClient);
        when(serviceRegistrarClient.getServiceHostURL(PRESET_SERVICE_NAME)).thenReturn(PRESET_SERVICE_NAME);
        when(devicePresetClient.getGlobalDevicePresetsByCustomer(PRESET_SERVICE_NAME, DEFAULT_UPTIME_ACCOUNT)).thenReturn(uptimeDevicePresetVOList);
        when(devicePresetClient.getGlobalDevicePresetsByCustomer(PRESET_SERVICE_NAME, customerAccount)).thenReturn(devicePresetVOList);
        when(devicePresetClient.getDevicePresetVOs(PRESET_SERVICE_NAME, customerAccount, userVO.getDefaultSite())).thenReturn(siteDevicePresetVOList);
        when(devicePresetClient.deletePresetDevices(PRESET_SERVICE_NAME, devicePresetVOs)).thenReturn(true);
        when(faultFrequenciesClient.deleteSiteFaultFrequencies(PRESET_SERVICE_NAME, faultFrequenciesVOs)).thenReturn(true);
        when(tachometerClient.deleteSiteTachometers(PRESET_SERVICE_NAME, tachometerVO)).thenReturn(true);
        when(pointLocationNamesClient.deleteSitePointLocationNames(PRESET_SERVICE_NAME, pvo)).thenReturn(true);

        instance = new PresetsBean();

        PowerMockito.mockStatic(PrimeFaces.class);
        PowerMockito.mockStatic(PrimeFaces.Ajax.class);
        pf = PowerMockito.mock(PrimeFaces.class);
        PrimeFaces.Ajax ax = PowerMockito.mock(PrimeFaces.Ajax.class);
        when(PrimeFaces.current()).thenReturn(pf);
        when(pf.ajax()).thenReturn(ax);
        ax.addCallbackParam("presetsTabViewId", new Object());
    }

    /**
     * Test of init method, of class PresetsBean.
     */
    @Test
    public void test01_Init() {
        System.out.println("init");
        instance.setUploadType("TEST TYPE");
        instance.init();
        assertEquals(instance.getUploadType(), null);
        System.out.println("init completed");
    }

    /**
     * Test of resetPage method, of class PresetsBean.
     */
    @Test
    public void test02_ResetPage() {
        System.out.println("resetPage");
        UserSitesVO usvo = new UserSitesVO();
        usvo.setSiteName("WWWWW");
        instance.resetPage();
        System.out.println("resetPage completed");
    }

    /**
     * Test of resetAnalysisParametersTab method, of class PresetsBean.
     */
    @Test
    public void test041_ResetAnalysisParametersTab() {
        System.out.println("resetAnalysisParametersTab");
        UserSitesVO usvo = new UserSitesVO();
        usvo.setSiteName("WWWWW");
        instance.resetAnalysisParametersTab();
        System.out.println("resetAnalysisParametersTab completed");
    }

    /**
     * Test of resetTachometersTab method, of class PresetsBean.
     */
    @Test
    public void test051_ResetTachometersTab() {
        System.out.println("resetTachometersTab");
        UserSitesVO usvo = new UserSitesVO();
        usvo.setSiteName("WWWWW");
        instance.resetTachometersTab();
        System.out.println("resetTachometersTab completed");
    }

    /**
     * Test of delete method, of class PresetsBean.
     */
    @Test
    public void test07_Delete() {
        System.out.println("delete");
        instance.delete(null, null);
        System.out.println("delete completed");
    }

    /**
     * Test of delete method, of class PresetsBean.
     */
    @Test
    public void test072_Delete_tachometers() {
        System.out.println("delete");        
        TachometerVO tvo = new TachometerVO();
        tvo.setCustomerAccount(customerAccount);
        tvo.setTachId(UUID.randomUUID());
        tvo.setSiteId(UUID.randomUUID());
        instance.resetTachometersTab();
        instance.getTachometerList().add(tvo);
        assertEquals(instance.getTachometerList().size(), 2);
        instance.delete(tachometerVO, "tachometers");
        assertEquals(instance.getTachometerList().size(), 1);
        System.out.println("delete tachometers completed");
    }

    /**
     * Test of delete method, of class PresetsBean.
     */
    @Test
    public void test073_Delete_devices() {
        System.out.println("delete");
        DevicePresetVO dvo = new DevicePresetVO();
        dvo.setCustomerAccount(customerAccount);
        dvo.setPresetId(UUID.randomUUID());
        dvo.setSiteId(UUID.randomUUID());
        instance.resetDevicePresetsTab();
        assertEquals(instance.getDevicePresetList().size(), 2);
        instance.getDevicePresetList().add(dvo);
        assertEquals(instance.getDevicePresetList().size(), 3);
        instance.delete(dvo, "devices");
        assertEquals(instance.getDevicePresetList().size(), 2);
        System.out.println("delete devices completed");
    }

    /**
     * Test of delete method, of class PresetsBean.
     */
    @Test
    public void test074_Delete_pointLocationNames() {
        System.out.println("delete");
        PointLocationNamesVO pvo = new PointLocationNamesVO();
        pvo.setCustomerAccount(customerAccount);
        pvo.setPointLocationName("TEST PL 2");
        pvo.setSiteId(userVO.getDefaultSite());
        instance.resetPointLocationNamesTab();
        instance.getPointLocationNameList().add(pvo);
        assertEquals(instance.getPointLocationNameList().size(), 2);
        instance.delete(pvo, "pointLocationNames");
        assertEquals(instance.getPointLocationNameList().size(), 1);
        System.out.println("delete pointLocationNames completed");
    }

    /**
     * Test of setTachometerDialog method, of class PresetsBean.
     */
    @Test
    public void test08_SetTachometerDialog() {
        System.out.println("setTachometerDialog");

        TachometerVO tachometerVO = new TachometerVO();
        tachometerVO.setCustomerAccount(customerAccount);
        tachometerVO.setSiteName(defaultsite);
        tachometerVO.setTachName("TEST TACH");
        assertEquals(sessionMap.size(), 3);
        instance.setTachometerDialog(tachometerVO, 0);
        assertEquals(sessionMap.size(), 4);
        System.out.println("setTachometerDialog completed");
    }

    /**
     * Test of setApAlSetDialog method, of class PresetsBean.
     */
    @Test
    public void test09_SetApAlSetDialog() {
        System.out.println("setApAlSetDialog");
        assertEquals(sessionMap.size(), 3);
        instance.setApAlSetDialog(null, 0);
        assertEquals(sessionMap.size(), 4);
        System.out.println("setApAlSetDialog completed");
    }

    /**
     * Test of getApAlSetList method, of class PresetsBean.
     */
    @Test
    public void test15_GetApAlSetList() {
        System.out.println("getApAlSetList");
        assertNull(instance.getApAlSetList(apalVOs.get(0).getApSetId()));
        System.out.println("getApAlSetList completed");
    }

    /**
     * Test of getFmaxList method, of class PresetsBean.
     */
    @Test
    public void test17_GetFmaxList() {
        System.out.println("getFmaxList");
        assertEquals(new ArrayList(), instance.getFmaxList());
        System.out.println("getFmaxList completed");
    }

    /**
     * Test of getResolutionList method, of class PresetsBean.
     */
    @Test
    public void test18_GetResolutionList() {
        System.out.println("getResolutionList");
        assertEquals(new ArrayList(), instance.getResolutionList());
        System.out.println("getResolutionList completed");
    }

    /**
     * Test of getTachometerList method, of class PresetsBean.
     */
    @Test
    public void test20_GetTachometerList() {
        System.out.println("getTachometerList");
        assertEquals(new ArrayList(), instance.getTachometerList());
        System.out.println("getTachometerList completed");
    }

    /**
     * Test of getOperationType method, of class PresetsBean.
     */
    @Test
    public void test25_GetOperationType() {
        System.out.println("getOperationType");
        assertEquals(0, instance.getOperationType());
        System.out.println("getOperationType completed");
    }

    /**
     * Test of getUploadType method, of class PresetsBean.
     */
    @Test
    public void test26_GetUploadType() {
        System.out.println("getUploadType");
        assertNull(instance.getUploadType());
        System.out.println("getUploadType completed");
    }

    /**
     * Test of setUploadType method, of class PresetsBean.
     */
    @Test
    public void test27_SetUploadType() {
        System.out.println("setUploadType");
        instance.setUploadType(null);
        assertNull(instance.getUploadType());
        System.out.println("setUploadType completed");
    }

    /**
     * Test of resetDevicePresetsTab method, of class PresetsBean.
     */
    @Test
    public void test30_ResetDevicePresetsTab() {
        System.out.println("resetDevicePresetsTab");
        instance.resetDevicePresetsTab();
        assertEquals(instance.getDevicePresetList().size(), 2);
    }

    /**
     * Test of resetApAlSetMapForAc method, of class PresetsBean.
     */
    @Test
    public void test31_ResetApAlSetMapForAc() {
        System.out.println("resetApAlSetMapForAc");
        instance.resetApAlSetMapForAc();
        assertEquals(instance.getAcApSetList().size(), 0);
    }

    /**
     * Test of resetApAlSetMapForDc method, of class PresetsBean.
     */
    @Test
    public void test32_ResetApAlSetMapForDc() {
        System.out.println("resetApAlSetMapForDc");
        instance.resetApAlSetMapForDc();
        assertEquals(instance.getDcApSetList().size(), 0);
    }

    /**
     * Test of setDevicesDialog method, of class PresetsBean.
     */
    @Test
    public void test34_SetDevicesDialog() {
        System.out.println("setDevicesDialog");
        DevicePresetVO devicePresetVO = null;
        int operationType = 0;
        assertEquals(sessionMap.size(), 3);
        instance.setDevicesDialog(devicePresetVO, operationType);
        assertEquals(sessionMap.size(), 3);
    }

    /**
     * Test of setPointLocationNameDialog method, of class PresetsBean.
     */
    @Test
    public void test35_SetPointLocationNameDialog() {
        System.out.println("setPointLocationNameDialog");
        PointLocationNamesVO pointLocationNameVO = null;
        int operationType = 0;
        assertEquals(sessionMap.size(), 3);
        instance.setPointLocationNameDialog(pointLocationNameVO, operationType);
        assertEquals(sessionMap.size(), 4);

    }

    /**
     * Test of apAlLookupByRowToggle method, of class PresetsBean.
     */
    @Test
    public void test36_ApAlLookupByRowToggle() {
        System.out.println("apAlLookupByRowToggle");
        ApAlSetVO apAlSetVO = new ApAlSetVO();
        UUID siteId = userVO.getDefaultSite();
        UUID apSetId = UUID.randomUUID();
        apAlSetVO.setApSetId(apSetId);
        apAlSetVO.setSiteId(siteId);
        instance.apAlLookupByRowToggle(apAlSetVO, false);
        assertEquals(instance.getApAlSetList(apSetId), new ArrayList<>());
        instance.apAlLookupByRowToggle(apAlSetVO, true);
        assertEquals(instance.getApAlSetList(apSetId), null);
    }

    /**
     * Test of getDevicePresetsByRowToggle method, of class PresetsBean.
     */
    @Test
    public void test38_GetDevicePresetsByRowToggle() {
        System.out.println("getDevicePresetsByRowToggle");
        DevicePresetVO devicePresetVO = new DevicePresetVO();
        devicePresetVO.setDeviceType("TS1X");
        devicePresetVO.setPresetId(devicePresetVOList.get(0).getPresetId());
        instance.getDevicePresetsByRowToggle(devicePresetVO);
        assertEquals(instance.getFilteredDevicePresetList().size(), 0);
    }

    /**
     * Test of tachReferenceRpmTableDisplay method, of class PresetsBean.
     */
    @Test
    public void test41_TachReferenceRpmTableDisplay() {
        System.out.println("tachReferenceRpmTableDisplay");
        TachometerVO tachometerVO = new TachometerVO();
        tachometerVO.setTachType("Constant Speed");
        tachometerVO.setTachReferenceSpeed(20);
        String expResult = "20.0";
        String result = instance.tachReferenceRpmTableDisplay(tachometerVO);
        assertEquals(expResult, result);
    }

    /**
     * Test of queryAcApSetNameList method, of class PresetsBean.
     */
    @Test
    public void test42_QueryAcApSetNameList() {
        System.out.println("queryAcApSetNameList");
        String query = "";
        List<String> expResult = new ArrayList<>();
        List<String> result = instance.queryAcApSetNameList(query);
        assertEquals(expResult, result);
    }

    /**
     * Test of queryDcApSetNameList method, of class PresetsBean.
     */
    @Test
    public void test43_QueryDcApSetNameList() {
        System.out.println("queryDcApSetNameList");
        String query = "";
        List<String> expResult = new ArrayList<>();
        List<String> result = instance.queryDcApSetNameList(query);
        assertEquals(expResult, result);
    }

    /**
     * Test of getAcSensorTypeList method, of class PresetsBean.
     */
    @Test
    public void test45_GetAcSensorTypeList() {
        System.out.println("getAcSensorTypeList");
        List<String> expResult = new ArrayList<>();
        List<SelectItem> result = instance.getAcSensorTypeList();
        assertEquals(expResult, result);
    }

    /**
     * Test of getDcSensorTypeList method, of class PresetsBean.
     */
    @Test
    public void test46_GetDcSensorTypeList() {
        System.out.println("getDcSensorTypeList");
        List<String> expResult = new ArrayList<>();
        List<SelectItem> result = instance.getDcSensorTypeList();
        assertEquals(expResult, result);
    }

    /**
     * Test of resetPointLocationNamesTab method, of class PresetsBean.
     */
    @Test
    public void test047_ResetPointLocationNamesTab() {
        System.out.println("resetPointLocationNamesTab");
        instance.resetPointLocationNamesTab();
        assertEquals(instance.getPointLocationNameList().size(), 1);
    }

    /**
     * Test of getAcApSetList method, of class PresetsBean.
     */
    @Test
    public void test48_GetAcApSetList() {
        System.out.println("getAcApSetList");
        List<ApAlSetVO> expResult = new ArrayList<>();
        List<ApAlSetVO> result = instance.getAcApSetList();
        assertEquals(expResult, result);
    }

    /**
     * Test of getDcApSetList method, of class PresetsBean.
     */
    @Test
    public void test49_GetDcApSetList() {
        System.out.println("getDcApSetList");
        List<ApAlSetVO> expResult = new ArrayList<>();
        List<ApAlSetVO> result = instance.getDcApSetList();
        assertEquals(expResult, result);
    }

    /**
     * Test of getFilteredAcApSetList method, of class PresetsBean.
     */
    @Test
    public void test50_GetFilteredAcApSetList() {
        System.out.println("getFilteredAcApSetList");
        List<ApAlSetVO> expResult = null;
        List<ApAlSetVO> result = instance.getFilteredAcApSetList();
        assertEquals(expResult, result);
    }

    /**
     * Test of setFilteredAcApSetList method, of class PresetsBean.
     */
    @Test
    public void test51_SetFilteredAcApSetList() {
        System.out.println("setFilteredAcApSetList");
        List<ApAlSetVO> filteredAcApSetList = new ArrayList<>();
        instance.setFilteredAcApSetList(filteredAcApSetList);
        assertEquals(instance.getFilteredAcApSetList().size(), 0);
    }

    /**
     * Test of getFilteredDcApSetList method, of class PresetsBean.
     */
    @Test
    public void test52_GetFilteredDcApSetList() {
        System.out.println("getFilteredDcApSetList");
        List<ApAlSetVO> filteredDcApSetList = new ArrayList<>();
        instance.setFilteredDcApSetList(filteredDcApSetList);
        assertEquals(instance.getFilteredDcApSetList().size(), 0);
    }

    /**
     * Test of setFilteredDcApSetList method, of class PresetsBean.
     */
    @Test
    public void test53_SetFilteredDcApSetList() {
        System.out.println("setFilteredDcApSetList");
        List<ApAlSetVO> filteredDcApSetList = new ArrayList<>();
        instance.setFilteredDcApSetList(filteredDcApSetList);
        assertEquals(instance.getFilteredDcApSetList().size(), 0);
    }

    /**
     * Test of getDevicePresetList method, of class PresetsBean.
     */
    @Test
    public void test57_GetDevicePresetList() {
        System.out.println("getDevicePresetList");
        instance.resetDevicePresetsTab();
        instance.getDevicePresetList();
    }

    /**
     * Test of getFilteredDevicePresetList method, of class PresetsBean.
     */
    @Test
    public void test58_GetFilteredDevicePresetList() {
        System.out.println("getFilteredDevicePresetList");
        List<DevicePresetVO> expResult = new ArrayList<>();
        List<DevicePresetVO> result = instance.getFilteredDevicePresetList();
        assertEquals(expResult, result);
    }

    /**
     * Test of getDistinctDevicePresetList method, of class PresetsBean.
     */
    @Test
    public void test59_GetDistinctDevicePresetList() {
        System.out.println("getDistinctDevicePresetList");
        instance.getDistinctDevicePresetList();
    }

    /**
     * Test of getPointLocationNameList method, of class PresetsBean.
     */
    @Test
    public void test60_GetPointLocationNameList() {
        System.out.println("getPointLocationNameList");
        instance.getPointLocationNameList();
    }

    /**
     * Test of queryAcAlSetItemList method, of class PresetsBean.
     */
    @Test
    public void test61_QueryAcAlSetItemList() {
        System.out.println("queryAcAlSetItemList");
        assertEquals(null, instance.queryAcAlSetItemList(null));
    }

    /**
     * Test of queryDcAlSetItemList method, of class PresetsBean.
     */
    @Test
    public void test62_QueryDcAlSetItemList() {
        System.out.println("queryDcAlSetItemList");
        assertEquals(null, instance.queryDcAlSetItemList(null));
    }

    /**
     * Test of getParamList method, of class PresetsBean.
     */
    @Test
    public void test63_GetParamList() {
        System.out.println("getParamList");
        assertEquals(new ArrayList(), instance.getParamList(null));
    }

    /**
     * Test of getAlSetList method, of class PresetsBean.
     */
    @Test
    public void test64_GetAlSetList() {
        System.out.println("getAlSetList");
        assertEquals(new ArrayList(), instance.getAlSetList(null));
    }

    /**
     * Test of addAlSet method, of class PresetsBean.
     */
    @Test
    public void test65_AddAlSet() {
        System.out.println("addAlSet");
        instance.addAlSet(null);
    }

    /**
     * Test of addAlSet method, of class PresetsBean.
     */
    @Test
    public void test66_RenderAddAlSet() {
        System.out.println("renderAddAlSet");
        assertFalse(instance.renderAddAlSet("test"));
        assertTrue(instance.renderAddAlSet("77777"));
    }
    
    
    /**
     * Test of resetAssetsTab method, of class PresetsBean.
     */
    @Test
    public void test7_ResetAssetsTab() {
        System.out.println("resetAssetsTab");
        UserSitesVO usvo = new UserSitesVO();
        usvo.setSiteName("WWWWW");
        List<AssetPresetVO> list = new ArrayList();
        AssetPresetVO assetPresetsVO = new AssetPresetVO();
        assetPresetsVO.setType("site");
        assetPresetsVO.setAssetPresetId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        assetPresetsVO.setAssetPresetName("Asset Preset 1");
        assetPresetsVO.setAssetTypeId(UUID.randomUUID());
        assetPresetsVO.setAssetTypeName("Asset Type name 1");
        assetPresetsVO.setCustomerAccount("77777");
        assetPresetsVO.setDescription("Test Description");
        assetPresetsVO.setDevicePresetId(UUID.randomUUID());
        assetPresetsVO.setDevicePresetName("Device Preset Name 1");
        assetPresetsVO.setDevicePresetType("MistLX");
        assetPresetsVO.setPointLocationName("Point Location 1");
        assetPresetsVO.setSampleInterval(720);
        assetPresetsVO.setSiteId(UUID.randomUUID());
        assetPresetsVO.setSiteName("Test Site 1");
        assetPresetsVO.setSpeedRatio(2.0f);
        list.add(assetPresetsVO);
        when(assetPresetClient.getGlobalAssetPresetsByCustomer(serviceRegistrarClient.getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount())).thenReturn(new ArrayList());
        when(assetPresetClient.getGlobalAssetPresetsByCustomer(serviceRegistrarClient.getServiceHostURL(PRESET_SERVICE_NAME), DEFAULT_UPTIME_ACCOUNT)).thenReturn(new ArrayList());
        when(userManageBean.getPresetUtil().getSiteAssetPresetList(userManageBean.getCurrentSite().getCustomerAccount(), userManageBean.getCurrentSite().getSiteId())).thenReturn(list);
        instance.resetAssetsTab();
        assertEquals(instance.getAssetPresetsList().size(), 1);
        System.out.println("resetAssetsTab completed");
    }

    /**
     * Test of getAssetPresetsByRowToggle method, of class PresetsBean.
     */
    @Test
    public void test71_getAssetPresetsByRowToggle() {
        System.out.println("getAssetPresetsByRowToggle");
        AssetPresetVO assetPresetsVO = new AssetPresetVO();
        assetPresetsVO.setType("site");
        assetPresetsVO.setAssetPresetId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        assetPresetsVO.setAssetPresetName("Asset Preset 1");
        assetPresetsVO.setAssetTypeId(UUID.randomUUID());
        assetPresetsVO.setAssetTypeName("Asset Type name 1");
        assetPresetsVO.setCustomerAccount("77777");
        assetPresetsVO.setDescription("Test Description");
        assetPresetsVO.setDevicePresetId(UUID.randomUUID());
        assetPresetsVO.setDevicePresetName("Device Preset Name 1");
        assetPresetsVO.setDevicePresetType("MistLX");
        assetPresetsVO.setPointLocationName("Point Location 1");
        assetPresetsVO.setSampleInterval(720);
        assetPresetsVO.setSiteId(UUID.randomUUID());
        assetPresetsVO.setSiteName("Test Site 1");
        assetPresetsVO.setSpeedRatio(2.0f);
        test7_ResetAssetsTab();
        instance.getAssetPresetsByRowToggle(assetPresetsVO);
        assertEquals(instance.getFilteredAssetPresetsList().size(), 1);
        System.out.println("getAssetPresetsByRowToggle completed");
    }

    /**
     * Test of setAssetsDialog method, of class PresetsBean.
     */
    @Test
    public void test72_SetAssetsDialog() {
        System.out.println("setApAlSetDialog");
        AssetPresetVO assetPresetsVO = new AssetPresetVO();
        assetPresetsVO.setType("site");
        assetPresetsVO.setAssetPresetId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        assetPresetsVO.setAssetPresetName("Asset Preset 1");
        assetPresetsVO.setAssetTypeId(UUID.randomUUID());
        assetPresetsVO.setAssetTypeName("Asset Type name 1");
        assetPresetsVO.setCustomerAccount("77777");
        assetPresetsVO.setDescription("Test Description");
        assetPresetsVO.setDevicePresetId(UUID.randomUUID());
        assetPresetsVO.setDevicePresetName("Device Preset Name 1");
        assetPresetsVO.setDevicePresetType("MistLX");
        assetPresetsVO.setPointLocationName("Point Location 1");
        assetPresetsVO.setSampleInterval(720);
        assetPresetsVO.setSiteId(UUID.randomUUID());
        assetPresetsVO.setSiteName("Test Site 1");
        assetPresetsVO.setSpeedRatio(2.0f);
        assertEquals(sessionMap.size(), 3);
        instance.setAssetsDialog(assetPresetsVO, 2);
        assertEquals(sessionMap.size(), 4);
        System.out.println("setApAlSetDialog completed");
    }

    /**
     * Test of delete method, of class PresetsBean.
     */
    @Test
    public void test73_Delete_AssetPresets() {
        System.out.println("delete");
         AssetPresetVO assetPresetsVO = new AssetPresetVO();
        assetPresetsVO.setType("site");
        assetPresetsVO.setAssetPresetId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        assetPresetsVO.setAssetPresetName("Asset Preset 1");
        assetPresetsVO.setAssetTypeId(UUID.randomUUID());
        assetPresetsVO.setAssetTypeName("Asset Type name 1");
        assetPresetsVO.setCustomerAccount("77777");
        assetPresetsVO.setDescription("Test Description");
        assetPresetsVO.setDevicePresetId(UUID.randomUUID());
        assetPresetsVO.setDevicePresetName("Device Preset Name 1");
        assetPresetsVO.setDevicePresetType("MistLX");
        assetPresetsVO.setPointLocationName("Point Location 1");
        assetPresetsVO.setSampleInterval(720);
        assetPresetsVO.setSiteId(UUID.randomUUID());
        assetPresetsVO.setSiteName("Test Site 1");
        assetPresetsVO.setSpeedRatio(2.0f);
        instance.resetAssetsTab();
        instance.getAssetPresetsList().add(assetPresetsVO);
        assertEquals(instance.getAssetPresetsList().size(), 1);
        instance.delete(assetPresetsVO, "assets");
        assertEquals(instance.getAssetPresetsList().size(), 0);
        System.out.println("delete assets completed");
    }
}
