/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.AssetClient;
import com.uptime.upcastcm.http.client.PointClient;
import com.uptime.upcastcm.http.client.PointLocationClient;
import com.uptime.upcastcm.http.client.WorkOrderClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import static com.uptime.upcastcm.utils.enums.WorkOrderActionEnum.getActionItemList;
import static com.uptime.upcastcm.utils.enums.WorkOrderIssueEnum.getIssueItemList;
import static com.uptime.upcastcm.utils.enums.WorkOrderPriorityEnum.getPriorityItemList;
import com.uptime.upcastcm.utils.helperclass.Filters;
import com.uptime.upcastcm.utils.vo.AlarmsVO;
import com.uptime.upcastcm.utils.vo.AssetVO;
import com.uptime.upcastcm.utils.vo.PointLocationVO;
import com.uptime.upcastcm.utils.vo.PointVO;
import com.uptime.upcastcm.utils.vo.TrendDetailsVO;
import com.uptime.upcastcm.utils.vo.UserPreferencesVO;
import com.uptime.upcastcm.utils.vo.UserSitesVO;
import com.uptime.upcastcm.utils.vo.WorkOrderVO;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.faces.component.UIViewRoot;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import static org.powermock.api.mockito.PowerMockito.when;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.primefaces.PrimeFaces;
import org.primefaces.event.NodeExpandEvent;
import org.primefaces.model.TreeNode;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.MenuModel;

/**
 *
 * @author kpati
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(PowerMockRunner.class)
@PowerMockIgnore("jdk.internal.reflect.*")
@PrepareForTest({FacesContext.class, ExternalContext.class, UserManageBean.class, ServiceRegistrarClient.class, NavigationBean.class, WorkOrderClient.class, PointClient.class, PointLocationClient.class,AssetClient.class, PrimeFaces.class, PrimeFaces.Ajax.class})
public class CreateWorkOrderBeanTest {
    
    @Mock
    ServiceRegistrarClient serviceRegistrarClient;
    @Mock
    WorkOrderClient workOrderClient;
    @Mock
    FacesContext facesContext;
    @Mock
    ExternalContext externalContext;
    @Mock
    HttpServletRequest request;
    @Mock
    HttpSession session;
    @Mock
    UIViewRoot root;
    @Mock
    NavigationBean navigationBean;
    @Mock
    UserManageBean userManageBean;
    @Mock
    PointClient pointClient;
    @Mock
    PointLocationClient pointLocationClient;
    @Mock
    AssetClient assetClient;
    
    Map<String, Object> sessionMap;
    String customerAccount;
    MenuModel breadcrumbModel;
    
    private final CreateWorkOrderBean instance;
    private final Filters filters;
    WorkOrderVO workOrderVO;
    
    public CreateWorkOrderBeanTest() {
        PowerMockito.mockStatic(FacesContext.class);
        PowerMockito.mockStatic(ServiceRegistrarClient.class);
        PowerMockito.mockStatic(WorkOrderClient.class);
        PowerMockito.mockStatic(PointClient.class);
        PowerMockito.mockStatic(PointLocationClient.class);
        PowerMockito.mockStatic(AssetClient.class);
        
        facesContext = PowerMockito.mock(FacesContext.class);
        externalContext = PowerMockito.mock(ExternalContext.class);
        request = PowerMockito.mock(HttpServletRequest.class);
        session = PowerMockito.mock(HttpSession.class);
        root = PowerMockito.mock(UIViewRoot.class);
        navigationBean = PowerMockito.mock(NavigationBean.class);
        userManageBean = PowerMockito.mock(UserManageBean.class);
        
        workOrderClient = PowerMockito.mock(WorkOrderClient.class);
        serviceRegistrarClient = PowerMockito.mock(ServiceRegistrarClient.class);
        pointClient = PowerMockito.mock(PointClient.class);
        pointLocationClient = PowerMockito.mock(PointLocationClient.class);
        assetClient = PowerMockito.mock(AssetClient.class);
        
        sessionMap = new HashMap();
        sessionMap.put("navigationBean", navigationBean);
        sessionMap.put("userManageBean", userManageBean);
        session.setAttribute("navigationBean", navigationBean);
        session.setAttribute("userManageBean", userManageBean);
        
        filters = new Filters();
       
        filters.setCountry("Test Country 1");
        filters.setRegion("Test Region 1");
        filters.setSite(new SelectItem(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), "Test Site 1"));
        filters.setArea(new SelectItem(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"), "Test Area 1"));
        filters.setAsset(new SelectItem(UUID.fromString("f33fcf5a-5f93-4504-bc09-74a61a460bf0"), "Test Asset 1"));
        when(userManageBean.getSelectedFilters()).thenReturn(filters);
        
        when(ServiceRegistrarClient.getInstance()).thenReturn(serviceRegistrarClient);
        when(WorkOrderClient.getInstance()).thenReturn(workOrderClient);
        when(PointClient.getInstance()).thenReturn(pointClient);
        when(PointLocationClient.getInstance()).thenReturn(pointLocationClient);
        when(AssetClient.getInstance()).thenReturn(assetClient);
        when(FacesContext.getCurrentInstance()).thenReturn(facesContext);
        when(facesContext.getExternalContext()).thenReturn(externalContext);
        when(externalContext.getSessionMap()).thenReturn(sessionMap);
        when(facesContext.getExternalContext().getRequest()).thenReturn(request);
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("navigationBean")).thenReturn(navigationBean);
        when(session.getAttribute("userManageBean")).thenReturn(userManageBean);
        
        when(facesContext.getViewRoot()).thenReturn(root);
        
        customerAccount = "77777";
        UserPreferencesVO userVO = new UserPreferencesVO();
        userVO.setDefaultSite(UUID.randomUUID());
        userVO.setCustomerAccount("77777");
        when(session.getAttribute("userVO")).thenReturn(userVO);
        UserSitesVO usvo = new UserSitesVO();
        usvo.setCustomerAccount(customerAccount);
        when(userManageBean.getCurrentSite()).thenReturn(usvo);
        breadcrumbModel = new DefaultMenuModel();
        when(navigationBean.getBreadcrumbModel()).thenReturn(breadcrumbModel);
        
        
        instance = new CreateWorkOrderBean();
    }
    
    @Before
    public void setUp() {
        workOrderVO = new WorkOrderVO();
    }

    /**
     * Test of init method, of class CreateWorkOrderBean.
     */
    @Test
    public void test01_Init() {
        System.out.println("init");
        
        List<SelectItem> expectedResult = new ArrayList();
        instance.init();
        List<SelectItem> result = instance.getAssetList();
        assertEquals(expectedResult, result);
        System.out.println("init Done");
    }

    /**
     * Test of init method, of class CreateWorkOrderBean.
     */
    @Test
    public void test02_Init() {
        System.out.println("init");
        List<SelectItem> expectedResult = new ArrayList();
        expectedResult.add(filters.getAsset());
        instance.init();
        List<SelectItem> result = instance.getAssetList();
        assertNotEquals(expectedResult, result);
        System.out.println("init Done");
    }

    /**
     * Test of resetPage method, of class CreateWorkOrderBean.
     */
    @Test
    public void test03_ResetPage() {
        System.out.println("resetPage");
        List<SelectItem> expectedResult = new ArrayList();
        instance.resetPage();
        List<SelectItem> result = instance.getAssetList();
        assertEquals(expectedResult, result);
        System.out.println("resetPage Done");
        
    }

    /**
     * Test of resetPage method, of class CreateWorkOrderBean.
     */
    @Test
    public void test04_ResetPage() {
        System.out.println("resetPage");
        List<SelectItem> expectedResult = new ArrayList();
        expectedResult.add(filters.getAsset());
        instance.resetPage();
        List<SelectItem> result = instance.getAssetList();
        assertNotEquals(expectedResult, result);
        System.out.println("resetPage Done");
        
    }

    /**
     * Test of presetFields method, of class CreateWorkOrderBean.
     */
    @Test
    public void test05_PresetFields_Filters_Object() {
        System.out.println("presetFields");
        Filters presets = filters;
        Object object = new AlarmsVO();
        AlarmsVO alarmVo = (AlarmsVO) object;
        alarmVo.setAssetName("Test Asset Names");
        alarmVo.setAssetId(UUID.fromString("f33fcf5a-5f93-4504-bc09-74a61a460bf0"));
        alarmVo.setPointLocationId(UUID.fromString("f33fcf5a-5f93-4504-bc09-74a61a460bf0"));
        alarmVo.setPointId(UUID.fromString("f33fcf5a-5f93-4504-bc09-74a61a460bf0"));
        alarmVo.setApSetId(UUID.fromString("f33fcf5a-5f93-4504-bc09-74a61a460bf0"));
        alarmVo.setParamName("Test Param Name 1");
        
        workOrderVO.setCustomerAccount("77777");
        workOrderVO.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"));
        workOrderVO.setAreaId(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"));
        workOrderVO.setAssetId(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"));
        workOrderVO.setPointLocationId(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"));
        workOrderVO.setPointId(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"));
        workOrderVO.setApSetId(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"));
        workOrderVO.setTrendDetails(new ArrayList());
        Whitebox.setInternalState(instance, "workOrderVO", workOrderVO);
        
        List<PointLocationVO> pointLocationVOs = new ArrayList();
        PointLocationVO plvo = new PointLocationVO();
        plvo.setPointLocationId(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"));
        plvo.setPointLocationName("Test Point Location 1");
        pointLocationVOs.add(plvo);
        
        when(pointLocationClient.getPointLocationsByPK(serviceRegistrarClient.getServiceHostURL(CONFIGURATION_SERVICE_NAME), instance.getWorkOrderVO().getCustomerAccount(), instance.getWorkOrderVO().getSiteId(), instance.getWorkOrderVO().getAreaId(), instance.getWorkOrderVO().getAssetId(), instance.getWorkOrderVO().getPointLocationId())).thenReturn(pointLocationVOs);
        
        List<PointVO> pointVOList = new ArrayList();
        PointVO pointVO = new PointVO();
        pointVO.setPointId(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"));
        pointVO.setPointName("Test Point 1");
        pointVO.setApSetId(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"));
        pointVO.setAlSetId(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"));
        pointVO.setParamType("Test Param 1");
        pointVOList.add(pointVO);
        when(pointClient.getPointsByCustomerSiteAreaAssetPointLocationPoint(serviceRegistrarClient.getServiceHostURL(CONFIGURATION_SERVICE_NAME), instance.getWorkOrderVO().getCustomerAccount(), instance.getWorkOrderVO().getSiteId(), instance.getWorkOrderVO().getAreaId(), instance.getWorkOrderVO().getAssetId(), instance.getWorkOrderVO().getPointLocationId(), instance.getWorkOrderVO().getPointId(), true)).thenReturn(pointVOList);
        instance.presetFields(presets, object);
        assertTrue(!instance.getWorkOrderVO().getTrendDetails().isEmpty());;
        System.out.println("presetFields Done");
    }

    /**
     * Test of presetFields method, of class CreateWorkOrderBean.
     */
    @Test
    public void test06_PresetFields_Filters_Object() {
        System.out.println("presetFields");
        Filters presets = filters;
        Object object = new Object();
        
         workOrderVO.setCustomerAccount("77777");
        workOrderVO.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"));
        workOrderVO.setAreaId(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"));
        workOrderVO.setAssetId(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"));
        workOrderVO.setPointLocationId(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"));
        workOrderVO.setPointId(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"));
        workOrderVO.setApSetId(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"));
        workOrderVO.setTrendDetails(new ArrayList());
        Whitebox.setInternalState(instance, "workOrderVO", workOrderVO);
        instance.presetFields(presets, object);
        String result = instance.getWorkOrderVO().getSiteName();;
        assertTrue(instance.getWorkOrderVO().getTrendDetails().isEmpty());
        
        System.out.println("presetFields Done");
    }

     /**
     * Test of presetFields method, of class CreateWorkOrderBean.
     */
    @Test
    public void test061_PresetFields_Filters_Object() {
        System.out.println("presetFields");
        Filters presets = filters;
        presets.setAsset(null);
        Object object = new Object();       
        Whitebox.setInternalState(instance, "assetPreset", true);
        instance.getAssetList().clear();
        workOrderVO.setCustomerAccount("77777");
        workOrderVO.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"));
        workOrderVO.setAreaId(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"));
        workOrderVO.setTrendDetails(new ArrayList());
        Whitebox.setInternalState(instance, "workOrderVO", workOrderVO);
        List<AssetVO>  assetVOs = new ArrayList();
        AssetVO assetVO = new AssetVO();
        assetVO.setAssetId(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"));
        assetVO.setAssetName("Test Asset 1");
        assetVOs.add(assetVO);
        when(assetClient.getAssetSiteAreaByCustomerSiteArea(serviceRegistrarClient.getServiceHostURL(CONFIGURATION_SERVICE_NAME), instance.getWorkOrderVO().getCustomerAccount(), instance.getWorkOrderVO().getSiteId(), instance.getWorkOrderVO().getAreaId())).thenReturn(assetVOs);
        instance.presetFields(presets, object);
        assertEquals(false,instance.isAssetPreset());       
        System.out.println("presetFields Done");
    }

    /**
     * Test of submit method, of class CreateWorkOrderBean.
     */
    @Test
    public void test07_Submit() {
        System.out.println("submit");
        workOrderVO.setChannelType("Test Channel Type");
        Whitebox.setInternalState(instance, "workOrderVO", workOrderVO);
        when(serviceRegistrarClient.getServiceHostURL(WORK_ORDER_SERVICE_NAME)).thenReturn(WORK_ORDER_SERVICE_NAME);
        when(workOrderClient.createOpenWorkOrder(WORK_ORDER_SERVICE_NAME, workOrderVO)).thenReturn(true);
        Whitebox.setInternalState(instance, "fromWorkOrder", true);
        PowerMockito.mockStatic(PrimeFaces.class);
        PowerMockito.mockStatic(PrimeFaces.Ajax.class);
        PrimeFaces pf = PowerMockito.mock(PrimeFaces.class);
        PrimeFaces.Ajax ax = PowerMockito.mock(PrimeFaces.Ajax.class);
        when(pf.current()).thenReturn(pf);
        when(pf.ajax()).thenReturn(ax);
        ax.addCallbackParam("workOrderFilterFormId", new Object());
        ax.addCallbackParam("workOrderResultFormId", new Object());

         NodeExpandEvent nee = PowerMockito.mock(NodeExpandEvent.class);
        TreeNode tn = PowerMockito.mock(TreeNode.class);
        when(tn.getData()).thenReturn(new String());
        List<TreeNode> tnList = new ArrayList<>();
        tnList.add(tn);
        when(navigationBean.getTreeNodeRoot()).thenReturn(tn);
        when(navigationBean.getTreeNodeRoot().getChildren()).thenReturn(tnList);
        instance.submit();
        
        WorkOrderVO expResult = workOrderVO;
        WorkOrderVO result = instance.getWorkOrderVO();
        assertEquals(expResult, result);
        System.out.println("submit Done");
    }

    /**
     * Test of submit method, of class CreateWorkOrderBean.
     */
    @Test
    public void test08_Submit() {
        System.out.println("submit");
        workOrderVO.setChannelType("Test Channel Type");
        Whitebox.setInternalState(instance, "workOrderVO", workOrderVO);
        when(serviceRegistrarClient.getServiceHostURL(WORK_ORDER_SERVICE_NAME)).thenReturn(WORK_ORDER_SERVICE_NAME);
        when(workOrderClient.createOpenWorkOrder(WORK_ORDER_SERVICE_NAME, workOrderVO)).thenReturn(true);
        Whitebox.setInternalState(instance, "fromWorkOrder", false);
        instance.submit();
        workOrderVO.setChannelType("Test Channel Type 22");
        WorkOrderVO expResult = new WorkOrderVO();
        WorkOrderVO result = instance.getWorkOrderVO();
        assertNotEquals(expResult, result);
        System.out.println("submit Done");
    }

    

    /**
     * Test of getWorkOrderVO method, of class CreateWorkOrderBean.
     */
    @Test
    public void test11_GetWorkOrderVO() {
        System.out.println("getWorkOrderVO");
        workOrderVO.setChannelType("Test Channel Type");
        Whitebox.setInternalState(instance, "workOrderVO", workOrderVO);
        WorkOrderVO expResult = workOrderVO;
        WorkOrderVO result = instance.getWorkOrderVO();
        assertEquals(expResult, result);
        System.out.println("getWorkOrderVO Done");
    }

    /**
     * Test of getWorkOrderVO method, of class CreateWorkOrderBean.
     */
    @Test
    public void test12_GetWorkOrderVO() {
        System.out.println("getWorkOrderVO");
        workOrderVO.setChannelType("Test Channel Type");
        Whitebox.setInternalState(instance, "workOrderVO", workOrderVO);
        WorkOrderVO expResult = new WorkOrderVO();
        WorkOrderVO result = instance.getWorkOrderVO();
        assertNotEquals(expResult, result);
        System.out.println("getWorkOrderVO Done");
    }

    /**
     * Test of getPriorities method, of class CreateWorkOrderBean.
     */
    @Test
    public void test25_GetPriorities() {
        System.out.println("getPriorities");
        assertEquals(instance.getPriorities().size(), getPriorityItemList().size());
        System.out.println("getPriorities Done");
    }

    /**
     * Test of getIssueList method, of class CreateWorkOrderBean.
     */
    @Test
    public void test27_GetIssueList() {
        System.out.println("getIssueList");
        assertEquals(instance.getIssueList().size(), getIssueItemList().size());
        System.out.println("getIssueList Done");
    }

    /**
     * Test of getActionList method, of class CreateWorkOrderBean.
     */
    @Test
    public void test29_GetActionList() {
        System.out.println("getActionList");
        assertEquals(instance.getActionList().size(), getActionItemList().size());
        System.out.println("getActionList Done");
    }

    /**
     * Test of presetFields method, of class CreateWorkOrderBean.
     */
    @Test(expected = UnsupportedOperationException.class)
    public void testPresetFields_int_Object() {
        System.out.println("presetFields");
        int operationType = 0;
        Object object = null;
        instance.presetFields(operationType, object);
    }

    /**
     * Test of presetFields method, of class CreateWorkOrderBean.
     */
    @Test(expected = UnsupportedOperationException.class)
    public void testPresetFields_Object() {
        System.out.println("presetFields");
        Object object = null;
        instance.presetFields(object);
    }

    /**
     * Test of updateBean method, of class CreateWorkOrderBean.
     */
    @Test
    public void testUpdateBean() {
        System.out.println("updateBean");
        instance.updateBean();
    }

    /**
     * Test of removeTrend method, of class CreateWorkOrderBean.
     */
    @Test
    public void test01_RemoveTrend() {
        System.out.println("removeTrend");
        List<TrendDetailsVO> trendDetails = new ArrayList();
        TrendDetailsVO trendDetailsVO = new TrendDetailsVO();
        trendDetailsVO.setParamName("Param 1");
        trendDetails.add(trendDetailsVO);
        instance.getWorkOrderVO().setTrendDetails(trendDetails);
        instance.removeTrend(null);
        assertTrue(!instance.getWorkOrderVO().getTrendDetails().isEmpty());
    }

    /**
     * Test of removeTrend method, of class CreateWorkOrderBean.
     */
    @Test
    public void test02_RemoveTrend() {
        System.out.println("removeTrend");
        List<TrendDetailsVO> trendDetails = new ArrayList();
        TrendDetailsVO trendDetailsVO = new TrendDetailsVO();
        trendDetailsVO.setParamName("Param 1");
        trendDetails.add(trendDetailsVO);
        instance.getWorkOrderVO().setTrendDetails(trendDetails);
        instance.removeTrend(trendDetailsVO);
        assertTrue(instance.getWorkOrderVO().getTrendDetails().isEmpty());
    }

    /**
     * Test of viewTrendGraph method, of class CreateWorkOrderBean.
     */
    // @Test 
    public void testViewTrendGraph() {
        System.out.println("viewTrendGraph");
        TrendDetailsVO trendDetailsVO = null;
        instance.viewTrendGraph(trendDetailsVO);
    }

    /**
     * Test of openAddTrendOverlay method, of class CreateWorkOrderBean.
     */
    @Test
    public void testOpenAddTrendOverlay() {
        System.out.println("openAddTrendOverlay");
        instance.openAddTrendOverlay();
    }

    /**
     * Test of onAssetChange method, of class CreateWorkOrderBean.
     */
    @Test
    public void testOnAssetChange() {
        System.out.println("onAssetChange");
        List<TrendDetailsVO> trendDetails = new ArrayList();
        TrendDetailsVO trendDetailsVO = new TrendDetailsVO();
        trendDetailsVO.setParamName("Param 1");
        trendDetails.add(trendDetailsVO);
        instance.getWorkOrderVO().setTrendDetails(trendDetails);
        instance.onAssetChange();
        assertTrue(instance.getWorkOrderVO().getTrendDetails().isEmpty());
    }
}
