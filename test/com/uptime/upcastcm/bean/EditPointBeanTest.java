/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.APALSetClient;
import com.uptime.upcastcm.http.client.AreaClient;
import com.uptime.upcastcm.http.client.AssetClient;
import com.uptime.upcastcm.http.client.PointClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import static com.uptime.upcastcm.utils.enums.DwellEnum.getDwellItemList;
import static com.uptime.upcastcm.utils.enums.FmaxEnum.getFmaxItemList;
import static com.uptime.upcastcm.utils.enums.ResolutionEnum.getResolutionItemList;
import com.uptime.upcastcm.utils.helperclass.Filters;
import com.uptime.upcastcm.utils.helperclass.PresetUtil;
import com.uptime.upcastcm.utils.vo.ApAlSetVO;
import com.uptime.upcastcm.utils.vo.PointVO;
import com.uptime.upcastcm.utils.vo.UserSitesVO;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.junit.runners.MethodSorters;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import static org.powermock.api.mockito.PowerMockito.when;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;
import org.powermock.reflect.Whitebox;
import org.primefaces.PrimeFaces;

/**
 *
 * @author joseph
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(PowerMockRunner.class)
@PowerMockIgnore("jdk.internal.reflect.*")
@PowerMockRunnerDelegate(JUnit4.class)
@PrepareForTest({FacesContext.class, ExternalContext.class, HttpServletRequest.class, HttpSession.class, UserManageBean.class, AreaClient.class, ServiceRegistrarClient.class, AssetClient.class, PointClient.class, PresetUtil.class, APALSetClient.class, PrimeFaces.class, PrimeFaces.Ajax.class})

public class EditPointBeanTest {

    @Mock
    ServiceRegistrarClient sc;

    @Mock
    AssetClient assetClient;

    @Mock
    AreaClient areaClient;

    @Mock
    PointClient pointClient;

    @Mock
    APALSetClient apALSetClient;

    @Mock
    FacesContext facesContext;

    @Mock
    ExternalContext externalContext;

    @Mock
    UserManageBean userManageBean;

    @Mock
    HttpServletRequest request;

    @Mock
    HttpSession session;

    @Mock
    PresetUtil presetUtil;

    Map<String, Object> sessionMap;
    PointVO pointVO;
    List<String> siteNames, assetNameList, sensorTypeList, pointTypeList, apSetList, alSetList;
    String customerAccount;
    List<UserSitesVO> userSiteList;
    EditPointBean instance;
    private final Filters filters;
    ApAlSetVO apAlSetVO;
    PrimeFaces pf;
    PrimeFaces.Ajax ax;

    public EditPointBeanTest() {

        PowerMockito.mockStatic(FacesContext.class);
        PowerMockito.mockStatic(ServiceRegistrarClient.class);
        PowerMockito.mockStatic(AreaClient.class);
        PowerMockito.mockStatic(PointClient.class);
        PowerMockito.mockStatic(APALSetClient.class);

        facesContext = PowerMockito.mock(FacesContext.class);
        externalContext = PowerMockito.mock(ExternalContext.class);
        userManageBean = PowerMockito.mock(UserManageBean.class);
        request = PowerMockito.mock(HttpServletRequest.class);
        presetUtil = PowerMockito.mock(PresetUtil.class);
        pointClient = PowerMockito.mock(PointClient.class);
        apALSetClient = PowerMockito.mock(APALSetClient.class);
        sc = PowerMockito.mock(ServiceRegistrarClient.class);
        areaClient = PowerMockito.mock(AreaClient.class);
        session = PowerMockito.mock(HttpSession.class);

        when(ServiceRegistrarClient.getInstance()).thenReturn(sc);
        when(AreaClient.getInstance()).thenReturn(areaClient);
        when(PointClient.getInstance()).thenReturn(pointClient);
        when(APALSetClient.getInstance()).thenReturn(apALSetClient);
        when(FacesContext.getCurrentInstance()).thenReturn(facesContext);

        when(facesContext.getExternalContext()).thenReturn(externalContext);
        when(externalContext.getRequest()).thenReturn(request);
        sessionMap = new HashMap();
        sessionMap.put("userManageBean", userManageBean);
        when(externalContext.getSessionMap()).thenReturn(sessionMap);

        when(request.getSession()).thenReturn(session);
        when(userManageBean.getPresetUtil()).thenReturn(presetUtil);

        customerAccount = "7777";

        UserSitesVO usvo = new UserSitesVO();
        usvo.setCustomerAccount(customerAccount);
        when(userManageBean.getCurrentSite()).thenReturn(usvo);

        PowerMockito.mockStatic(PrimeFaces.class);
        PowerMockito.mockStatic(PrimeFaces.Ajax.class);
        pf = PowerMockito.mock(PrimeFaces.class);
        ax = PowerMockito.mock(PrimeFaces.Ajax.class);
        when(PrimeFaces.current()).thenReturn(pf);
        when(pf.ajax()).thenReturn(ax);

        instance = new EditPointBean();

        filters = new Filters();

        filters.setCountry("Test Country 1");
        filters.setRegion("Test Region 1");
        filters.setSite(new SelectItem(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), "Test Site 1"));
        filters.setArea(new SelectItem(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf1"), "Test Area 1"));
        filters.setAsset(new SelectItem(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"), "Test Asset 1"));
        filters.setPointLocation(new SelectItem(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"), "Test Point Location 1"));
        filters.setPoint(new SelectItem(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"), "Test Point 1"));

        when(userManageBean.getSelectedFilters()).thenReturn(filters);
        userSiteList = new ArrayList<>();
        UserSitesVO uvo = new UserSitesVO();
        uvo.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        uvo.setSiteName("Test Site 1");
        userSiteList.add(uvo);
        uvo = new UserSitesVO();
        uvo.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf1"));
        uvo.setSiteName("Test Site 2");
        userSiteList.add(uvo);

        pointVO = new PointVO();
        pointVO.setCustomerAccount(customerAccount);
        pointVO.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        pointVO.setSiteName("Test Site 1");
        pointVO.setAreaId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf1"));
        pointVO.setAreaName("Test Area 1");
        pointVO.setAssetId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        pointVO.setAssetName("Test Asset 1");
        pointVO.setPointLocationId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        pointVO.setPointLocationName("Test Point Location 1");
        pointVO.setPointId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        pointVO.setPointName("Test Point 1");
        pointVO.setSensorType("Acceleration");
        pointVO.setAlSetId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        pointVO.setAlSetName("Test Al Set Name");
        pointVO.setApSetId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8242"));
        pointVO.setApSetName("Test Ap Set Name");
        pointVO.setApAlSetVOs(new ArrayList());
        Whitebox.setInternalState(instance, "pointVO", pointVO);

        apAlSetVO = new ApAlSetVO();
        apAlSetVO.setSiteName("Test Site 1");
        apAlSetVO.setSensorType("Acceleration");
        apAlSetVO.setAlSetId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        apAlSetVO.setAlSetName("Test Al Set Name");
        apAlSetVO.setApSetId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8242"));
        apAlSetVO.setApSetName("Test Ap Set Name");

    }

    /**
     * Test of init method, of class EditPointBean.
     */
    @Test
    public void test1_Init() {
        System.out.println("init");
        instance.getDisplayApAlsets().add(apAlSetVO);
        instance.init();
        assertEquals(0, instance.getDisplayApAlsets().size());
        System.out.println("init completed");
    }

    /**
     * Test of resetPage method, of class EditPointBean.
     */
    @Test
    public void test2_ResetPage() {
        System.out.println("resetPage");
        instance.getDisplayApAlsets().add(apAlSetVO);
        instance.resetPage();
        assertEquals(0, instance.getDisplayApAlsets().size());
        System.out.println("resetPage completed");
    }

    /**
     * Test of presetFields method, of class EditPointBean.
     */
    @Test
    public void test3_PresetFields_Filters_Object() {
        System.out.println("presetFields");
        List<ApAlSetVO> apAlSetVOs = new ArrayList();
        apAlSetVOs.add(apAlSetVO);
        apAlSetVO = new ApAlSetVO();
        apAlSetVO.setAlSetId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8243"));
        apAlSetVO.setAlSetName("Test Al Set Name 1");
        apAlSetVO.setApSetId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8244"));
        apAlSetVO.setApSetName("Test Ap Set Name1 ");
        List<ApAlSetVO> apAlSetVOList = new ArrayList();
        apAlSetVOList.add(apAlSetVO);
        List<PointVO> tempPointVOs = new ArrayList();
        pointVO.setApAlSetVOs(apAlSetVOs);
        tempPointVOs.add(pointVO);
        when(pointClient.getPointsByCustomerSiteAreaAssetPointLocationPointWithApsets(sc.getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), pointVO.getSiteId(), pointVO.getAreaId(), pointVO.getAssetId(), pointVO.getPointLocationId(), pointVO.getPointId())).thenReturn(tempPointVOs);
        when(apALSetClient.getGlobalApAlSetsAllByCustomerSensorType(sc.getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), pointVO.getSensorType())).thenReturn(apAlSetVOs);
        when(apALSetClient.getGlobalApAlSetsAllByCustomerSensorType(sc.getServiceHostURL(PRESET_SERVICE_NAME), DEFAULT_UPTIME_ACCOUNT, pointVO.getSensorType())).thenReturn(new ArrayList());
        when(apALSetClient.getSiteApAlSetsByCustomerSiteIdSensorType(sc.getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), pointVO.getSiteId(), pointVO.getSensorType())).thenReturn(apAlSetVOList);
        when(presetUtil.populateApSetSensorTypeList(userManageBean, "7777", pointVO.getSiteId(), pointVO.getSensorType())).thenReturn(new ArrayList());

        instance.presetFields(filters, pointVO);
        assertTrue(instance.getDisplayApAlsets().size() == 1);
        System.out.println("presetFields completed");
    }

    /**
     * Test of presetFields method, of class EditPointBean.
     */
    @Test(expected = UnsupportedOperationException.class)
    public void test4_PresetFields_int_Object() {
        System.out.println("presetFields");
        int operationType = 0;
        Object object = null;
        instance.presetFields(operationType, object);
    }

    /**
     * Test of presetFields method, of class EditPointBean.
     */
    @Test(expected = UnsupportedOperationException.class)
    public void test5_PresetFields_Object() {
        System.out.println("presetFields");
        Object object = null;
        instance.presetFields(object);
    }

    /**
     * Test of updateBean method, of class EditPointBean.
     */
    @Test
    public void test6_UpdateBean() {
        System.out.println("updateBean");
        List<PointVO> pointVOs = new ArrayList();
        pointVOs.add(pointVO);
        Whitebox.setInternalState(instance, "originalPointVOs", pointVOs);
        instance.updateBean();
        System.out.println("updateBean completed");
    }

    /**
     * Test of updatePoint method, of class EditPointBean.
     */
    @Test
    public void test8_UpdatePoint() {
        System.out.println("updatePoint");
        List<ApAlSetVO> apAlSetVOs = new ArrayList();
        apAlSetVOs.add(apAlSetVO);
        Whitebox.setInternalState(instance, "pointApAlsets", new ArrayList());
        Whitebox.setInternalState(instance, "selectedApAlSetVO", apAlSetVO);
        Whitebox.setInternalState(instance, "displayApAlsets", apAlSetVOs);
        Whitebox.setInternalState(instance, "pointVO", pointVO);
        List<PointVO> pointVOs = new ArrayList();
        pointVOs.add(pointVO);
        Whitebox.setInternalState(instance, "originalPointVOs", pointVOs);
        boolean updateTree = false;
        List<PointVO> updateList = new ArrayList();
        updateList.add(pointVO);
        when(pointClient.updatePoint(sc.getServiceHostURL(CONFIGURATION_SERVICE_NAME), updateList)).thenReturn(Boolean.TRUE);
        when(pointClient.getPointsByCustomerSiteAreaAssetPointLocationPoint(sc.getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) filters.getSite().getValue(), (UUID) filters.getArea().getValue(), (UUID) filters.getAsset().getValue(), (UUID) filters.getPointLocation().getValue(), (UUID) filters.getPoint().getValue(), true)).thenReturn(updateList);
        apAlSetVO = new ApAlSetVO();
        apAlSetVO.setAlSetId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8243"));
        apAlSetVO.setAlSetName("Test Al Set Name 1");
        apAlSetVO.setApSetId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8244"));
        apAlSetVO.setApSetName("Test Ap Set Name1 ");
        Whitebox.setInternalState(instance, "originalSelectedApAlSetVO", apAlSetVO);
        List<ApAlSetVO> apAlSetVOList = new ArrayList();
        apAlSetVOList.add(apAlSetVO);
        List<PointVO> tempPointVOs = new ArrayList();
        pointVO.setApAlSetVOs(apAlSetVOs);
        tempPointVOs.add(pointVO);
        instance.setSelectedApAlSetVO(apAlSetVO);

        when(pointClient.getPointsByCustomerSiteAreaAssetPointLocationPointWithApsets(sc.getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), pointVO.getSiteId(), pointVO.getAreaId(), pointVO.getAssetId(), pointVO.getPointLocationId(), pointVO.getPointId())).thenReturn(tempPointVOs);
        when(apALSetClient.getGlobalApAlSetsAllByCustomerSensorType(sc.getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), pointVO.getSensorType())).thenReturn(apAlSetVOs);
        when(apALSetClient.getGlobalApAlSetsAllByCustomerSensorType(sc.getServiceHostURL(PRESET_SERVICE_NAME), DEFAULT_UPTIME_ACCOUNT, pointVO.getSensorType())).thenReturn(new ArrayList());
        when(apALSetClient.getSiteApAlSetsByCustomerSiteIdSensorType(sc.getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), pointVO.getSiteId(), pointVO.getSensorType())).thenReturn(apAlSetVOList);
        when(presetUtil.populateApSetSensorTypeList(userManageBean, "7777", pointVO.getSiteId(), pointVO.getSensorType())).thenReturn(new ArrayList());

        instance.updatePoint(updateTree);
        assertSame(pointVO, instance.getPointVO());
        System.out.println("updatePoint completed");
    }

    /**
     * Test of updatePoint method, of class EditPointBean.
     */
    @Test
    public void test81_UpdatePoint() {
        System.out.println("updatePoint");
        List<ApAlSetVO> apAlSetVOs = new ArrayList();
        apAlSetVOs.add(apAlSetVO);
        Whitebox.setInternalState(instance, "pointApAlsets", new ArrayList());
        Whitebox.setInternalState(instance, "selectedApAlSetVO", apAlSetVO);
        Whitebox.setInternalState(instance, "displayApAlsets", apAlSetVOs);
        Whitebox.setInternalState(instance, "pointVO", pointVO);
        apAlSetVO = new ApAlSetVO();
        apAlSetVO.setAlSetId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8243"));
        apAlSetVO.setAlSetName("Test Al Set Name 1");
        apAlSetVO.setApSetId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8244"));
        apAlSetVO.setApSetName("Test Ap Set Name1 ");
        Whitebox.setInternalState(instance, "originalSelectedApAlSetVO", apAlSetVO);
        List<PointVO> pointVOs = new ArrayList();
        pointVO.setPointName("Test Point Name1");
        pointVOs.add(pointVO);
        Whitebox.setInternalState(instance, "originalPointVOs", pointVOs);
        boolean updateTree = true;
        List<PointVO> updateList = new ArrayList();
        updateList.add(pointVO);
        when(pointClient.updatePoint(sc.getServiceHostURL(CONFIGURATION_SERVICE_NAME), updateList)).thenReturn(Boolean.TRUE);
        instance.updatePoint(updateTree);
        // assertSame(Boolean.FALSE, instance.isValidationFailed());
        assertNotSame(pointVO, instance.getPointVO());
        System.out.println("updatePoint completed");
    }

    /**
     * Test of updatePoint method, of class EditPointBean.
     */
    @Test
    public void test82_UpdatePoint() {
        System.out.println("updatePoint");
         List<ApAlSetVO> apAlSetVOs = new ArrayList();
        apAlSetVOs.add(apAlSetVO);
        Whitebox.setInternalState(instance, "pointApAlsets", new ArrayList());
        Whitebox.setInternalState(instance, "selectedApAlSetVO", apAlSetVO);
        Whitebox.setInternalState(instance, "displayApAlsets", apAlSetVOs);
        Whitebox.setInternalState(instance, "pointVO", pointVO);
        apAlSetVO = new ApAlSetVO();
        apAlSetVO.setAlSetId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8243"));
        apAlSetVO.setAlSetName("Test Al Set Name 1");
        apAlSetVO.setApSetId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8244"));
        apAlSetVO.setApSetName("Test Ap Set Name1 ");
        Whitebox.setInternalState(instance, "originalSelectedApAlSetVO", apAlSetVO);
        List<PointVO> pointVOs = new ArrayList();
        pointVO.setPointName("Test Point Name1");
        pointVOs.add(pointVO);
        Whitebox.setInternalState(instance, "originalPointVOs", pointVOs);
        boolean updateTree = true;
        List<PointVO> updateList = new ArrayList();
        updateList.add(pointVO);
        when(pointClient.updatePoint(sc.getServiceHostURL(CONFIGURATION_SERVICE_NAME), updateList)).thenReturn(Boolean.FALSE);
        instance.updatePoint(updateTree);
        assertSame(pointVO, instance.getPointVO());
        System.out.println("updatePoint completed");
    }

    /**
     * Test of onApSetNameSelect method, of class EditPointBean.
     */
    @Test
    public void test9_OnApSetNameSelect() {
        System.out.println("onApSetNameSelect");
        Whitebox.setInternalState(instance, "pointVO", pointVO);
        instance.setRenderAlertMsg(true);
        List<ApAlSetVO> apAlSetVOs = new ArrayList();
        apAlSetVOs.add(apAlSetVO);
        when(apALSetClient.getSiteApAlSetsByCustomerSiteIdApSetId(sc.getServiceHostURL(PRESET_SERVICE_NAME), "7777", pointVO.getSiteId(), pointVO.getApSetId())).thenReturn(new ArrayList());
        when(apALSetClient.getGlobalApAlSetsByCustomerApSetId(sc.getServiceHostURL(PRESET_SERVICE_NAME), customerAccount, pointVO.getApSetId())).thenReturn(apAlSetVOs);
        when(presetUtil.populateAlSetByApAlsetList(apAlSetVOs)).thenReturn(new ArrayList());
        instance.onApSetNameSelect();
        assertTrue(instance.isRenderAlertMsg());

        System.out.println("onApSetNameSelect completed");
    }

    /**
     * Test of onApSetNameSelect method, of class EditPointBean.
     */
    @Test
    public void test91_OnApSetNameSelect() {
        System.out.println("onApSetNameSelect");
        Whitebox.setInternalState(instance, "pointVO", pointVO);
        instance.setTachValidationFailed(true);
        List<ApAlSetVO> apAlSetVOs = new ArrayList();
        apAlSetVOs.add(apAlSetVO);
        when(apALSetClient.getSiteApAlSetsByCustomerSiteIdApSetId(sc.getServiceHostURL(PRESET_SERVICE_NAME), "7777", pointVO.getSiteId(), pointVO.getApSetId())).thenReturn(new ArrayList());
        when(apALSetClient.getGlobalApAlSetsByCustomerApSetId(sc.getServiceHostURL(PRESET_SERVICE_NAME), customerAccount, pointVO.getApSetId())).thenReturn(new ArrayList());
        when(presetUtil.populateAlSetByApAlsetList(apAlSetVOs)).thenReturn(new ArrayList());
        instance.onApSetNameSelect();
        assertTrue(!instance.isTachValidationFailed());

        System.out.println("onApSetNameSelect completed");
    }

    /**
     * Test of onAlSetNameSelect method, of class EditPointBean.
     */
    @Test
    public void test10_OnAlSetNameSelect() {
        System.out.println("onAlSetNameSelect");
        List<ApAlSetVO> apAlSetVOs = new ArrayList();
        apAlSetVOs.add(apAlSetVO);
        Whitebox.setInternalState(instance, "selectedSiteApAlSetsVOList", new ArrayList());
        Whitebox.setInternalState(instance, "pointApAlsets", new ArrayList());
        Whitebox.setInternalState(instance, "siteApAlSetsVOList", apAlSetVOs);
        Whitebox.setInternalState(instance, "pointVO", pointVO);

        when(apALSetClient.getSiteApAlSetsByCustomerSiteIdApSetIdAlSetId(sc.getServiceHostURL(PRESET_SERVICE_NAME), "7777", instance.getPointVO().getSiteId(), instance.getPointVO().getApSetId(), instance.getPointVO().getAlSetId())).thenReturn(new ArrayList());
        when(apALSetClient.getGlobalApAlSetsByCustomerApSetIdAlSetId(sc.getServiceHostURL(PRESET_SERVICE_NAME), "7777", instance.getPointVO().getApSetId(), instance.getPointVO().getAlSetId())).thenReturn(apAlSetVOs);

        instance.onAlSetNameSelect();
        assertTrue(instance.isAddApalSetToPoint());
        System.out.println("onAlSetNameSelect completed");
    }

    /**
     * Test of onAlSetNameSelect method, of class EditPointBean.
     */
    @Test
    public void test101_OnAlSetNameSelect() {
        System.out.println("onAlSetNameSelect");
        List<ApAlSetVO> apAlSetVOs = new ArrayList();
        apAlSetVOs.add(apAlSetVO);
        Whitebox.setInternalState(instance, "selectedSiteApAlSetsVOList", new ArrayList());
        Whitebox.setInternalState(instance, "pointApAlsets", apAlSetVOs);
        Whitebox.setInternalState(instance, "siteApAlSetsVOList", apAlSetVOs);
        Whitebox.setInternalState(instance, "pointVO", pointVO);

        when(apALSetClient.getSiteApAlSetsByCustomerSiteIdApSetIdAlSetId(sc.getServiceHostURL(PRESET_SERVICE_NAME), "7777", instance.getPointVO().getSiteId(), instance.getPointVO().getApSetId(), instance.getPointVO().getAlSetId())).thenReturn(new ArrayList());
        when(apALSetClient.getGlobalApAlSetsByCustomerApSetIdAlSetId(sc.getServiceHostURL(PRESET_SERVICE_NAME), "7777", instance.getPointVO().getApSetId(), instance.getPointVO().getAlSetId())).thenReturn(apAlSetVOs);

        instance.onAlSetNameSelect();
        assertTrue(!instance.isAddApalSetToPoint());
        System.out.println("onAlSetNameSelect completed");
    }

    /**
     * Test of onLowFaultActiveChange method, of class EditPointBean.
     */
    @Test
    public void test11_OnLowFaultActiveChange() {
        System.out.println("onLowFaultActiveChange");
        instance.alarmLimitValidation(apAlSetVO, 1);
        System.out.println("onLowFaultActiveChange completed");
    }

    /**
     * Test of getFmaxList method, of class EditPointBean.
     */
    @Test
    public void test23_GetFmaxList() {
        System.out.println("getFmaxList");
        assertEquals(instance.getFmaxList().size(), getFmaxItemList().size());
        System.out.println("getFmaxList completed");
    }

    /**
     * Test of getDwellList method, of class EditPointBean.
     */
    @Test
    public void test24_GetDwellList() {
        System.out.println("getDwellList");
        assertEquals(instance.getDwellList().size(), getDwellItemList().size());
        System.out.println("getDwellList completed");
    }

    /**
     * Test of getResolutionList method, of class EditPointBean.
     */
    @Test
    public void test25_GetResolutionList() {
        System.out.println("getResolutionList");
        assertEquals(instance.getResolutionList().size(), getResolutionItemList().size());
        System.out.println("getResolutionList completed");
    }

}
