/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.AreaClient;
import com.uptime.upcastcm.http.client.SiteClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import static com.uptime.upcastcm.utils.enums.HourEnum.getHourItemList;
import com.uptime.upcastcm.utils.helperclass.Filters;
import com.uptime.upcastcm.utils.helperclass.PresetUtil;
import com.uptime.upcastcm.utils.vo.AreaConfigScheduleVO;
import com.uptime.upcastcm.utils.vo.AreaVO;
import com.uptime.upcastcm.utils.vo.AssetVO;
import com.uptime.upcastcm.utils.vo.DayVO;
import com.uptime.upcastcm.utils.vo.UserPreferencesVO;
import com.uptime.upcastcm.utils.vo.UserSitesVO;
import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.faces.component.UIViewRoot;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.junit.runners.MethodSorters;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import static org.powermock.api.mockito.PowerMockito.when;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;
import org.primefaces.PrimeFaces;
import org.primefaces.event.NodeExpandEvent;
import org.primefaces.model.TreeNode;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.MenuModel;

/**
 *
 * @author Joseph
 */
@PowerMockRunnerDelegate(JUnit4.class)
@RunWith(PowerMockRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@PowerMockIgnore("jdk.internal.reflect.*")
@PrepareForTest({FacesContext.class, ExternalContext.class, UserManageBean.class, ServiceRegistrarClient.class, NavigationBean.class, SiteClient.class, AreaClient.class,PresetUtil.class,ConfigurationBean.class, PrimeFaces.class, PrimeFaces.Ajax.class})
public class EditAreaBeanTest {

    @Mock
    UserManageBean userManageBean;

    @Mock
    ServiceRegistrarClient sc;

    @Mock
    AreaClient areaClient;

    @Mock
    SiteClient siteClient;

    @Mock
    FacesContext facesContext;

    @Mock
    ExternalContext externalContext;

    @Mock
    HttpServletRequest mockRequest;

    @Mock
    HttpServletRequest request;

    @Mock
    HttpSession session;
    
    @Mock
    PresetUtil presetUtil;

    @Mock
    UIViewRoot root;

    @Mock
    NavigationBean navigationBean;
    @Mock
    ConfigurationBean configurationBean;

    Map<String, Object> sessionMap;
    String customerAccount, defaultsite;
    AssetVO assetVO;
    UserPreferencesVO userVO;
    EditAreaBean instance;
    List<UserSitesVO> userSiteList;
    Filters filters;
    MenuModel breadcrumbModel;

    public EditAreaBeanTest() {
        PowerMockito.mockStatic(FacesContext.class);
        PowerMockito.mockStatic(ServiceRegistrarClient.class);
        PowerMockito.mockStatic(SiteClient.class);
        PowerMockito.mockStatic(AreaClient.class);

        facesContext = PowerMockito.mock(FacesContext.class);
        externalContext = PowerMockito.mock(ExternalContext.class);
        request = PowerMockito.mock(HttpServletRequest.class);
        session = PowerMockito.mock(HttpSession.class);
        root = PowerMockito.mock(UIViewRoot.class);
        navigationBean = PowerMockito.mock(NavigationBean.class);
        userManageBean = PowerMockito.mock(UserManageBean.class);
        siteClient = PowerMockito.mock(SiteClient.class);
        areaClient = PowerMockito.mock(AreaClient.class);
        sc = PowerMockito.mock(ServiceRegistrarClient.class);
        mockRequest = PowerMockito.mock(HttpServletRequest.class);
        presetUtil = PowerMockito.mock(PresetUtil.class);        
        configurationBean = PowerMockito.mock(ConfigurationBean.class);

        when(FacesContext.getCurrentInstance()).thenReturn(facesContext);

        when(facesContext.getExternalContext()).thenReturn(externalContext);
        when(externalContext.getRequest()).thenReturn(mockRequest);
        sessionMap = new HashMap();
        sessionMap.put("navigationBean", navigationBean);
        sessionMap.put("userManageBean", userManageBean);
        sessionMap.put("configurationBean", configurationBean);
        session.setAttribute("navigationBean", navigationBean);
        session.setAttribute("userManageBean", userManageBean);
        session.setAttribute("configurationBean", configurationBean);
        userSiteList = new ArrayList<>();
        UserSitesVO uvo = new UserSitesVO();
        uvo.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        uvo.setSiteName("Test Site 1");
        userSiteList.add(uvo);

        userVO = new UserPreferencesVO();
        userVO.setCustomerAccount(customerAccount);
        userVO.setDefaultSite(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        userVO.setDefaultSiteName("Test Site 1");

        when(ServiceRegistrarClient.getInstance()).thenReturn(sc);
        when(SiteClient.getInstance()).thenReturn(siteClient);
        when(AreaClient.getInstance()).thenReturn(areaClient);
        when(FacesContext.getCurrentInstance()).thenReturn(facesContext);
        when(facesContext.getExternalContext()).thenReturn(externalContext);
        when(externalContext.getSessionMap()).thenReturn(sessionMap);
        when(facesContext.getExternalContext().getRequest()).thenReturn(request);
         when(userManageBean.getPresetUtil()).thenReturn(presetUtil);
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("navigationBean")).thenReturn(navigationBean);
        when(session.getAttribute("userManageBean")).thenReturn(userManageBean);

        when(userManageBean.getUserSiteList()).thenReturn(userSiteList);
        when(userManageBean.getAccountUserPreferencesVO()).thenReturn(userVO);
        when(userManageBean.getCurrentSite()).thenReturn(uvo);
        when(facesContext.getViewRoot()).thenReturn(root);
        PowerMockito.mockStatic(PrimeFaces.class);
        PowerMockito.mockStatic(PrimeFaces.Ajax.class);
        PrimeFaces pf = PowerMockito.mock(PrimeFaces.class);
        PrimeFaces.Ajax ax = PowerMockito.mock(PrimeFaces.Ajax.class);
        when(PrimeFaces.current()).thenReturn(pf);
        when(pf.ajax()).thenReturn(ax);

        customerAccount = "77777";
        UserPreferencesVO userPrefVO = new UserPreferencesVO();
        userPrefVO.setDefaultSite(UUID.randomUUID());
        userPrefVO.setCustomerAccount("77777");
        when(session.getAttribute("userVO")).thenReturn(userPrefVO);
        UserSitesVO usvo = new UserSitesVO();
        usvo.setCustomerAccount(customerAccount);
        when(userManageBean.getCurrentSite()).thenReturn(usvo);
        instance = new EditAreaBean();

        filters = new Filters();
        filters.setCountry("Test Country 1");
        filters.setRegion("Test Region 1");
        filters.setSite(new SelectItem(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"), "Test Site 1"));
        filters.setArea(new SelectItem(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"), "Test Area 1"));
        filters.setAsset(new SelectItem(UUID.fromString("f33fcf5a-5f93-4504-bc09-74a61a460bf0"), "Test Asset 1"));
        when(userManageBean.getSelectedFilters()).thenReturn(filters);
    }

    @Before
    public void setUp() {
        AreaVO avo = new AreaVO();
        avo.setSiteName("SITE_1");
        List<AreaConfigScheduleVO> scheduleVOList = new ArrayList();
        avo.setScheduleVOList(scheduleVOList);
        instance.setAreaVO(avo);
        breadcrumbModel = new DefaultMenuModel();
        when(navigationBean.getBreadcrumbModel()).thenReturn(breadcrumbModel);
        NodeExpandEvent nee = PowerMockito.mock(NodeExpandEvent.class);
        TreeNode tn = PowerMockito.mock(TreeNode.class);
        List<TreeNode> tnList = new ArrayList<>();
        tnList.add(PowerMockito.mock(TreeNode.class));
        when(navigationBean.getTreeNodeRoot()).thenReturn(tn);
        when(navigationBean.getTreeNodeRoot().getChildren()).thenReturn(tnList);
    }

    /**
     * Test of init method, of class EditAreaBean.
     */
    @Test
    public void test00_Init() {
        System.out.println("init");
        instance.init();
    }

    /**
     * Test of presetFields method, of class EditAreaBean.
     */
    @Test
    public void test01_PresetFields_Filters_Object() {
        System.out.println("presetFields");
        instance.getAreaVO().setDescription("");
        when(areaClient.getAreaList(sc.getServiceHostURL(CONFIGURATION_SERVICE_NAME), instance.getAreaVO().getCustomerAccount(), instance.getAreaVO().getSiteId(), instance.getAreaVO().getAreaId())).thenReturn(instance.getAreaVO());
        instance.presetFields(filters, instance.getAreaVO());
        assertEquals("f33fcf5a-5f93-4504-bc07-74a61a460bf0", instance.getAreaVO().getSiteId().toString());
        System.out.println("presetFields completed");
    }

    /**
     * Test of presetFields method, of class EditAreaBean.
     */
    @Test(expected = UnsupportedOperationException.class)
    public void test02_PresetFields_int_Object() {
        System.out.println("presetFields");
        int operationType = 0;
        Object object = null;
        instance.presetFields(operationType, object);

    }

    /**
     * Test of presetFields method, of class EditAreaBean.
     */
    @Test(expected = UnsupportedOperationException.class)
    public void test03_PresetFields_Object() {
        System.out.println("presetFields");
        Object object = null;
        instance.presetFields(object);

    }

    /**
     * Test of onHrsChange method, of class EditAreaBean.
     */
    @Test
    public void test04OnHrsChange() {
        System.out.println("onHrsChange");
        AreaConfigScheduleVO asvo = new AreaConfigScheduleVO();
        DayVO svo = new DayVO();
        svo.setType("DB");
        instance.onHrsChange(asvo, svo);
        assertEquals(asvo.getType(), "Update");
        System.out.println("onHrsChange completed");
    }

    /**
     * Test of isContinousChange method, of class EditAreaBean.
     */
    @Test
    public void test05_IsContinousChange() {
        System.out.println("isContinousChange");
        AreaConfigScheduleVO svo = new AreaConfigScheduleVO();
        instance.isContinousChange(svo);
        assertEquals(svo.getDayVOList().get(0).getDay(), DayOfWeek.MONDAY.toString());
        System.out.println("isContinousChange completed");
    }

    /**
     * Test of updateArea method, of class EditAreaBean.
     */
    @Test
    public void test1_UpdateArea() {
        System.out.println("updateArea");

        AreaVO areaVO = new AreaVO();
        areaVO.setCustomerAccount("77777");
        areaVO.setSiteId(UUID.fromString("ddb973d4-c9ef-11ec-9d64-0242ac120002"));
        areaVO.setAreaId(UUID.fromString("ddb973d4-c9ef-11ec-9d64-0242ac120002"));
        areaVO.setAreaName("Area Test 9999");
        areaVO.setClimateControlled(false);
        areaVO.setDescription("Desc 9999");
        areaVO.setEnvironment("Test Env");

        List<AreaConfigScheduleVO> scheduleVOList = new ArrayList();
        AreaConfigScheduleVO acsvo = new AreaConfigScheduleVO();
        acsvo.setCustomerAccount("77777");
        acsvo.setSiteId(UUID.fromString("ddb973d4-c9ef-11ec-9d64-0242ac120002"));
        acsvo.setAreaId(UUID.fromString("ddb973d4-c9ef-11ec-9d64-0242ac120002"));
        acsvo.setScheduleId(UUID.fromString("ddb973d4-c9ef-11ec-9d64-0242ac120002"));
        acsvo.setAreaName("Area Test 9999");
        acsvo.setScheduleName("SCHEDULE 99");
        acsvo.setDayOfWeek((byte) 8);
        acsvo.setSchDescription("Test Desc");
        acsvo.setHoursOfDay("00:00-01:00,01:00-2:00");
        acsvo.setContinuous(false);
        acsvo.setType("New");
        instance.isContinousChange(acsvo);
        List<String> hrsList = new ArrayList();
        hrsList.add("00:00-01:00");
        hrsList.add("01:00-2:00");
        acsvo.getDayVOList().forEach(a -> {
            a.setSelectedHrsList(hrsList);
        });
        scheduleVOList.add(acsvo);
        areaVO.setScheduleVOList(scheduleVOList);

        instance.setAreaVO(areaVO);
        instance.setDeleteScheduleVOList(new ArrayList());
        when(areaClient.getAreaConfigScheduleByCustomerSiteAreaSchedule(sc.getServiceHostURL(CONFIGURATION_SERVICE_NAME), instance.getAreaVO().getCustomerAccount(), instance.getAreaVO().getSiteId(), instance.getAreaVO().getAreaId(), instance.getAreaVO().getScheduleVOList().get(0).getScheduleId())).thenReturn(scheduleVOList);
        when(areaClient.updateArea(sc.getServiceHostURL(CONFIGURATION_SERVICE_NAME), instance.getAreaVO())).thenReturn(Boolean.TRUE);

        instance.updateArea();
        assertEquals(Boolean.FALSE, instance.isValidationFailed());
        System.out.println("updateArea completed");
    }

    /**
     * Test of updateArea method, of class EditAreaBean.
     */
    @Test
    public void test2_UpdateArea() {
        System.out.println("updateArea");

        AreaVO areaVO = new AreaVO();
        areaVO.setCustomerAccount("77777");
        areaVO.setSiteId(UUID.fromString("ddb973d4-c9ef-11ec-9d64-0242ac120002"));
        areaVO.setAreaId(UUID.fromString("ddb973d4-c9ef-11ec-9d64-0242ac120002"));
        areaVO.setAreaName("Area Test 9999");
        areaVO.setClimateControlled(false);
        areaVO.setDescription("Desc 9999");
        areaVO.setEnvironment("Test Env");

        List<AreaConfigScheduleVO> scheduleVOList = new ArrayList();
        AreaConfigScheduleVO acsvo = new AreaConfigScheduleVO();
        acsvo.setCustomerAccount("77777");
        acsvo.setSiteId(UUID.fromString("ddb973d4-c9ef-11ec-9d64-0242ac120002"));
        acsvo.setAreaId(UUID.fromString("ddb973d4-c9ef-11ec-9d64-0242ac120002"));
        acsvo.setScheduleId(UUID.fromString("ddb973d4-c9ef-11ec-9d64-0242ac120002"));
        acsvo.setAreaName("Area Test 9999");
        acsvo.setScheduleName("SCHEDULE 99");
        acsvo.setDayOfWeek((byte) 2);
        acsvo.setSchDescription("Test Desc");
        acsvo.setHoursOfDay("00:00-01:00,01:00-2:00");
        acsvo.setContinuous(true);
        acsvo.setType("New");
        instance.isContinousChange(acsvo);
        List<String> hrsList = new ArrayList();
        hrsList.add("00:00-01:00");
        hrsList.add("01:00-2:00");
        acsvo.getDayVOList().forEach(a -> {
            a.setSelectedHrsList(hrsList);
        });
        scheduleVOList.add(acsvo);
        areaVO.setScheduleVOList(scheduleVOList);

        instance.setAreaVO(areaVO);
        instance.setDeleteScheduleVOList(new ArrayList());
        when(areaClient.getAreaConfigScheduleByCustomerSiteAreaSchedule(sc.getServiceHostURL(CONFIGURATION_SERVICE_NAME), instance.getAreaVO().getCustomerAccount(), instance.getAreaVO().getSiteId(), instance.getAreaVO().getAreaId(), instance.getAreaVO().getScheduleVOList().get(0).getScheduleId())).thenReturn(scheduleVOList);
        when(areaClient.updateArea(sc.getServiceHostURL(CONFIGURATION_SERVICE_NAME), instance.getAreaVO())).thenReturn(Boolean.TRUE);

        instance.updateArea();
        assertEquals(Boolean.FALSE, instance.isValidationFailed());
        System.out.println("updateArea completed");
    }

    /**
     * Test of updateArea method, of class EditAreaBean.
     */
    @Test
    public void test3_UpdateArea() {
        System.out.println("updateArea");

        AreaVO areaVO = new AreaVO();
        areaVO.setCustomerAccount("77777");
        areaVO.setSiteId(UUID.fromString("ddb973d4-c9ef-11ec-9d64-0242ac120002"));
        areaVO.setAreaId(UUID.fromString("ddb973d4-c9ef-11ec-9d64-0242ac120002"));
        areaVO.setAreaName("Area Test 9999");
        areaVO.setClimateControlled(false);
        areaVO.setDescription("Desc 9999");
        areaVO.setEnvironment("Test Env");

        List<AreaConfigScheduleVO> scheduleVOList = new ArrayList();
        AreaConfigScheduleVO acsvo = new AreaConfigScheduleVO();
        acsvo.setCustomerAccount("77777");
        acsvo.setSiteId(UUID.fromString("ddb973d4-c9ef-11ec-9d64-0242ac120002"));
        acsvo.setAreaId(UUID.fromString("ddb973d4-c9ef-11ec-9d64-0242ac120002"));
        acsvo.setScheduleId(UUID.fromString("ddb973d4-c9ef-11ec-9d64-0242ac120002"));
        acsvo.setAreaName("Area Test 9999");
        acsvo.setScheduleName("SCHEDULE 99");
        acsvo.setDayOfWeek((byte) 2);
        acsvo.setSchDescription("Test Desc");
        acsvo.setHoursOfDay("00:00-01:00,01:00-2:00");
        acsvo.setContinuous(false);
        acsvo.setType("New");
        instance.isContinousChange(acsvo);
        scheduleVOList.add(acsvo);
        areaVO.setScheduleVOList(scheduleVOList);

        instance.setAreaVO(areaVO);
        instance.setDeleteScheduleVOList(new ArrayList());
        when(areaClient.getAreaConfigScheduleByCustomerSiteAreaSchedule(sc.getServiceHostURL(CONFIGURATION_SERVICE_NAME), instance.getAreaVO().getCustomerAccount(), instance.getAreaVO().getSiteId(), instance.getAreaVO().getAreaId(), instance.getAreaVO().getScheduleVOList().get(0).getScheduleId())).thenReturn(scheduleVOList);
        when(areaClient.updateArea(sc.getServiceHostURL(CONFIGURATION_SERVICE_NAME), instance.getAreaVO())).thenReturn(Boolean.TRUE);

        instance.updateArea();
        assertEquals(Boolean.TRUE, instance.isValidationFailed());
        System.out.println("updateArea completed");
    }

    /**
     * Test of updateBean method, of class EditAreaBean.
     */
    @Test
    public void test4_UpdateBean_0args() {
        System.out.println("updateBean");
        instance.updateBean();
        System.out.println("updateBean completed");
    }

    /**
     * Test of updateBean method, of class EditAreaBean.
     */
    @Test
    public void test5_UpdateBean_AreaConfigScheduleVO() {
        System.out.println("updateBean");
        AreaConfigScheduleVO svo = new AreaConfigScheduleVO();
        instance.isContinousChange(svo);
        instance.updateBean(svo);
        System.out.println("updateBean completed");
    }

    /**
     * Test of updateClimateControlled method, of class EditAreaBean.
     */
    @Test
    public void test6_UpdateClimateControlled() {
        System.out.println("updateClimateControlled");
        instance.updateClimateControlled();
        System.out.println("updateClimateControlled completed");
    }

    /**
     * Test of addSchedule method, of class EditAreaBean.
     */
    @Test
    public void test7_AddSchedule() {
        System.out.println("addSchedule");
        List<AreaConfigScheduleVO> scheduleVOList = new ArrayList();
        instance.setScheduleVOList(scheduleVOList);
        instance.addSchedule();
        assertEquals(1, instance.getScheduleVOList().size());
        System.out.println("addSchedule completed");
    }

    /**
     * Test of deleteSchedule method, of class EditAreaBean.
     */
    @Test
    public void test8_DeleteSchedule() {
        System.out.println("deleteSchedule");
        List<AreaConfigScheduleVO> scheduleVOList = new ArrayList();
        AreaConfigScheduleVO svo = new AreaConfigScheduleVO();
        instance.isContinousChange(svo);
        scheduleVOList.add(svo);
        instance.setScheduleVOList(scheduleVOList);
        instance.setDeleteScheduleVOList(new ArrayList());
        instance.deleteSchedule(svo);
        assertEquals(1, instance.getDeleteScheduleVOList().size());
        System.out.println("deleteSchedule completed");
    }

    /**
     * Test of getHrsList method, of class EditAreaBean.
     */
    @Test
    public void test9_GetHrsList() {
        System.out.println("getHrsList");
        assertEquals(instance.getHrsList().size(), getHourItemList().size());
        System.out.println("getHrsList done");
    }
}
