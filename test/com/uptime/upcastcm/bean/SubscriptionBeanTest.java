/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.AlarmsClient;
import com.uptime.upcastcm.http.client.KafkaProxyClient;
import com.uptime.upcastcm.http.client.SubscriptionsClient;
import com.uptime.upcastcm.http.client.UserClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.CONFIGURATION_SERVICE_NAME;
import static com.uptime.upcastcm.utils.ApplicationConstants.KAFKA_PROXY_SERVICE_NAME;
import com.uptime.upcastcm.utils.vo.SubscriptionsVO;
import com.uptime.upcastcm.utils.vo.UserSitesVO;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.junit.runners.MethodSorters;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import static org.powermock.api.mockito.PowerMockito.when;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;
import org.primefaces.PrimeFaces;

/**
 *
 * @author aswan
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(PowerMockRunner.class)
@PowerMockIgnore("jdk.internal.reflect.*")
@PowerMockRunnerDelegate(JUnit4.class)
@PrepareForTest({FacesContext.class, ExternalContext.class, ServiceRegistrarClient.class, SubscriptionsClient.class, KafkaProxyClient.class, NavigationBean.class, UserManageBean.class, PrimeFaces.class, PrimeFaces.Ajax.class, UserClient.class})
public class SubscriptionBeanTest {

    @Mock
    ServiceRegistrarClient serviceRegistrarClient;

    @Mock
    FacesContext facesContext;

    @Mock
    ExternalContext externalContext;

    @Mock
    HttpServletRequest request;

    @Mock
    HttpSession session;

    @Mock
    UserManageBean userManageBean;

    @Mock
    NavigationBean navigationBean;

    @Mock
    SubscriptionsClient subscriptionsClient;

    @Mock
    KafkaProxyClient kafkaProxyClient;

    Map<String, Object> sessionMap;
    private List<SubscriptionsVO>  subscriptionsVOs ;
    SubscriptionBean instance;

    public SubscriptionBeanTest() {
        PowerMockito.mockStatic(FacesContext.class);
        PowerMockito.mockStatic(ServiceRegistrarClient.class);
        PowerMockito.mockStatic(AlarmsClient.class);
        PowerMockito.mockStatic(SubscriptionsClient.class);
        PowerMockito.mockStatic(KafkaProxyClient.class);

        facesContext = PowerMockito.mock(FacesContext.class);
        externalContext = PowerMockito.mock(ExternalContext.class);
        request = PowerMockito.mock(HttpServletRequest.class);
        session = PowerMockito.mock(HttpSession.class);
        userManageBean = PowerMockito.mock(UserManageBean.class);
        navigationBean = PowerMockito.mock(NavigationBean.class);

        serviceRegistrarClient = PowerMockito.mock(ServiceRegistrarClient.class);
        subscriptionsClient = PowerMockito.mock(SubscriptionsClient.class);
        kafkaProxyClient = PowerMockito.mock(KafkaProxyClient.class);

        sessionMap = new HashMap();
        sessionMap.put("navigationBean", navigationBean);
        sessionMap.put("userManageBean", userManageBean);

        session.setAttribute("navigationBean", navigationBean);
        session.setAttribute("userManageBean", userManageBean);
        when(FacesContext.getCurrentInstance()).thenReturn(facesContext);
        when(facesContext.getExternalContext()).thenReturn(externalContext);
        when(externalContext.getSessionMap()).thenReturn(sessionMap);
        when(ServiceRegistrarClient.getInstance()).thenReturn(serviceRegistrarClient);
        when(SubscriptionsClient.getInstance()).thenReturn(subscriptionsClient);
        when(KafkaProxyClient.getInstance()).thenReturn(kafkaProxyClient);

        when(serviceRegistrarClient.getServiceHostURL(CONFIGURATION_SERVICE_NAME)).thenReturn("host");

        UserSitesVO usvo = new UserSitesVO();
        usvo.setCustomerAccount("77777");
        usvo.setSiteId(UUID.fromString("8b086e15-371f-4a8d-bedb-c1b67b49be3e"));
        when(userManageBean.getCurrentSite()).thenReturn(usvo);
        subscriptionsVOs = new ArrayList<>();
        SubscriptionsVO svo = new SubscriptionsVO();
        svo.setCustomerAccount("77777");
        svo.setSiteId(UUID.fromString("8b086e15-371f-4a8d-bedb-c1b67b49be3e"));
        svo.setSubscriptionType("ALARMS");
        svo.setMqProtocol("ARTEMIS");
        svo.setMqConnectString("tcp://mqdev.corp.uptime-solutions.us:61616");
        svo.setMqUser("Test User");
        svo.setMqPwd("PWD");
        svo.setMqQueueName("Test Queue1");
        svo.setWebhookUrl("Trst Url");
        svo.setInternal(true);
        subscriptionsVOs.add(svo);
//        selectedSubscriptionsVOs = new ArrayList();
        svo = new SubscriptionsVO();
        svo.setCustomerAccount("77777");
        svo.setSiteId(UUID.fromString("8b086e15-371f-4a8d-bedb-c1b67b49be3e"));
        svo.setSubscriptionType("DEVICE");
        svo.setMqProtocol("ARTEMIS");
        svo.setMqConnectString("tcp://mqdev.corp.uptime-solutions.us:61617");
        svo.setMqUser("Test User");
        svo.setMqPwd("PWD");
        svo.setMqQueueName("Test Queue2");
        svo.setWebhookUrl("Trst Url2");
        svo.setInternal(true);
        subscriptionsVOs.add(svo);
//        selectedSubscriptionsVOs.add(svo);
        instance = new SubscriptionBean();
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of init method, of class SubscriptionBean.
     */
    @Test
    public void test01_Init() {
        System.out.println("init");
        instance.setSubscriptionsVOs(new ArrayList());
        when(subscriptionsClient.getSubscriptionsByCustomerSite(serviceRegistrarClient.getServiceHostURL(CONFIGURATION_SERVICE_NAME),
                "77777", UUID.fromString("8b086e15-371f-4a8d-bedb-c1b67b49be3e").toString())).thenReturn(subscriptionsVOs);
        instance.init();
        assertTrue(instance.getSubscriptionsVOs().size() > 0);
        System.out.println("init completed");
    }

    /**
     * Test of addSubscription method, of class SubscriptionBean.
     */
    @Test
    public void test02_AddSubscription() {
        System.out.println("addSubscription");
        instance.setSubscriptionsVO(null);
        instance.addSubscription();
        assertNotEquals(instance.getSubscriptionsVO(), null);
        System.out.println("addSubscription completed");
    }

    /**
     * Test of createSubscription method, of class SubscriptionBean.
     */
    @Test
    public void test03_CreateSubscription() {
       System.out.println("createSubscription");
       instance.setSubscriptionsVO(subscriptionsVOs.get(0));
       SubscriptionsVO createSubscriptionObj = instance.getSubscriptionVoObj();
        when(subscriptionsClient.createSubscriptions(serviceRegistrarClient.getServiceHostURL(CONFIGURATION_SERVICE_NAME), createSubscriptionObj)).thenReturn(true);
       
        instance.createSubscription();
        assertTrue(!instance.getSubscriptionsVOs().isEmpty());
        assertEquals(instance.getSubscriptionsVO().getCustomerAccount(), null);
        System.out.println("createSubscription completed");
    }

    /**
     * Test of updateSubscription method, of class SubscriptionBean.
     */
    @Test
    public void test04_UpdateSubscription() {
        System.out.println("updateSubscription");
        instance.setSubscriptionsVO(subscriptionsVOs.get(0));
       SubscriptionsVO createSubscriptionObj = instance.getSubscriptionVoObj();
        when(subscriptionsClient.updateSubscriptions(serviceRegistrarClient.getServiceHostURL(CONFIGURATION_SERVICE_NAME), createSubscriptionObj)).thenReturn(true);
        when(subscriptionsClient.getSubscriptionsByCustomerSite(serviceRegistrarClient.getServiceHostURL(CONFIGURATION_SERVICE_NAME),
                "77777", UUID.fromString("8b086e15-371f-4a8d-bedb-c1b67b49be3e").toString())).thenReturn(subscriptionsVOs);
        instance.setSubscriptionsVO(subscriptionsVOs.get(0));
        instance.updateSubscription();
        assertTrue(!instance.getSubscriptionsVOs().isEmpty());
        assertEquals(instance.getSubscriptionsVO().getCustomerAccount(), null);
        System.out.println("updateSubscription completed");
    }

    /**
     * Test of deleteSubscription method, of class SubscriptionBean.
     */
    @Test
    public void test09_DeleteSubscription() {
        System.out.println("deleteSubscription");
        when(subscriptionsClient.deleteSubscriptions(serviceRegistrarClient.getServiceHostURL(CONFIGURATION_SERVICE_NAME), subscriptionsVOs.get(0))).thenReturn(true);
        instance.setSubscriptionsVO(subscriptionsVOs.get(0));
        instance.deleteSubscription();
        assertEquals(instance.getSubscriptionsVO().getCustomerAccount(), null);
        System.out.println("deleteSubscription completed");
    }

    /**
     * Test of editSubscription method, of class SubscriptionBean.
     */
    @Test
    public void test05_EditSubscription() {
        System.out.println("editSubscription");
        instance.setSubscriptionsVO(null);
        instance.editSubscription(subscriptionsVOs.get(0));
        assertNotEquals(instance.getSubscriptionsVO(), null);
        System.out.println("editSubscription completed");
    }

    /**
     * Test of republishSubscriptions method, of class SubscriptionBean.
     */
    @Test
    public void test07_RepublishSubscriptions() {
        System.out.println("republishSubscriptions");
        instance.setSubscriptionsVOs(subscriptionsVOs);
        when(kafkaProxyClient.postSubscriptionsToKafka(serviceRegistrarClient.getServiceHostURL(KAFKA_PROXY_SERVICE_NAME), subscriptionsVOs)).thenReturn(Boolean.TRUE);
        instance.republishSubscriptions();
        assertTrue(!instance.getSubscriptionsVOs().isEmpty());
        System.out.println("republishSubscriptions completed");
    }

    /**
     * Test of cancleSubscriptions method, of class SubscriptionBean.
     */
    @Test
    public void test08_CancleSubscriptions() {
        System.out.println("cancleSubscriptions");
        instance.setSubscriptionsVOs(subscriptionsVOs);
        when(subscriptionsClient.cancelSubscriptions(serviceRegistrarClient.getServiceHostURL(CONFIGURATION_SERVICE_NAME), subscriptionsVOs.get(0))).thenReturn(Boolean.TRUE);
        instance.cancleSubscriptions();
        assertTrue(instance.getSubscriptionsVOs().isEmpty());
        System.out.println("cancleSubscriptions completed");
    }

    /**
     * Test of updateBean method, of class SubscriptionBean.
     */
    @Test
    public void test06_UpdateBean() {
        System.out.println("updateBean");
        instance.updateBean();
        System.out.println("updateBean completed");
    }

   

    

   
    

}
