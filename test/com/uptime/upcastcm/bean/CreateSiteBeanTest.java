/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.SiteClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import static com.uptime.upcastcm.utils.enums.ContactRoleEnum.getRoleItemList;
import static com.uptime.upcastcm.utils.enums.CountryRegionEnum.getCountryList;
import static com.uptime.upcastcm.utils.enums.IndustryEnum.getIndustryItemList;
import com.uptime.upcastcm.utils.helperclass.Filters;
import com.uptime.upcastcm.utils.vo.SiteContactVO;
import com.uptime.upcastcm.utils.vo.SiteVO;
import com.uptime.upcastcm.utils.vo.UserSitesVO;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import static org.powermock.api.mockito.PowerMockito.when;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

/**
 *
 * @author kpati
 */
@RunWith(PowerMockRunner.class)
@PowerMockIgnore("jdk.internal.reflect.*")
@PrepareForTest({FacesContext.class, ExternalContext.class, HttpServletRequest.class, HttpSession.class, UserManageBean.class, SiteClient.class, ServiceRegistrarClient.class, ConfigurationBean.class})
public class CreateSiteBeanTest {

    private final CreateSiteBean instance;

    @Mock
    private final UserManageBean userManageBean;

    @Mock
    ServiceRegistrarClient sc;

    @Mock
    SiteClient siteClient;

    @Mock
    FacesContext facesContext;

    @Mock
    ExternalContext externalContext;

    @Mock
    HttpServletRequest request;

    @Mock
    HttpSession session;

    @Mock
    ConfigurationBean configurationBean;

    Map<String, Object> sessionMap;
    SiteVO siteVO;
    String customerAccount;

    public CreateSiteBeanTest() {

        siteVO = new SiteVO();
        siteVO.setCustomerAccount("Test 77777");
        siteVO.setSiteName("Test Site 201");
        siteVO.setPhysicalAddress1("4820 Executive Park Ct");
        siteVO.setPhysicalCity("Jacksonville");
        siteVO.setPhysicalPostalCode("32216");
        siteVO.setPhysicalStateProvince("FL");
        siteVO.setCountry("UNITED STATES");
        siteVO.setRegion("NORTH AMERICA");
        siteVO.setLatitude("30.252254");
        siteVO.setLongitude("-81.602805");
        siteVO.setIndustry("Production");
        siteVO.setTimezone("Pacific/Niue (UTC -11:00)");

        PowerMockito.mockStatic(FacesContext.class);
        PowerMockito.mockStatic(SiteClient.class);
        facesContext = PowerMockito.mock(FacesContext.class);
        siteClient = PowerMockito.mock(SiteClient.class);
        externalContext = PowerMockito.mock(ExternalContext.class);
        request = PowerMockito.mock(HttpServletRequest.class);
        session = PowerMockito.mock(HttpSession.class);
        userManageBean = PowerMockito.mock(UserManageBean.class);
        configurationBean = PowerMockito.mock(ConfigurationBean.class);

        sessionMap = new HashMap();
        sessionMap.put("userManageBean", userManageBean);
        sessionMap.put("configurationBean", configurationBean);
        customerAccount = "77777";

        when(FacesContext.getCurrentInstance()).thenReturn(facesContext);
        when(facesContext.getExternalContext()).thenReturn(externalContext);
        when(externalContext.getSessionMap()).thenReturn(sessionMap);
        when(externalContext.getRequest()).thenReturn(request);
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("userManageBean")).thenReturn(userManageBean);
        when(session.getAttribute("configurationBean")).thenReturn(configurationBean);
        UserSitesVO usvo = new UserSitesVO();
        usvo.setCustomerAccount(customerAccount);
        when(userManageBean.getCurrentSite()).thenReturn(usvo);
        when(SiteClient.getInstance()).thenReturn(siteClient);
        when(siteClient.getLatLongByPhysicalAddress(siteVO.getPhysicalAddress1(), siteVO.getPhysicalCity(), siteVO.getPhysicalStateProvince(), siteVO.getPhysicalPostalCode())).thenReturn(siteVO);
        instance = new CreateSiteBean();
        List<SiteContactVO> contactList = new ArrayList();
        instance.setContactList(contactList);
        instance.setSiteVo(siteVO);
    }

    /**
     * Test of init method, of class CreateSiteBean.
     */
    @Test
    public void test01_Init() {
        System.out.println("init");
        instance.init();
        System.out.println("init completed");
    }

    /**
     * Test of resetPage method, of class CreateSiteBean.
     */
    @Test
    public void test02_ResetPage() {
        System.out.println("resetPage");
        instance.resetPage();
        System.out.println("resetPage completed");
    }

    /**
     * Test of presetFields method, of class CreateSiteBean.
     */
    @Test
    public void test03_PresetFields_Filters_Object() {
        System.out.println("presetFields");
        Filters presets = new Filters();
        presets.setRegion("Region-1");
        presets.setCountry("USA");
        Object object = null;
        instance.presetFields(presets, object);
        assertEquals(instance.getSiteVo().getRegion(), "Region-1");
        assertEquals(instance.getSiteVo().getCountry(), "USA");
    }

    /**
     * Test of presetFields method, of class CreateSiteBean.
     */
    @Test(expected = UnsupportedOperationException.class)
    public void test04_PresetFields_int_Object() {
        System.out.println("presetFields");
        int operationType = 0;
        Object object = null;
        instance.presetFields(operationType, object);
    }

    /**
     * Test of presetFields method, of class CreateSiteBean.
     */
    @Test(expected = UnsupportedOperationException.class)
    public void test05_PresetFields_Object() {
        System.out.println("presetFields");
        Object object = null;
        instance.presetFields(object);

    }

    /**
     * Test of onCountryChange method, of class CreateSiteBean.
     */
    @Test
    public void test06_OnCountryChange() {
        System.out.println("onCountryChange");
        instance.onCountryChange();
        assertEquals(instance.getSiteVo().getRegion(), "NORTH AMERICA");
    }

    /**
     * Test of getLatLongByGeoLocation method, of class CreateSiteBean.
     */
    @Test
    public void test07_GetLatLongByGeoLocation() {
        System.out.println("getLatLongByGeoLocation");
        instance.getLatLongByGeoLocation();
        assertEquals(instance.getSiteVo().getLatitude(), "30.252254");
        assertEquals(instance.getSiteVo().getLongitude(), "-81.602805");
    }

    /**
     * Test of populatePhyscAddForShipp method, of class CreateSiteBean.
     */
    @Test
    public void test08_PopulatePhyscAddForShipp_01() {
        System.out.println("populatePhyscAddForShipp");
        instance.populatePhyscAddForShipp();
        assertNull(instance.getSiteVo().getShipAddress1());
        assertNull(instance.getSiteVo().getShipAddress2());
        assertNull(instance.getSiteVo().getShipCity());
        assertNull(instance.getSiteVo().getShipStateProvince());
        assertNull(instance.getSiteVo().getShipPostalCode());
    }

    /**
     * Test of populatePhyscAddForShipp method, of class CreateSiteBean.
     */
    @Test
    public void test08_PopulatePhyscAddForShipp_02() {
        System.out.println("populatePhyscAddForShipp");
        instance.setSamePhyscAddShipChk(true);
        instance.populatePhyscAddForShipp();
        assertEquals(instance.getSiteVo().getPhysicalAddress1(), instance.getSiteVo().getShipAddress1());
        assertEquals(instance.getSiteVo().getPhysicalAddress2(), instance.getSiteVo().getShipAddress2());
        assertEquals(instance.getSiteVo().getPhysicalCity(), instance.getSiteVo().getShipCity());
        assertEquals(instance.getSiteVo().getPhysicalStateProvince(), instance.getSiteVo().getShipStateProvince());
        assertEquals(instance.getSiteVo().getPhysicalPostalCode(), instance.getSiteVo().getShipPostalCode());
    }

    /**
     * Test of populatePhyscAddForBill method, of class CreateSiteBean.
     */
    @Test
    public void test09_PopulatePhyscAddForBill_01() {
        System.out.println("populatePhyscAddForBill");
        instance.populatePhyscAddForBill();
        assertNull(instance.getSiteVo().getBillingAddress1());
        assertNull(instance.getSiteVo().getBillingAddress2());
        assertNull(instance.getSiteVo().getBillingCity());
        assertNull(instance.getSiteVo().getBillingStateProvince());
        assertNull(instance.getSiteVo().getBillingPostalCode());
    }

    /**
     * Test of populatePhyscAddForBill method, of class CreateSiteBean.
     */
    @Test
    public void test09_PopulatePhyscAddForBill_02() {
        System.out.println("populatePhyscAddForBill");
        instance.setSamePhyscAddBillChk(true);
        instance.populatePhyscAddForBill();
        assertEquals(instance.getSiteVo().getPhysicalAddress1(), instance.getSiteVo().getBillingAddress1());
        assertEquals(instance.getSiteVo().getPhysicalAddress2(), instance.getSiteVo().getBillingAddress2());
        assertEquals(instance.getSiteVo().getPhysicalCity(), instance.getSiteVo().getBillingCity());
        assertEquals(instance.getSiteVo().getPhysicalStateProvince(), instance.getSiteVo().getBillingStateProvince());
        assertEquals(instance.getSiteVo().getPhysicalPostalCode(), instance.getSiteVo().getBillingPostalCode());
    }

    /**
     * Test of updateBean method, of class CreateSiteBean.
     */
    @Test
    public void test10_UpdateBean_01() {
        System.out.println("updateBean");
        instance.updateBean();
        assertEquals(instance.getSiteVo().getLatitude(), "30.252254");
        assertEquals(instance.getSiteVo().getLongitude(), "-81.602805");
    }

    /**
     * Test of updateBean method, of class CreateSiteBean.
     */
    @Test
    public void test10_UpdateBean_02() {
        System.out.println("updateBean");
        instance.getSiteVo().setPhysicalPostalCode("1");
        instance.updateBean();
        //assertEquals(instance.getSiteVo().getLatitude(), null);
        //assertEquals(instance.getSiteVo().getLongitude(), null);
    }

    /**
     * Test of onAddNewRow method, of class CreateSiteBean.
     */
    @Test
    public void test11_OnAddNewRow() {
        System.out.println("onAddNewRow");
        instance.onAddNewRow();
        assertNotNull(instance.getSiteVo().getSiteContactList());
    }

    /**
     * Test of onDeleteRow method, of class CreateSiteBean.
     */
    @Test
    public void test12_OnDeleteRow() {
        System.out.println("onDeleteRow");
        SiteContactVO siteContactVO = new SiteContactVO();
        siteContactVO.setCustomerAccount("666666");
        siteContactVO.setSiteName("Site-1");
        instance.getContactList().add(siteContactVO);
        instance.onDeleteRow(siteContactVO);
        assertEquals(instance.getContactList().size(), 0);
        assertNotNull(instance.getSiteVo().getSiteContactList());
    }

    /**
     * Test of getCountries method, of class CreateSiteBean.
     */
    @Test
    public void test13_GetCountries() {
        System.out.println("getCountries");
        assertEquals(instance.getCountries().size(), getCountryList(true).size());
        System.out.println("getCountries completed");
    }

    /**
     * Test of isSamePhyscAddShipChk method, of class CreateSiteBean.
     */
    @Test
    public void test14_IsSamePhyscAddShipChk() {
        System.out.println("isSamePhyscAddShipChk");
        boolean expResult = false;
        boolean result = instance.isSamePhyscAddShipChk();
        assertEquals(expResult, result);
    }

    /**
     * Test of setSamePhyscAddShipChk method, of class CreateSiteBean.
     */
    @Test
    public void test15_SetSamePhyscAddShipChk() {
        System.out.println("setSamePhyscAddShipChk");
        boolean samePhyscAddShipChk = false;
        instance.setSamePhyscAddShipChk(samePhyscAddShipChk);
    }

    /**
     * Test of isSamePhyscAddBillChk method, of class CreateSiteBean.
     */
    @Test
    public void test16_IsSamePhyscAddBillChk() {
        System.out.println("isSamePhyscAddBillChk");
        boolean expResult = false;
        boolean result = instance.isSamePhyscAddBillChk();
        assertEquals(expResult, result);
    }

    @Test
    public void test17_CreateSite_01() {
        System.out.println("createSite");
        Whitebox.setInternalState(ServiceRegistrarClient.class, "instance", sc);
        when(sc.getServiceHostURL(CONFIGURATION_SERVICE_NAME)).thenReturn("config");
        when(siteClient.createSite("config", instance.getSiteVo())).thenReturn(true);
        instance.createSite();
        assertEquals(instance.getSiteVo().getSiteName(), "Test Site 201");
    }

    @Test
    public void test17_CreateSite_02() {
        System.out.println("createSite");
        Whitebox.setInternalState(ServiceRegistrarClient.class, "instance", sc);
        when(sc.getServiceHostURL(CONFIGURATION_SERVICE_NAME)).thenReturn("config");
        when(siteClient.createSite("config", instance.getSiteVo())).thenReturn(true);

        SiteContactVO siteContactVO = new SiteContactVO();
        siteContactVO.setCustomerAccount("666666");
        siteContactVO.setSiteName("Site-1");
        siteContactVO.setContactEmail("aa@google.com");
        siteContactVO.setContactPhone("564543534");
        siteContactVO.setRole("Admin");
        siteContactVO.setContactName("Uptime");
        List<SiteContactVO> siteContactList = new ArrayList();
        siteContactList.add(siteContactVO);
        instance.getSiteVo().setSiteContactList(siteContactList);
        instance.createSite();
        assertEquals(instance.getSiteVo().getSiteName(), "Test Site 201");
    }

    /**
     * Test of getIndustries method, of class CreateSiteBean.
     */
    @Test
    public void testGetIndustries() {
        System.out.println("getIndustries");
        assertEquals(instance.getIndustries().size(), getIndustryItemList().size());
        System.out.println("getIndustries completed");
    }

    /**
     * Test of getRoles method, of class CreateSiteBean.
     */
    @Test
    public void testGetRoles() {
        System.out.println("getRoles");
        assertEquals(instance.getRoles().size(), getRoleItemList().size());
        System.out.println("getRoles completed");
    }
}
