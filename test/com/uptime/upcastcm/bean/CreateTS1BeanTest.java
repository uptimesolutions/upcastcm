/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.APALSetClient;
import com.uptime.upcastcm.http.client.ApALByPointClient;
import com.uptime.upcastcm.http.client.AreaClient;
import com.uptime.upcastcm.http.client.AssetClient;
import com.uptime.upcastcm.http.client.HwUnitToPointClient;
import com.uptime.upcastcm.http.client.ManageDeviceClient;
import com.uptime.upcastcm.http.client.PointLocationClient;
import com.uptime.upcastcm.http.client.TachometerClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import com.uptime.upcastcm.utils.helperclass.PresetUtil;
import com.uptime.upcastcm.utils.singletons.CommonUtilSingleton;
import com.uptime.upcastcm.utils.vo.AssetVO;
import com.uptime.upcastcm.utils.vo.ChannelVO;
import com.uptime.upcastcm.utils.vo.PointLocationVO;
import com.uptime.upcastcm.utils.vo.PointVO;
import com.uptime.upcastcm.utils.vo.UserSitesVO;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.junit.runners.MethodSorters;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import static org.powermock.api.mockito.PowerMockito.when;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;
import org.primefaces.PrimeFaces;

/**
 *
 * @author madhavi
 */

@RunWith(PowerMockRunner.class)
@PowerMockIgnore("jdk.internal.reflect.*")
@PrepareForTest({FacesContext.class, ExternalContext.class, NavigationBean.class, UserManageBean.class, PrimeFaces.class, PrimeFaces.Ajax.class, ServiceRegistrarClient.class, APALSetClient.class, ApALByPointClient.class, HwUnitToPointClient.class, AssetClient.class, AreaClient.class, PointLocationClient.class, PresetUtil.class, CommonUtilSingleton.class, TachometerClient.class, ManageDeviceClient.class})
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@PowerMockRunnerDelegate(JUnit4.class)
public class CreateTS1BeanTest {
    
    @Mock
    ManageDeviceClient manageDeviceClient; 
    
    @Mock
    ServiceRegistrarClient serviceRegistrarClient;
    
    @Mock
    APALSetClient aPALSetClient;
            
    @Mock
    ApALByPointClient apALByPointClient;
    
    @Mock
    HwUnitToPointClient hwUnitToPointClient; 
    
    @Mock
    AssetClient assetClient; 
    
    @Mock
    AreaClient areaClient;
    
    @Mock
    PointLocationClient pointLocationClient;
    
    @Mock
    FacesContext facesContext;
    
    @Mock
    ExternalContext externalContext;
    
    @Mock
    NavigationBean navigationBean;
    
    @Mock
    UserManageBean userManageBean;
    
    @Mock
    PrimeFaces pf;
    
    @Mock
    PrimeFaces.Ajax ax;
    
    @Mock
    PresetUtil presetUtil;
    
    @Mock
    CommonUtilSingleton commonUtilSingleton;
    
    @Mock
    TachometerClient tachometerClient;
    
    private static CreateTS1Bean instance;
    private Map<String,Object> sessionMap;
    private static List<ChannelVO> channelVOList;
    
    public CreateTS1BeanTest() {
        UserSitesVO usvo;
        
        List<AssetVO> assetList = new ArrayList();
        List<PointLocationVO> plList = new ArrayList();
        List<PointVO> ptList = new ArrayList();
        
        SelectItem si = new SelectItem();

        AssetVO assetVO = new AssetVO();
        assetVO.setAreaId(UUID.fromString("2ef129fa-2d05-4dec-a84d-ed8f386ac1d0"));
        assetVO.setAreaName("Test Area Name");
        assetVO.setAssetId(UUID.fromString("0361880c-d0d8-4eb7-91be-a436eb735c75"));
        assetVO.setAssetName("Test Asset Name");
        
        PointLocationVO plvo = new PointLocationVO();
        plvo.setPointLocationName("Test PL Name");
        
        PointVO pvo = new PointVO();
        pvo.setPointName("Test PL Name VIBE");
        pvo.setSensorType("Acceleration");
        pvo.setApSetId(UUID.fromString("f33fcf5a-5f93-4504-bc08-74a61a460bf0"));
        pvo.setAlSetId(UUID.fromString("b9c92679-9376-40fc-9498-083c104edca6"));
        pvo.setApSetName("Test Ap Set Name");
        pvo.setAlSetName("Test Al Set Name");
        ptList.add(pvo);
        plvo.setPointList(ptList);
        plList.add(plvo);
        assetVO.setPointLocationList(plList);
        assetVO.setAssetName("Test Asset Name");
        assetList.add(assetVO);
        
        PowerMockito.mockStatic(ServiceRegistrarClient.class);
        PowerMockito.mockStatic(APALSetClient.class);
        PowerMockito.mockStatic(ApALByPointClient.class);
        PowerMockito.mockStatic(HwUnitToPointClient.class);
        PowerMockito.mockStatic(AssetClient.class);
        PowerMockito.mockStatic(AreaClient.class);        
        PowerMockito.mockStatic(PointLocationClient.class);
        PowerMockito.mockStatic(FacesContext.class);
        PowerMockito.mockStatic(ExternalContext.class);
        PowerMockito.mockStatic(NavigationBean.class);
        PowerMockito.mockStatic(UserManageBean.class);
        PowerMockito.mockStatic(PrimeFaces.class);
        PowerMockito.mockStatic(PrimeFaces.Ajax.class);
        PowerMockito.mockStatic(PresetUtil.class);
        PowerMockito.mockStatic(CommonUtilSingleton.class);
        PowerMockito.mockStatic(TachometerClient.class);
        PowerMockito.mockStatic(ManageDeviceClient.class);
        
        serviceRegistrarClient = PowerMockito.mock(ServiceRegistrarClient.class);
        assetClient = PowerMockito.mock(AssetClient.class);
        facesContext = PowerMockito.mock(FacesContext.class);
        externalContext = PowerMockito.mock(ExternalContext.class);
        navigationBean = PowerMockito.mock(NavigationBean.class);
        userManageBean = PowerMockito.mock(UserManageBean.class);
        presetUtil = PowerMockito.mock(PresetUtil.class);
        
        pf = PowerMockito.mock(PrimeFaces.class);
        ax = PowerMockito.mock(PrimeFaces.Ajax.class);
        
        when(PrimeFaces.current()).thenReturn(pf);
        when(pf.ajax()).thenReturn(ax);
        
        sessionMap = new HashMap();
        sessionMap.put("navigationBean", navigationBean);
        sessionMap.put("userManageBean", userManageBean);
        
        when(FacesContext.getCurrentInstance()).thenReturn(facesContext);
        when(facesContext.getExternalContext()).thenReturn(externalContext);
        when(externalContext.getSessionMap()).thenReturn(sessionMap);
        
        usvo = new UserSitesVO();
        usvo.setCustomerAccount("FEDEX EXPRESS");
        usvo.setSiteId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        when(userManageBean.getCurrentSite()).thenReturn(usvo);
        when(ServiceRegistrarClient.getInstance()).thenReturn(serviceRegistrarClient);
        when(serviceRegistrarClient.getServiceHostURL(CONFIGURATION_SERVICE_NAME)).thenReturn("HOST");
        when(AssetClient.getInstance()).thenReturn(assetClient);
        //when(assetClient.getAssetSiteAreaByCustomerSiteArea("HOST", userManageBean.getCurrentSite().getCustomerAccount(), userManageBean.getCurrentSite().getSiteId(), assetVO.getAreaId())).thenReturn(assetList);
        //when(manageDeviceClient.createTS1Device("HOST", assetList)).thenReturn(true);
        
        instance = new CreateTS1Bean();
    }
    
    @Before
    public void setUp() {
        channelVOList = new ArrayList();
        instance.setChannelVOList(channelVOList);
    }

    /**
     * Test of init method, of class CreateTS1Bean.
     */
    @Test
    public void test00_Init() {
        System.out.println("init");
        instance.init();
        assertEquals(instance.getChannelVOList().size(), 0);
        System.out.println("init done");
    }

    /**
     * Test of resetPage method, of class CreateTS1Bean.
     */
    @Test
    public void test01_ResetPage() {
        System.out.println("resetPage");
        instance.resetPage();
        assertEquals(instance.getChannelVOList().size(), 0);
        System.out.println("resetPage done");
    }

    /**
     * Test of addChannel method, of class CreateTS1Bean.
     */
    @Test
    public void test02_AddChannel() {
        System.out.println("addChannel");
        instance.addChannel();
        System.out.println("addChannel done");
    }


    /**
     * Test of viewInfo method, of class CreateTS1Bean.
     */
    @Test
    public void test14_ViewInfo() {
        System.out.println("viewInfo");
        ChannelVO cvo = new ChannelVO();
        List<ChannelVO> cvoList =  new ArrayList<>();
        cvoList.add(cvo);
        instance.setChannelVOList(cvoList);
        instance.viewInfo(instance.getChannelVOList().get(0));
        assertEquals(cvo, instance.getChannelVOList().get(0));
        System.out.println("viewInfo done");
    }
    
    /**
     * Test of delete method, of class CreateTS1Bean.
     */
    @Test
    public void test15_Delete() {
        System.out.println("delete");
        ChannelVO cvo = new ChannelVO();
        List<ChannelVO> cvoList =  new ArrayList<>();
        cvo.setAssetId(UUID.fromString("0361880c-d0d8-4eb7-91be-a436eb735c75"));
        cvoList.add(cvo);
        instance.setChannelVOList(cvoList);
        assertEquals(instance.getChannelVOList().size(), 1);
        instance.delete(instance.getChannelVOList().get(0));
        assertEquals(instance.getChannelVOList().size(), 0);
        System.out.println("delete done");
    }

    /**
     * Test of createTS1 method, of class CreateTS1Bean.
     */
    @Test
    public void testCreateTS1() {
        System.out.println("createTS1");
        List<AssetVO> newAssetVOList= new ArrayList<>();
        newAssetVOList.add(new AssetVO());
        newAssetVOList.get(0).setAssetId(UUID.fromString("0361880c-d0d8-4eb7-91be-a436eb735c75"));
        when(manageDeviceClient.createTS1Device("HOST", newAssetVOList)).thenReturn(true);
        instance.createTS1();
        System.out.println("createTS1 done");
    }

}
