/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.AlarmNotificationClient;
import com.uptime.upcastcm.http.client.AssetClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import com.uptime.upcastcm.utils.vo.AlarmNotificationVO;
import com.uptime.upcastcm.utils.vo.AssetVO;
import com.uptime.upcastcm.utils.vo.UserPreferencesVO;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.junit.runners.MethodSorters;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;
import org.primefaces.PrimeFaces;

/**
 *
 * @author joseph
 */
@RunWith(PowerMockRunner.class)
@PowerMockIgnore("jdk.internal.reflect.*")
@PrepareForTest({FacesContext.class, ExternalContext.class, UserManageBean.class, ServiceRegistrarClient.class, AssetClient.class, AlarmNotificationClient.class, PrimeFaces.class, PrimeFaces.Ajax.class})
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@PowerMockRunnerDelegate(JUnit4.class)
public class AlarmNotificationBeanTest {

    @Mock
    FacesContext facesContext;

    @Mock
    ExternalContext externalContext;

    @Mock
    UserManageBean userManageBean;

    @Mock
    ServiceRegistrarClient serviceRegistrarClient;

    @Mock
    AssetClient assetClient;

    @Mock
    AlarmNotificationClient alarmNotificationClient;
    @Mock
    PrimeFaces pf;
    @Mock
    PrimeFaces.Ajax ax;

    AlarmNotificationBean instance;
    Map<String, Object> sessionMap;
    AlarmNotificationVO alarmNotificationVO;
    UserPreferencesVO userPreferencesVO;

    public AlarmNotificationBeanTest() {
        PowerMockito.mockStatic(FacesContext.class);
        PowerMockito.mockStatic(PrimeFaces.class);
        PowerMockito.mockStatic(PrimeFaces.Ajax.class);
        PowerMockito.mockStatic(ServiceRegistrarClient.class);
        PowerMockito.mockStatic(AssetClient.class);
        PowerMockito.mockStatic(AlarmNotificationClient.class);

        facesContext = PowerMockito.mock(FacesContext.class);
        externalContext = PowerMockito.mock(ExternalContext.class);
        userManageBean = PowerMockito.mock(UserManageBean.class);
        serviceRegistrarClient = PowerMockito.mock(ServiceRegistrarClient.class);
        assetClient = PowerMockito.mock(AssetClient.class);
        alarmNotificationClient = PowerMockito.mock(AlarmNotificationClient.class);
        pf = PowerMockito.mock(PrimeFaces.class);
        ax = PowerMockito.mock(PrimeFaces.Ajax.class);
        when(pf.current()).thenReturn(pf);
        when(pf.current().ajax()).thenReturn(ax);

        sessionMap = new HashMap();
        sessionMap.put("userManageBean", userManageBean);

        when(FacesContext.getCurrentInstance()).thenReturn(facesContext);
        when(facesContext.getExternalContext()).thenReturn(externalContext);
        when(externalContext.getSessionMap()).thenReturn(sessionMap);
        when(AssetClient.getInstance()).thenReturn(assetClient);
        when(AlarmNotificationClient.getInstance()).thenReturn(alarmNotificationClient);
        when(ServiceRegistrarClient.getInstance()).thenReturn(serviceRegistrarClient);

        instance = new AlarmNotificationBean();
        instance.setSiteId("e4c66964-5a8c-451d-ae2e-9d04d83c8241");

        alarmNotificationVO = new AlarmNotificationVO();
        alarmNotificationVO.setUserId("sam.sapde@uptime-solution.us");
        alarmNotificationVO.setAssetId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        alarmNotificationVO.setSiteId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        alarmNotificationVO.setEmailAddress("sam.sapde@uptime-solution.us");
        instance.setAlarmNotificationVO(alarmNotificationVO);

        userPreferencesVO = new UserPreferencesVO();
        userPreferencesVO.setCustomerAccount("77777");
        userPreferencesVO.setDefaultSite(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        userPreferencesVO.setUserId("sam.spade@uptime-solution.us");
        instance.setUserPreferencesVO(userPreferencesVO);
    }

    /**
     * Test of reset method, of class AlarmNotificationBean.
     */
    @Test
    public void testReset() {
        System.out.println("reset");
        instance.reset();
        System.out.println("reset completed");
    }

    /**
     * Test of onSiteChange method, of class AlarmNotificationBean.
     */
    @Test
    public void testOnSiteChange() {
        System.out.println("onSiteChange");
        instance.setSiteId("e4c66964-5a8c-451d-ae2e-9d04d83c8241");
        instance.setAssetVOList(new ArrayList());
        UserPreferencesVO userPreferencesVO = new UserPreferencesVO();
        userPreferencesVO.setCustomerAccount("77777");
        userPreferencesVO.setUserId("sam.spade@uptime-solution.us");
        instance.setUserPreferencesVO(userPreferencesVO);
        List<AssetVO> assetVOList = new ArrayList();
        AssetVO assetVO = new AssetVO();
        assetVO.setAssetId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        List<AlarmNotificationVO> alNoteList = new ArrayList();
        alNoteList.add(alarmNotificationVO);
        when(alarmNotificationClient.getUserSiteListByUserIdSiteId(serviceRegistrarClient.getServiceHostURL(USER_PREFERENCE_SERVICE_NAME), instance.getUserPreferencesVO().getUserId(), instance.getSiteId())).thenReturn(alNoteList);
        when(assetClient.getAssetSiteAreaByCustomerSite(serviceRegistrarClient.getServiceHostURL(CONFIGURATION_SERVICE_NAME), instance.getUserPreferencesVO().getCustomerAccount(), instance.getAlarmNotificationVO().getSiteId().toString())).thenReturn(instance.getAssetVOList());
        instance.onSiteChange();
        assertTrue(instance.getAssetIds().size() > 0);
        System.out.println("onSiteChange completed");
    }

    /**
     * Test of onSiteChange method, of class AlarmNotificationBean.
     */
    @Test
    public void test1OnSiteChange() {
        System.out.println("onSiteChange");
        instance.setAssetIds(new HashSet());

        instance.setAssetVOList(new ArrayList());
        AssetVO assetVO = new AssetVO();
        assetVO.setAssetId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
       
        when(alarmNotificationClient.getUserSiteListByUserIdSiteId(serviceRegistrarClient.getServiceHostURL(USER_PREFERENCE_SERVICE_NAME), instance.getUserPreferencesVO().getUserId(), instance.getSiteId())).thenReturn(new ArrayList());
        when(assetClient.getAssetSiteAreaByCustomerSite(serviceRegistrarClient.getServiceHostURL(CONFIGURATION_SERVICE_NAME), instance.getUserPreferencesVO().getCustomerAccount(), instance.getAlarmNotificationVO().getSiteId().toString())).thenReturn(instance.getAssetVOList());
        instance.onSiteChange();
        assertTrue(instance.getAssetIds().isEmpty());
        System.out.println("onSiteChange completed");
    }

    /**
     * Test of saveAlarmNotification method, of class AlarmNotificationBean.
     */
    @Test
    public void testSaveAlarmNotification() {
        System.out.println("saveAlarmNotification");
        instance.setAssetIds(new HashSet());
        instance.setInitialAssets(new HashSet());
        when(alarmNotificationClient.createAlarmNotification(serviceRegistrarClient.getServiceHostURL(USER_PREFERENCE_SERVICE_NAME), instance.getAlarmNotificationVO())).thenReturn(Boolean.TRUE);
        instance.saveAlarmNotification();
        assertTrue(instance.getAlarmNotificationVO().getSiteId().compareTo(userPreferencesVO.getDefaultSite()) == 0);
        System.out.println("saveAlarmNotification completed");
    }

    /**
     * Test of saveAlarmNotification method, of class AlarmNotificationBean.
     */
    @Test
    public void test1SaveAlarmNotification() {
        System.out.println("saveAlarmNotification");
        instance.setAssetIds(new HashSet());
        instance.setInitialAssets(new HashSet());
        instance.setCreateNotification(true);
        // alarmNotificationVO.setSiteId(null);
        instance.setAlarmNotificationVO(alarmNotificationVO);
        when(alarmNotificationClient.createAlarmNotification(serviceRegistrarClient.getServiceHostURL(USER_PREFERENCE_SERVICE_NAME), instance.getAlarmNotificationVO())).thenReturn(Boolean.TRUE);
        instance.saveAlarmNotification();
        assertTrue(instance.getAlarmNotificationVO().getSiteId().compareTo(userPreferencesVO.getDefaultSite()) == 0);
        System.out.println("saveAlarmNotification completed");
    }

    /**
     * Test of saveAlarmNotification method, of class AlarmNotificationBean.
     */
    @Test
    public void test2SaveAlarmNotification() {
        System.out.println("saveAlarmNotification");
        instance.setAssetIds(new HashSet());
        instance.setInitialAssets(new HashSet());
        instance.setSelectAll(true);
        when(alarmNotificationClient.updateAlarmNotification(serviceRegistrarClient.getServiceHostURL(USER_PREFERENCE_SERVICE_NAME), instance.getAlarmNotificationVO())).thenReturn(Boolean.TRUE);
        instance.saveAlarmNotification();
        assertTrue(instance.getAlarmNotificationVO().getSiteId().compareTo(userPreferencesVO.getDefaultSite()) == 0);
        System.out.println("saveAlarmNotification completed");
    }

    /**
     * Test of updateBean method, of class AlarmNotificationBean.
     */
    @Test
    public void testUpdateBean() {
        System.out.println("updateBean");
        instance.setSiteId(null);
        instance.updateBean();
        assertTrue(instance.isEnableCreate());
        System.out.println("updateBean completed");
    }

    /**
     * Test of updateBean method, of class AlarmNotificationBean.
     */
    @Test
    public void test1UpdateBean() {
        System.out.println("updateBean");
        instance.setSiteId("e4c66964-5a8c-451d-ae2e-9d04d83c8241");
        instance.setSelectAll(true);
        instance.updateBean();
        assertFalse(instance.isEnableCreate());
        System.out.println("updateBean completed");
    }

    /**
     * Test of updateBean method, of class AlarmNotificationBean.
     */
    @Test
    public void test2UpdateBean() {
        System.out.println("updateBean");
        instance.setSiteId("e4c66964-5a8c-451d-ae2e-9d04d83c8241");
        instance.setSelectAll(false);
        instance.setAssetIds(new HashSet());
        instance.updateBean();
        assertTrue(instance.isEnableCreate());
        System.out.println("updateBean completed");
    }

}
