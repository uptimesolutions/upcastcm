/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.AssetClient;
import com.uptime.upcastcm.http.client.ManageDeviceClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import com.uptime.upcastcm.utils.helperclass.PresetUtil;
import com.uptime.upcastcm.utils.vo.ChannelVO;
import com.uptime.upcastcm.utils.vo.UserSitesVO;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.junit.runners.MethodSorters;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import static org.powermock.api.mockito.PowerMockito.when;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;
import org.primefaces.PrimeFaces;

/**
 *
 * @author twilcox
 */
@RunWith(PowerMockRunner.class)
@PowerMockIgnore("jdk.internal.reflect.*")
@PrepareForTest({FacesContext.class, ExternalContext.class, NavigationBean.class, UserManageBean.class, PrimeFaces.class, PrimeFaces.Ajax.class, ServiceRegistrarClient.class, ManageDeviceClient.class, PresetUtil.class, AssetClient.class})
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@PowerMockRunnerDelegate(JUnit4.class)
public class EditTS1BeanTest {
    
    @Mock
    ServiceRegistrarClient serviceRegistrarClient;
    
    @Mock
    ManageDeviceClient manageDeviceClient; 
    
    @Mock
    AssetClient assetClient; 
    
    @Mock
    FacesContext facesContext;
    
    @Mock
    ExternalContext externalContext;
    
    @Mock
    NavigationBean navigationBean;
    
    @Mock
    UserManageBean userManageBean;
    
    @Mock
    AddChannelBean addChannelBean;
    
    @Mock
    PresetUtil presetUtil;
    
    @Mock
    PrimeFaces pf;
    
    @Mock
    PrimeFaces.Ajax ax;
    
    private final EditTS1Bean instance;
    private Map<String,Object> sessionMap;
    private List<ChannelVO> channelVOList;
    private ChannelVO channelVO;
    
    public EditTS1BeanTest() {
        UserSitesVO usvo;
        
        PowerMockito.mockStatic(ServiceRegistrarClient.class);
        PowerMockito.mockStatic(ManageDeviceClient.class);
        PowerMockito.mockStatic(AssetClient.class);
        PowerMockito.mockStatic(FacesContext.class);
        PowerMockito.mockStatic(ExternalContext.class);
        PowerMockito.mockStatic(NavigationBean.class);
        PowerMockito.mockStatic(UserManageBean.class);
        PowerMockito.mockStatic(PrimeFaces.class);
        PowerMockito.mockStatic(PrimeFaces.Ajax.class);
        
        serviceRegistrarClient = PowerMockito.mock(ServiceRegistrarClient.class);
        manageDeviceClient = PowerMockito.mock(ManageDeviceClient.class);
        assetClient = PowerMockito.mock(AssetClient.class);
        facesContext = PowerMockito.mock(FacesContext.class);
        externalContext = PowerMockito.mock(ExternalContext.class);
        navigationBean = PowerMockito.mock(NavigationBean.class);
        userManageBean = PowerMockito.mock(UserManageBean.class);
        addChannelBean = PowerMockito.mock(AddChannelBean.class);
        presetUtil = PowerMockito.mock(PresetUtil.class);
        pf = PowerMockito.mock(PrimeFaces.class);
        ax = PowerMockito.mock(PrimeFaces.Ajax.class);
        
        when(PrimeFaces.current()).thenReturn(pf);
        when(pf.ajax()).thenReturn(ax);
        
        sessionMap = new HashMap();
        sessionMap.put("navigationBean", navigationBean);
        sessionMap.put("userManageBean", userManageBean);
        sessionMap.put("addChannelBean", addChannelBean);
        
        when(FacesContext.getCurrentInstance()).thenReturn(facesContext);
        when(facesContext.getExternalContext()).thenReturn(externalContext);
        when(externalContext.getSessionMap()).thenReturn(sessionMap);
        when(userManageBean.getPresetUtil()).thenReturn(presetUtil);
        
        usvo = new UserSitesVO();
        usvo.setCustomerAccount("FEDEX EXPRESS");
        usvo.setSiteId(UUID.fromString("8b086e15-371f-4a8d-bedb-c1b67b49be3e"));
        when(userManageBean.getCurrentSite()).thenReturn(usvo);
        
        when(ServiceRegistrarClient.getInstance()).thenReturn(serviceRegistrarClient);
        when(serviceRegistrarClient.getServiceHostURL(CONFIGURATION_SERVICE_NAME)).thenReturn("HOST");
        when(ManageDeviceClient.getInstance()).thenReturn(manageDeviceClient);
        when(AssetClient.getInstance()).thenReturn(assetClient);
        
        instance = new EditTS1Bean();
    }
    
    @Before
    public void setUp() {
        channelVO = new ChannelVO();
        channelVO.setCustomerAccount("FEDEX EXPRESS");
        channelVO.setDeviceId("00100000");
        channelVO.setHwDisabled(false);
        
        channelVOList = new ArrayList();
        channelVOList.add(channelVO);
    }

    /**
     * Test of init method, of class EditTS1Bean.
     */
    @Test
    public void test00_Init() {
        System.out.println("init");
        
        instance.setSerialNumber("00100000");
        instance.init();
        assertNull(instance.getSerialNumber());
        
        System.out.println("init Completed");
    }

    /**
     * Test of resetPage method, of class EditTS1Bean.
     */
    @Test
    public void test01_ResetPage() {
        System.out.println("resetPage");
        
        instance.setSerialNumber("00100000");
        instance.resetPage();
        assertNull(instance.getSerialNumber());
        
        System.out.println("resetPage Completed");
    }

    /**
     * Test of presetFields method, of class EditTS1Bean.
     */
    @Test
    public void test02_PresetFields_Object() {
        System.out.println("presetFields");
        
        when(ManageDeviceClient.getInstance().getDeviceManagementRowsByCustomerSiteIdDevice("HOST", userManageBean.getCurrentSite().getCustomerAccount(), UUID.fromString("8b086e15-371f-4a8d-bedb-c1b67b49be3e"), "00100000", true)).thenReturn(channelVOList);
        instance.presetFields("00100000");
        assertEquals(instance.getChannelVOList().toString(), channelVOList.toString());
        
        System.out.println("presetFields Completed");
    }

    /**
     * Test of addChannel method, of class EditTS1Bean.
     */
    @Test
    public void test03_AddChannel() {
        System.out.println("addChannel");
        
        instance.addChannel();
        assertTrue(sessionMap.containsKey("addChannelBean"));
        
        System.out.println("addChannel Completed");
    }

    /**
     * Test of viewInfo method, of class EditTS1Bean.
     */
    @Test
    public void test04_ViewInfo() {
        System.out.println("viewInfo");
        
        instance.viewInfo(channelVO);
        assertTrue(sessionMap.containsKey("deviceManagementInfoRowBean"));
        assertEquals(channelVO, ((DeviceManagementInfoRowBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("deviceManagementInfoRowBean")).getChannelVO());
        
        System.out.println("viewInfo Completed");
    }

    /**
     * Test of editChannel method, of class EditTS1Bean.
     */
    @Test
    public void test05_EditChannel() {
        System.out.println("editChannel");
        
        when(ManageDeviceClient.getInstance().getDeviceManagementRowsByCustomerSiteIdDevice("HOST", userManageBean.getCurrentSite().getCustomerAccount(), UUID.fromString("8b086e15-371f-4a8d-bedb-c1b67b49be3e"), "00100000", true)).thenReturn(channelVOList);
        instance.presetFields("00100000");
         channelVO.setAlarmEnabledValue("Test");
        instance.editChannel(channelVO);
        assertTrue(sessionMap.containsKey("editChannelBean"));
        assertEquals(channelVO, ((EditChannelBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("editChannelBean")).getChannelVO());
        
        System.out.println("editChannel Completed");
    }

    /**
     * Test of delete method, of class EditTS1Bean.
     */
    @Test
    public void test06_Delete() {
        System.out.println("delete");
        
        when(ManageDeviceClient.getInstance().getDeviceManagementRowsByCustomerSiteIdDevice("HOST", userManageBean.getCurrentSite().getCustomerAccount(), UUID.fromString("8b086e15-371f-4a8d-bedb-c1b67b49be3e"), "00100000", true)).thenReturn(channelVOList);
        instance.presetFields("00100000");
        assertEquals(instance.getChannelVOList().toString(), channelVOList.toString());
        instance.delete(channelVO);
        assertEquals(instance.getChannelVOList().size(), 0);
        
        System.out.println("delete Completed");
    }

    /**
     * Test of editTS1 method, of class EditTS1Bean.
     */
    @Test
    public void test07_EditTS1() {
        System.out.println("editTS1");
        
        // There is nothing to check because everythings is being sent to the service.
        instance.editTS1();
        
        System.out.println("editTS1 Completed");
    }

    /**
     * Test of getSerialNumber method, of class EditTS1Bean.
     */
    @Test
    public void test08_GetSerialNumber() {
        System.out.println("getSerialNumber");
        
        when(ManageDeviceClient.getInstance().getDeviceManagementRowsByCustomerSiteIdDevice("HOST", userManageBean.getCurrentSite().getCustomerAccount(), UUID.fromString("8b086e15-371f-4a8d-bedb-c1b67b49be3e"), "00100000", true)).thenReturn(channelVOList);
        instance.presetFields("00100000");
        assertEquals(instance.getSerialNumber(),"00100000");
        
        System.out.println("getSerialNumber Completed");
    }

    /**
     * Test of setSerialNumber method, of class EditTS1Bean.
     */
    @Test
    public void test09_SetSerialNumber() {
        System.out.println("setSerialNumber");
        
        instance.setSerialNumber("00100000");
        assertEquals(instance.getSerialNumber(),"00100000");
        
        System.out.println("setSerialNumber Completed");
    }

    /**
     * Test of getChannelVOList method, of class EditTS1Bean.
     */
    @Test
    public void test10_GetChannelVOList() {
        System.out.println("getChannelVOList");
        
        when(ManageDeviceClient.getInstance().getDeviceManagementRowsByCustomerSiteIdDevice("HOST", userManageBean.getCurrentSite().getCustomerAccount(), UUID.fromString("8b086e15-371f-4a8d-bedb-c1b67b49be3e"), "00100000", true)).thenReturn(channelVOList);
        instance.presetFields("00100000");
        assertEquals(instance.getChannelVOList().toString(), channelVOList.toString());
        
        System.out.println("getChannelVOList Completed");
    }
    
}
