/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.dao.PrestoDAO;
import com.uptime.upcastcm.http.client.AreaClient;
import com.uptime.upcastcm.http.client.AssetClient;
import com.uptime.upcastcm.http.client.PointClient;
import com.uptime.upcastcm.http.client.PointLocationClient;
import com.uptime.upcastcm.http.client.SiteClient;
import com.uptime.upcastcm.http.client.TrendDataClient;
import com.uptime.upcastcm.http.client.WorkOrderClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import com.uptime.upcastcm.utils.helperclass.Filters;
import com.uptime.upcastcm.utils.vo.AreaVO;
import com.uptime.upcastcm.utils.vo.AssetVO;
import com.uptime.upcastcm.utils.vo.PointLocationVO;
import com.uptime.upcastcm.utils.vo.PointVO;
import com.uptime.upcastcm.utils.vo.SiteVO;
import com.uptime.upcastcm.utils.vo.TrendValuesVO;
import com.uptime.upcastcm.utils.vo.UserSitesVO;
import com.uptime.upcastcm.utils.vo.WorkOrderVO;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.faces.component.UIComponent;
import javax.faces.component.UIViewRoot;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.Mock;
import static org.mockito.Mockito.mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import static org.powermock.api.mockito.PowerMockito.when;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.primefaces.PrimeFaces;
import org.primefaces.event.NodeCollapseEvent;
import org.primefaces.event.NodeExpandEvent;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.TreeNode;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.MenuModel;

/**
 *
 * @author twilcox
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(PowerMockRunner.class)
@PowerMockIgnore("jdk.internal.reflect.*")
@PrepareForTest({FacesContext.class, ExternalContext.class, ServiceRegistrarClient.class, NavigationBean.class, UserManageBean.class, SiteClient.class, AreaClient.class, AssetClient.class, WorkOrderClient.class, TrendDataClient.class, PrimeFaces.class, PrimeFaces.Ajax.class, UIComponent.class, UIViewRoot.class, PointLocationClient.class, PointClient.class, PrestoDAO.class})
public class WorkOrderBeanTest {

    @Mock
    ServiceRegistrarClient serviceRegistrarClient;

    @Mock
    SiteClient siteClient;

    @Mock
    AreaClient areaClient;

    @Mock
    AssetClient assetClient;

    @Mock
    WorkOrderClient workOrderClient;

    @Mock
    TrendDataClient trendDataClient;

    @Mock
    FacesContext facesContext;

    @Mock
    ExternalContext externalContext;

    @Mock
    NavigationBean navigationBean;

    @Mock
    UserManageBean userManageBean;

    @Mock
    UIComponent uIComponent;

    @Mock
    UIViewRoot uiViewRoot;

    @Mock
    PointLocationClient pointLocationClient;

    @Mock
    PointClient pointClient;

    @Mock
    PrestoDAO prestoDAO; 
    
    private final WorkOrderBean instance;
    Map<String, Object> sessionMap;
    List<SiteVO> siteVOList;
    UserSitesVO currentSite;
    MenuModel breadcrumbModel;
    List<UserSitesVO> userSiteList;
    List<WorkOrderVO> workOrderVOList;
    WorkOrderVO workOrderVO;
    private final Filters filters;

    public WorkOrderBeanTest() {
        PowerMockito.mockStatic(FacesContext.class);
        PowerMockito.mockStatic(PrimeFaces.class);
        PowerMockito.mockStatic(PrimeFaces.Ajax.class);
        PowerMockito.mockStatic(UIComponent.class);
        PowerMockito.mockStatic(UIViewRoot.class);
        PowerMockito.mockStatic(ServiceRegistrarClient.class);
        PowerMockito.mockStatic(WorkOrderClient.class);
        PowerMockito.mockStatic(PointLocationClient.class);
        PowerMockito.mockStatic(PointClient.class);

        PrimeFaces pf = PowerMockito.mock(PrimeFaces.class);
        PrimeFaces.Ajax ax = PowerMockito.mock(PrimeFaces.Ajax.class);
        when(PrimeFaces.current()).thenReturn(pf);
        when(pf.ajax()).thenReturn(ax);
        PowerMockito.mockStatic(TrendDataClient.class);
        PowerMockito.mockStatic(PrestoDAO.class);
        
        prestoDAO=PowerMockito.mock(PrestoDAO.class);
        facesContext = PowerMockito.mock(FacesContext.class);
        externalContext = PowerMockito.mock(ExternalContext.class);
        navigationBean = PowerMockito.mock(NavigationBean.class);
        userManageBean = PowerMockito.mock(UserManageBean.class);
        trendDataClient = PowerMockito.mock(TrendDataClient.class);
        uIComponent = PowerMockito.mock(UIComponent.class);
        uiViewRoot = PowerMockito.mock(UIViewRoot.class);
        workOrderClient = PowerMockito.mock(WorkOrderClient.class);
        serviceRegistrarClient = PowerMockito.mock(ServiceRegistrarClient.class);
        pointLocationClient = PowerMockito.mock(PointLocationClient.class);
        pointClient = PowerMockito.mock(PointClient.class);
        sessionMap = new HashMap();
        sessionMap.put("navigationBean", navigationBean);
        sessionMap.put("userManageBean", userManageBean);

        userSiteList = new ArrayList<>();
        UserSitesVO uvo = new UserSitesVO();
        uvo.setSiteId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        uvo.setSiteName("Test Site 1");
        userSiteList.add(uvo);

        NodeExpandEvent nee = PowerMockito.mock(NodeExpandEvent.class);
        TreeNode tn = PowerMockito.mock(TreeNode.class);
        when(tn.getData()).thenReturn(new String());
        List<TreeNode> tnList = new ArrayList<>();
        tnList.add(tn);
        when(navigationBean.getTreeNodeRoot()).thenReturn(tn);
        when(tn.getChildren()).thenReturn(tnList);

        when(FacesContext.getCurrentInstance()).thenReturn(facesContext);
        when(facesContext.getExternalContext()).thenReturn(externalContext);
        when(externalContext.getSessionMap()).thenReturn(sessionMap);
        when(TrendDataClient.getInstance()).thenReturn(trendDataClient);
        when(userManageBean.getUserSiteList()).thenReturn(userSiteList);
        when(facesContext.getViewRoot()).thenReturn(uiViewRoot);
        when(uiViewRoot.findComponent("addNewPannel2WO")).thenReturn(uIComponent);
        when(WorkOrderClient.getInstance()).thenReturn(workOrderClient);
        when(ServiceRegistrarClient.getInstance()).thenReturn(serviceRegistrarClient);
        when(PointLocationClient.getInstance()).thenReturn(pointLocationClient);
        when(PointClient.getInstance()).thenReturn(pointClient);
        UserSitesVO usvo = new UserSitesVO();
        usvo.setCustomerAccount("77777");
        when(userManageBean.getCurrentSite()).thenReturn(usvo);
        instance = new WorkOrderBean();

        workOrderVOList = new ArrayList();
        workOrderVO = new WorkOrderVO();
        workOrderVO.setCreatedDate(Instant.now());
        workOrderVO.setCustomerAccount("77777");
        workOrderVO.setSiteId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8242"));
        workOrderVO.setAreaId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8242"));
        workOrderVO.setAssetId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8242"));
        workOrderVO.setPriority((byte) 2);
        workOrderVO.setAreaName("Area 2");
        workOrderVO.setAssetName("Asset 2");
        workOrderVO.setIssue("Chain problem 2");
        workOrderVOList.add(workOrderVO);
        workOrderVO = new WorkOrderVO();
        workOrderVO.setCreatedDate(Instant.now());
        workOrderVO.setCustomerAccount("77777");
        workOrderVO.setSiteId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        workOrderVO.setAreaId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        workOrderVO.setAssetId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        workOrderVO.setPointLocationId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        workOrderVO.setPointId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        workOrderVO.setApSetId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        workOrderVO.setPriority((byte) 1);
        workOrderVO.setAreaName("Area 1");
        workOrderVO.setAssetName("Asset 1");
        workOrderVO.setIssue("Chain problem 1");
        workOrderVOList.add(workOrderVO);

        filters = new Filters();

        filters.setCountry("Test Country 1");
        filters.setRegion("Test Region 1");
        filters.setSite(new SelectItem(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"), "Test site 1"));
        filters.setArea(new SelectItem(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"), "Test Area 1"));
        filters.setAsset(new SelectItem(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"), "Test Asset 1"));
        filters.setPointLocation(new SelectItem(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"), "Test Point Location 1"));
        filters.setPoint(new SelectItem(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"), "Test point 1"));

    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
        SiteVO siteVO;

        siteVOList = new ArrayList();
        siteVO = new SiteVO();
        siteVO.setRegion("REGION_1");
        siteVO.setCountry("COUNTRY_1");
        siteVO.setSiteName("Test Site 1");
        siteVO.setSiteId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        siteVOList.add(siteVO);
        siteVO = new SiteVO();
        siteVO.setRegion("REGION_1");
        siteVO.setCountry("COUNTRY_1");
        siteVO.setSiteName("SITE_2");
        siteVO.setSiteId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8243"));
        siteVOList.add(siteVO);

        currentSite = new UserSitesVO();
        currentSite.setCustomerAccount("77777");
        currentSite.setSiteId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        currentSite.setSiteName("SITE_1");
        currentSite.setUserId("sam.spade@uptime-solutions.us");
        currentSite.setSiteRole("Site Administrator");

        breadcrumbModel = new DefaultMenuModel();

        String json = "{\n" +
"  \"customer_id\":\"FEDEX EXPRESS\",\n" +
"  \"site_name\":\"MEMPHIS\",\n" +
"  \"devices\":[\n" +
"    {\"asset_id\":\"cc284411-f1c1-4a37-89a9-bbdfa91ac83b\", \"device_id\":\"BB002407\"},\n" +
"    {\"asset_id\":\"1c27ae64-df18-46d1-91e7-0f825d34c570\", \"device_id\":\"BB000034\"},\n" +
"    {\"asset_id\":\"2897ec50-bbe7-4ffe-9641-627662b0a0c3\", \"device_id\":\"BB002293,BB001180\"},\n" +
"    {\"asset_id\":\"568b7c0e-adf9-4e08-9cfe-9c1ee89edde2\", \"device_id\":\"BB000003,BB161511\"}\n" +
"  ]\n" +
"}";
        instance.setSelectedFilters();
        instance.getSelectedFilters().setCountry("Country 1");
        instance.getSelectedFilters().setRegion("Region 1");
        UUID siteID = UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8243");
        instance.getSelectedFilters().setSite(new SelectItem(siteID, "site"));
        instance.getSelectedFilters().setArea(new SelectItem(UUID.randomUUID(), "area"));
        instance.getSelectedFilters().setAsset(new SelectItem(UUID.randomUUID(), "asset"));
        when(workOrderClient.getOpenWorkOrdersByCustomerSiteArea(serviceRegistrarClient.getServiceHostURL(WORK_ORDER_SERVICE_NAME), "77777", (UUID) instance.getSelectedFilters().getSite().getValue(), (UUID) instance.getSelectedFilters().getArea().getValue())).thenReturn(workOrderVOList);
        //when(workOrderClient.getOpenWorkOrdersByCustomerSite(serviceRegistrarClient.getServiceHostURL(WORK_ORDER_SERVICE_NAME), "77777", (UUID) instance.getSelectedFilters().getSite().getValue())).thenReturn(workOrderVOList);
        when(prestoDAO.getDevicesByCustomerSite("Country 1", instance.getSelectedFilters().getSite().getLabel())).thenReturn(json);
        when(navigationBean.getBreadcrumbModel()).thenReturn(breadcrumbModel);
    }

    @After
    public void tearDown() throws Exception {
    }

    /**
     * Test of init method, of class WorkOrderBean.
     */
    @Test
    public void test01_Init() {
        System.out.println("init");

        try {
            MockitoAnnotations.initMocks(instance);
            serviceRegistrarClient = mock(ServiceRegistrarClient.class);
            siteClient = mock(SiteClient.class);

            Whitebox.setInternalState(ServiceRegistrarClient.class, "instance", serviceRegistrarClient);
            Whitebox.setInternalState(SiteClient.class, "instance", siteClient);

            when(userManageBean.getCurrentSite()).thenReturn(currentSite);
            when(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME)).thenReturn("test");
            UserSitesVO usvo = new UserSitesVO();
            usvo.setCustomerAccount("77777");
            when(userManageBean.getCurrentSite()).thenReturn(usvo);
            when(siteClient.getSiteRegionCountryByCustomer("test", "77777")).thenReturn(siteVOList);
            when(siteClient.getSiteRegionCountryByCustomer(serviceRegistrarClient.getServiceHostURL(CONFIGURATION_SERVICE_NAME), "77777")).thenReturn(siteVOList);

            instance.init();
            System.out.println("init completed");
        } catch (Exception ex) {
            fail("The method init Failed.");
        }
    }

    /**
     * Test of resetPage method, of class WorkOrderBean.
     */
    @Test
    public void test02_ResetPage() {
        System.out.println("resetPage");

        try {
            MockitoAnnotations.initMocks(instance);
            serviceRegistrarClient = mock(ServiceRegistrarClient.class);

            siteClient = mock(SiteClient.class);

            Whitebox.setInternalState(ServiceRegistrarClient.class, "instance", serviceRegistrarClient);
            Whitebox.setInternalState(SiteClient.class, "instance", siteClient);

            when(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME)).thenReturn("test");
            UserSitesVO usvo = new UserSitesVO();
            usvo.setCustomerAccount("77777");
            when(userManageBean.getCurrentSite()).thenReturn(usvo);
            when(siteClient.getSiteRegionCountryByCustomer("test", "77777")).thenReturn(siteVOList);
            when(siteClient.getSiteRegionCountryByCustomer(serviceRegistrarClient.getServiceHostURL(CONFIGURATION_SERVICE_NAME), "77777")).thenReturn(siteVOList);

            instance.resetPage();
            instance.setSelectedFilters();
            instance.getSelectedFilters().setRegion("REGION_1");
            assertEquals(instance.getSelectedFilters().getRegion(), siteVOList.get(0).getRegion());

            System.out.println("resetPage completed");
        } catch (Exception ex) {
            fail("The method resetPage Failed.");
        }
    }

    /**
     * Test of close method, of class WorkOrderBean.
     */
    @Test
    public void test03_Close() {
        System.out.println("close");
        WorkOrderVO vo = new WorkOrderVO();
        vo.setCreatedDate(Instant.now());
        when(siteClient.getSiteRegionCountryByCustomer(serviceRegistrarClient.getServiceHostURL(CONFIGURATION_SERVICE_NAME), "77777")).thenReturn(siteVOList);

        when(workOrderClient.closeWorkOrder(serviceRegistrarClient.getServiceHostURL(WORK_ORDER_SERVICE_NAME), vo)).thenReturn(true);
        instance.close(vo);
        assertTrue(vo.getOpenedYear() > 0);
        System.out.println("close completed");
    }

    /**
     * Test of edit method, of class WorkOrderBean.
     */
    @Test
    public void test04_Edit() {
        System.out.println("edit");
        WorkOrderVO wovo = new WorkOrderVO();
        instance.edit(wovo);
        System.out.println("edit completed");
    }

    /**
     * Test of onRowToggle method, of class WorkOrderBean.
     */
    @Test
    public void test06_OnRowToggle() {
        System.out.println("onRowToggle");
        when(workOrderClient.getWorkOrderTrendsByCustomerSiteAreaAssetPriorityDate(serviceRegistrarClient.getServiceHostURL(WORK_ORDER_SERVICE_NAME), workOrderVO.getCustomerAccount(), workOrderVO.getSiteId(), workOrderVO.getAreaId(), workOrderVO.getAssetId(), workOrderVO.getPriority(), workOrderVO.getCreatedDate())).thenReturn(workOrderVOList);

        Whitebox.setInternalState(instance, "workOrderTrendsMap", new HashMap());

        instance.onRowToggle(workOrderVO);

        System.out.println("onRowToggle completed");
    }

    /**
     * Test of onRegionChange method, of class WorkOrderBean.
     */
    @Test
    public void test07_OnRegionChange() {
        System.out.println("onRegionChange");
        try {
            instance.getSelectedFilters().setCountry("Country 1");
            instance.onRegionChange(true);
            assertTrue(instance.getSelectedFilters().getCountry() == null);
            System.out.println("onRegionChange completed");
        } catch (Exception ex) {
            fail("The method onRegionChange Failed.");
        }
    }

    /**
     * Test of onCountryChange method, of class ConfigurationBean.
     */
    @Test
    public void test08_OnCountryChange() {
        System.out.println("onCountryChange");

        try {
            instance.getSelectedFilters().setSite(new SelectItem("site 1"));
            instance.onCountryChange(true);
            assertTrue(instance.getSelectedFilters().getSite() == null);
            System.out.println("onCountryChange completed");
        } catch (Exception ex) {
            fail("The method onCountryChange Failed.");
        }
    }

    /**
     * Test of onSiteChange method, of class ConfigurationBean.
     */
    @Test
    public void test09_OnSiteChange() {
        System.out.println("onSiteChange");

        try {
            instance.getSelectedFilters().setArea(new SelectItem("area 1"));
            instance.onSiteChange(true);
            assertTrue(instance.getSelectedFilters().getArea() == null);
            System.out.println("onSiteChange completed");
        } catch (Exception ex) {
            fail("The method onSiteChange Failed.");
        }
    }

    /**
     * Test of onAreaChange method, of class ConfigurationBean.
     */
    @Test
    public void test10_OnAreaChange() {
        System.out.println("onAreaChange");

        try {
            instance.getSelectedFilters().setAsset(new SelectItem("asset 1"));
            instance.onAreaChange(true);
            assertTrue(instance.getSelectedFilters().getAsset() == null);
            System.out.println("onAreaChange completed");
        } catch (Exception ex) {
            fail("The method onAreaChange Failed.");
        }
    }

    /**
     * Test of onAssetChange method, of class ConfigurationBean.
     */
    @Test
    public void test11_OnAssetChange() {
        System.out.println("onAssetChange");

        try {
            when(workOrderClient.getOpenWorkOrdersByCustomerSiteAreaAsset(serviceRegistrarClient.getServiceHostURL(WORK_ORDER_SERVICE_NAME), "77777", (UUID) instance.getSelectedFilters().getSite().getValue(), (UUID) instance.getSelectedFilters().getArea().getValue(), (UUID) instance.getSelectedFilters().getAsset().getValue())).thenReturn(workOrderVOList);
            instance.onAssetChange(true);
            assertTrue(instance.getOpenWorkOrdersList().size() > 0);
            System.out.println("onAssetChange completed");
        } catch (Exception ex) {
            fail("The method onAssetChange Failed.");
        }
    }

    /**
     * Test of updateUI method, of class WorkOrderBean.
     */
    @Test
    public void test12_UpdateUI() {
        System.out.println("updateUI");
        instance.getSelectedFilters().setPoint(new SelectItem(UUID.randomUUID(), "point1"));
        instance.updateUI("content");
        boolean expectedResult = instance.getSelectedFilters().getPoint().getLabel().equals("point1");
        assertTrue(expectedResult);
        System.out.println("updateUI completed");
    }

    /**
     * Test of updateBreadcrumbModel method, of class WorkOrderBean.
     */
    @Test
    public void test13_UpdateBreadcrumbModel() {
        System.out.println("updateBreadcrumbModel");

        try {
            when(userManageBean.getResourceBundleString("breadcrumb_3")).thenReturn("test");
            instance.setSelectedFilters();
            instance.getSelectedFilters().setCountry("Country 1");
            instance.getSelectedFilters().setRegion("Region 1");
            instance.getSelectedFilters().setSite(new SelectItem(UUID.randomUUID(), "site"));
            instance.getSelectedFilters().setArea(new SelectItem(UUID.randomUUID(), "area"));
            instance.getSelectedFilters().setAsset(new SelectItem(UUID.randomUUID(), "asset"));
            instance.getSelectedFilters().setPointLocation(new SelectItem(UUID.randomUUID(), "ptlo"));
            instance.getSelectedFilters().setPoint(new SelectItem(UUID.randomUUID(), "point"));
            instance.updateBreadcrumbModel();
            boolean expectedResult = instance.getSelectedFilters().getArea().getLabel().equals("area");
            assertTrue(expectedResult);
            System.out.println("updateBreadcrumbModel completed");
        } catch (Exception ex) {
            fail("The method updateBreadcrumbModel Failed.");
        }
    }

    /**
     * Test of treeContentMenuBuilder method, of class WorkOrderBean.
     */
    @Test
    public void test14_TreeContentMenuBuilder() {
        System.out.println("test14_TreeContentMenuBuilder");

        try {
            when(userManageBean.getResourceBundleString("breadcrumb_3")).thenReturn("test");
            instance.setSelectedFilters();
            instance.getSelectedFilters().setCountry("Country 1");

            instance.treeContentMenuBuilder(null, null, null, null);
            instance.treeContentMenuBuilder(ROOT, "", null, null);
            boolean expectedResult = instance.getSelectedFilters().getCountry().equals("Country 1");
            assertTrue(expectedResult);
            System.out.println("test14_TreeContentMenuBuilder completed");
        } catch (Exception ex) {
            fail("The method test14_TreeContentMenuBuilder Failed.");
        }
    }

    /**
     * Test of onNodeExpand method, of class WorkOrderBean.
     */
    @Test
    public void test15_OnNodeExpand() {
        System.out.println("onNodeExpand");
        NodeExpandEvent nee = PowerMockito.mock(NodeExpandEvent.class);
        TreeNode tn = PowerMockito.mock(TreeNode.class);
        TreeNode tn1 = PowerMockito.mock(TreeNode.class);
        when(nee.getTreeNode()).thenReturn(tn);
        when(nee.getTreeNode().getParent()).thenReturn(tn1);

        when(tn.getType()).thenReturn("area");
        when(tn1.getType()).thenReturn("site");
        when(tn1.getData()).thenReturn(new SelectItem(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"), "Area 1"));
        when(tn.getData()).thenReturn(new SelectItem(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"), "Area 1"));
     
        List<AssetVO> assetVOs = new ArrayList();
        AssetVO assetVO = new AssetVO();
        assetVO.setCustomerAccount("77777");
        assetVO.setSiteId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        assetVO.setAreaId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        assetVO.setAssetId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        assetVO.setAreaName("Area 1");
        assetVO.setAssetName("Asset 1");
        instance.resetPage();
        when(assetClient.getAssetSiteAreaByCustomerSiteArea(serviceRegistrarClient.getServiceHostURL(CONFIGURATION_SERVICE_NAME), "77777", workOrderVO.getAssetId(), workOrderVO.getAreaId())).thenReturn(assetVOs);
        instance.onNodeExpand(nee);
       
        System.out.println("onNodeExpand completed");

    }

    /**
     * Test of onNodeCollapse method, of class WorkOrderBean.
     */
    @Test
    public void test16_OnNodeCollapse() {
        System.out.println("onNodeCollapse");
        NodeCollapseEvent nodeCollapseEvent = PowerMockito.mock(NodeCollapseEvent.class);
        TreeNode tn = PowerMockito.mock(TreeNode.class);
        when(nodeCollapseEvent.getTreeNode()).thenReturn(tn);
        when(tn.getType()).thenReturn("area");

        instance.onNodeCollapse(nodeCollapseEvent);
        assertEquals(nodeCollapseEvent.getTreeNode().getChildren().size(), 0);
        System.out.println("onNodeCollapse completed");
    }

    /**
     * Test of onNodeSelect method, of class WorkOrderBean.
     */
    @Test
    public void test17_OnNodeSelect() {
        System.out.println("onNodeSelect");
        NodeSelectEvent nse = PowerMockito.mock(NodeSelectEvent.class);
        TreeNode tn = PowerMockito.mock(TreeNode.class);
        when(nse.getTreeNode()).thenReturn(tn);
        when(tn.getType()).thenReturn("area");
        
   
        List<AreaVO> areaVOs = new ArrayList();
        AreaVO areaVO = new AreaVO();
        areaVO.setAreaId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8242"));
        areaVO.setAreaName("Area 1");
        areaVOs.add(areaVO);
        when(areaClient.getAreaSiteByCustomerSite(serviceRegistrarClient.getServiceHostURL(CONFIGURATION_SERVICE_NAME), "77777", (UUID) instance.getSelectedFilters().getSite().getValue())).thenReturn(areaVOs);
        List<AssetVO> assetVOs = new ArrayList();
        AssetVO assetVO = new AssetVO();
        assetVO.setCustomerAccount("77777");
        assetVO.setSiteId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        assetVO.setAreaId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        assetVO.setAssetId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        assetVO.setAreaName("Area 1");
        assetVO.setAssetName("Asset 1");
        when(assetClient.getAssetSiteAreaByCustomerSiteArea(serviceRegistrarClient.getServiceHostURL(CONFIGURATION_SERVICE_NAME), "77777", (UUID) instance.getSelectedFilters().getSite().getValue(), (UUID) instance.getSelectedFilters().getArea().getValue())).thenReturn(assetVOs);

        when(workOrderClient.getOpenWorkOrdersByCustomerSiteArea(serviceRegistrarClient.getServiceHostURL(WORK_ORDER_SERVICE_NAME), "77777", (UUID) instance.getSelectedFilters().getSite().getValue(), (UUID) instance.getSelectedFilters().getArea().getValue())).thenReturn(workOrderVOList);
        instance.onNodeSelect(nse);
        assertEquals(nse.getTreeNode().getChildren().size(), 0);
        // assertEquals(instance.getSiteVOList().size(), 0);
        System.out.println("onNodeSelect completed");
    }

    /**
     * Test of setRegionCountrySiteMap method, of class WorkOrderBean.
     */
    @Test
    public void test18_SetRegionCountrySiteMap() {
        System.out.println("setRegionCountrySiteMap");
        List<SiteVO> list = new ArrayList();
        SiteVO s1 = new SiteVO();
        s1.setSiteId(UUID.fromString("f33fcf5a-5f93-4504-bc07-74a61a460bf0"));
        s1.setSiteName("Test Site 1");
        s1.setRegion("Region1");
        s1.setCountry("Country1");
        list.add(s1);
        when(serviceRegistrarClient.getServiceHostURL(CONFIGURATION_SERVICE_NAME)).thenReturn(CONFIGURATION_SERVICE_NAME);
        when(siteClient.getSiteRegionCountryByCustomer(CONFIGURATION_SERVICE_NAME, "77777")).thenReturn(list);
        instance.setRegionCountrySiteMap();
        boolean expectedResult = instance.countryListCheck() == false;
        assertFalse(expectedResult);

        System.out.println("setRegionCountrySiteMap completed");
    }

    /**
     * Test of getPriorityStyle method, of class WorkOrderBean.
     */
    @Test
    public void test19_GetPriorityStyle() {
        System.out.println("getPriorityStyle");
        assertEquals("background-image: radial-gradient(rgba(255,0,0,1), rgba(255,0,0,.3));", instance.getPriorityStyle((byte) 1));
        System.out.println("getPriorityStyle completed");
    }

    /**
     * Test of getOpenWorkOrdersList method, of class WorkOrderBean.
     */
    @Test
    public void test20_GetOpenWorkOrdersList() {
        System.out.println("getPriorityStyle");
        assertEquals(new ArrayList(), instance.getOpenWorkOrdersList());
        System.out.println("getPriorityStyle completed");
    }

    /**
     * Test of getWorkOrderTrendsList method, of class WorkOrderBean.
     */
    @Test
    public void test21_GetWorkOrderTrendsList() {
        System.out.println("getWorkOrderTrendsList");
        assertNull(instance.getWorkOrderTrendsList(workOrderVO));
        System.out.println("getWorkOrderTrendsList completed");
    }

    /**
     * Test of updateCount method, of class WorkOrderBean.
     */
    @Test
    public void test22_UpdateCount() {
        System.out.println("updateCount");
        //   instance.setSelectedFilters();
        instance.updateCount();
        System.out.println("updateCount completed");
    }

    /**
     * Test of openWorkOrderPage method, of class WorkOrderBean.
     */
    @Test
    public void test23_OpenWorkOrderPage() {
        System.out.println("openWorkOrderPage");

        instance.openWorkOrderPage();
        System.out.println("openWorkOrderPage completed");
    }

    /**
     * Test of viewTrendGraph method, of class WorkOrderBean.
     */
    @Test
    public void test24_ViewTrendGraph() {
        System.out.println("viewTrendGraph");

        workOrderVO.setChannelType("DC");
        workOrderVO.setParamName("Param 1");
        when(trendDataClient.getTrendsByCustomerSiteAreaAssetPointLocationPointApSet(serviceRegistrarClient.getServiceHostURL(WAVEFORM_SERVICE_NAME), workOrderVO.getCustomerAccount(), workOrderVO.getSiteId(), workOrderVO.getAreaId(), workOrderVO.getAssetId(), workOrderVO.getPointLocationId(), workOrderVO.getPointId(), workOrderVO.getApSetId())).thenReturn(new ArrayList());
        List<PointLocationVO> pointLocationVOs = new ArrayList();
        PointLocationVO pointLocationVO = new PointLocationVO();
        pointLocationVO.setCustomerAccount("77777");
        pointLocationVO.setSiteId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        pointLocationVO.setAreaId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        pointLocationVO.setAssetId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        pointLocationVO.setPointLocationId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        pointLocationVO.setAreaName("Area 1");
        pointLocationVO.setAssetName("Asset 1");
        pointLocationVO.setPointLocationName("Point Location 1");
        pointLocationVOs.add(pointLocationVO);
        // workOrderVO.setAssetId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        when(pointLocationClient.getPointLocationsByPK(serviceRegistrarClient.getServiceHostURL(CONFIGURATION_SERVICE_NAME), workOrderVO.getCustomerAccount(), workOrderVO.getSiteId(), workOrderVO.getAreaId(), workOrderVO.getAssetId(), workOrderVO.getPointLocationId())).thenReturn(pointLocationVOs);
        List<PointVO> pointVOs = new ArrayList();
        PointVO pointVO = new PointVO();
        pointVO.setCustomerAccount("77777");
        pointVO.setSiteId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        pointVO.setAreaId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        pointVO.setAssetId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        pointVO.setPointLocationId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        pointVO.setPointId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        pointVO.setApSetId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        pointVO.setPointLocationName("Point Location 1");
        pointVO.setPointName("Point 1");
        pointVOs.add(pointVO);
        when(pointClient.getPointsByPK(serviceRegistrarClient.getServiceHostURL(CONFIGURATION_SERVICE_NAME), workOrderVO.getCustomerAccount(), workOrderVO.getSiteId(), workOrderVO.getAreaId(), workOrderVO.getAssetId(), workOrderVO.getPointLocationId(), workOrderVO.getPointId(), workOrderVO.getApSetId())).thenReturn(pointVOs);
        instance.viewTrendGraph(workOrderVO);
      //  assertFalse(instance.isGenerateGraph());
        System.out.println("viewTrendGraph completed");
    }

    /**
     * Test of viewTrendGraph method, of class WorkOrderBean.
     */
    @Test
    public void test241_ViewTrendGraph() {
        System.out.println("viewTrendGraph");

        List<TrendValuesVO> trendDataList = new ArrayList();
        TrendValuesVO trendValuesVO = new TrendValuesVO();
        trendValuesVO.setValue(1.11f);
        trendValuesVO.setSampleTime(Instant.now());
        trendValuesVO.setUnits("Units 1");
        trendDataList.add(trendValuesVO);
        workOrderVO.setChannelType("DC");
        workOrderVO.setParamName("Param 1");
        when(trendDataClient.getTrendsByCustomerSiteAreaAssetPointLocationPointApSet(serviceRegistrarClient.getServiceHostURL(WAVEFORM_SERVICE_NAME), workOrderVO.getCustomerAccount(), workOrderVO.getSiteId(), workOrderVO.getAreaId(), workOrderVO.getAssetId(), workOrderVO.getPointLocationId(), workOrderVO.getPointId(), workOrderVO.getApSetId())).thenReturn(trendDataList);
        List<PointLocationVO> pointLocationVOs = new ArrayList();
        PointLocationVO pointLocationVO = new PointLocationVO();
        pointLocationVO.setCustomerAccount("77777");
        pointLocationVO.setSiteId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        pointLocationVO.setAreaId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        pointLocationVO.setAssetId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        pointLocationVO.setPointLocationId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        pointLocationVO.setAreaName("Area 1");
        pointLocationVO.setAssetName("Asset 1");
        pointLocationVO.setPointLocationName("Point Location 1");
        pointLocationVOs.add(pointLocationVO);
        // workOrderVO.setAssetId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        when(pointLocationClient.getPointLocationsByPK(serviceRegistrarClient.getServiceHostURL(CONFIGURATION_SERVICE_NAME), workOrderVO.getCustomerAccount(), workOrderVO.getSiteId(), workOrderVO.getAreaId(), workOrderVO.getAssetId(), workOrderVO.getPointLocationId())).thenReturn(pointLocationVOs);
        List<PointVO> pointVOs = new ArrayList();
        PointVO pointVO = new PointVO();
        pointVO.setCustomerAccount("77777");
        pointVO.setSiteId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        pointVO.setAreaId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        pointVO.setAssetId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        pointVO.setPointLocationId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        pointVO.setPointId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        pointVO.setApSetId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        pointVO.setPointLocationName("Point Location 1");
        pointVO.setPointName("Point 1");
        pointVOs.add(pointVO);
        when(pointClient.getPointsByPK(serviceRegistrarClient.getServiceHostURL(CONFIGURATION_SERVICE_NAME), workOrderVO.getCustomerAccount(), workOrderVO.getSiteId(), workOrderVO.getAreaId(), workOrderVO.getAssetId(), workOrderVO.getPointLocationId(), workOrderVO.getPointId(), workOrderVO.getApSetId())).thenReturn(pointVOs);
        instance.viewTrendGraph(workOrderVO);
      //  assertTrue(instance.isGenerateGraph());
        System.out.println("viewTrendGraph completed");
    }

    /**
     * Test of onYearChange method, of class WorkOrderBean.
     */
    @Test(expected = UnsupportedOperationException.class)
    public void test28_OnYearChange() {
        System.out.println("onYearChange");
        instance.onYearChange(true);
    }

    /**
     * Test of onPointLocationChange method, of class WorkOrderBean.
     */
    @Test(expected = UnsupportedOperationException.class)
    public void test29_OnPointLocationChange() {
        System.out.println("onPointLocationChange");
        instance.onPointLocationChange(true);
    }

    /**
     * Test of onPointChange method, of class WorkOrderBean.
     */
    @Test(expected = UnsupportedOperationException.class)
    public void test30_OnPointChange() {
        System.out.println("onPointChange");
        instance.onPointChange(true);
    }

    /**
     * Test of onApSetChange method, of class WorkOrderBean.
     */
    @Test(expected = UnsupportedOperationException.class)
    public void test31_OnApSetChange() {
        System.out.println("onApSetChange");
        instance.onApSetChange(true);
    }

    /**
     * Test of onAlSetParamChange method, of class WorkOrderBean.
     */
    @Test(expected = UnsupportedOperationException.class)
    public void test32_OnAlSetParamChange() {
        System.out.println("onAlSetParamChange");
        instance.onAlSetParamChange(true);
    }

    /**
     * Test of showCreatePage method, of class WorkOrderBean.
     */
    @Test
    public void test33_ShowCreatePage() {
        System.out.println("showCreatePage");
        instance.showCreatePage();
    }

    /**
     * Test of showCreatePage method, of class WorkOrderBean.
     */
    @Test
    public void test34_downloadOpenOrderCSV() {
        System.out.println("downloadOpenOrderCSV");
        instance.downloadOpenOrderCSV();
    } 

}
