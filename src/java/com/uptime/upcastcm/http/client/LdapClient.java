/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.http.client;

import com.uptime.client.http.client.Client;
import com.uptime.client.utils.vo.HttpResponseVO;
import com.uptime.upcastcm.http.utils.json.dom.SiteDomJsonParser;
import com.uptime.upcastcm.http.utils.json.dom.UserDomJsonParser;
import com.uptime.upcastcm.http.utils.json.sax.UserSaxJsonParser;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import static com.uptime.upcastcm.utils.helperclass.GenericMessage.showErrorMsg;
import com.uptime.upcastcm.utils.vo.LdapUserVO;
import java.io.Serializable;
import java.net.URLEncoder;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 *
 * @author gsingh
 */
public class LdapClient extends Client implements Serializable {
    private static LdapClient instance = null;

    private LdapClient() {

    }

    /**
     * Returns an instance of the class
     *
     * @return LdapClient object
     */
    public static LdapClient getInstance() {
        if (instance == null) {
            instance = new LdapClient();
        }
        return instance;
    }

    public LdapUserVO getUserAccountByUserId(String host, String userId, String customerAccount) {
        LdapUserVO ldapUserVO = null;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;

        try {
            if ((responseVO = httpGet(LDAP_SERVICE_NAME, host, "/ldap?request=getUserAccount&userId=" + URLEncoder.encode(userId, "UTF-8")+"&customerAccount=" +URLEncoder.encode(customerAccount, "UTF-8"))) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        ldapUserVO = UserSaxJsonParser.getInstance().populateSiteUsersFromLdapJson(responseVO.getEntity());
                        Logger.getLogger(LdapClient.class.getName()).log(Level.INFO, "Fetched Ldap Users");
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(LdapClient.class.getName()).log(Level.WARNING, "Message from Ldap Service: {0}", SiteDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(LdapClient.class.getName()).log(Level.WARNING, "Error Code from Ldap Service : {0}", responseVO.getStatusCode());
                        Logger.getLogger(LdapClient.class.getName()).log(Level.WARNING, "Error Message from Ldap Service : {0}", responseVO.getEntity());
                        Logger.getLogger(LdapClient.class.getName()).log(Level.WARNING, "errMsg : {0}", UserSaxJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            } else {
                Logger.getLogger(LdapClient.class.getName()).log(Level.WARNING, "Error - No host found for Ldap Service.");
            }
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(LdapClient.class.getName()).log(Level.WARNING, e.getMessage(), e);
        }
        return ldapUserVO;
    }

    /**
     * Create the given user
     *
     * @param host, String Object
     * @param ldapUserVO, LdapUserVO Object
     * @return authldapUserVO, means success
     */
    public LdapUserVO validateAuthUsernamePassword(String host, LdapUserVO ldapUserVO) {
        HttpResponseVO responseVO;
        LdapUserVO authldapUserVO = null;

        try {
            if ((responseVO = httpPost(LDAP_SERVICE_NAME, host, "/auth", UserDomJsonParser.getInstance().getLdapAuthJsonFromObject(ldapUserVO))) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        Logger.getLogger(LdapClient.class.getName()).log(Level.INFO, "User successfully created in Ldap server. {0}", responseVO.getEntity());
                        authldapUserVO = UserSaxJsonParser.getInstance().populateSiteUsersFromLdapJson(responseVO.getEntity());
                        break;
                    case 404:
                        Logger.getLogger(LdapClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(LdapClient.class.getName()).log(Level.WARNING, "**********Message from Ldap Service: USER NOT FOUND**********");
                        Logger.getLogger(LdapClient.class.getName()).log(Level.WARNING, "Error Message from Ldap Service: {0}", SiteDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));                   
                        break;
                    default:
                        Logger.getLogger(LdapClient.class.getName()).log(Level.WARNING, "Error Code: {0}", responseVO.getStatusCode());
                        Logger.getLogger(LdapClient.class.getName()).log(Level.WARNING, "Error Message entity from Ldap Service : {0}", responseVO.getEntity());
                        Logger.getLogger(LdapClient.class.getName()).log(Level.WARNING, "Error Message from Ldap Service: {0}", UserSaxJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
        } catch (Exception e) {
            Logger.getLogger(LdapClient.class.getName()).log(Level.WARNING, e.getMessage(), e);
            authldapUserVO = null;
        }
        return authldapUserVO;
    }

    /**
     * Create the given user
     *
     * @param host, String Object
     * @param ldapUserVO, LdapUserVO Object
     * @return boolean, true means success
     */
    public boolean createUsers(String host, LdapUserVO ldapUserVO) {
        HttpResponseVO responseVO;
        String errMsg;
        
        if ((responseVO = httpPost(LDAP_SERVICE_NAME, host, "/ldap", UserDomJsonParser.getInstance().getJsonFromObject(ldapUserVO, "createUserAccount"))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                Logger.getLogger(LdapClient.class.getName()).log(Level.INFO, "User successfully created in Ldap server.");
                return true;
            } else {
                Logger.getLogger(LdapClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(LdapClient.class.getName()).log(Level.WARNING, "Error Message from Ldap Service: {0}", (errMsg = SiteDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity())));
                FacesContext.getCurrentInstance().addMessage("growlMessages", new FacesMessage("Error Message from Ldap Service: " + errMsg + "."));
            }
        }
        showErrorMsg();
        
        return false;
    }

    /**
     * Update the given user
     *
     * @param host, String Object
     * @param ldapUserVO, LdapUserVO Object
     * @param requestType
     * @return boolean, true if success, else false
     */
    public boolean updateUsers(String host, LdapUserVO ldapUserVO, String requestType) {
        HttpResponseVO responseVO;
        String errMsg;
        
        if ((responseVO = httpPut(LDAP_SERVICE_NAME, host, "/ldap", UserDomJsonParser.getInstance().getJsonFromObject(ldapUserVO, requestType))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                Logger.getLogger(LdapClient.class.getName()).log(Level.INFO, "User successfully updated in Ldap server.");
                return true;
            } else {
                Logger.getLogger(LdapClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(LdapClient.class.getName()).log(Level.WARNING, "Error Message from Ldap Service: {0}", (errMsg = SiteDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity())));
                FacesContext.getCurrentInstance().addMessage("growlMessages", new FacesMessage("Error Message from Ldap Service: " + errMsg + "."));
            }
        }
        return false;
    }

    /**
     * Delete the given user
     *
     * @param host, String Object
     * @param userId, String Object
     * @param customerAccount
     * @return boolean, true means success
     */
    public boolean deleteUsers(String host, String userId, String customerAccount) {
        HttpResponseVO responseVO;
        String errMsg;
        StringBuilder endPoint;
        
        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/ldap")
                    .append("?userId=").append(URLEncoder.encode(userId, "UTF-8"))
                    .append("&customerAccount=").append(URLEncoder.encode(customerAccount, "UTF-8"));
            
            if ((responseVO = httpDelete(LDAP_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                if (responseVO.getStatusCode() == 200) {
                    Logger.getLogger(LdapClient.class.getName()).log(Level.INFO, "User successfully deleted in Ldap server.");
                    return true;
                } else {
                    Logger.getLogger(LdapClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                    Logger.getLogger(LdapClient.class.getName()).log(Level.WARNING, "Error Message from Ldap Service: {0}", (errMsg = SiteDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity())));
                    FacesContext.getCurrentInstance().addMessage("growlMessages", new FacesMessage("Error Message from Ldap Service: " + errMsg + "."));
                }
            }
        } catch (Exception e) {
            Logger.getLogger(LdapClient.class.getName()).log(Level.WARNING, e.getMessage(), e);
        }

        return false;
    }

}
