/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.http.client;

import com.uptime.client.http.client.Client;
import com.uptime.client.utils.vo.HttpResponseVO;
import com.uptime.upcastcm.http.utils.json.dom.APALSetDomJsonParser;
import com.uptime.upcastcm.http.utils.json.sax.APALSetSaxJsonParser;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import static com.uptime.upcastcm.utils.helperclass.GenericMessage.createItemMsg;
import static com.uptime.upcastcm.utils.helperclass.GenericMessage.deleteItemMsg;
import static com.uptime.upcastcm.utils.helperclass.GenericMessage.showErrorMsg;
import static com.uptime.upcastcm.utils.helperclass.GenericMessage.updateItemMsg;
import com.uptime.upcastcm.utils.vo.ApAlSetVO;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author madhavi
 */
public class APALSetClient extends Client {
    private static APALSetClient instance = null;

    /**
     * Private Singleton class
     */
    private APALSetClient() {
    }

    /**
     * Returns an instance of the class
     *
     * @return APALSetClient object
     */
    public static APALSetClient getInstance() {
        if (instance == null) {
            instance = new APALSetClient();
        }
        return instance;
    }

    /**
     * Get GlobalApAlSets Info based on the given info
     *
     * @param host String Object
     * @param customer String Object
     * @param sensorType, String Object
     * @return List Object of ApAlSetVO Objects
     */
    public List<ApAlSetVO> getGlobalApAlSetsByCustomerSensorType(String host, String customer, String sensorType) {
        List<ApAlSetVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;

        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/presets/globalApAlSets")
                    .append("?customerAccount=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&sensorType=").append(URLEncoder.encode(sensorType, "UTF-8"));
            
            if ((responseVO = httpGet(PRESET_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        list = APALSetSaxJsonParser.getInstance().populateApAlSetVOFromJson(responseVO.getEntity());
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(APALSetClient.class.getName()).log(Level.WARNING, "Message from Presets Service: {0}", APALSetDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(APALSetClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(APALSetClient.class.getName()).log(Level.WARNING, "Error Message from Presets Service: {0}", APALSetDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(APALSetClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }

    /**
     * Get GlobalApAlSets Info based on the given info
     *
     * @param host String Object
     * @param customer String Object
     * @param sensorType, String Object
     * @return List Object of ApAlSetVO Objects
     */
    public List<ApAlSetVO> getGlobalApAlSetsAllByCustomerSensorType(String host, String customer, String sensorType) {
        List<ApAlSetVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;

        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/presets/globalApAlSets")
                    .append("?customerAccount=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&sensorType=").append(URLEncoder.encode(sensorType, "UTF-8"))
                    .append("&All=").append("All");
            
            if ((responseVO = httpGet(PRESET_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        list = APALSetSaxJsonParser.getInstance().populateApAlSetVOFromJson(responseVO.getEntity());
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(APALSetClient.class.getName()).log(Level.WARNING, "Message from Presets Service: {0}", APALSetDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(APALSetClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(APALSetClient.class.getName()).log(Level.WARNING, "Error Message from Presets Service: {0}", APALSetDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(APALSetClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }

    /**
     * Get GlobalApAlSets Info based on the given info
     *
     * @param host String Object
     * @param customer String Object
     * @param apSetId, UUID Object
     * @return List Object of ApAlSetVO Objects
     */
    public List<ApAlSetVO> getGlobalApAlSetsByCustomerApSetId(String host, String customer, UUID apSetId) {
        List<ApAlSetVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;

        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/presets/globalApAlSets")
                    .append("?customerAccount=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&apSetId=").append(apSetId);
            
            if ((responseVO = httpGet(PRESET_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        list = APALSetSaxJsonParser.getInstance().populateApAlSetVOFromJson(responseVO.getEntity());
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(APALSetClient.class.getName()).log(Level.WARNING, "Message from Presets Service: {0}", APALSetDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(APALSetClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(APALSetClient.class.getName()).log(Level.WARNING, "Error Message from Presets Service: {0}", APALSetDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(APALSetClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }

    /**
     * Get GlobalApAlSets Info based on the given info
     *
     * @param host String Object
     * @param customer String Object
     * @param apSetIdList, List of UUID Objects
     * @return List Object of ApAlSetVO Objects
     */
    public List<ApAlSetVO> getGlobalApAlSetsByCustomerApSetIds(String host, String customer, List<UUID> apSetIdList) {
        List<ApAlSetVO> list = new ArrayList();
        StringBuilder endPoint, apSetIds;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;

        try {
            apSetIds = new StringBuilder();
            if (apSetIdList != null && !apSetIdList.isEmpty()) {
                for (int i = 0; i < apSetIdList.size(); i++) {
                    apSetIds.append(apSetIdList.get(i)).append(i < apSetIdList.size() - 1 ? "," : "");
                }
            }
            
            endPoint = new StringBuilder();
            endPoint
                    .append("/presets/globalApAlSets")
                    .append("?customerAccount=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&apSetIds=").append(apSetIds);
            
            if ((responseVO = httpGet(PRESET_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        list = APALSetSaxJsonParser.getInstance().populateApAlSetVOFromJson(responseVO.getEntity());
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(APALSetClient.class.getName()).log(Level.WARNING, "Message from Presets Service: {0}", APALSetDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(APALSetClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(APALSetClient.class.getName()).log(Level.WARNING, "Error Message from Presets Service: {0}", APALSetDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(APALSetClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }

    /**
     * Get GlobalApAlSets Info based on the given info
     *
     * @param host String Object
     * @param customer String Object
     * @param apSetId, UUID Object
     * @param alSetId, UUID Object
     * @return List Object of ApAlSetVO Objects
     */
    public List<ApAlSetVO> getGlobalApAlSetsByCustomerApSetIdAlSetId(String host, String customer, UUID apSetId, UUID alSetId) {
        List<ApAlSetVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;

        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/presets/globalApAlSets")
                    .append("?customerAccount=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&apSetId=").append(apSetId)
                    .append("&alSetId=").append(alSetId);
            
            if ((responseVO = httpGet(PRESET_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        list = APALSetSaxJsonParser.getInstance().populateApAlSetVOFromJson(responseVO.getEntity());
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(APALSetClient.class.getName()).log(Level.WARNING, "Message from Presets Service: {0}", APALSetDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(APALSetClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(APALSetClient.class.getName()).log(Level.WARNING, "Error Message from Presets Service: {0}", APALSetDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(APALSetClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }

    /**
     * Get GlobalApAlSets Info based on the given info
     *
     * @param host String Object
     * @param customer String Object
     * @param apSetId, UUID Object
     * @param alSetId, UUID Object
     * @param paramName, String Object
     * @return List Object of ApAlSetVO Objects
     */
    public List<ApAlSetVO> getGlobalApAlSetsByPK(String host, String customer, UUID apSetId, UUID alSetId, String paramName) {
        List<ApAlSetVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;

        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/presets/globalApAlSets")
                    .append("?customerAccount=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&apSetId=").append(apSetId)
                    .append("&alSetId=").append(alSetId)
                    .append("&paramName=").append(URLEncoder.encode(paramName, "UTF-8"));
            
            if ((responseVO = httpGet(PRESET_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        list = APALSetSaxJsonParser.getInstance().populateApAlSetVOFromJson(responseVO.getEntity());
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(APALSetClient.class.getName()).log(Level.WARNING, "Message from Presets Service: {0}", APALSetDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(APALSetClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(APALSetClient.class.getName()).log(Level.WARNING, "Error Message from Presets Service: {0}", APALSetDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(APALSetClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }

    /**
     * Get SiteApAlSets Info based on the given info
     *
     * @param host String Object
     * @param customer String Object
     * @param siteId, UUID Object
     * @param apSetId, UUID Object
     * @return List Object of ApAlSetVO Objects
     */
    public List<ApAlSetVO> getSiteApAlSetsByCustomerSiteIdApSetId(String host, String customer, UUID siteId, UUID apSetId) {
        List<ApAlSetVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;

        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/presets/siteApAlSets")
                    .append("?customerAccount=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&siteId=").append(siteId)
                    .append("&apSetId=").append(apSetId);
            
            if ((responseVO = httpGet(PRESET_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        list = APALSetSaxJsonParser.getInstance().populateApAlSetVOFromJson(responseVO.getEntity());
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(APALSetClient.class.getName()).log(Level.WARNING, "Message from Presets Service: {0}", APALSetDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(APALSetClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(APALSetClient.class.getName()).log(Level.WARNING, "Error Message from Presets Service: {0}", APALSetDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(APALSetClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }

    /**
     * Get SiteApAlSets Info based on the given info
     *
     * @param host String Object
     * @param customer String Object
     * @param siteId, UUID Object
     * @param apSetIdList, List of UUID Object
     * @return List Object of ApAlSetVO Objects
     */
    public List<ApAlSetVO> getSiteApAlSetsByCustomerSiteIdApSetIds(String host, String customer, UUID siteId, List<UUID> apSetIdList) {
        List<ApAlSetVO> list = new ArrayList();
        StringBuilder endPoint, apSetIds;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;

        try {
            apSetIds = new StringBuilder();
            if (apSetIdList != null && !apSetIdList.isEmpty()) {
                for (int i = 0; i < apSetIdList.size(); i++) {
                    apSetIds.append(apSetIdList.get(i)).append(i < apSetIdList.size() - 1 ? "," : "");
                }
            }
            endPoint = new StringBuilder();
            endPoint
                    .append("/presets/siteApAlSets")
                    .append("?customerAccount=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&siteId=").append(siteId)
                    .append("&apSetIds=").append(apSetIds);
            
            if ((responseVO = httpGet(PRESET_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        list = APALSetSaxJsonParser.getInstance().populateApAlSetVOFromJson(responseVO.getEntity());
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(APALSetClient.class.getName()).log(Level.WARNING, "Message from Presets Service: {0}", APALSetDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(APALSetClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(APALSetClient.class.getName()).log(Level.WARNING, "Error Message from Presets Service: {0}", APALSetDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(APALSetClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }

    /**
     * Get SiteApAlSets Info based on the given info
     *
     * @param host String Object
     * @param customer String Object
     * @param siteId, UUID Object
     * @param sensorType, String Object
     * @return List Object of ApAlSetVO Objects
     */
    public List<ApAlSetVO> getSiteApAlSetsByCustomerSiteIdSensorType(String host, String customer, UUID siteId, String sensorType) {
        List<ApAlSetVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;

        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/presets/siteApAlSets")
                    .append("?customerAccount=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&siteId=").append(siteId)
                    .append("&sensorType=").append(URLEncoder.encode(sensorType, "UTF-8"));
            
            if ((responseVO = httpGet(PRESET_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        list = APALSetSaxJsonParser.getInstance().populateApAlSetVOFromJson(responseVO.getEntity());
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(APALSetClient.class.getName()).log(Level.WARNING, "Message from Presets Service: {0}", APALSetDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(APALSetClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(APALSetClient.class.getName()).log(Level.WARNING, "Error Message from Presets Service: {0}", APALSetDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(APALSetClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }

    /**
     * Get SiteApAlSets Info based on the given info
     *
     * @param host String Object
     * @param customer String Object
     * @param siteId, UUID Object
     * @param sensorType, String Object
     * @return List Object of ApAlSetVO Objects
     */
    public List<ApAlSetVO> getSiteApAlSetsAllByCustomerSiteIdSensorType(String host, String customer, UUID siteId, String sensorType) {
        List<ApAlSetVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;

        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/presets/siteApAlSets")
                    .append("?customerAccount=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&siteId=").append(siteId)
                    .append("&sensorType=").append(URLEncoder.encode(sensorType, "UTF-8"))
                    .append("&All=").append("All");
            
            if ((responseVO = httpGet(PRESET_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        list = APALSetSaxJsonParser.getInstance().populateApAlSetVOFromJson(responseVO.getEntity());
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(APALSetClient.class.getName()).log(Level.WARNING, "Message from Presets Service: {0}", APALSetDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(APALSetClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(APALSetClient.class.getName()).log(Level.WARNING, "Error Message from Presets Service: {0}", APALSetDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(APALSetClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }

    /**
     * Get SiteApAlSets Info based on the given info
     *
     * @param host String Object
     * @param customer String Object
     * @param siteId, UUID Object
     * @param apSetId, UUID Object
     * @param alSetId, UUID Object
     * @return List Object of ApAlSetVO Objects
     */
    public List<ApAlSetVO> getSiteApAlSetsByCustomerSiteIdApSetIdAlSetId(String host, String customer, UUID siteId, UUID apSetId, UUID alSetId) {
        List<ApAlSetVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;

        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/presets/siteApAlSets")
                    .append("?customerAccount=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&siteId=").append(siteId)
                    .append("&apSetId=").append(apSetId)
                    .append("&alSetId=").append(alSetId);
            
            if ((responseVO = httpGet(PRESET_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        list = APALSetSaxJsonParser.getInstance().populateApAlSetVOFromJson(responseVO.getEntity());
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(APALSetClient.class.getName()).log(Level.WARNING, "Message from Presets Service: {0}", APALSetDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(APALSetClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(APALSetClient.class.getName()).log(Level.WARNING, "Error Message from Presets Service: {0}", APALSetDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(APALSetClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }

    /**
     * Get SiteApAlSets Info based on the given info
     *
     * @param host String Object
     * @param customer String Object
     * @param siteId, UUID Object
     * @param apSetId, UUID Object
     * @param alSetId, UUID Object
     * @param paramName, String Object
     * @return List Object of ApAlSetVO Objects
     */
    public List<ApAlSetVO> getSiteApAlSetsByPK(String host, String customer, UUID siteId, UUID apSetId, UUID alSetId, String paramName) {
        List<ApAlSetVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;

        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/presets/siteApAlSets")
                    .append("?customerAccount=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&siteId=").append(siteId)
                    .append("&apSetId=").append(apSetId)
                    .append("&alSetId=").append(alSetId)
                    .append("&paramName=").append(URLEncoder.encode(paramName, "UTF-8"));
            
            if ((responseVO = httpGet(PRESET_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        list = APALSetSaxJsonParser.getInstance().populateApAlSetVOFromJson(responseVO.getEntity());
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(APALSetClient.class.getName()).log(Level.WARNING, "Message from Presets Service: {0}", APALSetDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(APALSetClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(APALSetClient.class.getName()).log(Level.WARNING, "Error Message from Presets Service: {0}", APALSetDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(APALSetClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }

    /**
     * Get CustomerApSet Info based on the given info
     *
     * @param host String Object
     * @param customer String Object
     * @return List Object of ApAlSetVO Objects
     */
    public List<ApAlSetVO> getCustomerApSetByCustomer(String host, String customer) {
        List<ApAlSetVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;

        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/presets/customerApSet")
                    .append("?customerAccount=").append(URLEncoder.encode(customer, "UTF-8"));
            
            if ((responseVO = httpGet(PRESET_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        list = APALSetSaxJsonParser.getInstance().populateApAlSetVOFromJson(responseVO.getEntity());
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(APALSetClient.class.getName()).log(Level.WARNING, "Message from Presets Service: {0}", APALSetDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(APALSetClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(APALSetClient.class.getName()).log(Level.WARNING, "Error Message from Presets Service: {0}", APALSetDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(APALSetClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }

    /**
     * Get CustomerApSet Info based on the given info
     *
     * @param host String Object
     * @param customer String Object
     * @param apSetId, UUID Object
     * @return List Object of ApAlSetVO Objects
     */
    public List<ApAlSetVO> getCustomerApSetByPK(String host, String customer, UUID apSetId) {
        List<ApAlSetVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;

        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/presets/customerApSet")
                    .append("?customerAccount=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&apSetId=").append(apSetId);
            
            if ((responseVO = httpGet(PRESET_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        list = APALSetSaxJsonParser.getInstance().populateApAlSetVOFromJson(responseVO.getEntity());
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(APALSetClient.class.getName()).log(Level.WARNING, "Message from Presets Service: {0}", APALSetDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(APALSetClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(APALSetClient.class.getName()).log(Level.WARNING, "Error Message from Presets Service: {0}", APALSetDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(APALSetClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }

    /**
     * Create the given GlobalApAlSets
     *
     * @param host, String Object
     * @param apAlSetVO, ApAlSetVO Object
     * @return boolean, true if success, else false
     */
    public boolean createGlobalApAlSets(String host, ApAlSetVO apAlSetVO) {
        HttpResponseVO responseVO;

        if ((responseVO = httpPost(PRESET_SERVICE_NAME, host, "/presets/globalApAlSets", APALSetDomJsonParser.getInstance().getJsonFromApAlSet(apAlSetVO))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                createItemMsg();
                Logger.getLogger(TachometerClient.class.getName()).log(Level.INFO, "GlobalApAlSets successfully created.");
                return true;
            } else {
                Logger.getLogger(TachometerClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(TachometerClient.class.getName()).log(Level.WARNING, "Error Message from Presets Service: {0}", APALSetDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        showErrorMsg();
        return false;
    }

    /**
     * Create the given SiteApAlSets
     *
     * @param host, String Object
     * @param apAlSetVO, ApAlSetVO Object
     * @return boolean, true if success, else false
     */
    public boolean createSiteApAlSets(String host, ApAlSetVO apAlSetVO) {
        HttpResponseVO responseVO;

        if ((responseVO = httpPost(PRESET_SERVICE_NAME, host, "/presets/siteApAlSets", APALSetDomJsonParser.getInstance().getJsonFromApAlSet(apAlSetVO))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                createItemMsg();
                Logger.getLogger(TachometerClient.class.getName()).log(Level.INFO, "SiteApAlSets successfully created.");
                return true;
            } else {
                Logger.getLogger(TachometerClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(TachometerClient.class.getName()).log(Level.WARNING, "Error Message from Presets Service: {0}", APALSetDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        showErrorMsg();
        return false;
    }

    /**
     * Create the given CustomerApSet
     *
     * @param host, String Object
     * @param apAlSetVO, ApAlSetVO Object
     * @return boolean, true if success, else false
     */
    public boolean createCustomerApSet(String host, ApAlSetVO apAlSetVO) {
        HttpResponseVO responseVO;

        if ((responseVO = httpPost(PRESET_SERVICE_NAME, host, "/presets/customerApSet", APALSetDomJsonParser.getInstance().getJsonFromApAlSet(apAlSetVO))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                createItemMsg();
                Logger.getLogger(TachometerClient.class.getName()).log(Level.INFO, "CustomerApSet successfully created.");
                return true;
            } else {
                Logger.getLogger(TachometerClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(TachometerClient.class.getName()).log(Level.WARNING, "Error Message from Presets Service: {0}", APALSetDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        showErrorMsg();
        return false;
    }

    /**
     * Update the given GlobalApAlSets
     *
     * @param host, String Object
     * @param apAlSetVO, ApAlSetVO Object
     * @return boolean, true if success, else false
     */
    public boolean updateGlobalApAlSets(String host, ApAlSetVO apAlSetVO) {
        HttpResponseVO responseVO;

        if ((responseVO = httpPut(PRESET_SERVICE_NAME, host, "/presets/globalApAlSets", APALSetDomJsonParser.getInstance().getJsonFromApAlSet(apAlSetVO))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                updateItemMsg();
                Logger.getLogger(TachometerClient.class.getName()).log(Level.INFO, "GlobalApAlSets successfully updated.");
                return true;
            } else {
                Logger.getLogger(TachometerClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(TachometerClient.class.getName()).log(Level.WARNING, "Error Message from Presets Service: {0}", APALSetDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        showErrorMsg();
        return false;
    }

    /**
     * Update the given SiteApAlSets
     *
     * @param host, String Object
     * @param apAlSetVO, ApAlSetVO Object
     * @return boolean, true if success, else false
     */
    public boolean updateSiteApAlSets(String host, ApAlSetVO apAlSetVO) {
        HttpResponseVO responseVO;

        if ((responseVO = httpPut(PRESET_SERVICE_NAME, host, "/presets/siteApAlSets", APALSetDomJsonParser.getInstance().getJsonFromApAlSet(apAlSetVO))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                updateItemMsg();
                Logger.getLogger(TachometerClient.class.getName()).log(Level.INFO, "GlobalApAlSets successfully updated.");
                return true;
            } else {
                Logger.getLogger(TachometerClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(TachometerClient.class.getName()).log(Level.WARNING, "Error Message from Presets Service: {0}", APALSetDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        showErrorMsg();
        return false;
    }

    /**
     * Update the given CustomerApSet
     *
     * @param host, String Object
     * @param apAlSetVO, ApAlSetVO Object
     * @return boolean, true if success, else false
     */
    public boolean updateCustomerApSet(String host, ApAlSetVO apAlSetVO) {
        HttpResponseVO responseVO;

        if ((responseVO = httpPut(PRESET_SERVICE_NAME, host, "/presets/customerApSet", APALSetDomJsonParser.getInstance().getJsonFromApAlSet(apAlSetVO))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                updateItemMsg();
                Logger.getLogger(TachometerClient.class.getName()).log(Level.INFO, "CustomerApSet successfully updated.");
                return true;
            } else {
                Logger.getLogger(TachometerClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(TachometerClient.class.getName()).log(Level.WARNING, "Error Message from Presets Service: {0}", APALSetDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        showErrorMsg();
        return false;
    }

    /**
     * Delete the given GlobalApAlSets
     *
     * @param host, String Object
     * @param apAlSetVO, ApAlSetVO Object
     * @return boolean, true if success, else false
     */
    public boolean deleteGlobalApAlSets(String host, ApAlSetVO apAlSetVO) {
        HttpResponseVO responseVO;

        if ((responseVO = httpDelete(PRESET_SERVICE_NAME, host, "/presets/globalApAlSets", APALSetDomJsonParser.getInstance().getJsonFromApAlSet(apAlSetVO))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                deleteItemMsg();
                Logger.getLogger(TachometerClient.class.getName()).log(Level.INFO, "GlobalApAlSets successfully deleted.");
                return true;
            } else {
                Logger.getLogger(TachometerClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(TachometerClient.class.getName()).log(Level.WARNING, "Error Message from Presets Service: {0}", APALSetDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        showErrorMsg();
        return false;
    }

    /**
     * Delete the given SiteApAlSets
     *
     * @param host, String Object
     * @param apAlSetVO, ApAlSetVO Object
     * @return boolean, true if success, else false
     */
    public boolean deleteSiteApAlSets(String host, ApAlSetVO apAlSetVO) {
        HttpResponseVO responseVO;

        if ((responseVO = httpDelete(PRESET_SERVICE_NAME, host, "/presets/siteApAlSets", APALSetDomJsonParser.getInstance().getJsonFromApAlSet(apAlSetVO))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                deleteItemMsg();
                Logger.getLogger(TachometerClient.class.getName()).log(Level.INFO, "SiteApAlSets successfully deleted.");
                return true;
            } else {
                Logger.getLogger(TachometerClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(TachometerClient.class.getName()).log(Level.WARNING, "Error Message from Presets Service: {0}", APALSetDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        showErrorMsg();
        return false;
    }

    /**
     * Delete the given CustomerApSet
     *
     * @param host, String Object
     * @param apAlSetVO, ApAlSetVO Object
     * @return boolean, true if success, else false
     */
    public boolean deleteCustomerApSet(String host, ApAlSetVO apAlSetVO) {
        HttpResponseVO responseVO;

        if ((responseVO = httpDelete(PRESET_SERVICE_NAME, host, "/presets/customerApSet", APALSetDomJsonParser.getInstance().getJsonFromApAlSet(apAlSetVO))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                deleteItemMsg();
                Logger.getLogger(TachometerClient.class.getName()).log(Level.INFO, "CustomerApSet successfully deleted.");
                return true;
            } else {
                Logger.getLogger(TachometerClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(TachometerClient.class.getName()).log(Level.WARNING, "Error Message from Presets Service: {0}", APALSetDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        showErrorMsg();
        return false;
    }
}
