/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.http.client;

import com.uptime.client.http.client.Client;
import com.uptime.client.utils.vo.HttpResponseVO;
import com.uptime.upcastcm.bean.UserManageBean;
import com.uptime.upcastcm.http.utils.json.dom.PointDomJsonParser;
import com.uptime.upcastcm.http.utils.json.sax.PointSaxJsonParser;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import static com.uptime.upcastcm.utils.helperclass.GenericMessage.showErrorMsg;
import com.uptime.upcastcm.utils.vo.PointVO;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 *
 * @author twilcox
 */
public class PointClient extends Client {
    private static PointClient instance = null;
    
    /**
     * Private Singleton class
     */
    private PointClient() {
    }

    /**
     * Returns an instance of the class
     * @return PointClient object
     */
    public static PointClient getInstance() {
        if (instance == null) {
            instance = new PointClient();
        }
        return instance; 
    }
    
    /**
     * Get Point Info based on the given info
     * @param host String Object
     * @param customer String Object
     * @param siteId UUID Object
     * @param areaId UUID Object
     * @param assetId UUID Object
     * @param onlyActive boolean, true if isDisable is false, else all
     * @return List Object of PointVO Objects
     */
    public List<PointVO> getPointsByCustomerSiteAreaAsset(String host, String customer, UUID siteId, UUID areaId, UUID assetId, boolean onlyActive) {
        List<PointVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;
        
        try {
            endPoint = new StringBuilder();
            endPoint
                .append("/config/point")
                .append("?customer=").append(URLEncoder.encode(customer, "UTF-8"))
                .append("&siteId=").append(siteId)
                .append("&areaId=").append(areaId)
                .append("&assetId=").append(assetId)
                .append("&points=").append(true);
            if ((responseVO = httpGet(CONFIGURATION_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        list = PointSaxJsonParser.getInstance().populatePointsFromJson(responseVO.getEntity());
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(PointClient.class.getName()).log(Level.WARNING, "Message from Config Service: {0}", PointDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(PointClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(PointClient.class.getName()).log(Level.WARNING, "Error Message from Config Service: {0}", PointDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(PointClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        
        if (onlyActive) {
            list = list.stream().filter(vo -> !vo.isDisabled()).collect(Collectors.toList());
        }
        return list;
    }
    
    /**
     * Get Point Info based on the given info
     * @param host String Object
     * @param customer String Object
     * @param siteId UUID Object
     * @param areaId UUID Object
     * @param assetId UUID Object
     * @param pointLocationId UUID Object
     * @param onlyActive boolean, true if isDisable is false, else all
     * @return List Object of PointVO Objects
     */
    public List<PointVO> getPointsByCustomerSiteAreaAssetPointLocation(String host, String customer, UUID siteId, UUID areaId, UUID assetId, UUID pointLocationId, boolean onlyActive) {
        List<PointVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;
        
        try {
            endPoint = new StringBuilder();
            endPoint
                .append("/config/point")
                .append("?customer=").append(URLEncoder.encode(customer, "UTF-8"))
                .append("&siteId=").append(siteId)
                .append("&areaId=").append(areaId)
                .append("&assetId=").append(assetId)
                .append("&pointLocationId=").append(pointLocationId)
                .append("&points=").append(true);
            
            if ((responseVO = httpGet(CONFIGURATION_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        list = PointSaxJsonParser.getInstance().populatePointsFromJson(responseVO.getEntity());
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(PointClient.class.getName()).log(Level.WARNING, "Message from Config Service: {0}", PointDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(PointClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(PointClient.class.getName()).log(Level.WARNING, "Error Message from Config Service: {0}", PointDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(PointClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        
        if (onlyActive) {
            list = list.stream().filter(vo -> !vo.isDisabled()).collect(Collectors.toList());
        }
        return list;
    }
    
    /**
     * Get Point Info based on the given info
     * @param host String Object
     * @param customer String Object
     * @param siteId UUID Object
     * @param areaId UUID Object
     * @param assetId UUID Object
     * @param pointLocationId UUID Object
     * @param pointId UUID Object
     * @param onlyActive boolean, true if isDisable is false, else all
     * @return List Object of PointVO Objects
     */
    public List<PointVO> getPointsByCustomerSiteAreaAssetPointLocationPoint(String host, String customer, UUID siteId, UUID areaId, UUID assetId, UUID pointLocationId, UUID pointId, boolean onlyActive) {
        List<PointVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;
        
        try {
            endPoint = new StringBuilder();
            endPoint
                .append("/config/point")
                .append("?customer=").append(URLEncoder.encode(customer, "UTF-8"))
                .append("&siteId=").append(siteId)
                .append("&areaId=").append(areaId)
                .append("&assetId=").append(assetId)
                .append("&pointLocationId=").append(pointLocationId)
                .append("&pointId=").append(pointId)
                .append("&points=").append(true);
            
            if ((responseVO = httpGet(CONFIGURATION_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        list = PointSaxJsonParser.getInstance().populatePointsFromJson(responseVO.getEntity());
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(PointClient.class.getName()).log(Level.WARNING, "Message from Config Service: {0}", PointDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(PointClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(PointClient.class.getName()).log(Level.WARNING, "Error Message from Config Service: {0}", PointDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(PointClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        
        if (onlyActive) {
            list = list.stream().filter(vo -> !vo.isDisabled()).collect(Collectors.toList());
        }
        
        return list;
    }
    
    /**
     * Get Point Info based on the given info
     * @param host String Object
     * @param customer String Object
     * @param siteId UUID Object
     * @param areaId UUID Object
     * @param assetId UUID Object
     * @param pointLocationId UUID Object
     * @param pointId UUID Object
     * @return List Object of PointVO Objects
     */
    public List<PointVO> getPointsByCustomerSiteAreaAssetPointLocationPointWithApsets(String host, String customer, UUID siteId, UUID areaId, UUID assetId, UUID pointLocationId, UUID pointId) {
        List<PointVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;
        
        try {
            endPoint = new StringBuilder();
            endPoint
                .append("/config/point")
                .append("?customer=").append(URLEncoder.encode(customer, "UTF-8"))
                .append("&siteId=").append(siteId)
                .append("&areaId=").append(areaId)
                .append("&assetId=").append(assetId)
                .append("&pointLocationId=").append(pointLocationId)
                .append("&pointId=").append(pointId)
                .append("&points=").append(true)
                .append("&apAlByPoint=").append(true);
            
            if ((responseVO = httpGet(CONFIGURATION_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        list = PointDomJsonParser.getInstance().populatePointsWithApAlSetsFromJson(responseVO.getEntity());
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(PointClient.class.getName()).log(Level.WARNING, "Message from Config Service: {0}", PointDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(PointClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(PointClient.class.getName()).log(Level.WARNING, "Error Message from Config Service: {0}", PointDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(PointClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        
        return list;
    }
    
    /**
     * Get Point Info based on the given info
     * @param host String Object
     * @param customer String Object
     * @param siteId UUID Object
     * @param areaId UUID Object
     * @param assetId UUID Object
     * @param pointLocationId UUID Object
     * @param pointId UUID Object
     * @param apSetId UUID Object
     * @return List Object of PointVO Objects
     */
    public List<PointVO> getPointsByPK(String host, String customer, UUID siteId, UUID areaId, UUID assetId, UUID pointLocationId, UUID pointId, UUID apSetId) {
        List<PointVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;
        
        try {
            endPoint = new StringBuilder();
            endPoint
                .append("/config/point")
                .append("?customer=").append(URLEncoder.encode(customer, "UTF-8"))
                .append("&siteId=").append(siteId)
                .append("&areaId=").append(areaId)
                .append("&assetId=").append(assetId)
                .append("&pointLocationId=").append(pointLocationId)
                .append("&pointId=").append(pointId)
                .append("&apSetId=").append(apSetId)
                .append("&points=").append(true);
            
            if ((responseVO = httpGet(CONFIGURATION_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        list = PointSaxJsonParser.getInstance().populatePointsFromJson(responseVO.getEntity());
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(PointClient.class.getName()).log(Level.WARNING, "Message from Config Service: {0}", PointDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(PointClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(PointClient.class.getName()).log(Level.WARNING, "Error Message from Config Service: {0}", PointDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(PointClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        
        return list;
    }
    
    /**
     * Get Point Info based on the given info
     * @param host String Object
     * @param customer String Object
     * @param siteId UUID Object
     * @param areaId UUID Object
     * @param assetId UUID Object
     * @param pointLocationId UUID Object
     * @param pointId UUID Object
     * @param apSetId UUID Object
     * @return List Object of PointVO Objects
     */
    public List<PointVO> getApALByPoints_bySiteAreaAssetPointLocationPointApSet(String host, String customer, UUID siteId, UUID areaId, UUID assetId, UUID pointLocationId, UUID pointId, UUID apSetId) {
        List<PointVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;
        
        try {
            endPoint = new StringBuilder();
            endPoint
                .append("/config/point")
                .append("?customer=").append(URLEncoder.encode(customer, "UTF-8"))
                .append("&siteId=").append(siteId)
                .append("&areaId=").append(areaId)
                .append("&assetId=").append(assetId)
                .append("&pointLocationId=").append(pointLocationId)
                .append("&pointId=").append(pointId)
                .append("&apSetId=").append(apSetId)
                .append("&apAlByPoint=").append(true);
            
            if ((responseVO = httpGet(CONFIGURATION_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        list = PointSaxJsonParser.getInstance().populatePointsFromJson(responseVO.getEntity());
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(PointClient.class.getName()).log(Level.WARNING, "Message from Config Service: {0}", PointDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(PointClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(PointClient.class.getName()).log(Level.WARNING, "Error Message from Config Service: {0}", PointDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(PointClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        
        return list;
    }
    
    /**
     * Get Point Info based on the given info
     * @param host String Object
     * @param customer String Object
     * @param siteId UUID Object
     * @param areaId UUID Object
     * @param assetId UUID Object
     * @param pointLocationId UUID Object
     * @param pointId UUID Object
     * @param apSetId UUID Object
     * @param alSetId UUID Object
     * @return List Object of PointVO Objects
     */
    public List<PointVO> getApALByPoints_bySiteAreaAssetPointLocationPointApSetAlSet(String host, String customer, UUID siteId, UUID areaId, UUID assetId, UUID pointLocationId, UUID pointId, UUID apSetId, UUID alSetId) {
        List<PointVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;
        
        try {
            endPoint = new StringBuilder();
            endPoint
                .append("/config/point")
                .append("?customer=").append(URLEncoder.encode(customer, "UTF-8"))
                .append("&siteId=").append(siteId)
                .append("&areaId=").append(areaId)
                .append("&assetId=").append(assetId)
                .append("&pointLocationId=").append(pointLocationId)
                .append("&pointId=").append(pointId)
                .append("&apSetId=").append(apSetId)
                .append("&alSetId=").append(alSetId)
                .append("&apAlByPoint=").append(true);
            
            if ((responseVO = httpGet(CONFIGURATION_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        list = PointSaxJsonParser.getInstance().populatePointsFromJson(responseVO.getEntity());
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(PointClient.class.getName()).log(Level.WARNING, "Message from Config Service: {0}", PointDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(PointClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(PointClient.class.getName()).log(Level.WARNING, "Error Message from Config Service: {0}", PointDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(PointClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        
        return list;
    }
    
    /**
     * Get Point Info based on the given info
     * @param host String Object
     * @param customer String Object
     * @param siteId UUID Object
     * @param areaId UUID Object
     * @param assetId UUID Object
     * @param pointLocationId UUID Object
     * @param pointId UUID Object
     * @param apSetId UUID Object
     * @param alSetId UUID Object
     * @param paramName String Object
     * @return List Object of PointVO Objects
     */
    public List<PointVO> getApALByPoints_byPK(String host, String customer, UUID siteId, UUID areaId, UUID assetId, UUID pointLocationId, UUID pointId, UUID apSetId, UUID alSetId, String paramName) {
        List<PointVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;
        
        try {
            endPoint = new StringBuilder();
            endPoint
                .append("/config/point")
                .append("?customer=").append(URLEncoder.encode(customer, "UTF-8"))
                .append("&siteId=").append(siteId)
                .append("&areaId=").append(areaId)
                .append("&assetId=").append(assetId)
                .append("&pointLocationId=").append(pointLocationId)
                .append("&pointId=").append(pointId)
                .append("&apSetId=").append(apSetId)
                .append("&alSetId=").append(alSetId)
                .append("&paramName=").append(URLEncoder.encode(paramName, "UTF-8"))
                .append("&apAlByPoint=").append(true);
            
            if ((responseVO = httpGet(CONFIGURATION_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        list = PointSaxJsonParser.getInstance().populatePointsFromJson(responseVO.getEntity());
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(PointClient.class.getName()).log(Level.WARNING, "Message from Config Service: {0}", PointDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(PointClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(PointClient.class.getName()).log(Level.WARNING, "Error Message from Config Service: {0}", PointDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(PointClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        
        return list;
    }
    
    
    /**
     * Create a new Point by inserting into point table
     *
     * @param host String Object
     * @param pointVO PointVO Object
     * @return boolean
     */
    public boolean createPoint(String host, PointVO pointVO) {
        UserManageBean userManageBean;
        HttpResponseVO responseVO;
        
        if ((responseVO = httpPost(CONFIGURATION_SERVICE_NAME, host, "/config/point", PointDomJsonParser.getInstance().getJsonFromPointVO(pointVO))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
                FacesContext.getCurrentInstance().addMessage("nonAutoGrowlMessages", new FacesMessage(userManageBean.getResourceBundleString("growl_create_point_2")));
                Logger.getLogger(PointClient.class.getName()).log(Level.INFO, "Point created successfully.");
                return true;
            } else {
                Logger.getLogger(PointClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(PointClient.class.getName()).log(Level.WARNING, "Error Message from Config Service: {0}", PointDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        showErrorMsg();

        return false;
    }
    
    /**
     * Create a new Point by inserting into point table
     *
     * @param host String Object
     * @param pointVOs list
     * @return boolean
     */
    public boolean createPoint(String host, List<PointVO> pointVOs) {
        UserManageBean userManageBean;
        HttpResponseVO responseVO;
        
        if ((responseVO = httpPost(CONFIGURATION_SERVICE_NAME, host, "/config/point", PointDomJsonParser.getInstance().getJsonFromPointVO(pointVOs))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
                FacesContext.getCurrentInstance().addMessage("nonAutoGrowlMessages", new FacesMessage(userManageBean.getResourceBundleString("growl_create_point_2")));
                Logger.getLogger(PointClient.class.getName()).log(Level.INFO, "Point created successfully.");
                return true;
            } else {
                Logger.getLogger(PointClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(PointClient.class.getName()).log(Level.WARNING, "Error Message from Config Service: {0}", PointDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        showErrorMsg();

        return false;
    }

    /**
     * Update the given Point in the point table
     *
     * @param host String Object
     * @param pointVO PointVO Object
     * @return boolean
     */
    public boolean updatePoint(String host, PointVO pointVO) {
        UserManageBean userManageBean;
        HttpResponseVO responseVO;
        
        if ((responseVO = httpPut(CONFIGURATION_SERVICE_NAME, host, "/config/point", PointDomJsonParser.getInstance().getJsonFromPointVO(pointVO))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
                FacesContext.getCurrentInstance().addMessage("nonAutoGrowlMessages", new FacesMessage(userManageBean.getResourceBundleString("growl_update_point")));
                Logger.getLogger(PointClient.class.getName()).log(Level.INFO, "Point updated successfully.");
                return true;
            } else {
                Logger.getLogger(PointClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(PointClient.class.getName()).log(Level.WARNING, "Error Message from Config Service: {0}", PointDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        showErrorMsg();

        return false;
    }
    /**
     * Update the given Point in the point table
     *
     * @param host String Object
     * @param pointVOs PointVO's Object
     * @return boolean
     */
    public boolean updatePoint(String host, List<PointVO> pointVOs) {
        UserManageBean userManageBean;
        HttpResponseVO responseVO;
        
        if ((responseVO = httpPut(CONFIGURATION_SERVICE_NAME, host, "/config/point", PointDomJsonParser.getInstance().getJsonFromPointVO(pointVOs))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
                FacesContext.getCurrentInstance().addMessage("nonAutoGrowlMessages", new FacesMessage(userManageBean.getResourceBundleString("growl_update_point")));
                Logger.getLogger(PointClient.class.getName()).log(Level.INFO, "Point updated successfully.");
                return true;
            } else {
                Logger.getLogger(PointClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(PointClient.class.getName()).log(Level.WARNING, "Error Message from Config Service: {0}", PointDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        showErrorMsg();

        return false;
    }

    /**
     * Delete the given Point from the point table
     *
     * @param host String Object
     * @param pointVO PointVO Object
     * @return boolean
     */
    public boolean deletePoint(String host, PointVO pointVO) throws IllegalArgumentException {
        UserManageBean userManageBean;
        HttpResponseVO responseVO;
        
        if ((responseVO = httpDelete(CONFIGURATION_SERVICE_NAME, host, "/config/point", PointDomJsonParser.getInstance().getJsonFromPointVO(pointVO))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
                FacesContext.getCurrentInstance().addMessage("growlMessages", new FacesMessage(userManageBean.getResourceBundleString("growl_delete_point")));
                Logger.getLogger(PointClient.class.getName()).log(Level.INFO, "Point deleted successfully.");
                return true;
            } else {
                Logger.getLogger(PointClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(PointClient.class.getName()).log(Level.WARNING, "Error Message from Config Service: {0}", PointDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        showErrorMsg();

        return false;
    }
}
