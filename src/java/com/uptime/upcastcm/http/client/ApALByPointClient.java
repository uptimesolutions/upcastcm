/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.http.client;

import com.uptime.client.http.client.Client;
import com.uptime.client.utils.vo.HttpResponseVO;
import com.uptime.upcastcm.bean.UserManageBean;
import com.uptime.upcastcm.http.utils.json.dom.PointDomJsonParser;
import com.uptime.upcastcm.http.utils.json.sax.PointSaxJsonParser;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import static com.uptime.upcastcm.utils.helperclass.GenericMessage.showErrorMsg;
import com.uptime.upcastcm.utils.vo.PointVO;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 *
 * @author gsingh
 */
public class ApALByPointClient extends Client {
    private static ApALByPointClient instance = null;
    
    /**
     * Private Singleton class
     */
    private ApALByPointClient() {
    }

    /**
     * Returns an instance of the class
     * 
     * @return PointClient object
     */
    public static ApALByPointClient getInstance() {
        if (instance == null) {
            instance = new ApALByPointClient();
        }
        return instance; 
    }
    
    /**
     * Get Point Info based on the given info
     * @param host String Object
     * @param customer String Object
     * @param siteId UUID Object
     * @param areaId UUID Object
     * @param assetId UUID Object
     * @param pointLocationId UUID Object
     * @param pointId UUID Object
     * @param apSetId UUID Object
     * @return List Object of PointVO Objects
     */
    public List<PointVO> getApALByPoints_bySiteAreaAssetPointLocationPointApSet(String host, String customer, UUID siteId, UUID areaId, UUID assetId, UUID pointLocationId, UUID pointId, UUID apSetId) {
        List<PointVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;
        
        try {
            endPoint = new StringBuilder();
            endPoint
                .append("/config/point")
                .append("?customer=").append(URLEncoder.encode(customer, "UTF-8"))
                .append("&siteId=").append(siteId)
                .append("&areaId=").append(areaId)
                .append("&assetId=").append(assetId)
                .append("&pointLocationId=").append(pointLocationId)
                .append("&pointId=").append(pointId)
                .append("&apSetId=").append(apSetId)
                .append("&apAlByPoint=").append(true);
            
            if ((responseVO = httpGet(CONFIGURATION_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        list = PointSaxJsonParser.getInstance().populatePointsFromJson(responseVO.getEntity());
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(ApALByPointClient.class.getName()).log(Level.WARNING, "Message from Config Service: {0}", PointDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(ApALByPointClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(ApALByPointClient.class.getName()).log(Level.WARNING, "Error Message from Config Service: {0}", PointDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(ApALByPointClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }
    
    /**
     * Get Point Info based on the given info
     * @param host String Object
     * @param customer String Object
     * @param siteId UUID Object
     * @param areaId UUID Object
     * @param assetId UUID Object
     * @param pointLocationId UUID Object
     * @param pointId UUID Object
     * @param apSetId UUID Object
     * @param alSetId UUID Object
     * @return List Object of PointVO Objects
     */
    public List<PointVO> getApALByPoints_bySiteAreaAssetPointLocationPointApSetAlSet(String host, String customer, UUID siteId, UUID areaId, UUID assetId, UUID pointLocationId, UUID pointId, UUID apSetId, UUID alSetId) {
        List<PointVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;
        
        try {
            endPoint = new StringBuilder();
            endPoint
                .append("/config/point")
                .append("?customer=").append(URLEncoder.encode(customer, "UTF-8"))
                .append("&siteId=").append(siteId)
                .append("&areaId=").append(areaId)
                .append("&assetId=").append(assetId)
                .append("&pointLocationId=").append(pointLocationId)
                .append("&pointId=").append(pointId)
                .append("&apSetId=").append(apSetId)
                .append("&alSetId=").append(alSetId)
                .append("&apAlByPoint=").append(true);
            
            if ((responseVO = httpGet(CONFIGURATION_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        list = PointSaxJsonParser.getInstance().populatePointsFromJson(responseVO.getEntity());
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(ApALByPointClient.class.getName()).log(Level.WARNING, "Message from Config Service: {0}", PointDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(ApALByPointClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(ApALByPointClient.class.getName()).log(Level.WARNING, "Error Message from Config Service: {0}", PointDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(ApALByPointClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }
    
    /**
     * Get Point Info based on the given info
     * @param host String Object
     * @param customer String Object
     * @param siteId UUID Object
     * @param areaId UUID Object
     * @param assetId UUID Object
     * @param pointLocationId UUID Object
     * @param pointId UUID Object
     * @param apSetId UUID Object
     * @param alSetId UUID Object
     * @param paramName String Object
     * @return List Object of PointVO Objects
     */
    public List<PointVO> getApALByPoints_byPK(String host, String customer, UUID siteId, UUID areaId, UUID assetId, UUID pointLocationId, UUID pointId, UUID apSetId, UUID alSetId, String paramName) {
        List<PointVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;
        
        try {
            endPoint = new StringBuilder();
            endPoint
                .append("/config/point")
                .append("?customer=").append(URLEncoder.encode(customer, "UTF-8"))
                .append("&siteId=").append(siteId)
                .append("&areaId=").append(areaId)
                .append("&assetId=").append(assetId)
                .append("&pointLocationId=").append(pointLocationId)
                .append("&pointId=").append(pointId)
                .append("&apSetId=").append(apSetId)
                .append("&alSetId=").append(alSetId)
                .append("&paramName=").append(URLEncoder.encode(paramName, "UTF-8"))
                .append("&apAlByPoint=").append(true);
            
            if ((responseVO = httpGet(CONFIGURATION_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        list = PointSaxJsonParser.getInstance().populatePointsFromJson(responseVO.getEntity());
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(ApALByPointClient.class.getName()).log(Level.WARNING, "Message from Config Service: {0}", PointDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(ApALByPointClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(ApALByPointClient.class.getName()).log(Level.WARNING, "Error Message from Config Service: {0}", PointDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(ApALByPointClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }
    
    /**
     * Update the given Point in the point table
     *
     * @param host String Object
     * @param pointVO PointVO Object
     * @param growlType
     * @return boolean
     */
    public boolean updateApALByPoint(String host, PointVO pointVO, String growlType) {
        UserManageBean userManageBean;
        HttpResponseVO responseVO;
        
        Logger.getLogger(ApALByPointClient.class.getName()).log(Level.INFO, "PointDomJsonParser JSON**************: {0}", PointDomJsonParser.getInstance().getJsonFromPointVO(pointVO));
        if ((responseVO = httpPut(CONFIGURATION_SERVICE_NAME, host, "/config/apalbypoint", PointDomJsonParser.getInstance().getJsonFromPointVO(pointVO))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
                FacesContext.getCurrentInstance().addMessage(growlType, new FacesMessage(userManageBean.getResourceBundleString("growl_update_al")));
                Logger.getLogger(ApALByPointClient.class.getName()).log(Level.INFO, "Alarm limits updated successfully.");
                return true;
            } else {
                Logger.getLogger(ApALByPointClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(ApALByPointClient.class.getName()).log(Level.WARNING, "Error Message from Config Service: {0}", PointDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        showErrorMsg();

        return false;
    }

}
