/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.http.client;

import com.uptime.client.http.client.Client;
import com.uptime.client.utils.vo.HttpResponseVO;
import com.uptime.upcastcm.http.utils.json.dom.DevicePresetDomJsonParser;
import com.uptime.upcastcm.http.utils.json.dom.FaultFrequenciesDomJsonParser;
import com.uptime.upcastcm.http.utils.json.dom.TachometerDomJsonParser;
import com.uptime.upcastcm.http.utils.json.sax.DevicePresetSaxJsonParser;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import static com.uptime.upcastcm.utils.helperclass.GenericMessage.setNonAutoGrowlMsg;
import static com.uptime.upcastcm.utils.helperclass.GenericMessage.showErrorMsg;
import com.uptime.upcastcm.utils.vo.DevicePresetVO;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author kpati
 */
public class DevicePresetClient extends Client {
    private static DevicePresetClient instance = null;
    
    /**
     * Private Singleton class
     */
    private DevicePresetClient() {
    }

    /**
     * Returns an instance of the class
     *
     * @return DevicePresetClient object
     */
    public static DevicePresetClient getInstance() {
        if (instance == null) {
            instance = new DevicePresetClient();
        }
        return instance;
    }
    
    /**
     * Get Alarms Info based on the given info
     *
     * @param host String Object
     * @param customerAccount, String Object
     * @param siteId, String Object
     * @param deviceType, String Object
     * @return List Object of HwUnitVO Objects
     */
    public List<DevicePresetVO> getSiteDeviceByCustomerSiteDeviceType(String host, String customerAccount, String siteId, String deviceType) {
        List<DevicePresetVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;

        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/presets/siteDevice")
                    .append("?customerAccount=").append(URLEncoder.encode(customerAccount, "UTF-8"))
                    .append("&siteId=").append(siteId)
                    .append("&deviceType=").append(deviceType);
            
            if ((responseVO = httpGet(PRESET_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        list = DevicePresetSaxJsonParser.getInstance().populateSiteDevicePresetsFromJson(responseVO.getEntity());
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(DevicePresetClient.class.getName()).log(Level.WARNING, "Message from Presets Service: {0}", DevicePresetSaxJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(DevicePresetClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(DevicePresetClient.class.getName()).log(Level.WARNING, "Error Message from Presets Service: {0}", DevicePresetSaxJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(DevicePresetClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }
    
    
     /**
     * Get DevicePresetVOs Info based on the given info
     *
     * @param host String Object
     * @param customer String Object
     * @param siteId
     * @return List Object of DevicePresetVO Objects
     */
    public List<DevicePresetVO> getDevicePresetVOs(String host, String customer, UUID siteId) {
        List<DevicePresetVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;

        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/presets/siteDevice")
                    .append("?customerAccount=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&siteId=").append(siteId);
            
            if ((responseVO = httpGet(PRESET_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        list = DevicePresetSaxJsonParser.getInstance().populateSiteDevicePresetsFromJson(responseVO.getEntity());
                        list.forEach(l -> l.setType("Site"));
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(DevicePresetClient.class.getName()).log(Level.WARNING, "Message from Presets Service: {0}", FaultFrequenciesDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(DevicePresetClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(DevicePresetClient.class.getName()).log(Level.WARNING, "Error Message from Presets Service: {0}", FaultFrequenciesDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                setNonAutoGrowlMsg("growl_service_error_client_message");
            }
        } catch (Exception e) {
            Logger.getLogger(DevicePresetClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }

    
     /**
     * Get DevicePresetVOs Info based on the given info
     *
     * @param host String Object
     * @param customer String Object
     * @param siteId
     * @param deviceType
     * @param presetId
     * @return List Object of DevicePresetVO Objects
     */
    public List<DevicePresetVO> getSiteDevicePresetVOs(String host, String customer, UUID siteId, String deviceType, UUID presetId) {
        List<DevicePresetVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;

        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/presets/siteDevice")
                    .append("?customerAccount=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&siteId=").append(siteId)
                    .append("&deviceType=").append(URLEncoder.encode(deviceType, "UTF-8"))
                    .append("&presetId=").append(presetId);
            
            if ((responseVO = httpGet(PRESET_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        list = DevicePresetSaxJsonParser.getInstance().populateSiteDevicePresetsFromJson(responseVO.getEntity());
                        list.forEach(l -> l.setType("Site"));
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(DevicePresetClient.class.getName()).log(Level.WARNING, "Message from Presets Service: {0}", FaultFrequenciesDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(DevicePresetClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(DevicePresetClient.class.getName()).log(Level.WARNING, "Error Message from Presets Service: {0}", FaultFrequenciesDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                setNonAutoGrowlMsg("growl_service_error_client_message");
            }
        } catch (Exception e) {
            Logger.getLogger(DevicePresetClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }

    
    /**
     * Create the given SiteDevicePreset
     *
     * @param host, String Object
     * @param devicePresetsVOList, List of DevicePresetVOs Object
     * @return boolean, true if success, else false
     */
    public boolean createDevicePresetVO(String host, List<DevicePresetVO> devicePresetsVOList) {
        HttpResponseVO responseVO;

        if ((responseVO = httpPost(PRESET_SERVICE_NAME, host, "/presets/siteDevice", DevicePresetDomJsonParser.getInstance().getJsonFromDevicePresetVO(devicePresetsVOList))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                Logger.getLogger(DevicePresetClient.class.getName()).log(Level.INFO, "Site Device Presets successfully created.");
                return true;
            } else {
                Logger.getLogger(DevicePresetClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(DevicePresetClient.class.getName()).log(Level.WARNING, "Error Message from Site Device Presets Service: {0}", DevicePresetDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        setNonAutoGrowlMsg("growl_service_error_client_message");
        return false;
    }
 
    /**
     * Update the given SiteDevicePreset
     *
     * @param host, String Object
     * @param devicePresetsVOList, List of DevicePresetVOs Object
     * @return boolean, true if success, else false
     */
    public boolean updateSiteDevicePresetVO(String host, List<DevicePresetVO> devicePresetsVOList) {
        HttpResponseVO responseVO;

        if ((responseVO = httpPut(PRESET_SERVICE_NAME, host, "/presets/siteDevice", DevicePresetDomJsonParser.getInstance().getJsonFromDevicePresetVO(devicePresetsVOList))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {             
                Logger.getLogger(DevicePresetClient.class.getName()).log(Level.INFO, "Site Device Presets successfully updated.");
                return true;
            } else {
                Logger.getLogger(DevicePresetClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(DevicePresetClient.class.getName()).log(Level.WARNING, "Error Message from Site Device Presets Service: {0}", DevicePresetDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        setNonAutoGrowlMsg("growl_service_error_client_message");
        
        return false;
    }
     
   
    public boolean deletePresetDevices(String host, List<DevicePresetVO> devicePresetVOs) {
     HttpResponseVO responseVO;
        
        if ((responseVO = httpDelete(PRESET_SERVICE_NAME, host, "/presets/siteDevice", DevicePresetDomJsonParser.getInstance().getJsonFromDevicePresetVO(devicePresetVOs))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                Logger.getLogger(TachometerClient.class.getName()).log(Level.INFO, "Device successfully deleted.");
                return true;
            } else {
                Logger.getLogger(TachometerClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(TachometerClient.class.getName()).log(Level.WARNING, "Error Message from Presets Service: {0}", TachometerDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        showErrorMsg();
        
        return false;   
    }

    public boolean createGlobalDevicePresetVO(String host, List<DevicePresetVO> devicePresetsVOList) {
     HttpResponseVO responseVO;

        if ((responseVO = httpPost(PRESET_SERVICE_NAME, host, "/presets/globalDevice", DevicePresetDomJsonParser.getInstance().getJsonFromDevicePresetVO(devicePresetsVOList))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {                
                Logger.getLogger(DevicePresetClient.class.getName()).log(Level.INFO, "Global Device Presets successfully created.");
                return true;
            } else {
                Logger.getLogger(DevicePresetClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(DevicePresetClient.class.getName()).log(Level.WARNING, "Error Message from Global Device Presets Service: {0}", DevicePresetDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        setNonAutoGrowlMsg("growl_service_error_client_message");
        
        return false;  
    }
    
    
    /**
     * Update the given SiteDevicePreset
     *
     * @param host, String Object
     * @param devicePresetsVOList, List of DevicePresetVOs Object
     * @return boolean, true if success, else false
     */
    public boolean updateGlobalDevicePresetVO(String host, List<DevicePresetVO> devicePresetsVOList) {
        HttpResponseVO responseVO;

        if ((responseVO = httpPut(PRESET_SERVICE_NAME, host, "/presets/globalDevice", DevicePresetDomJsonParser.getInstance().getJsonFromDevicePresetVO(devicePresetsVOList))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {             
                Logger.getLogger(DevicePresetClient.class.getName()).log(Level.INFO, "Global Device Presets successfully updated.");
                return true;
            } else {
                Logger.getLogger(DevicePresetClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(DevicePresetClient.class.getName()).log(Level.WARNING, "Error Message from Global Device Presets Service: {0}", DevicePresetDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        setNonAutoGrowlMsg("growl_service_error_client_message");
        
        return false;
    }
     
   
    public boolean deleteGlobalPresetDevices(String host, List<DevicePresetVO> devicePresetVOs) {
     HttpResponseVO responseVO;
        
        if ((responseVO = httpDelete(PRESET_SERVICE_NAME, host, "/presets/globalDevice", DevicePresetDomJsonParser.getInstance().getJsonFromDevicePresetVO(devicePresetVOs))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                Logger.getLogger(TachometerClient.class.getName()).log(Level.INFO, "Device successfully deleted.");
                return true;
            } else {
                Logger.getLogger(TachometerClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(TachometerClient.class.getName()).log(Level.WARNING, "Error Message from Presets Service: {0}", TachometerDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        showErrorMsg();
        
        return false;   
    }

    public List<DevicePresetVO> getGlobalDevicePresetsByCustomer(String host, String customerAccount) {
     List<DevicePresetVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;

        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/presets/globalDevice")
                    .append("?customerAccount=").append(URLEncoder.encode(customerAccount, "UTF-8"));
            
            if ((responseVO = httpGet(PRESET_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        list = DevicePresetSaxJsonParser.getInstance().populateSiteDevicePresetsFromJson(responseVO.getEntity());
                        list.forEach(l -> l.setType("Global"));
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(DevicePresetClient.class.getName()).log(Level.WARNING, "Message from Presets Service: {0}", FaultFrequenciesDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(DevicePresetClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(DevicePresetClient.class.getName()).log(Level.WARNING, "Error Message from Presets Service: {0}", FaultFrequenciesDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                setNonAutoGrowlMsg("growl_service_error_client_message");
            }
        } catch (Exception e) {
            Logger.getLogger(DevicePresetClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;   
    }

    public List<DevicePresetVO> getGlobalDevicePresetsByCustomerDeviceType(String host, String customerAccount,String deviceType) {
     List<DevicePresetVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;

        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/presets/globalDevice")
                    .append("?customerAccount=").append(URLEncoder.encode(customerAccount, "UTF-8"))
                    .append("&deviceType=").append(URLEncoder.encode(deviceType, "UTF-8"));
            
            if ((responseVO = httpGet(PRESET_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        list = DevicePresetSaxJsonParser.getInstance().populateSiteDevicePresetsFromJson(responseVO.getEntity());
                        list.forEach(l -> l.setType("Global"));
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(DevicePresetClient.class.getName()).log(Level.WARNING, "Message from Presets Service: {0}", FaultFrequenciesDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(DevicePresetClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(DevicePresetClient.class.getName()).log(Level.WARNING, "Error Message from Presets Service: {0}", FaultFrequenciesDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                setNonAutoGrowlMsg("growl_service_error_client_message");
            }
        } catch (Exception e) {
            Logger.getLogger(DevicePresetClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;   
    }
    public Collection<? extends DevicePresetVO> getGlobalDevicePresetVOs(String host, String customerAccount, String deviceType, UUID presetId) {
    List<DevicePresetVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;

        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/presets/globalDevice")
                    .append("?customerAccount=").append(URLEncoder.encode(customerAccount, "UTF-8"))
                    .append("&deviceType=").append(URLEncoder.encode(deviceType, "UTF-8"))
                    .append("&presetId=").append(presetId);
            
            if ((responseVO = httpGet(PRESET_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        list = DevicePresetSaxJsonParser.getInstance().populateSiteDevicePresetsFromJson(responseVO.getEntity());
                       list.forEach(l -> l.setType("Global"));
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(DevicePresetClient.class.getName()).log(Level.WARNING, "Message from Presets Service: {0}", FaultFrequenciesDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(DevicePresetClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(DevicePresetClient.class.getName()).log(Level.WARNING, "Error Message from Presets Service: {0}", FaultFrequenciesDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                setNonAutoGrowlMsg("growl_service_error_client_message");
            }
        } catch (Exception e) {
            Logger.getLogger(DevicePresetClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;   
    }
}
