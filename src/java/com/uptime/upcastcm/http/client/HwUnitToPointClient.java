/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.http.client;

import com.uptime.client.http.client.Client;
import com.uptime.client.utils.vo.HttpResponseVO;
import com.uptime.upcastcm.http.utils.json.sax.HwUnitSaxJsonParser;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import static com.uptime.upcastcm.utils.helperclass.GenericMessage.showErrorMsg;
import com.uptime.upcastcm.utils.vo.HwUnitVO;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author kpati
 */
public class HwUnitToPointClient extends Client {
    private static HwUnitToPointClient instance = null;

    /**
     * Private Singleton class
     */
    private HwUnitToPointClient() {
    }

    /**
     * Returns an instance of the class
     *
     * @return HwUnitToPointClient object
     */
    public static HwUnitToPointClient getInstance() {
        if (instance == null) {
            instance = new HwUnitToPointClient();
        }
        return instance;
    }
    
    /**
     * Get HardwareUnit Info based on the given info
     *
     * @param host String Object
     * @param deviceId, String Object
     * @return List Object of HwUnitVO Objects
     */
    public List<HwUnitVO> getHwUnitToPointByDevice(String host, String deviceId) {
        List<HwUnitVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;

        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/config/hwUnit")
                    .append("?deviceId=").append(URLEncoder.encode(deviceId, "UTF-8"));
            
            if ((responseVO = httpGet(CONFIGURATION_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        list = HwUnitSaxJsonParser.getInstance().populateHwUnitFromJson(responseVO.getEntity());
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(HwUnitToPointClient.class.getName()).log(Level.WARNING, "Message from Config Service: {0}", HwUnitSaxJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(HwUnitToPointClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(HwUnitToPointClient.class.getName()).log(Level.WARNING, "Error Message from Config Service: {0}", HwUnitSaxJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(HwUnitToPointClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }
}
