/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.http.client;

import com.uptime.client.http.client.Client;
import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.client.utils.vo.HttpResponseVO;
import com.uptime.upcastcm.http.utils.json.dom.TachometerDomJsonParser;
import com.uptime.upcastcm.http.utils.json.sax.TachometerSaxJsonParser;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import static com.uptime.upcastcm.utils.helperclass.GenericMessage.createItemMsg;
import static com.uptime.upcastcm.utils.helperclass.GenericMessage.deleteItemMsg;
import static com.uptime.upcastcm.utils.helperclass.GenericMessage.failedSuccessUploads;
import static com.uptime.upcastcm.utils.helperclass.GenericMessage.showErrorMsg;
import static com.uptime.upcastcm.utils.helperclass.GenericMessage.updateItemMsg;
import com.uptime.upcastcm.utils.vo.TachometerVO;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author twilcox
 */
public class TachometerClient extends Client {
    private static TachometerClient instance = null;

    /**
     * Private Singleton class
     */
    private TachometerClient() {
    }

    /**
     * Returns an instance of the class
     *
     * @return TachometerClient object
     */
    public static TachometerClient getInstance() {
        if (instance == null) {
            instance = new TachometerClient();
        }
        return instance;
    }

    /**
     * Get Global Tachometers Info based on the given info
     *
     * @param host String Object
     * @param customer String Object
     * @return List Object of TachometerVO Objects
     */
    public List<TachometerVO> getGlobalTachometersByCustomer(String host, String customer) {
        List<TachometerVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;
        
        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/presets/globalTachometer")
                    .append("?customerAccount=").append(URLEncoder.encode(customer, "UTF-8"));
            
            if ((responseVO = httpGet(PRESET_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        list = TachometerSaxJsonParser.getInstance().populateTachometersFromJson(responseVO.getEntity());
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(TachometerClient.class.getName()).log(Level.WARNING, "Message from Presets Service: {0}", TachometerDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(TachometerClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(TachometerClient.class.getName()).log(Level.WARNING, "Error Message from Presets Service: {0}", TachometerDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(TachometerClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }

    /**
     * Get Global Tachometers Info based on the given info
     *
     * @param host String Object
     * @param customer String Object
     * @param tachId UUID Object
     * @return List Object of TachometerVO Objects
     */
    public List<TachometerVO> getGlobalTachometersByPK(String host, String customer, UUID tachId) {
        List<TachometerVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;
        
        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/presets/globalTachometer")
                    .append("?customerAccount=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&tachId=").append(tachId);
            
            if ((responseVO = httpGet(PRESET_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        list = TachometerSaxJsonParser.getInstance().populateTachometersFromJson(responseVO.getEntity());
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(TachometerClient.class.getName()).log(Level.WARNING, "Message from Presets Service: {0}", TachometerDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(TachometerClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(TachometerClient.class.getName()).log(Level.WARNING, "Error Message from Presets Service: {0}", TachometerDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(TachometerClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }

    /**
     * Get Site Tachometers Info based on the given info
     *
     * @param host String Object
     * @param customer String Object
     * @param siteId UUID Object
     * @return List Object of TachometerVO Objects
     */
    public List<TachometerVO> getSiteTachometersByCustomerSiteId(String host, String customer, UUID siteId) {
        List<TachometerVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;
        
        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/presets/siteTachometer")
                    .append("?customerAccount=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&siteId=").append(siteId);
            
            if ((responseVO = httpGet(PRESET_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        list = TachometerSaxJsonParser.getInstance().populateTachometersFromJson(responseVO.getEntity());
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(TachometerClient.class.getName()).log(Level.WARNING, "Message from Presets Service: {0}", TachometerDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(TachometerClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(TachometerClient.class.getName()).log(Level.WARNING, "Error Message from Presets Service: {0}", TachometerDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(TachometerClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }

    /**
     * Get Site Tachometers Info based on the given info
     *
     * @param host String Object
     * @param customer String Object
     * @param siteId UUID Object
     * @param tachId UUID Object
     * @return List Object of TachometerVO Objects
     */
    public List<TachometerVO> getSiteTachometersByPK(String host, String customer, UUID siteId, UUID tachId) {
        List<TachometerVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;
        
        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/presets/siteTachometer")
                    .append("?customerAccount=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&siteId=").append(siteId)
                    .append("&tachId=").append(tachId);
            
            if ((responseVO = httpGet(PRESET_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        list = TachometerSaxJsonParser.getInstance().populateTachometersFromJson(responseVO.getEntity());
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(TachometerClient.class.getName()).log(Level.WARNING, "Message from Presets Service: {0}", TachometerDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(TachometerClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(TachometerClient.class.getName()).log(Level.WARNING, "Error Message from Presets Service: {0}", TachometerDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(TachometerClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }
    
    /**
     * Create the given Global Tachometers
     *
     * @param host, String Object
     * @param tachometerVO, TachometerVO Object
     * @return boolean, true if success, else false
     */
    public boolean createGlobalTachometers(String host, TachometerVO tachometerVO) {
        HttpResponseVO responseVO;
        
        if ((responseVO = httpPost(PRESET_SERVICE_NAME, host, "/presets/globalTachometer", TachometerDomJsonParser.getInstance().getJsonFromTachometersVO(tachometerVO))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                Logger.getLogger(TachometerClient.class.getName()).log(Level.INFO, "Global Tachometers successfully created.");
                createItemMsg();
                return true;
            } else {
                Logger.getLogger(TachometerClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(TachometerClient.class.getName()).log(Level.WARNING, "Error Message from Presets Service: {0}", TachometerDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        showErrorMsg();
        
        return false;
    }
    
    /**
     * Create the given Site Tachometers
     *
     * @param host, String Object
     * @param tachometerVO, TachometerVO Object
     * @return boolean, true if success, else false
     */
    public boolean createSiteTachometers(String host, TachometerVO tachometerVO) {
        HttpResponseVO responseVO;
        
        if ((responseVO = httpPost(PRESET_SERVICE_NAME, host, "/presets/siteTachometer", TachometerDomJsonParser.getInstance().getJsonFromTachometersVO(tachometerVO))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                Logger.getLogger(TachometerClient.class.getName()).log(Level.INFO, "Site Tachometers successfully created.");
                createItemMsg();
                return true;
            } else {
                Logger.getLogger(TachometerClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(TachometerClient.class.getName()).log(Level.WARNING, "Error Message from Presets Service: {0}", TachometerDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        showErrorMsg();
        
        return false;
    }
    
    /**
     * Create multipule tachometers
     * @param tachometerVOList, List Object of TachometerVO Objects
     * @param errorCount, int 
     * @param alreadyExist, int
     */
    public void createSiteTachometers(List<TachometerVO> tachometerVOList, int errorCount, int alreadyExist) {
        HttpResponseVO responseVO;
        int successCount = 0;
        
        if (tachometerVOList != null) {
            for (TachometerVO tachometerVO : tachometerVOList) {
                if ((responseVO = httpPost(PRESET_SERVICE_NAME, ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), "/presets/siteTachometer", TachometerDomJsonParser.getInstance().getJsonFromTachometersVO(tachometerVO))) != null && responseVO.isHostFound()) {
                    if (responseVO.getStatusCode() == 200) {
                        Logger.getLogger(TachometerClient.class.getName()).log(Level.INFO, "Site Tachometers successfully created.");
                        successCount++;
                    } else {
                        Logger.getLogger(TachometerClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(TachometerClient.class.getName()).log(Level.WARNING, "Error Message from Presets Service: {0}", TachometerDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        errorCount++;
                    }
                }
            }
        }
        failedSuccessUploads(successCount, errorCount, alreadyExist);
    }
    
    /**
     * Update the given Global Tachometers
     *
     * @param host, String Object
     * @param tachometerVO, TachometerVO Object
     * @return boolean, true if success, else false
     */
    public boolean updateGlobalTachometers(String host, TachometerVO tachometerVO) {
        HttpResponseVO responseVO;
        
        if ((responseVO = httpPut(PRESET_SERVICE_NAME, host, "/presets/globalTachometer", TachometerDomJsonParser.getInstance().getJsonFromTachometersVO(tachometerVO))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                updateItemMsg();
                Logger.getLogger(TachometerClient.class.getName()).log(Level.INFO, "Global Tachometers successfully updated.");
                return true;
            } else {
                Logger.getLogger(TachometerClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(TachometerClient.class.getName()).log(Level.WARNING, "Error Message from Presets Service: {0}", TachometerDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        showErrorMsg();
        
        return false;
    }
    
    /**
     * Update the given Site Tachometers
     *
     * @param host, String Object
     * @param tachometerVO, TachometerVO Object
     * @return boolean, true if success, else false
     */
    public boolean updateSiteTachometers(String host, TachometerVO tachometerVO) {
        HttpResponseVO responseVO;
        
        if ((responseVO = httpPut(PRESET_SERVICE_NAME, host, "/presets/siteTachometer", TachometerDomJsonParser.getInstance().getJsonFromTachometersVO(tachometerVO))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                updateItemMsg();
                Logger.getLogger(TachometerClient.class.getName()).log(Level.INFO, "Site Tachometers successfully updated.");
                return true;
            } else {
                Logger.getLogger(TachometerClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(TachometerClient.class.getName()).log(Level.WARNING, "Error Message from Presets Service: {0}", TachometerDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        showErrorMsg();
        
        return false;
    }
    
    /**
     * Delete the given Global Tachometers
     *
     * @param host, String Object
     * @param tachometerVO, TachometerVO Object
     * @return boolean, true if success, else false
     */
    public boolean deleteGlobalTachometers(String host, TachometerVO tachometerVO) {
        HttpResponseVO responseVO;
        
        if ((responseVO = httpDelete(PRESET_SERVICE_NAME, host, "/presets/globalTachometer", TachometerDomJsonParser.getInstance().getJsonFromTachometersVO(tachometerVO))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                deleteItemMsg();
                Logger.getLogger(TachometerClient.class.getName()).log(Level.INFO, "Global Tachometers successfully deleted.");
                return true;
            } else {
                Logger.getLogger(TachometerClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(TachometerClient.class.getName()).log(Level.WARNING, "Error Message from Presets Service: {0}", TachometerDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        showErrorMsg();
        
        return false;
    }
    
    /**
     * Delete the given Site Tachometers
     *
     * @param host, String Object
     * @param tachometerVO, TachometerVO Object
     * @return boolean, true if success, else false
     */
    public boolean deleteSiteTachometers(String host, TachometerVO tachometerVO) {
        HttpResponseVO responseVO;
        
        if ((responseVO = httpDelete(PRESET_SERVICE_NAME, host, "/presets/siteTachometer", TachometerDomJsonParser.getInstance().getJsonFromTachometersVO(tachometerVO))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                deleteItemMsg();
                Logger.getLogger(TachometerClient.class.getName()).log(Level.INFO, "Site Tachometers successfully deleted.");
                return true;
            } else {
                Logger.getLogger(TachometerClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(TachometerClient.class.getName()).log(Level.WARNING, "Error Message from Presets Service: {0}", TachometerDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        showErrorMsg();
        
        return false;
    }
}
