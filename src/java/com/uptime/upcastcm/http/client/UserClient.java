/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.http.client;

import com.uptime.client.http.client.Client;
import com.uptime.client.utils.vo.HttpResponseVO;
import com.uptime.upcastcm.bean.UserManageBean;
import com.uptime.upcastcm.http.utils.json.sax.UserSaxJsonParser;
import com.uptime.upcastcm.http.utils.json.dom.UserDomJsonParser;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import static com.uptime.upcastcm.utils.helperclass.GenericMessage.showErrorMsg;
import com.uptime.upcastcm.utils.vo.SiteUsersVO;
import com.uptime.upcastcm.utils.vo.UserPreferencesVO;
import com.uptime.upcastcm.utils.vo.UserSitesVO;
import com.uptime.upcastcm.utils.vo.PasswordTokenVO;
import java.io.Serializable;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 *
 * @author madhavi
 */
public class UserClient extends Client implements Serializable {
    private static UserClient instance = null;

    private UserClient() {

    }

    /**
     * Returns an instance of the class
     *
     * @return UserClient object
     */
    public static UserClient getInstance() {
        if (instance == null) {
            instance = new UserClient();
        }
        return instance;
    }

    /**
     * @param host, String Object
     * @param userId
     * @param siteId
     * @param customerAccount
     * @return list of SiteUsersVO objects.
     */
    public List<SiteUsersVO> getSiteUsersByPK(String host, String userId, String siteId, String customerAccount) {
        List<SiteUsersVO> siteList = new ArrayList();
        HttpResponseVO responseVO;
        boolean serviceIssue = true;

        try {
            if ((responseVO = httpGet(USER_PREFERENCE_SERVICE_NAME, host, "/user/sites?userId=" + URLEncoder.encode(userId, "UTF-8") + "&customerAccount=" + URLEncoder.encode(customerAccount, "UTF-8") + "&siteId=" + URLEncoder.encode(siteId, "UTF-8") + "&returnType=siteUsers")) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        siteList = UserSaxJsonParser.getInstance().populateSiteUsersFromJson(responseVO.getEntity());
                        Logger.getLogger(UserClient.class.getName()).log(Level.INFO, "Fetched Site Users");
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(UserClient.class.getName()).log(Level.WARNING, "Message from User Service: {0}", UserDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(UserClient.class.getName()).log(Level.WARNING, "Error Code from User Service : {0}", responseVO.getStatusCode());
                        Logger.getLogger(UserClient.class.getName()).log(Level.WARNING, "Error Message from User Service : {0}", responseVO.getEntity());
                        Logger.getLogger(UserClient.class.getName()).log(Level.WARNING, "errMsg : {0}", UserDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }

            } else {
                Logger.getLogger(UserClient.class.getName()).log(Level.WARNING, "Error - No host found for User Service.");
            }
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(UserClient.class.getName()).log(Level.WARNING, e.getMessage(), e);
        }
        return siteList;
    }

    /**
     * @param host, String Object
     * @param userId
     * @param customerAccount
     * @return list of UserSitesVO objects.
     */
    public List<UserSitesVO> getUserSiteListByUserIdCustomerAccount(String host, String userId, String customerAccount) {
        List<UserSitesVO> siteList = new ArrayList();
        HttpResponseVO responseVO;
        boolean serviceIssue = true;

        try {
            if ((responseVO = httpGet(USER_PREFERENCE_SERVICE_NAME, host, "/user/sites?userId=" + URLEncoder.encode(userId, "UTF-8") + "&customerAccount=" + URLEncoder.encode(customerAccount, "UTF-8"))) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        siteList = UserSaxJsonParser.getInstance().populateUserSitesFromJson(responseVO.getEntity());
                        Logger.getLogger(UserClient.class.getName()).log(Level.INFO, "Fetched User Sites");
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(UserClient.class.getName()).log(Level.WARNING, "Message from User Service: {0}", UserDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(UserClient.class.getName()).log(Level.WARNING, "Error Code from User Service : {0}", responseVO.getStatusCode());
                        Logger.getLogger(UserClient.class.getName()).log(Level.WARNING, "Error Message from User Service : {0}", responseVO.getEntity());
                        Logger.getLogger(UserClient.class.getName()).log(Level.WARNING, "errMsg : {0}", UserDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }

            } else {
                Logger.getLogger(UserClient.class.getName()).log(Level.WARNING, "Error - No host found for User Service.");
            }
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(UserClient.class.getName()).log(Level.WARNING, e.getMessage(), e);
        }
        return siteList;
    }

    /**
     * @param host, String Object
     * @param siteId
     * @param customerAccount
     * @return list of SiteUsersVO objects.
     */
    public List<SiteUsersVO> getSiteUsersListBySiteIdCustomerAccount(String host, String siteId, String customerAccount) {
        List<SiteUsersVO> siteList = new ArrayList();
        HttpResponseVO responseVO;
        boolean serviceIssue = true;

        try {
            if ((responseVO = httpGet(USER_PREFERENCE_SERVICE_NAME, host, "/user/sites?siteId=" + URLEncoder.encode(siteId, "UTF-8") + "&customerAccount=" + URLEncoder.encode(customerAccount, "UTF-8"))) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        siteList = UserSaxJsonParser.getInstance().populateSiteUsersFromJson(responseVO.getEntity());
                        Logger.getLogger(UserClient.class.getName()).log(Level.INFO, "Fetched User Sites");
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(UserClient.class.getName()).log(Level.WARNING, "Message from User Service: {0}", UserDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(UserClient.class.getName()).log(Level.WARNING, "Error Code from User Service : {0}", responseVO.getStatusCode());
                        Logger.getLogger(UserClient.class.getName()).log(Level.WARNING, "Error Message from User Service : {0}", responseVO.getEntity());
                        Logger.getLogger(UserClient.class.getName()).log(Level.WARNING, "errMsg : {0}", UserDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            } else {
                Logger.getLogger(UserClient.class.getName()).log(Level.WARNING, "Error - No host found for User Service.");
            }
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(UserClient.class.getName()).log(Level.WARNING, e.getMessage(), e);
        }
        return siteList;
    }

    /**
     * @param host, String Object
     * @param userId
     * @return list of UserSitesVO objects.
     */
    public List<UserSitesVO> getUserSiteListByUserId(String host, String userId) {
        List<UserSitesVO> siteList = new ArrayList();
        HttpResponseVO responseVO;
        boolean serviceIssue = true;

        try {
            if ((responseVO = httpGet(USER_PREFERENCE_SERVICE_NAME, host, "/user/sites?userId=" + URLEncoder.encode(userId, "UTF-8"))) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        siteList = UserSaxJsonParser.getInstance().populateUserSitesFromJson(responseVO.getEntity());
                        Logger.getLogger(UserClient.class.getName()).log(Level.INFO, "Fetched User Sites");
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(UserClient.class.getName()).log(Level.WARNING, "Message from User Service: {0}", UserDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(UserClient.class.getName()).log(Level.WARNING, "Error Code from User Service : {0}", responseVO.getStatusCode());
                        Logger.getLogger(UserClient.class.getName()).log(Level.WARNING, "Error Message from User Service : {0}", responseVO.getEntity());
                        Logger.getLogger(UserClient.class.getName()).log(Level.WARNING, "errMsg : {0}", UserDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            } else {
                Logger.getLogger(UserClient.class.getName()).log(Level.WARNING, "Error - No host found for User Service.");
            }
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(UserClient.class.getName()).log(Level.WARNING, e.getMessage(), e);
        }
        return siteList;
    }

    /**
     * @param host, String Object
     * @param userId
     * @return UserPreferencesVO object.
     */
    public UserPreferencesVO getUserPreferencesByPK(String host, String userId) {
        UserPreferencesVO userVO = null;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;

        try {
            if ((responseVO = httpGet(USER_PREFERENCE_SERVICE_NAME, host, "/user/preferences?userId=" + URLEncoder.encode(userId, "UTF-8"))) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        userVO = UserSaxJsonParser.getInstance().populateUserPreferenceFromJson(responseVO.getEntity());
                        Logger.getLogger(UserClient.class.getName()).log(Level.INFO, "Fetched UserVO");
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(UserClient.class.getName()).log(Level.WARNING, "Message from User Service: {0}", UserDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(UserClient.class.getName()).log(Level.WARNING, "Error Code from User Service : {0}", responseVO.getStatusCode());
                        Logger.getLogger(UserClient.class.getName()).log(Level.WARNING, "Error Message from User Service : {0}", responseVO.getEntity());
                        Logger.getLogger(UserClient.class.getName()).log(Level.WARNING, "errMsg : {0}", UserDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            } else {
                Logger.getLogger(UserClient.class.getName()).log(Level.WARNING, "Error - No host found for User Service.");
            }
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(UserClient.class.getName()).log(Level.WARNING, e.getMessage(), e);
        }
        return userVO;
    }

    /**
     * Create the given Site
     *
     * @param host, String Object
     * @param userPreferencesVO, UserPreferencesVO Object
     * @return boolean, true if success, else false
     */
    public boolean createUserPreferences(String host, UserPreferencesVO userPreferencesVO) {
        HttpResponseVO responseVO;

        if ((responseVO = httpPost(USER_PREFERENCE_SERVICE_NAME, host, "/user/preferences", UserDomJsonParser.getInstance().getJsonFromObject(userPreferencesVO))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                Logger.getLogger(UserClient.class.getName()).log(Level.INFO, "User Preferences successfully created.");
                return true;
            } else {
                Logger.getLogger(UserClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(UserClient.class.getName()).log(Level.WARNING, "Error Message from User Service: {0}", UserDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        return false;
    }

    /**
     * Delete the given Site
     *
     * @param host, String Object
     * @param userPreferencesVO, UserPreferencesVO Object
     * @return boolean, true if success, else false
     */
    public boolean deleteUserPreferences(String host, UserPreferencesVO userPreferencesVO) {
        UserManageBean userManageBean;
        HttpResponseVO responseVO;
        String errMsg;

        if ((responseVO = httpDelete(USER_PREFERENCE_SERVICE_NAME, host, "/user/preferences", UserDomJsonParser.getInstance().getJsonFromObject(userPreferencesVO))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
                FacesContext.getCurrentInstance().addMessage("growlMessages", new FacesMessage(userManageBean.getResourceBundleString("dialog_preferences_msg_11")));
                Logger.getLogger(UserClient.class.getName()).log(Level.INFO, "User Preferences successfully deleted.");
                return true;
            } else {
                Logger.getLogger(UserClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(UserClient.class.getName()).log(Level.WARNING, "Error Message from User Service: {0}", (errMsg = UserDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity())));
                FacesContext.getCurrentInstance().addMessage("growlMessages", new FacesMessage("Error Message from User Service: " + errMsg + "."));
            }
        }
        showErrorMsg();

        return false;
    }

    /**
     * Create the given user
     *
     * @param host, String Object
     * @param siteUsersVO, SiteUsersVO Object
     * @return boolean, true if success, else false
     */
    public boolean createUsers(String host, SiteUsersVO siteUsersVO) {
        UserManageBean userManageBean;
        HttpResponseVO responseVO;
        String errMsg;

        if ((responseVO = httpPost(USER_PREFERENCE_SERVICE_NAME, host, "/user/sites", UserDomJsonParser.getInstance().getJsonFromObject(siteUsersVO))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
                FacesContext.getCurrentInstance().addMessage("growlMessages", new FacesMessage(userManageBean.getResourceBundleString("growl_user_created")));
                Logger.getLogger(UserClient.class.getName()).log(Level.INFO, "User Sites / Site Users successfully created.");
                return true;
            } else {
                Logger.getLogger(UserClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(UserClient.class.getName()).log(Level.WARNING, "Error Message from User Service: {0}", (errMsg = UserDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity())));
                FacesContext.getCurrentInstance().addMessage("growlMessages", new FacesMessage("Error Message from User Service: " + errMsg + "."));
            }
        }
        showErrorMsg();

        return false;
    }

    /**
     * Update the given user
     *
     * @param host, String Object
     * @param siteUsersVO, SiteUsersVO Object
     * @return boolean, true if success, else false
     */
    public boolean updateUsers(String host, SiteUsersVO siteUsersVO) {
        UserManageBean userManageBean;
        HttpResponseVO responseVO;
        String errMsg;

        if ((responseVO = httpPut(USER_PREFERENCE_SERVICE_NAME, host, "/user/sites", UserDomJsonParser.getInstance().getJsonFromObject(siteUsersVO))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
                FacesContext.getCurrentInstance().addMessage("growlMessages", new FacesMessage(userManageBean.getResourceBundleString("growl_user_updated")));
                Logger.getLogger(UserClient.class.getName()).log(Level.INFO, "User Sites / Site Users successfully updated.");
                return true;
            } else {
                Logger.getLogger(UserClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(UserClient.class.getName()).log(Level.WARNING, "Error Message from User Service: {0}", (errMsg = UserDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity())));
                FacesContext.getCurrentInstance().addMessage("growlMessages", new FacesMessage("Error Message from User Service: " + errMsg + "."));
            }
        }
        showErrorMsg();

        return false;
    }

    /**
     * Delete the given user
     *
     * @param host, String Object
     * @param siteUsersVO, SiteUsersVO Object
     * @return boolean, true if success, else false
     */
    public boolean deleteUsers(String host, SiteUsersVO siteUsersVO) {
        UserManageBean userManageBean;
        HttpResponseVO responseVO;
        String errMsg;

        if ((responseVO = httpDelete(USER_PREFERENCE_SERVICE_NAME, host, "/user/sites", UserDomJsonParser.getInstance().getJsonFromObject(siteUsersVO))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
                FacesContext.getCurrentInstance().addMessage("growlMessages", new FacesMessage(userManageBean.getResourceBundleString("growl_user_deleted")));
                Logger.getLogger(UserClient.class.getName()).log(Level.INFO, "User Sites & Site Users successfully deleted.");
                return true;
            } else {
                Logger.getLogger(UserClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(UserClient.class.getName()).log(Level.WARNING, "Error Message from User Service: {0}", (errMsg = UserDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity())));
                FacesContext.getCurrentInstance().addMessage("growlMessages", new FacesMessage("Error Message from User Service: " + errMsg + "."));
            }
        }
        showErrorMsg();

        return false;
    }

    /**
     * Check if a PasswordToken exists for the given info
     *
     * @param host, String Object
     * @param passwordTokenVO, PasswordTokenVO Object
     * @return Boolean Object, true if found, false if not found, and null if
     * Error Occured
     */
    public Boolean checkForPasswordTokenByPK(String host, PasswordTokenVO passwordTokenVO) {
        StringBuilder endPoint;
        HttpResponseVO responseVO;

        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/user/passwordToken")
                    .append("?userId=").append(URLEncoder.encode(passwordTokenVO.getUserId(), "UTF-8"))
                    .append("&passwordToken=").append(passwordTokenVO.getPasswordToken());

            if ((responseVO = httpGet("/user/passwordToken", host, endPoint.toString())) != null && responseVO.isHostFound()) {
                if (responseVO.getStatusCode() == 200) {
                    return UserDomJsonParser.getInstance().getPasswordTokenCheckFromJson(responseVO.getEntity());
                }
            }
        } catch (Exception e) {
            Logger.getLogger(UserClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
        showErrorMsg();
        return null;
    }

    /**
     * Create the given PasswordToken
     *
     * @param host, String Object
     * @param passwordTokenVO, PasswordTokenVO Object
     * @return boolean, true if success, else false
     */
    public boolean createPasswordToken(String host, PasswordTokenVO passwordTokenVO) {
        HttpResponseVO responseVO;

        if ((responseVO = httpPost(USER_PREFERENCE_SERVICE_NAME, host, "/user/passwordToken", UserDomJsonParser.getInstance().getJsonFromObject(passwordTokenVO))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                return true;
            } else {
                Logger.getLogger(UserClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(UserClient.class.getName()).log(Level.WARNING, "Error Message from User Service: {0}", UserDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        showErrorMsg();
        return false;
    }

}
