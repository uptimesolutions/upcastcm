/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.http.client;

import com.uptime.client.http.client.Client;
import com.uptime.client.utils.vo.HttpResponseVO;
import com.uptime.upcastcm.bean.UserManageBean;
import com.uptime.upcastcm.http.utils.json.dom.SubscriptionsDomJsonParser;
import com.uptime.upcastcm.http.utils.json.sax.SubscriptionsSaxJsonParser;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import static com.uptime.upcastcm.utils.helperclass.GenericMessage.showErrorMsg;
import com.uptime.upcastcm.utils.vo.SubscriptionsVO;
import java.net.URLEncoder;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 *
 * @author madhavi
 */
public class SubscriptionsClient extends Client {
    private static SubscriptionsClient instance = null;
    
    /**
     * Private Singleton class
     */
    private SubscriptionsClient() {
    }

    /**
     * Returns an instance of the class
     *
     * @return SiteClient object
     */
    public static SubscriptionsClient getInstance() {
        if (instance == null) {
            instance = new SubscriptionsClient();
        }
        return instance;
    }

    /**
     * Get Subscriptions Info based on the given info
     *
     * @param host String Object
     * @param customer, String Object
     * @param siteId, String Object
     * @return List Object of SubscriptionsVO Objects
     */
    public List<SubscriptionsVO> getSubscriptionsByCustomerSite(String host, String customer, String siteId) {
        List<SubscriptionsVO> list = null;
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;
        
        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/config/subscriptions")
                    .append("?customerAccount=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&siteId=").append(URLEncoder.encode(siteId, "UTF-8"));
            
            if ((responseVO = httpGet(CONFIGURATION_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        list = SubscriptionsSaxJsonParser.getInstance().populateSubscriptionsFromJson(responseVO.getEntity());
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(SubscriptionsClient.class.getName()).log(Level.WARNING, "Message from Config Service: {0}", SubscriptionsDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(SubscriptionsClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(SubscriptionsClient.class.getName()).log(Level.WARNING, "Error Message from Config Service: {0}", SubscriptionsDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(SubscriptionsClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            return list;
        }
        return list;
    }

    /**
     * Get Subscriptions Info based on the given info
     *
     * @param host String Object
     * @param customer, String Object
     * @param siteId, String Object
     * @param subscriptionType, String Object
     * @return List Object of SubscriptionsVO Objects
     */
    public List<SubscriptionsVO> getSubscriptionsByPK(String host, String customer, String siteId, String subscriptionType) {
        List<SubscriptionsVO> list = null;
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;
        
        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/config/subscriptions")
                    .append("?customer=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&siteId=").append(URLEncoder.encode(siteId, "UTF-8"))
                    .append("&subscriptionType=").append(URLEncoder.encode(subscriptionType, "UTF-8"));
            
            if ((responseVO = httpGet(CONFIGURATION_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        list = SubscriptionsSaxJsonParser.getInstance().populateSubscriptionsFromJson(responseVO.getEntity());
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(SubscriptionsClient.class.getName()).log(Level.WARNING, "Message from Config Service: {0}", SubscriptionsDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(SubscriptionsClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(SubscriptionsClient.class.getName()).log(Level.WARNING, "Error Message from Config Service: {0}", SubscriptionsDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(SubscriptionsClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            return list;
        }
        return list;
    }

    /**
     * Create the given Subscriptions
     *
     * @param host, String Object
     * @param subscriptionsVO, SubscriptionsVO Object
     * @return boolean, true if success, else false
     */
    public boolean createSubscriptions(String host, SubscriptionsVO subscriptionsVO) {
        UserManageBean userManageBean;
        HttpResponseVO responseVO;
        
        Logger.getLogger(SubscriptionsClient.class.getName()).log(Level.INFO, "SubscriptionsDomJsonParser JSON**************: {0}", SubscriptionsDomJsonParser.getInstance().getJsonFromSubscriptions(subscriptionsVO));
        if ((responseVO = httpPost(CONFIGURATION_SERVICE_NAME, host, "/config/subscriptions", SubscriptionsDomJsonParser.getInstance().getJsonFromSubscriptions(subscriptionsVO))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
                FacesContext.getCurrentInstance().addMessage("nonAutoGrowlMessages", new FacesMessage(userManageBean.getResourceBundleString("growl_create_subscription_success_message")));
                Logger.getLogger(SubscriptionsClient.class.getName()).log(Level.INFO, "Subscriptions created successfully.");
                return true;
            } else {
                Logger.getLogger(SubscriptionsClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(SubscriptionsClient.class.getName()).log(Level.WARNING, "Error Message from Config Service: {0}", SubscriptionsDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        showErrorMsg();

        return false;
    }

    /**
     * Update the given Subscriptions
     *
     * @param host, String Object
     * @param subscriptionsVO, SubscriptionsVO Object
     * @return boolean, true if success, else false
     */
    public boolean updateSubscriptions(String host, SubscriptionsVO subscriptionsVO) {
        UserManageBean userManageBean;
        HttpResponseVO responseVO;
        
        if ((responseVO = httpPut(CONFIGURATION_SERVICE_NAME, host, "/config/subscriptions", SubscriptionsDomJsonParser.getInstance().getJsonFromSubscriptions(subscriptionsVO))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
                FacesContext.getCurrentInstance().addMessage("nonAutoGrowlMessages", new FacesMessage(userManageBean.getResourceBundleString("growl_update_subscription_success_message")));
                Logger.getLogger(SubscriptionsClient.class.getName()).log(Level.INFO, "Subscriptions updated successfully.");
                return true;
            } else {
                Logger.getLogger(SubscriptionsClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(SubscriptionsClient.class.getName()).log(Level.WARNING, "Error Message from Config Service: {0}", SubscriptionsDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        showErrorMsg();

        return false;
    }

    /**
     * Delete the given Subscriptions
     *
     * @param host, String Object
     * @param subscriptionsVO, SubscriptionsVO Object
     * @return boolean, true if success, else false
     */
    public boolean deleteSubscriptions(String host, SubscriptionsVO subscriptionsVO) {
        UserManageBean userManageBean;
        HttpResponseVO responseVO;
        
        subscriptionsVO.setRequestType("Delete");
        if ((responseVO = httpDelete(CONFIGURATION_SERVICE_NAME, host, "/config/subscriptions", SubscriptionsDomJsonParser.getInstance().getJsonFromSubscriptions(subscriptionsVO))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
                FacesContext.getCurrentInstance().addMessage("nonAutoGrowlMessages", new FacesMessage(userManageBean.getResourceBundleString("growl_delete_subscription_success_message")));
                Logger.getLogger(SubscriptionsClient.class.getName()).log(Level.INFO, "Subscriptions deleted successfully.");
                return true;
            } else {
                Logger.getLogger(SubscriptionsClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(SubscriptionsClient.class.getName()).log(Level.WARNING, "Error Message from Config Service: {0}", SubscriptionsDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        showErrorMsg();

        return false;
    }

    /**
     * Cancel all the Subscriptions
     *
     * @param host, String Object
     * @param subscriptionsVO, SubscriptionsVO Object
     * @return boolean, true if success, else false
     */
    public boolean cancelSubscriptions(String host, SubscriptionsVO subscriptionsVO) {
        UserManageBean userManageBean;
        HttpResponseVO responseVO;
        
        subscriptionsVO.setRequestType("Cancel");
        if ((responseVO = httpDelete(CONFIGURATION_SERVICE_NAME, host, "/config/subscriptions", SubscriptionsDomJsonParser.getInstance().getJsonFromSubscriptions(subscriptionsVO))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
                FacesContext.getCurrentInstance().addMessage("nonAutoGrowlMessages", new FacesMessage(userManageBean.getResourceBundleString("growl_cancel_subscription_success_message")));
                Logger.getLogger(SubscriptionsClient.class.getName()).log(Level.INFO, "Subscriptions deleted successfully.");
                return true;
            } else {
                Logger.getLogger(SubscriptionsClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(SubscriptionsClient.class.getName()).log(Level.WARNING, "Error Message from Config Service: {0}", SubscriptionsDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        showErrorMsg();

        return false;
    }
}
