/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.http.client;

import com.uptime.client.http.client.Client;
import com.uptime.client.utils.vo.HttpResponseVO;
import com.uptime.upcastcm.http.utils.json.dom.AlarmsDomJsonParser;
import com.uptime.upcastcm.http.utils.json.sax.TrendDataSaxJsonParser;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import com.uptime.upcastcm.utils.vo.TrendValuesVO;
import com.uptime.upcastcm.utils.vo.WaveDataVO;
import java.net.URLEncoder;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author gsingh
 */
public class TrendDataClient extends Client {
    private static TrendDataClient instance = null;

    /**
     * Private Singleton class
     */
    private TrendDataClient() {
    }

    /**
     * Returns an instance of the class
     *
     * @return TrendDataClient object
     */
    public static TrendDataClient getInstance() {
        if (instance == null) {
            instance = new TrendDataClient();
        }
        return instance;
    }

    /**
     * Get Alarms Info based on the given info
     *
     * @param host String Object
     * @param customer, String Object
     * @param siteId
     * @param areaId
     * @param assetId
     * @param pointLocationId
     * @param pointId
     * @param apSetId
     * @param paramId
     * @return List Object of TrendValuesVO Objects
     */
    public List<TrendValuesVO> getTrendsByCustomerSiteAreaAssetPointLocationPointApSetParam(String host, String customer, UUID siteId, UUID areaId, UUID assetId, UUID pointLocationId, UUID pointId, UUID apSetId, String paramId) {
        List<TrendValuesVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;

        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/trend")
                    .append("?customerAccount=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&siteId=").append(siteId)
                    .append("&areaId=").append(areaId)
                    .append("&assetId=").append(assetId)
                    .append("&pointLocationId=").append(pointLocationId)
                    .append("&pointId=").append(pointId)
                    .append("&apSetId=").append(apSetId)
                    .append("&paramId=").append(URLEncoder.encode(paramId, "UTF-8"))
                    .append("&limit=730");
            
            if ((responseVO = httpGet(WAVEFORM_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                if (responseVO.getStatusCode() == 200) {
                    list = TrendDataSaxJsonParser.getInstance().populateTrendDataFromJson(responseVO.getEntity());
                } else {
                    Logger.getLogger(TrendDataClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                    Logger.getLogger(TrendDataClient.class.getName()).log(Level.WARNING, "Error Message from WaveformData Service: {0}", AlarmsDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
        } catch (Exception e) {
            Logger.getLogger(TrendDataClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }

    /**
     * Get Alarms Info based on the given info
     *
     * @param host String Object
     * @param customer, String Object
     * @param siteId
     * @param areaId
     * @param assetId
     * @param pointLocationId
     * @param pointId
     * @param apSetId
     * @param paramId
     * @param startDate
     * @param endDate
     * @return List Object of TrendValuesVO Objects
     */
    public List<TrendValuesVO> getTrendsByCustomerSiteAreaAssetPointLocationPointApSetParamDateRange(String host, String customer, UUID siteId, UUID areaId, UUID assetId, UUID pointLocationId, UUID pointId, UUID apSetId, String paramId, String startDate, String endDate) {
        List<TrendValuesVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;

        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/trend")
                    .append("?customerAccount=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&siteId=").append(siteId)
                    .append("&areaId=").append(areaId)
                    .append("&assetId=").append(assetId)
                    .append("&pointLocationId=").append(pointLocationId)
                    .append("&pointId=").append(pointId)
                    .append("&apSetId=").append(apSetId)
                    .append("&paramId=").append(URLEncoder.encode(paramId, "UTF-8"))
                    .append("&startDate=").append(startDate)
                    .append("&endDate=").append(endDate);
            
            if ((responseVO = httpGet(WAVEFORM_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                if (responseVO.getStatusCode() == 200) {
                    list = TrendDataSaxJsonParser.getInstance().populateTrendDataFromJson(responseVO.getEntity());
                } else {
                    Logger.getLogger(TrendDataClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                    Logger.getLogger(TrendDataClient.class.getName()).log(Level.WARNING, "Error Message from WaveformData Service: {0}", AlarmsDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
        } catch (Exception e) {
            Logger.getLogger(TrendDataClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }

    /**
     * Get Alarms Info based on the given info
     *
     * @param host String Object
     * @param customer, String Object
     * @param siteId
     * @param areaId
     * @param assetId
     * @param pointLocationId
     * @param pointId
     * @param apSetId
     * @param startDate
     * @param endDate
     * @return List Object of TrendValuesVO Objects
     */
    public List<TrendValuesVO> getTrendsByCustomerSiteAreaAssetPointLocationPointApSetDateRange(String host, String customer, UUID siteId, UUID areaId, UUID assetId, UUID pointLocationId, UUID pointId, UUID apSetId, String startDate, String endDate) {
        List<TrendValuesVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;

        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/trend")
                    .append("?customerAccount=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&siteId=").append(siteId)
                    .append("&areaId=").append(areaId)
                    .append("&assetId=").append(assetId)
                    .append("&pointLocationId=").append(pointLocationId)
                    .append("&pointId=").append(pointId)
                    .append("&apSetId=").append(apSetId)
                    .append("&startDate=").append(startDate)
                    .append("&endDate=").append(endDate);
            
            if ((responseVO = httpGet(WAVEFORM_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                if (responseVO.getStatusCode() == 200) {
                    list = TrendDataSaxJsonParser.getInstance().populateTrendDataFromJson(responseVO.getEntity());
                } else {
                    Logger.getLogger(TrendDataClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                    Logger.getLogger(TrendDataClient.class.getName()).log(Level.WARNING, "Error Message from WaveformData Service: {0}", AlarmsDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
        } catch (Exception e) {
            Logger.getLogger(TrendDataClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }

    /**
     * Get Alarms Info based on the given info
     *
     * @param host String Object
     * @param customer, String Object
     * @param siteId
     * @param areaId
     * @param assetId
     * @param pointLocationId
     * @param pointId
     * @param apSetId
     * @return List Object of TrendValuesVO Objects
     */
    public List<TrendValuesVO> getTrendsByCustomerSiteAreaAssetPointLocationPointApSet(String host, String customer, UUID siteId, UUID areaId, UUID assetId, UUID pointLocationId, UUID pointId, UUID apSetId) {
        List<TrendValuesVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;

        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/trend")
                    .append("?customerAccount=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&siteId=").append(siteId)
                    .append("&areaId=").append(areaId)
                    .append("&assetId=").append(assetId)
                    .append("&pointLocationId=").append(pointLocationId)
                    .append("&pointId=").append(pointId)
                    .append("&apSetId=").append(apSetId)
                    .append("&limit=730");
            
            if ((responseVO = httpGet(WAVEFORM_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                if (responseVO.getStatusCode() == 200) {
                    list = TrendDataSaxJsonParser.getInstance().populateTrendDataFromJson(responseVO.getEntity());
                } else {
                    Logger.getLogger(TrendDataClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                    Logger.getLogger(TrendDataClient.class.getName()).log(Level.WARNING, "Error Message from WaveformData Service: {0}", AlarmsDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
        } catch (Exception e) {
            Logger.getLogger(TrendDataClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }

    /**
     *
     * @param host
     * @param customer
     * @param siteId
     * @param areaId
     * @param assetId
     * @param pointLocationId
     * @param pointId
     * @param sampleYear
     * @param sampleMonth
     * @param sampleTime
     * @return
     */
    public WaveDataVO getByCustomerSiteAreaAssetPointLocationPointSampleYearSampleMonthSampleTime(String host, String customer, UUID siteId, UUID areaId, UUID assetId, UUID pointLocationId, UUID pointId, short sampleYear, byte sampleMonth, Instant sampleTime) {
        WaveDataVO waveDataVO = null;
        StringBuilder endPoint;
        HttpResponseVO responseVO;

        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/wavedata")
                    .append("?customerAccount=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&siteId=").append(siteId)
                    .append("&areaId=").append(areaId)
                    .append("&assetId=").append(assetId)
                    .append("&pointLocationId=").append(pointLocationId)
                    .append("&pointId=").append(pointId)
                    .append("&sampleYear=").append(sampleYear)
                    .append("&sampleMonth=").append(sampleMonth)
                    .append("&sampleTime=").append(sampleTime);
            
            if ((responseVO = httpGet(WAVEFORM_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                if (responseVO.getStatusCode() == 200) {
                    waveDataVO = TrendDataSaxJsonParser.getInstance().populateWaveDataFromJson(responseVO.getEntity());
                } else {
                    Logger.getLogger(TrendDataClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                    Logger.getLogger(TrendDataClient.class.getName()).log(Level.WARNING, "Error Message from WaveformData Service: {0}", AlarmsDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
        } catch (Exception e) {
            Logger.getLogger(TrendDataClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
        return waveDataVO;
    }

}
