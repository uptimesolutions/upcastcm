/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.http.client;

import com.uptime.client.http.client.Client;
import com.uptime.client.utils.vo.HttpResponseVO;
import com.uptime.upcastcm.bean.UserManageBean;
import com.uptime.upcastcm.http.utils.json.dom.AreaDomJsonParser;
import com.uptime.upcastcm.http.utils.json.dom.PointLocationDomJsonParser;
import com.uptime.upcastcm.http.utils.json.sax.PointLocationSaxJsonParser;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import static com.uptime.upcastcm.utils.helperclass.GenericMessage.showErrorMsg;
import com.uptime.upcastcm.utils.vo.PointLocationVO;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 *
 * @author twilcox
 */
public class PointLocationClient extends Client {
    private static PointLocationClient instance = null;

    /**
     * Private Singleton class
     */
    private PointLocationClient() {
    }

    /**
     * Returns an instance of the class
     *
     * @return PointLocationClient object
     */
    public static PointLocationClient getInstance() {
        if (instance == null) {
            instance = new PointLocationClient();
        }
        return instance;
    }

    /**
     * Get Point Location Info based on the given info
     *
     * @param host String Object
     * @param customer String Object
     * @param siteId UUID Object
     * @param areaId UUID Object
     * @param assetId UUID Object
     * @return List Object of PointLocationVO Objects
     */
    public List<PointLocationVO> getPointLocationsByCustomerSiteAreaAsset(String host, String customer, UUID siteId, UUID areaId, UUID assetId) {
        List<PointLocationVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;

        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/config/pointLocation")
                    .append("?customer=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&siteId=").append(siteId)
                    .append("&areaId=").append(areaId)
                    .append("&assetId=").append(assetId);
            
            if ((responseVO = httpGet(CONFIGURATION_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        list = PointLocationSaxJsonParser.getInstance().populatePointLocationsFromJson(responseVO.getEntity());
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(PointLocationClient.class.getName()).log(Level.WARNING, "Message from Config Service: {0}", PointLocationDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(PointLocationClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(PointLocationClient.class.getName()).log(Level.WARNING, "Error Message from Config Service: {0}", PointLocationDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(PointClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }

    /**
     * Get Point Location Info based on the given info
     *
     * @param host String Object
     * @param customer String Object
     * @param siteId UUID Object
     * @param areaId UUID Object
     * @param assetId UUID Object
     * @param pointLocationId UUID Object
     * @return List Object of PointLocationVO Objects
     */
    public List<PointLocationVO> getPointLocationsByPK(String host, String customer, UUID siteId, UUID areaId, UUID assetId, UUID pointLocationId) {
        List<PointLocationVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;

        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/config/pointLocation")
                    .append("?customer=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&siteId=").append(siteId)
                    .append("&areaId=").append(areaId)
                    .append("&assetId=").append(assetId)
                    .append("&pointLocationId=").append(pointLocationId);
            
            if ((responseVO = httpGet(CONFIGURATION_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        list = PointLocationSaxJsonParser.getInstance().populatePointLocationsFromJson(responseVO.getEntity());
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(PointLocationClient.class.getName()).log(Level.WARNING, "Message from Config Service: {0}", PointLocationDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(PointLocationClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(PointLocationClient.class.getName()).log(Level.WARNING, "Error Message from Config Service: {0}", PointLocationDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(PointClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }

    public boolean createPointLocation(String host, PointLocationVO pointLocationVO) {
        UserManageBean userManageBean;
        HttpResponseVO responseVO;
        
        if ((responseVO = httpPost(CONFIGURATION_SERVICE_NAME, host, "/config/pointLocation", PointLocationDomJsonParser.getInstance().getJsonFromPointLocation(pointLocationVO))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
                FacesContext.getCurrentInstance().addMessage("nonAutoGrowlMessages", new FacesMessage(userManageBean.getResourceBundleString("growl_create_point_location_2")));
                Logger.getLogger(PointLocationClient.class.getName()).log(Level.INFO, "PointLocation successfully created.");
                return true;
            } else {
                Logger.getLogger(PointLocationClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(PointLocationClient.class.getName()).log(Level.WARNING, "Error Message from Config Service: {0}", AreaDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        showErrorMsg();
        
        return false;
    }

    public boolean updatePointLocation(String host, PointLocationVO pointLocationVO) {
        UserManageBean userManageBean;
        HttpResponseVO responseVO;
        
        if ((responseVO = httpPut(CONFIGURATION_SERVICE_NAME, host, "/config/pointLocation", PointLocationDomJsonParser.getInstance().getJsonFromPointLocation(pointLocationVO))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
                FacesContext.getCurrentInstance().addMessage("nonAutoGrowlMessages", new FacesMessage(userManageBean.getResourceBundleString("growl_update_point_location")));
                Logger.getLogger(PointLocationClient.class.getName()).log(Level.INFO, "PointLocation successfully updated.");
                return true;
            } else {
                Logger.getLogger(PointLocationClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(PointLocationClient.class.getName()).log(Level.WARNING, "Error Message from Config Service: {0}", AreaDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        showErrorMsg();
        
        return false;
    }
    
    public boolean deleteAllPointsPointLocation(String host, PointLocationVO pointLocationVO) {
        UserManageBean userManageBean;
        HttpResponseVO responseVO;
        
        if ((responseVO = httpDelete(CONFIGURATION_SERVICE_NAME, host, "/config/pointLocation", PointLocationDomJsonParser.getInstance().getJsonFromObject(pointLocationVO, "deleteAllPoints"))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
                FacesContext.getCurrentInstance().addMessage("nonAutoGrowlMessages", new FacesMessage(userManageBean.getResourceBundleString("growl_delete_point")));
                Logger.getLogger(PointClient.class.getName()).log(Level.INFO, "Point(s) deleted successfully.");
                return true;
            } else {
                Logger.getLogger(PointClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(PointClient.class.getName()).log(Level.WARNING, "Error Message from Config Service: {0}", PointLocationDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        showErrorMsg();
        
        return false;
    }

    /**
     *
     * @param host
     * @param customer
     * @param siteId
     * @param areaId
     * @return
     */
    public List<PointLocationVO> getPointLocationsByCustomerSiteArea(String host, String customer, UUID siteId, UUID areaId) {
        List<PointLocationVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;

        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/config/pointLocation")
                    .append("?customer=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&siteId=").append(siteId)
                    .append("&areaId=").append(areaId);
            
            if ((responseVO = httpGet(CONFIGURATION_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        list = PointLocationSaxJsonParser.getInstance().populatePointLocationsFromJson(responseVO.getEntity());
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(PointLocationClient.class.getName()).log(Level.WARNING, "Message from Config Service: {0}", PointLocationDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(PointLocationClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(PointLocationClient.class.getName()).log(Level.WARNING, "Error Message from Config Service: {0}", PointLocationDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(PointClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }
}
