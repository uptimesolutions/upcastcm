/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.http.client;

import com.uptime.client.http.client.Client;
import com.uptime.client.utils.vo.HttpResponseVO;
import com.uptime.upcastcm.bean.UserManageBean;
import com.uptime.upcastcm.dao.PrestoDAO;
import com.uptime.upcastcm.http.utils.json.dom.ManageDeviceDomJsonParser;
import com.uptime.upcastcm.http.utils.json.dom.SiteDomJsonParser;
import com.uptime.upcastcm.http.utils.json.sax.DeviceLocationSaxJsonParser;
import com.uptime.upcastcm.http.utils.json.sax.ManageDeviceSaxJsonParser;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import static com.uptime.upcastcm.utils.helperclass.GenericMessage.showErrorMsg;
import com.uptime.upcastcm.utils.vo.AssetVO;
import com.uptime.upcastcm.utils.vo.ChannelVO;
import com.uptime.upcastcm.utils.vo.DeviceStatusVO;
import com.uptime.upcastcm.utils.vo.DeviceVO;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 *
 * @author kpati
 */
public class ManageDeviceClient  extends Client {
    private static ManageDeviceClient instance = null;
    private PrestoDAO prestoDAO;

    private ManageDeviceClient() {
        try {
            prestoDAO = new PrestoDAO();
        } catch (Exception e) {
            Logger.getLogger(ManageDeviceClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            prestoDAO = null;
        }
    }
    
     /**
     * Returns an instance of the class
     *
     * @return PointToHwUnitClient object
     */
    public static ManageDeviceClient getInstance() {
        if (instance == null) {
            instance = new ManageDeviceClient();
        }
        return instance;
    }
    
    /**
     * Get HardwareUnit Info based on the given info
     *
     * @param host String Object
     * @param customer, String Object
     * @param siteId, UUID Object
     * @param deviceId, String Object
     * @param verbose, boolean, true if more informed json is needed, else false;
     * @return List Object of ChannelVO Objects
     */
    public List<ChannelVO> getDeviceManagementRowsByCustomerSiteIdDevice(String host, String customer, UUID siteId, String deviceId, boolean verbose) {
        List<ChannelVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;

        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/config/manageDevice")
                    .append("?customer=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&siteId=").append(siteId)
                    .append("&deviceId=").append(URLEncoder.encode(deviceId, "UTF-8"))
                    .append("&verbose=").append(verbose);
            
            if ((responseVO = httpGet(CONFIGURATION_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        if(verbose) {
                            list = ManageDeviceDomJsonParser.getInstance().populateManageDeviceRowsFromVerboseJson(responseVO.getEntity());
                        } else {
                            list = ManageDeviceSaxJsonParser.getInstance().populateManageDeviceRowsFromJson(responseVO.getEntity());
                        }
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(ManageDeviceClient.class.getName()).log(Level.WARNING, "Message from Config Service: {0}", ManageDeviceDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(ManageDeviceClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(ManageDeviceClient.class.getName()).log(Level.WARNING, "Error Message from Config Service: {0}", ManageDeviceDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(ManageDeviceClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }
    
    /**
     * Create a new Point by inserting into point table
     *
     * @param host String Object
     * @param deviceVO PointVO Object
     * @return boolean
     */
    public boolean installDevice(String host, DeviceVO deviceVO) {
        UserManageBean userManageBean;
        HttpResponseVO responseVO;
        
        if ((responseVO = httpPost(CONFIGURATION_SERVICE_NAME, host, "/config/manageDevice", ManageDeviceDomJsonParser.getInstance().getJsonFromDeviceVO(deviceVO))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
                FacesContext.getCurrentInstance().addMessage("nonAutoGrowlMessages", new FacesMessage(userManageBean.getResourceBundleString("growl_device_installed_success_message")));
                Logger.getLogger(ManageDeviceClient.class.getName()).log(Level.INFO, "Device installed successfully");
                return true;
            } else {
                Logger.getLogger(ManageDeviceClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(ManageDeviceClient.class.getName()).log(Level.WARNING, "Error Message from Config Service: {0}", ManageDeviceDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        showErrorMsg();

        return false;
    }
    
    public boolean replaceDevice(String host, DeviceVO deviceVO) {
        UserManageBean userManageBean;
        HttpResponseVO responseVO;
        
        if ((responseVO = httpPut(CONFIGURATION_SERVICE_NAME, host, "/config/manageDevice", ManageDeviceDomJsonParser.getInstance().getJsonFromObject(deviceVO, "replacedevice"))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
                FacesContext.getCurrentInstance().addMessage("nonAutoGrowlMessages", new FacesMessage(userManageBean.getResourceBundleString("growl_map_existing_success_message")));
                Logger.getLogger(ManageDeviceClient.class.getName()).log(Level.INFO, "Device replaced successfully.");
                return true;
            } else {
                Logger.getLogger(ManageDeviceClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(ManageDeviceClient.class.getName()).log(Level.WARNING, "Error Message from Config Service: {0}", SiteDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        showErrorMsg();

        return false;
    }
    
    /*
    * Enable a device by enabling all the points associated with the device
    * and ensuring that there is a corresponding entry in the site_basestation_device
    * and site_device_basestation tables
    *
    * @param host, String Object
    * @param deviceVO, DeviceVO Object
    * @return boolean    
    */
    public boolean enableDevice(String host, DeviceVO deviceVO) {
        UserManageBean userManageBean;
        HttpResponseVO responseVO;
        
        if ((responseVO = httpPut(CONFIGURATION_SERVICE_NAME, host, "/config/manageDevice", ManageDeviceDomJsonParser.getInstance().getJsonFromObject(deviceVO, "enabledevice"))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
                FacesContext.getCurrentInstance().addMessage("nonAutoGrowlMessages", new FacesMessage(userManageBean.getResourceBundleString("growl_device_enabled_success_message")));
                Logger.getLogger(ManageDeviceClient.class.getName()).log(Level.INFO, "Device enabled successfully.");
                return true;
            } else {
                Logger.getLogger(ManageDeviceClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(ManageDeviceClient.class.getName()).log(Level.WARNING, "Error Message from Config Service: {0}", SiteDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        showErrorMsg();

        return false;
    }
    
     /*
    * Disable a device by disabling all the points associated with the device
    * and ensuring that there is no corresponding entry in the site_basestation_device
    * or site_device_basestation tables
    *
    * @param host, String Object
    * @param deviceVO, DeviceVO Object
    * @return boolean
    */
    public boolean disableDevice(String host, DeviceVO deviceVO) {
        UserManageBean userManageBean;
        HttpResponseVO responseVO;
        
        if ((responseVO = httpPut(CONFIGURATION_SERVICE_NAME, host, "/config/manageDevice", ManageDeviceDomJsonParser.getInstance().getJsonFromObject(deviceVO, "disabledevice"))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
                FacesContext.getCurrentInstance().addMessage("nonAutoGrowlMessages", new FacesMessage(userManageBean.getResourceBundleString("growl_device_disabled_success_message")));
                Logger.getLogger(ManageDeviceClient.class.getName()).log(Level.INFO, "Device Disabled successfully.");
                return true;
            } else {
                Logger.getLogger(ManageDeviceClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(ManageDeviceClient.class.getName()).log(Level.WARNING, "Error Message from Config Service: {0}", SiteDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        showErrorMsg();

        return false;
    }
     
    /*
    * remove a device by removing the device serial number from the point_location table
    * and all the corresponding entries in the site_basestation_device,
    * site_device_basestation, hwunit_to_point and point_to_hwunit tables
    *
    * @param host, String Object
    * @param deviceVO, DeviceVO Object
    * @return boolean
    */
    public boolean removeDevice(String host, DeviceVO deviceVO) {
        UserManageBean userManageBean;
        HttpResponseVO responseVO;
        
        if ((responseVO = httpDelete(CONFIGURATION_SERVICE_NAME, host, "/config/manageDevice", ManageDeviceDomJsonParser.getInstance().getJsonFromDeviceVO(deviceVO))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
                FacesContext.getCurrentInstance().addMessage("nonAutoGrowlMessages", new FacesMessage(userManageBean.getResourceBundleString("growl_mist_device_removed_success_message")));
                Logger.getLogger(ManageDeviceClient.class.getName()).log(Level.INFO, "Device Disabled successfully.");
                return true;
            } else {
                Logger.getLogger(ManageDeviceClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(ManageDeviceClient.class.getName()).log(Level.WARNING, "Error Message from Config Service: {0}", SiteDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        showErrorMsg();

        return false;
    }

    /**
     * Create a new TS1 device by inserting into multiple tables
     *
     * @param host String Object
     * @param assetVOList, List of AssetVO Objects
     * @return boolean
     */
    public boolean createTS1Device(String host, List<AssetVO> assetVOList) {
        UserManageBean userManageBean;
        HttpResponseVO responseVO;
        StringBuilder endPoint;
        
        endPoint = new StringBuilder();
        endPoint
                .append("/config/manageDevice")
                .append("?hwDisabled=false")
                .append("&modified=false");
        
        if ((responseVO = httpPost(CONFIGURATION_SERVICE_NAME, host, endPoint.toString(), ManageDeviceDomJsonParser.getInstance().getJsonFromAssetVOList(assetVOList))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
                FacesContext.getCurrentInstance().addMessage("nonAutoGrowlMessages", new FacesMessage(userManageBean.getResourceBundleString("growl_ts1_device_installed_success_message")));
                Logger.getLogger(ManageDeviceClient.class.getName()).log(Level.INFO, "TS1 Device created successfully.");
                return true;
            } else {
                Logger.getLogger(ManageDeviceClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(ManageDeviceClient.class.getName()).log(Level.WARNING, "Error Message from Config Service: {0}", ManageDeviceDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        showErrorMsg();

        return false;
    }

    /**
     * Create a new TS1 device by inserting into multiple tables
     *
     * @param host String Object
     * @param assetVOList, List of AssetVO Objects
     * @param hwDisabled, boolean true if device is disabled, else false
     * @return boolean
     */
    public boolean modifyTS1Device(String host, List<AssetVO> assetVOList, boolean hwDisabled) {
        UserManageBean userManageBean;
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        
        endPoint = new StringBuilder();
        endPoint
                .append("/config/manageDevice")
                .append("?hwDisabled=").append(hwDisabled)
                .append("&modified=true");

        if ((responseVO = httpPost(CONFIGURATION_SERVICE_NAME, host, endPoint.toString(), ManageDeviceDomJsonParser.getInstance().getJsonFromAssetVOList(assetVOList))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
                FacesContext.getCurrentInstance().addMessage("nonAutoGrowlMessages", new FacesMessage(userManageBean.getResourceBundleString("growl_ts1_device_modified_success_message")));
                Logger.getLogger(ManageDeviceClient.class.getName()).log(Level.INFO, "TS1 Device modified successfully.");
                return true;
            } else {
                Logger.getLogger(ManageDeviceClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(ManageDeviceClient.class.getName()).log(Level.WARNING, "Error Message from Config Service: {0}", ManageDeviceDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        showErrorMsg();

        return false;
    }

    /**
     * Return all devices for a given customer and site name by Presto
     * Note: The site name is being used instead of the UUID do to the PrestoDAO provided
     * 
     * @param customerId, String Object
     * @param siteName, String Object
     * @return List Object of DeviceVO Objects
     */
    public List<DeviceVO> getDeviceLocationsByCustomerSiteName(String customerId, String siteName) {
        String deviceMappingString;
        
        try {
            if (customerId != null && !customerId.isEmpty() && siteName != null && !siteName.isEmpty() && (deviceMappingString = prestoDAO.getDeviceMapping(customerId, siteName)) != null && !deviceMappingString.isEmpty()) {
                return DeviceLocationSaxJsonParser.getInstance().populateDeviceFromJson(deviceMappingString);
            }
        } catch (Exception e) {
            Logger.getLogger(ManageDeviceClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
        return new ArrayList();
    }
    
    
     /**
     * Get List of DeviceStatusVO Objects based on the given info
     *
     * @param host String Object
     * @param customer, String Object
     * @param siteName, String Object
     * @return List of DeviceStatusVO Objects
     */
    public List<DeviceStatusVO> getDeviceStatusByCustomerSite(String host, String customer, String siteName) {
        List<DeviceStatusVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        String errMsg;

        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/device-status")
                    .append("?customer_acct=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&site_name=").append(URLEncoder.encode(siteName, "UTF-8"));
            if ((responseVO = httpGet(CONFIGURATION_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        list = ManageDeviceSaxJsonParser.getInstance().populateDeviceStatusFromJson(responseVO.getEntity());
                        break;
                    case 404:
                        Logger.getLogger(ManageDeviceClient.class.getName()).log(Level.WARNING, "Message from Config Service: {0}", ManageDeviceDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(ManageDeviceClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(ManageDeviceClient.class.getName()).log(Level.WARNING, "Error Message from Config Service: {0}", (errMsg = ManageDeviceDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity())));
                        FacesContext.getCurrentInstance().addMessage("growlMessages", new FacesMessage("Error Message from Config Service: " + errMsg));
                }
            }
        } catch (Exception e) {
            Logger.getLogger(ManageDeviceClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }

        return list;
    }
    
}
