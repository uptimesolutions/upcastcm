/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.upcastcm.http.client;

import com.uptime.client.http.client.Client;
import com.uptime.client.utils.vo.HttpResponseVO;
import com.uptime.upcastcm.http.utils.json.dom.AssetPresetDomJsonParser;
import com.uptime.upcastcm.http.utils.json.dom.FaultFrequenciesDomJsonParser;
import com.uptime.upcastcm.http.utils.json.dom.PointLocationNamesDomJsonParser;
import com.uptime.upcastcm.http.utils.json.sax.AssetPresetSaxJsonParser;
import static com.uptime.upcastcm.utils.ApplicationConstants.PRESET_SERVICE_NAME;
import static com.uptime.upcastcm.utils.helperclass.GenericMessage.setNonAutoGrowlMsg;
import static com.uptime.upcastcm.utils.helperclass.GenericMessage.showErrorMsg;
import com.uptime.upcastcm.utils.vo.AssetPresetVO;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author joseph
 */
public class AssetPresetClient extends Client {

    private static AssetPresetClient instance = null;

    /**
     * Private Singleton class
     */
    private AssetPresetClient() {
    }

    /**
     * Returns an instance of the class
     *
     * @return AssetPresetClient object
     */
    public static AssetPresetClient getInstance() {
        if (instance == null) {
            instance = new AssetPresetClient();
        }
        return instance;
    }
    
    
    public List<AssetPresetVO> getGlobalAssetPresetsByCustomer(String host, String customerAccount) {
     List<AssetPresetVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;

        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/presets/globalAsset")
                    .append("?customerAccount=").append(URLEncoder.encode(customerAccount, "UTF-8"));
            
            if ((responseVO = httpGet(PRESET_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        list = AssetPresetSaxJsonParser.getInstance().populateAssetPresetsFromJson(responseVO.getEntity());
                        list.forEach(l -> l.setType("Global"));
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(AssetPresetClient.class.getName()).log(Level.WARNING, "Message from Presets Service: {0}", FaultFrequenciesDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(AssetPresetClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(AssetPresetClient.class.getName()).log(Level.WARNING, "Error Message from Presets Service: {0}", FaultFrequenciesDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                setNonAutoGrowlMsg("growl_service_error_client_message");
            }
        } catch (Exception e) {
            Logger.getLogger(AssetPresetClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;   
    }


    /**
     * @param host, String Object
     * @param customerAccount
     * @param siteId
     * @return list of PointLocationNamesVO objects.
     */
    public List<AssetPresetVO> getSiteAssetPresetsByCustAccSiteId(String host, String customerAccount, UUID siteId) {
        List<AssetPresetVO> siteAssetPresetVOList = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;

        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/presets/siteAsset")
                    .append("?customerAccount=").append(URLEncoder.encode(customerAccount, "UTF-8"))
                    .append("&siteId=").append(siteId);
            
            if ((responseVO = httpGet(PRESET_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        siteAssetPresetVOList = AssetPresetSaxJsonParser.getInstance().populateAssetPresetsFromJson(responseVO.getEntity());
                       siteAssetPresetVOList.forEach(l -> l.setType("Site"));
                        Logger.getLogger(AssetPresetClient.class.getName()).log(Level.INFO, "Fetched Point Location Names");
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(AssetPresetClient.class.getName()).log(Level.WARNING, "Message from Presets Service: {0}", PointLocationNamesDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(AssetPresetClient.class.getName()).log(Level.WARNING, "Error Code from Presets Service : {0}", responseVO.getStatusCode());
                        Logger.getLogger(AssetPresetClient.class.getName()).log(Level.WARNING, "Error Message from Presets Service : {0}", responseVO.getEntity());
                        Logger.getLogger(AssetPresetClient.class.getName()).log(Level.WARNING, "errMsg : {0}", PointLocationNamesDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }

            } else {
                Logger.getLogger(AssetPresetClient.class.getName()).log(Level.WARNING, "Error - No host found for Presets Service.");
            }
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(AssetPresetClient.class.getName()).log(Level.WARNING, e.getMessage(), e);
        }
        return siteAssetPresetVOList;
    }

    /**
     * Create the given GlobalAssetPreset
     *
     * @param host, String Object
     * @param assetPresetsVOList, List of AssetPresetVOs Object
     * @return boolean, true if success, else false
     */
    public boolean createGlobalAssetPresetVO(String host, List<AssetPresetVO> assetPresetsVOList) {
        HttpResponseVO responseVO;

        if ((responseVO = httpPost(PRESET_SERVICE_NAME, host, "/presets/globalAsset", AssetPresetDomJsonParser.getInstance().getJsonFromAssetPresetVO(assetPresetsVOList))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                Logger.getLogger(AssetPresetClient.class.getName()).log(Level.INFO, "Site Asset Presets successfully created.");
                return true;
            } else {
                Logger.getLogger(AssetPresetClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(AssetPresetClient.class.getName()).log(Level.WARNING, "Error Message from Site Asset Presets Service: {0}", AssetPresetDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        setNonAutoGrowlMsg("growl_service_error_client_message");
        return false;
    }

      /**
     * Create the given SiteAssetPreset
     *
     * @param host, String Object
     * @param assetPresetsVOList, List of AssetPresetVOs Object
     * @return boolean, true if success, else false
     */
    public boolean createSiteAssetPresetVO(String host, List<AssetPresetVO> assetPresetsVOList) {
        HttpResponseVO responseVO;

        if ((responseVO = httpPost(PRESET_SERVICE_NAME, host, "/presets/siteAsset", AssetPresetDomJsonParser.getInstance().getJsonFromAssetPresetVO(assetPresetsVOList))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                Logger.getLogger(AssetPresetClient.class.getName()).log(Level.INFO, "Site Asset Presets successfully created.");
                return true;
            } else {
                Logger.getLogger(AssetPresetClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(AssetPresetClient.class.getName()).log(Level.WARNING, "Error Message from Site Asset Presets Service: {0}", AssetPresetDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        setNonAutoGrowlMsg("growl_service_error_client_message");
        return false;
    }  
    /**
     * Create the given SiteAssetPreset
     *
     * @param host, String Object
     * @param assetPresetsVOList, List of AssetPresetVOs Object
     * @return boolean, true if success, else false
     */
    public boolean updateSiteAssetPresetVO(String host, List<AssetPresetVO> assetPresetsVOList) {
        HttpResponseVO responseVO;

        if ((responseVO = httpPut(PRESET_SERVICE_NAME, host, "/presets/siteAsset", AssetPresetDomJsonParser.getInstance().getJsonFromAssetPresetVO(assetPresetsVOList))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                Logger.getLogger(AssetPresetClient.class.getName()).log(Level.INFO, "Site Asset Presets successfully created.");
                return true;
            } else {
                Logger.getLogger(AssetPresetClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(AssetPresetClient.class.getName()).log(Level.WARNING, "Error Message from Site Asset Presets Service: {0}", AssetPresetDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        setNonAutoGrowlMsg("growl_service_error_client_message");
        return false;
    }
    
    /**
     * Create the given GlobalAssetPreset
     *
     * @param host, String Object
     * @param assetPresetsVOList, List of AssetPresetVOs Object
     * @return boolean, true if success, else false
     */
    public boolean updateGlobalAssetPresetVO(String host, List<AssetPresetVO> assetPresetsVOList) {
        HttpResponseVO responseVO;

        if ((responseVO = httpPut(PRESET_SERVICE_NAME, host, "/presets/globalAsset", AssetPresetDomJsonParser.getInstance().getJsonFromAssetPresetVO(assetPresetsVOList))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                Logger.getLogger(AssetPresetClient.class.getName()).log(Level.INFO, "Global Asset Presets successfully created.");
                return true;
            } else {
                Logger.getLogger(AssetPresetClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(AssetPresetClient.class.getName()).log(Level.WARNING, "Error Message from Site Asset Presets Service: {0}", AssetPresetDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        setNonAutoGrowlMsg("growl_service_error_client_message");
        return false;
    }
    
    public List<AssetPresetVO> getSiteAssetPresetsByPK(String host, String customerAccount, UUID siteId, String assetTypeName, UUID assetPresetId) {
    List<AssetPresetVO> siteAssetPresetVOList = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;

        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/presets/siteAsset")
                    .append("?customerAccount=").append(URLEncoder.encode(customerAccount, "UTF-8"))
                    .append("&siteId=").append(siteId)
                     .append("&assetTypeName=").append(assetTypeName)
                    .append("&assetPresetId=").append(assetPresetId);
            
            if ((responseVO = httpGet(PRESET_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        siteAssetPresetVOList = AssetPresetSaxJsonParser.getInstance().populateAssetPresetsFromJson(responseVO.getEntity());
                        Logger.getLogger(AssetPresetClient.class.getName()).log(Level.INFO, "Fetched Point Location Names");
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(AssetPresetClient.class.getName()).log(Level.WARNING, "Message from Presets Service: {0}", PointLocationNamesDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(AssetPresetClient.class.getName()).log(Level.WARNING, "Error Code from Presets Service : {0}", responseVO.getStatusCode());
                        Logger.getLogger(AssetPresetClient.class.getName()).log(Level.WARNING, "Error Message from Presets Service : {0}", responseVO.getEntity());
                        Logger.getLogger(AssetPresetClient.class.getName()).log(Level.WARNING, "errMsg : {0}", PointLocationNamesDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }

            } else {
                Logger.getLogger(AssetPresetClient.class.getName()).log(Level.WARNING, "Error - No host found for Presets Service.");
            }
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(AssetPresetClient.class.getName()).log(Level.WARNING, e.getMessage(), e);
        }
        return siteAssetPresetVOList;    
    }

    public List<AssetPresetVO> getGlobalAssetPresetsByPK(String host, String customerAccount, String assetTypeName, UUID assetPresetId) {
    List<AssetPresetVO> siteAssetPresetVOList = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;

        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/presets/globalAsset")
                    .append("?customerAccount=").append(URLEncoder.encode(customerAccount, "UTF-8"))
                     .append("&assetTypeName=").append(assetTypeName)
                    .append("&assetPresetId=").append(assetPresetId);
            
            if ((responseVO = httpGet(PRESET_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        siteAssetPresetVOList = AssetPresetSaxJsonParser.getInstance().populateAssetPresetsFromJson(responseVO.getEntity());
                        Logger.getLogger(AssetPresetClient.class.getName()).log(Level.INFO, "Fetched Point Location Names");
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(AssetPresetClient.class.getName()).log(Level.WARNING, "Message from Presets Service: {0}", PointLocationNamesDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(AssetPresetClient.class.getName()).log(Level.WARNING, "Error Code from Presets Service : {0}", responseVO.getStatusCode());
                        Logger.getLogger(AssetPresetClient.class.getName()).log(Level.WARNING, "Error Message from Presets Service : {0}", responseVO.getEntity());
                        Logger.getLogger(AssetPresetClient.class.getName()).log(Level.WARNING, "errMsg : {0}", PointLocationNamesDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }

            } else {
                Logger.getLogger(AssetPresetClient.class.getName()).log(Level.WARNING, "Error - No host found for Presets Service.");
            }
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(AssetPresetClient.class.getName()).log(Level.WARNING, e.getMessage(), e);
        }
        return siteAssetPresetVOList;    
    }

    /**
     * Create the given SiteAssetPreset
     *
     * @param host, String Object
     * @param assetPresetsVOList, List of AssetPresetVOs Object
     */
    public boolean deleteSiteAssetPresetVO(String host, List<AssetPresetVO> assetPresetsVOList) {
        HttpResponseVO responseVO;

        if ((responseVO = httpDelete(PRESET_SERVICE_NAME, host, "/presets/siteAsset", AssetPresetDomJsonParser.getInstance().getJsonFromAssetPresetVO(assetPresetsVOList))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                Logger.getLogger(AssetPresetClient.class.getName()).log(Level.INFO, "Site Asset Presets successfully created.");
               return true;
            } else {
                Logger.getLogger(AssetPresetClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(AssetPresetClient.class.getName()).log(Level.WARNING, "Error Message from Site Asset Presets Service: {0}", AssetPresetDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        setNonAutoGrowlMsg("growl_service_error_client_message");
       return false;
    }
    /**
     * Create the given globalAssetPreset
     *
     * @param host, String Object
     * @param assetPresetsVOList, List of AssetPresetVOs Object
     */
    public boolean deleteGlobalAssetPresetVO(String host, List<AssetPresetVO> assetPresetsVOList) {
        HttpResponseVO responseVO;

        if ((responseVO = httpDelete(PRESET_SERVICE_NAME, host, "/presets/globalAsset", AssetPresetDomJsonParser.getInstance().getJsonFromAssetPresetVO(assetPresetsVOList))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                Logger.getLogger(AssetPresetClient.class.getName()).log(Level.INFO, "Global Asset Presets successfully created.");
               return true;
            } else {
                Logger.getLogger(AssetPresetClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(AssetPresetClient.class.getName()).log(Level.WARNING, "Error Message from Site Asset Presets Service: {0}", AssetPresetDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        setNonAutoGrowlMsg("growl_service_error_client_message");
       return false;
    }
 
}
