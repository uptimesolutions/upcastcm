/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.http.client;

import com.uptime.client.http.client.Client;
import com.uptime.client.utils.vo.HttpResponseVO;
import com.uptime.upcastcm.bean.UserManageBean;
import com.uptime.upcastcm.http.utils.json.dom.AlarmsDomJsonParser;
import com.uptime.upcastcm.http.utils.json.sax.AlarmsSaxJsonParser;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import com.uptime.upcastcm.utils.vo.AlarmsVO;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 *
 * @author gsingh
 */
public class AlarmsClient extends Client {
    private static AlarmsClient instance = null;

    /**
     * Private Singleton class
     */
    private AlarmsClient() {
    }

    /**
     * Returns an instance of the class
     *
     * @return AlarmsClient object
     */
    public static AlarmsClient getInstance() {
        if (instance == null) {
            instance = new AlarmsClient();
        }
        return instance;
    }

    /**
     * Get Alarms Info based on the given info
     *
     * @param host String Object
     * @param customer, String Object
     * @param siteId
     * @return List Object of AlarmsVO Objects
     */
    public List<AlarmsVO> getAlarmCountByCustomerSite(String host, String customer, UUID siteId) {
        List<AlarmsVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;

        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/alarms")
                    .append("?customer=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&siteId=").append(siteId);
            
            if ((responseVO = httpGet(ALARM_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                if (responseVO.getStatusCode() == 200) {
                    list = AlarmsSaxJsonParser.getInstance().populateAlarmsFromJson(responseVO.getEntity());
                } else {
                    Logger.getLogger(AlarmsClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                    Logger.getLogger(AlarmsClient.class.getName()).log(Level.WARNING, "Error Message from Alarms Service: {0}", AlarmsDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
        } catch (Exception e) {
            Logger.getLogger(AlarmsClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }

    /**
     * Get Alarms Info based on the given info
     *
     * @param host String Object
     * @param customer, String Object
     * @param siteId
     * @param areaId
     * @return List Object of AlarmsVO Objects
     */
    public List<AlarmsVO> getAlarmsByCustomerSiteArea(String host, String customer, UUID siteId, UUID areaId) {
        List<AlarmsVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;

        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/alarms")
                    .append("?customer=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&siteId=").append(siteId)
                    .append("&areaId=").append(areaId)
                    .append("&returnType=activeAlarms");
            
            if ((responseVO = httpGet(ALARM_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                if (responseVO.getStatusCode() == 200) {
                    list = AlarmsSaxJsonParser.getInstance().populateAlarmsFromJson(responseVO.getEntity());
                } else {
                    Logger.getLogger(AlarmsClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                    Logger.getLogger(AlarmsClient.class.getName()).log(Level.WARNING, "Error Message from Alarms Service: {0}", AlarmsDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
        } catch (Exception e) {
            Logger.getLogger(AlarmsClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }

    /**
     * Get Alarms Info based on the given info
     *
     * @param host String Object
     * @param customer, String Object
     * @param siteId
     * @param areaId
     * @param assetId
     * @return List Object of AlarmsVO Objects
     */
    public List<AlarmsVO> getAlarmsByCustomerSiteAreaAsset(String host, String customer, UUID siteId, UUID areaId, UUID assetId) {
        List<AlarmsVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;

        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/alarms")
                    .append("?customer=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&siteId=").append(siteId)
                    .append("&areaId=").append(areaId)
                    .append("&assetId=").append(assetId)
                    .append("&returnType=activeAlarms");
            
            if ((responseVO = httpGet(ALARM_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                if (responseVO.getStatusCode() == 200) {
                    list = AlarmsSaxJsonParser.getInstance().populateAlarmsFromJson(responseVO.getEntity());
                } else {
                    Logger.getLogger(AlarmsClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                    Logger.getLogger(AlarmsClient.class.getName()).log(Level.WARNING, "Error Message from Alarms Service: {0}", AlarmsDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
        } catch (Exception e) {
            Logger.getLogger(AlarmsClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }

    /**
     * Get Alarms Info based on the given info
     *
     * @param host String Object
     * @param customer, String Object
     * @param siteId
     * @param areaId
     * @param assetId
     * @param pointLocationId
     * @return List Object of AlarmsVO Objects
     */
    public List<AlarmsVO> getAlarmsByCustomerSiteAreaAssetPointLocation(String host, String customer, UUID siteId, UUID areaId, UUID assetId, UUID pointLocationId) {
        List<AlarmsVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;

        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/alarms")
                    .append("?customer=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&siteId=").append(siteId)
                    .append("&areaId=").append(areaId)
                    .append("&assetId=").append(assetId)
                    .append("&pointLocationId=").append(pointLocationId)
                    .append("&returnType=activeAlarms");
            
            if ((responseVO = httpGet(ALARM_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                if (responseVO.getStatusCode() == 200) {
                    list = AlarmsSaxJsonParser.getInstance().populateAlarmsFromJson(responseVO.getEntity());
                } else {
                    Logger.getLogger(AlarmsClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                    Logger.getLogger(AlarmsClient.class.getName()).log(Level.WARNING, "Error Message from Alarms Service: {0}", AlarmsDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
        } catch (Exception e) {
            Logger.getLogger(AlarmsClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }

    /**
     * Get Alarms Info based on the given info
     *
     * @param host String Object
     * @param customer, String Object
     * @param siteId
     * @param areaId
     * @param assetId
     * @param pointLocationId
     * @param pointId
     * @return List Object of AlarmsVO Objects
     */
    public List<AlarmsVO> getAlarmsByCustomerSiteAreaAssetPointLocationPoint(String host, String customer, UUID siteId, UUID areaId, UUID assetId, UUID pointLocationId, UUID pointId) {
        List<AlarmsVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;

        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/alarms")
                    .append("?customer=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&siteId=").append(siteId)
                    .append("&areaId=").append(areaId)
                    .append("&assetId=").append(assetId)
                    .append("&pointLocationId=").append(pointLocationId)
                    .append("&pointId=").append(pointId)
                    .append("&returnType=activeAlarms"); 
            
            if ((responseVO = httpGet(ALARM_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                if (responseVO.getStatusCode() == 200) {
                    list = AlarmsSaxJsonParser.getInstance().populateAlarmsFromJson(responseVO.getEntity());
                } else {
                    Logger.getLogger(AlarmsClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                    Logger.getLogger(AlarmsClient.class.getName()).log(Level.WARNING, "Error Message from Alarms Service: {0}", AlarmsDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
        } catch (Exception e) {
            Logger.getLogger(AlarmsClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }

    /**
     * Get Alarms Info based on the given info
     *
     * @param host String Object
     * @param customer, String Object
     * @param siteId
     * @param createdYear
     * @param areaId
     * @param assetId
     * @param pointLocationId
     * @param pointId
     * @param paramName
     * @return List Object of AlarmsVO Objects
     */
    public List<AlarmsVO> getAlarmsHistory(String host,String customer, UUID siteId, int createdYear, UUID areaId, UUID assetId, UUID pointLocationId, UUID pointId, String paramName) {
        List<AlarmsVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;

        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/alarms")
                    .append("?customer=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&siteId=").append(siteId)
                    .append("&createdYear=").append(createdYear)
                    .append("&areaId=").append(areaId)
                    .append("&assetId=").append(assetId)
                    .append("&pointLocationId=").append(pointLocationId)
                    .append("&pointId=").append(pointId)
                    .append("&paramName=").append(URLEncoder.encode(paramName, "UTF-8"));
            
            if ((responseVO = httpGet(ALARM_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                if (responseVO.getStatusCode() == 200) {
                    list = AlarmsSaxJsonParser.getInstance().populateAlarmsFromJson(responseVO.getEntity());
                } else {
                    Logger.getLogger(AlarmsClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                    Logger.getLogger(AlarmsClient.class.getName()).log(Level.WARNING, "Error Message from Alarms Service: {0}", AlarmsDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
        } catch (Exception e) {
            Logger.getLogger(AlarmsClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }

    
    /**
     * Get Alarms Info based on the given info
     *
     * @param host String Object
     * @param customer, String Object
     * @param siteId
     * @param createdYear
     * @param areaId
     * @param assetId
     * @param pointLocationId
     * @return List Object of AlarmsVO Objects
     */
    public List<AlarmsVO> viewAlarmsHistory(String host,String customer, UUID siteId, int createdYear, UUID areaId, UUID assetId, UUID pointLocationId) {
        List<AlarmsVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;

        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/alarms")
                    .append("?customer=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&siteId=").append(siteId)
                    .append("&createdYear=").append(createdYear)
                    .append("&areaId=").append(areaId)
                    .append("&assetId=").append(assetId)
                    .append("&pointLocationId=").append(pointLocationId);
            
            if ((responseVO = httpGet(ALARM_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                if (responseVO.getStatusCode() == 200) {
                    list = AlarmsSaxJsonParser.getInstance().populateAlarmsFromJson(responseVO.getEntity());
                } else {
                    Logger.getLogger(AlarmsClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                    Logger.getLogger(AlarmsClient.class.getName()).log(Level.WARNING, "Error Message from Alarms Service: {0}", AlarmsDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
        } catch (Exception e) {
            Logger.getLogger(AlarmsClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }

    /**
     * Get Alarms list Info based on the given info
     *
     * @param host String Object
     * @param customer, String Object
     * @param siteName, String Object
     * @param areaName, String Object
     * @return List Object of AlarmsVO Objects
     */
    // This need sto clerify from Madam. 
    public List<AlarmsVO> getAlarmsList(String host, String customer, String siteName, String areaName) {
        List<AlarmsVO> alarmsVOList = null;
        StringBuilder endPoint;
        HttpResponseVO responseVO;

        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/alarms")
                    .append("?customer=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&siteName=").append(URLEncoder.encode(siteName, "UTF-8"))
                    .append("&areaName=").append(URLEncoder.encode(areaName, "UTF-8"))
                    .append("&areaSite=").append(true)
                    .append("&areaConfigSchedule=").append(true);
            
            if ((responseVO = httpGet(ALARM_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                if (responseVO.getStatusCode() == 200) {
                    alarmsVOList = AlarmsSaxJsonParser.getInstance().populateAlarmsFromJson(responseVO.getEntity());
                } else {
                    Logger.getLogger(AlarmsClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                    Logger.getLogger(AlarmsClient.class.getName()).log(Level.WARNING, "Error Message from Alarms Service: {0}", AlarmsDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
        } catch (Exception e) {
            Logger.getLogger(AlarmsClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
        return alarmsVOList;
    }

    public boolean createAlarm(String host, AlarmsVO alarmsVO) {
        UserManageBean userManageBean;
        HttpResponseVO responseVO;
        String errMsg;

        if ((responseVO = httpPost(ALARM_SERVICE_NAME, host, "/alarms", AlarmsDomJsonParser.getInstance().getJsonFromAlarms(alarmsVO))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
                FacesContext.getCurrentInstance().addMessage("growlMessages", new FacesMessage(userManageBean.getResourceBundleString("growl_create_item_items")));
                Logger.getLogger(AlarmsClient.class.getName()).log(Level.INFO, "Alarms successfully created.");
                return true;
            } else {
                Logger.getLogger(AlarmsClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(AlarmsClient.class.getName()).log(Level.WARNING, "Error Message from Alarms Service: {0}", (errMsg = AlarmsDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity())));
                FacesContext.getCurrentInstance().addMessage("growlMessages", new FacesMessage("Error Message from Alarms Service: " + errMsg + "."));
            }
        }
        return false;
    }

    public boolean updateAlarm(String host, AlarmsVO alarmsVO) {
        UserManageBean userManageBean;
        HttpResponseVO responseVO;
        String errMsg;

        if ((responseVO = httpPut(ALARM_SERVICE_NAME, host, "/alarms", AlarmsDomJsonParser.getInstance().getJsonFromAlarms(alarmsVO))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
                FacesContext.getCurrentInstance().addMessage("growlMessages", new FacesMessage(userManageBean.getResourceBundleString("growl_update_item_items")));
                Logger.getLogger(AlarmsClient.class.getName()).log(Level.INFO, "Alarms successfully updated.");
                return true;
            } else {
                Logger.getLogger(AlarmsClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(AlarmsClient.class.getName()).log(Level.WARNING, "Error Message from Alarms Service: {0}", (errMsg = AlarmsDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity())));
                FacesContext.getCurrentInstance().addMessage("growlMessages", new FacesMessage("Error Message from Alarms Service: " + errMsg + "."));
            }
        }
        return false;
    }

    public boolean updateAlarmList(String host, List<AlarmsVO> alarmsVOList) {
        UserManageBean userManageBean;
        HttpResponseVO responseVO;
        String errMsg;

        if ((responseVO = httpPut(ALARM_SERVICE_NAME, host, "/alarms", AlarmsDomJsonParser.getInstance().getJsonFromAlarmsList(alarmsVOList))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
                FacesContext.getCurrentInstance().addMessage("growlMessages", new FacesMessage(userManageBean.getResourceBundleString("growl_update_item_items")));
                Logger.getLogger(AlarmsClient.class.getName()).log(Level.INFO, "Alarms successfully updated.");
                return true;
            } else {
                Logger.getLogger(AlarmsClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(AlarmsClient.class.getName()).log(Level.WARNING, "Error Message from Alarms Service: {0}", (errMsg = AlarmsDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity())));
                FacesContext.getCurrentInstance().addMessage("growlMessages", new FacesMessage("Error Message from Alarms Service: " + errMsg + "."));
            }
        }
        return false;
    }

}
