/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.http.client;

import com.uptime.client.http.client.Client;
import com.uptime.client.utils.vo.HttpResponseVO;
import com.uptime.upcastcm.bean.UserManageBean;
import com.uptime.upcastcm.http.utils.json.dom.UserDomJsonParser;
import com.uptime.upcastcm.http.utils.json.sax.UserSaxJsonParser;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import static com.uptime.upcastcm.utils.helperclass.GenericMessage.showErrorMsg;
import com.uptime.upcastcm.utils.vo.AlarmNotificationVO;
import java.io.Serializable;
import java.net.URLEncoder;
import java.util.ArrayList; 
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 *
 * @author aswani
 */
public class AlarmNotificationClient extends Client implements Serializable {
    private static AlarmNotificationClient instance = null;

    /**
     * Private Singleton class
     */
    private AlarmNotificationClient() {
    }

    /**
     * Returns an instance of the class
     * 
     * @return AlarmNotificationClient object
     */
    public static AlarmNotificationClient getInstance() {
        if (instance == null) {
            instance = new AlarmNotificationClient();
        }
        return instance;
    }
    
    /**
     * Get UserSiteList based on the given info
     * 
     * @param host, String Object
     * @param userId, String Object
     * @param siteId, String Object
     * @return list of  AlarmNotificationVO Object
     */
    public List<AlarmNotificationVO> getUserSiteListByUserIdSiteId(String host, String userId, String siteId) {
        List<AlarmNotificationVO> siteList = new ArrayList();
        HttpResponseVO responseVO;
        boolean serviceIssue = true;
        
        try {
            if ((responseVO = httpGet(USER_PREFERENCE_SERVICE_NAME, host, "/user/alarm?userId=" + URLEncoder.encode(userId, "UTF-8") + "&siteId=" + URLEncoder.encode(siteId, "UTF-8"))) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        siteList = UserSaxJsonParser.getInstance().populateAlarmNotificationFromJson(responseVO.getEntity());
                        Logger.getLogger(AlarmNotificationClient.class.getName()).log(Level.INFO, "Fetched Alarm notifications");
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(AlarmNotificationClient.class.getName()).log(Level.WARNING, "Message from User Service: {0}", UserDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(AlarmNotificationClient.class.getName()).log(Level.WARNING, "Error Code from User Service : {0}", responseVO.getStatusCode());
                        Logger.getLogger(AlarmNotificationClient.class.getName()).log(Level.WARNING, "Error Message from User Service : {0}", responseVO.getEntity());
                        Logger.getLogger(AlarmNotificationClient.class.getName()).log(Level.WARNING, "errMsg : {0}", UserDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }

            } else {
                Logger.getLogger(AlarmNotificationClient.class.getName()).log(Level.WARNING, "Error - No host found for User Service.");
            }
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(AlarmNotificationClient.class.getName()).log(Level.WARNING, e.getMessage(), e);
        }
        return siteList;
    }
    
    /**
     * Get UserSiteList based on the given info
     * 
     * @param host, String Object
     * @param userId, String Object
     * @return list of  AlarmNotificationVO Object
     */
    public List<AlarmNotificationVO> getUserSiteListByUserId(String host, String userId) {
        List<AlarmNotificationVO> siteList = new ArrayList();
        HttpResponseVO responseVO;
        boolean serviceIssue = true;
        
        try {
            if ((responseVO = httpGet(USER_PREFERENCE_SERVICE_NAME, host, "/user/alarm?userId=" + URLEncoder.encode(userId, "UTF-8"))) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        siteList = UserSaxJsonParser.getInstance().populateAlarmNotificationFromJson(responseVO.getEntity());
                        Logger.getLogger(AlarmNotificationClient.class.getName()).log(Level.INFO, "Fetched Alarm notifications");
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(AlarmNotificationClient.class.getName()).log(Level.WARNING, "Message from User Service: {0}", UserDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(AlarmNotificationClient.class.getName()).log(Level.WARNING, "Error Code from User Service : {0}", responseVO.getStatusCode());
                        Logger.getLogger(AlarmNotificationClient.class.getName()).log(Level.WARNING, "Error Message from User Service : {0}", responseVO.getEntity());
                        Logger.getLogger(AlarmNotificationClient.class.getName()).log(Level.WARNING, "errMsg : {0}", UserDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }

            } else {
                Logger.getLogger(AlarmNotificationClient.class.getName()).log(Level.WARNING, "Error - No host found for User Service.");
            }
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(AlarmNotificationClient.class.getName()).log(Level.WARNING, e.getMessage(), e);
        }
        return siteList;
    }
    
    /**
     * Get UserSiteList based on the given info
     * 
     * @param host, String Object
     * @param userId, String Object
     * @param siteId, String Object
     * @param assetId, String Object
     * @return list of  AlarmNotificationVO Object
     */
    public List<AlarmNotificationVO> getUserSiteListByUserIdSiteIdAssetId(String host, String userId, String siteId, String assetId) {
        List<AlarmNotificationVO> siteList = new ArrayList();
        HttpResponseVO responseVO;
        boolean serviceIssue = true;
        StringBuilder endPoint;
        
        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/user/alarm")
                    .append("?userId=").append(URLEncoder.encode(userId, "UTF-8"))
                    .append("&siteId=").append(URLEncoder.encode(siteId, "UTF-8"))
                    .append("&assetId=").append(URLEncoder.encode(assetId, "UTF-8"));
            
            if ((responseVO = httpGet(USER_PREFERENCE_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        siteList = UserSaxJsonParser.getInstance().populateAlarmNotificationFromJson(responseVO.getEntity());
                        Logger.getLogger(AlarmNotificationClient.class.getName()).log(Level.INFO, "Fetched Alarm notifications");
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(AlarmNotificationClient.class.getName()).log(Level.WARNING, "Message from User Service: {0}", UserDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(AlarmNotificationClient.class.getName()).log(Level.WARNING, "Error Code from User Service : {0}", responseVO.getStatusCode());
                        Logger.getLogger(AlarmNotificationClient.class.getName()).log(Level.WARNING, "Error Message from User Service : {0}", responseVO.getEntity());
                        Logger.getLogger(AlarmNotificationClient.class.getName()).log(Level.WARNING, "errMsg : {0}", UserDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }

            } else {
                Logger.getLogger(AlarmNotificationClient.class.getName()).log(Level.WARNING, "Error - No host found for User Service.");
            }
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(AlarmNotificationClient.class.getName()).log(Level.WARNING, e.getMessage(), e);
        }
        return siteList;
    }
    
    /**
     * Create the given Site
     *
     * @param host, String Object
     * @param alarmNotificationVO, AlarmNotificationVO Object
     * @return boolean, true if success, else false
     */
    public boolean createAlarmNotification(String host, AlarmNotificationVO alarmNotificationVO) {
        UserManageBean userManageBean;
        HttpResponseVO responseVO;
        String errMsg;
        
        if ((responseVO = httpPost(USER_PREFERENCE_SERVICE_NAME, host, "/user/alarm", UserDomJsonParser.getInstance().getJsonFromObject(alarmNotificationVO))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
                FacesContext.getCurrentInstance().addMessage("nonAutoGrowlMessages", new FacesMessage(userManageBean.getResourceBundleString("dialog_preferences_msg_2")));
                Logger.getLogger(AlarmNotificationClient.class.getName()).log(Level.INFO, "Alarm Notification successfully created.");
                return true;
            } else {
                Logger.getLogger(AlarmNotificationClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(AlarmNotificationClient.class.getName()).log(Level.WARNING, "Error Message from User Service: {0}", (errMsg = UserDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity())));
                FacesContext.getCurrentInstance().addMessage("growlMessages", new FacesMessage("Error Message from User Service: " + errMsg + "."));
            }
        }
        showErrorMsg();
        
        return false;
    }
    
    /**
     * Create the given Site
     *
     * @param host, String Object
     * @param alarmNotificationVO, AlarmNotificationVO Object
     * @return boolean, true if success, else false
     */
    public boolean updateAlarmNotification(String host, AlarmNotificationVO alarmNotificationVO) {
        UserManageBean userManageBean;
        HttpResponseVO responseVO;
        String errMsg;
        
        if ((responseVO = httpPut(USER_PREFERENCE_SERVICE_NAME, host, "/user/alarm", UserDomJsonParser.getInstance().getJsonFromObject(alarmNotificationVO))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
                FacesContext.getCurrentInstance().addMessage("nonAutoGrowlMessages", new FacesMessage(userManageBean.getResourceBundleString("dialog_preferences_msg_3")));
                Logger.getLogger(AlarmNotificationClient.class.getName()).log(Level.INFO, "Alarm Notification successfully created.");
                return true;
            } else {
                Logger.getLogger(AlarmNotificationClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(AlarmNotificationClient.class.getName()).log(Level.WARNING, "Error Message from User Service: {0}", (errMsg = UserDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity())));
                FacesContext.getCurrentInstance().addMessage("growlMessages", new FacesMessage("Error Message from User Service: " + errMsg + "."));
            }
        }
        showErrorMsg();

        return false;
    }

}
