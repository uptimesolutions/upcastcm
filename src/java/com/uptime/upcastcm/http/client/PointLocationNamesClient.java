/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.http.client;

import com.uptime.client.http.client.Client;
import com.uptime.client.utils.vo.HttpResponseVO;
import com.uptime.upcastcm.http.utils.json.dom.FaultFrequenciesDomJsonParser;
import com.uptime.upcastcm.http.utils.json.dom.PointLocationNamesDomJsonParser;
import com.uptime.upcastcm.http.utils.json.sax.PointLocationNamesSaxJsonParser;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import static com.uptime.upcastcm.utils.helperclass.GenericMessage.setNonAutoGrowlMsg;
import static com.uptime.upcastcm.utils.helperclass.GenericMessage.showErrorMsg;
import com.uptime.upcastcm.utils.vo.PointLocationNamesVO;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Mohd Juned Alam
 */
public class PointLocationNamesClient extends Client {
    private static PointLocationNamesClient instance = null;

    /**
     * Private Singleton class
     */
    private PointLocationNamesClient() {

    }

    /**
     * Returns an instance of the class
     *
     * @return PointLocationNamesClient object
     */
    public static PointLocationNamesClient getInstance() {
        if (instance == null) {
            instance = new PointLocationNamesClient();
        }
        return instance;
    }

    /**
     * @param host, String Object
     * @param customerAccount
     * @return list of PointLocationNamesVO objects.
     */
    public List<PointLocationNamesVO> getGlobalPointLocationNamesByCustAcc(String host, String customerAccount) {
        List<PointLocationNamesVO> globalPtLocationNameList = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;

        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/presets/globalPtLocationNames")
                    .append("?customerAccount=").append(URLEncoder.encode(customerAccount, "UTF-8"));
            
            if ((responseVO = httpGet(PRESET_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        globalPtLocationNameList = PointLocationNamesSaxJsonParser.getInstance().populatePointLocationNamesFromJson(responseVO.getEntity());
                        Logger.getLogger(PointLocationNamesClient.class.getName()).log(Level.INFO, "Fetched Point Location Names");
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(PointLocationNamesClient.class.getName()).log(Level.WARNING, "Message from Presets Service: {0}", PointLocationNamesDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(PointLocationNamesClient.class.getName()).log(Level.WARNING, "Error Code from Presets Service : {0}", responseVO.getStatusCode());
                        Logger.getLogger(PointLocationNamesClient.class.getName()).log(Level.WARNING, "Error Message from Presets Service : {0}", responseVO.getEntity());
                        Logger.getLogger(PointLocationNamesClient.class.getName()).log(Level.WARNING, "errMsg : {0}", PointLocationNamesDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }

            } else {
                Logger.getLogger(PointLocationNamesClient.class.getName()).log(Level.WARNING, "Error - No host found for Presets Service.");
            }
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(PointLocationNamesClient.class.getName()).log(Level.WARNING, e.getMessage(), e);
        }
        return globalPtLocationNameList;
    }

     /**
     * @param host, String Object
     * @param customerAccount
     * @param pointLocationName
     * @return list of PointLocationNamesVO objects.
     */
    public List<PointLocationNamesVO> getGlobalPointLocationNamesByPK(String host, String customerAccount, String pointLocationName) {
        List<PointLocationNamesVO> globalPtLocationNameList = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;

        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/presets/globalPtLocationNames")
                    .append("?customerAccount=").append(URLEncoder.encode(customerAccount, "UTF-8"))
                    .append("&pointLocationName=").append(URLEncoder.encode(pointLocationName, "UTF-8"));
            
            if ((responseVO = httpGet(PRESET_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        globalPtLocationNameList = PointLocationNamesSaxJsonParser.getInstance().populatePointLocationNamesFromJson(responseVO.getEntity());
                        Logger.getLogger(PointLocationNamesClient.class.getName()).log(Level.INFO, "Fetched Point Location Names");
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(PointLocationNamesClient.class.getName()).log(Level.WARNING, "Message from Presets Service: {0}", PointLocationNamesDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(PointLocationNamesClient.class.getName()).log(Level.WARNING, "Error Code from Presets Service : {0}", responseVO.getStatusCode());
                        Logger.getLogger(PointLocationNamesClient.class.getName()).log(Level.WARNING, "Error Message from Presets Service : {0}", responseVO.getEntity());
                        Logger.getLogger(PointLocationNamesClient.class.getName()).log(Level.WARNING, "errMsg : {0}", PointLocationNamesDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }

            } else {
                Logger.getLogger(PointLocationNamesClient.class.getName()).log(Level.WARNING, "Error - No host found for Presets Service.");
            }
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(PointLocationNamesClient.class.getName()).log(Level.WARNING, e.getMessage(), e);
        }
        return globalPtLocationNameList;
    }

    /**
     * @param host, String Object
     * @param customerAccount
     * @param siteId
     * @return list of PointLocationNamesVO objects.
     */
    public List<PointLocationNamesVO> getSitePointLocationNamesByCustAccSiteId(String host, String customerAccount, UUID siteId) {
        List<PointLocationNamesVO> sitePtLocationNameList = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;

        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/presets/sitePtLocationNames")
                    .append("?customerAccount=").append(URLEncoder.encode(customerAccount, "UTF-8"))
                    .append("&siteId=").append(siteId);
            
            if ((responseVO = httpGet(PRESET_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        sitePtLocationNameList = PointLocationNamesSaxJsonParser.getInstance().populatePointLocationNamesFromJson(responseVO.getEntity());
                        Logger.getLogger(PointLocationNamesClient.class.getName()).log(Level.INFO, "Fetched Point Location Names");
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(PointLocationNamesClient.class.getName()).log(Level.WARNING, "Message from Presets Service: {0}", PointLocationNamesDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(PointLocationNamesClient.class.getName()).log(Level.WARNING, "Error Code from Presets Service : {0}", responseVO.getStatusCode());
                        Logger.getLogger(PointLocationNamesClient.class.getName()).log(Level.WARNING, "Error Message from Presets Service : {0}", responseVO.getEntity());
                        Logger.getLogger(PointLocationNamesClient.class.getName()).log(Level.WARNING, "errMsg : {0}", PointLocationNamesDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }

            } else {
                Logger.getLogger(PointLocationNamesClient.class.getName()).log(Level.WARNING, "Error - No host found for Presets Service.");
            }
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(PointLocationNamesClient.class.getName()).log(Level.WARNING, e.getMessage(), e);
        }
        return sitePtLocationNameList;
    }

    /**
     * @param host, String Object
     * @param customerAccount
     * @param siteId
     * @param pointLocationName
     * @return list of PointLocationNamesVO objects.
     */
    public List<PointLocationNamesVO> getSitePointLocationNamesByPK(String host, String customerAccount, UUID siteId, String pointLocationName) {
        List<PointLocationNamesVO> sitePtLocationNameList = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;

        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/presets/sitePtLocationNames")
                    .append("?customerAccount=").append(URLEncoder.encode(customerAccount, "UTF-8"))
                    .append("&siteId=").append(siteId)
                    .append("&pointLocationName=").append(URLEncoder.encode(pointLocationName, "UTF-8"));
            
            if ((responseVO = httpGet(PRESET_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        sitePtLocationNameList = PointLocationNamesSaxJsonParser.getInstance().populatePointLocationNamesFromJson(responseVO.getEntity());
                        Logger.getLogger(PointLocationNamesClient.class.getName()).log(Level.INFO, "Fetched Point Location Names");
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(PointLocationNamesClient.class.getName()).log(Level.WARNING, "Message from Presets Service: {0}", PointLocationNamesDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(PointLocationNamesClient.class.getName()).log(Level.WARNING, "Error Code from Presets Service : {0}", responseVO.getStatusCode());
                        Logger.getLogger(PointLocationNamesClient.class.getName()).log(Level.WARNING, "Error Message from Presets Service : {0}", responseVO.getEntity());
                        Logger.getLogger(PointLocationNamesClient.class.getName()).log(Level.WARNING, "errMsg : {0}", PointLocationNamesDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }

            } else {
                Logger.getLogger(PointLocationNamesClient.class.getName()).log(Level.WARNING, "Error - No host found for Presets Service.");
            }
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(PointLocationNamesClient.class.getName()).log(Level.WARNING, e.getMessage(), e);
        }
        return sitePtLocationNameList;
    }

    
    
    /**
     * Create the given SitePointLocationNames
     *
     * @param host, String Object
     * @param pointLocationNamesVO, PointLocationNamesVO Object
     * @return boolean, true if success, else false
     */
    public boolean createSitePointLocationNames(String host, PointLocationNamesVO pointLocationNamesVO) {
        HttpResponseVO responseVO;

        if ((responseVO = httpPost(PRESET_SERVICE_NAME, host, "/presets/sitePtLocationNames", PointLocationNamesDomJsonParser.getInstance().getJsonFrompointLocationNames(pointLocationNamesVO))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {               
                Logger.getLogger(PointLocationNamesClient.class.getName()).log(Level.INFO, "Site Point Location Names successfully created.");
                return true;
            } else {
                Logger.getLogger(PointLocationNamesClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(PointLocationNamesClient.class.getName()).log(Level.WARNING, "Error Message from Site Point Location Names Service: {0}", FaultFrequenciesDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        setNonAutoGrowlMsg("growl_service_error_client_message");
        
        return false;
    }

    
    /**
     * Create the given GlobalPointLocationNames
     *
     * @param host, String Object
     * @param pointLocationNamesVO, PointLocationNamesVO Object
     * @return boolean, true if success, else false
     */
    public boolean createGlobalPointLocationNames(String host, PointLocationNamesVO pointLocationNamesVO) {
        HttpResponseVO responseVO;

        if ((responseVO = httpPost(PRESET_SERVICE_NAME, host, "/presets/globalPtLocationNames", PointLocationNamesDomJsonParser.getInstance().getJsonFrompointLocationNames(pointLocationNamesVO))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {               
                Logger.getLogger(PointLocationNamesClient.class.getName()).log(Level.INFO, "Global Point Location Names successfully created.");
                return true;
            } else {
                Logger.getLogger(PointLocationNamesClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(PointLocationNamesClient.class.getName()).log(Level.WARNING, "Error Message from Global Point Location Names Service: {0}", FaultFrequenciesDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        setNonAutoGrowlMsg("growl_service_error_client_message");
        
        return false;
    }

    
    
    /**
     * Update the given SitePointLocationNames
     *
     * @param host, String Object
     * @param pointLocationNamesVO, PointLocationNamesVO Object
     * @return boolean, true if success, else false
     */
    public boolean updateSitePointLocationNames(String host, PointLocationNamesVO pointLocationNamesVO) {
        HttpResponseVO responseVO;

        if ((responseVO = httpPut(PRESET_SERVICE_NAME, host, "/presets/sitePtLocationNames", PointLocationNamesDomJsonParser.getInstance().getJsonFrompointLocationNames(pointLocationNamesVO))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {               
                Logger.getLogger(PointLocationNamesClient.class.getName()).log(Level.INFO, "Updated Site Point Location Names successfully.");
                return true;
            } else {
                Logger.getLogger(PointLocationNamesClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(PointLocationNamesClient.class.getName()).log(Level.WARNING, "Error Message from Site Point Location Names Service: {0}", FaultFrequenciesDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        setNonAutoGrowlMsg("growl_service_error_client_message");
        
        return false;
    }

    
    /**
     * Update the given GlobalPointLocationNames
     *
     * @param host, String Object
     * @param pointLocationNamesVO, PointLocationNamesVO Object
     * @return boolean, true if success, else false
     */
    public boolean updateGlobalPointLocationNames(String host, PointLocationNamesVO pointLocationNamesVO) {
        HttpResponseVO responseVO;

        if ((responseVO = httpPut(PRESET_SERVICE_NAME, host, "/presets/globalPtLocationNames", PointLocationNamesDomJsonParser.getInstance().getJsonFrompointLocationNames(pointLocationNamesVO))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {               
                Logger.getLogger(PointLocationNamesClient.class.getName()).log(Level.INFO, "Updated Global Point Location Names successfully.");
                return true;
            } else {
                Logger.getLogger(PointLocationNamesClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(PointLocationNamesClient.class.getName()).log(Level.WARNING, "Error Message from Global Point Location Names Service: {0}", FaultFrequenciesDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        setNonAutoGrowlMsg("growl_service_error_client_message");
        
        return false;
    }

    
    /**
     * Delete the given SitePointLocationNames
     *
     * @param host, String Object
     * @param pointLocationNamesVO, PointLocationNamesVO Object
     * @return boolean, true if success, else false
     */
    public boolean deleteSitePointLocationNames(String host, PointLocationNamesVO pointLocationNamesVO) {
        HttpResponseVO responseVO;

        if ((responseVO = httpDelete(PRESET_SERVICE_NAME, host, "/presets/sitePtLocationNames", PointLocationNamesDomJsonParser.getInstance().getJsonFrompointLocationNames(pointLocationNamesVO))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {               
                Logger.getLogger(PointLocationNamesClient.class.getName()).log(Level.INFO, "Deleted Site Point Location Names successfully.");
                return true;
            } else {
                Logger.getLogger(PointLocationNamesClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(PointLocationNamesClient.class.getName()).log(Level.WARNING, "Error Message from Site Point Location Names Service: {0}", FaultFrequenciesDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        setNonAutoGrowlMsg("growl_service_error_client_message");
        
        return false;
    }

    
    /**
     * Delete the given GlobalPointLocationNames
     *
     * @param host, String Object
     * @param pointLocationNamesVO, PointLocationNamesVO Object
     * @return boolean, true if success, else false
     */
    public boolean deleteGlobalPointLocationNames(String host, PointLocationNamesVO pointLocationNamesVO) {
        HttpResponseVO responseVO;

        if ((responseVO = httpDelete(PRESET_SERVICE_NAME, host, "/presets/globalPtLocationNames", PointLocationNamesDomJsonParser.getInstance().getJsonFrompointLocationNames(pointLocationNamesVO))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {               
                Logger.getLogger(PointLocationNamesClient.class.getName()).log(Level.INFO, "Deleted Global Point Location Names successfully.");
                return true;
            } else {
                Logger.getLogger(PointLocationNamesClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(PointLocationNamesClient.class.getName()).log(Level.WARNING, "Error Message from Global Point Location Names Service: {0}", FaultFrequenciesDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        setNonAutoGrowlMsg("growl_service_error_client_message");
        
        return false;
    }

}
