/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.http.client;

import static com.uptime.client.http.servlet.AbstractServiceManagerServlet.getPropertyValue;
import com.uptime.client.http.client.Client;
import com.uptime.client.utils.vo.HttpResponseVO;
import com.uptime.upcastcm.bean.UserManageBean;
import com.uptime.upcastcm.http.utils.json.dom.SiteDomJsonParser;
import com.uptime.upcastcm.http.utils.json.sax.SiteSaxJsonParser;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import static com.uptime.upcastcm.utils.helperclass.GenericMessage.showErrorMsg;
import com.uptime.upcastcm.utils.vo.SiteContactVO;
import com.uptime.upcastcm.utils.vo.SiteVO;
import java.io.StringWriter;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.apache.commons.io.IOUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

/**
 *
 * @author kpati
 */
public class SiteClient extends Client {
    private static SiteClient instance = null;
    
    /**
     * Private Singleton class
     */
    private SiteClient() {
    }

    /**
     * Returns an instance of the class
     *
     * @return SiteClient object
     */
    public static SiteClient getInstance() {
        if (instance == null) {
            instance = new SiteClient();
        }
        return instance;
    }

    /**
     * Get SiteVO Info based on the given info
     *
     * @param physicAdd String Object
     * @param city String Object
     * @param state short
     * @param postalCode short
     * @return the Object of SiteVO Objects
     */
    public SiteVO getLatLongByPhysicalAddress(String physicAdd, String city, String state, String postalCode) {
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        CloseableHttpResponse response;
        StringWriter writer;
        SiteVO siteVO = null;
        
        try (CloseableHttpClient client = HttpClients.createDefault()) {
            endPoint = new StringBuilder();
            endPoint
                    .append(getPropertyValue("GEOCODE_END_POINT"))
                    .append("/address")
                    .append("?key=").append(getPropertyValue("GEOCODE_API_KEY"))
                    .append("&street=").append(URLEncoder.encode(physicAdd, "UTF-8"))
                    .append("&city=").append(URLEncoder.encode(city, "UTF-8"))
                    .append("&state=").append(URLEncoder.encode(state, "UTF-8"))
                    .append("&postalCode=").append(postalCode);

            response = client.execute(new HttpGet(endPoint.toString()));
            IOUtils.copy(response.getEntity().getContent(), (writer = new StringWriter()));
            
            responseVO = new HttpResponseVO();
            responseVO.setStatusCode(response.getStatusLine().getStatusCode());
            responseVO.setEntity(writer.toString());
            
            if (responseVO.getStatusCode() == 200) {
                siteVO = SiteDomJsonParser.getInstance().populateSiteVOFromGeocodeJson(responseVO.getEntity());
            } else {
                UserManageBean userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
                FacesContext.getCurrentInstance().addMessage("growlMessages", new FacesMessage(userManageBean.getResourceBundleString("growl_create_site_error_message1")));
                Logger.getLogger(SiteClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
            }
        } catch (Exception e) {
            Logger.getLogger(SiteClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
        return siteVO;
    }

    /**
     * Get Site Info based on the given info
     *
     * @param host String Object
     * @param customer, String Object
     * @param siteId, String Object
     * @return List Object of SiteVO Objects
     */
    public List<SiteVO> getSiteByCustomerSite(String host, String customer, String siteId) {
        List<SiteVO> list = null;
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;
        
        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/config/site")
                    .append("?customer=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&siteId=").append(URLEncoder.encode(siteId, "UTF-8"))
                    .append("&SiteCustomer=True");
            
            if ((responseVO = httpGet(CONFIGURATION_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        list = SiteSaxJsonParser.getInstance().populateSiteFromJson(responseVO.getEntity());
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(SiteClient.class.getName()).log(Level.WARNING, "Message from Config Service: {0}", SiteDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(SiteClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(SiteClient.class.getName()).log(Level.WARNING, "Error Message from Config Service: {0}", SiteDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(SiteClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            return list;
        }
        return list;
    }

    /**
     * Get Site Info based on the given info
     *
     * @param host String Object
     * @param customer, String Object
     * @return List Object of SiteVO Objects
     */
    public List<SiteVO> getSiteByCustomer(String host, String customer) {
        List<SiteVO> list = null;
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;
        
        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/config/site")
                    .append("?customer=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&SiteCustomer=True");
            
            if ((responseVO = httpGet(CONFIGURATION_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {

                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        list = SiteSaxJsonParser.getInstance().populateSiteFromJson(responseVO.getEntity());
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(SiteClient.class.getName()).log(Level.WARNING, "Message from Config Service: {0}", SiteDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(SiteClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(SiteClient.class.getName()).log(Level.WARNING, "Error Message from Config Service: {0}", SiteDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(SiteClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            return list;
        }
        return list;
    }
    
    /**
     * Get Site Info based on the given info
     *
     * @param host String Object
     * @param customer, String Object
     * @param siteId
     * @return List Object of SiteVO Objects
     */
    public List<SiteContactVO> getSiteContactsByCustomerSite(String host, String customer, UUID siteId) {
        List<SiteContactVO> list = null;
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;
        
        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/config/site")
                    .append("?customer=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&siteId=").append(siteId)
                    .append("&SiteContacts=True");
            
            if ((responseVO = httpGet(CONFIGURATION_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {

                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        list = SiteSaxJsonParser.getInstance().populateSiteContactsFromJson(responseVO.getEntity());
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(SiteClient.class.getName()).log(Level.WARNING, "Message from Config Service: {0}", SiteDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(SiteClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(SiteClient.class.getName()).log(Level.WARNING, "Error Message from Config Service: {0}", SiteDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(SiteClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            return list;
        }
        return list;
    }


    /**
     * Get Site Info based on the given info
     *
     * @param host String Object
     * @param customer, String Object
     * @return List Object of SiteVO Objects
     */
    public List<SiteVO> getSiteRegionCountryByCustomer(String host, String customer) {
        List<SiteVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;
        
        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/config/site")
                    .append("?customer=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&SiteRegionCountry=True");
            
            if ((responseVO = httpGet(CONFIGURATION_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        list = SiteSaxJsonParser.getInstance().populateSiteFromJson(responseVO.getEntity());
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(SiteClient.class.getName()).log(Level.WARNING, "Message from Config Service: {0}", SiteDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(SiteClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(SiteClient.class.getName()).log(Level.WARNING, "Error Message from Config Service: {0}", SiteDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(SiteClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            return list;
        }
        return list;
    }

    /**
     * Get Site Info based on the given info
     *
     * @param host String Object
     * @param customer, String Object
     * @param region, String Object
     * @param country, String Object
     * @return List Object of SiteVO Objects
     */
    public List<SiteVO> getSiteRegionCountryByCustomerRegionCountry(String host, String customer, String region, String country) {
        List<SiteVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;
        
        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/config/site")
                    .append("?customer=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&region=").append(URLEncoder.encode(region, "UTF-8"))
                    .append("&country=").append(URLEncoder.encode(country, "UTF-8"))
                    .append("&SiteRegionCountry=True");
            
            if ((responseVO = httpGet(CONFIGURATION_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        list = SiteSaxJsonParser.getInstance().populateSiteFromJson(responseVO.getEntity());
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(SiteClient.class.getName()).log(Level.WARNING, "Message from Config Service: {0}", SiteDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(SiteClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(SiteClient.class.getName()).log(Level.WARNING, "Error Message from Config Service: {0}", SiteDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(SiteClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            return list;
        }
        return list;
    }

    /**
     * Create the given Site
     *
     * @param host, String Object
     * @param siteVO, SiteVO Object
     * @return boolean, true if success, else false
     */
    public boolean createSite(String host, SiteVO siteVO) {
        UserManageBean userManageBean;
        HttpResponseVO responseVO;
        
        Logger.getLogger(SiteClient.class.getName()).log(Level.INFO, "SiteDomJsonParser JSON**************: {0}", SiteDomJsonParser.getInstance().getJsonFromSite(siteVO));
        if ((responseVO = httpPost(CONFIGURATION_SERVICE_NAME, host, "/config/site", SiteDomJsonParser.getInstance().getJsonFromSite(siteVO))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
                FacesContext.getCurrentInstance().addMessage("nonAutoGrowlMessages", new FacesMessage(userManageBean.getResourceBundleString("growl_create_site_success_message")));
                Logger.getLogger(SiteClient.class.getName()).log(Level.INFO, "Site successfully created.");
                return true;
            } else {
                Logger.getLogger(SiteClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(SiteClient.class.getName()).log(Level.WARNING, "Error Message from Config Service: {0}", SiteDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        showErrorMsg();

        return false;
    }

    /**
     * Update the given Site
     *
     * @param host, String Object
     * @param siteVO, SiteVO Object
     * @return boolean, true if success, else false
     */
    public boolean updateSite(String host, SiteVO siteVO) {
        UserManageBean userManageBean;
        HttpResponseVO responseVO;
        
        if ((responseVO = httpPut(CONFIGURATION_SERVICE_NAME, host, "/config/site", SiteDomJsonParser.getInstance().getJsonFromSite(siteVO))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
                FacesContext.getCurrentInstance().addMessage("nonAutoGrowlMessages", new FacesMessage(userManageBean.getResourceBundleString("growl_update_site_success_message")));
                Logger.getLogger(SiteClient.class.getName()).log(Level.INFO, "Site successfully updated.");
                return true;
            } else {
                Logger.getLogger(SiteClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(SiteClient.class.getName()).log(Level.WARNING, "Error Message from Config Service: {0}", SiteDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        showErrorMsg();

        return false;
    }

    /**
     * Delete the given Site
     *
     * @param host, String Object
     * @param siteVO, SiteVO Object
     * @return boolean, true if success, else false
     */
    public boolean deleteSite(String host, SiteVO siteVO) {
        HttpResponseVO responseVO;
        
        if ((responseVO = httpDelete(CONFIGURATION_SERVICE_NAME, host, "/config/site", SiteDomJsonParser.getInstance().getJsonFromSite(siteVO))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                FacesContext.getCurrentInstance().addMessage("growlMessages", new FacesMessage("Site successfully deleted."));
                Logger.getLogger(SiteClient.class.getName()).log(Level.INFO, "Site successfully deleted.");
                return true;
            } else {
                Logger.getLogger(SiteClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(SiteClient.class.getName()).log(Level.WARNING, "Error Message from Config Service: {0}", SiteDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        showErrorMsg();

        return false;
    }
}
