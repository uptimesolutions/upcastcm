/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.http.client;

import com.uptime.client.http.client.Client;
import com.uptime.client.utils.vo.HttpResponseVO;
import com.uptime.upcastcm.bean.UserManageBean;
import com.uptime.upcastcm.http.utils.json.dom.AreaDomJsonParser;
import com.uptime.upcastcm.http.utils.json.sax.AreaSaxJsonParser;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import static com.uptime.upcastcm.utils.helperclass.GenericMessage.showErrorMsg;
import com.uptime.upcastcm.utils.vo.AreaConfigScheduleVO;
import com.uptime.upcastcm.utils.vo.AreaVO;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 *
 * @author twilcox
 */
public class AreaClient extends Client {
    private static AreaClient instance = null;

    /**
     * Private Singleton class
     */
    private AreaClient() {
    }

    /**
     * Returns an instance of the class
     *
     * @return AreaClient object
     */
    public static AreaClient getInstance() {
        if (instance == null) {
            instance = new AreaClient();
        }
        return instance;
    }

    /**
     * Get Area Info based on the given info
     *
     * @param host String Object
     * @param customer, String Object
     * @param siteId, String Object
     * @param areaId, String Object
     * @param scheduleId, String Object
     * @param dayOfWeek, byte Object
     * @return List Object of AreaVO Objects
     */
    public List<AreaConfigScheduleVO> getAreaConfigScheduleByPK(String host, String customer, UUID siteId, UUID areaId, UUID scheduleId, byte dayOfWeek) {
        List<AreaConfigScheduleVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;
        
        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/config/area")
                    .append("?customer=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&siteId=").append(siteId)
                    .append("&areaId=").append(areaId)
                    .append("&scheduleId=").append(scheduleId)
                    .append("&dayOfWeek=").append(dayOfWeek);
            
            if ((responseVO = httpGet(CONFIGURATION_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        list = AreaSaxJsonParser.getInstance().populateAreaConfigScheduleFromJson(responseVO.getEntity());
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(AreaClient.class.getName()).log(Level.WARNING, "Message from Config Service: {0}", AreaDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(AreaClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(AreaClient.class.getName()).log(Level.WARNING, "Error Message from Config Service: {0}", AreaDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(AreaClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }

    /**
     * Get Area Info based on the given info
     *
     * @param host String Object
     * @param customer, String Object
     * @param siteId, String Object
     * @param areaId, String Object
     * @param scheduleId, String Object
     * @return List Object of AreaVO Objects
     */
    public List<AreaConfigScheduleVO> getAreaConfigScheduleByCustomerSiteAreaSchedule(String host, String customer, UUID siteId, UUID areaId, UUID scheduleId) {
        List<AreaConfigScheduleVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;
        
        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/config/area")
                    .append("?customer=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&siteId=").append(siteId)
                    .append("&areaId=").append(areaId)
                    .append("&scheduleId=").append(scheduleId);
            
            if ((responseVO = httpGet(CONFIGURATION_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        list = AreaSaxJsonParser.getInstance().populateAreaConfigScheduleFromJson(responseVO.getEntity());
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(AreaClient.class.getName()).log(Level.WARNING, "Message from Config Service: {0}", AreaDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(AreaClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(AreaClient.class.getName()).log(Level.WARNING, "Error Message from Config Service: {0}", AreaDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(AreaClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }

    /**
     * Get Area Info based on the given info
     *
     * @param host String Object
     * @param customer, String Object
     * @param siteId, String Object
     * @param areaId, String Object
     * @return List Object of AreaVO Objects
     */
    public List<AreaConfigScheduleVO> getAreaConfigScheduleByCustomerSiteArea(String host, String customer, UUID siteId, UUID areaId) {
        List<AreaConfigScheduleVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;
        
        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/config/area")
                    .append("?customer=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&siteId=").append(siteId)
                    .append("&areaId=").append(areaId)
                    .append("&areaConfigSchedule=").append(true);
            
            if ((responseVO = httpGet(CONFIGURATION_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {

                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        list = AreaSaxJsonParser.getInstance().populateAreaConfigScheduleFromJson(responseVO.getEntity());
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(AreaClient.class.getName()).log(Level.WARNING, "Message from Config Service: {0}", AreaDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(AreaClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(AreaClient.class.getName()).log(Level.WARNING, "Error Message from Config Service: {0}", AreaDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(AreaClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }

    /**
     * Get Area Info based on the given info
     *
     * @param host String Object
     * @param customer, String Object
     * @param siteId, String Object
     * @param areaId, String Object
     * @return List Object of AreaVO Objects
     */
    public List<AreaVO> getAreaSiteByPK(String host, String customer, UUID siteId, UUID areaId) {
        List<AreaVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;
        
        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/config/area")
                    .append("?customer=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&siteId=").append(siteId)
                    .append("&areaId=").append(areaId)
                    .append("&areaSite=").append(true);
            
            if ((responseVO = httpGet(CONFIGURATION_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        list = AreaSaxJsonParser.getInstance().populateAreaSiteFromJson(responseVO.getEntity());
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(AreaClient.class.getName()).log(Level.WARNING, "Message from Config Service: {0}", AreaDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(AreaClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(AreaClient.class.getName()).log(Level.WARNING, "Error Message from Config Service: {0}", AreaDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(AreaClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }

    /**
     * Get Area Info based on the given info
     *
     * @param host String Object
     * @param customer, String Object
     * @param siteId, String Object
     * @param areaId, String Object
     * @return List Object of AreaVO Objects
     */
    public AreaVO getAreaList(String host, String customer, UUID siteId, UUID areaId) {
        AreaVO areaVO = null;
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;
        
        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/config/area")
                    .append("?customer=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&siteId=").append(siteId)
                    .append("&areaId=").append(areaId)
                    .append("&areaSite=").append(true)
                    .append("&areaConfigSchedule=").append(true);
            
            if ((responseVO = httpGet(CONFIGURATION_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        areaVO = AreaSaxJsonParser.getInstance().populateAreaListFromJson(responseVO.getEntity());
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(AreaClient.class.getName()).log(Level.WARNING, "Message from Config Service: {0}", AreaDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(AreaClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(AreaClient.class.getName()).log(Level.WARNING, "Error Message from Config Service: {0}", AreaDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(AreaClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
        return areaVO;
    }

    /**
     * Get Area Info based on the given info
     *
     * @param host String Object
     * @param customer, String Object
     * @param siteId, String Object
     * @return List Object of AreaVO Objects
     */
    public List<AreaVO> getAreaSiteByCustomerSite(String host, String customer, UUID siteId) {
        List<AreaVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;
        
        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/config/area")
                    .append("?customer=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&siteId=").append(siteId);
            
            if ((responseVO = httpGet(CONFIGURATION_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {

                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        list = AreaSaxJsonParser.getInstance().populateAreaSiteFromJson(responseVO.getEntity());
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(AreaClient.class.getName()).log(Level.WARNING, "Message from Config Service: {0}", AreaDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(AreaClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(AreaClient.class.getName()).log(Level.WARNING, "Error Message from Config Service: {0}", AreaDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(AreaClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }

    public boolean createArea(String host, AreaVO areaVO) {
        UserManageBean userManageBean;
        HttpResponseVO responseVO;
        
        if ((responseVO = httpPost(CONFIGURATION_SERVICE_NAME, host, "/config/area", AreaDomJsonParser.getInstance().getJsonFromArea(areaVO))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
                FacesContext.getCurrentInstance().addMessage("nonAutoGrowlMessages", new FacesMessage(userManageBean.getResourceBundleString("growl_create_area_3")));
                Logger.getLogger(AreaClient.class.getName()).log(Level.INFO, "Area successfully created.");
                return true;
            } else {
                Logger.getLogger(AreaClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(AreaClient.class.getName()).log(Level.WARNING, "Error Message from Config Service: {0}", AreaDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        showErrorMsg();

        return false;
    }
    
    public boolean updateArea(String host, AreaVO areaVO) {
        UserManageBean userManageBean;
        HttpResponseVO responseVO;
        
        if ((responseVO = httpPut(CONFIGURATION_SERVICE_NAME, host, "/config/area", AreaDomJsonParser.getInstance().getJsonFromArea(areaVO))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
                FacesContext.getCurrentInstance().addMessage("nonAutoGrowlMessages", new FacesMessage(userManageBean.getResourceBundleString("growl_update_area_3")));
                Logger.getLogger(AreaClient.class.getName()).log(Level.INFO, "Area successfully updated.");
                return true;
            } else {
                Logger.getLogger(AreaClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(AreaClient.class.getName()).log(Level.WARNING, "Error Message from Config Service: {0}", AreaDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        showErrorMsg();

        return false;
    }
}
