/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.http.client;

import com.uptime.client.http.client.Client;
import com.uptime.client.utils.vo.HttpResponseVO;
import com.uptime.upcastcm.http.utils.json.sax.PointToHwUnitSaxJsonParser;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import com.uptime.upcastcm.utils.vo.PointToHwVO;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;
import static com.uptime.upcastcm.utils.helperclass.GenericMessage.showErrorMsg;
import java.util.UUID;

/**
 *
 * @author kpati
 */
public class PointToHwUnitClient extends Client {
    private static PointToHwUnitClient instance = null;

    /**
     * Private Singleton class
     */
    private PointToHwUnitClient() {
    }

    /**
     * Returns an instance of the class
     *
     * @return PointToHwUnitClient object
     */
    public static PointToHwUnitClient getInstance() {
        if (instance == null) {
            instance = new PointToHwUnitClient();
        }
        return instance;
    }

    /**
     * Get list of PointToHwVO Info based on the given info
     *
     * @param host String Object
     * @param customer, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param pointLocationId, UUID Object
     * @return List Object of HwUnitVO Objects
     */
    public List<PointToHwVO> getPointToHwUnitByPointLocation(String host, String customer, UUID siteId, UUID areaId, UUID assetId, UUID pointLocationId) {
        List<PointToHwVO> pointToHwVOs = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;

        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/config/manageDevice")
                    .append("?customer=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&siteId=").append(siteId)
                    .append("&areaId=").append(areaId)
                    .append("&assetId=").append(assetId)
                    .append("&pointLocationId=").append(pointLocationId);
            
            if ((responseVO = httpGet(CONFIGURATION_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        pointToHwVOs = PointToHwUnitSaxJsonParser.getInstance().populatePointToHwUnitFromJson(responseVO.getEntity());
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(PointToHwUnitClient.class.getName()).log(Level.WARNING, "Message from Config Service: {0}", PointToHwUnitSaxJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(PointToHwUnitClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(PointToHwUnitClient.class.getName()).log(Level.WARNING, "Error Message from Config Service: {0}", PointToHwUnitSaxJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(PointToHwUnitClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            pointToHwVOs = new ArrayList();
        }
        return pointToHwVOs;
    }

}
