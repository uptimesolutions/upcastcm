/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.http.client;

import com.uptime.client.http.client.Client;
import com.uptime.client.utils.vo.HttpResponseVO;
import com.uptime.upcastcm.http.utils.json.dom.EmailDomJsonParser;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import com.uptime.upcastcm.utils.vo.EmailVO;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Mohd Juned Alam
 */
public class EmailClient extends Client implements Serializable {
    private static EmailClient instance = null;

    private EmailClient() {
    }

    /**
     * Returns an instance of the class
     *
     * @return EmailClient object
     */
    public static EmailClient getInstance() {
        if (instance == null) {
            instance = new EmailClient();
        }
        return instance;
    }

    public boolean sendEmail(String host, EmailVO emailVO) {
        HttpResponseVO responseVO;
        
        if ((responseVO = httpPost(EMAIL_SERVICE_NAME, host, "/email", EmailDomJsonParser.getInstance().getEmailJsonData(emailVO))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                Logger.getLogger(EmailClient.class.getName()).log(Level.INFO, "Email sent successfully ");
                return true;
            } else {
                Logger.getLogger(EmailClient.class.getName()).log(Level.WARNING, "Error Message from Email Service : {0}", responseVO.getEntity());
                Logger.getLogger(EmailClient.class.getName()).log(Level.WARNING, "errMsg : {0}", EmailDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        return false;
    }

}