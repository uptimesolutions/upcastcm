/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.http.client;

import com.uptime.client.http.client.Client;
import com.uptime.client.utils.vo.HttpResponseVO;
import com.uptime.upcastcm.bean.UserManageBean;
import com.uptime.upcastcm.http.utils.json.dom.AssetDomJsonParser;
import com.uptime.upcastcm.http.utils.json.dom.FaultFrequenciesDomJsonParser;
import com.uptime.upcastcm.http.utils.json.sax.AssetSaxJsonParser;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import static com.uptime.upcastcm.utils.helperclass.GenericMessage.setNonAutoGrowlMsg;
import static com.uptime.upcastcm.utils.helperclass.GenericMessage.showErrorMsg;
import com.uptime.upcastcm.utils.vo.AssetTypesVO;
import com.uptime.upcastcm.utils.vo.AssetVO;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 *
 * @author madhavi
 */
public class AssetClient extends Client {
    private static AssetClient instance = null;

    /**
     * Private Singleton class
     */
    private AssetClient() {
    }

    /**
     * Returns an instance of the class
     *
     * @return AssetClient object
     */
    public static AssetClient getInstance() {
        if (instance == null) {
            instance = new AssetClient();
        }
        return instance;
    }

    /**
     * Get a List of AssetSiteArea Objects by the given params
     *
     * @param host, String Object
     * @param customer, String Object
     * @param siteId, String Object
     * @return List of AssetVO Objects
     */
    public List<AssetVO> getAssetSiteAreaByCustomerSite(String host, String customer, String siteId) {
        List<AssetVO> assetList = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;
        
        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/config/asset")
                    .append("?customer=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&siteId=").append(siteId)
                    .append("&assetSiteArea=").append(true);
            
            if ((responseVO = httpGet(CONFIGURATION_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        assetList = AssetSaxJsonParser.getInstance().populateAssetVOFromJson(responseVO.getEntity());
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(AssetClient.class.getName()).log(Level.WARNING, "Message from Config Service: {0}", AssetDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                     case 400:
                        serviceIssue = false;
                        Logger.getLogger(AssetClient.class.getName()).log(Level.WARNING, "Message from Config Service: {0}", AssetDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(AssetClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(AssetClient.class.getName()).log(Level.WARNING, "Error Message from Config Service: {0}", AssetDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(AreaClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            assetList = new ArrayList();
        }
        return assetList;
    }

    /**
     * Validate Al_set_id for all the points of given asset.
     *
     * @param host, String Object
     * @param customer, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param validateAlSet, boolean
     * @return boolean
     */
    public boolean validateAlSetforPoints(String host, String customer, UUID siteId, UUID areaId, UUID assetId, boolean validateAlSet) {
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;

        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/config/asset")
                    .append("?customer=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&siteId=").append(siteId)
                    .append("&areaId=").append(areaId)
                    .append("&assetId=").append(assetId)
                    .append("&validateAlSet=").append(validateAlSet);
            
            if ((responseVO = httpGet(CONFIGURATION_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        return responseVO.getEntity().equalsIgnoreCase("true");
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(AssetClient.class.getName()).log(Level.WARNING, "Message from Config Service: {0}", AssetDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(AssetClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(AssetClient.class.getName()).log(Level.WARNING, "Error Message from Config Service: {0}", AssetDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(AreaClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
        return false;
    }

    /**
     * Get a List of AssetSiteArea Objects by the given params
     *
     * @param host, String Object
     * @param customer, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @return List of AssetVO Objects
     */
    public List<AssetVO> getAssetSiteAreaByCustomerSiteArea(String host, String customer, UUID siteId, UUID areaId) {
        List<AssetVO> assetList = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;
        
        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/config/asset")
                    .append("?customer=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&siteId=").append(siteId)
                    .append("&areaId=").append(areaId);
            
            if ((responseVO = httpGet(CONFIGURATION_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        assetList = AssetSaxJsonParser.getInstance().populateAssetVOFromJson(responseVO.getEntity());
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(AssetClient.class.getName()).log(Level.WARNING, "Message from Config Service: {0}", AssetDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                     case 400:
                        serviceIssue = false;
                        Logger.getLogger(AssetClient.class.getName()).log(Level.WARNING, "Message from Config Service: {0}", AssetDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(AssetClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(AssetClient.class.getName()).log(Level.WARNING, "Error Message from Config Service: {0}", AssetDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(AreaClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            assetList = new ArrayList();
        }
        return assetList;
    }

    /**
     * Get a List of AssetSiteArea Objects by the given params
     *
     * @param host, String Object
     * @param customer, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @return List of AssetVO Objects
     */
    public List<AssetVO> getAssetSiteAreaByCustomerSiteArea(String host, String customer, String siteId, String areaId) {
        List<AssetVO> assetList = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;
        
        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/config/asset")
                    .append("?customer=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&siteId=").append(siteId)
                    .append("&areaId=").append(areaId);
            
            if ((responseVO = httpGet(CONFIGURATION_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        assetList = AssetSaxJsonParser.getInstance().populateAssetVOFromJson(responseVO.getEntity());
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(AssetClient.class.getName()).log(Level.WARNING, "Message from Config Service: {0}", AssetDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(AssetClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(AssetClient.class.getName()).log(Level.WARNING, "Error Message from Config Service: {0}", AssetDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(AreaClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            assetList = new ArrayList();
        }
        return assetList;
    }

    /**
     * Get a List of AssetSiteArea Objects by the given params
     *
     * @param host, String Object
     * @param customer, String Object
     * @param siteId, String Object
     * @param areaId, String Object
     * @param assetId, String Object
     * @return List of AssetVO Objects
     */
    public List<AssetVO> getAssetSiteAreaByPK(String host, String customer, String siteId, String areaId, String assetId) {
        List<AssetVO> assetList = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;
        
        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/config/asset")
                    .append("?customer=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&siteId=").append(siteId)
                    .append("&areaId=").append(areaId)
                    .append("&assetId=").append(assetId);
            
            if ((responseVO = httpGet(CONFIGURATION_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        assetList = AssetSaxJsonParser.getInstance().populateAssetVOFromJson(responseVO.getEntity());
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(AssetClient.class.getName()).log(Level.WARNING, "Message from Config Service: {0}", AssetDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(AssetClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(AssetClient.class.getName()).log(Level.WARNING, "Error Message from Config Service: {0}", AssetDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(AreaClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            assetList = new ArrayList();
        }
        return assetList;
    }

    /**
     * Create a new Asset by inserting into multiple tables
     *
     * @param host String Object
     * @param assetVO AssetVO Object
     * @return boolean
     */
    public boolean createAsset(String host, AssetVO assetVO) {
        UserManageBean userManageBean;
        HttpResponseVO responseVO;
        
        if ((responseVO = httpPost(CONFIGURATION_SERVICE_NAME, host, "/config/asset", AssetDomJsonParser.getInstance().getJsonFromAssetVO(assetVO))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
                Logger.getLogger(AssetClient.class.getName()).log(Level.INFO, "Asset created successfully.");
                return true;
            } else {
                Logger.getLogger(AssetClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(AssetClient.class.getName()).log(Level.WARNING, "Error Message from Config Service: {0}", AssetDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        showErrorMsg();

        return false;
    }

    /**
     * Update tables dealing with the given Asset
     *
     * @param host String Object
     * @param assetVO AssetVO Object
     * @return boolean
     */
    public boolean updateAsset(String host, AssetVO assetVO) {
        UserManageBean userManageBean;
        HttpResponseVO responseVO;
        
        if ((responseVO = httpPut(CONFIGURATION_SERVICE_NAME, host, "/config/asset", AssetDomJsonParser.getInstance().getJsonFromAssetVO(assetVO))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
                FacesContext.getCurrentInstance().addMessage("nonAutoGrowlMessages", new FacesMessage(userManageBean.getResourceBundleString("growl_update_asset_1")));
                Logger.getLogger(AssetClient.class.getName()).log(Level.INFO, "Asset updated successfully.");
                return true;
            } else {
                Logger.getLogger(AssetClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(AssetClient.class.getName()).log(Level.WARNING, "Error Message from Config Service: {0}", AssetDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        showErrorMsg();

        return false;
    }

    /**
     * Delete tables dealing with the given Asset
     *
     * @param host String Object
     * @param assetVO AssetVO Object
     * @return boolean
     */
    public boolean deleteAsset(String host, AssetVO assetVO) throws IllegalArgumentException {
        UserManageBean userManageBean;
        HttpResponseVO responseVO;

        if ((responseVO = httpDelete(CONFIGURATION_SERVICE_NAME, host, "/config/asset", AssetDomJsonParser.getInstance().getJsonFromAssetVO(assetVO))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
                FacesContext.getCurrentInstance().addMessage("nonAutoGrowlMessages", new FacesMessage(userManageBean.getResourceBundleString("growl_delete_asset_1")));
                Logger.getLogger(AssetClient.class.getName()).log(Level.INFO, "Asset deleted successfully.");
                return true;
            } else {
                Logger.getLogger(AssetClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(AssetClient.class.getName()).log(Level.WARNING, "Error Message from Config Service: {0}", AssetDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        showErrorMsg();

        return false;
    }
    
     public List<AssetTypesVO> getGlobalAssetTypesByCustomer(String host, String customerAccount) {
     List<AssetTypesVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;

        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/presets/globalAssetType")
                    .append("?customerAccount=").append(URLEncoder.encode(customerAccount, "UTF-8"));
            
            
            if ((responseVO = httpGet(PRESET_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        list = AssetSaxJsonParser.getInstance().populateAssetTypesFromJson(responseVO.getEntity());
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(AreaClient.class.getName()).log(Level.WARNING, "Message from Presets Service: {0}", FaultFrequenciesDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(AreaClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(AreaClient.class.getName()).log(Level.WARNING, "Error Message from Presets Service: {0}", FaultFrequenciesDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                setNonAutoGrowlMsg("growl_service_error_client_message");
            }
        } catch (Exception e) {
            Logger.getLogger(AreaClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;   
    }
    
}
