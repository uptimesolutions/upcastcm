/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.http.client;

import com.uptime.client.http.client.Client;
import com.uptime.client.utils.vo.HttpResponseVO;
import com.uptime.upcastcm.bean.UserManageBean;
import com.uptime.upcastcm.http.utils.json.dom.WorkOrderDomJsonParser;
import com.uptime.upcastcm.http.utils.json.sax.WorkOrderSaxJsonParser;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import static com.uptime.upcastcm.utils.helperclass.GenericMessage.showErrorMsg;
import static com.uptime.upcastcm.utils.helperclass.GenericMessage.showGrowlMsg;
import com.uptime.upcastcm.utils.vo.WorkOrderVO;
import java.net.URLEncoder;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 *
 * @author twilcox
 */
public class WorkOrderClient extends Client {
    private static WorkOrderClient instance = null;

    /**
     * Private Singleton class
     */
    private WorkOrderClient() {
    }

    /**
     * Returns an instance of the class
     *
     * @return WorkOrderClient object
     */
    public static WorkOrderClient getInstance() {
        if (instance == null) {
            instance = new WorkOrderClient();
        }
        return instance;
    }
    
    /**
     * Get OpenWorkOrders items based on the given info
     * @param host, String Object
     * @param customer, String Object
     * @param siteId, UUID Object
     * @return List Object of WorkOrderVO Objects
     */
    public List<WorkOrderVO> getOpenWorkOrdersByCustomerSite(String host, String customer, UUID siteId) {
        List<WorkOrderVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        
        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/workorder")
                    .append("?customer=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&siteId=").append(siteId);
            
            if ((responseVO = httpGet(WORK_ORDER_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        list = WorkOrderSaxJsonParser.getInstance().populateWorkOrderVOFromJson(responseVO.getEntity());
                        break;
                    case 404:
                        Logger.getLogger(WorkOrderClient.class.getName()).log(Level.WARNING, "Message from WorkOrder Service: {0}", WorkOrderDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(WorkOrderClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(WorkOrderClient.class.getName()).log(Level.WARNING, "Error Message from WorkOrder Service: {0}", WorkOrderDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
        } catch (Exception e) {
            Logger.getLogger(WorkOrderClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }
    
    
    /**
     * Get OpenWorkOrders items based on the given info
     * @param host, String Object
     * @param customer, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @return List Object of WorkOrderVO Objects
     */
    public List<WorkOrderVO> getOpenWorkOrdersByCustomerSiteArea(String host, String customer, UUID siteId, UUID areaId) {
        List<WorkOrderVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        
        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/workorder")
                    .append("?customer=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&siteId=").append(siteId)
                    .append("&areaId=").append(areaId);
            
            if ((responseVO = httpGet(WORK_ORDER_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        list = WorkOrderSaxJsonParser.getInstance().populateWorkOrderVOFromJson(responseVO.getEntity());
                        break;
                    case 404:
                        Logger.getLogger(WorkOrderClient.class.getName()).log(Level.WARNING, "Message from WorkOrder Service: {0}", WorkOrderDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(WorkOrderClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(WorkOrderClient.class.getName()).log(Level.WARNING, "Error Message from WorkOrder Service: {0}", WorkOrderDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
        } catch (Exception e) {
            Logger.getLogger(WorkOrderClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }
    
    /**
     * Get OpenWorkOrders items based on the given info
     * @param host, String Object
     * @param customer, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @return List Object of WorkOrderVO Objects
     */
    public List<WorkOrderVO> getOpenWorkOrdersByCustomerSiteAreaAsset(String host, String customer, UUID siteId, UUID areaId, UUID assetId) {
        List<WorkOrderVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        
        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/workorder")
                    .append("?customer=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&siteId=").append(siteId)
                    .append("&areaId=").append(areaId)
                    .append("&assetId=").append(assetId);
            
            if ((responseVO = httpGet(WORK_ORDER_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        list = WorkOrderSaxJsonParser.getInstance().populateWorkOrderVOFromJson(responseVO.getEntity());
                        break;
                    case 404:
                        Logger.getLogger(WorkOrderClient.class.getName()).log(Level.WARNING, "Message from WorkOrder Service: {0}", WorkOrderDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    case 503:
                        Logger.getLogger(WorkOrderClient.class.getName()).log(Level.WARNING, "Message from WorkOrder Service: {0}", WorkOrderDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(WorkOrderClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(WorkOrderClient.class.getName()).log(Level.WARNING, "Error Message from WorkOrder Service: {0}", WorkOrderDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
        } catch (Exception e) {
            Logger.getLogger(WorkOrderClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }
    
    /**
     * Get WorkOrderCounts items based on the given info
     * @param host, String Object
     * @param customer, String Object
     * @param siteId, UUID Object
     * @return List Object of WorkOrderVO Objects
     */
    public List<WorkOrderVO> getWorkOrderCountsByCustomerSite(String host, String customer, UUID siteId) {
        List<WorkOrderVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;
        
        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/workorder")
                    .append("?customer=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&siteId=").append(siteId)
                    .append("&counts=true");
            
            if ((responseVO = httpGet(WORK_ORDER_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        list = WorkOrderSaxJsonParser.getInstance().populateWorkOrderVOFromJson(responseVO.getEntity());
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(WorkOrderClient.class.getName()).log(Level.WARNING, "Message from WorkOrder Service: {0}", WorkOrderDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(WorkOrderClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(WorkOrderClient.class.getName()).log(Level.WARNING, "Error Message from WorkOrder Service: {0}", WorkOrderDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(WorkOrderClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }
    
    /**
     * Get WorkOrderTrends items based on the given info
     * @param host, String Object
     * @param customer, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param priority, byte
     * @param createdDate, Instant Object
     * @return List Object of WorkOrderVO Objects
     */
    public List<WorkOrderVO> getWorkOrderTrendsByCustomerSiteAreaAssetPriorityDate(String host, String customer, UUID siteId, UUID areaId, UUID assetId, byte priority, Instant createdDate) {
        List<WorkOrderVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;
        
        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/workorder")
                    .append("?customer=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&siteId=").append(siteId)
                    .append("&areaId=").append(areaId)
                    .append("&assetId=").append(assetId)
                    .append("&priority=").append(priority)
                    .append("&createdDate=").append(URLEncoder.encode(createdDate.toString(), "UTF-8"))
                    .append("&workOrderTrends=true");
            
            if ((responseVO = httpGet(WORK_ORDER_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        list = WorkOrderSaxJsonParser.getInstance().populateWorkOrderVOFromJson(responseVO.getEntity());
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(WorkOrderClient.class.getName()).log(Level.WARNING, "Message from WorkOrder Service: {0}", WorkOrderDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                        
                    case 503:
                        serviceIssue = false;
                        Logger.getLogger(WorkOrderClient.class.getName()).log(Level.WARNING, "Message from WorkOrder Service: {0}", WorkOrderDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(WorkOrderClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(WorkOrderClient.class.getName()).log(Level.WARNING, "Error Message from WorkOrder Service: {0}", WorkOrderDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(WorkOrderClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }
    
    /**
     * Get ClosedWorkOrders items based on the given info
     * @param host, String Object
     * @param customer, String Object
     * @param siteId, UUID Object
     * @param year, short
     * @return List Object of WorkOrderVO Objects
     */
    public List<WorkOrderVO> getClosedWorkOrdersByCustomerSiteYear(String host, String customer, UUID siteId, short year) {
        List<WorkOrderVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;
        
        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/workorder")
                    .append("?customer=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&siteId=").append(siteId)
                    .append("&openedYear=").append(year);
            
            if ((responseVO = httpGet(WORK_ORDER_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        list = WorkOrderSaxJsonParser.getInstance().populateWorkOrderVOFromJson(responseVO.getEntity());
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(WorkOrderClient.class.getName()).log(Level.WARNING, "Message from WorkOrder Service: {0}", WorkOrderDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(WorkOrderClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(WorkOrderClient.class.getName()).log(Level.WARNING, "Error Message from WorkOrder Service: {0}", WorkOrderDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            
            if (serviceIssue) {
                showErrorMsg();
            }
        } catch (Exception e) {
            Logger.getLogger(WorkOrderClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }

    public boolean closeWorkOrder(String host, WorkOrderVO workOrderVO) {
        HttpResponseVO responseVO;
        
        if ((responseVO = httpPost(WORK_ORDER_SERVICE_NAME, host, "/workorder", WorkOrderDomJsonParser.getInstance().getJsonFromWorkOrder(workOrderVO))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                showGrowlMsg("growl_work_order_closed_successfully");
                Logger.getLogger(WorkOrderClient.class.getName()).log(Level.INFO, "CloseWorkOrders successfully created.");
                return true;
            } else {
                Logger.getLogger(WorkOrderClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(WorkOrderClient.class.getName()).log(Level.WARNING, "Error Message from Work Order Service: {0}", WorkOrderDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        showErrorMsg();
        
        return false;
    }
    
    
    /**
     * Create a new work orders by inserting into open work orders table
     *
     * @param host String Object
     * @param workOrderVO WorkOrderVO Object
     * @return boolean
     */
    public boolean createOpenWorkOrder(String host, WorkOrderVO workOrderVO) {
        HttpResponseVO responseVO;
        
        if ((responseVO = httpPost(WORK_ORDER_SERVICE_NAME, host, "/workorder", WorkOrderDomJsonParser.getInstance().getJsonFromWorkOrder(workOrderVO))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {               
                Logger.getLogger(WorkOrderClient.class.getName()).log(Level.INFO, "Work Order created successfully.");
                return true;
            } else {
                Logger.getLogger(WorkOrderClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(WorkOrderClient.class.getName()).log(Level.WARNING, "Error Message from Work Order Service: {0}", WorkOrderDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        showErrorMsg();

        return false;
    }
    
    /**
     * Create a new work orders by inserting into open work orders table
     *
     * @param host String Object
     * @param workOrderVO WorkOrderVO Object
     * @return boolean
     */
    public boolean updateWorkOrder(String host, WorkOrderVO workOrderVO) {
        UserManageBean userManageBean;
        HttpResponseVO responseVO;
        
        if ((responseVO = httpPut(WORK_ORDER_SERVICE_NAME, host, "/workorder", WorkOrderDomJsonParser.getInstance().getJsonFromWorkOrder(workOrderVO))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
                FacesContext.getCurrentInstance().addMessage("nonAutoGrowlMessages", new FacesMessage(userManageBean.getResourceBundleString("growl_update_work_order")));
                Logger.getLogger(WorkOrderClient.class.getName()).log(Level.INFO, "Work Order updated successfully.");
                return true;
            } else {
                Logger.getLogger(WorkOrderClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(WorkOrderClient.class.getName()).log(Level.WARNING, "Error Message from Work Order Service: {0}", WorkOrderDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        showErrorMsg();

        return false;
    }
}
