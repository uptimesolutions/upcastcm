/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.upcastcm.http.client;

import com.uptime.client.http.client.Client;
import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.client.utils.vo.HttpResponseVO;
import com.uptime.upcastcm.http.utils.json.dom.FaultFrequenciesDomJsonParser;
import com.uptime.upcastcm.http.utils.json.sax.FaultFrequenciesSaxJsonParser;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import static com.uptime.upcastcm.utils.helperclass.GenericMessage.failedSuccessUploads;
import static com.uptime.upcastcm.utils.helperclass.GenericMessage.setNonAutoGrowlMsg;
import com.uptime.upcastcm.utils.vo.BearingCatalogVO;
import com.uptime.upcastcm.utils.vo.FaultFrequenciesVO;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Joseph
 */
public class FaultFrequenciesClient extends Client {
    private static FaultFrequenciesClient instance = null;

    /**
     * Private Singleton class
     */
    private FaultFrequenciesClient() {
    }

    /**
     * Returns an instance of the class
     *
     * @return FaultFrequenciesClient object
     */
    public static FaultFrequenciesClient getInstance() {
        if (instance == null) {
            instance = new FaultFrequenciesClient();
        }
        return instance;
    }

    /**
     * Get GlobalFaultFrequencies Info based on the given info
     *
     * @param host String Object
     * @param customer String Object
     * @return List Object of GlobalFaultFrequenciesVO Objects
     */
    public List<FaultFrequenciesVO> getGlobalFaultFrequenciesByCustomer(String host, String customer) {
        List<FaultFrequenciesVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;

        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/presets/globalFaultFrequency")
                    .append("?customerAccount=").append(URLEncoder.encode(customer, "UTF-8"));
            
            if ((responseVO = httpGet(PRESET_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        list = FaultFrequenciesSaxJsonParser.getInstance().populateFaultFrequenciesFromJson(responseVO.getEntity());
                        list.forEach(l -> l.setFfType("global"));
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.WARNING, "Message from Presets Service: {0}", FaultFrequenciesDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.WARNING, "Error Message from Presets Service: {0}", FaultFrequenciesDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                setNonAutoGrowlMsg("growl_service_error_client_message");
            }
        } catch (Exception e) {
            Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }

    /**
     * Get GlobalFaultFrequencies Info based on the given info
     *
     * @param host String Object
     * @param customer String Object
     * @param setId
     * @param id
     * @return List Object of GlobalFaultFrequenciesVO Objects
     */
    public List<FaultFrequenciesVO> getGlobalFaultFrequenciesByCustomerSetIdAndId(String host, String customer, UUID setId, UUID id) {
        List<FaultFrequenciesVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;

        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/presets/globalFaultFrequency")
                    .append("?customerAccount=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&ffSetId=").append(setId)
                    .append("&ffId=").append(id);
            
            if ((responseVO = httpGet(PRESET_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        list = FaultFrequenciesSaxJsonParser.getInstance().populateFaultFrequenciesFromJson(responseVO.getEntity());
                        list.forEach(l -> l.setFfType("global"));
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.WARNING, "Message from Presets Service: {0}", FaultFrequenciesDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.WARNING, "Error Message from Presets Service: {0}", FaultFrequenciesDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                setNonAutoGrowlMsg("growl_service_error_client_message");
            }
        } catch (Exception e) {
            Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }

    /**
     * Get GlobalFaultFrequencies Info based on the given info
     *
     * @param host String Object
     * @param customer String Object
     * @param setId
     * @return List Object of GlobalFaultFrequenciesVO Objects
     */
    public List<FaultFrequenciesVO> findByCustomerSetId(String host, String customer, UUID setId) {
        List<FaultFrequenciesVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;

        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/presets/globalFaultFrequency")
                    .append("?customerAccount=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&ffSetId=").append(setId);
            
            if ((responseVO = httpGet(PRESET_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        list = FaultFrequenciesSaxJsonParser.getInstance().populateFaultFrequenciesFromJson(responseVO.getEntity());
                        list.forEach(l -> l.setFfType("global"));
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.WARNING, "Message from Presets Service: {0}", FaultFrequenciesDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.WARNING, "Error Message from Presets Service: {0}", FaultFrequenciesDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                setNonAutoGrowlMsg("growl_service_error_client_message");
            }
        } catch (Exception e) {
            Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }

    /**
     * Get SiteFaultFrequencies Info based on the given info
     *
     * @param host String Object
     * @param customer String Object
     * @param siteId
     * @return List Object of FaultFrequenciesVO Objects
     */
    public List<FaultFrequenciesVO> getSiteFaultFrequenciesByCustomerAndSiteId(String host, String customer, UUID siteId) {
        List<FaultFrequenciesVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;

        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/presets/siteFaultFrequency")
                    .append("?customerAccount=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&siteId=").append(siteId);
            
            if ((responseVO = httpGet(PRESET_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        list = FaultFrequenciesSaxJsonParser.getInstance().populateFaultFrequenciesFromJson(responseVO.getEntity());
                        list.forEach(l -> l.setFfType("site"));
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.WARNING, "Message from Presets Service: {0}", FaultFrequenciesDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.WARNING, "Error Message from Presets Service: {0}", FaultFrequenciesDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                setNonAutoGrowlMsg("growl_service_error_client_message");
            }
        } catch (Exception e) {
            Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }

    /**
     * Get FaultFrequenciesVO Info based on the given info
     *
     * @param host String Object
     * @param customer String Object
     * @param siteId
     * @param setId
     * @param id
     * @return List Object of FaultFrequenciesVO Objects
     */
    public List<FaultFrequenciesVO> getSiteFaultFrequenciesByPK(String host, String customer, UUID siteId, UUID setId, UUID id) {
        List<FaultFrequenciesVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;

        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/presets/siteFaultFrequency")
                    .append("?customerAccount=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&siteId=").append(siteId)
                    .append("&ffSetId=").append(setId)
                    .append("&ffId=").append(id);
            
            if ((responseVO = httpGet(PRESET_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        list = FaultFrequenciesSaxJsonParser.getInstance().populateFaultFrequenciesFromJson(responseVO.getEntity());
                        list.forEach(l -> l.setFfType("site"));
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.WARNING, "Message from Presets Service: {0}", FaultFrequenciesDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.WARNING, "Error Message from Presets Service: {0}", FaultFrequenciesDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                setNonAutoGrowlMsg("growl_service_error_client_message");
            }
        } catch (Exception e) {
            Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }

    /**
     * Get FaultFrequenciesVO Info based on the given info
     *
     * @param host String Object
     * @param customer String Object
     * @param siteId
     * @param setId
     * @return List Object of FaultFrequenciesVO Objects
     */
    public List<FaultFrequenciesVO> findByCustomerSiteSetId(String host, String customer, UUID siteId, UUID setId) {
        List<FaultFrequenciesVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;

        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/presets/siteFaultFrequency")
                    .append("?customerAccount=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&siteId=").append(siteId)
                    .append("&ffSetId=").append(setId);
            
            if ((responseVO = httpGet(PRESET_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        list = FaultFrequenciesSaxJsonParser.getInstance().populateFaultFrequenciesFromJson(responseVO.getEntity());
                        list.forEach(l -> l.setFfType("site"));
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.WARNING, "Message from Presets Service: {0}", FaultFrequenciesDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.WARNING, "Error Message from Presets Service: {0}", FaultFrequenciesDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                setNonAutoGrowlMsg("growl_service_error_client_message");
            }
        } catch (Exception e) {
            Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }

    /**
     * Create the given GlobalFaultFrequencies
     *
     * @param host, String Object
     * @param faultFrequenciesVOList, List of FaultFrequenciesVO Object
     * @return boolean, true if success, else false
     */
    public boolean createGlobalFaultFrequencies(String host, List<FaultFrequenciesVO> faultFrequenciesVOList) {
        HttpResponseVO responseVO;

        if ((responseVO = httpPost(PRESET_SERVICE_NAME, host, "/presets/globalFaultFrequency", FaultFrequenciesDomJsonParser.getInstance().getJsonFromFaultFrequencies(faultFrequenciesVOList))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.INFO, "Global Fault Frequencies successfully created.");
                return true;
            } else {
                Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.WARNING, "Error Message from Global Fault Frequencies Service: {0}", FaultFrequenciesDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        setNonAutoGrowlMsg("growl_service_error_client_message");
        
        return false;
    }

    /**
     * Create the given GlobalFaultFrequencies
     *
     * @param host, String Object
     * @param faultFrequenciesVOList
     * @return boolean, true if success, else false
     */
    public boolean updateGlobalFaultFrequencies(String host, List<FaultFrequenciesVO> faultFrequenciesVOList) {
        HttpResponseVO responseVO;

        if ((responseVO = httpPut(PRESET_SERVICE_NAME, host, "/presets/globalFaultFrequency", FaultFrequenciesDomJsonParser.getInstance().getJsonFromFaultFrequencies(faultFrequenciesVOList))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.INFO, "Global Fault Frequencies successfully created.");
                return true;
            } else {
                Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.WARNING, "Error Message from Global Fault Frequencies Service: {0}", FaultFrequenciesDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        setNonAutoGrowlMsg("growl_service_error_client_message");
        
        return false;
    }

    /**
     * Delete the given GlobalFaultFrequencies
     *
     * @param host, String Object
     * @param faultFrequenciesVOList
     * @return boolean, true if success, else false
     */
    public boolean deleteGlobalFaultFrequencies(String host, List<FaultFrequenciesVO> faultFrequenciesVOList) {
        HttpResponseVO responseVO;

        if ((responseVO = httpDelete(PRESET_SERVICE_NAME, host, "/presets/globalFaultFrequency", FaultFrequenciesDomJsonParser.getInstance().getJsonFromFaultFrequencies(faultFrequenciesVOList))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.INFO, "Global Fault Frequencies successfully deleted.");
                return true;
            } else {
                Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.WARNING, "Error Message from Global Fault Frequencies Service: {0}", FaultFrequenciesDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        setNonAutoGrowlMsg("growl_service_error_client_message");
        
        return false;
    }

    /**
     * Create the given SiteFaultFrequencies
     *
     * @param host, String Object
     * @param faultFrequenciesVOList, List of FaultFrequenciesVO Object
     * @return boolean, true if success, else false
     */
    public boolean createSiteFaultFrequencies(String host, List<FaultFrequenciesVO> faultFrequenciesVOList) {
        HttpResponseVO responseVO;

        if ((responseVO = httpPost(PRESET_SERVICE_NAME, host, "/presets/siteFaultFrequency", FaultFrequenciesDomJsonParser.getInstance().getJsonFromFaultFrequencies(faultFrequenciesVOList))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.INFO, "Site Fault Frequencies successfully created.");
                return true;
            } else {
                Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.WARNING, "Error Message from Site Fault Frequencies Service: {0}", FaultFrequenciesDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        setNonAutoGrowlMsg("growl_service_error_client_message");
        
        return false;
    }

    /**
     * Create multipule faultFrequencies
     *
     * @param faultFrequenciesVOList, List Object of FaultFrequenciesVO Objects
     * @param errorCount, int
     */
    public void createSiteFaultFrequencies(List<FaultFrequenciesVO> faultFrequenciesVOList, int errorCount, int validationFailedCount) {
        HttpResponseVO responseVO;
        int successCount = 0, existsCount = 0;
        List<FaultFrequenciesVO> ffVOList;
        
        if (faultFrequenciesVOList != null && !faultFrequenciesVOList.isEmpty())  {
            for (FaultFrequenciesVO faultFrequenciesVO : faultFrequenciesVOList) {
                ffVOList= new ArrayList();
                ffVOList.add(faultFrequenciesVO);
                
                if ((responseVO = httpPost(PRESET_SERVICE_NAME,  ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), "/presets/siteFaultFrequency", FaultFrequenciesDomJsonParser.getInstance().getJsonFromFaultFrequencies(ffVOList))) != null && responseVO.isHostFound()) {
                    switch (responseVO.getStatusCode()) {
                        case 200:
                            Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.INFO, "Site FaultFrequencies successfully created.");
                            successCount++;
                            break;
                        case 400:
                            Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.INFO, "Site Fault Frequencies already exist by the given PK.");
                            existsCount++;
                            break;
                        default:
                            Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                            Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.WARNING, "Error Message from Site Fault Frequencies Service: {0}", FaultFrequenciesDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                            errorCount++;
                            break;
                    }
                }
            }
        }
        
        failedSuccessUploads(successCount, existsCount, errorCount, validationFailedCount);
    }

    /**
     * Update the given SiteFaultFrequencies
     *
     * @param host, String Object
     * @param faultFrequenciesVOList
     * @return boolean, true if success, else false
     */
    public boolean updateSiteFaultFrequencies(String host, List<FaultFrequenciesVO> faultFrequenciesVOList) {
        HttpResponseVO responseVO;

        if ((responseVO = httpPut(PRESET_SERVICE_NAME, host, "/presets/siteFaultFrequency", FaultFrequenciesDomJsonParser.getInstance().getJsonFromFaultFrequencies(faultFrequenciesVOList))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.INFO, "Site Fault Frequencies successfully updated.");
                return true;
            } else {
                Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.WARNING, "Error Message from Site Fault Frequencies Service: {0}", FaultFrequenciesDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        setNonAutoGrowlMsg("growl_service_error_client_message");
        
        return false;
    }

    /**
     * Delete the given SiteFaultFrequencies
     *
     * @param host, String Object
     * @param faultFrequenciesVOList
     * @return boolean, true if success, else false
     */
    public boolean deleteSiteFaultFrequencies(String host, List<FaultFrequenciesVO> faultFrequenciesVOList) {
        HttpResponseVO responseVO;

        if ((responseVO = httpDelete(PRESET_SERVICE_NAME, host, "/presets/siteFaultFrequency", FaultFrequenciesDomJsonParser.getInstance().getJsonFromFaultFrequencies(faultFrequenciesVOList))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.INFO, "Site Fault Frequencies successfully deleted.");
                return true;
            } else {
                Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.WARNING, "Error Message from Site Fault Frequencies Service: {0}", FaultFrequenciesDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        setNonAutoGrowlMsg("growl_service_error_client_message");
        
        return false;
    }
    
    /**
     * Get SiteFaultFrequenciesFavorites Info based on the given info
     *
     * @param host String Object
     * @param customer String Object
     * @param siteId
     * @return List Object of FaultFrequenciesVO Objects
     */
    public List<FaultFrequenciesVO> getSiteFaultFrequenciesFavoritesByCustomerAndSiteId(String host, String customer, UUID siteId) {
        List<FaultFrequenciesVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;

        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/presets/siteFFSetFavorites")
                    .append("?customerAccount=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&siteId=").append(siteId);
            
            if ((responseVO = httpGet(PRESET_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        list = FaultFrequenciesSaxJsonParser.getInstance().populateFaultFrequenciesFromJson(responseVO.getEntity());
                        list.forEach(l -> l.setFfType("site"));
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.WARNING, "Message from Presets Service: {0}", FaultFrequenciesDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.WARNING, "Error Message from Presets Service: {0}", FaultFrequenciesDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                setNonAutoGrowlMsg("growl_service_error_client_message");
            }
        } catch (Exception e) {
            Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }

    /**
     * Get the list of unique bearing vendors
     *
     * @param host String Object
     * @return List of String Objects
     */
    public List<String> getVendors(String host) {
        List<String> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;

        try {
            endPoint = new StringBuilder();
            endPoint.append("/presets/bearingCatalog?vendorId=DISTINCT");
            
            if ((responseVO = httpGet(PRESET_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        list = FaultFrequenciesSaxJsonParser.getInstance().populateVendorsFromJson(responseVO.getEntity());
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.WARNING, "Message from Presets Service: {0}", FaultFrequenciesDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.WARNING, "Error Message from Presets Service: {0}", FaultFrequenciesDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                setNonAutoGrowlMsg("growl_service_error_client_message");
            }
        } catch (Exception e) {
            Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }

    /**
     * Get BearingCatalogVO objects based on the given info
     *
     * @param host String Object
     * @param vendor String Object
     * @return List Object of BearingCatalogVO Objects
     */
    public List<BearingCatalogVO> getBearingCatalog(String host, String vendor) {
        List<BearingCatalogVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;

        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/presets/bearingCatalog")
                    .append("?vendorId=").append(URLEncoder.encode(vendor, "UTF-8"));
            
            if ((responseVO = httpGet(PRESET_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        list = FaultFrequenciesSaxJsonParser.getInstance().populateBearingCatalogVOFromJson(responseVO.getEntity());
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.WARNING, "Message from Presets Service: {0}", FaultFrequenciesDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.WARNING, "Error Message from Presets Service: {0}", FaultFrequenciesDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                setNonAutoGrowlMsg("growl_service_error_client_message");
            }
        } catch (Exception e) {
            Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }

    /**
     * Get BearingCatalogVO objects based on the given info
     *
     * @param host String Object
     * @param customer String Object
     * @param siteId
     * @return List Object of BearingCatalogVO Objects
     */
    public List<BearingCatalogVO> getSiteBearingFavorites(String host, String customer, UUID siteId) {
        List<BearingCatalogVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;

        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/presets/siteBearingFavorites")
                    .append("?customerAccount=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&siteId=").append(siteId);
            
            if ((responseVO = httpGet(PRESET_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        list = FaultFrequenciesSaxJsonParser.getInstance().populateBearingCatalogVOFromJson(responseVO.getEntity());
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.WARNING, "Message from Presets Service: {0}", FaultFrequenciesDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.WARNING, "Error Message from Presets Service: {0}", FaultFrequenciesDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                setNonAutoGrowlMsg("growl_service_error_client_message");
            }
        } catch (Exception e) {
            Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }
   
      /**
     * Get BearingCatalogVO objects based on the given info
     *
     * @param host String Object
     * @param customer String Object
     * @param siteId
     * @param vendorId
     * @return List Object of BearingCatalogVO Objects
     */
    public List<BearingCatalogVO> getSiteBearingFavoritesByVendorId(String host, String customer, UUID siteId,String vendorId) {
        List<BearingCatalogVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;

        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/presets/siteBearingFavorites")
                    .append("?customerAccount=").append(URLEncoder.encode(customer, "UTF-8"))                     
                    .append("&siteId=").append(siteId)
                    .append("&vendorId=").append(URLEncoder.encode(vendorId, "UTF-8"));
            
            if ((responseVO = httpGet(PRESET_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        list = FaultFrequenciesSaxJsonParser.getInstance().populateBearingCatalogVOFromJson(responseVO.getEntity());
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.WARNING, "Message from Presets Service: {0}", FaultFrequenciesDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.WARNING, "Error Message from Presets Service: {0}", FaultFrequenciesDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                setNonAutoGrowlMsg("growl_service_error_client_message");
            }
        } catch (Exception e) {
            Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }
   
    /**
     * Create multiple SiteBearingFavorites
     *
     * @param bearingCatalogVOList, List Object of BearingCatalogVO Objects
     */
    public void createSiteBearingFavorites(List<BearingCatalogVO> bearingCatalogVOList) {
        HttpResponseVO responseVO;
        
        if (bearingCatalogVOList != null && !bearingCatalogVOList.isEmpty())  {
             if ((responseVO = httpPost(PRESET_SERVICE_NAME,  ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), "/presets/siteBearingFavorites", FaultFrequenciesDomJsonParser.getInstance().getJsonFromBearingCatalogVO(bearingCatalogVOList))) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                         setNonAutoGrowlMsg("growl_create_item_items");
                        Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.INFO, "Site Bearing Favorites successfully created.");
                        break;
                    default:
                        Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.WARNING, "Error Message from Site Bearing Favorites Service: {0}", FaultFrequenciesDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                }
            }
        }
    }

    /**
     * Update by adding or removing SiteBearingFavorites
     *
     * @param createBearingCatalogVOList, List Object of BearingCatalogVO Objects
     * @param deleteBearingCatalogVOList, List Object of BearingCatalogVO Objects
     */
    public void updateSiteBearingFavorites(List<BearingCatalogVO> createBearingCatalogVOList, List<BearingCatalogVO> deleteBearingCatalogVOList) {
        HttpResponseVO responseVO;
        
        if ((createBearingCatalogVOList != null && !createBearingCatalogVOList.isEmpty())
                || (deleteBearingCatalogVOList != null && !deleteBearingCatalogVOList.isEmpty()))  {
             if ((responseVO = httpPut(PRESET_SERVICE_NAME,  ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), "/presets/siteBearingFavorites", FaultFrequenciesDomJsonParser.getInstance().getUpdateJsonFromBearingCatalogVO(createBearingCatalogVOList, deleteBearingCatalogVOList))) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        setNonAutoGrowlMsg("growl_update_item_items");
                        Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.INFO, "Updated Site Bearing Favorites successfully.");
                        break;
                    default:
                        Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.WARNING, "Error Message from Site Bearing Favorites Service: {0}", FaultFrequenciesDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                }
            }
        }
    }

    /**
     * Create multiple SiteFFSetFavoritesVO
     *
     * @param faultFrequenciesVOs
     */
    public void createSiteFFSetFavoritesVOList(List<FaultFrequenciesVO> faultFrequenciesVOs) {
        HttpResponseVO responseVO;
        
        if (faultFrequenciesVOs != null && !faultFrequenciesVOs.isEmpty())  {
             if ((responseVO = httpPost(PRESET_SERVICE_NAME,  ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), "/presets/siteFFSetFavorites", FaultFrequenciesDomJsonParser.getInstance().getJsonFromFaultFrequencies(faultFrequenciesVOs))) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                         setNonAutoGrowlMsg("growl_create_item_items");
                        Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.INFO, "Site FFSet Favorites successfully created.");
                        break;
                    default:
                        Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.WARNING, "Error Message from Site FFSet Favorites Service: {0}", FaultFrequenciesDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                }
            }
        }
    }
    
    /**
     * Update by adding or removing SiteFFSetFavorites
     *
     * @param createFaultFrequenciesVOList, List Object of FaultFrequenciesVO Objects
     * @param deleteFaultFrequenciesVOList, List Object of FaultFrequenciesVO Objects
     * @return 
     */
    public boolean updateSiteFFSetFavoritesVOList(List<FaultFrequenciesVO> createFaultFrequenciesVOList, List<FaultFrequenciesVO> deleteFaultFrequenciesVOList) {
        HttpResponseVO responseVO;
        
        if ((createFaultFrequenciesVOList != null && !createFaultFrequenciesVOList.isEmpty())
                || (deleteFaultFrequenciesVOList != null && !deleteFaultFrequenciesVOList.isEmpty())) {
             if ((responseVO = httpPut(PRESET_SERVICE_NAME,  ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), "/presets/siteFFSetFavorites", FaultFrequenciesDomJsonParser.getInstance().getUpdateJsonFromFaultFrequenciesVO(createFaultFrequenciesVOList, deleteFaultFrequenciesVOList))) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        setNonAutoGrowlMsg("growl_update_item_items");
                        Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.INFO, "Site FFSet Favorites updated successfully.");
                       return true;
                    default:
                        setNonAutoGrowlMsg("growl_service_error_client_message");
                        Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.WARNING, "Error Message from Site FFSet Favorites Service: {0}", FaultFrequenciesDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
        }
        return false;
    }
    
    /**
     * Get SiteFFSetFavoritesVO objects based on the given info
     *
     * @param host String Object
     * @param customer String Object
     * @param siteId
     * @return List Object of SiteFFSetFavoritesVO Objects
     */
    public List<FaultFrequenciesVO> getSiteFFSetFavoritesVOList(String host, String customer, UUID siteId) {
        List<FaultFrequenciesVO> list = new ArrayList();
        StringBuilder endPoint;
        HttpResponseVO responseVO;
        boolean serviceIssue = true;

        try {
            endPoint = new StringBuilder();
            endPoint
                    .append("/presets/siteFFSetFavorites")
                    .append("?customerAccount=").append(URLEncoder.encode(customer, "UTF-8"))
                    .append("&siteId=").append(siteId)
                    .append("&bearings=true");
            
            if ((responseVO = httpGet(PRESET_SERVICE_NAME, host, endPoint.toString())) != null && responseVO.isHostFound()) {
                switch (responseVO.getStatusCode()) {
                    case 200:
                        serviceIssue = false;
                        list = FaultFrequenciesSaxJsonParser.getInstance().populateFaultFrequenciesFromJson(responseVO.getEntity());
                        break;
                    case 404:
                        serviceIssue = false;
                        Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.WARNING, "Message from Presets Service: {0}", FaultFrequenciesDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                        break;
                    default:
                        Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                        Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.WARNING, "Error Message from Presets Service: {0}", FaultFrequenciesDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
                }
            }
            if (serviceIssue) {
                setNonAutoGrowlMsg("growl_service_error_client_message");
            }
        } catch (Exception e) {
            Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }
    
     /**
     * Delete the given SiteBearingFavorites
     *
     * @param host, String Object
     * @param bearingCatalogVO
     * @return boolean, true if success, else false
     */
    public boolean deleteSiteBearingFavorites(String host, BearingCatalogVO bearingCatalogVO) {
        HttpResponseVO responseVO;

        if ((responseVO = httpDelete(PRESET_SERVICE_NAME, host, "/presets/siteBearingFavorites", FaultFrequenciesDomJsonParser.getInstance().getJsonFromBearingCatalogVO(bearingCatalogVO))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {             
                Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.INFO, "Bearing Catalog successfully deleted.");
                return true;
            } else {
                Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.WARNING, "Error Message from Bearing Catalog Service: {0}", FaultFrequenciesDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        setNonAutoGrowlMsg("growl_service_error_client_message");
        
        return false;
    }
     
    /**
     * Delete the given SiteFaultFrequencies
     *
     * @param host, String Object
     * @param faultFrequenciesVOList
     * @return boolean, true if success, else false
     */
    public boolean deleteCustomFaultFrequencies(String host, List<FaultFrequenciesVO> faultFrequenciesVOList) {
        HttpResponseVO responseVO;

        if ((responseVO = httpDelete(PRESET_SERVICE_NAME, host, "/presets/siteFFSetFavorites", FaultFrequenciesDomJsonParser.getInstance().getJsonFromFaultFrequencies(faultFrequenciesVOList))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {               
                Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.INFO, "Custom Fault Frequencies successfully deleted.");
                return true;
            } else {
                Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(FaultFrequenciesClient.class.getName()).log(Level.WARNING, "Error Message from Custom Fault Frequencies Service: {0}", FaultFrequenciesDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        setNonAutoGrowlMsg("growl_service_error_client_message");
        
        return false;
    }
}
