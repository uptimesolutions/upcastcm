/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.http.client;

import com.uptime.client.http.client.Client;
import com.uptime.client.utils.vo.HttpResponseVO;
import com.uptime.upcastcm.bean.UserManageBean;
import com.uptime.upcastcm.http.utils.json.dom.SubscriptionsDomJsonParser;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import static com.uptime.upcastcm.utils.helperclass.GenericMessage.showErrorMsg;
import com.uptime.upcastcm.utils.vo.SubscriptionsVO;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 *
 * @author madhavi
 */
public class KafkaProxyClient extends Client {
    private static KafkaProxyClient instance = null;
    
    /**
     * Private Singleton class
     */
    private KafkaProxyClient() {
    }

    /**
     * Returns an instance of the class
     *
     * @return KafkaProxyClient object
     */
    public static KafkaProxyClient getInstance() {
        if (instance == null) {
            instance = new KafkaProxyClient();
        }
        return instance;
    }

    /**
     * Post Subscriptions to Kafka
     *
     * @param host, String Object
     * @param subscriptionsList, List of SubscriptionsVO Objects
     * @return boolean, true if success, else false
     */
    public boolean postSubscriptionsToKafka(String host, List<SubscriptionsVO> subscriptionsList) {
        UserManageBean userManageBean;
        HttpResponseVO responseVO;
        
        Logger.getLogger(KafkaProxyClient.class.getName()).log(Level.INFO, "json: {0}", SubscriptionsDomJsonParser.getInstance().getKafkaJsonFromSubscriptions(subscriptionsList));
        if ((responseVO = httpPost(KAFKA_PROXY_SERVICE_NAME, host, "/subscriptions", SubscriptionsDomJsonParser.getInstance().getKafkaJsonFromSubscriptions(subscriptionsList))) != null && responseVO.isHostFound()) {
            if (responseVO.getStatusCode() == 200) {
                userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
                FacesContext.getCurrentInstance().addMessage("nonAutoGrowlMessages", new FacesMessage(userManageBean.getResourceBundleString("growl_subscription_republish_success_message")));
                Logger.getLogger(KafkaProxyClient.class.getName()).log(Level.INFO, "Subscription successfully republished.");
                return true;
            } else {
                Logger.getLogger(KafkaProxyClient.class.getName()).log(Level.WARNING, "Error Code: {0}", String.valueOf(responseVO.getStatusCode()));
                Logger.getLogger(KafkaProxyClient.class.getName()).log(Level.WARNING, "Error Message from Config Service: {0}", SubscriptionsDomJsonParser.getInstance().getMessageFromJSON(responseVO.getEntity()));
            }
        }
        showErrorMsg();

        return false;
    }

}
