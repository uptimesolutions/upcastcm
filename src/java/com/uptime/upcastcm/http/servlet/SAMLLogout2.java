/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.http.servlet;

import com.onelogin.saml2.Auth;
import com.onelogin.saml2.exception.Error;
import com.onelogin.saml2.exception.SettingsException;
import com.uptime.upcastcm.dao.AuthPropertiesDAO;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * This class should be used for IdP that use HTTP-Redirect for SLO Responses rather than HTTP POST
 * 
 * @author ksimmons
 */
public class SAMLLogout2 extends HttpServlet {
    private static AuthPropertiesDAO properties;

    @Override
    public void init() throws ServletException {
        ServletConfig config = getServletConfig();
        properties = new AuthPropertiesDAO(config.getServletContext());
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String subdomain, propFile, nameId, nameIdFormat, nameidNameQualifier, nameidSPNameQualifier, sessionIndex;
        RequestDispatcher dispatcher;
        StringBuilder fwdUrl;
        HttpSession session;
        Auth auth;
        
        try{
            response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
            response.setHeader("Pragma", "no-cache");
            response.setHeader("Expires", "0");
        
            // lookup saml properties from auth.properties
            subdomain = request.getHeader("host").split("\\.")[0]; // get the subdomain of the URL
            propFile = properties.getSamlProperties(subdomain);
            if(propFile == null){
                Logger.getLogger(SAMLLogout2.class.getName()).log(Level.SEVERE, "Cannot retreive saml.properties file location from auth.properties for domain:{0}", subdomain);
                dispatcher = request.getRequestDispatcher("loginError.xhtml");
                dispatcher.forward(request, response);
                return;
            }
          
            auth = new Auth(propFile,request, response);
            
            // invalidate session
            if((session = request.getSession(false)) != null){
                nameId = null;
                if (session.getAttribute("nameId") != null) {
                    nameId = session.getAttribute("nameId").toString();
                    session.removeAttribute("nameId");
                    session.removeAttribute("displayName");
                    session.removeAttribute("company");
                    session.removeAttribute("accountNumber");
                }
                
                nameIdFormat = null;
                if (session.getAttribute("nameIdFormat") != null) {
                    nameIdFormat = session.getAttribute("nameIdFormat").toString();
                    session.removeAttribute("nameIdFormat");
                }
                
                nameidNameQualifier = null;
                if (session.getAttribute("nameidNameQualifier") != null) {
                    nameidNameQualifier = session.getAttribute("nameidNameQualifier").toString();
                    session.removeAttribute("nameidNameQualifier");
                }
                
                nameidSPNameQualifier = null;
                if (session.getAttribute("nameidSPNameQualifier") != null) {
                    nameidSPNameQualifier = session.getAttribute("nameidSPNameQualifier").toString();
                    session.removeAttribute("nameidSPNameQualifier");
                }
                
                sessionIndex = null;
                if (session.getAttribute("sessionIndex") != null) {
                    sessionIndex = session.getAttribute("sessionIndex").toString();
                    session.removeAttribute("sessionIndex");
                }
                
                // this is the initial SLO request
                if(nameId != null) {
                    Logger.getLogger(SAMLLogout2.class.getName()).log(Level.INFO, "BEGINNING SLO PROCESS for:{0} REDIRECTING TO SLO URL", subdomain);
                    auth.logout(null, nameId, sessionIndex, nameIdFormat, nameidNameQualifier, nameidSPNameQualifier);
                }
                
                // this is the SLO response
                else {
                    Logger.getLogger(SAMLLogout2.class.getName()).log(Level.INFO, "SLO RESPONSE RECEIVED for:{0} REDIRECTING TO LOGIN", subdomain);
                    session.invalidate();
                    
                    // redirect to login page
                    fwdUrl = new StringBuilder();
                    fwdUrl.append("https://").append(request.getHeader("host")).append("/UpCastCM/SAMLLogin");
                    Logger.getLogger(SAMLLogout2.class.getName()).log(Level.INFO, "Redirecting to:{0}", fwdUrl.toString());
                    response.sendRedirect(fwdUrl.toString());
                }
            } 
            
            // no session found redirect to login page
            else {
                fwdUrl = new StringBuilder();
                fwdUrl.append("https://").append(request.getHeader("host")).append("/UpCastCM/SAMLLogin");
                Logger.getLogger(SAMLLogout2.class.getName()).log(Level.INFO, "Session is null. Redirecting to:{0}", fwdUrl.toString());
                response.sendRedirect(fwdUrl.toString());
            }
            
        } catch(SettingsException | Error e){
            Logger.getLogger(SAMLLogout2.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            throw new ServletException(e);
        } catch(Exception e){
            Logger.getLogger(SAMLLogout2.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            throw new ServletException(e);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        StringBuilder fwdUrl;
        
        response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
        response.setHeader("Pragma", "no-cache");
        response.setHeader("Expires", "0");
        
        fwdUrl = new StringBuilder();
        fwdUrl.append("https://").append(request.getHeader("host")).append("/UpCastCM");
        Logger.getLogger(SAMLLogout2.class.getName()).log(Level.INFO, "Session is null. Redirecting to:{0}", fwdUrl.toString());
        
        response.sendRedirect(fwdUrl.toString());
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
