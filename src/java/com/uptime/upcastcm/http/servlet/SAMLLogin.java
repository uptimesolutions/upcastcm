/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.http.servlet;

import com.onelogin.saml2.Auth;
import com.onelogin.saml2.exception.Error;
import com.onelogin.saml2.exception.SettingsException;
import com.uptime.upcastcm.dao.AuthPropertiesDAO;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 *
 * @author ksimmons
 */
public class SAMLLogin extends HttpServlet {
    private static AuthPropertiesDAO properties;
    
    @Override
    public void init() throws ServletException {
        ServletConfig config = getServletConfig();
        properties = new AuthPropertiesDAO(config.getServletContext());
    }
    
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher dispatcher;
        String subdomain, propFile;
        Auth auth;
        
        try{
            response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
            response.setHeader("Pragma", "no-cache");
            response.setHeader("Expires", "0");

            // lookup saml properties from auth.properties
            subdomain = request.getHeader("host").split("\\.")[0]; // get the subdomain of the URL
            
            //Logger.getLogger(SAMLLogin.class.getName()).log(Level.INFO, "SAML PROPERTIES:"+properties.getSamlProperties(subdomain));
            if((propFile = properties.getSamlProperties(subdomain)) == null){
                Logger.getLogger(SAMLLogin.class.getName()).log(Level.SEVERE, "Cannot retreive saml.properties file location from auth.properties for domain:{0}", subdomain);
                dispatcher = request.getRequestDispatcher("loginError.xhtml");
                dispatcher.forward(request, response);
                return;
            }
            
            auth = new Auth(propFile, request, response);
            if (request.getParameter("referrer") == null) {
		auth.login();
            } else{
                auth.login(request.getParameter("referrer"));
            }
        } catch(SettingsException | Error e){
            Logger.getLogger(SAMLLogin.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            throw new ServletException(e);
        } catch(Exception e){
            Logger.getLogger(SAMLLogin.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            throw new ServletException(e);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.sendRedirect("/SAMLLogin");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
