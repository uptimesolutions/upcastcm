/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.http.servlet;

import com.uptime.upcastcm.bean.LoginBean;
import com.uptime.upcastcm.bean.UserManageBean;
import com.uptime.upcastcm.dao.AuthPropertiesDAO;
import com.uptime.upcastcm.utils.FacesUtil;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.el.ELContext;
import javax.faces.application.ResourceHandler;
import javax.faces.context.FacesContext;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


/**
 * This is an intercepting filter design pattern which intercepts all HTTP requests to the servlet container. It
 * looks for the "nameId" session attribute. If a session and the "nameId" attribute exist, it is assumed the user is 
 * logged in and the filter passes control to the next filter.
 * 
 * If the "nameId" attribute is not found then the browser is redirected to either a SAML IdP endpoint or the LDAP login page.
 * 
 * @author ksimmons
 */
public class LoginFilter implements Filter{
    private static AuthPropertiesDAO properties;
    private FilterConfig config;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        this.config = filterConfig;
        ServletContext context = config.getServletContext();
        properties = new AuthPropertiesDAO(context);
        Filter.super.init(filterConfig);
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
        String subdomain, requestUri, authType, contextPath, accountName, queryString, redirectUrl;
        HttpServletResponse httpresResponse;
        HttpServletRequest httpRequest;
        UserManageBean userManageBean;
        RequestDispatcher dispatcher;
        LoginBean loginBean;
        HttpSession session;
        boolean isLoggedIn;
        
        httpRequest = (HttpServletRequest) request;
        httpresResponse = (HttpServletResponse) response;
        session = httpRequest.getSession(false); // do not create a session if one does not exist
        subdomain = httpRequest.getHeader("host").split("\\.")[0]; // get the subdomain of the URL
        requestUri = httpRequest.getRequestURI();
        contextPath = httpRequest.getContextPath();
        queryString = httpRequest.getQueryString();
        //subdomain = "notifications";
        
        //************************************Note************************************
        // Please Comment out the Loggers after test. If the Loggers 
        // are left uncommented, then every request going through the
        // filter will be logged  and it will be too much for the logs.
        //****************************************************************************
        
        /*
        java.util.logging.Logger.getLogger(LoginFilter.class.getName()).log(Level.INFO, "Request URI:{0}", requestUri);
        java.util.logging.Logger.getLogger(LoginFilter.class.getName()).log(Level.INFO, "HOST:{0}", httpRequest.getHeader("host"));
        java.util.logging.Logger.getLogger(LoginFilter.class.getName()).log(Level.INFO, "SUBDOMAIN:{0}", subdomain);
        java.util.logging.Logger.getLogger(LoginFilter.class.getName()).log(Level.INFO, "QUERY:{0}", httpRequest.getQueryString());
        */
        
        /*
        LOGGER.info("HOST:"+httpRequest.getHeader("host"));
        LOGGER.info("SUBDOMAIN:"+subdomain);
        LOGGER.info("PATH:"+requestUri);
        */
 
        // NOTE: notifications and notifications-uat are fixed subdomains managed by content rules in the hardware load balancers
        if(subdomain.equals("notifications") || subdomain.equals("notifications-uat")) {
            accountName = properties.getSubDomain(request.getParameter("account"));
            //Logger.getLogger(LoginFilter.class.getName()).log(Level.INFO, "****NOTIFICATIONS:"+accountName);

            if(subdomain.equals("notifications-uat"))
                redirectUrl = httpRequest.getRequestURL().toString().replace("notifications-uat", accountName); // UAT
            else
                redirectUrl = httpRequest.getRequestURL().toString().replace("notifications", accountName); // PROD
            
            redirectUrl = redirectUrl.substring(0, redirectUrl.length()-1)+"?"+queryString;
            //Logger.getLogger(LoginFilter.class.getName()).log(Level.INFO, "****REDIRECT:"+redirectUrl);
            httpresResponse.sendRedirect(redirectUrl);
            return;
        }

        isLoggedIn = ((session != null) &&
                (session.getAttribute("nameId") != null) && !session.getAttribute("nameId").toString().isEmpty() &&
                (session.getAttribute("accountNumber") != null) && !session.getAttribute("accountNumber").toString().isEmpty());


        // If this is a health check request from the load balancer allow request to continue
        if(requestUri.endsWith("monitor.html")){
            filterChain.doFilter(request, response);
        }

        // If this is a metadata request allow request to continue
        else if(requestUri.endsWith("SAMLMetaData")){
            //java.util.logging.Logger.getLogger(LoginFilter.class.getName()).log(Level.INFO, "Metadata Request");
            //LOGGER.info("Metadata Request");
            filterChain.doFilter(request, response);
        }

        // User is already logged in and trying to login again so forward to home page
        else if(isLoggedIn && (requestUri.endsWith("defaultLogin.xhtml") || requestUri.endsWith("SAMLLogin"))){
            //Logger.getLogger(LoginFilter.class.getName()).log(Level.INFO, "HTTP Session found for:{0}", session.getAttribute("nameId"));
            //Logger.getLogger(LoginFilter.class.getName()).log(Level.INFO, "HTTP Session Account Number:{0}", session.getAttribute("accountNumber"));
            //Logger.getLogger(LoginFilter.class.getName()).log(Level.INFO, "HTTP Session found for:"+session.getAttribute("nameId"));
            //Logger.getLogger(LoginFilter.class.getName()).log(Level.INFO, "HTTP Session Account Number:"+session.getAttribute("accountNumber"));

            dispatcher = request.getRequestDispatcher("/user/home.xhtml");
            dispatcher.forward(request, response);
        }

        // if user is not logged in and request is for SAMLLogout, this is usually SLO response
        else if(!isLoggedIn && ((requestUri.endsWith("SAMLLogout")) || (requestUri.endsWith("SAMLLogout2"))) ){
            Logger.getLogger(LoginFilter.class.getName()).log(Level.INFO, "SLO Response Received for:"+subdomain);
            filterChain.doFilter(request, response);
        }

        // The user is logged in so allow request to continue
        else if(isLoggedIn){
            //Logger.getLogger(LoginFilter.class.getName()).log(Level.INFO, "HTTP Session found for:{0}", session.getAttribute("nameId"));
            //Logger.getLogger(LoginFilter.class.getName()).log(Level.INFO, "HTTP Session Account Number:{0}", session.getAttribute("accountNumber"));
            //Logger.getLogger(LoginFilter.class.getName()).log(Level.INFO, "HTTP Session found for:"+session.getAttribute("nameId"));
            //Logger.getLogger(LoginFilter.class.getName()).log(Level.INFO, "HTTP Session Account Number:"+session.getAttribute("accountNumber"));

            //MutableHttpServletRequest mutableRequest = new MutableHttpServletRequest(httpRequest);
            // Add custom HTTP headers
            //mutableRequest.putHeader("nameId", session.getAttribute("nameId").toString());
            //filterChain.doFilter(mutableRequest, response);
            if(httpRequest.getParameterMap().containsKey("page")) {
                if (session != null) {
                    httpRequest.getParameterMap().keySet().stream().filter(key -> key.equalsIgnoreCase("page") || key.contains("Id")).forEachOrdered(key -> session.setAttribute(key, httpRequest.getParameterMap().get(key)[0]));
                }
                userManageBean = (UserManageBean) getBean("userManageBean", httpRequest, httpresResponse);
                userManageBean.init();
            } else {
                filterChain.doFilter(request, response);
            }
        }

        // The user is not logged in but we are receiving the authn response
        else if(!isLoggedIn && (requestUri.endsWith("/SAMLAssertionProcessor"))){
            //Logger.getLogger(LoginFilter.class.getName()).log(Level.INFO, "Receiving SAML Response");
            filterChain.doFilter(request, response);
        }

        // The user is not logged in and forgot password allow request to continue
        else if(!isLoggedIn && (requestUri.contains("/forgotPassword.xhtml"))){
            // If this is from javax.faces.resource/login request or resources/login request allow request to continue
            if (requestUri.startsWith(contextPath + ResourceHandler.RESOURCE_IDENTIFIER + "/login") || requestUri.startsWith(contextPath + "/resources/login")) {
                filterChain.doFilter(request, response);
            } 
            // Forward to Forgot Password Page
            else {
                //Logger.getLogger(LoginFilter.class.getName()).log(Level.INFO, "forgotPassword page invoked");
                dispatcher = request.getRequestDispatcher("/forgotPassword.xhtml?account_number=" + properties.getAccountNumber(subdomain));
                dispatcher.forward(request, response);
            }
        }

        // The user is not logged in and reset user password allow request to continue
        else if(!isLoggedIn && (requestUri.contains("/userPassword.xhtml"))){
            // If this is from javax.faces.resource/login request or resources/login request allow request to continue
            if (requestUri.startsWith(contextPath + ResourceHandler.RESOURCE_IDENTIFIER + "/login") || requestUri.startsWith(contextPath + "/resources/login")) {
                filterChain.doFilter(request, response);
            } 
            // Forward to Reset Password Page
            else {
                //Logger.getLogger(LoginFilter.class.getName()).log(Level.INFO, "forgotPassword page invoked");
                dispatcher = request.getRequestDispatcher("/userPassword.xhtml?account_number=" + properties.getAccountNumber(subdomain));
                dispatcher.forward(request, response);
            }
        }

        // The user is not logged in so forward to login page
        else{
            // If this is from javax.faces.resource/login request or resources/login request allow request to continue
            if (requestUri.startsWith(contextPath + ResourceHandler.RESOURCE_IDENTIFIER + "/login") || requestUri.startsWith(contextPath + "/resources/login")) {
                filterChain.doFilter(request, response);
            } 
            // Forward to LDAP Login Page, SAML Login, or Login Error Page
            else {
                //Logger.getLogger(LoginFilter.class.getName()).log(Level.INFO, "No HTTP Session found, forwarding to login...");

                // Get the auth type for this account based on the subdomain of the URL
                subdomain = subdomain.contains("localhost") ? "localhost" : subdomain;
                authType = properties.getAuthType(subdomain);

                //Logger.getLogger(LoginFilter.class.getName()).log(Level.INFO, "Auth Type = "+authType);

                // If auth type is SAML redirect to the configured SSO IdP endpoint
                if(authType.compareToIgnoreCase("saml") == 0) {
                    //dispatcher = request.getRequestDispatcher("dologin.jsp");
                    
                    if (httpRequest.getParameterMap().containsKey("page") && queryString != null && !queryString.isEmpty()) {
                        dispatcher = request.getRequestDispatcher("/SAMLLogin?referrer=" + requestUri+ "&" + queryString);
                    } else {
                        dispatcher = request.getRequestDispatcher("/SAMLLogin?referrer=" + requestUri);
                    }
                } 

                // If auth type is LDAP redirect to the LDAP login page
                else if(authType.compareToIgnoreCase("ldap") == 0){
                    //redirectUrl = "/defaultLogin.xhtml?account_number=" + properties.getAccountNumber(subdomain) + "&account_name=" + properties.getAccountName(subdomain);
                    //dispatcher = request.getRequestDispatcher(redirectUrl);
                    //Logger.getLogger(LoginFilter.class.getName()).log(Level.INFO, "Ldap login reditect url {0} ", redirectUrl);

                    loginBean = (LoginBean)getBean("loginBean", httpRequest, httpresResponse);
                    loginBean.getRequestParameterMap().put("account_number", new String[]{properties.getAccountNumber(subdomain)});
                    loginBean.getRequestParameterMap().put("account_name", new String[]{properties.getAccountName(subdomain)});
                    if(httpRequest.getParameterMap().containsKey("page")) {
                        httpRequest.getParameterMap().keySet().stream().filter(key -> key != null && !key.isEmpty() && !key.equalsIgnoreCase("account")).forEachOrdered(key -> loginBean.getRequestParameterMap().put(key, httpRequest.getParameterMap().get(key)));
                    }

                    dispatcher = request.getRequestDispatcher("/defaultLogin.xhtml");
                }

                // If auth type isn't SAML or LDAP redirect to the loginError page
                else {
                    dispatcher = request.getRequestDispatcher("/loginError.xhtml");
                }

                dispatcher.forward(request, response);
            }
        }
    }

    @Override
    public void destroy() {
        Filter.super.destroy(); //To change body of generated methods, choose Tools | Templates.
    }

    private static Object getBean(String beanName, HttpServletRequest request, HttpServletResponse response) {
        Object bean = null;
        FacesContext fc = FacesUtil.getFacesContext(request, response);
        if (fc != null) {
            ELContext elContext = fc.getELContext();
            bean = elContext.getELResolver().getValue(elContext, null, beanName);
        }

        return bean;
    }
}
