/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.http.servlet;

import com.onelogin.saml2.Auth;
import com.onelogin.saml2.exception.Error;
import com.onelogin.saml2.exception.SettingsException;
import com.onelogin.saml2.servlet.ServletUtils;
import com.uptime.upcastcm.dao.AuthPropertiesDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author ksimmons
 */
//@WebServlet(name = "SAMLAssertionProcessor", urlPatterns = {"/SAMLAssertionProcessor"})
public class SAMLAssertionProcessor extends HttpServlet {
    private static AuthPropertiesDAO properties;
    
    @Override
    public void init() throws ServletException {
        ServletConfig config = getServletConfig();
        properties = new AuthPropertiesDAO(config.getServletContext());
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet SAMLAssertionProcessor</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet SAMLAssertionProcessor at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String subdomain, propFile, nameId, nameIdFormat, sessionIndex, nameidNameQualifier, nameidSPNameQualifier, opco, group, role, relayState;
        RequestDispatcher dispatcher;
        Properties samlProperties;
        Map<String, List<String>> attributes;
        List<String> errors;
        HttpSession session;
        Auth auth;
        
        response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
        response.setHeader("Pragma", "no-cache");
        response.setHeader("Expires", "0");
            
        //processRequest(request, response);
        Logger.getLogger(SAMLAssertionProcessor.class.getName()).log(Level.INFO, "HOST:"+request.getHeader("host"));

        // lookup saml properties from auth.properties
        subdomain = request.getHeader("host").split("\\.")[0]; // get the subdomain of the URL

        try{
            if((propFile = properties.getSamlProperties(subdomain)) == null){
                Logger.getLogger(SAMLAssertionProcessor.class.getName()).log(Level.SEVERE, "Cannot retreive saml.properties file location from auth.properties for domain:"+subdomain);
                dispatcher = request.getRequestDispatcher("loginError.xhtml");
                dispatcher.forward(request, response);
                return;
            }
            
            auth = new Auth(propFile, request, response);
            auth.processResponse();
            
            if (!auth.isAuthenticated()) {
                dispatcher = request.getRequestDispatcher("loginError.xhtml");
                dispatcher.forward(request, response);
                return;
            }
            
            errors = auth.getErrors();
            if (!errors.isEmpty()) {
                response.setContentType("text/html;charset=UTF-8");
                try (PrintWriter out = response.getWriter()) {
                    out.println("<!DOCTYPE html>");
                    out.println("<html>\n<head>\n<title>Servlet SAMLAssertionProcessor</title>\n</head>");
                    out.println("<body>");
                    out.println("<p>" + StringUtils.join(errors, ", ") + "</p>");
                    if (auth.isDebugActive()) {
                        String errorReason = auth.getLastErrorReason();
                        if (errorReason != null && !errorReason.isEmpty()) {
                            out.println("<p>" + auth.getLastErrorReason() + "</p>");
                        }
                    }
                    out.println("<a href=\"SAMLLogin\" class=\"btn btn-primary\">Login</a>");
                    out.println("</body>\n</html>");
                    out.flush();
                    out.close();
                }
            }
            else{
                // process the assertions
                attributes = auth.getAttributes();
                
                nameId = auth.getNameId();
                nameIdFormat = auth.getNameIdFormat();
                sessionIndex = auth.getSessionIndex();
                nameidNameQualifier = auth.getNameIdNameQualifier();
                nameidSPNameQualifier = auth.getNameIdSPNameQualifier();

                // create a session and add the assertion attributes
                session = request.getSession(true);
                session.setAttribute("attributes", attributes);
                session.setAttribute("nameId", nameId);
                session.setAttribute("nameIdFormat", nameIdFormat);
                session.setAttribute("sessionIndex", sessionIndex);
                session.setAttribute("nameidNameQualifier", nameidNameQualifier);
                session.setAttribute("nameidSPNameQualifier", nameidSPNameQualifier);
                session.setAttribute("accountNumber", properties.getAccountNumber(subdomain));
                session.setAttribute("company", properties.getAccountName(subdomain));
                session.setAttribute("displayName", nameId); // TODO - use the full name attribute
                
                // parse the attributes for "groups", 
                // parse the attributes from a notice email: "page", "siteId", "areaId", "assetId", "pointLocationId", or "pointId"
                for(Map.Entry<String,List<String>> entry : attributes.entrySet()) {
                    // get the value of the attribute
                    List<String> values = entry.getValue();
                    StringBuilder valuesList = new StringBuilder();
                    for(String val : values){
                        valuesList.append(val).append(",");
                    }
                    valuesList.deleteCharAt(valuesList.length()-1);
                    Logger.getLogger(SAMLAssertionProcessor.class.getName()).log(Level.INFO, "ATTRIBUTE:{0}", entry.getKey()+":"+valuesList.toString());
                    
                    //if(entry.getKey().compareToIgnoreCase("opcoFamily") == 0) {
                        //opco = entry.getValue().get(0);
                        
                        // only FXE users
                        //if(opco.compareToIgnoreCase("FXE") != 0){
                        //    Logger.getLogger(SAMLAssertionProcessor.class.getName()).log(Level.INFO, "ONLY FXE USERS ALLOWED... REDIRECTING");
                        //    dispatcher = request.getRequestDispatcher("loginError.xhtml");
                        //    dispatcher.forward(request, response);
                        //    return;
                        //}
                    //}
                    
                    if(entry.getKey().compareToIgnoreCase("groups") == 0){
                        Logger.getLogger(SAMLAssertionProcessor.class.getName()).log(Level.INFO, "GROUP ATTRIBUTE FOUND");
                        
                        // if groups are included, load the SAML properties file
                        samlProperties = properties.getSamlPropertiesFile(propFile);
                        
                        // the groups attribute can have multiple values so check each one to see if it maps to a role
                        for(String val : values){
                            Logger.getLogger(SAMLAssertionProcessor.class.getName()).log(Level.INFO, "CHECKING GROUP:{0}", val);
                            // get the role mapping from saml.properties using the group name
                            role = samlProperties.getProperty(val+".role.mapping");
                            // exit if a role has been matched
                            if(role != null){
                                Logger.getLogger(SAMLAssertionProcessor.class.getName()).log(Level.INFO, "UPCAST MAPPED ROLE:{0}", role);
                                session.setAttribute("customerRole", role);
                                break;
                            }
                        }
                    } 
                    
                    // Add "page", "siteId", "areaId", "assetId", "pointLocationId", or "pointId" to session
                    else if (entry.getKey().compareToIgnoreCase("page") == 0 || 
                            entry.getKey().compareToIgnoreCase("siteId") == 0 || 
                            entry.getKey().compareToIgnoreCase("areaId") == 0 || 
                            entry.getKey().compareToIgnoreCase("assetId") == 0 || 
                            entry.getKey().compareToIgnoreCase("pointLocationId") == 0 || 
                            entry.getKey().compareToIgnoreCase("pointId") == 0) {
                        session.setAttribute(entry.getKey(), entry.getValue().get(0));
                    }
                }
                
                if ((relayState = request.getParameter("RelayState")) != null && !relayState.isEmpty() && !relayState.equals(ServletUtils.getSelfRoutedURLNoQuery(request)) && !relayState.endsWith("SAMLLogin")) { 
                    // We don't want to be redirected to login.jsp neither
                    response.sendRedirect(request.getParameter("RelayState"));
                }
                
                // redirect to home page
                else{
                    response.sendRedirect("user/home.xhtml"); 
                }
            }
        } catch(SettingsException | Error e){
            Logger.getLogger(SAMLAssertionProcessor.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            throw new ServletException(e);
        } catch(Exception e){
            Logger.getLogger(SAMLAssertionProcessor.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            throw new ServletException(e);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
