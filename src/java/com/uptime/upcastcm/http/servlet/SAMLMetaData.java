/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.http.servlet;

import com.onelogin.saml2.Auth;
import com.onelogin.saml2.exception.Error;
import com.onelogin.saml2.exception.SettingsException;
import com.onelogin.saml2.settings.Saml2Settings;
import com.uptime.upcastcm.dao.AuthPropertiesDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author ksimmons
 */
public class SAMLMetaData extends HttpServlet {
    private static final Logger LOGGER = LoggerFactory.getLogger(SAMLMetaData.class.getName());
    private static AuthPropertiesDAO properties;
    
    @Override
    public void init() throws ServletException {
        ServletConfig config = getServletConfig();
        properties = new AuthPropertiesDAO(config.getServletContext());
    }
    
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String subdomain, propFile, metadata;
        RequestDispatcher dispatcher;
        Saml2Settings settings;
        List<String> errors;
        Auth auth;
        
        try{
            // lookup saml properties from auth.properties
            subdomain = request.getHeader("host").split("\\.")[0]; // get the subdomain of the URL
            LOGGER.info("SAML PROPERTIES:" + properties.getSamlProperties(subdomain));
            
            if((propFile = properties.getSamlProperties(subdomain)) == null){
                LOGGER.error("Cannot retreive saml.properties file location from auth.properties.");
                dispatcher = request.getRequestDispatcher("loginError.xhtml");
                dispatcher.forward(request, response);
                return;
            }
            
            auth = new Auth(propFile,request, response);
            
            settings = auth.getSettings();
            settings.setSPValidationOnly(true);
            errors = settings.checkSettings();
            
            if (errors.isEmpty()) {
                metadata = settings.getSPMetadata();
                response.setContentType("application/xml; charset=UTF-8");
                try (PrintWriter out = response.getWriter()) {
                    out.println(metadata);
                    out.flush();
                    out.close();
                }
            } else {
                response.setContentType("text/html; charset=UTF-8");
                try (PrintWriter out = response.getWriter()) {
                    out.println("<!DOCTYPE html>");
                    out.println("<html>\n<head>\n<title>Servlet SAMLAssertionProcessor</title>\n</head>");
                    out.println("<body>");
                    errors.forEach(error -> out.println("<p>" + error +"</p>"));
                    out.println("</body>\n</html>");
                    out.flush();
                    out.close();
                }
            }
        } catch(SettingsException | Error e){
            LOGGER.error(e.getMessage(), e);
            throw new ServletException(e);
        } catch(Exception e){
            LOGGER.error(e.getMessage(), e);
            throw new ServletException(e);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "SAMLMetaData";
    }// </editor-fold>

}
