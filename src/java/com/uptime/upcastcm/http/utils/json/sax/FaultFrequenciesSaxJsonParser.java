/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.http.utils.json.sax;

import com.uptime.client.utils.SaxJsonParser;
import com.uptime.upcastcm.utils.vo.BearingCatalogVO;
import com.uptime.upcastcm.utils.vo.FaultFrequenciesVO;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;

/**
 *
 * @author Joseph
 */
public class FaultFrequenciesSaxJsonParser extends SaxJsonParser {
    private static FaultFrequenciesSaxJsonParser instance = null;
    
    /**
     * Private Singleton class
     */
    private FaultFrequenciesSaxJsonParser() {
    }

    /**
     * Returns an instance of the class
     * @return FaultFrequenciesSaxJsonParser object
     */
    public static FaultFrequenciesSaxJsonParser getInstance() {
        if (instance == null) {
            instance = new FaultFrequenciesSaxJsonParser();
        }
        return instance; 
    }
    
    /**
     * Converts a json to a List of FaultFrequenciesVO Objects
     * @param content, String Object, json 
     * @return List of FaultFrequenciesVO Objects
     */
    public List<FaultFrequenciesVO> populateFaultFrequenciesFromJson(String content) {
        List<FaultFrequenciesVO> list = new ArrayList();
        String keyName;
        FaultFrequenciesVO siteFaultFrequenciesVO;
        boolean inArray;
         
        try {
            parser = Json.createParser(new StringReader(content));
            keyName = null;
            siteFaultFrequenciesVO = null;
            inArray = false;
            
            while (parser.hasNext()) {
                switch((event = parser.next())) {
                    case START_ARRAY:
                        inArray = true;
                        break;
                    case END_ARRAY:
                        inArray = false;
                        keyName = null;
                        siteFaultFrequenciesVO = null;
                        break;
                    case START_OBJECT:
                        siteFaultFrequenciesVO = inArray ? new FaultFrequenciesVO() : null;
                        break;
                    case END_OBJECT:
                        if(siteFaultFrequenciesVO != null && inArray)
                            list.add(siteFaultFrequenciesVO);
                        siteFaultFrequenciesVO = null;
                        keyName = null;
                        break;
                    case KEY_NAME:
                            switch(parser.getString()) {
                                case "customerAccount":
                                    keyName = "customerAccount";
                                    break;
                                case "siteName":
                                    keyName = "siteName";
                                    break;
                                case "ffName":
                                    keyName = "ffName";
                                    break;
                                case "ffSetDesc":
                                    keyName = "ffSetDesc";
                                    break;                                    
                                case "ffSetName":
                                    keyName = "ffSetName";
                                    break;
                                case "ffUnit":
                                    keyName = "ffUnit";
                                    break;                                    
                                case "ffValue":
                                    keyName = "ffValue";
                                    break;                               
                                case "ffSetId":
                                    keyName = "ffSetId";
                                    break;                               
                                case "ffId":
                                    keyName = "ffId";
                                    break;                               
                                case "siteId":
                                    keyName = "siteId";
                                    break;   
                                case "global":
                                    keyName = "global";
                                    break;               
                                default:
                                    keyName = null;
                                    break;      
                            }
                        break;
                    case VALUE_STRING:
                        if(siteFaultFrequenciesVO != null && inArray && keyName != null) {
                            switch(keyName) {
                                case "customerAccount":
                                    siteFaultFrequenciesVO.setCustomerAccount(parser.getString());
                                    break;
                                case "siteName":
                                    siteFaultFrequenciesVO.setSiteName(parser.getString());
                                    break;
                                case "ffName":
                                    siteFaultFrequenciesVO.setFfName(parser.getString());
                                    break;
                                case "ffSetDesc":
                                    siteFaultFrequenciesVO.setFfSetDesc(parser.getString());
                                    break;
                                case "ffSetName":
                                    siteFaultFrequenciesVO.setFfSetName(parser.getString());
                                    break;
                                case "ffUnit":
                                    siteFaultFrequenciesVO.setFfUnit(parser.getString());
                                    break;
                                case "ffSetId":
                                    siteFaultFrequenciesVO.setFfSetId(UUID.fromString(parser.getString()));
                                    break;
                                case "ffId":
                                    siteFaultFrequenciesVO.setFfId(UUID.fromString(parser.getString()));
                                    break;
                                case "siteId":
                                    siteFaultFrequenciesVO.setSiteId(UUID.fromString(parser.getString()));
                                    break;
                            }
                        }
                        break;
                    case VALUE_NUMBER:
                        if(siteFaultFrequenciesVO != null && inArray && keyName != null) {
                            switch(keyName) {
                                case "ffValue":
                                    siteFaultFrequenciesVO.setFfValue(parser.getBigDecimal().floatValue());
                                    break;
                            }
                        }
                        break;  
                     case VALUE_TRUE:
                        if(siteFaultFrequenciesVO != null && inArray && keyName != null) {
                            switch(keyName) {
                                case "global":
                                    siteFaultFrequenciesVO.setGlobal(true);
                                    break;
                            }
                        }
                        break;
                    case VALUE_FALSE:
                        if(siteFaultFrequenciesVO != null && inArray && keyName != null) {
                            switch(keyName) {
                                case "global":
                                    siteFaultFrequenciesVO.setGlobal(false);
                                    break;
                            }
                        }
                        break;
                }
            }
        } catch (Exception e) {
            Logger.getLogger(FaultFrequenciesSaxJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }
    
    /**
     * Converts a json to a List of String Objects
     * @param content, String Object, json 
     * @return List of String Objects
     */
    public List<String> populateVendorsFromJson(String content) {
        List<String> list = new ArrayList();
        String vendor;
        boolean inArray;
        if (content != null && !content.isEmpty()) {
            parser = Json.createParser(new StringReader(content));
            vendor = null;
            inArray = false;
            while (parser.hasNext()) {
                switch((event = parser.next())) {
                    case START_ARRAY:
                        inArray = true;
                        break;
                    case END_ARRAY:
                        inArray = false;
                        break;
                    case VALUE_STRING:
                        vendor = parser.getString();
                        if(inArray)
                            list.add(vendor);
                        //System.out.println("vendor - " + vendor);
                        break;
                }
            }
        }
        return list;
    }

    /**
     * Converts a json to a List of BearingCatalogVO Objects
     * @param content, String Object, json 
     * @return List of BearingCatalogVO Objects
     */
    public List<BearingCatalogVO> populateBearingCatalogVOFromJson(String content) {
        List<BearingCatalogVO> list = new ArrayList();
        String keyName;
        BearingCatalogVO bearingCatalogVO;
        boolean inArray;
         
        try {
            parser = Json.createParser(new StringReader(content));
            keyName = null;
            bearingCatalogVO = null;
            inArray = false;
            
            while (parser.hasNext()) {
                switch((event = parser.next())) {
                    case START_ARRAY:
                        inArray = true;
                        break;
                    case END_ARRAY:
                        inArray = false;
                        keyName = null;
                        bearingCatalogVO = null;
                        break;
                    case START_OBJECT:
                        bearingCatalogVO = inArray ? new BearingCatalogVO() : null;
                        break;
                    case END_OBJECT:
                        if(bearingCatalogVO != null && inArray)
                            list.add(bearingCatalogVO);
                        bearingCatalogVO = null;
                        keyName = null;
                        break;
                    case KEY_NAME:
                            switch(parser.getString()) {
                                case "customerAccount":
                                    keyName = "customerAccount";
                                    break;
                                case "siteId":
                                    keyName = "siteId";
                                    break;
                                case "vendorId":
                                    keyName = "vendorId";
                                    break;
                                case "bearingId":
                                    keyName = "bearingId";
                                    break;                                    
                                case "bc":
                                    keyName = "bc";
                                    break; 
                                case "ftf":
                                    keyName = "ftf";
                                    break;
                                case "bsf":
                                    keyName = "bsf";
                                    break;                                    
                                case "bpfo":
                                    keyName = "bpfo";
                                    break;
                                case "bpfi":
                                    keyName = "bpfi";
                                    break;
                                default:
                                    keyName = null;
                                    break;      
                            }
                        break;
                    case VALUE_STRING:
                        if(bearingCatalogVO != null && inArray && keyName != null) {
                            switch(keyName) {
                                case "customerAccount":
                                    bearingCatalogVO.setCustomerAcct(parser.getString());
                                    break;
                                case "siteId":
                                    bearingCatalogVO.setSiteId(UUID.fromString(parser.getString()));
                                    break;
                                case "vendorId":
                                    bearingCatalogVO.setVendorId(parser.getString());
                                    break;
                                case "bearingId":
                                    bearingCatalogVO.setBearingId(parser.getString());
                                    break;
                                
                            }
                        }
                        break;
                    case VALUE_NUMBER:
                        if(bearingCatalogVO != null && inArray && keyName != null) {
                            switch(keyName) {
                                case "bc":
                                    bearingCatalogVO.setBc(parser.getBigDecimal().shortValue());
                                    break;
                                case "ftf":
                                    bearingCatalogVO.setFtf(parser.getBigDecimal().floatValue());
                                    break;
                                case "bsf":
                                    bearingCatalogVO.setBsf(parser.getBigDecimal().floatValue());
                                    break;
                                case "bpfo":
                                    bearingCatalogVO.setBpfo(parser.getBigDecimal().floatValue());
                                    break;
                                case "bpfi":
                                    bearingCatalogVO.setBpfi(parser.getBigDecimal().floatValue());
                                    break;
                            }
                        }
                        break;                    
                }
            }
        } catch (Exception e) {
            Logger.getLogger(FaultFrequenciesSaxJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }
   
}
