/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.http.utils.json.sax;

import com.uptime.client.utils.SaxJsonParser;
import com.uptime.upcastcm.utils.vo.WorkOrderVO;
import java.io.StringReader;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;

/**
 *
 * @author twilcox
 */
public class WorkOrderSaxJsonParser extends SaxJsonParser  {
    private static WorkOrderSaxJsonParser instance = null;
    
    /**
     * Private Singleton class
     */
    private WorkOrderSaxJsonParser() {
    }

    /**
     * Returns an instance of the class
     * @return WorkOrderSaxJsonParser object
     */
    public static WorkOrderSaxJsonParser getInstance() {
        if (instance == null) {
            instance = new WorkOrderSaxJsonParser();
        }
        return instance; 
    }

    /**
     * Converts a json to a List Object of WorkOrderVO Objects
     * @param content, String Object, json 
     * @return List Object of WorkOrderVO Objects
     */
    public List<WorkOrderVO> populateWorkOrderVOFromJson(String content) {
        List<WorkOrderVO> list = new ArrayList();
        String keyName;
        WorkOrderVO workOrderVO;
        boolean inArray, inInnerObject;
         
        try {
            parser = Json.createParser(new StringReader(content));
            keyName = null;
            workOrderVO = null;
            inArray = false;
            
            while (parser.hasNext()) {
                switch((event = parser.next())) {
                    case START_ARRAY:
                        inArray = true;
                        break;
                    case END_ARRAY:
                        inArray = false;
                        keyName = null;
                        workOrderVO = null;
                        break;
                    case START_OBJECT:
                        workOrderVO = inArray ? new WorkOrderVO(): null;
                        break;
                    case END_OBJECT:
                        if(workOrderVO != null && inArray)
                            list.add(workOrderVO);
                        workOrderVO = null;
                        keyName = null;
                        break;
                    case KEY_NAME:
                            switch(parser.getString()) {
                                case "customerAccount":
                                    keyName = "customerAccount";
                                    break;
                                case "siteId":
                                    keyName = "siteId";
                                    break;
                                case "areaId":
                                    keyName = "areaId";
                                    break;
                                case "assetId":
                                    keyName = "assetId";
                                    break;
                                case "pointLocationId":
                                    keyName = "pointLocationId";
                                    break;
                                case "pointId":
                                    keyName = "pointId";
                                    break;
                                case "rowId":
                                    keyName = "rowId";
                                    break;
                                case "apSetId":
                                    keyName = "apSetId";
                                    break;
                                case "paramName":
                                    keyName = "paramName";
                                    break;
                                case "priority":
                                    keyName = "priority";
                                    break;
                                case "createdDate":
                                    keyName = "createdDate";
                                    break;
                                case "openedYear":
                                    keyName = "openedYear";
                                    break;
                                case "action":
                                    keyName = "action";
                                    break;
                                case "areaName":
                                    keyName = "areaName";
                                    break;
                                case "assetComponent":
                                    keyName = "assetComponent";
                                    break;
                                case "assetName":
                                    keyName = "assetName";
                                    break;
                                case "channelType":
                                    keyName = "channelType";
                                    break;
                                case "description":
                                    keyName = "description";
                                    break;
                                case "issue":
                                    keyName = "issue";
                                    break;
                                case "notes":
                                    keyName = "notes";
                                    break;
                                case "user":
                                    keyName = "user";
                                    break;
                                case "priority1":
                                    keyName = "priority1";
                                    break;
                                case "priority2":
                                    keyName = "priority2";
                                    break;
                                case "priority3":
                                    keyName = "priority3";
                                    break;
                                case "priority4":
                                    keyName = "priority4";
                                    break;
                                case "totalCount":
                                    keyName = "totalCount";
                                    break;
                                case "workOrderDetails":
                                    inInnerObject = false;

                                    do {
                                        if (parser.hasNext()) {
                                            switch((event = parser.next())) {
                                                case START_OBJECT:
                                                    inInnerObject = true;
                                                    break;
                                                case END_OBJECT:
                                                    inInnerObject = false;
                                                    break;
                                                case KEY_NAME:
                                                    switch(parser.getString()) {
                                                        case "areaName":
                                                            keyName = "areaName";
                                                            break;
                                                        case "assetComponent":
                                                            keyName = "assetComponent";
                                                            break;
                                                        case "assetName":
                                                            keyName = "assetName";
                                                            break;
                                                        case "priority":
                                                            keyName = "priority";
                                                            break;
                                                        case "closeDate":
                                                            keyName = "closeDate";
                                                            break;
                                                        case "action":
                                                            keyName = "action";
                                                            break;
                                                        case "description":
                                                            keyName = "description";
                                                            break;
                                                        case "issue":
                                                            keyName = "issue";
                                                            break;
                                                        case "notes":
                                                            keyName = "notes";
                                                            break;
                                                        case "user":
                                                            keyName = "user";
                                                            break;
                                                    }
                                                case VALUE_STRING:
                                                    if(workOrderVO != null && keyName != null) {
                                                        switch(keyName) {
                                                            case "closeDate":
                                                                try {
                                                                    workOrderVO.setCloseDate(LocalDateTime.parse(parser.getString(), DateTimeFormatter.ofPattern(DATE_TIME_OFFSET_FORMAT)).atZone(ZoneOffset.UTC).toInstant());
                                                                } catch (Exception ex) {}
                                                                break;
                                                            case "action":
                                                                workOrderVO.setAction(parser.getString());
                                                                break;
                                                            case "areaName":
                                                                workOrderVO.setAreaName(parser.getString());
                                                                break;
                                                            case "assetComponent":
                                                                workOrderVO.setAssetComponent(parser.getString());
                                                                break;
                                                            case "assetName":
                                                                workOrderVO.setAssetName(parser.getString());
                                                                break;
                                                            case "description":
                                                                workOrderVO.setDescription(parser.getString());
                                                                break;
                                                            case "issue":
                                                                workOrderVO.setIssue(parser.getString());
                                                                break;
                                                            case "notes":
                                                                workOrderVO.setNotes(parser.getString());
                                                                break;
                                                            case "user":
                                                                workOrderVO.setUser(parser.getString());
                                                                break;
                                                            case "priority":
                                                                try {
                                                                    workOrderVO.setPriority(Byte.parseByte(parser.getString()));
                                                                } catch (Exception ex) {}
                                                                break; 
                                                        }
                                                    }
                                                    break;
                                            }
                                        }
                                    } while (inInnerObject == true);
                                    
                                    keyName = null;
                                    break;
                                default:
                                    keyName = null;
                                    break;      
                            }
                        break;
                    case VALUE_STRING:
                        if(workOrderVO != null && inArray && keyName != null) {
                            switch(keyName) {
                                case "customerAccount":
                                    workOrderVO.setCustomerAccount(parser.getString());
                                    break;
                                case "siteId":
                                    try {
                                        workOrderVO.setSiteId(UUID.fromString(parser.getString()));
                                    } catch (Exception ex) {}
                                    break;
                                case "areaId":
                                    try {
                                        workOrderVO.setAreaId(UUID.fromString(parser.getString()));
                                    } catch (Exception ex) {}
                                    break;
                                case "assetId":
                                    try {
                                        workOrderVO.setAssetId(UUID.fromString(parser.getString()));
                                    } catch (Exception ex) {}
                                    break;
                                case "pointLocationId":
                                    try {
                                        workOrderVO.setPointLocationId(UUID.fromString(parser.getString()));
                                    } catch (Exception ex) {}
                                    break;
                                case "pointId":
                                    try {
                                        workOrderVO.setPointId(UUID.fromString(parser.getString()));
                                    } catch (Exception ex) {}
                                    break;
                                case "rowId":
                                    try {
                                        workOrderVO.setRowId(UUID.fromString(parser.getString()));
                                    } catch (Exception ex) {}
                                    break;
                                case "apSetId":
                                    try {
                                        workOrderVO.setApSetId(UUID.fromString(parser.getString()));
                                    } catch (Exception ex) {}
                                    break;
                                case "paramName":
                                    workOrderVO.setParamName(parser.getString());
                                    break;
                                case "createdDate":
                                    try {
                                        workOrderVO.setCreatedDate(LocalDateTime.parse(parser.getString(), DateTimeFormatter.ofPattern(DATE_TIME_OFFSET_FORMAT)).atZone(ZoneOffset.UTC).toInstant());
                                    } catch (Exception ex) {}
                                    break;
                                case "action":
                                    workOrderVO.setAction(parser.getString());
                                    break;
                                case "areaName":
                                    workOrderVO.setAreaName(parser.getString());
                                    break;
                                case "assetComponent":
                                    workOrderVO.setAssetComponent(parser.getString());
                                    break;
                                case "assetName":
                                    workOrderVO.setAssetName(parser.getString());
                                    break;
                                case "channelType":
                                    workOrderVO.setChannelType(parser.getString());
                                    break;
                                case "description":
                                    workOrderVO.setDescription(parser.getString());
                                    break;
                                case "issue":
                                    workOrderVO.setIssue(parser.getString());
                                    break;
                                case "notes":
                                    workOrderVO.setNotes(parser.getString());
                                    break;
                                case "user":
                                    workOrderVO.setUser(parser.getString());
                                    break;
                            }
                        }
                        break;
                    case VALUE_NUMBER:
                        if(workOrderVO != null && inArray && keyName != null) {
                            switch(keyName) {
                                case "priority":
                                    workOrderVO.setPriority((byte)parser.getInt());
                                    break;
                                case "openedYear":
                                    workOrderVO.setOpenedYear((short)parser.getInt());
                                    break;
                                case "priority1":
                                    workOrderVO.setPriority1(parser.getInt());
                                    break;
                                case "priority2":
                                    workOrderVO.setPriority2(parser.getInt());
                                    break;
                                case "priority3":
                                    workOrderVO.setPriority3(parser.getInt());
                                    break;
                                case "priority4":
                                    workOrderVO.setPriority4(parser.getInt());
                                    break;
                                case "totalCount":
                                    workOrderVO.setTotalCount(parser.getInt());
                                    break; 
                            }
                        }
                        break;
                }
            }
        } catch (Exception e) {
            Logger.getLogger(WorkOrderSaxJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }
}
