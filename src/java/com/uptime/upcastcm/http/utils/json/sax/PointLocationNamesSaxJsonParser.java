/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.http.utils.json.sax;

import com.uptime.client.utils.SaxJsonParser;
import com.uptime.upcastcm.utils.vo.PointLocationNamesVO;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;

/**
 *
 * @author Mohd Juned Alam
 */
public class PointLocationNamesSaxJsonParser extends SaxJsonParser {

    private static PointLocationNamesSaxJsonParser instance = null;

    /**
     * Private Singleton class
     */
    private PointLocationNamesSaxJsonParser() {
    }

    /**
     * Returns an instance of the class
     *
     * @return PointLocationNamesSaxJsonParser object
     */
    public static PointLocationNamesSaxJsonParser getInstance() {
        if (instance == null) {
            instance = new PointLocationNamesSaxJsonParser();
        }
        return instance;
    }

    /**
     * Converts a json to a List Object of PointLocationNamesVO Objects
     *
     * @param content, String Object, json
     * @return List Object of PointLocationNamesVO Objects
     */
    public List<PointLocationNamesVO> populatePointLocationNamesFromJson(String content) {
        List<PointLocationNamesVO> list = new ArrayList();
        String keyName;
        PointLocationNamesVO pointLocationNamesVO;
        boolean inArray;

        try {
            parser = Json.createParser(new StringReader(content));
            keyName = null;
            pointLocationNamesVO = null;
            inArray = false;

            while (parser.hasNext()) {
                switch ((event = parser.next())) {
                    case START_ARRAY:
                        inArray = true;
                        break;
                    case END_ARRAY:
                        inArray = false;
                        keyName = null;
                        pointLocationNamesVO = null;
                        break;
                    case START_OBJECT:
                        pointLocationNamesVO = inArray ? new PointLocationNamesVO() : null;
                        break;
                    case END_OBJECT:
                        if (pointLocationNamesVO != null && inArray) {
                            list.add(pointLocationNamesVO);
                        }
                        pointLocationNamesVO = null;
                        keyName = null;
                        break;
                    case KEY_NAME:
                        switch (parser.getString()) {
                            case "customerAccount":
                                keyName = "customerAccount";
                                break;
                            case "siteId":
                                keyName = "siteId";
                                break;
                            case "pointLocationName":
                                keyName = "pointLocationName";
                                break;
                            case "description":
                                keyName = "description";
                                break;
                            default:
                                keyName = null;
                                break;
                        }
                        break;
                    case VALUE_STRING:
                        if (pointLocationNamesVO != null && inArray && keyName != null) {
                            switch (keyName) {
                                case "customerAccount":
                                    pointLocationNamesVO.setCustomerAccount(parser.getString());
                                    break;
                                case "siteId":
                                    pointLocationNamesVO.setSiteId(UUID.fromString(parser.getString()));
                                    break;
                                case "pointLocationName":
                                    pointLocationNamesVO.setPointLocationName(parser.getString());
                                    break;
                                case "description":
                                    pointLocationNamesVO.setDescription(parser.getString());
                                    break;
                            }
                        }
                        break;
                }
            }
        } catch (Exception e) {
            Logger.getLogger(PointLocationNamesSaxJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }
}
