/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.http.utils.json.dom;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.uptime.client.utils.DomJsonParser;
import com.uptime.upcastcm.utils.vo.AirbaseStatusVO;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author gopal
 */
public class SystemStatusDomJsonParser extends DomJsonParser {
    private static SystemStatusDomJsonParser instance = null;

    /**
     * Private Singleton class
     */
    private SystemStatusDomJsonParser() {
    }

    /**
     * Returns an instance of the class
     *
     * @return SystemStatusDomJsonParser object
     */
    public static SystemStatusDomJsonParser getInstance() {
        if (instance == null) {
            instance = new SystemStatusDomJsonParser();
        }
        return instance;
    }

    public List<AirbaseStatusVO> parseAirbaseStatusJson(String result, String timezone) {
        List<AirbaseStatusVO> list = new ArrayList();
        AirbaseStatusVO airbaseStatusVO;
        JsonObject obj;
        int startIndex, endIndex;
        String subString, truncatedTZ;

        try {  
            if (result != null && (element = JsonParser.parseString(result)) != null && (jsonObject = element.getAsJsonObject()) != null && jsonObject.has("baseStations")) {
                array = jsonObject.get("baseStations").getAsJsonArray();
                if (array != null && array.size() > 0) {
                    if (timezone != null && !timezone.isEmpty()) {
                        if (timezone.contains("(") && timezone.contains(")")) {
                            startIndex = timezone.indexOf("(");
                            endIndex = timezone.indexOf(")");
                            subString = timezone.substring(startIndex, endIndex + 1);
                            truncatedTZ = timezone.replace(subString, "").trim();
                        } else {
                            truncatedTZ = timezone;
                        }
                        
                        for (int j = 0; j < array.size(); j++) {
                            airbaseStatusVO = new AirbaseStatusVO();
                            if (jsonObject.has("customerId")) {
                                airbaseStatusVO.setCustomerId(jsonObject.get("customerId").getAsString());
                            }
                            if (jsonObject.has("siteName")) {
                                airbaseStatusVO.setSiteName(jsonObject.get("siteName").getAsString());
                            }
                            
                            obj = array.get(j).getAsJsonObject();
                            if (obj.has("baseStationId")) {
                                airbaseStatusVO.setBaseStationId(obj.get("baseStationId").getAsString());
                            }
                            if (obj.has("lastSampleReceived")) {
                                try {
                                    airbaseStatusVO.setLastSampleReceived(LocalDateTime.parse(obj.get("lastSampleReceived").getAsString(), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS")).atZone(ZoneId.of(truncatedTZ)).toInstant());
                                } catch (Exception e) {
                                    Logger.getLogger(SystemStatusDomJsonParser.class.getName()).log(Level.SEVERE, e.getMessage(), e);
                                }
                            }
                            list.add(airbaseStatusVO);
                        }
                    } else {
                        for (int j = 0; j < array.size(); j++) {
                            airbaseStatusVO = new AirbaseStatusVO();
                            if (jsonObject.has("customerId")) {
                                airbaseStatusVO.setCustomerId(jsonObject.get("customerId").getAsString());
                            }
                            if (jsonObject.has("siteName")) {
                                airbaseStatusVO.setSiteName(jsonObject.get("siteName").getAsString());
                            }
                            
                            obj = array.get(j).getAsJsonObject();
                            if (obj.has("baseStationId")) {
                                airbaseStatusVO.setBaseStationId(obj.get("baseStationId").getAsString());
                            }
                            if (obj.has("lastSampleReceived")) {
                                try {
                                    airbaseStatusVO.setLastSampleReceived(LocalDateTime.parse(obj.get("lastSampleReceived").getAsString(), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS")).atZone(ZoneOffset.UTC).toInstant());
                                } catch (Exception e) {
                                    Logger.getLogger(SystemStatusDomJsonParser.class.getName()).log(Level.SEVERE, e.getMessage(), e);
                                }
                            }
                            list.add(airbaseStatusVO);
                        }
                    }
                }
            }
        } catch (Exception e) {
            Logger.getLogger(SystemStatusDomJsonParser.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        
        return list;
    }

    public static void main(String[] args) {
        SystemStatusDomJsonParser sp = new SystemStatusDomJsonParser();
        String json = "{\"customerId\":\"FEDEX EXPRESS\",\"siteName\":\"MEMPHIS\",\"baseStations\":[{\"baseStationId\":\"ABOLJAX01\",\"lastSampleReceived\":\"2024-04-10 12:52:55.000\"}, {\"baseStationId\":\"ABOLJAX02\",\"lastSampleReceived\":\"2024-02-10 12:52:55.000\"}]}";
        sp.parseAirbaseStatusJson(json, "America/New_York (UTC -05:00)");
    }
}
