/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.http.utils.json.dom;

import com.uptime.client.utils.DomJsonParser;
import com.uptime.client.utils.JsonConverterUtil;
import com.uptime.upcastcm.utils.vo.AssetVO;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author madhavi
 */
public class AssetDomJsonParser extends DomJsonParser {
    private static AssetDomJsonParser instance = null;

    /**
     * Private Singleton class
     */
    private AssetDomJsonParser() {
    }

    /**
     * Returns an instance of the class
     *
     * @return AssetDomJsonParser object
     */
    public static AssetDomJsonParser getInstance() {
        if (instance == null) {
            instance = new AssetDomJsonParser();
        }
        return instance;
    }

    /**
     * Convert a AssetVO Object to a String Object json
     *
     * @param assetVO, AssetVO Object
     * @return String Object
     */
    public String getJsonFromAssetVO(AssetVO assetVO) {
        try {
            if (assetVO != null) {
                return JsonConverterUtil.toJSON(assetVO);
            }
        } catch (Exception e) {
            Logger.getLogger(AssetDomJsonParser.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
        return null;
    }

    /**
     * Convert a List of AssetVO Objects to a String Object json
     *
     * @param assetVOList, List of AssetVO Objects
     * @return String Object
     */
    public String getJsonFromAssetVOList(List<AssetVO> assetVOList) {
        try {
            if (assetVOList != null && !assetVOList.isEmpty()) {
                return JsonConverterUtil.toJSON(assetVOList);
            }
        } catch (Exception e) {
            Logger.getLogger(AssetDomJsonParser.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
        return null;
    }
}
