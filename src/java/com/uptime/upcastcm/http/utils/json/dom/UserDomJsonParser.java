/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.http.utils.json.dom;

import com.google.gson.JsonParser;
import com.uptime.client.utils.DomJsonParser;
import com.uptime.client.utils.JsonConverterUtil;
import com.uptime.upcastcm.utils.vo.AlarmNotificationVO;
import com.uptime.upcastcm.utils.vo.LdapUserVO;
import com.uptime.upcastcm.utils.vo.PasswordTokenVO;
import com.uptime.upcastcm.utils.vo.SiteUsersVO;
import com.uptime.upcastcm.utils.vo.UserPreferencesVO;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author twilcox
 */
public class UserDomJsonParser extends DomJsonParser {
    private static UserDomJsonParser instance = null;

    /**
     * Private Singleton class
     */
    private UserDomJsonParser() {
    }

    /**
     * Returns an instance of the class
     *
     * @return UserDomJsonParser object
     */
    public static UserDomJsonParser getInstance() {
        if (instance == null) {
            instance = new UserDomJsonParser();
        }
        return instance;
    }

    /**
     * Convert a UserPreferencesVO Object to a String Object json
     *
     * @param userPreferencesVO, UserPreferencesVO Object
     * @return String Object
     */
    public String getJsonFromObject(UserPreferencesVO userPreferencesVO) {
        try {
            if (userPreferencesVO != null) {
                return JsonConverterUtil.toJSON(userPreferencesVO);
            }
        } catch (Exception e) {
            Logger.getLogger(UserDomJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
        }
        return null;
    }

    /**
     * Convert a AlarmNotificationVO Object to a String Object json
     *
     * @param alarmNotificationVO, AlarmNotificationVO Object
     * @return String Object
     */
    public String getJsonFromObject(AlarmNotificationVO alarmNotificationVO) {
        try {
            if (alarmNotificationVO != null) {
                return JsonConverterUtil.toJSON(alarmNotificationVO);
            }
        } catch (Exception e) {
            Logger.getLogger(UserDomJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
        }
        return null;
    }

    /**
     * Convert a PasswordTokenVO Object to a String Object json
     *
     * @param passwordTokenVO, PasswordTokenVO Object
     * @return String Object
     */
    public String getJsonFromObject(PasswordTokenVO passwordTokenVO) {
        try {
            if (passwordTokenVO != null) {
                return JsonConverterUtil.toJSON(passwordTokenVO);
            }
        } catch (Exception e) {
            Logger.getLogger(UserDomJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
        }
        return null;
    }

    /**
     * Convert a SiteUsersVO Object to a String Object json
     *
     * @param siteUsersVO, SiteUsersVO Object
     * @return String Object
     */
    public String getJsonFromObject(SiteUsersVO siteUsersVO) {
        try {
            if (siteUsersVO != null) {
                return JsonConverterUtil.toJSON(siteUsersVO);
            }
        } catch (Exception e) {
            Logger.getLogger(UserDomJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
        }
        return null;
    }

    /**
     * Convert a LdapUserVO Object to a String Object json
     *
     * @param ldapUserVO, LdapUserVO Object
     * @param requestType, String Object
     * @return String Object
     */
    public String getJsonFromObject(LdapUserVO ldapUserVO, String requestType) {
        StringBuilder sb;
        
        try {
            sb = new StringBuilder();
            sb.append("{\"request\":\"").append(requestType).append("\",\"attributes\":");
            if (ldapUserVO != null) {
                sb.append(JsonConverterUtil.toJSON(ldapUserVO));
            }
            sb.append("}");
            return sb.toString();
        } catch (Exception e) {
            Logger.getLogger(UserDomJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
        }
        return null;
    }
    
     /**
     * Convert a LdapUserVO Object to a String Object json
     *
     * @param ldapUserVO, LdapUserVO Object
     * @return String Object
     */
    public String getLdapAuthJsonFromObject(LdapUserVO ldapUserVO) {
        StringBuilder sb;
        
        try {
            if (ldapUserVO != null) {
                sb = new StringBuilder();
                sb
                    .append("{")
                    .append("\"request\":\"authenticateUserAccountAndGetGroupMemberships\",")
                    .append("\"user_id\":\"").append(ldapUserVO.getUserId()).append("\",")
                    .append("\"pwd\":\"").append(ldapUserVO.getUserPassword()).append("\",")
                    .append("\"company\":\"").append(ldapUserVO.getCompany()).append("\",")
                    .append("\"customerAccount\":\"").append(ldapUserVO.getCustomerAccount()).append("\"")
                    .append("}");
                return sb.toString();
            }
        } catch (Exception e) {
            Logger.getLogger(UserDomJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
        }
        return null;
    }

    /**
     * Parse given JSON for PasswordToken check
     *
     * @param content, String Object
     * @return Boolean Object, true if found, false if not found, and null if
     * Error Occured
     */
    public Boolean getPasswordTokenCheckFromJson(String content) {
        try {
            if ((element = JsonParser.parseString(content)) != null && (jsonObject = element.getAsJsonObject()) != null && jsonObject.has("found")) {
                return jsonObject.get("found").getAsBoolean();
            }
        } catch (Exception e) {
            Logger.getLogger(UserDomJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
        }
        return null;
    }
}
