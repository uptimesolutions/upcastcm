/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.http.utils.json.dom;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.uptime.client.utils.DomJsonParser;
import com.uptime.upcastcm.utils.vo.EmailVO;

/**
 *
 * @author Mohd Juned Alam
 */
public class EmailDomJsonParser extends DomJsonParser {
    private static EmailDomJsonParser instance = null;
    
    /**
     * Private Singleton class
     */
    private EmailDomJsonParser() {
    }

    /**
     * Returns an instance of the class
     * 
     * @return EmailDomJsonParser object
     */
    public static EmailDomJsonParser getInstance() {
        if (instance == null) {
            instance = new EmailDomJsonParser();
        }
        return instance; 
    }

    /**
     * Convert an EmailVO Object to a String Object json
     *
     * @param emailVO, EmailVO Object
     * @return String Object
     */
    public String getEmailJsonData(EmailVO emailVO) {
        JsonObject emailJson;
        jsonObject = new JsonObject();
        array = new JsonArray();
        
        if (emailVO != null) {
            if (emailVO.getFromEmail() != null && !emailVO.getFromEmail().isEmpty()) {
                jsonObject.addProperty("from_email", emailVO.getFromEmail());
            }
            
            if (emailVO.getSubject() != null && !emailVO.getSubject().isEmpty()) {
                jsonObject.addProperty("subject", emailVO.getSubject());
            }
            
            if (emailVO.getMailBody() != null && !emailVO.getMailBody().isEmpty()) {
                jsonObject.addProperty("body", emailVO.getMailBody());
            }

            if (emailVO.getToEmails() != null && !emailVO.getToEmails().isEmpty()) {
                for (String email : emailVO.getToEmails()) {
                    emailJson = new JsonObject();
                    emailJson.addProperty("to_email", email);
                    array.add(emailJson);
                }
                jsonObject.add("to_emails", array);
            }
        }
        return jsonObject.toString();
    }
}
