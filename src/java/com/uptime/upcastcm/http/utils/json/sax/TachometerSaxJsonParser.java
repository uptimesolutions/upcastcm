/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.http.utils.json.sax;

import com.uptime.client.utils.SaxJsonParser;
import com.uptime.upcastcm.utils.vo.TachometerVO;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;

/**
 *
 * @author twilcox
 */
public class TachometerSaxJsonParser extends SaxJsonParser {
    private static TachometerSaxJsonParser instance = null;
    
    /**
     * Private Singleton class
     */
    private TachometerSaxJsonParser() {
    }

    /**
     * Returns an instance of the class
     * @return TachometerSaxJsonParser object
     */
    public static TachometerSaxJsonParser getInstance() {
        if (instance == null) {
            instance = new TachometerSaxJsonParser();
        }
        return instance; 
    }

    /**
     * Converts a json to a List Object of TachometerVO Objects
     * @param content, String Object, json 
     * @return List Object of TachometerVO Objects
     */
    public List<TachometerVO> populateTachometersFromJson(String content) {
        List<TachometerVO> list = new ArrayList();
        String keyName;
        TachometerVO tachometerVO;
        boolean inArray;
         
        try {
            parser = Json.createParser(new StringReader(content));
            keyName = null;
            tachometerVO = null;
            inArray = false;
            
            while (parser.hasNext()) {
                switch((event = parser.next())) {
                    case START_ARRAY:
                        inArray = true;
                        break;
                    case END_ARRAY:
                        inArray = false;
                        keyName = null;
                        tachometerVO = null;
                        break;
                    case START_OBJECT:
                        tachometerVO = inArray ? new TachometerVO() : null;
                        break;
                    case END_OBJECT:
                        if(tachometerVO != null && inArray)
                            list.add(tachometerVO);
                        tachometerVO = null;
                        keyName = null;
                        break;
                    case KEY_NAME:
                            switch(parser.getString()) {
                                case "customerAccount":
                                    keyName = "customerAccount";
                                    break;
                                case "tachName":
                                    keyName = "tachName";
                                    break;
                                case "tachId":
                                    keyName = "tachId";
                                    break;
                                case "siteName":
                                    keyName = "siteName";
                                    break; 
                                case "siteId":
                                    keyName = "siteId";
                                    break; 
                                case "tachHardwareUnitChannel":
                                    keyName = "tachHardwareUnitChannel";
                                    break; 
                                case "tachHardwareUnitSerialNumber":
                                    keyName = "tachHardwareUnitSerialNumber";
                                    break; 
                                case "tachMaxRpm":
                                    keyName = "tachMaxRpm";
                                    break; 
                                case "tachMinRpm":
                                    keyName = "tachMinRpm";
                                    break; 
                                case "tachPulsesPerRev":
                                    keyName = "tachPulsesPerRev";
                                    break; 
                                case "tachReferenceSpeed":
                                    keyName = "tachReferenceSpeed";
                                    break;
                                case "tachReferenceUnits":
                                    keyName = "tachReferenceUnits";
                                    break;
                                case "tachRisingEdge":
                                    keyName = "tachRisingEdge";
                                    break; 
                                case "tachSpeedRatio":
                                    keyName = "tachSpeedRatio";
                                    break; 
                                case "tachTriggerPercent":
                                    keyName = "tachTriggerPercent";
                                    break; 
                                case "tachType": 
                                    keyName = "tachType";
                                    break;                              
                                default:
                                    keyName = null;
                                    break;      
                            }
                        break;
                    case VALUE_STRING:
                        if(tachometerVO != null && inArray && keyName != null) {
                            switch(keyName) {
                                case "customerAccount":
                                    tachometerVO.setCustomerAccount(parser.getString());
                                    break;
                                case "tachName":
                                    tachometerVO.setTachName(parser.getString());
                                    break;
                                case "tachId":
                                    tachometerVO.setTachId(UUID.fromString(parser.getString()));
                                    break;
                                case "siteName":
                                    tachometerVO.setSiteName(parser.getString());
                                    break;
                                case "siteId":
                                    tachometerVO.setSiteId(UUID.fromString(parser.getString()));
                                    break;                                
                                case "tachType":
                                    tachometerVO.setTachType(parser.getString());
                                    break;
                                case "tachReferenceUnits":
                                    tachometerVO.setTachReferenceUnits(parser.getString());
                                    break;
                            }
                        }
                        break;
                    case VALUE_NUMBER:
                        if(tachometerVO != null && inArray && keyName != null) {
                            switch(keyName) {
                                case "tachReferenceSpeed":
                                    tachometerVO.setTachReferenceSpeed(parser.getBigDecimal().floatValue());;
                                    break;   
                            }
                        }
                        break;
                    case VALUE_TRUE:
                        if(tachometerVO != null && inArray && keyName != null) {
                            switch(keyName) {
                            }
                        }
                        break;
                    case VALUE_FALSE:
                        if(tachometerVO != null && inArray && keyName != null) {
                            switch(keyName) {
                            }
                        }
                        break;                    
                }
            }
        } catch (Exception e) {
            Logger.getLogger(TachometerSaxJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }
}
