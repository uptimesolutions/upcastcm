/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.http.utils.json.sax;

import com.uptime.client.utils.SaxJsonParser;
import com.uptime.upcastcm.utils.vo.ApAlSetVO;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;

/**
 *
 * @author madhavi
 */
public class APALSetSaxJsonParser extends SaxJsonParser {
    private static APALSetSaxJsonParser instance = null;
    
    /**
     * Private Singleton class
     */
    private APALSetSaxJsonParser() {
    }

    /**
     * Returns an instance of the class
     * @return APALSetSaxJsonParser object
     */
    public static APALSetSaxJsonParser getInstance() {
        if (instance == null) {
            instance = new APALSetSaxJsonParser();
        }
        return instance; 
    }

    /**
     * Converts a json to a List Object of ApAlSetVO Objects
     * @param content, String Object, json 
     * @return List Object of ApAlSetVO Objects
     */
    public List<ApAlSetVO> populateApAlSetVOFromJson(String content) {
        List<ApAlSetVO> list = new ArrayList();
        String keyName;
        ApAlSetVO apAlSensorTypeVO;
        boolean inArray;
         
        try {
            parser = Json.createParser(new StringReader(content));
            keyName = null;
            apAlSensorTypeVO = null;
            inArray = false;
            
            while (parser.hasNext()) {
                switch((event = parser.next())) {
                    case START_ARRAY:
                        inArray = true;
                        break;
                    case END_ARRAY:
                        inArray = false;
                        keyName = null;
                        apAlSensorTypeVO = null;
                        break;
                    case START_OBJECT:
                        apAlSensorTypeVO = inArray ? new ApAlSetVO(): null;
                        break;
                    case END_OBJECT:
                        if(apAlSensorTypeVO != null && inArray)
                            list.add(apAlSensorTypeVO);
                        apAlSensorTypeVO = null;
                        keyName = null;
                        break;
                    case KEY_NAME:
                            switch(parser.getString()) {
                                case "customerAccount":
                                    keyName = "customerAccount";
                                    break;
                                case "sensorType":
                                    keyName = "sensorType";
                                    break;
                                case "apSetName":
                                    keyName = "apSetName";
                                    break;
                                case "apSetId":
                                    keyName = "apSetId";
                                    break;
                                case "alSetName":
                                    keyName = "alSetName";
                                    break;
                                case "alSetId":
                                    keyName = "alSetId";
                                    break;
                                case "paramName":
                                    keyName = "paramName";
                                    break;
                                case "siteName":
                                    keyName = "siteName";
                                    break;
                                case "siteId":
                                    keyName = "siteId";
                                    break;
                                case "fmax":
                                    keyName = "fmax";
                                    break;
                                case "resolution":
                                    keyName = "resolution";
                                    break;
                                case "period":
                                    keyName = "period";
                                    break;
                                case "sampleRate":
                                    keyName = "sampleRate";
                                    break;
                                case "demod":
                                    keyName = "demod";
                                    break;
                                case "demodHighPass":
                                    keyName = "demodHighPass";
                                    break;
                                case "demodLowPass":
                                    keyName = "demodLowPass";
                                    break;
                                case "paramType":
                                    keyName = "paramType";
                                    break;
                                case "frequencyUnits":
                                    keyName = "frequencyUnits";
                                    break;
                                case "paramUnits":
                                    keyName = "paramUnits";
                                    break;
                                case "paramAmpFactor":
                                    keyName = "paramAmpFactor";
                                    break;
                                case "minFrequency":
                                    keyName = "minFrequency";
                                    break;
                                case "maxFrequency":
                                    keyName = "maxFrequency";
                                    break;
                                case "dwell":
                                    keyName = "dwell";
                                    break;
                                case "lowFaultActive":
                                    keyName = "lowFaultActive";
                                    break;
                                case "lowAlertActive":
                                    keyName = "lowAlertActive";
                                    break;
                                case "highAlertActive":
                                    keyName = "highAlertActive";
                                    break;
                                case "highFaultActive":
                                    keyName = "highFaultActive";
                                    break;
                                case "lowFault":
                                    keyName = "lowFault";
                                    break;
                                case "lowAlert":
                                    keyName = "lowAlert";
                                    break;
                                case "highAlert":
                                    keyName = "highAlert";
                                    break;
                                case "highFault":
                                    keyName = "highFault";
                                    break;
                                default:
                                    keyName = null;
                                    break;      
                            }
                        break;
                    case VALUE_STRING:
                        if(apAlSensorTypeVO != null && inArray && keyName != null) {
                            switch(keyName) {
                                case "customerAccount":
                                    apAlSensorTypeVO.setCustomerAccount(parser.getString());
                                    break;
                                case "sensorType":
                                    apAlSensorTypeVO.setSensorType(parser.getString());
                                    break;
                                case "apSetName":
                                    apAlSensorTypeVO.setApSetName(parser.getString());
                                    break;
                                case "apSetId":
                                    apAlSensorTypeVO.setApSetId(UUID.fromString(parser.getString()));
                                    break;
                                case "alSetName":
                                    apAlSensorTypeVO.setAlSetName(parser.getString());
                                    break;
                                case "alSetId":
                                    apAlSensorTypeVO.setAlSetId(UUID.fromString(parser.getString()));
                                    break;
                                case "paramName":
                                    apAlSensorTypeVO.setParamName(parser.getString());
                                    break;
                                case "siteName":
                                    apAlSensorTypeVO.setSiteName(parser.getString());
                                    break;
                                case "siteId":
                                    apAlSensorTypeVO.setSiteId(UUID.fromString(parser.getString()));
                                    break;
                                case "paramType":
                                    apAlSensorTypeVO.setParamType(parser.getString());
                                    break;
                                case "frequencyUnits":
                                    apAlSensorTypeVO.setFrequencyUnits(parser.getString());
                                    break;
                                case "paramUnits":
                                    apAlSensorTypeVO.setParamUnits(parser.getString());
                                    break;
                                case "paramAmpFactor":
                                    apAlSensorTypeVO.setParamAmpFactor(parser.getString());
                                    break;
                            }
                        }
                        break;
                    case VALUE_NUMBER:
                        if(apAlSensorTypeVO != null && inArray && keyName != null) {
                            switch(keyName) {
                                case "fmax":
                                    apAlSensorTypeVO.setFmax(parser.getInt());
                                    break;
                                case "resolution":
                                    apAlSensorTypeVO.setResolution(parser.getInt());
                                    break;
                                case "period":
                                    apAlSensorTypeVO.setPeriod(parser.getBigDecimal().floatValue());
                                    break;
                                case "sampleRate":
                                    apAlSensorTypeVO.setSampleRate(parser.getBigDecimal().floatValue());
                                    break;
                                case "demodHighPass":
                                    apAlSensorTypeVO.setDemodHighPass(parser.getBigDecimal().floatValue());
                                    break;
                                case "demodLowPass":
                                    apAlSensorTypeVO.setDemodLowPass(parser.getBigDecimal().floatValue());
                                    break;
                                case "minFrequency":
                                    apAlSensorTypeVO.setMinFrequency(parser.getBigDecimal().floatValue());
                                    break;
                                case "maxFrequency":
                                    apAlSensorTypeVO.setMaxFrequency(parser.getBigDecimal().floatValue());
                                    break;
                                case "dwell":
                                    apAlSensorTypeVO.setDwell(parser.getInt());
                                    break;
                                case "lowFault":
                                    apAlSensorTypeVO.setLowFault(parser.getBigDecimal().floatValue());
                                    break;
                                case "lowAlert":
                                    apAlSensorTypeVO.setLowAlert(parser.getBigDecimal().floatValue());
                                    break;
                                case "highAlert":
                                    apAlSensorTypeVO.setHighAlert(parser.getBigDecimal().floatValue());
                                    break;
                                case "highFault":
                                    apAlSensorTypeVO.setHighFault(parser.getBigDecimal().floatValue());
                                    break;
                            }
                        }
                        break;
                    case VALUE_TRUE:
                        if(apAlSensorTypeVO != null && inArray && keyName != null) {
                            switch(keyName) {
                                case "demod":
                                    apAlSensorTypeVO.setDemod(true);
                                    break;
                                case "lowFaultActive":
                                    apAlSensorTypeVO.setLowFaultActive(true);
                                    break;
                                case "lowAlertActive":
                                    apAlSensorTypeVO.setLowAlertActive(true);
                                    break;
                                case "highAlertActive":
                                    apAlSensorTypeVO.setHighAlertActive(true);
                                    break;
                                case "highFaultActive":
                                    apAlSensorTypeVO.setHighFaultActive(true);
                                    break;
                                
                            }
                        }
                        break;
                    case VALUE_FALSE:
                        if(apAlSensorTypeVO != null && inArray && keyName != null) {
                            switch(keyName) {
                                case "demod":
                                    apAlSensorTypeVO.setDemod(false);
                                    break;
                                case "lowFaultActive":
                                    apAlSensorTypeVO.setLowFaultActive(false);
                                    break;
                                case "lowAlertActive":
                                    apAlSensorTypeVO.setLowAlertActive(false);
                                    break;
                                case "highAlertActive":
                                    apAlSensorTypeVO.setHighAlertActive(false);
                                    break;
                                case "highFaultActive":
                                    apAlSensorTypeVO.setHighFaultActive(false);
                                    break;
                            }
                        }
                        break;
                }
            }
        } catch (Exception e) {
            Logger.getLogger(APALSetSaxJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }
}
