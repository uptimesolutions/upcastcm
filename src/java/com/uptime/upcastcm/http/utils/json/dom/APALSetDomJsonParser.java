/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.http.utils.json.dom;

import com.uptime.client.utils.DomJsonParser;
import com.uptime.client.utils.JsonConverterUtil;
import com.uptime.upcastcm.utils.vo.ApAlSetVO;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author twilcox
 */
public class APALSetDomJsonParser extends DomJsonParser {
    private static APALSetDomJsonParser instance = null;
    
    /**
     * Private Singleton class
     */
    private APALSetDomJsonParser() {
    }

    /**
     * Returns an instance of the class
     * @return APALSetDomJsonParser object
     */
    public static APALSetDomJsonParser getInstance() {
        if (instance == null) {
            instance = new APALSetDomJsonParser();
        }
        return instance; 
    }

    /**
     * Convert a ApAlSetVO Object to a String Object json
     * @param apAlSetVO, ApAlSetVO Object
     * @return String Object
     */
    public String getJsonFromApAlSet(ApAlSetVO apAlSetVO) {
        try {
            if (apAlSetVO != null) {
                return JsonConverterUtil.toJSON(apAlSetVO);
            }
        } catch (Exception e) {
            Logger.getLogger(APALSetDomJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
        }
        return null;
    }
}
