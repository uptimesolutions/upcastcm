/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.http.utils.json.dom;

import com.uptime.client.utils.DomJsonParser;
import com.uptime.client.utils.JsonConverterUtil;
import com.uptime.upcastcm.utils.vo.AlarmsVO;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author gsingh
 */
public class AlarmsDomJsonParser extends DomJsonParser {
    private static AlarmsDomJsonParser instance = null;

    /**
     * Private Singleton class
     */
    private AlarmsDomJsonParser() {
    }

    /**
     * Returns an instance of the class
     *
     * @return AlarmsDomJsonParser object
     */
    public static AlarmsDomJsonParser getInstance() {
        if (instance == null) {
            instance = new AlarmsDomJsonParser();
        }
        return instance;
    }

    /**
     * Convert a AlarmsVO Object to a String Object json
     *
     * @param alarmsVO, AlarmsVO Object
     * @return String Object
     */
    public String getJsonFromAlarms(AlarmsVO alarmsVO) {
        try {
            if (alarmsVO != null) {
                return JsonConverterUtil.toJSON(alarmsVO);
            }
        } catch (Exception e) {
            Logger.getLogger(AlarmsDomJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
        }
        return null;
    }

    /**
     * Convert a List of AlarmsVO Object to a String Object json
     *
     * @param alarmsVOList, List Object of AlarmsVO Objects
     * @return String Object
     */
    public String getJsonFromAlarmsList(List<AlarmsVO> alarmsVOList) {
        StringBuilder json = new StringBuilder();
        
        try {
            if (alarmsVOList != null && !alarmsVOList.isEmpty()) {
                return JsonConverterUtil.toJSON(alarmsVOList);
            }
        } catch (Exception e) {
            Logger.getLogger(AlarmsDomJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
        }
        return json.toString();
    }
}
