/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.http.utils.json.sax;

import com.uptime.client.utils.SaxJsonParser;
import com.uptime.upcastcm.utils.vo.AssetPresetVO;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;

/**
 *
 * @author joseph
 */
public class AssetPresetSaxJsonParser extends SaxJsonParser {
    
    private static AssetPresetSaxJsonParser instance = null;

    /**
     * Private Singleton class
     */
    private AssetPresetSaxJsonParser() {
    }

    /**
     * Returns an instance of the class
     *
     * @return AssetPresetSaxJsonParser object
     */
    public static AssetPresetSaxJsonParser getInstance() {
        if (instance == null) {
            instance = new AssetPresetSaxJsonParser();
        }
        return instance;
    }
    
      /**
     * Converts a json to a List Object of AssetPresetVO Objects
     *
     * @param content, String Object, json
     * @return List Object of AssetPresetVO Objects
     */
    public List<AssetPresetVO> populateAssetPresetsFromJson(String content) {
        List<AssetPresetVO> list = new ArrayList();
        String keyName;
        AssetPresetVO assetPresetVO;
        boolean inArray;

        try {
            parser = Json.createParser(new StringReader(content));
            keyName = null;
            assetPresetVO = null;
            inArray = false;

            while (parser.hasNext()) {
                switch ((event = parser.next())) {
                    case START_ARRAY:
                        inArray = true;
                        break;
                    case END_ARRAY:
                        inArray = false;
                        keyName = null;
                        assetPresetVO = null;
                        break;
                    case START_OBJECT:
                        assetPresetVO = inArray ? new AssetPresetVO() : null;
                        break;
                    case END_OBJECT:
                        if (assetPresetVO != null && inArray) {
                            list.add(assetPresetVO);
                        }
                        assetPresetVO = null;
                        keyName = null;
                        break;
                    case KEY_NAME:
                        switch (parser.getString()) {
                            case "customerAccount":
                                keyName = "customerAccount";
                                break;
                            case "siteId":
                                keyName = "siteId";
                                break;
                             case "assetTypeName":
                                keyName = "assetTypeName";
                                break;
                            case "assetPresetId":
                                keyName = "assetPresetId";
                                break;
                            case "pointLocationName":
                                keyName = "pointLocationName";
                                break;
                            case "assetPresetName":
                                keyName = "assetPresetName";
                                break;
                            case "devicePresetId":
                                keyName = "devicePresetId";
                                break;                          
                            case "sampleInterval":
                                keyName = "sampleInterval";
                                break;
                            case "ffSetIds":
                                keyName = "ffSetIds";
                                break;
                            case "rollDiameter":
                                keyName = "rollDiameter";
                                break;
                            case "rollDiameterUnits":
                                keyName = "rollDiameterUnits";
                                break;
                            case "speedRatio":
                                keyName = "speedRatio";
                                break;
                            case "tachId":
                                keyName = "tachId";
                                break;
                            case "description":
                                keyName = "description";
                                break; 
                            case "devicePresetType":
                                keyName = "devicePresetType";
                                break; 
                            case "devicePresetName":
                                keyName ="devicePresetName";
                                break;
                            case "tachName":
                                keyName = "tachName";
                                break;
                            default:
                                keyName = null;
                                break;
                        }
                        break;
                    case VALUE_STRING:
                        if (assetPresetVO != null && inArray && keyName != null) {
                            switch (keyName) {
                                case "customerAccount":
                                    assetPresetVO.setCustomerAccount(parser.getString());
                                    break;                                        
                                case "siteId":
                                    try {
                                    assetPresetVO.setSiteId(UUID.fromString(parser.getString()));
                                    } catch (Exception ex) {
                                    }
                                    break;    
                                case "assetTypeName":
                                     assetPresetVO.setAssetTypeName(parser.getString());
                                     break;
                                case "assetPresetId":
                                    assetPresetVO.setAssetPresetId(UUID.fromString(parser.getString()));
                                    break;    
                                case "pointLocationName":
                                    assetPresetVO.setPointLocationName(parser.getString());
                                    break;
                                case "assetPresetName":
                                    assetPresetVO.setAssetPresetName(parser.getString());
                                    break;
                                case "devicePresetId":
                                    assetPresetVO.setDevicePresetId(UUID.fromString(parser.getString()));
                                    break;
                                case "ffSetIds":
                                    assetPresetVO.setFfSetIds(parser.getString());
                                    break;
                                case "tachId":
                                    assetPresetVO.setTachId(UUID.fromString(parser.getString()));
                                    break;
                                case "rollDiameterUnits":
                                    assetPresetVO.setRollDiameterUnits(parser.getString());
                                    break;
                                case "description":
                                    assetPresetVO.setDescription(parser.getString());
                                 break;
                                case "devicePresetType":
                                    assetPresetVO.setDevicePresetType(parser.getString());
                                    break;
                                case "devicePresetName":
                                    assetPresetVO.setDevicePresetName(parser.getString());
                                    break;
                                case "tachName":
                                    assetPresetVO.setTachName(parser.getString());
                            }
                        }
                        break;
                    case VALUE_NUMBER:
                        if (assetPresetVO != null && inArray && keyName != null) {
                            switch (keyName) {                                
                                case "sampleInterval":
                                    assetPresetVO.setSampleInterval(parser.getInt());
                                    break;       
                                case "rollDiameter":
                                    assetPresetVO.setRollDiameter(Float.parseFloat(parser.getString()));
                                    break;
                                case "speedRatio":
                                    assetPresetVO.setSpeedRatio(Float.parseFloat(parser.getString()));
                                    break;
                            }
                        }
                        break;
                    case VALUE_TRUE:
                        if (assetPresetVO != null && inArray && keyName != null) {
                            switch (keyName) {
                               
                            }
                        } 
                        break;
                    case VALUE_FALSE:
                        if (assetPresetVO != null && inArray && keyName != null) {
                            switch (keyName) {
                               
                            }
                        } 
                        break;
                }
            }
        } catch (Exception e) {
            Logger.getLogger(AlarmsSaxJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }
}
