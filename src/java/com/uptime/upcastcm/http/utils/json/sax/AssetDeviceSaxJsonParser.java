/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.http.utils.json.sax;

import com.uptime.client.utils.SaxJsonParser;
import com.uptime.upcastcm.utils.vo.AssetDeviceVO;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;

/**
 *
 * @author madhavi
 */
public class AssetDeviceSaxJsonParser extends SaxJsonParser {
    private static AssetDeviceSaxJsonParser instance = null;
    
    /**
     * Private Singleton class
     */
    private AssetDeviceSaxJsonParser() {
    }

    /**
     * Returns an instance of the class
     * @return AssetDeviceSaxJsonParser object
     */
    public static AssetDeviceSaxJsonParser getInstance() {
        if (instance == null) {
            instance = new AssetDeviceSaxJsonParser();
        }
        return instance; 
    }

    /**
     * Converts a json to a List Object of AssetDeviceVO Objects
     * @param content, String Object, json 
     * @return List Object of AssetDeviceVO Objects
     */
    public List<AssetDeviceVO> populateAssetDeviceVOFromJson(String content) {
        List<AssetDeviceVO> list = new ArrayList();
        String keyName;
        AssetDeviceVO assetDeviceVO;
        boolean inArray;
       
        try {
            parser = Json.createParser(new StringReader(content));
            keyName = null;
            assetDeviceVO = null;
            inArray = false;
            
            while (parser.hasNext()) {
                switch((event = parser.next())) {
                    case START_ARRAY:
                        inArray = true;
                        break;
                    case END_ARRAY:
                        inArray = false;
                        keyName = null;
                        assetDeviceVO = null;
                        break;
                    case START_OBJECT:
                        assetDeviceVO = inArray ? new AssetDeviceVO() : null;
                        break;
                    case END_OBJECT:
                        if(assetDeviceVO != null && inArray)
                            list.add(assetDeviceVO);
                        assetDeviceVO = null;
                        keyName = null;
                        break;
                    case KEY_NAME:
                            switch(parser.getString()) {
                                
                                case "asset_id":
                                    keyName = "asset_id";
                                    break;
                                case "device_id":
                                    keyName = "device_id";
                                    break; 
                                default:
                                    keyName = null;
                                    break;      
                            }
                        break;
                    case VALUE_STRING:
                        if(assetDeviceVO != null && inArray && keyName != null) {
                            switch(keyName) {
                                case "asset_id":
                                    assetDeviceVO.setAssetId(UUID.fromString(parser.getString()));
                                    break;
                                case "device_id":
                                    assetDeviceVO.setDeviceId(parser.getString());
                                    break;                               
                            }
                        }
                        break;
                }
            }
        } catch (Exception e) {
            Logger.getLogger(AssetDeviceSaxJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }
}
