/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.http.utils.json.sax;

import com.uptime.client.utils.SaxJsonParser;
import com.uptime.upcastcm.utils.vo.HwUnitVO;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;

/**
 *
 * @author kpati
 */
public class HwUnitSaxJsonParser extends SaxJsonParser {

    private static HwUnitSaxJsonParser instance = null;

    /**
     * Private Singleton class
     */
    private HwUnitSaxJsonParser() {
    }

    /**
     * Returns an instance of the class
     *
     * @return HwUnitSaxJsonParser object
     */
    public static HwUnitSaxJsonParser getInstance() {
        if (instance == null) {
            instance = new HwUnitSaxJsonParser();
        }
        return instance;
    }

    /**
     * Converts a json to a List Object of AlarmsVO Objects
     *
     * @param content, String Object, json
     * @return List Object of HwUnitVO Objects
     */
    public List<HwUnitVO> populateHwUnitFromJson(String content) {
        List<HwUnitVO> list = new ArrayList();
        String keyName;
        HwUnitVO hwUnitVO;
        boolean inArray;

        try {
            parser = Json.createParser(new StringReader(content));
            keyName = null;
            hwUnitVO = null;
            inArray = false;

            while (parser.hasNext()) {
                switch ((event = parser.next())) {
                    case START_ARRAY:
                        inArray = true;
                        break;
                    case END_ARRAY:
                        inArray = false;
                        keyName = null;
                        hwUnitVO = null;
                        break;
                    case START_OBJECT:
                        hwUnitVO = inArray ? new HwUnitVO() : null;
                        break;
                    case END_OBJECT:
                        if (hwUnitVO != null && inArray) {
                            list.add(hwUnitVO);
                        }
                        hwUnitVO = null;
                        keyName = null;
                        break;
                    case KEY_NAME:
                        switch (parser.getString()) {
                            case "deviceId":
                                keyName = "deviceId";
                                break;
                            case "customerAcct":
                                keyName = "customerAcct";
                                break;
                            case "channelType":
                                keyName = "channelType";
                                break;
                            case "channelNum":
                                keyName = "channelNum";
                                break;
                            case "siteId":
                                keyName = "siteId";
                                break;
                            case "areaId":
                                keyName = "areaId";
                                break;
                            case "assetId":
                                keyName = "assetId";
                                break;
                            case "pointLocationId":
                                keyName = "pointLocationId";
                                break;
                            case "pointId":
                                keyName = "pointId";
                                break;
                            case "apSetId":
                                keyName = "apSetId";
                                break;
                            default:
                                keyName = null;
                                break;
                        }
                        break;
                    case VALUE_STRING:
                        if (hwUnitVO != null && inArray && keyName != null) {
                            switch (keyName) {
                                case "siteId":
                                    try {
                                    hwUnitVO.setSiteId(UUID.fromString(parser.getString()));
                                } catch (Exception ex) {
                                }
                                break;
                                case "areaId":
                                    try {
                                    hwUnitVO.setAreaId(UUID.fromString(parser.getString()));
                                } catch (Exception ex) {
                                }
                                break;
                                case "assetId":
                                    try {
                                    hwUnitVO.setAssetId(UUID.fromString(parser.getString()));
                                } catch (Exception ex) {
                                }
                                break;
                                case "pointLocationId":
                                    try {
                                    hwUnitVO.setPointLocationId(UUID.fromString(parser.getString()));
                                } catch (Exception ex) {
                                }
                                break;
                                case "pointId":
                                    try {
                                    hwUnitVO.setPointId(UUID.fromString(parser.getString()));
                                } catch (Exception ex) {
                                }
                                break;
                                case "apSetId":
                                    try {
                                    hwUnitVO.setApSetId(UUID.fromString(parser.getString()));
                                } catch (Exception ex) {
                                }
                                break;
                                case "deviceId":
                                    hwUnitVO.setExistingDeviceId(parser.getString());
                                    break;
                                case "customerAcct":
                                    hwUnitVO.setCustomerAccount(parser.getString());
                                    break;
                                case "channelType":
                                    hwUnitVO.setChannelType(parser.getString());
                                    break;
                            }
                        }
                        break;
                    case VALUE_NUMBER:
                        if (hwUnitVO != null && inArray && keyName != null) {
                            switch (keyName) {
                                case "channelNum":
                                    hwUnitVO.setChannelNum(parser.getBigDecimal().byteValue());
                                    break;
                            }
                        }
                        break;
                }
            }
        } catch (Exception e) {
            Logger.getLogger(AlarmsSaxJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }

}
