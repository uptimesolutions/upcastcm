/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.http.utils.json.sax;

import com.uptime.client.utils.SaxJsonParser;
import com.uptime.upcastcm.utils.vo.SubscriptionsVO;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;

/**
 *
 * @author madhavi
 */
public class SubscriptionsSaxJsonParser extends SaxJsonParser {
    private static SubscriptionsSaxJsonParser instance = null;
    
    /**
     * Private Singleton class
     */
    private SubscriptionsSaxJsonParser() {
    }

    /**
     * Returns an instance of the class
     * @return SubscriptionsSaxJsonParser object
     */
    public static SubscriptionsSaxJsonParser getInstance() {
        if (instance == null) {
            instance = new SubscriptionsSaxJsonParser();
        }
        return instance; 
    }
    
    /**
     * Converts a json to a List Object of SubscriptionsVO Objects
     * @param content, String Object, json 
     * @return List Object of SubscriptionsVO Objects
     */
    public List<SubscriptionsVO> populateSubscriptionsFromJson(String content) {
        List<SubscriptionsVO> list = new ArrayList();
        String keyName;
        SubscriptionsVO subscriptionsVO;
        boolean inArray;
         
        try {
            parser = Json.createParser(new StringReader(content));
            keyName = null;
            subscriptionsVO = null;
            inArray = false;
            
            while (parser.hasNext()) {
                switch((event = parser.next())) {
                    case START_ARRAY:
                        inArray = true;
                        break;
                    case END_ARRAY:
                        inArray = false;
                        keyName = null;
                        subscriptionsVO = null;
                        break;
                    case START_OBJECT:
                        subscriptionsVO = inArray ? new SubscriptionsVO() : null;
                        break;
                    case END_OBJECT:
                        if(subscriptionsVO != null && inArray)
                            list.add(subscriptionsVO);
                        subscriptionsVO = null;
                        keyName = null;
                        break;
                    case KEY_NAME:
                            switch(parser.getString()) {
                                case "customerAccount":
                                    keyName = "customerAccount";
                                    break;
                                case "siteId":
                                    keyName = "siteId";
                                    break;
                                case "subscriptionType":
                                    keyName = "subscriptionType";
                                    break;
                                case "mqProtocol":
                                    keyName = "mqProtocol";
                                    break;
                                case "mqConnectString":
                                    keyName = "mqConnectString";
                                    break;
                                case "mqUser":
                                    keyName = "mqUser";
                                    break;
                                case "mqPwd":
                                    keyName = "mqPwd";
                                    break;
                                case "mqQueueName":
                                    keyName = "mqQueueName";
                                    break;
                                case "mqClientId":
                                    keyName = "mqClientId";
                                    break;
                                case "internal":
                                    keyName = "internal";
                                    break;
                                case "webhookUrl":
                                    keyName = "webhookUrl";
                                    break;
                                default:
                                    keyName = null;
                                    break;      
                            }
                        break;
                    case VALUE_STRING:
                        if(subscriptionsVO != null && inArray && keyName != null) {
                            switch(keyName) {
                                case "customerAccount":
                                    subscriptionsVO.setCustomerAccount(parser.getString());
                                    break;
                                case "siteId":
                                    subscriptionsVO.setSiteId(UUID.fromString(parser.getString()));
                                    break;
                                case "subscriptionType":
                                    subscriptionsVO.setSubscriptionType(parser.getString());
                                    break;
                                case "mqProtocol":
                                    subscriptionsVO.setMqProtocol(parser.getString());
                                    break;
                                case "mqConnectString":
                                    subscriptionsVO.setMqConnectString(parser.getString());
                                    break;
                                case "mqUser":
                                    subscriptionsVO.setMqUser(parser.getString());
                                    break;
                                case "mqPwd":
                                    subscriptionsVO.setMqPwd(parser.getString());
                                    break;
                                case "mqQueueName":
                                    subscriptionsVO.setMqQueueName(parser.getString());
                                    break;
                                case "mqClientId":
                                    subscriptionsVO.setMqClientId(parser.getString());
                                    break;
                                case "webhookUrl":
                                    subscriptionsVO.setWebhookUrl(parser.getString());
                                    break;
                            }
                        }
                        break;
                    case VALUE_TRUE:
                        if (subscriptionsVO != null && inArray && keyName != null) {
                            switch (keyName) {
                                case "internal":
                                    subscriptionsVO.setInternal(true);
                                    break;
                            }
                        }
                        break;
                    case VALUE_FALSE:
                        if (subscriptionsVO != null && inArray && keyName != null) {
                            switch (keyName) {
                                case "internal":
                                    subscriptionsVO.setInternal(false);
                                    break;
                            }
                        }
                        break;
                }
            }
        } catch (Exception e) {
            Logger.getLogger(SubscriptionsSaxJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }

}
