/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.http.utils.json.dom;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.uptime.client.utils.DomJsonParser;
import com.uptime.client.utils.JsonConverterUtil;
import com.uptime.upcastcm.utils.vo.FaultFrequenciesVO;
import com.uptime.upcastcm.utils.vo.PointLocationVO;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author twilcox
 */
public class PointLocationDomJsonParser extends DomJsonParser {
    private static PointLocationDomJsonParser instance = null;

    /**
     * Private Singleton class
     */
    private PointLocationDomJsonParser() {
    }

    /**
     * Returns an instance of the class
     *
     * @return PointLocationDomJsonParser object
     */
    public static PointLocationDomJsonParser getInstance() {
        if (instance == null) {
            instance = new PointLocationDomJsonParser();
        }
        return instance;
    }

    /**
     * Convert a AreaVO Object to a String Object json
     *
     * @param pointLocationVO, PointLocationVO Object
     * @return String Object
     */
    public String getJsonFromPointLocation(PointLocationVO pointLocationVO) {
        try {
            if (pointLocationVO != null) {
                return JsonConverterUtil.toJSON(pointLocationVO);
            }
        } catch (Exception e) {
            Logger.getLogger(PointLocationDomJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
        }
        return null;
    }

    /**
     * Convert a DeviceVO Object to a String Object json
     *
     * @param pointLocationVO, PointLocationVO Object
     * @param requestType, String Object
     * @return String Object
     */
    public String getJsonFromObject(PointLocationVO pointLocationVO, String requestType) {
        StringBuilder sb;
        
        try {
            sb = new StringBuilder();
            sb.append("{\"request\":\"").append(requestType).append("\",\"attributes\":");
            if (pointLocationVO != null) {
                sb.append(JsonConverterUtil.toJSON(pointLocationVO));
            }
            sb.append("}");
            return sb.toString();
        } catch (Exception e) {
            Logger.getLogger(PointLocationDomJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
        }
        return null;
    }

    /**
     * Convert the given json String Object and return a Set of String objects
     *
     * @param content, String Object
     * @param siteFFSetFavorites, List Object of FaultFrequenciesVO Objects
     * @return Set of String Objects
     */
    public Set<String> populateFFSetFromJson(String content, List<FaultFrequenciesVO> siteFFSetFavorites) {
        Set<String> ffsetNames = new HashSet();
        JsonObject innerJsonObject, freqJsonObj;
        JsonArray innerJsonArray;
        String ffsetName, units, freqName;
        float freqVal;
        
        try {
            if (content != null && !content.isEmpty() && (element = JsonParser.parseString(content)) != null && (jsonObject = element.getAsJsonObject()) != null) {
                if (jsonObject.has("ff_sets")) {
                    if ((innerJsonArray = jsonObject.getAsJsonArray("ff_sets")) != null && !innerJsonArray.isEmpty()) {
                        for (int l = 0; l < innerJsonArray.size(); l++) {
                            innerJsonObject = innerJsonArray.get(l).getAsJsonObject();

                            if (innerJsonObject.has("source") && innerJsonObject.get("source").getAsString().trim().equalsIgnoreCase("Bearing Catalog")) {
                                try {
                                    ffsetNames.add(innerJsonObject.get("ff_set_name").getAsString().trim());
                                } catch (Exception ex) {
                                }
                            }
                            
                            if (innerJsonObject.has("source") && !innerJsonObject.get("source").getAsString().trim().equalsIgnoreCase("Bearing Catalog")) {
                                try {
                                    freqJsonObj = innerJsonObject.getAsJsonArray("frequencies").get(0).getAsJsonObject();
                                    ffsetName = innerJsonObject.get("ff_set_name").getAsString().trim();
                                    units = innerJsonObject.get("units").getAsString().trim();
                                    freqName = null;
                                    freqVal = 0;

                                    for (String key : freqJsonObj.keySet()) {
                                        freqName = key;
                                        freqVal = freqJsonObj.get(key).getAsFloat();
                                    }

                                    for (FaultFrequenciesVO freq : siteFFSetFavorites) {
                                        if (freq.getFfSetName().equalsIgnoreCase(ffsetName)
                                                && freq.getFfUnit().equalsIgnoreCase(units)
                                                && freq.getFfName().equalsIgnoreCase(freqName)
                                                && Objects.equals(freq.getFfValue(), freqVal)) {
                                            ffsetNames.add(freq.getFfSetId().toString());
                                            break;
                                        }
                                    }
                                } catch (Exception e) {
                                    Logger.getLogger(PointLocationDomJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
                                }
                            }
                        }
                    }

                }
            }
        } catch (Exception e) {
            Logger.getLogger(PointLocationDomJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
            ffsetNames = new HashSet();
        }
        return ffsetNames;
    }

    /**
     * Parse the given json String Object and return a List of String objects
     *
     * @param content, String Object
     * @return List of String Objects
     */
    public List<String> populateFFSetFromJson(String content) {
        List<String> ffsetNames = new ArrayList();
        JsonObject innerJsonObject;
        JsonArray innerJsonArray;
        
        try {
            if (content != null && !content.isEmpty() && (element = JsonParser.parseString(content)) != null && (jsonObject = element.getAsJsonObject()) != null) {
                if (jsonObject.has("ff_sets")) {
                    if ((innerJsonArray = jsonObject.getAsJsonArray("ff_sets")) != null && !innerJsonArray.isEmpty()) {
                        for (int l = 0; l < innerJsonArray.size(); l++) {
                            innerJsonObject = innerJsonArray.get(l).getAsJsonObject();

                            if (innerJsonObject.has("source") && innerJsonObject.get("source").getAsString().trim().equalsIgnoreCase("Bearing Catalog")) {
                                try {
                                    ffsetNames.add(innerJsonObject.get("ff_set_name").getAsString().trim());
                                } catch (Exception ex) {
                                }
                            }
                            
                            if (innerJsonObject.has("source") && !innerJsonObject.get("source").getAsString().trim().equalsIgnoreCase("Bearing Catalog")) {
                                try {
                                    ffsetNames.add(innerJsonObject.get("ff_set_name").getAsString().trim());
                                } catch (Exception ex) {
                                }
                            }
                        }
                    }

                }
            }
        } catch (Exception e) {
            Logger.getLogger(PointLocationDomJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
            ffsetNames = new ArrayList();
        }
        return ffsetNames;
    }

    /**
     * Convert the given Items to a json String Object
     *
     * @param ffSetNames, Set Object of String Object
     * @param siteFFSetFavorites, List Object of FaultFrequenciesVO Object
     * @return String Object
     */
    public String getJsonFromSelectedPointLocation(Set<String> ffSetNames, List<FaultFrequenciesVO> siteFFSetFavorites) {
        List<FaultFrequenciesVO> ffVOs = null;
        List<String> ffSetNameList;
        StringBuilder json;
        String ffSetItem;
        UUID ffSetId;
        
        json = new StringBuilder();
        try {
            json.append("{\"ff_sets\":[");
            
            if (ffSetNames != null) {
                ffSetNameList = new ArrayList(ffSetNames);
                for (int i = 0; i < ffSetNameList.size(); i++) {
                    json.append("{");

                    if ((ffSetItem = ffSetNameList.get(i)) != null && siteFFSetFavorites != null) {

                        // Check if ffSetItem is a UUID Object
                        try {
                            ffSetId = UUID.fromString(ffSetItem);
                        } catch (Exception e) {
                            ffSetId = null;
                        }

                        // ffSetItem isn't a UUID Object
                        // Bearing Catalog
                        if (ffSetId == null) { 
                            ffVOs = new ArrayList();
                            for (FaultFrequenciesVO faultFrequenciesVO : siteFFSetFavorites) {
                                if (faultFrequenciesVO.getFfSetName().equalsIgnoreCase(ffSetItem)) {
                                    ffVOs.add(faultFrequenciesVO);
                                }
                            }

                            json
                                .append("\"ff_set_name\":\"").append(ffVOs.get(0).getFfSetName()).append("\",")
                                .append("\"source\":\"Bearing Catalog\",")
                                .append("\"units\":\"Orders\",");
                        } 

                        // ffSetItem is a UUID Object
                        // Custom ff set
                        else {
                            ffVOs = new ArrayList();
                            for (FaultFrequenciesVO faultFrequenciesVO : siteFFSetFavorites) {
                                if (Objects.equals(faultFrequenciesVO.getFfSetId(), ffSetId)) {
                                    ffVOs.add(faultFrequenciesVO);
                                }
                            }

                            if (ffVOs.get(0).isGlobal()) {
                                json
                                    .append("\"ff_set_name\":\"").append(ffVOs.get(0).getFfSetName()).append("\",")
                                    .append("\"source\":\"Global FF Set\",")
                                    .append("\"units\":\"").append(ffVOs.get(0).getFfUnit()).append("\",");
                            } else {
                                json
                                    .append("\"ff_set_name\":\"").append(ffVOs.get(0).getFfSetName()).append("\",")
                                    .append("\"source\":\"Site FF Set\",").append(" \"units\":\"")
                                    .append(ffVOs.get(0).getFfUnit()).append("\",");
                            }
                        }

                    }

                    json.append("\"frequencies\":[{");
                    if (ffVOs != null) {
                        for (int j = 0; j < ffVOs.size(); j++) {
                            json.append("\"").append(ffVOs.get(j).getFfName()).append("\":").append(ffVOs.get(j).getFfValue()).append(j < ffVOs.size() - 1 ? "," : "");
                        }
                    }
                    json.append("}]}").append(i < ffSetNameList.size() - 1 ? "," : "");
                }
            }
            json.append("]}");
        } catch (Exception e) {
            Logger.getLogger(PointLocationDomJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
            json.append("{\"ff_sets\":[]}");
        }
        
        return json.toString();
    }
}
