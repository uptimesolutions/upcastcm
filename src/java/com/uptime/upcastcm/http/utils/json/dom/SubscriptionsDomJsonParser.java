/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.http.utils.json.dom;

import com.uptime.client.utils.DomJsonParser;
import com.uptime.client.utils.JsonConverterUtil;
import com.uptime.upcastcm.utils.vo.SubscriptionsVO;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author madhavi
 */
public class SubscriptionsDomJsonParser extends DomJsonParser {
    private static SubscriptionsDomJsonParser instance = null;

    /**
     * Private Singleton class
     */
    private SubscriptionsDomJsonParser() {
    }

    /**
     * Returns an instance of the class
     *
     * @return public class SubscriptionsDomJsonParser extends DomJsonParser {
 object
     */
    public static SubscriptionsDomJsonParser getInstance() {
        if (instance == null) {
            instance = new SubscriptionsDomJsonParser();
        }
        return instance;
    }

    /**
     * Convert a SubscriptionsVO Object to a String Object json
     *
     * @param subscriptionsVO, SubscriptionsVO Object
     * @return String Object
     */
    public String getJsonFromSubscriptions(SubscriptionsVO subscriptionsVO) {
        try {
            if (subscriptionsVO != null) {
                return JsonConverterUtil.toJSON(subscriptionsVO);
            }
        } catch (Exception e) {
            Logger.getLogger(SubscriptionsDomJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
        }
        return null;
    }

    /**
     * Convert a SubscriptionsVO Object to a String Object json
     *
     * @param subscriptionsList, List of SubscriptionsVO Objects
     * @return String Object
     */
    public String getKafkaJsonFromSubscriptions(List<SubscriptionsVO> subscriptionsList) {
        StringBuilder json;
        try {
            if (subscriptionsList != null) {
                json = new StringBuilder();
                json
                    .append("{\"customerId\":\"").append(subscriptionsList.get(0).getCustomerAccount()).append("\",")
                    .append("\"siteId\":\"").append(subscriptionsList.get(0).getSiteId()).append("\",")
                    .append("\"subscriptions\":[");
                    for (int i = 0; i < subscriptionsList.size(); i++) {
                        json.append("{");
                        json.append("\"subscriptionType\":\"").append(subscriptionsList.get(i).getSubscriptionType()).append("\",");
                        json.append("\"mqProtocol\":\"").append(subscriptionsList.get(i).getMqProtocol()).append("\",");
                        json.append("\"mqConnectString\":\"").append(subscriptionsList.get(i).getMqConnectString()).append("\",");
                        json.append("\"mqUser\":\"").append(subscriptionsList.get(i).getMqUser()).append("\",");
                        json.append("\"mqPwd\":\"").append(subscriptionsList.get(i).getMqPwd()).append("\",");
                        json.append("\"mqQueueName\":\"").append(subscriptionsList.get(i).getMqQueueName()).append("\",");
                        json.append("\"mqClientId\":\"").append(subscriptionsList.get(i).getMqClientId()).append("\",");
                        json.append("\"isInternal\":").append(subscriptionsList.get(i).isInternal());
                        json.append("}");
                        json.append(i < subscriptionsList.size() - 1 ? "," : "");
                    }
                    json.append("]}");
                    return json.toString();
            }
        } catch (Exception e) {
            Logger.getLogger(SubscriptionsDomJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
        }
        return null;
    }

}
