/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.http.utils.json.dom;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.uptime.client.utils.DomJsonParser;
import com.uptime.client.utils.JsonConverterUtil;
import com.uptime.upcastcm.utils.vo.ApAlSetVO;
import com.uptime.upcastcm.utils.vo.AssetVO;
import com.uptime.upcastcm.utils.vo.ChannelVO;
import com.uptime.upcastcm.utils.vo.DeviceVO;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author kpati
 */
public class ManageDeviceDomJsonParser extends DomJsonParser {
    private static ManageDeviceDomJsonParser instance = null;

    /**
     * Private Singleton class
     */
    private ManageDeviceDomJsonParser() {
    }

    /**
     * Returns an instance of the class
     *
     * @return DeviceVODomJsonParser object
     */
    public static ManageDeviceDomJsonParser getInstance() {
        if (instance == null) {
            instance = new ManageDeviceDomJsonParser();
        }
        return instance;
    }

    /**
     * Convert a DeviceVO Object to a String Object json
     *
     * @param deviceVO, DeviceVO Object
     * @return String Object
     */
    public String getJsonFromDeviceVO(DeviceVO deviceVO) {
        try {
            if (deviceVO != null) {
                return JsonConverterUtil.toJSON(deviceVO);
            }
        } catch (Exception e) {
            Logger.getLogger(ManageDeviceDomJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
        }
        return null;
    }

    /**
     * Convert a List of AssetVO Objects to a String Object json
     *
     * @param assetVOList, List of AssetVO Objects
     * @return String Object
     */
    public String getJsonFromAssetVOList(List<AssetVO> assetVOList) {
        try {
            if (assetVOList != null && !assetVOList.isEmpty()) {
                return JsonConverterUtil.toJSON(assetVOList);
            }
        } catch (Exception e) {
            Logger.getLogger(ManageDeviceDomJsonParser.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
        return null;
    }

    /**
     * Convert a DeviceVO Object to a String Object json
     *
     * @param deviceVO, LdapUserVO Object
     * @param requestType, String Object
     * @return String Object
     */
    public String getJsonFromObject(DeviceVO deviceVO, String requestType) {
        StringBuilder sb;

        try {
            sb = new StringBuilder();
            sb.append("{\"request\":\"").append(requestType).append("\",\"attributes\":");
            if (deviceVO != null) {
                sb.append(JsonConverterUtil.toJSON(deviceVO));
            }
            sb.append("}");
            return sb.toString();
        } catch (Exception e) {
            Logger.getLogger(ManageDeviceDomJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
        }

        return null;
    }

    /**
     * Parse the given json String Object and return a List of ChannelVO objects
     *
     * @param content, String Object
     * @return List of ChannelVO Objects
     */
    public List<ChannelVO> populateManageDeviceRowsFromVerboseJson(String content) {
        List<ChannelVO> channelVOList = new ArrayList();
        ChannelVO channelVO;
        JsonObject innerJsonObject;
        JsonArray innerJsonArray;
        List<ApAlSetVO> apAlByPointVOList;
        ApAlSetVO apAlSetVO;
        boolean disabled = false;

        try {
            if (content != null && !content.isEmpty() && (element = JsonParser.parseString(content)) != null && (jsonObject = element.getAsJsonObject()) != null) {
                if (jsonObject.has("disabled")) {
                    try {
                        disabled = jsonObject.get("disabled").getAsBoolean();
                    } catch (Exception ex) {
                    }
                }
                if (jsonObject.has("channelVOs")) {
                    if ((array = jsonObject.getAsJsonArray("channelVOs")) != null && !array.isEmpty()) {
                        for (int i = 0; i < array.size(); i++) {
                            jsonObject = array.get(i).getAsJsonObject();
                            channelVO = new ChannelVO();
                            channelVO.setHwDisabled(disabled);

                            if (jsonObject.has("customerAccount")) {
                                try {
                                    channelVO.setCustomerAccount(jsonObject.get("customerAccount").getAsString().trim());
                                } catch (Exception ex) {
                                }
                            }
                            if (jsonObject.has("deviceId")) {
                                try {
                                    channelVO.setDeviceId(jsonObject.get("deviceId").getAsString().trim());
                                } catch (Exception ex) {
                                }
                            }
                            if (jsonObject.has("siteId")) {
                                try {
                                    channelVO.setSiteId(UUID.fromString(jsonObject.get("siteId").getAsString()));
                                } catch (Exception ex) {
                                }
                            }
                            if (jsonObject.has("areaId")) {
                                try {
                                    channelVO.setAreaId(UUID.fromString(jsonObject.get("areaId").getAsString()));
                                } catch (Exception ex) {
                                }
                            }
                            if (jsonObject.has("areaName")) {
                                try {
                                    channelVO.setAreaName(jsonObject.get("areaName").getAsString());
                                } catch (Exception ex) {
                                }
                            }
                            if (jsonObject.has("assetId")) {
                                try {
                                    channelVO.setAssetId(UUID.fromString(jsonObject.get("assetId").getAsString()));
                                } catch (Exception ex) {
                                }
                            }
                            if (jsonObject.has("assetName")) {
                                try {
                                    channelVO.setAssetName(jsonObject.get("assetName").getAsString());
                                } catch (Exception ex) {
                                }
                            }
                            if (jsonObject.has("pointLocationId")) {
                                try {
                                    channelVO.setPointLocationId(UUID.fromString(jsonObject.get("pointLocationId").getAsString()));
                                } catch (Exception ex) {
                                }
                            }
                            if (jsonObject.has("pointLocationName")) {
                                try {
                                    channelVO.setPointLocationName(jsonObject.get("pointLocationName").getAsString());
                                } catch (Exception ex) {
                                }
                            }
                            if (jsonObject.has("pointLocationDesc")) {
                                try {
                                    channelVO.setPointLocationDesc(jsonObject.get("pointLocationDesc").getAsString());
                                } catch (Exception ex) {
                                }
                            }
                            if (jsonObject.has("pointId")) {
                                try {
                                    channelVO.setPointId(UUID.fromString(jsonObject.get("pointId").getAsString()));
                                } catch (Exception ex) {
                                }
                            }
                            if (jsonObject.has("pointName")) {
                                try {
                                    channelVO.setPointName(jsonObject.get("pointName").getAsString());
                                } catch (Exception ex) {
                                }
                            }
                            if (jsonObject.has("apSetId")) {
                                try {
                                    channelVO.setApSetId(UUID.fromString(jsonObject.get("apSetId").getAsString()));
                                } catch (Exception ex) {
                                }
                            }
                            if (jsonObject.has("apSetName")) {
                                try {
                                    channelVO.setApSetName(jsonObject.get("apSetName").getAsString());
                                } catch (Exception ex) {
                                }
                            }
                            if (jsonObject.has("alSetId")) {
                                try {
                                    channelVO.setAlSetId(UUID.fromString(jsonObject.get("alSetId").getAsString()));
                                } catch (Exception ex) {
                                }
                            }
                            if (jsonObject.has("alSetName")) {
                                try {
                                    channelVO.setAlSetName(jsonObject.get("alSetName").getAsString());
                                } catch (Exception ex) {
                                }
                            }
                            if (jsonObject.has("pointType")) {
                                try {
                                    channelVO.setPointType(jsonObject.get("pointType").getAsString());
                                } catch (Exception ex) {
                                }
                            }
                            if (jsonObject.has("basestationPortNum")) {
                                try {
                                    channelVO.setBasestationPortNum(jsonObject.get("basestationPortNum").getAsShort());
                                } catch (Exception ex) {
                                }
                            }
                            if (jsonObject.has("sensorChannelNum")) {
                                try {
                                    channelVO.setSensorChannelNum(jsonObject.get("sensorChannelNum").getAsInt());
                                } catch (Exception ex) {
                                }
                            }
                            if (jsonObject.has("sensorOffset")) {
                                try {
                                    channelVO.setSensorOffset(jsonObject.get("sensorOffset").getAsFloat());
                                } catch (Exception ex) {
                                }
                            }
                            if (jsonObject.has("sensorSensitivity")) {
                                try {
                                    channelVO.setSensorSensitivity(jsonObject.get("sensorSensitivity").getAsFloat());
                                } catch (Exception ex) {
                                }
                            }
                            if (jsonObject.has("sensorType")) {
                                try {
                                    channelVO.setSensorType(jsonObject.get("sensorType").getAsString());
                                } catch (Exception ex) {
                                }
                            }
                            if (jsonObject.has("sensorUnits")) {
                                try {
                                    channelVO.setSensorUnits(jsonObject.get("sensorUnits").getAsString());
                                } catch (Exception ex) {
                                }
                            }
                            if (jsonObject.has("ffSetIds")) {
                                try {
                                    channelVO.setFfSetIds(jsonObject.get("ffSetIds").getAsString());
                                } catch (Exception ex) {
                                }
                            }
                            if (jsonObject.has("tachId")) {
                                try {
                                    channelVO.setTachId(UUID.fromString(jsonObject.get("tachId").getAsString()));
                                } catch (Exception ex) {
                                }
                            }
                            if (jsonObject.has("tachReferenceUnits")) {
                                try {
                                    channelVO.setTachReferenceUnits(jsonObject.get("tachReferenceUnits").getAsString());
                                } catch (Exception ex) {
                                }
                            }
                            if (jsonObject.has("rollDiameter")) {
                                try {
                                    channelVO.setRollDiameter(jsonObject.get("rollDiameter").getAsFloat());
                                } catch (Exception ex) {
                                }
                            }
                            if (jsonObject.has("rollDiameterUnits")) {
                                try {
                                    channelVO.setRollDiameterUnits(jsonObject.get("rollDiameterUnits").getAsString());
                                } catch (Exception ex) {
                                }
                            }
                            if (jsonObject.has("speedRatio")) {
                                try {
                                    channelVO.setSpeedRatio(jsonObject.get("speedRatio").getAsFloat());
                                } catch (Exception ex) {
                                }
                            }
                            if (jsonObject.has("fMax")) {
                                try {
                                    channelVO.setfMax(jsonObject.get("fMax").getAsInt());
                                } catch (Exception ex) {
                                }
                            }
                            if (jsonObject.has("resolution")) {
                                try {
                                    channelVO.setResolution(jsonObject.get("resolution").getAsInt());
                                } catch (Exception ex) {
                                }
                            }
                            if (jsonObject.has("period")) {
                                try {
                                    channelVO.setPeriod(jsonObject.get("period").getAsFloat());
                                } catch (Exception ex) {
                                }
                            }
                            if (jsonObject.has("sampleRate")) {
                                try {
                                    channelVO.setSampleRate(jsonObject.get("sampleRate").getAsFloat());
                                } catch (Exception ex) {
                                }
                            }
                            if (jsonObject.has("sampleInterval")) {
                                try {
                                    channelVO.setSampleInterval(jsonObject.get("sampleInterval").getAsShort());
                                } catch (Exception ex) {
                                }
                            }
                            if (jsonObject.has("alarmEnabled")) {
                                try {
                                    channelVO.setAlarmEnabled(jsonObject.get("alarmEnabled").getAsBoolean());
                                } catch (Exception ex) {
                                }
                            }
                            if (jsonObject.has("apAlSetVOs")) {
                                try {
                                    if ((innerJsonArray = jsonObject.getAsJsonArray("apAlSetVOs")) != null && !innerJsonArray.isEmpty()) {
                                        apAlByPointVOList = new ArrayList();
                                        for (int l = 0; l < innerJsonArray.size(); l++) {
                                            innerJsonObject = innerJsonArray.get(l).getAsJsonObject();
                                            apAlSetVO = new ApAlSetVO();
                                            apAlSetVO.setCustomerAccount(channelVO.getCustomerAccount());
                                            apAlSetVO.setSiteId(channelVO.getSiteId());
                                            apAlSetVO.setApSetId(channelVO.getApSetId());
                                            apAlSetVO.setAlSetId(channelVO.getAlSetId());
                                            apAlSetVO.setApSetName(channelVO.getApSetName());
                                            apAlSetVO.setAlSetName(channelVO.getAlSetName());
                                            apAlSetVO.setSensorType(channelVO.getSensorType());
                                            apAlSetVO.setFmax(channelVO.getfMax());
                                            apAlSetVO.setResolution(channelVO.getResolution());
                                            apAlSetVO.setPeriod(channelVO.getPeriod());
                                            apAlSetVO.setSampleRate(channelVO.getSampleRate());

                                            if (innerJsonObject.has("paramName")) {
                                                try {
                                                    apAlSetVO.setParamName(innerJsonObject.get("paramName").getAsString().trim());
                                                } catch (Exception ex) {
                                                }
                                            }

                                            if (innerJsonObject.has("dwell")) {
                                                try {
                                                    apAlSetVO.setDwell(innerJsonObject.get("dwell").getAsInt());
                                                } catch (Exception ex) {
                                                }
                                            }

                                            if (innerJsonObject.has("minFrequency")) {
                                                try {
                                                    apAlSetVO.setMinFrequency(innerJsonObject.get("minFrequency").getAsFloat());
                                                } catch (Exception ex) {
                                                }
                                            }

                                            if (innerJsonObject.has("maxFrequency")) {
                                                try {
                                                    apAlSetVO.setMaxFrequency(innerJsonObject.get("maxFrequency").getAsFloat());
                                                } catch (Exception ex) {
                                                }
                                            }

                                            if (innerJsonObject.has("lowFault")) {
                                                try {
                                                    apAlSetVO.setLowFault(innerJsonObject.get("lowFault").getAsFloat());
                                                } catch (Exception ex) {
                                                }
                                            }

                                            if (innerJsonObject.has("lowAlert")) {
                                                try {
                                                    apAlSetVO.setLowAlert(innerJsonObject.get("lowAlert").getAsFloat());
                                                } catch (Exception ex) {
                                                }
                                            }

                                            if (innerJsonObject.has("highAlert")) {
                                                try {
                                                    apAlSetVO.setHighAlert(innerJsonObject.get("highAlert").getAsFloat());
                                                } catch (Exception ex) {
                                                }
                                            }

                                            if (innerJsonObject.has("highFault")) {
                                                try {
                                                    apAlSetVO.setHighFault(innerJsonObject.get("highFault").getAsFloat());
                                                } catch (Exception ex) {
                                                }
                                            }

                                            if (innerJsonObject.has("demodHighPass")) {
                                                try {
                                                    apAlSetVO.setDemodHighPass(innerJsonObject.get("demodHighPass").getAsFloat());
                                                } catch (Exception ex) {
                                                }
                                            }

                                            if (innerJsonObject.has("demodLowPass")) {
                                                try {
                                                    apAlSetVO.setDemodLowPass(innerJsonObject.get("demodLowPass").getAsFloat());
                                                } catch (Exception ex) {
                                                }
                                            }

                                            if (innerJsonObject.has("lowFaultActive")) {
                                                try {
                                                    apAlSetVO.setLowFaultActive(innerJsonObject.get("lowFaultActive").getAsBoolean());
                                                } catch (Exception ex) {
                                                }
                                            }

                                            if (innerJsonObject.has("lowAlertActive")) {
                                                try {
                                                    apAlSetVO.setLowAlertActive(innerJsonObject.get("lowAlertActive").getAsBoolean());
                                                } catch (Exception ex) {
                                                }
                                            }

                                            if (innerJsonObject.has("highAlertActive")) {
                                                try {
                                                    apAlSetVO.setHighAlertActive(innerJsonObject.get("highAlertActive").getAsBoolean());
                                                } catch (Exception ex) {
                                                }
                                            }

                                            if (innerJsonObject.has("highFaultActive")) {
                                                try {
                                                    apAlSetVO.setHighFaultActive(innerJsonObject.get("highFaultActive").getAsBoolean());
                                                } catch (Exception ex) {
                                                }
                                            }

                                            if (innerJsonObject.has("demod")) {
                                                try {
                                                    apAlSetVO.setDemod(innerJsonObject.get("demod").getAsBoolean());
                                                } catch (Exception ex) {
                                                }
                                            }

                                            if (innerJsonObject.has("paramType")) {
                                                try {
                                                    apAlSetVO.setParamType(innerJsonObject.get("paramType").getAsString().trim());
                                                } catch (Exception ex) {
                                                }
                                            }

                                            if (innerJsonObject.has("frequencyUnits")) {
                                                try {
                                                    apAlSetVO.setFrequencyUnits(innerJsonObject.get("frequencyUnits").getAsString().trim());
                                                } catch (Exception ex) {
                                                }
                                            }

                                            if (innerJsonObject.has("paramUnits")) {
                                                try {
                                                    apAlSetVO.setParamUnits(innerJsonObject.get("paramUnits").getAsString().trim());
                                                } catch (Exception ex) {
                                                }
                                            }

                                            if (innerJsonObject.has("paramAmpFactor")) {
                                                try {
                                                    apAlSetVO.setParamAmpFactor(innerJsonObject.get("paramAmpFactor").getAsString().trim());
                                                } catch (Exception ex) {
                                                }
                                            }

                                            apAlByPointVOList.add(apAlSetVO);
                                        }
                                        channelVO.setApAlSetVOs(apAlByPointVOList);
                                    }
                                } catch (Exception e) {
                                    Logger.getLogger(ManageDeviceDomJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
                                }
                            }
                            channelVOList.add(channelVO);
                        }
                    }
                }
            }
        } catch (Exception e) {
            Logger.getLogger(ManageDeviceDomJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
            channelVOList = new ArrayList();
        }
        return channelVOList;
    }
}
