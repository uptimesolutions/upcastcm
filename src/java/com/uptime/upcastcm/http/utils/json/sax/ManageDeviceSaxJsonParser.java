/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.http.utils.json.sax;

import com.uptime.client.utils.SaxJsonParser;
import com.uptime.upcastcm.utils.vo.ChannelVO;
import com.uptime.upcastcm.utils.vo.DeviceStatusVO;
import java.io.StringReader;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;

/**
 *
 * @author twilcox
 */
public class ManageDeviceSaxJsonParser extends SaxJsonParser {

    private static ManageDeviceSaxJsonParser instance = null;

    /**
     * Private Singleton class
     */
    private ManageDeviceSaxJsonParser() {
    }

    /**
     * Returns an instance of the class
     *
     * @return ManageDeviceSaxJsonParser object
     */
    public static ManageDeviceSaxJsonParser getInstance() {
        if (instance == null) {
            instance = new ManageDeviceSaxJsonParser();
        }
        return instance;
    }

    /**
     * Converts a json to a List Object of ChannelVO Objects
     *
     * @param content, String Object, json
     * @return List Object of ChannelVO Objects
     */
    public List<ChannelVO> populateManageDeviceRowsFromJson(String content) {
        List<ChannelVO> list = new ArrayList();
        String keyName;
        ChannelVO channelVO;
        boolean inArray;
        Boolean disabled;

        try {
            parser = Json.createParser(new StringReader(content));
            keyName = null;
            channelVO = null;
            inArray = false;
            disabled = null;

            while (parser.hasNext()) {
                switch ((event = parser.next())) {
                    case START_ARRAY:
                        inArray = true;
                        break;
                    case END_ARRAY:
                        inArray = false;
                        keyName = null;
                        channelVO = null;
                        break;
                    case START_OBJECT:
                        channelVO = inArray ? new ChannelVO() : null;
                        break;
                    case END_OBJECT:
                        if (channelVO != null && inArray) {
                            list.add(channelVO);
                        }
                        channelVO = null;
                        keyName = null;
                        break;
                    case KEY_NAME:
                        switch (parser.getString()) {
                            case "deviceId":
                                keyName = "deviceId";
                                break;
                            case "customerAcct":
                                keyName = "customerAcct";
                                break;
                            case "channelType":
                                keyName = "channelType";
                                break;
                            case "channelNum":
                                keyName = "channelNum";
                                break;
                            case "siteId":
                                keyName = "siteId";
                                break;
                            case "areaId":
                                keyName = "areaId";
                                break;
                            case "assetId":
                                keyName = "assetId";
                                break;
                            case "pointLocationId":
                                keyName = "pointLocationId";
                                break;
                            case "pointId":
                                keyName = "pointId";
                                break;
                            case "apSetId":
                                keyName = "apSetId";
                                break;
                            case "areaName":
                                keyName = "areaName";
                                break;
                            case "assetName":
                                keyName = "assetName";
                                break;
                            case "pointLocationName":
                                keyName = "pointLocationName";
                                break;
                            case "pointName":
                                keyName = "pointName";
                                break;
                            case "sensorType":
                                keyName = "sensorType";
                                break;
                            case "disabled":
                                keyName = "disabled";
                                break;
                            case "alarmEnabled":
                                keyName = "alarmEnabled";
                                break;
                            case "fmax":
                                keyName = "fmax";
                                break;
                            case "resolution":
                                keyName = "resolution";
                                break;
                            case "basestationPortNum":
                                keyName = "basestationPortNum";
                                break;
                            default:
                                keyName = null;
                                break;
                        }
                        break;
                    case VALUE_STRING:
                        if (channelVO != null && inArray && keyName != null) {
                            switch (keyName) {
                                case "siteId":
                                    try {
                                        channelVO.setSiteId(UUID.fromString(parser.getString()));
                                    } catch (Exception ex) {
                                    }
                                    break;
                                case "areaId":
                                    try {
                                        channelVO.setAreaId(UUID.fromString(parser.getString()));
                                    } catch (Exception ex) {
                                    }
                                    break;
                                case "assetId":
                                    try {
                                        channelVO.setAssetId(UUID.fromString(parser.getString()));
                                    } catch (Exception ex) {
                                    }
                                    break;
                                case "pointLocationId":
                                    try {
                                        channelVO.setPointLocationId(UUID.fromString(parser.getString()));
                                    } catch (Exception ex) {
                                    }
                                    break;
                                case "pointId":
                                    try {
                                        channelVO.setPointId(UUID.fromString(parser.getString()));
                                    } catch (Exception ex) {
                                    }
                                    break;
                                case "apSetId":
                                    try {
                                        channelVO.setApSetId(UUID.fromString(parser.getString()));
                                    } catch (Exception ex) {
                                    }
                                    break;
                                case "deviceId":
                                    channelVO.setDeviceId(parser.getString());
                                    break;
                                case "customerAcct":
                                    channelVO.setCustomerAccount(parser.getString());
                                    break;
                                case "channelType":
                                    channelVO.setPointType(parser.getString());
                                    break;
                                case "areaName":
                                    channelVO.setAreaName(parser.getString());
                                    break;
                                case "assetName":
                                    channelVO.setAssetName(parser.getString());
                                    break;
                                case "pointLocationName":
                                    channelVO.setPointLocationName(parser.getString());
                                    break;
                                case "pointName":
                                    channelVO.setPointName(parser.getString());
                                    break;
                                case "sensorType":
                                    channelVO.setSensorType(parser.getString());
                                    break;
                            }
                        }
                        break;
                    case VALUE_NUMBER:
                        if (channelVO != null && inArray && keyName != null) {
                            switch (keyName) {
                                case "channelNum":
                                    channelVO.setSensorChannelNum(parser.getBigDecimal().byteValue());
                                    break;
                                case "fmax":
                                    channelVO.setfMax(parser.getInt());
                                    break;
                                case "resolution":
                                    channelVO.setResolution(parser.getInt());
                                    break;
                                case "basestationPortNum":
                                    channelVO.setBasestationPortNum((short)parser.getInt());
                                    break;
                            }
                        }
                        break;
                    case VALUE_TRUE:
                        if(channelVO != null && inArray && keyName != null && keyName.equals("alarmEnabled")) {
                            channelVO.setAlarmEnabled(true);
                        } else if (keyName != null && keyName.equals("disabled")) {
                            disabled = true;
                        }
                        break;
                    case VALUE_FALSE:
                        if(channelVO != null && inArray && keyName != null && keyName.equals("alarmEnabled")) {
                            channelVO.setAlarmEnabled(false);
                        } else if (keyName != null && keyName.equals("disabled")) {
                            disabled = false;
                        }
                        break;
                }
            }
            
            // Set Disabled Value
            for (ChannelVO vo : list) {
                vo.setHwDisabled(disabled);
            }
            
        } catch (Exception e) {
            Logger.getLogger(ManageDeviceSaxJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }

    /**
     * Converts a json to a List Object of DeviceStatusVO Objects
     *
     * @param content, String Object, json
     * @return List Object of DeviceStatusVO Objects
     */
    public List<DeviceStatusVO> populateDeviceStatusFromJson(String content) {
        List<DeviceStatusVO> list = new ArrayList<>();
        String keyName = null;
        DeviceStatusVO deviceStatusVO = null;
        boolean inArray = false;
        String customerAcct = null;
        String siteName = null;
        try {
            parser = Json.createParser(new StringReader(content));
            while (parser.hasNext()) {
                switch (parser.next()) {
                    case START_ARRAY:
                        inArray = true;
                        break;
                    case END_ARRAY:
                        inArray = false;
                        break;
                    case START_OBJECT:
                        if (inArray) {
                            deviceStatusVO = new DeviceStatusVO();
                        }
                        break;
                    case END_OBJECT:
                        if (deviceStatusVO != null && inArray) {
                            // Ensure both customer_acct and site_name are set for each object
                            deviceStatusVO.setCustomerAcct(customerAcct);
                            deviceStatusVO.setSiteName(siteName);
                            list.add(deviceStatusVO);
                        }
                        deviceStatusVO = null;
                        break;
                    case KEY_NAME:
                        keyName = parser.getString();
                        break;
                    case VALUE_STRING:
                        if (keyName != null) {
                            switch (keyName) {
                                case "customer_acct":
                                    customerAcct = parser.getString();  // Store global data
                                    break;
                                case "site_name":
                                    siteName = parser.getString();  // Store global data
                                    break;
                                case "device_id":
                                    if (deviceStatusVO != null) {
                                        deviceStatusVO.setDeviceId(parser.getString());
                                    }
                                    break;
                                case "area_name":
                                    if (deviceStatusVO != null) {
                                        deviceStatusVO.setAreaName(parser.getString());
                                    }
                                    break;
                                case "asset_name":
                                    if (deviceStatusVO != null) {
                                        deviceStatusVO.setAssetName(parser.getString());
                                    }
                                    break;
                                case "point_location_name":
                                    if (deviceStatusVO != null) {
                                        deviceStatusVO.setPointLocationName(parser.getString());
                                    }
                                    break;
                                case "last_sample":
                                    if (deviceStatusVO != null) {
                                        String lastSample = parser.getString();
                                        if (lastSample != null && !lastSample.isEmpty()) {
                                            deviceStatusVO.setLastSample(LocalDateTime.parse(lastSample, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S")).atZone(ZoneOffset.UTC).toInstant());
                                        }
                                    }
                                    break;
                            }
                        }
                        break;
                    case VALUE_NUMBER:
                        if (deviceStatusVO != null && keyName != null) {
                            switch (keyName) {
                                case "battery_voltage":
                                    deviceStatusVO.setBatteryVoltage(parser.getBigDecimal().floatValue());
                                    break;
                            }
                        }
                        break;
                    case VALUE_TRUE:
                        if (deviceStatusVO != null && keyName != null) {
                            if ("is_disabled".equals(keyName)) {
                                deviceStatusVO.setDisabled(true);
                            }
                        }
                        break;
                    case VALUE_FALSE:
                        if (deviceStatusVO != null && keyName != null) {
                            if ("is_disabled".equals(keyName)) {
                                deviceStatusVO.setDisabled(false);
                            }
                        }
                        break;
                }
            }
        } catch (Exception e) {
            Logger.getLogger(ManageDeviceSaxJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
            list = new ArrayList();
        }

        return list;
    }
}
