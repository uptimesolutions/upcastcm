/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.http.utils.json.dom;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.uptime.client.utils.DomJsonParser;
import com.uptime.client.utils.JsonConverterUtil;
import com.uptime.upcastcm.utils.vo.BearingCatalogVO;
import com.uptime.upcastcm.utils.vo.FaultFrequenciesVO;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Joseph
 */
public class FaultFrequenciesDomJsonParser extends DomJsonParser {
    private static FaultFrequenciesDomJsonParser instance = null;

    /**
     * Private Singleton class
     */
    private FaultFrequenciesDomJsonParser() {
    }

    /**
     * Returns an instance of the class
     *
     * @return FaultFrequenciesDomJsonParser object
     */
    public static FaultFrequenciesDomJsonParser getInstance() {
        if (instance == null) {
            instance = new FaultFrequenciesDomJsonParser();
        }
        return instance;
    }

    /**
     * Convert a FaultFrequenciesVO Object to a String Object json
     *
     * @param siteFaultFrequenciesVO, FaultFrequenciesVO Object
     * @return String Object
     */
    public String getJsonFromFaultFrequencies(FaultFrequenciesVO siteFaultFrequenciesVO) {
        try {
            if (siteFaultFrequenciesVO != null) {
                return JsonConverterUtil.toJSON(siteFaultFrequenciesVO);
            }
        } catch (Exception e) {
            Logger.getLogger(FaultFrequenciesDomJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
        }
        return null;
    }

    /**
     * Convert a lists of FaultFrequenciesVO Objects to a String Object json
     *
     * @param createSiteFFSetFavoritesVOList, List of FaultFrequenciesVO Objects
     * @param deleteSiteFFSetFavoritesVOList, List of FaultFrequenciesVO Objects
     * @return String Object
     */
    public String getUpdateJsonFromFaultFrequenciesVO(List<FaultFrequenciesVO> createSiteFFSetFavoritesVOList, List<FaultFrequenciesVO> deleteSiteFFSetFavoritesVOList) {
        StringBuilder jsonBuilder = new StringBuilder();

        try {
            jsonBuilder.append("{");
            if (createSiteFFSetFavoritesVOList != null && !createSiteFFSetFavoritesVOList.isEmpty()) {
                jsonBuilder.append("\"createSiteFFSetFavoritesVOList\": ");
                jsonBuilder.append(JsonConverterUtil.toJSON(createSiteFFSetFavoritesVOList));
            }
            if (deleteSiteFFSetFavoritesVOList != null && !deleteSiteFFSetFavoritesVOList.isEmpty()) {
                if (createSiteFFSetFavoritesVOList != null && !createSiteFFSetFavoritesVOList.isEmpty()) {
                    jsonBuilder.append(",");
                }
                jsonBuilder.append("\"deleteSiteFFSetFavoritesVOList\": ");
                jsonBuilder.append(JsonConverterUtil.toJSON(deleteSiteFFSetFavoritesVOList));
            }
            jsonBuilder.append("}");
            return jsonBuilder.toString();
        } catch (Exception e) {
            Logger.getLogger(FaultFrequenciesDomJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
        }
        return null;
    }

    /**
     * Convert a list of FaultFrequenciesVO Objects to a String Object json
     *
     * @param siteFaultFrequenciesVOList, List of FaultFrequenciesVO Objects
     * @return String Object
     */
    public String getJsonFromFaultFrequencies(List<FaultFrequenciesVO> siteFaultFrequenciesVOList) {
        try {
            if (siteFaultFrequenciesVOList != null) {
                return JsonConverterUtil.toJSON(siteFaultFrequenciesVOList);
            }
        } catch (Exception e) {
            Logger.getLogger(FaultFrequenciesDomJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
        }
        return null;
    }

    /**
     * Convert a list of BearingCatalogVO Objects to a String Object json
     *
     * @param bearingCatalogVOList, List of BearingCatalogVO Objects
     * @return String Object
     */
    public String getJsonFromBearingCatalogVO(List<BearingCatalogVO> bearingCatalogVOList) {
        try {
            if (bearingCatalogVOList != null) {
                return JsonConverterUtil.toJSON(bearingCatalogVOList);
            }
        } catch (Exception e) {
            Logger.getLogger(FaultFrequenciesDomJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
        }
        return null;
    }

    /**
     * Convert a list of BearingCatalogVO Object to a String Object json
     *
     * @param bearingCatalogVO, List of BearingCatalogVO Object
     * @return String Object
     */
    public String getJsonFromBearingCatalogVO(BearingCatalogVO bearingCatalogVO) {
        try {
            if (bearingCatalogVO != null) {
                return JsonConverterUtil.toJSON(bearingCatalogVO);
            }
        } catch (Exception e) {
            Logger.getLogger(FaultFrequenciesDomJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
        }
        return null;
    }

    /**
     * Convert a lists of BearingCatalogVO Objects to a String Object json
     *
     * @param createBearingCatalogVOList, List of BearingCatalogVO Objects
     * @param deleteBearingCatalogVOList, List of BearingCatalogVO Objects
     * @return String Object
     */
    public String getUpdateJsonFromBearingCatalogVO(List<BearingCatalogVO> createBearingCatalogVOList, List<BearingCatalogVO> deleteBearingCatalogVOList) {
        StringBuilder jsonBuilder = new StringBuilder();

        try {
            jsonBuilder.append("{");
            if (createBearingCatalogVOList != null && !createBearingCatalogVOList.isEmpty()) {
                jsonBuilder.append("\"createSiteBearingFavoritesVOList\": ");
                jsonBuilder.append(JsonConverterUtil.toJSON(createBearingCatalogVOList));
            }
            if (deleteBearingCatalogVOList != null && !deleteBearingCatalogVOList.isEmpty()) {
                if (createBearingCatalogVOList != null && !createBearingCatalogVOList.isEmpty()) {
                    jsonBuilder.append(",");
                }
                jsonBuilder.append("\"deleteSiteBearingFavoritesVOList\": ");
                jsonBuilder.append(JsonConverterUtil.toJSON(deleteBearingCatalogVOList));
            }
            jsonBuilder.append("}");
            return jsonBuilder.toString();
        } catch (Exception e) {
            Logger.getLogger(FaultFrequenciesDomJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
        }
        return null;
    }
    
    /**
     * Convert a String Object to a List Object of BearingCatalogVO Object
     *
     * @param ffSetIdsJson, String Object
     * @return List Object of BearingCatalogVO Objects
     */
    public List<FaultFrequenciesVO> parseFfSetIdsJson(String ffSetIdsJson) {
        List<FaultFrequenciesVO> list;
        FaultFrequenciesVO faultFrequenciesVO;
        JsonObject freqObj;
        JsonArray freqArr;
        UUID ffSetId;
        
        
        try {
            list = new ArrayList();
            if (ffSetIdsJson != null) {
                if ((element = JsonParser.parseString(ffSetIdsJson)) != null && (jsonObject = element.getAsJsonObject()) != null && 
                        jsonObject.has("ff_sets") && (array = jsonObject.get("ff_sets").getAsJsonArray()) != null) {
                    for (int i = 0; i < array.size(); i++) {
                        if ((jsonObject = array.get(i).getAsJsonObject()) != null && jsonObject.has("frequencies") && jsonObject.has("ff_set_name") && jsonObject.has("units")) {
                            ffSetId = UUID.randomUUID();
                            freqArr = jsonObject.get("frequencies").getAsJsonArray();
                            
                            for (int j = 0; j < freqArr.size(); j++) {
                                freqObj = freqArr.get(j).getAsJsonObject();

                                for (Map.Entry tmp : freqObj.entrySet()) {
                                    faultFrequenciesVO = new FaultFrequenciesVO();
                                    faultFrequenciesVO.setFfSetId(ffSetId);
                                    faultFrequenciesVO.setFfId(UUID.randomUUID());
                                    faultFrequenciesVO.setFfSetName(jsonObject.get("ff_set_name").getAsString());
                                    faultFrequenciesVO.setFfName(tmp.getKey().toString());
                                    faultFrequenciesVO.setFfValue(Float.valueOf(tmp.getValue().toString()));
                                    faultFrequenciesVO.setFfUnit(jsonObject.get("units").getAsString());
                                    list.add(faultFrequenciesVO);
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            Logger.getLogger(FaultFrequenciesDomJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
            list = new ArrayList();
        }
        
        return list;
    }
    
    public static void main(String[] args) {
        FaultFrequenciesDomJsonParser pp = new FaultFrequenciesDomJsonParser();
        List<BearingCatalogVO> creatingbearingCatalogVOList = new ArrayList<>();
        List<BearingCatalogVO> deletingbearingCatalogVOList = new ArrayList<>();
        BearingCatalogVO bvo = new BearingCatalogVO();
        bvo.setCustomerAcct("FEDEX EXPRESS");
        bvo.setSiteId(UUID.fromString("8b086e15-371f-4a8d-bedb-c1b67b49be3e"));
        bvo.setVendorId("MRC");
        bvo.setBearingId("BEARING 1");
        bvo.setFtf(2);
        bvo.setBsf(20);
        bvo.setBpfo(200);
        bvo.setBpfi(201);
        creatingbearingCatalogVOList.add(bvo);

        bvo = new BearingCatalogVO();
        bvo.setCustomerAcct("FEDEX EXPRESS");
        bvo.setSiteId(UUID.fromString("8b086e15-371f-4a8d-bedb-c1b67b49be3e"));
        bvo.setVendorId("MRC");
        bvo.setBearingId("BEARING 2");
        bvo.setFtf(3);
        bvo.setBsf(30);
        bvo.setBpfo(300);
        bvo.setBpfi(301);
        deletingbearingCatalogVOList.add(bvo);
        String json = pp.getUpdateJsonFromBearingCatalogVO(creatingbearingCatalogVOList, deletingbearingCatalogVOList);
        System.out.println("json - " + json);
    }

}
