/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.http.utils.json.sax;

import com.uptime.client.utils.SaxJsonParser;
import com.uptime.upcastcm.utils.vo.TrendValuesVO;
import com.uptime.upcastcm.utils.vo.WaveDataVO;
import com.uptime.upcastcm.utils.vo.WaveformVO;
import java.io.StringReader;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;

/**
 *
 * @author gsingh
 */
public class TrendDataSaxJsonParser extends SaxJsonParser {

    private static TrendDataSaxJsonParser instance = null;

    /**
     * Private Singleton class
     */
    private TrendDataSaxJsonParser() {
    }

    /**
     * Returns an instance of the class
     *
     * @return TrendDataSaxJsonParser object
     */
    public static TrendDataSaxJsonParser getInstance() {
        if (instance == null) {
            instance = new TrendDataSaxJsonParser();
        }
        return instance;
    }

    /**
     * Converts a json to a List Object of TrendValuesVO Objects
     *
     * @param content, String Object, json
     * @return List Object of TrendValuesVO Objects
     */
    public List<TrendValuesVO> populateTrendDataFromJson(String content) {
        List<TrendValuesVO> list = new ArrayList();
        String keyName;
        TrendValuesVO trendValuesVO;
        boolean inArray;

        try {
            parser = Json.createParser(new StringReader(content));
            keyName = null;
            trendValuesVO = null;
            inArray = false;

            while (parser.hasNext()) {
                switch ((event = parser.next())) {
                    case START_ARRAY:
                        inArray = true;
                        break;
                    case END_ARRAY:
                        inArray = false;
                        keyName = null;
                        trendValuesVO = null;
                        break;
                    case START_OBJECT:
                        trendValuesVO = inArray ? new TrendValuesVO() : null;
                        break;
                    case END_OBJECT:
                        if (trendValuesVO != null && inArray) {
                            list.add(trendValuesVO);
                        }
                        trendValuesVO = null;
                        keyName = null;
                        break;
                    case KEY_NAME:
                        switch (parser.getString()) {
                            case "customerAccount":
                                keyName = "customerAccount";
                                break;
                            case "siteId":
                                keyName = "siteId";
                                break;
                            case "areaId":
                                keyName = "areaId";
                                break;
                            case "assetId":
                                keyName = "assetId";
                                break;
                            case "pointLocationId":
                                keyName = "pointLocationId";
                                break;
                            case "pointId":
                                keyName = "pointId";
                                break;
                            case "apSetId":
                                keyName = "apSetId";
                                break;
                            case "alSetId":
                                keyName = "alSetId";
                                break;
                            case "paramName":
                                keyName = "paramName";
                                break;
                            case "sampleTime":
                                keyName = "sampleTime";
                                break;
                            case "label":
                                keyName = "label";
                                break;
                            case "notes":
                                keyName = "notes";
                                break;
                            case "lowAlert":
                                keyName = "lowAlert";
                                break;
                            case "lowFault":
                                keyName = "lowFault";
                                break;
                            case "highAlert":
                                keyName = "highAlert";
                                break;
                            case "highFault":
                                keyName = "highFault";
                                break;
                            case "sensorType":
                                keyName = "sensorType";
                                break;
                            case "units":
                                keyName = "units";
                                break;
                            case "value":
                                keyName = "value";
                                break;
                            default:
                                keyName = null;
                                break;
                        }
                        break;
                    case VALUE_STRING:
                        if (trendValuesVO != null && inArray && keyName != null) {
                            switch (keyName) {
                                case "customerAccount":
                                    trendValuesVO.setCustomerAccount(parser.getString());
                                    break;
                                case "siteId":
                                    try {
                                        trendValuesVO.setSiteId(UUID.fromString(parser.getString()));
                                    } catch (Exception ex) {
                                    }
                                    break;
                                case "areaId":
                                    try {
                                        trendValuesVO.setAreaId(UUID.fromString(parser.getString()));
                                    } catch (Exception ex) {
                                    }
                                    break;
                                case "assetId":
                                    try {
                                        trendValuesVO.setAssetId(UUID.fromString(parser.getString()));
                                    } catch (Exception ex) {
                                    }
                                    break;
                                case "pointLocationId":
                                    try {
                                        trendValuesVO.setPointLocationId(UUID.fromString(parser.getString()));
                                    } catch (Exception ex) {
                                    }
                                    break;
                                case "pointId":
                                    try {
                                        trendValuesVO.setPointId(UUID.fromString(parser.getString()));
                                    } catch (Exception ex) {
                                    }
                                    break;
                                case "apSetId":
                                    try {
                                    trendValuesVO.setApSetId(UUID.fromString(parser.getString()));
                                    } catch (Exception ex) {
                                    }
                                    break;
                                case "alSetId":
                                    try {
                                        trendValuesVO.setAlSetId(UUID.fromString(parser.getString()));
                                    } catch (Exception ex) {
                                    }
                                    break;
                                case "paramName":
                                    trendValuesVO.setParamName(parser.getString());
                                    break;
                                case "sampleTime":
                                    trendValuesVO.setSampleTime(LocalDateTime.parse(parser.getString(), DateTimeFormatter.ofPattern(DATE_TIME_OFFSET_FORMAT)).atZone(ZoneOffset.UTC).toInstant());
                                    break;
                                case "label":
                                    trendValuesVO.setLabel(parser.getString());
                                    break;
                                case "notes":
                                    trendValuesVO.setNotes(parser.getString());
                                    break;
                                case "sensorType":
                                    trendValuesVO.setSensorType(parser.getString());
                                    break;
                                case "units":
                                    trendValuesVO.setUnits(parser.getString());
                                    break;
                            }
                        }
                        break;
                    case VALUE_NUMBER:
                        if (trendValuesVO != null && inArray && keyName != null) {
                            switch (keyName) {
                                case "highAlert":
                                    trendValuesVO.setHighAlert(parser.getBigDecimal().floatValue());
                                    break;
                                case "highFault":
                                    trendValuesVO.setHighFault(parser.getBigDecimal().floatValue());
                                    break;
                                case "lowAlert":
                                    trendValuesVO.setLowAlert(parser.getBigDecimal().floatValue());
                                    break;
                                case "lowFault":
                                    trendValuesVO.setLowFault(parser.getBigDecimal().floatValue());
                                    break;
                                case "value":
                                    trendValuesVO.setValue(parser.getBigDecimal().floatValue());
                                    break;
                            }
                        }
                        break;

                }
            }
        } catch (Exception e) {
            Logger.getLogger(TrendDataSaxJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }
    
    /**
     * Converts a json to a List Object of WaveDataVO Objects
     *
     * @param content, String Object, json
     * @return List Object of WaveDataVO Objects
     */
    public List<WaveDataVO> populateWaveDataListFromJson(String content) {
        List<WaveDataVO> list = new ArrayList();
        String keyName;
        WaveDataVO waveDataVO;
        WaveformVO waveformVO;
        boolean inArray, inInnerArray;

        try {
            parser = Json.createParser(new StringReader(content));
            keyName = null;
            waveDataVO = null;
            waveformVO = null;
            inArray = false;
            inInnerArray = false;

            while (parser.hasNext()) {
                switch ((event = parser.next())) {
                    case START_ARRAY:
                        inArray = true;
                        break;
                    case END_ARRAY:
                        inArray = false;
                        keyName = null;
                        waveDataVO = null;
                        waveformVO = null;
                        break;
                    case START_OBJECT:
                        if (keyName != null && keyName.equals("wave") && waveformVO == null && waveDataVO != null && inArray) {
                            waveformVO = new WaveformVO();
                        } else if (waveDataVO == null && inArray) {
                            waveDataVO = new WaveDataVO();
                        }
                        break;
                    case END_OBJECT:
                        if (waveformVO != null && waveDataVO != null && inArray) {
                            waveDataVO.setWave(waveformVO);
                            waveformVO = null;
                        } else if (waveDataVO != null && inArray) {
                            list.add(waveDataVO);
                            waveDataVO = null;
                        }
                        keyName = null;
                        break;
                    case KEY_NAME:
                        switch (parser.getString()) {
                            case "customerAccount":
                                keyName = "customerAccount";
                                break;
                            case "siteId":
                                keyName = "siteId";
                                break;
                            case "areaId":
                                keyName = "areaId";
                                break;
                            case "assetId":
                                keyName = "assetId";
                                break;
                            case "pointLocationId":
                                keyName = "pointLocationId";
                                break;
                            case "pointId":
                                keyName = "pointId";
                                break;
                            case "sampleYear":
                                keyName = "sampleYear";
                                break;
                            case "sampleMonth":
                                keyName = "sampleMonth";
                                break;
                            case "sampleTime":
                                keyName = "sampleTime";
                                break;
                            case "sensitivityValue":
                                keyName = "sensitivityValue";
                                break;
                            case "wave":
                                keyName = "wave";
                                break;
                            case "deviceId":
                                keyName = "deviceId";
                                break;
                            case "channelNum":
                                keyName = "channelNum";
                                break;
                            case "sampleRateHz":
                                keyName = "sampleRateHz";
                                break;
                            case "sensitivityUnits":
                                keyName = "sensitivityUnits";
                                break;
                            case "tachSpeed":
                                keyName = "tachSpeed";
                                break;
                            case "samples":
                                if (waveformVO != null) {
                                    waveformVO.setSamples(new ArrayList());
                                    
                                    innerArray:
                                    while (parser.hasNext()) {
                                        switch ((event = parser.next())) {
                                            case START_ARRAY:
                                                inInnerArray = true;
                                                break;
                                            case END_ARRAY:
                                                break innerArray;
                                            case VALUE_NUMBER:
                                                if (inInnerArray) {
                                                    // This value from the json is a double, but right now it is getting casted as a float
                                                    // waveformVO.getSamples().add(parser.getBigDecimal().doubleValue());
                                                    waveformVO.getSamples().add(parser.getBigDecimal().floatValue());
                                                }
                                            break;
                                        }

                                    }
                                }
                                inInnerArray = false;
                                keyName = null;
                                break;
                            default:
                                keyName = null;
                                break;
                        }
                        break;
                    case VALUE_STRING:
                        if (inArray && keyName != null) {
                            switch (keyName) {
                                case "customerAccount":
                                    if (waveDataVO != null) {
                                        waveDataVO.setCustomerAccount(parser.getString());
                                    }
                                    break;
                                case "siteId":
                                    if (waveDataVO != null) {
                                        try {
                                            waveDataVO.setSiteId(UUID.fromString(parser.getString()));
                                        } catch (Exception ex) {}
                                    }
                                    break;
                                case "areaId":
                                    if (waveDataVO != null) {
                                        try {
                                            waveDataVO.setAreaId(UUID.fromString(parser.getString()));
                                        } catch (Exception ex) {}
                                    }
                                    break;
                                case "assetId":
                                    if (waveDataVO != null) {
                                        try {
                                            waveDataVO.setAssetId(UUID.fromString(parser.getString()));
                                        } catch (Exception ex) {}
                                    }
                                    break;
                                case "pointLocationId":
                                    if (waveDataVO != null) {
                                        try {
                                            waveDataVO.setPointLocationId(UUID.fromString(parser.getString()));
                                        } catch (Exception ex) {}
                                    }
                                    break;
                                case "pointId":
                                    if (waveDataVO != null) {
                                        try {
                                            waveDataVO.setPointId(UUID.fromString(parser.getString()));
                                        } catch (Exception ex) {}
                                    }
                                    break;
                                case "sampleTime":
                                    if (waveformVO != null) {
                                        try {
                                            waveformVO.setSampleTime(LocalDateTime.parse(parser.getString(), DateTimeFormatter.ofPattern(DATE_TIME_OFFSET_FORMAT)).atZone(ZoneOffset.UTC).toInstant());
                                        } catch (Exception ex) {}
                                    } else if (waveDataVO != null) {
                                        try {
                                            waveDataVO.setSampleTime(LocalDateTime.parse(parser.getString(), DateTimeFormatter.ofPattern(DATE_TIME_OFFSET_FORMAT)).atZone(ZoneOffset.UTC).toInstant());
                                        } catch (Exception ex) {}
                                    }
                                    break;
                                case "deviceId":
                                    if (waveformVO != null) {
                                        waveformVO.setDeviceId(parser.getString());
                                    }
                                    break;
                                case "sensitivityUnits":
                                    if (waveformVO != null) {
                                        waveformVO.setSensitivityUnits(parser.getString());
                                    }
                                    break;
                            }
                        }
                        break;
                    case VALUE_NUMBER:
                        if (inArray && keyName != null) {
                            switch (keyName) {
                                case "sampleYear":
                                    if (waveDataVO != null) {
                                        waveDataVO.setSampleYear((short)parser.getInt());
                                    }
                                    break;
                                case "sampleMonth":
                                    if (waveDataVO != null) {
                                        waveDataVO.setSampleMonth((byte)parser.getInt());
                                    }
                                    break;
                                case "sensitivityValue":
                                    if (waveDataVO != null) {
                                        waveDataVO.setSensitivityValue(parser.getBigDecimal().floatValue());
                                    }
                                    break;
                                case "channelNum":
                                    if (waveformVO != null) {
                                        waveformVO.setChannelNum((byte)parser.getInt());
                                    }
                                    break;
                                case "sampleRateHz":
                                    if (waveformVO != null) {
                                        waveformVO.setSampleRateHz(parser.getBigDecimal().floatValue());
                                    }
                                    break;
                                case "tachSpeed":
                                    if (waveformVO != null) {
                                        waveformVO.setTachSpeed(parser.getBigDecimal().floatValue());
                                    }
                                    break;
                            }
                        }
                        break;

                }
            }
        } catch (Exception e) {
            Logger.getLogger(TrendDataSaxJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
            list = new ArrayList();
        }
    
        return list;
    }
    
    /**
     * Converts a json to a WaveDataVO Object
     *
     * @param content, String Object, json
     * @return WaveDataVO Object
     */
    public WaveDataVO populateWaveDataFromJson(String content) {
        String keyName;
        WaveDataVO waveDataVO;
        WaveformVO waveformVO;
        boolean inInnerArray;

        try {
            parser = Json.createParser(new StringReader(content));
            keyName = null;
            waveDataVO = null;
            waveformVO = null;
            inInnerArray = false;

            while (parser.hasNext()) {
                switch ((event = parser.next())) {
                    case START_OBJECT:
                        if (keyName != null && keyName.equals("wave") && waveformVO == null && waveDataVO != null) {
                            waveformVO = new WaveformVO();
                        } else if (waveDataVO == null) {
                            waveDataVO = new WaveDataVO();
                        }
                        break;
                    case END_OBJECT:
                        if (waveformVO != null && waveDataVO != null) {
                            waveDataVO.setWave(waveformVO);
                        }
                        keyName = null;
                        break;
                    case KEY_NAME:
                        switch (parser.getString()) {
                            case "customerAccount":
                                keyName = "customerAccount";
                                break;
                            case "siteId":
                                keyName = "siteId";
                                break;
                            case "areaId":
                                keyName = "areaId";
                                break;
                            case "assetId":
                                keyName = "assetId";
                                break;
                            case "pointLocationId":
                                keyName = "pointLocationId";
                                break;
                            case "pointId":
                                keyName = "pointId";
                                break;
                            case "sampleYear":
                                keyName = "sampleYear";
                                break;
                            case "sampleMonth":
                                keyName = "sampleMonth";
                                break;
                            case "sampleTime":
                                keyName = "sampleTime";
                                break;
                            case "sensitivityValue":
                                keyName = "sensitivityValue";
                                break;
                            case "wave":
                                keyName = "wave";
                                break;
                            case "deviceId":
                                keyName = "deviceId";
                                break;
                            case "channelNum":
                                keyName = "channelNum";
                                break;
                            case "sampleRateHz":
                                keyName = "sampleRateHz";
                                break;
                            case "sensitivityUnits":
                                keyName = "sensitivityUnits";
                                break;
                            case "tachSpeed":
                                keyName = "tachSpeed";
                                break;
                            case "samples":
                                if (waveformVO != null) {
                                    waveformVO.setSamples(new ArrayList());
                                    
                                    innerArray:
                                    while (parser.hasNext()) {
                                        switch ((event = parser.next())) {
                                            case START_ARRAY:
                                                inInnerArray = true;
                                                break;
                                            case END_ARRAY:
                                                break innerArray;
                                            case VALUE_NUMBER:
                                                if (inInnerArray) {
                                                    // This value from the json is a double, but right now it is getting casted as a float
                                                    // waveformVO.getSamples().add(parser.getBigDecimal().doubleValue());
                                                    waveformVO.getSamples().add(parser.getBigDecimal().floatValue());
                                                }
                                            break;
                                        }

                                    }
                                }
                                inInnerArray = false;
                                keyName = null;
                                break;
                            default:
                                keyName = null;
                                break;
                        }
                        break;
                    case VALUE_STRING:
                        if (keyName != null) {
                            switch (keyName) {
                                case "customerAccount":
                                    if (waveDataVO != null) {
                                        waveDataVO.setCustomerAccount(parser.getString());
                                    }
                                    break;
                                case "siteId":
                                    if (waveDataVO != null) {
                                        try {
                                            waveDataVO.setSiteId(UUID.fromString(parser.getString()));
                                        } catch (Exception ex) {}
                                    }
                                    break;
                                case "areaId":
                                    if (waveDataVO != null) {
                                        try {
                                            waveDataVO.setAreaId(UUID.fromString(parser.getString()));
                                        } catch (Exception ex) {}
                                    }
                                    break;
                                case "assetId":
                                    if (waveDataVO != null) {
                                        try {
                                            waveDataVO.setAssetId(UUID.fromString(parser.getString()));
                                        } catch (Exception ex) {}
                                    }
                                    break;
                                case "pointLocationId":
                                    if (waveDataVO != null) {
                                        try {
                                            waveDataVO.setPointLocationId(UUID.fromString(parser.getString()));
                                        } catch (Exception ex) {}
                                    }
                                    break;
                                case "pointId":
                                    if (waveDataVO != null) {
                                        try {
                                            waveDataVO.setPointId(UUID.fromString(parser.getString()));
                                        } catch (Exception ex) {}
                                    }
                                    break;
                                case "sampleTime":
                                    if (waveformVO != null) {
                                        try {
                                            waveformVO.setSampleTime(LocalDateTime.parse(parser.getString(), DateTimeFormatter.ofPattern(DATE_TIME_OFFSET_FORMAT)).atZone(ZoneOffset.UTC).toInstant());
                                        } catch (Exception ex) {}
                                    } else if (waveDataVO != null) {
                                        try {
                                            waveDataVO.setSampleTime(LocalDateTime.parse(parser.getString(), DateTimeFormatter.ofPattern(DATE_TIME_OFFSET_FORMAT)).atZone(ZoneOffset.UTC).toInstant());
                                        } catch (Exception ex) {}
                                    }
                                    break;
                                case "deviceId":
                                    if (waveformVO != null) {
                                        waveformVO.setDeviceId(parser.getString());
                                    }
                                    break;
                                case "sensitivityUnits":
                                    if (waveformVO != null) {
                                        waveformVO.setSensitivityUnits(parser.getString());
                                    }
                                    break;
                            }
                        }
                        break;
                    case VALUE_NUMBER:
                        if (keyName != null) {
                            switch (keyName) {
                                case "sampleYear":
                                    if (waveDataVO != null) {
                                        waveDataVO.setSampleYear((short)parser.getInt());
                                    }
                                    break;
                                case "sampleMonth":
                                    if (waveDataVO != null) {
                                        waveDataVO.setSampleMonth((byte)parser.getInt());
                                    }
                                    break;
                                case "sensitivityValue":
                                    if (waveDataVO != null) {
                                        waveDataVO.setSensitivityValue(parser.getBigDecimal().floatValue());
                                    }
                                    break;
                                case "channelNum":
                                    if (waveformVO != null) {
                                        waveformVO.setChannelNum((byte)parser.getInt());
                                    }
                                    break;
                                case "sampleRateHz":
                                    if (waveformVO != null) {
                                        waveformVO.setSampleRateHz(parser.getBigDecimal().floatValue());
                                    }
                                    break;
                                case "tachSpeed":
                                    if (waveformVO != null) {
                                        waveformVO.setTachSpeed(parser.getBigDecimal().floatValue());
                                    }
                                    break;
                            }
                        }
                        break;

                }
            }
        } catch (Exception e) {
            Logger.getLogger(TrendDataSaxJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
            waveDataVO = null;
        }
    
        return waveDataVO;
    }
}
