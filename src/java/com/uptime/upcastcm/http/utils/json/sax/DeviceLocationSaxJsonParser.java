
package com.uptime.upcastcm.http.utils.json.sax;

import com.uptime.client.utils.SaxJsonParser;
import com.uptime.upcastcm.utils.vo.DeviceVO;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;



/**
 *
 * @author venka
 */
public class DeviceLocationSaxJsonParser extends SaxJsonParser{
    

    private static DeviceLocationSaxJsonParser instance = null;
  

    /**
     * Private Singleton class
     */
    private DeviceLocationSaxJsonParser() {
    }

    /**
     * Returns an instance of the class
     *
     * @return DeviceLocationSaxJsonParser object
     */
    public static DeviceLocationSaxJsonParser getInstance() {
        if (instance == null) {
            instance = new DeviceLocationSaxJsonParser();
        }
        return instance;
    }

    /**
     * Converts a json to a List Object of DeviceVO Objects
     *
     * @param content, String Object, json
     * @return List Object of DeviceVO Objects
     */
    public List<DeviceVO> populateDeviceFromJson(String content) {
        List<DeviceVO> list = new ArrayList();
        String keyName;
        DeviceVO deviceVO;
        boolean inArray;
   

        try {
            parser = Json.createParser(new StringReader(content));
            keyName = null;
            deviceVO = null;
            inArray = false;

            while (parser.hasNext()) {
                switch ((event = parser.next())) {
                    case START_ARRAY:
                        inArray = true;
                        break;
                    case END_ARRAY:
                        inArray = false;
                        keyName = null;
                        deviceVO = null;
                        break;
                    case START_OBJECT:
                        deviceVO = inArray ? new DeviceVO() : null;
                        break;
                    case END_OBJECT:
                        if (deviceVO != null && inArray) {
                            list.add(deviceVO);
                        }
                        deviceVO = null;
                        keyName = null;
                        break;
                    case KEY_NAME:
                        switch (parser.getString()) {
                            case "deviceId":
                                keyName = "deviceId";
                                break;
                            case "customerAcct":
                                keyName = "customerAcct";
                                break;
                            
                            case "areaName":
                                keyName = "areaName";
                                break;
                            case "assetName":
                                keyName = "assetName";
                                break;
                            case "pointLocationName":
                                keyName = "pointLocationName";
                                break;
                            default:
                                keyName = null;
                                break;
                        }
                        break;
                    case VALUE_STRING:
                        if (deviceVO != null && inArray && keyName != null) {
                            switch (keyName) {
                                case "customerAcct":
                                    deviceVO.setCustomerAccount(parser.getString());
                                    break;
                                case "areaName":
                                    deviceVO.setAreaName(parser.getString());
                                    break;
                                case "assetName":
                                    deviceVO.setAssetName(parser.getString());
                                    break;
                                case "pointLocationName":
                                    deviceVO.setPointLocationName(parser.getString());
                                    break;
                                case "deviceId":
                                    deviceVO.setNewDeviceId(parser.getString());
                                    break;  
                                  
                                
                            }
                        }
                        break;
                }
            }

        } catch (Exception e) {
            Logger.getLogger(ManageDeviceSaxJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }
    
}
