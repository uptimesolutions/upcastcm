/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.http.utils.json.sax;

import com.uptime.client.utils.SaxJsonParser;
import com.uptime.upcastcm.utils.vo.AreaConfigScheduleVO;
import com.uptime.upcastcm.utils.vo.AreaVO;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;

/**
 *
 * @author twilcox
 */
public class AreaSaxJsonParser extends SaxJsonParser {
    private static AreaSaxJsonParser instance = null;
    
    /**
     * Private Singleton class
     */
    private AreaSaxJsonParser() {
    }

    /**
     * Returns an instance of the class
     * @return AreaSaxJsonParser object
     */
    public static AreaSaxJsonParser getInstance() {
        if (instance == null) {
            instance = new AreaSaxJsonParser();
        }
        return instance; 
    }

    /**
     * Converts a json to a List Object of AreaVO Objects
     * @param content, String Object, json 
     * @return List Object of AreaVO Objects
     */
    public List<AreaVO> populateAreaSiteFromJson(String content) {
        List<AreaVO> list = new ArrayList();
        String keyName;
        AreaVO areaVO;
        boolean inArray;
         
        try {
            parser = Json.createParser(new StringReader(content));
            keyName = null;
            areaVO = null;
            inArray = false;
            
            while (parser.hasNext()) {
                switch((event = parser.next())) {
                    case START_ARRAY:
                        inArray = true;
                        break;
                    case END_ARRAY:
                        inArray = false;
                        keyName = null;
                        areaVO = null;
                        break;
                    case START_OBJECT:
                        areaVO = inArray ? new AreaVO() : null;
                        break;
                    case END_OBJECT:
                        if(areaVO != null && inArray)
                            list.add(areaVO);
                        areaVO = null;
                        keyName = null;
                        break;
                    case KEY_NAME:
                            switch(parser.getString()) {
                                case "customerAccount":
                                    keyName = "customerAccount";
                                    break;
                                case "siteId":
                                    keyName = "siteId";
                                    break;
                                case "areaId":
                                    keyName = "areaId";
                                    break;
                                case "siteName":
                                    keyName = "siteName";
                                    break;
                                case "areaName":
                                    keyName = "areaName";
                                    break;
                                case "description":
                                    keyName = "description";
                                    break;
                                case "environment":
                                    keyName = "environment";
                                    break;
                                case "climateControlled":
                                    keyName = "climateControlled";
                                    break;
                                default:
                                    keyName = null;
                                    break;      
                            }
                        break;
                    case VALUE_STRING:
                        if(areaVO != null && inArray && keyName != null) {
                            switch(keyName) {
                                case "customerAccount":
                                    areaVO.setCustomerAccount(parser.getString());
                                    break;
                                case "siteId":
                                    areaVO.setSiteId(UUID.fromString(parser.getString()));
                                    break;
                               case "areaId":
                                    areaVO.setAreaId(UUID.fromString(parser.getString()));
                                    break;
                               case "siteName":
                                    areaVO.setSiteName(parser.getString());
                                    break;
                               case "areaName":
                                    areaVO.setAreaName(parser.getString());
                                    break;
                                case "description":
                                    areaVO.setDescription(parser.getString());
                                    break;
                                case "environment":
                                    areaVO.setEnvironment(parser.getString());
                                    break;
                            }
                        }
                        break;
                    
                    case VALUE_TRUE:
                        if(areaVO != null && inArray && keyName != null) {
                            switch(keyName) {
                                case "climateControlled":
                                    areaVO.setClimateControlled(true);
                                    break;
                            }
                        }
                        break;
                    case VALUE_FALSE:
                        if(areaVO != null && inArray && keyName != null) {
                            switch(keyName) {
                                case "climateControlled":
                                    areaVO.setClimateControlled(false);
                                    break;
                            }
                        }
                        break;
                }
            }
        } catch (Exception e) {
            Logger.getLogger(AreaSaxJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }
    
    /**
     * Converts a json to a List Object of AreaConfigScheduleVO Objects
     * @param content, String Object, json 
     * @return List Object of AreaConfigScheduleVO Objects
     */
    public List<AreaConfigScheduleVO> populateAreaConfigScheduleFromJson(String content) {
        List<AreaConfigScheduleVO> list = new ArrayList();
        String keyName;
        AreaConfigScheduleVO acsVO;
        boolean inArray;
        try {
            parser = Json.createParser(new StringReader(content));
            keyName = null;
            acsVO = null;
            inArray = false;
            
            while (parser.hasNext()) {
                switch((event = parser.next())) {
                    case START_ARRAY:
                        inArray = true;
                        break;
                    case END_ARRAY:
                        inArray = false;
                        keyName = null;
                        acsVO = null;
                        break;
                    case START_OBJECT:
                        acsVO = inArray ? new AreaConfigScheduleVO() : null;
                        break;
                    case END_OBJECT:
                        if(acsVO != null && inArray)
                            list.add(acsVO);
                        acsVO = null;
                        keyName = null;
                        break;
                    case KEY_NAME:
                            switch(parser.getString()) {
                                case "customerAccount":
                                    keyName = "customerAccount";
                                    break;
                                case "siteId":
                                    keyName = "siteId";
                                    break;
                                case "areaId":
                                    keyName = "areaId";
                                     break;
                                case "scheduleId":
                                    keyName = "scheduleId";
                                     break;
                                case "siteName":
                                    keyName = "siteName";
                                    break;
                                case "areaName":
                                    keyName = "areaName";
                                    break;
                                case "scheduleName":
                                    keyName = "scheduleName";
                                    break;
                                case "dayOfWeek":
                                    keyName = "dayOfWeek";
                                    break;
                                case "description":
                                    keyName = "description";
                                    break;
                                case "continuous":
                                    keyName = "continuous";
                                    break;
                                case "hoursOfDay":
                                    keyName = "hoursOfDay";
                                    break;
                                default:
                                    keyName = null;
                                    break;      
                            }
                        break;
                    case VALUE_STRING:
                        if(acsVO != null && inArray && keyName != null) {
                            switch(keyName) {
                                case "customerAccount":
                                    acsVO.setCustomerAccount(parser.getString());
                                    break;
                                case "siteId":
                                    acsVO.setSiteId(UUID.fromString(parser.getString()));
                                    break;
                                case "areaId":
                                     acsVO.setAreaId(UUID.fromString(parser.getString()));
                                     break;
                                case "scheduleId":
                                     acsVO.setScheduleId(UUID.fromString(parser.getString()));
                                     break;
                                case "siteName":
                                    acsVO.setSiteName(parser.getString());
                                    break;
                                case "areaName":
                                    acsVO.setAreaName(parser.getString());
                                    break;
                                case "description":
                                    acsVO.setSchDescription(parser.getString());
                                    break;
                                case "hoursOfDay":
                                    acsVO.setHoursOfDay(parser.getString());
                                    break;
                                case "scheduleName":
                                    acsVO.setScheduleName(parser.getString());
                                    break;
                            }
                        }
                        break;
                    case VALUE_NUMBER:
                        if(acsVO != null && inArray && keyName != null) {
                            switch(keyName) {
                                case "dayOfWeek":
                                    acsVO.setDayOfWeek((byte)parser.getInt());
                                    break;
                            }
                        }
                        break;
                    case VALUE_TRUE:
                        if(acsVO != null && inArray && keyName != null) {
                            switch(keyName) {
                                case "continuous":
                                    acsVO.setContinuous(true);
                                    break;
                            }
                        }
                        break;
                    case VALUE_FALSE:
                        if(acsVO != null && inArray && keyName != null) {
                            switch(keyName) {
                                case "continuous":
                                    acsVO.setContinuous(false);
                                    break;
                            }
                        }
                        break;
                }
            }
        } catch (Exception e) {
            Logger.getLogger(AreaSaxJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }

    /**
     * Converts a json to a List Object of AreaConfigScheduleVO Objects
     * @param content, String Object, json 
     * @return List Object of AreaConfigScheduleVO Objects
     */
    public AreaVO populateAreaListFromJson(String content) {
        AreaVO areaVO = new AreaVO();
        String keyName;
        AreaConfigScheduleVO acsVO;
        List<AreaConfigScheduleVO> acsVOList = new ArrayList();
        boolean inArray;
        try {
            parser = Json.createParser(new StringReader(content));
            keyName = null;
            acsVO = null;
            inArray = false;
            
            while (parser.hasNext()) {
                switch ((event = parser.next())) {
                    case START_ARRAY:
                        inArray = true;
                        break;
                    case END_ARRAY:
                        inArray = false;
                        keyName = null;
                        acsVO = null;
                        break;
                    case START_OBJECT:
                        acsVO = inArray ? new AreaConfigScheduleVO() : null;
                        break;
                    case END_OBJECT:
                        if (acsVO != null && inArray) {
                            acsVOList.add(acsVO);
                        }
                        acsVO = null;
                        keyName = null;
                        break;
                    case KEY_NAME:
                        switch (parser.getString()) {
                            case "customerAccount":
                                keyName = "customerAccount";
                                break;
                            case "siteId":
                                keyName = "siteId";
                                break;
                            case "areaId":
                                keyName = "areaId";
                                break;
                            case "scheduleId":
                                keyName = "scheduleId";
                                break;
                            case "siteName":
                                keyName = "siteName";
                                break;
                            case "areaName":
                                keyName = "areaName";
                                break;
                            case "scheduleName":
                                keyName = "scheduleName";
                                break;
                            case "dayOfWeek":
                                keyName = "dayOfWeek";
                                break;
                            case "description":
                                keyName = "description";
                                break;
                            case "continuous":
                                keyName = "continuous";
                                break;
                            case "hoursOfDay":
                                keyName = "hoursOfDay";
                                break;
                            case "environment":
                                keyName = "environment";
                                break;
                            case "climateControlled":
                                keyName = "climateControlled";
                                break;
                            default:
                                keyName = null;
                                break;
                        }
                        break;
                    case VALUE_STRING:
                        if (acsVO != null && inArray && keyName != null) {
                            switch (keyName) {
                                case "customerAccount":
                                    acsVO.setCustomerAccount(parser.getString());
                                    break;
                                case "siteName":
                                    acsVO.setSiteName(parser.getString());
                                    break;
                                case "areaName":
                                    acsVO.setAreaName(parser.getString());
                                    break;
                                case "siteId":
                                    acsVO.setSiteId(UUID.fromString(parser.getString()));
                                    break;
                                case "areaId":
                                    acsVO.setAreaId(UUID.fromString(parser.getString()));
                                    break;
                                case "description":
                                    acsVO.setSchDescription(parser.getString());
                                    break;
                                    case "scheduleId":
                                    acsVO.setScheduleId(UUID.fromString(parser.getString()));
                                    break;
                                case "hoursOfDay":
                                    acsVO.setHoursOfDay(parser.getString());
                                    break;
                                case "scheduleName":
                                    acsVO.setScheduleName(parser.getString());
                                    break;
                            }
                        } else if (keyName != null) {
                            switch (keyName) {
                                case "customerAccount":
                                    areaVO.setCustomerAccount(parser.getString());
                                    break;
                                    case "siteId":
                                    areaVO.setSiteId(UUID.fromString(parser.getString()));
                                    break;
                                case "areaId":
                                    areaVO.setAreaId(UUID.fromString(parser.getString()));
                                    break;
                                case "siteName":
                                    areaVO.setSiteName(parser.getString());
                                    break;
                                case "areaName":
                                    areaVO.setAreaName(parser.getString());
                                    break;
                                case "description":
                                    areaVO.setDescription(parser.getString());
                                    break;
                                case "environment":
                                    areaVO.setEnvironment(parser.getString());
                                    break;
                            }
                        }
                        break;
                    case VALUE_NUMBER:
                        if (acsVO != null && inArray && keyName != null) {
                            switch (keyName) {
                                case "dayOfWeek":
                                    acsVO.setDayOfWeek((byte) parser.getInt());
                                    break;
                            }
                        }
                        break;
                    case VALUE_TRUE:
                        if (acsVO != null && inArray && keyName != null) {
                            switch (keyName) {
                                case "continuous":
                                    acsVO.setContinuous(true);
                                    break;
                            }
                        } else if (keyName != null) {
                            switch (keyName) {
                                case "climateControlled":
                                    areaVO.setClimateControlled(true);
                                    break;
                            }
                        }
                        break;
                    case VALUE_FALSE:
                        if (acsVO != null && inArray && keyName != null) {
                            switch (keyName) {
                                case "continuous":
                                    acsVO.setContinuous(false);
                                    break;
                            }
                        } else if (keyName != null) {
                            switch (keyName) {
                                case "climateControlled":
                                    areaVO.setClimateControlled(false);
                                    break;
                            }
                        }
                        break;
                }
            }
            areaVO.setScheduleVOList(acsVOList);
        } catch (Exception e) {
            Logger.getLogger(AreaSaxJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
        }
        return areaVO;
    }

}
