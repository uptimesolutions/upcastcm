/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.http.utils.json.sax;

import com.uptime.client.utils.SaxJsonParser;
import com.uptime.upcastcm.utils.vo.AssetTypesVO;
import com.uptime.upcastcm.utils.vo.AssetVO;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;
import static javax.json.stream.JsonParser.Event.END_ARRAY;
import static javax.json.stream.JsonParser.Event.END_OBJECT;
import static javax.json.stream.JsonParser.Event.KEY_NAME;
import static javax.json.stream.JsonParser.Event.START_ARRAY;
import static javax.json.stream.JsonParser.Event.START_OBJECT;
import static javax.json.stream.JsonParser.Event.VALUE_FALSE;
import static javax.json.stream.JsonParser.Event.VALUE_NUMBER;
import static javax.json.stream.JsonParser.Event.VALUE_STRING;
import static javax.json.stream.JsonParser.Event.VALUE_TRUE;

/**
 *
 * @author madhavi
 */
public class AssetSaxJsonParser extends SaxJsonParser {
    private static AssetSaxJsonParser instance = null;
    
    /**
     * Private Singleton class
     */
    private AssetSaxJsonParser() {
    }

    /**
     * Returns an instance of the class
     * @return AssetSaxJsonParser object
     */
    public static AssetSaxJsonParser getInstance() {
        if (instance == null) {
            instance = new AssetSaxJsonParser();
        }
        return instance; 
    }

    /**
     * Converts a json to a List Object of AssetVO Objects
     * @param content, String Object, json 
     * @return List Object of AssetVO Objects
     */
    public List<AssetVO> populateAssetVOFromJson(String content) {
        List<AssetVO> list = new ArrayList();
        String keyName;
        AssetVO assetVO;
        boolean inArray;
       
        try {
            parser = Json.createParser(new StringReader(content));
            keyName = null;
            assetVO = null;
            inArray = false;
            
            while (parser.hasNext()) {
                switch((event = parser.next())) {
                    case START_ARRAY:
                        inArray = true;
                        break;
                    case END_ARRAY:
                        inArray = false;
                        keyName = null;
                        assetVO = null;
                        break;
                    case START_OBJECT:
                        assetVO = inArray ? new AssetVO() : null;
                        break;
                    case END_OBJECT:
                        if(assetVO != null && inArray)
                            list.add(assetVO);
                        assetVO = null;
                        keyName = null;
                        break;
                    case KEY_NAME:
                            switch(parser.getString()) {
                                case "customerAccount":
                                    keyName = "customerAccount";
                                    break;
                                case "siteName":
                                    keyName = "siteName";
                                    break;
                                case "areaName":
                                    keyName = "areaName";
                                    break;
                                case "siteId":
                                    keyName = "siteId";
                                    break;
                                case "areaId":
                                    keyName = "areaId";
                                    break;
                                case "assetId":
                                    keyName = "assetId";
                                    break;
                                case "assetName":
                                    keyName = "assetName";
                                    break;
                                case "assetTypeId":
                                    keyName = "assetTypeId";
                                    break;
                                case "assetType":
                                    keyName = "assetType";
                                    break;
                                case "description":
                                    keyName = "description";
                                    break;
                                case "sampleInterval":
                                    keyName = "sampleInterval";
                                    break; 
                                case "alarmEnabled":
                                    keyName = "alarmEnabled";
                                    break; 
                                default:
                                    keyName = null;
                                    break;      
                            }
                        break;
                    case VALUE_STRING:
                        if(assetVO != null && inArray && keyName != null) {
                            switch(keyName) {
                                case "customerAccount":
                                    assetVO.setCustomerAccount(parser.getString());
                                    break;
                                case "siteName":
                                    assetVO.setSiteName(parser.getString());
                                    break;
                                case "areaName":
                                    assetVO.setAreaName(parser.getString());
                                    break;
                                case "siteId":
                                    assetVO.setSiteId(UUID.fromString(parser.getString()));
                                    break;
                                case "areaId":
                                    assetVO.setAreaId(UUID.fromString(parser.getString()));
                                    break;
                                case "assetId":
                                    assetVO.setAssetId(UUID.fromString(parser.getString()));
                                    break;
                                case "assetName":
                                    assetVO.setAssetName(parser.getString());
                                    break;
                                case "assetType":
                                    assetVO.setAssetType(parser.getString());
                                    break;
                                case "assetTypeId":
                                    assetVO.setAssetTypeId(UUID.fromString(parser.getString()));
                                    break;
                                case "description":
                                    assetVO.setDescription(parser.getString());
                                    break;                               
                            }
                        }
                        break;
                    case VALUE_NUMBER:
                        if(assetVO != null && inArray && keyName != null) {
                            switch(keyName) {
                                case "sampleInterval":
                                    assetVO.setSampleInterval(parser.getInt());
                                    break;
                            }
                        }
                        break;
                        case VALUE_TRUE:
                        if(assetVO != null && inArray && keyName != null) {
                            switch(keyName) {
                                case "alarmEnabled":
                                    assetVO.setAlarmEnabled(true);
                                    break;
                            }
                        }
                        break;
                        case VALUE_FALSE:
                        if(assetVO != null && inArray && keyName != null) {
                            switch(keyName) {
                                case "alarmEnabled":
                                     assetVO.setAlarmEnabled(false);
                                    break;
                            }
                        }
                        break;
                }
            }
        } catch (Exception e) {
            Logger.getLogger(AssetSaxJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }
    
    /**
     * Converts a json to a List Object of AssetTypesVO Objects
     *
     * @param content, String Object, json
     * @return List Object of AssetTypesVO Objects
     */
    public List<AssetTypesVO> populateAssetTypesFromJson(String content) {
        List<AssetTypesVO> list = new ArrayList();
        String keyName;
        AssetTypesVO assetTypesVO;
        boolean inArray;

        try {
            parser = Json.createParser(new StringReader(content));
            keyName = null;
            assetTypesVO = null;
            inArray = false;

            while (parser.hasNext()) {
                switch ((event = parser.next())) {
                    case START_ARRAY:
                        inArray = true;
                        break;
                    case END_ARRAY:
                        inArray = false;
                        keyName = null;
                        assetTypesVO = null;
                        break;
                    case START_OBJECT:
                        assetTypesVO = inArray ? new AssetTypesVO() : null;
                        break;
                    case END_OBJECT:
                        if (assetTypesVO != null && inArray) {
                            list.add(assetTypesVO);
                        }
                        assetTypesVO = null;
                        keyName = null;
                        break;
                    case KEY_NAME:
                        switch (parser.getString()) {
                            case "customerAccount":
                                keyName = "customerAccount";
                                break; 
                            case "assetTypeName":
                                keyName = "assetTypeName";
                                break;                            
                            case "description":
                                keyName = "description";
                                break;                            
                            default:
                                keyName = null;
                                break;
                        }
                        break;
                    case VALUE_STRING:
                        if (assetTypesVO != null && inArray && keyName != null) {
                            switch (keyName) {
                                case "customerAccount":
                                    assetTypesVO.setCustomerAccount(parser.getString());
                                break;                                        
                                
                                case "assetTypeName":
                                     assetTypesVO.setAssetTypeName(parser.getString());
                                break;
                                case "description":
                                    assetTypesVO.setDescription(parser.getString());
                                 break;
                            }
                        }
                        break;
                    case VALUE_NUMBER:
                       
                        break;
                    case VALUE_TRUE:
                        
                        break;
                    case VALUE_FALSE:
                         
                        break;
                }
            }
        } catch (Exception e) {
            Logger.getLogger(AlarmsSaxJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }
}
