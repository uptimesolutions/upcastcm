/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.http.utils.json.dom;

import com.uptime.client.utils.DomJsonParser;
import com.uptime.client.utils.JsonConverterUtil;
import com.uptime.upcastcm.utils.vo.DevicePresetVO;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author kpati
 */
public class DevicePresetDomJsonParser extends DomJsonParser {
    private static DevicePresetDomJsonParser instance = null;

    /**
     * Private Singleton class
     */
    private DevicePresetDomJsonParser() {
    }

    /**
     * Returns an instance of the class
     *
     * @return SiteDeviceDomJsonParser object
     */
    public static DevicePresetDomJsonParser getInstance() {
        if (instance == null) {
            instance = new DevicePresetDomJsonParser();
        }
        return instance;
    }
    
    /**
     * Convert a DevicePresetVO Object to a String Object json
     *
     * @param devicePresetVO, SiteVO Object
     * @return String Object
     */
    public String getJsonFromSiteDevicePreset(DevicePresetVO devicePresetVO) {
        try {
            if (devicePresetVO != null) {
                return JsonConverterUtil.toJSON(devicePresetVO);
            }
        } catch (Exception e) {
            Logger.getLogger(DevicePresetDomJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
        }
        return null;
    }
    
    /**
     * Convert a list of DevicePresetVO Objects to a String Object json
     * 
     * @param DevicePresetVOList, List of DevicePresetVOs Object
     * @return String Object
     */
    public String getJsonFromDevicePresetVO(List<DevicePresetVO> DevicePresetVOList) {
        try {
            if (DevicePresetVOList != null) {
                return JsonConverterUtil.toJSON(DevicePresetVOList);
            }
        } catch (Exception e) {
            Logger.getLogger(DevicePresetDomJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
        }
        return null;
    }
}
