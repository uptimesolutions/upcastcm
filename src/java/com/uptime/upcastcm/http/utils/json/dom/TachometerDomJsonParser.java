/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.http.utils.json.dom;

import com.uptime.client.utils.DomJsonParser;
import com.uptime.client.utils.JsonConverterUtil;
import com.uptime.upcastcm.utils.vo.TachometerVO;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author twilcox
 */
public class TachometerDomJsonParser extends DomJsonParser {
    private static TachometerDomJsonParser instance = null;
    
    /**
     * Private Singleton class
     */
    private TachometerDomJsonParser() {
    }

    /**
     * Returns an instance of the class
     * 
     * @return TachometerDomJsonParser object
     */
    public static TachometerDomJsonParser getInstance() {
        if (instance == null) {
            instance = new TachometerDomJsonParser();
        }
        return instance; 
    }

    /**
     * Convert a TachometerVO Object to a String Object json
     * 
     * @param tachometerVO, TachometerVO Object
     * @return String Object
     */
    public Object getJsonFromTachometersVO(TachometerVO tachometerVO) {
        try {
            if (tachometerVO != null) {
                return JsonConverterUtil.toJSON(tachometerVO);
            }
        } catch (Exception e) {
            Logger.getLogger(TachometerDomJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
        }
        return null;
    }
}
