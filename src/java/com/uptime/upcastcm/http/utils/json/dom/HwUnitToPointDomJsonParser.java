/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.http.utils.json.dom;

import com.uptime.client.utils.DomJsonParser;
import com.uptime.client.utils.JsonConverterUtil;
import com.uptime.upcastcm.utils.vo.HwUnitVO;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author kpati
 */
public class HwUnitToPointDomJsonParser extends DomJsonParser {
    private static HwUnitToPointDomJsonParser instance = null;

    /**
     * Private Singleton class
     */
    private HwUnitToPointDomJsonParser() {
    }

    /**
     * Returns an instance of the class
     *
     * @return HwUnitToPointDomJsonParser object
     */
    public static HwUnitToPointDomJsonParser getInstance() {
        if (instance == null) {
            instance = new HwUnitToPointDomJsonParser();
        }
        return instance;
    }
    
    /**
     * Convert a HwUnitVO Object to a String Object json
     *
     * @param hwUnitVO, HwUnitVO Object
     * @return String Object
     */
    public String getJsonFromHwUnitToPoint(HwUnitVO hwUnitVO) {
        try {
            if (hwUnitVO != null) {
                return JsonConverterUtil.toJSON(hwUnitVO);
            }
        } catch (Exception e) {
            Logger.getLogger(HwUnitToPointDomJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
        }
        return null;
    }
    
    /**
     * Convert a HwUnitVO Object to a String Object json
     *
     * @param hwUnitVO, LdapUserVO Object
     * @param requestType, String Object
     * @return String Object
     */
    public String getJsonFromObject(HwUnitVO hwUnitVO, String requestType) {
        StringBuilder sb;
        
        try {
            sb = new StringBuilder();
            sb.append("{\"request\":\"").append(requestType).append("\",\"attributes\":");
            if (hwUnitVO != null) {
                sb.append(JsonConverterUtil.toJSON(hwUnitVO));
            }
            sb.append("}");
            return sb.toString();
        } catch (Exception e) {
            Logger.getLogger(HwUnitToPointDomJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
        }
        
        return null;
    }
}
