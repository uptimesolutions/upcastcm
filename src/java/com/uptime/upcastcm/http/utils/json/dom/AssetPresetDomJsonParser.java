/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.http.utils.json.dom;

import com.uptime.client.utils.DomJsonParser;
import com.uptime.client.utils.JsonConverterUtil;
import com.uptime.upcastcm.utils.vo.AssetPresetVO;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author joseph
 */
public class AssetPresetDomJsonParser extends DomJsonParser {

    private static AssetPresetDomJsonParser instance = null;

    /**
     * Private Singleton class
     */
    private AssetPresetDomJsonParser() {
    }

    /**
     * Returns an instance of the class
     *
     * @return SiteAssetDomJsonParser object
     */
    public static AssetPresetDomJsonParser getInstance() {
        if (instance == null) {
            instance = new AssetPresetDomJsonParser();
        }
        return instance;
    }

    /**
     * Convert a AssetPresetVO Object to a String Object json
     *
     * @param assetPresetVO, SiteVO Object
     * @return String Object
     */
    public String getJsonFromSiteAssetPreset(AssetPresetVO assetPresetVO) {
        try {
            if (assetPresetVO != null) {
                return JsonConverterUtil.toJSON(assetPresetVO);
            }
        } catch (Exception e) {
            Logger.getLogger(AssetPresetDomJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
        }
        return null;
    }

    /**
     * Convert a list of AssetPresetVO Objects to a String Object json
     *
     * @param AssetPresetVOList, List of AssetPresetVOs Object
     * @return String Object
     */
    public String getJsonFromAssetPresetVO(List<AssetPresetVO> AssetPresetVOList) {
        try {
            if (AssetPresetVOList != null) {
                return JsonConverterUtil.toJSON(AssetPresetVOList);
            }
        } catch (Exception e) {
            Logger.getLogger(AssetPresetDomJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
        }
        return null;
    }
}
