/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.http.utils.json.sax;

import com.uptime.client.utils.SaxJsonParser;
import com.uptime.upcastcm.utils.vo.PointToHwVO;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.json.Json;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author kpati
 */
public class PointToHwUnitSaxJsonParser extends SaxJsonParser {
    
     private static PointToHwUnitSaxJsonParser instance = null;

    /**
     * Private Singleton class
     */
    private PointToHwUnitSaxJsonParser() {
    }

    /**
     * Returns an instance of the class
     *
     * @return PointToHwUnitSaxJsonParser object
     */
    public static PointToHwUnitSaxJsonParser getInstance() {
        if (instance == null) {
            instance = new PointToHwUnitSaxJsonParser();
        }
        return instance;
    }

    /**
     * Converts a json to a List Object of PointToHwVO Objects
     *
     * @param content, String Object, json
     * @return List Object of PointToHwVO Objects
     */
    public List<PointToHwVO> populatePointToHwUnitFromJson(String content) {
        List<PointToHwVO> list = new ArrayList();
        String keyName;
        PointToHwVO pointToHwVO;
        boolean inArray;

        try {
            parser = Json.createParser(new StringReader(content));
            keyName = null;
            pointToHwVO = null;
            inArray = false;

            while (parser.hasNext()) {
                switch ((event = parser.next())) {
                    case START_ARRAY:
                        inArray = true;
                        break;
                    case END_ARRAY:
                        inArray = false;
                        keyName = null;
                        pointToHwVO = null;
                        break;
                    case START_OBJECT:
                        pointToHwVO = inArray ? new PointToHwVO() : null;
                        break;
                    case END_OBJECT:
                        if (pointToHwVO != null && inArray) {
                            list.add(pointToHwVO);
                        }
                        pointToHwVO = null;
                        keyName = null;
                        break;
                    case KEY_NAME:
                        switch (parser.getString()) {
                            case "deviceId":
                                keyName = "deviceId";
                                break;
                            case "customerAcct":
                                keyName = "customerAcct";
                                break;
                            case "channelType":
                                keyName = "channelType";
                                break;
                            case "channelNum":
                                keyName = "channelNum";
                                break;
                            case "siteId":
                                keyName = "siteId";
                                break;
                            case "areaId":
                                keyName = "areaId";
                                break;
                            case "assetId":
                                keyName = "assetId";
                                break;
                            case "pointLocationId":
                                keyName = "pointLocationId";
                                break;
                            case "pointId":
                                keyName = "pointId";
                                break;
                            case "apSetId":
                                keyName = "apSetId";
                                break;
                            default:
                                keyName = null;
                                break;
                        }
                        break;
                    case VALUE_STRING:
                        if (pointToHwVO != null && inArray && keyName != null) {
                            switch (keyName) {
                                case "pointId":
                                    try {
                                    pointToHwVO.setPointId(UUID.fromString(parser.getString()));
                                } catch (Exception ex) {
                                }
                                break;
                                case "deviceId":
                                    pointToHwVO.setDeviceId(parser.getString());
                                    break;
                                case "channelType":
                                    pointToHwVO.setChannelType(parser.getString());
                                    break;
                            }
                        }
                        break;
                    case VALUE_NUMBER:
                        if (pointToHwVO != null && inArray && keyName != null) {
                            switch (keyName) {
                                case "channelNum":
                                    pointToHwVO.setChannelNum(parser.getBigDecimal().byteValue());
                                    break;
                            }
                        }
                        break;
                }
            }
        } catch (Exception e) {
            Logger.getLogger(AlarmsSaxJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }

    
}
