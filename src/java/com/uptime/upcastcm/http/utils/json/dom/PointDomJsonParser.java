/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.http.utils.json.dom;

import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.uptime.client.utils.DomJsonParser;
import com.uptime.client.utils.JsonConverterUtil;
import com.uptime.upcastcm.utils.vo.ApAlSetVO;
import com.uptime.upcastcm.utils.vo.PointVO;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author twilcox
 */
public class PointDomJsonParser extends DomJsonParser {
    private static PointDomJsonParser instance = null;
    
    /**
     * Private Singleton class
     */
    private PointDomJsonParser() {
    }

    /**
     * Returns an instance of the class
     * @return PointDomJsonParser object
     */
    public static PointDomJsonParser getInstance() {
        if (instance == null) {
            instance = new PointDomJsonParser();
        }
        return instance; 
    }
    
    /**
     * Convert a PointVO Object to a String Object json
     *
     * @param pointVO, PointVO Object
     * @return String Object
     */
    public String getJsonFromPointVO(PointVO pointVO) {
        try {
            if (pointVO != null) {
                return JsonConverterUtil.toJSON(pointVO);
            }
        } catch (Exception e) {
            Logger.getLogger(PointDomJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
        }
        return null;
    }

    /**
     * Convert a PointVO list Object to a String Object json
     *
     * @param pointVO, PointVO Object
     * @return String Object
     */
    public String getJsonFromPointVO(List<PointVO> pointVO) {
        try {
            if (pointVO != null) {
                return JsonConverterUtil.toJSON(pointVO);
            }
        } catch (Exception e) {
            Logger.getLogger(PointDomJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
        }
        return null;
    }
    
    /**
     * Converts a json to a List Object of PointVO Objects
     * 
     * @param content, String Object, json 
     * @return List Object of PointVO Objects
     */
    public List<PointVO> populatePointsWithApAlSetsFromJson(String content) {
        List<PointVO> list = new ArrayList();
        List<ApAlSetVO> apAlsetList;
        ApAlSetVO apAlSetVO;
        PointVO pointVO;

        try {
            if (content != null && !content.isEmpty() && (element = JsonParser.parseString(content)) != null && (jsonObject = element.getAsJsonObject()) != null) {
                if (jsonObject.has("Points") && (array = jsonObject.getAsJsonArray("Points")) != null) {
                    for (int i = 0; i < array.size(); i++) {
                        if ((jsonObject = array.get(i).getAsJsonObject()) != null) {
                            pointVO = new PointVO();
                            apAlsetList = new ArrayList();
                            
                            if (jsonObject.has("customerAccount")) {
                                try {
                                    pointVO.setCustomerAccount(jsonObject.get("customerAccount").getAsString());
                                } catch (Exception ex) {
                                }
                            }
                            if (jsonObject.has("siteId")) {
                                try {
                                    pointVO.setSiteId(UUID.fromString(jsonObject.get("siteId").getAsString()));
                                } catch (Exception ex) {
                                }
                            }
                            if (jsonObject.has("areaId")) {
                                try {
                                    pointVO.setAreaId(UUID.fromString(jsonObject.get("areaId").getAsString()));
                                } catch (Exception ex) {
                                }
                            }
                            if (jsonObject.has("assetId")) {
                                try {
                                    pointVO.setAssetId(UUID.fromString(jsonObject.get("assetId").getAsString()));
                                } catch (Exception ex) {
                                }
                            }
                            if (jsonObject.has("pointLocationId")) {
                                try {
                                    pointVO.setPointLocationId(UUID.fromString(jsonObject.get("pointLocationId").getAsString()));
                                } catch (Exception ex) {
                                }
                            }
                            if (jsonObject.has("pointId")) {
                                try {
                                    pointVO.setPointId(UUID.fromString(jsonObject.get("pointId").getAsString()));
                                } catch (Exception ex) {
                                }
                            }
                            if (jsonObject.has("apSetId")) {
                                try {
                                    pointVO.setApSetId(UUID.fromString(jsonObject.get("apSetId").getAsString()));
                                } catch (Exception ex) {
                                }
                            }
                            if (jsonObject.has("alSetId")) {
                                try {
                                    pointVO.setAlSetId(UUID.fromString(jsonObject.get("alSetId").getAsString()));
                                } catch (Exception ex) {
                                }
                            }
                            if (jsonObject.has("disabled")) {
                                try {
                                    pointVO.setDisabled(jsonObject.get("disabled").getAsBoolean());
                                } catch (Exception ex) {
                                }
                            }
                            if (jsonObject.has("pointName")) {
                                try {
                                    pointVO.setPointName(jsonObject.get("pointName").getAsString());
                                } catch (Exception ex) {
                                }
                            }
                            if (jsonObject.has("pointType")) {
                                try {
                                    pointVO.setPointType(jsonObject.get("pointType").getAsString());
                                } catch (Exception ex) {
                                }
                            }
                            if (jsonObject.has("sensorChannelNum")) {
                                try {
                                    pointVO.setSensorChannelNum(jsonObject.get("sensorChannelNum").getAsInt());
                                } catch (Exception ex) {
                                }
                            }
                            if (jsonObject.has("sensorOffset")) {
                                try {
                                    pointVO.setSensorOffset(jsonObject.get("sensorOffset").getAsFloat());
                                } catch (Exception ex) {
                                }
                            }
                            if (jsonObject.has("sensorType")) {
                                try {
                                    pointVO.setSensorType(jsonObject.get("sensorType").getAsString());
                                } catch (Exception ex) {
                                }
                            }
                            if (jsonObject.has("sensorUnits")) {
                                try {
                                    pointVO.setSensorUnits(jsonObject.get("sensorUnits").getAsString());
                                } catch (Exception ex) {
                                }
                            }
                            if (jsonObject.has("disabled")) {
                                try {
                                    pointVO.setDisabled(jsonObject.get("disabled").getAsBoolean());
                                } catch (Exception ex) {
                                }
                            }
                            if (jsonObject.has("alarmEnabled")) {
                                try {
                                    pointVO.setAlarmEnabled(jsonObject.get("alarmEnabled").getAsBoolean());
                                } catch (Exception ex) {
                                }
                            }
                            JsonArray arrayapsets;
                            
                            if (jsonObject.has("ApAlByPoints") && (arrayapsets = jsonObject.getAsJsonArray("ApAlByPoints")) != null) {
                                for (int j = 0; j < arrayapsets.size(); j++) {
                                    if ((jsonObject = arrayapsets.get(j).getAsJsonObject()) != null) {
                                        apAlSetVO = new ApAlSetVO();

                                        if (jsonObject.has("customerAccount")) {
                                            try {
                                                apAlSetVO.setCustomerAccount(jsonObject.get("customerAccount").getAsString());
                                            } catch (Exception ex) {
                                            }
                                        }
                                        if (jsonObject.has("siteId")) {
                                            try {
                                                apAlSetVO.setSiteId(UUID.fromString(jsonObject.get("siteId").getAsString()));
                                            } catch (Exception ex) {
                                            }
                                        }
                                        if (jsonObject.has("apSetId")) {
                                            try {
                                                apAlSetVO.setApSetId(UUID.fromString(jsonObject.get("apSetId").getAsString()));
                                            } catch (Exception ex) {
                                            }
                                        }
                                        if (jsonObject.has("alSetId")) {
                                            try {
                                                apAlSetVO.setAlSetId(UUID.fromString(jsonObject.get("alSetId").getAsString()));
                                            } catch (Exception ex) {
                                            }
                                        }
                                        if (jsonObject.has("paramName")) {
                                            try {
                                                apAlSetVO.setParamName(jsonObject.get("paramName").getAsString());
                                            } catch (Exception ex) {
                                            }
                                        }
                                        if (jsonObject.has("freqUnits")) {
                                            try {
                                                apAlSetVO.setFrequencyUnits(jsonObject.get("freqUnits").getAsString());
                                            } catch (Exception ex) {
                                            }
                                        }
                                        if (jsonObject.has("demodEnabled")) {
                                            try {
                                                apAlSetVO.setDemod(jsonObject.get("demodEnabled").getAsBoolean());
                                            } catch (Exception ex) {
                                            }
                                        }
                                        if (jsonObject.has("demodHighPass")) {
                                            try {
                                                apAlSetVO.setDemodHighPass(jsonObject.get("demodHighPass").getAsFloat());
                                            } catch (Exception ex) {
                                            }
                                        }
                                        if (jsonObject.has("demodLowPass")) {
                                            try {
                                                apAlSetVO.setDemodLowPass(jsonObject.get("demodLowPass").getAsFloat());
                                            } catch (Exception ex) {
                                            }
                                        }
                                        if (jsonObject.has("dwell")) {
                                            try {
                                                apAlSetVO.setDwell(jsonObject.get("dwell").getAsInt());
                                            } catch (Exception ex) {
                                            }
                                        }
                                        if (jsonObject.has("fMax")) {
                                            try {
                                                apAlSetVO.setFmax(jsonObject.get("fMax").getAsInt());
                                            } catch (Exception ex) {
                                            }
                                        }
                                        if (jsonObject.has("highAlert")) {
                                            try {
                                                apAlSetVO.setHighAlert(jsonObject.get("highAlert").getAsFloat());
                                            } catch (Exception ex) {
                                            }
                                        }
                                        if (jsonObject.has("highAlertActive")) {
                                            try {
                                                apAlSetVO.setHighAlertActive(jsonObject.get("highAlertActive").getAsBoolean());
                                            } catch (Exception ex) {
                                            }
                                        }
                                        if (jsonObject.has("highFault")) {
                                            try {
                                                apAlSetVO.setHighFault(jsonObject.get("highFault").getAsFloat());
                                            } catch (Exception ex) {
                                            }
                                        }
                                        if (jsonObject.has("highFaultActive")) {
                                            try {
                                                apAlSetVO.setHighFaultActive(jsonObject.get("highFaultActive").getAsBoolean());
                                            } catch (Exception ex) {
                                            }
                                        }
                                        if (jsonObject.has("lowAlert")) {
                                            try {
                                                apAlSetVO.setLowAlert(jsonObject.get("lowAlert").getAsFloat());
                                            } catch (Exception ex) {
                                            }
                                        }
                                        if (jsonObject.has("lowAlertActive")) {
                                            try {
                                                apAlSetVO.setLowAlertActive(jsonObject.get("lowAlertActive").getAsBoolean());
                                            } catch (Exception ex) {
                                            }
                                        }
                                        if (jsonObject.has("lowFault")) {
                                            try {
                                                apAlSetVO.setLowFault(jsonObject.get("lowFault").getAsFloat());
                                            } catch (Exception ex) {
                                            }
                                        }
                                        if (jsonObject.has("lowFaultActive")) {
                                            try {
                                                apAlSetVO.setLowFaultActive(jsonObject.get("lowFaultActive").getAsBoolean());
                                            } catch (Exception ex) {
                                            }
                                        }
                                        if (jsonObject.has("maxFreq")) {
                                            try {
                                                apAlSetVO.setMaxFrequency(jsonObject.get("maxFreq").getAsFloat());
                                            } catch (Exception ex) {
                                            }
                                        }
                                        if (jsonObject.has("minFreq")) {
                                            try {
                                                apAlSetVO.setMinFrequency(jsonObject.get("minFreq").getAsFloat());
                                            } catch (Exception ex) {
                                            }
                                        }
                                        if (jsonObject.has("paramAmpFactor")) {
                                            try {
                                                apAlSetVO.setParamAmpFactor(jsonObject.get("paramAmpFactor").getAsString());
                                            } catch (Exception ex) {
                                            }
                                        }
                                        if (jsonObject.has("paramUnits")) {
                                            try {
                                                apAlSetVO.setParamUnits(jsonObject.get("paramUnits").getAsString());
                                            } catch (Exception ex) {
                                            }
                                        }
                                        if (jsonObject.has("paramType")) {
                                            try {
                                                apAlSetVO.setParamType(jsonObject.get("paramType").getAsString());
                                            } catch (Exception ex) {
                                            }
                                        }
                                        if (jsonObject.has("period")) {
                                            try {
                                                apAlSetVO.setPeriod(jsonObject.get("period").getAsFloat());
                                            } catch (Exception ex) {
                                            }
                                        }
                                        if (jsonObject.has("resolution")) {
                                            try {
                                                apAlSetVO.setResolution(jsonObject.get("resolution").getAsInt());
                                            } catch (Exception ex) {
                                            }
                                        }
                                        if (jsonObject.has("sampleRate")) {
                                            try {
                                                apAlSetVO.setSampleRate(jsonObject.get("sampleRate").getAsFloat());
                                            } catch (Exception ex) {
                                            }
                                        }
                                        if (jsonObject.has("sensorType")) {
                                            try {
                                                apAlSetVO.setSensorType(jsonObject.get("sensorType").getAsString());
                                            } catch (Exception ex) {
                                            }
                                        }
                                        if (jsonObject.has("customized")) {
                                            try {
                                                apAlSetVO.setCustomized(jsonObject.get("customized").getAsBoolean());
                                            } catch (Exception ex) {
                                            }
                                        }
                                        apAlSetVO.setReadOnly(true);
                                        apAlsetList.add(apAlSetVO);
                                    }
                                }
                            }
                            pointVO.setApAlSetVOs(apAlsetList);
                            list.add(pointVO);
                        }
                    }
                }
            }
        } catch (Exception e) {
            Logger.getLogger(PointDomJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }
}
