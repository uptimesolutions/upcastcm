/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.http.utils.json.sax;

import com.uptime.client.utils.SaxJsonParser;
import com.uptime.upcastcm.utils.vo.AlarmsVO;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;

/**
 *
 * @author gsingh
 */
public class AlarmsSaxJsonParser extends SaxJsonParser {
    private static AlarmsSaxJsonParser instance = null;
    
    /**
     * Private Singleton class
     */
    private AlarmsSaxJsonParser() {
    }

    /**
     * Returns an instance of the class
     * @return AlarmsSaxJsonParser object
     */
    public static AlarmsSaxJsonParser getInstance() {
        if (instance == null) {
            instance = new AlarmsSaxJsonParser();
        }
        return instance; 
    }

    /**
     * Converts a json to a List Object of AlarmsVO Objects
     * @param content, String Object, json 
     * @return List Object of AlarmsVO Objects
     */
    public List<AlarmsVO> populateAlarmsFromJson(String content) {
        List<AlarmsVO> list = new ArrayList();
        String keyName;
        AlarmsVO alarmsVO;
        boolean inArray;
         
        try {
            parser = Json.createParser(new StringReader(content));
            keyName = null;
            alarmsVO = null;
            inArray = false;
            
            while (parser.hasNext()) {
                switch((event = parser.next())) {
                    case START_ARRAY:
                        inArray = true;
                        break;
                    case END_ARRAY:
                        inArray = false;
                        keyName = null;
                        alarmsVO = null;
                        break;
                    case START_OBJECT:
                        alarmsVO = inArray ? new AlarmsVO() : null;
                        break;
                    case END_OBJECT:
                        if(alarmsVO != null && inArray)
                            list.add(alarmsVO); 
                        alarmsVO = null;
                        keyName = null;
                        break;
                    case KEY_NAME:
                            switch(parser.getString()) {
                                case "customerAccount":
                                    keyName = "customerAccount";
                                    break;
                                case "siteId":
                                    keyName = "siteId";
                                    break;
                                case "areaId":
                                    keyName = "areaId";
                                    break;
                                case "assetId":
                                    keyName = "assetId";
                                    break;
                                case "pointLocationId":
                                    keyName = "pointLocationId";
                                    break;
                                case "pointId":
                                    keyName = "pointId";
                                    break;
                                case "apSetId":
                                    keyName = "apSetId";
                                    break;
                                case "exceedPercent":
                                    keyName = "exceedPercent";
                                    break;
                                case "highAlert":
                                    keyName = "highAlert";
                                    break;
                                case "highFault":
                                    keyName = "highFault";
                                    break;
                                case "lowAlert":
                                    keyName = "lowAlert";
                                    break;
                                case "lowFault":
                                    keyName = "lowFault";
                                    break;
                                case "paramType":
                                    keyName = "paramType";
                                    break;
                                case "siteName":
                                    keyName = "siteName";
                                    break;
                                case "sensorType":
                                    keyName = "sensorType";
                                    break;
                                case "trendValue":
                                    keyName = "trendValue";
                                    break;
                                case "createdYear":
                                    keyName = "createdYear";
                                    break;
                                case "createdMonth":
                                    keyName = "createdMonth";
                                    break;
                                case "alSetName":
                                    keyName = "alSetName";
                                    break;
                                case "alarmType":
                                    keyName = "alarmType";
                                    break;
                                case "apSetName":
                                    keyName = "apSetName";
                                    break;
                                case "areaName":
                                    keyName = "areaName";
                                    break;
                                case "assetName":
                                    keyName = "assetName";
                                    break;
                                case "createdDate":
                                    keyName = "createdDate";
                                    break;
                                case "createdTime":
                                    keyName = "createdTime";
                                    break;
                                case "acknowledged":
                                    keyName = "acknowledged";
                                    break;
                                case "deleted":
                                    keyName = "deleted";
                                    break;
                                case "paramName":
                                    keyName = "paramName";
                                    break;
                                case "pointLocationName":
                                    keyName = "pointLocationName";
                                    break;
                                case "pointName":
                                    keyName = "pointName";
                                    break;
                                case "sampleTimestamp":
                                    keyName = "sampleTimestamp";
                                    break;
                                case "triggerTime":
                                    keyName = "triggerTime";
                                    break;
                                case "spectrumId":
                                    keyName = "spectrumId";
                                    break;
                                case "trendId":
                                    keyName = "trendId";
                                    break;
                                case "triggerValue":
                                    keyName = "triggerValue";
                                    break;
                                case "waveId":
                                    keyName = "waveId";
                                    break; 
                                case "alarmCount":
                                    keyName = "alarmCount";
                                    break; 
                                case "alertCount":
                                    keyName = "alertCount";
                                    break; 
                                case "faultCount":
                                    keyName = "faultCount";
                                    break; 
                                default:
                                    keyName = null;
                                    break;      
                            }
                        break;
                    case VALUE_STRING:
                        if(alarmsVO != null && inArray && keyName != null) {
                            switch(keyName) {
                                case "siteId":
                                    try {
                                        alarmsVO.setSiteId(UUID.fromString(parser.getString()));
                                    }catch (Exception ex) {}
                                    break;
                                case "areaId":
                                    try {
                                        alarmsVO.setAreaId(UUID.fromString(parser.getString()));
                                    }catch (Exception ex) {}
                                    break;
                                case "assetId":
                                    try {
                                        alarmsVO.setAssetId(UUID.fromString(parser.getString()));
                                    }catch (Exception ex) {}
                                    break;
                                case "pointLocationId":
                                    try {
                                        alarmsVO.setPointLocationId(UUID.fromString(parser.getString()));
                                    }catch (Exception ex) {}
                                    break;
                                case "pointId":
                                    try {
                                        alarmsVO.setPointId(UUID.fromString(parser.getString()));
                                    }catch (Exception ex) {}
                                    break;                                    
                                case "paramName":
                                    alarmsVO.setParamName(parser.getString());
                                    break;
                                case "alarmType":
                                    alarmsVO.setAlarmType(parser.getString());
                                    break;
                                case "apSetId":
                                    try {
                                        alarmsVO.setApSetId(UUID.fromString(parser.getString()));
                                    }catch (Exception ex) {}
                                    break; 
                                case "paramType":
                                    alarmsVO.setParamType(parser.getString());
                                    break;
                                case "sensorType":
                                    alarmsVO.setSensorType(parser.getString());
                                    break;
                                case "trendId":
                                    try {
                                        alarmsVO.setTrendId(UUID.fromString(parser.getString()));
                                    }catch (Exception ex) {}
                                    break;
                                case "spectrumId":
                                    try {
                                        alarmsVO.setSpectrumId(UUID.fromString(parser.getString()));
                                    }catch (Exception ex) {}
                                    break;
                                case "waveId":
                                    try {
                                        alarmsVO.setWaveId(UUID.fromString(parser.getString()));
                                    }catch (Exception ex) {}
                                    break;
                                case "customerAccount":
                                    alarmsVO.setCustomerAccount(parser.getString());
                                    break;
                                case "siteName":
                                    alarmsVO.setSiteName(parser.getString());
                                    break;
                                case "areaName":
                                    alarmsVO.setAreaName(parser.getString());
                                    break;
                                case "assetName":
                                    alarmsVO.setAssetName(parser.getString());
                                    break;
                                case "alSetName":
                                    alarmsVO.setAlSetName(parser.getString());
                                    break;
                                
                                case "apSetName":
                                    alarmsVO.setApSetName(parser.getString());
                                    break;
                                case "createdTime":
                                    alarmsVO.setCreatedTime(parser.getString());
                                    break;
                                
                                case "pointLocationName":
                                    alarmsVO.setPointLocationName(parser.getString());
                                    break;
                                case "pointName":
                                    alarmsVO.setPointName(parser.getString());
                                    break;
                                case "sampleTimestamp":
                                    alarmsVO.setSampleTimestamp(parser.getString());
                                    break;
                                case "triggerTime":
                                    alarmsVO.setTriggerTime(parser.getString());
                                    break;
                            }
                        }
                        break;
                    case VALUE_NUMBER:
                        if(alarmsVO != null && inArray && keyName != null) {
                            switch(keyName) {
                                case "exceedPercent":
                                    alarmsVO.setExceedPercent(parser.getBigDecimal().floatValue());
                                    break;
                                case "highAlert":
                                    alarmsVO.setHighAlert(parser.getBigDecimal().floatValue());
                                    break;
                                case "highFault":
                                    alarmsVO.setHighFault(parser.getBigDecimal().floatValue());
                                    break;
                                case "lowAlert":
                                    alarmsVO.setLowAlert(parser.getBigDecimal().floatValue());
                                    break;
                                case "lowFault":
                                    alarmsVO.setLowFault(parser.getBigDecimal().floatValue());
                                    break;
                                case "trendValue":
                                    alarmsVO.setTrendValue(parser.getBigDecimal().floatValue());
                                    break;
                                case "createdYear":
                                    alarmsVO.setCreatedYear(parser.getInt());
                                    break;
                                case "createdMonth":
                                    alarmsVO.setCreatedMonth(parser.getInt());
                                    break;
                                case "createdDate":
                                    alarmsVO.setCreatedDate(parser.getInt());
                                    break;
                                case "triggerValue":
                                    alarmsVO.setTriggerValue(parser.getInt());
                                    break;
                                case "alarmCount":
                                    alarmsVO.setAlarmCount(parser.getInt());
                                    break;
                                case "alertCount":
                                    alarmsVO.setAlertCount(parser.getInt());
                                    break;
                                case "faultCount":
                                    alarmsVO.setFaultCount(parser.getInt());
                                    break;
                            }
                        }
                        break;
                    case VALUE_TRUE:
                        if(alarmsVO != null && inArray && keyName != null) {
                            switch(keyName) {
                                case "acknowledged":
                                    alarmsVO.setAcknowledged(true);
                                    break;
                                case "deleted":
                                    alarmsVO.setDeleted(true);
                                    break;
                            }
                        }
                        break;
                    case VALUE_FALSE:
                        if(alarmsVO != null && inArray && keyName != null) {
                            switch(keyName) {
                                case "acknowledged":
                                    alarmsVO.setAcknowledged(false);
                                    break;
                                case "deleted":
                                    alarmsVO.setDeleted(false);
                                    break;
                            }
                        }
                        break;
                }
            }
        } catch (Exception e) {
            Logger.getLogger(AlarmsSaxJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }
}
