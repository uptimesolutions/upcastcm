/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.http.utils.json.sax;

import com.uptime.client.utils.SaxJsonParser;
import com.uptime.upcastcm.utils.vo.PointLocationVO;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;

/**
 *
 * @author twilcox
 */
public class PointLocationSaxJsonParser extends SaxJsonParser {

    private static PointLocationSaxJsonParser instance = null;

    /**
     * Private Singleton class
     */
    private PointLocationSaxJsonParser() {
    }

    /**
     * Returns an instance of the class
     *
     * @return PointLocationSaxJsonParser object
     */
    public static PointLocationSaxJsonParser getInstance() {
        if (instance == null) {
            instance = new PointLocationSaxJsonParser();
        }
        return instance;
    }

    /**
     * Converts a json to a List Object of PointLocationVO Objects
     *
     * @param content, String Object, json
     * @return List Object of PointLocationVO Objects
     */
    public List<PointLocationVO> populatePointLocationsFromJson(String content) {
        List<PointLocationVO> list = new ArrayList();
        String keyName;
        PointLocationVO pointLocationVO;
        boolean inArray;

        try {
            parser = Json.createParser(new StringReader(content));
            keyName = null;
            pointLocationVO = null;
            inArray = false;

            while (parser.hasNext()) {
                switch ((event = parser.next())) {
                    case START_ARRAY:
                        inArray = true;
                        break;
                    case END_ARRAY:
                        inArray = false;
                        keyName = null;
                        pointLocationVO = null;
                        break;
                    case START_OBJECT:
                        pointLocationVO = inArray ? new PointLocationVO() : null;
                        break;
                    case END_OBJECT:
                        if (pointLocationVO != null && inArray) {
                            list.add(pointLocationVO);
                        }
                        pointLocationVO = null;
                        keyName = null;
                        break;
                    case KEY_NAME:
                        switch (parser.getString()) {
                            case "customerAccount":
                                keyName = "customerAccount";
                                break;
                            case "siteId":
                                keyName = "siteId";
                                break;
                            case "areaId":
                                keyName = "areaId";
                                break;
                            case "assetId":
                                keyName = "assetId";
                                break;
                            case "pointLocationId":
                                keyName = "pointLocationId";
                                break;
                            case "pointLocationName":
                                keyName = "pointLocationName";
                                break;
                            case "description":
                                keyName = "description";
                                break;
                            case "deviceSerialNumber":
                                keyName = "deviceSerialNumber";
                                break;
                            case "ffSetIds":
                                keyName = "ffSetIds";
                                break;
                            case "speedRatio":
                                keyName = "speedRatio";
                                break;
                            case "sampleInterval":
                                keyName = "sampleInterval";
                                break;
                            case "tachId":
                                keyName = "tachId";
                                break;
                            case "alarmEnabled":
                                keyName = "alarmEnabled";
                                break;
                            case "basestationPortNum":
                                keyName = "basestationPortNum";
                                break;
                            case "rollDiameter":
                                keyName = "rollDiameter";
                                break;
                            case "rollDiameterUnits":
                                keyName = "rollDiameterUnits";
                                break;
                            default:
                                keyName = null;
                                break;
                        }
                        break;
                    case VALUE_STRING:
                        if (pointLocationVO != null && inArray && keyName != null) {
                            switch (keyName) {
                                case "customerAccount":
                                    pointLocationVO.setCustomerAccount(parser.getString());
                                    break;
                                case "siteId":
                                    pointLocationVO.setSiteId(UUID.fromString(parser.getString()));
                                    break;
                                case "areaId":
                                    pointLocationVO.setAreaId(UUID.fromString(parser.getString()));
                                    break;
                                case "assetId":
                                    pointLocationVO.setAssetId(UUID.fromString(parser.getString()));
                                    break;
                                case "pointLocationId":
                                    pointLocationVO.setPointLocationId(UUID.fromString(parser.getString()));
                                    break;
                                case "pointLocationName":
                                    pointLocationVO.setPointLocationName(parser.getString());
                                    break;
                                case "description":
                                    pointLocationVO.setDescription(parser.getString());
                                    break;
                                case "deviceSerialNumber":
                                    pointLocationVO.setDeviceSerialNumber(parser.getString());
                                    break;
                                case "ffSetIds":
//                                    String[] strFFids = parser.getString().split(",");
//                                    Set<UUID> ffsetIds = new HashSet();
//                                    for (String s : strFFids) {
//                                        UUID ffid = UUID.fromString(s.trim());
//                                        ffsetIds.add(ffid);
//                                    }
                                    pointLocationVO.setFfSetIds(parser.getString());
                                    break;

                                case "rollDiameterUnits":
                                    pointLocationVO.setRollDiameterUnits(parser.getString());
                                    break;
                                case "tachId":
                                    pointLocationVO.setTachId(UUID.fromString(parser.getString()));
                                    break;
                            }
                        }
                        break;
                    case VALUE_NUMBER:
                        if (pointLocationVO != null && inArray && keyName != null) {
                            switch (keyName) {
                                case "speedRatio":
                                    pointLocationVO.setSpeedRatio(parser.getBigDecimal().floatValue());
                                    break;
                                case "sampleInterval":
                                    pointLocationVO.setSampleInterval(parser.getBigDecimal().shortValue());
                                    break;
                                case "basestationPortNum":
                                    pointLocationVO.setBasestationPortNum(parser.getBigDecimal().shortValue());
                                    break;
                                case "rollDiameter":
                                    pointLocationVO.setRollDiameter(parser.getBigDecimal().floatValue());
                                    break;
                            }
                        }
                        break;
                    case VALUE_TRUE:
                        if (pointLocationVO != null && inArray && keyName != null) {
                            switch (keyName) {
                                case "alarmEnabled":
                                    pointLocationVO.setAlarmEnabled(true);
                                    break;
                            }
                        }
                        break;
                    case VALUE_FALSE:
                        if (pointLocationVO != null && inArray && keyName != null) {
                            switch (keyName) {
                                case "alarmEnabled":
                                    pointLocationVO.setAlarmEnabled(false);
                                    break;
                            }
                        }
                        break;
                }
            }
        } catch (Exception e) {
            Logger.getLogger(PointLocationSaxJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }
}
