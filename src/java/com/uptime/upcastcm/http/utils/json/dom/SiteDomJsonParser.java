/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.http.utils.json.dom;

import com.uptime.client.utils.DomJsonParser;
import com.uptime.client.utils.JsonConverterUtil;
import com.uptime.upcastcm.utils.vo.SiteVO;
import java.io.StringReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;

/**
 *
 * @author kpati
 */
public class SiteDomJsonParser extends DomJsonParser {
    private static SiteDomJsonParser instance = null;

    /**
     * Private Singleton class
     */
    private SiteDomJsonParser() {
    }

    /**
     * Returns an instance of the class
     *
     * @return SiteDomJsonParser object
     */
    public static SiteDomJsonParser getInstance() {
        if (instance == null) {
            instance = new SiteDomJsonParser();
        }
        return instance;
    }

    /**
     * Convert a SiteVO Object to a String Object json
     *
     * @param siteVO, SiteVO Object
     * @return String Object
     */
    public String getJsonFromSite(SiteVO siteVO) {
        try {
            if (siteVO != null) {
                return JsonConverterUtil.toJSON(siteVO);
            }
        } catch (Exception e) {
            Logger.getLogger(SiteDomJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
        }
        return null;
    }

    /**
     * Converts a json to an Object of SiteVO Objects
     *
     * @param content, String Object, json
     * @return List Object of SiteVO Objects
     */
    public SiteVO populateSiteVOFromGeocodeJson(String content) {
        SiteVO siteVO = null;
        JsonObject jsonObj;
        JsonArray arrayObj;

        
        if (content != null && !content.isEmpty()) {
            siteVO = new SiteVO();
            
            try (JsonReader jsonReader = Json.createReader(new StringReader(content))) {
                if ((jsonObj = jsonReader.readObject()) != null && (arrayObj = jsonObj.getJsonArray("results")) != null && !arrayObj.isEmpty() &&
                        (jsonObj = arrayObj.getJsonObject(0)) != null && (arrayObj = jsonObj.getJsonArray("locations")) != null && !arrayObj.isEmpty() &&
                        (jsonObj = arrayObj.getJsonObject(0)) != null && jsonObj.containsKey("latLng") && (jsonObj = jsonObj.getJsonObject("latLng")) != null) {
                    siteVO.setLatitude(jsonObj.getJsonNumber("lat").toString());
                    siteVO.setLongitude(jsonObj.getJsonNumber("lng").toString());
                }
                jsonReader.close();
            } catch (Exception e) {
                Logger.getLogger(SiteDomJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
                siteVO = new SiteVO();
            }
        }
        return siteVO;
    }
}
