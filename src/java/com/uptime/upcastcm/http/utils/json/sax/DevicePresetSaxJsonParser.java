/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.http.utils.json.sax;

import com.uptime.client.utils.SaxJsonParser;
import com.uptime.upcastcm.utils.vo.DevicePresetVO;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;

/**
 *
 * @author kpati
 */
public class DevicePresetSaxJsonParser extends SaxJsonParser {
    
    private static DevicePresetSaxJsonParser instance = null;

    /**
     * Private Singleton class
     */
    private DevicePresetSaxJsonParser() {
    }

    /**
     * Returns an instance of the class
     *
     * @return DevicePresetSaxJsonParser object
     */
    public static DevicePresetSaxJsonParser getInstance() {
        if (instance == null) {
            instance = new DevicePresetSaxJsonParser();
        }
        return instance;
    }
    
      /**
     * Converts a json to a List Object of AlarmsVO Objects
     *
     * @param content, String Object, json
     * @return List Object of DevicePresetVO Objects
     */
    public List<DevicePresetVO> populateSiteDevicePresetsFromJson(String content) {
        List<DevicePresetVO> list = new ArrayList();
        String keyName;
        DevicePresetVO devicePresetVO;
        boolean inArray;

        try {
            parser = Json.createParser(new StringReader(content));
            keyName = null;
            devicePresetVO = null;
            inArray = false;

            while (parser.hasNext()) {
                switch ((event = parser.next())) {
                    case START_ARRAY:
                        inArray = true;
                        break;
                    case END_ARRAY:
                        inArray = false;
                        keyName = null;
                        devicePresetVO = null;
                        break;
                    case START_OBJECT:
                        devicePresetVO = inArray ? new DevicePresetVO() : null;
                        break;
                    case END_OBJECT:
                        if (devicePresetVO != null && inArray) {
                            list.add(devicePresetVO);
                        }
                        devicePresetVO = null;
                        keyName = null;
                        break;
                    case KEY_NAME:
                        switch (parser.getString()) {
                            case "customerAccount":
                                keyName = "customerAccount";
                                break;
                            case "name":
                                keyName = "name";
                                break;
                            case "deviceType":
                                keyName = "deviceType";
                                break;
                            case "presetId":
                                keyName = "presetId";
                                break;
                            case "channelType":
                                keyName = "channelType";
                                break;
                            case "channelNumber":
                                keyName = "channelNumber";
                                break;
                            case "siteId":
                                keyName = "siteId";
                                break;
                            case "apSetId":
                                keyName = "apSetId";
                                break;
                            case "alSetId":
                                keyName = "alSetId";
                                break;
                            case "alarmEnabled":
                                keyName = "alarmEnabled";
                                break;
                            case "disabled":
                                keyName = "disabled";
                                break;
                            case "sampleInterval":
                                keyName = "sampleInterval";
                                break;
                            case "sensorOffset":
                                keyName = "sensorOffset";
                                break;
                            case "sensorSensitivity":
                                keyName = "sensorSensitivity";
                                break;
                            case "sensorType":
                                keyName = "sensorType";
                                break;
                            case "sensorUnits":
                                keyName = "sensorUnits";
                                break;
                             case "apSetName":
                                keyName = "apSetName";
                                break;
                            case "alSetName":
                                keyName = "alSetName";
                                break;
                            case "sensorSubType":
                                keyName ="sensorSubType";
                                break;
                            case "sensorLocalOrientation":
                                keyName ="sensorLocalOrientation";
                                break;
                            default:
                                keyName = null;
                                break;
                        }
                        break;
                    case VALUE_STRING:
                        if (devicePresetVO != null && inArray && keyName != null) {
                            switch (keyName) {
                                case "siteId":
                                    try {
                                    devicePresetVO.setSiteId(UUID.fromString(parser.getString()));
                                } catch (Exception ex) {
                                }
                                break;
                                case "apSetId":
                                    try {
                                    devicePresetVO.setApSetId(UUID.fromString(parser.getString()));
                                } catch (Exception ex) {
                                }
                                break;
                                case "alSetId":
                                    try {
                                    devicePresetVO.setAlSetId(UUID.fromString(parser.getString()));
                                } catch (Exception ex) {
                                }
                                break;
                                case "presetId":
                                    try {
                                    devicePresetVO.setPresetId(UUID.fromString(parser.getString()));
                                } catch (Exception ex) {
                                }
                                break;
                                case "customerAccount":
                                    devicePresetVO.setCustomerAccount(parser.getString());
                                    break;
                                case "name":
                                    devicePresetVO.setName(parser.getString());
                                    break;
                                case "channelType":
                                    devicePresetVO.setChannelType(parser.getString());
                                    break;
                                case "deviceType":
                                    devicePresetVO.setDeviceType(parser.getString());
                                    break;
                                case "sensorType":
                                    devicePresetVO.setSensorType(parser.getString());
                                    break;
                                case "sensorUnits":
                                    devicePresetVO.setSensorUnits(parser.getString());
                                    break;
                                case "apSetName":
                                    devicePresetVO.setApSetName(parser.getString());
                                    break;
                                case "alSetName":
                                    devicePresetVO.setAlSetName(parser.getString());
                                    break;
                                case "sensorSubType":
                                    devicePresetVO.setSensorSubType(parser.getString());
                                    break;
                                case "sensorLocalOrientation":
                                    devicePresetVO.setSensorLocalOrientation(parser.getString());
                                    break;
                            }
                        }
                        break;
                    case VALUE_NUMBER:
                        if (devicePresetVO != null && inArray && keyName != null) {
                            switch (keyName) {
                                case "channelNumber":
                                    devicePresetVO.setChannelNumber(parser.getBigDecimal().byteValue());
                                    break;
                                case "sampleInterval":
                                    devicePresetVO.setSampleInterval(parser.getInt());
                                    break;
                                case "sensorOffset":
                                    devicePresetVO.setSensorOffset(parser.getBigDecimal().floatValue());
                                    break;
                                case "sensorSensitivity":
                                    devicePresetVO.setSensorSensitivity(parser.getBigDecimal().floatValue());
                                    break;
                            }
                        }
                        break;
                    case VALUE_TRUE:
                        if (devicePresetVO != null && inArray && keyName != null) {
                            switch (keyName) {
                                case "alarmEnabled":
                                    devicePresetVO.setAlarmEnabled(true);
                                    break;
                               case "disabled":
                                    devicePresetVO.setDisabled(true);
                                    break;
                            }
                        } 
                        break;
                    case VALUE_FALSE:
                        if (devicePresetVO != null && inArray && keyName != null) {
                            switch (keyName) {
                                case "alarmEnabled":
                                    devicePresetVO.setAlarmEnabled(false);
                                    break;
                               case "disabled":
                                    devicePresetVO.setDisabled(false);
                                    break;
                            }
                        } 
                        break;
                }
            }
        } catch (Exception e) {
            Logger.getLogger(AlarmsSaxJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }
    
    
}
