/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.http.utils.json.dom;

import com.uptime.client.utils.DomJsonParser;
import com.uptime.client.utils.JsonConverterUtil;
import com.uptime.upcastcm.utils.vo.AreaVO;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author gsingh
 */
public class AreaDomJsonParser extends DomJsonParser {
    private static AreaDomJsonParser instance = null;
    
    /**
     * Private Singleton class
     */
    private AreaDomJsonParser() {
    }

    /**
     * Returns an instance of the class
     * @return AreaDomJsonParser object
     */
    public static AreaDomJsonParser getInstance() {
        if (instance == null) {
            instance = new AreaDomJsonParser();
        }
        return instance; 
    }

    /**
     * Convert a AreaVO Object to a String Object json
     * @param areaVO, AreaVO Object
     * @return String Object
     */
    public String getJsonFromArea(AreaVO areaVO) {
        try {
            if (areaVO != null) {
                return JsonConverterUtil.toJSON(areaVO);
            }
        } catch (Exception e) {
            Logger.getLogger(AreaDomJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
        }
        return null;
    }

    /**
     * Convert a List of AreaVO Object to a String Object json
     * 
     * @param areaVOList, List Object of AreaVO Objects
     * @return String Object
     */
    public String getJsonFromAreaList(List<AreaVO> areaVOList) {
        try {
            if (areaVOList != null && !areaVOList.isEmpty()) {
                return JsonConverterUtil.toJSON(areaVOList);
            }
        } catch (Exception e) {
            Logger.getLogger(AreaDomJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
        }
        return null;
    }
}
