/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.http.utils.json.dom;

import com.uptime.client.utils.DomJsonParser;
import com.uptime.client.utils.JsonConverterUtil;
import com.uptime.upcastcm.utils.vo.PointLocationNamesVO;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Mohd Juned Alam
 */
public class PointLocationNamesDomJsonParser extends DomJsonParser {
    private static PointLocationNamesDomJsonParser instance = null;

    /**
     * Private Singleton class
     */
    private PointLocationNamesDomJsonParser() {
    }

    /**
     * Returns an instance of the class
     *
     * @return PointLocationNamesDomJsonParser object
     */
    public static PointLocationNamesDomJsonParser getInstance() {
        if (instance == null) {
            instance = new PointLocationNamesDomJsonParser();
        }
        return instance;
    }

    /**
     * Convert a PointLocationNamesVO Object to a String Object json
     *
     * @param pointLocationNamesVO, PointLocationNamesVO Object
     * @return String Object
     */
    public String getJsonFrompointLocationNames(PointLocationNamesVO pointLocationNamesVO) {
        try {
            if (pointLocationNamesVO != null) {
                return JsonConverterUtil.toJSON(pointLocationNamesVO);
            }
        } catch (Exception e) {
            Logger.getLogger(PointLocationNamesDomJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
        }
        return null;
    }

    /**
     * Convert a list of PointLocationNamesVO Objects to a String Object json
     *
     * @param pointLocationNamesVOList, List of PointLocationNamesVOs Object
     * @return String Object
     */
    public String getJsonFromPointLocationNameList(List<PointLocationNamesVO> pointLocationNamesVOList) {
        StringBuilder json = new StringBuilder();
        try {
            if (pointLocationNamesVOList != null && !pointLocationNamesVOList.isEmpty()) {
                return JsonConverterUtil.toJSON(pointLocationNamesVOList);
            }
        } catch (Exception e) {
            Logger.getLogger(PointLocationNamesDomJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
        }
        return json.toString();
    }
}
