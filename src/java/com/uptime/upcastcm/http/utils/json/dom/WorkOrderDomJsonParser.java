/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.http.utils.json.dom;

import com.uptime.client.utils.DomJsonParser;
import com.uptime.client.utils.JsonConverterUtil;
import com.uptime.upcastcm.utils.vo.WorkOrderVO;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author twilcox
 */
public class WorkOrderDomJsonParser extends DomJsonParser {
    private static WorkOrderDomJsonParser instance = null;
    
    /**
     * Private Singleton class
     */
    private WorkOrderDomJsonParser() {
    }

    /**
     * Returns an instance of the class
     * 
     * @return WorkOrderDomJsonParser object
     */
    public static WorkOrderDomJsonParser getInstance() {
        if (instance == null) {
            instance = new WorkOrderDomJsonParser();
        }
        return instance; 
    }

    /**
     * Convert a WorkOrderVO Object to a String Object json
     * 
     * @param workOrderVO, WorkOrderVO Object
     * @return String Object
     */
    public String getJsonFromWorkOrder(WorkOrderVO workOrderVO) {
        try {
            if (workOrderVO != null) {
                return JsonConverterUtil.toJSON(workOrderVO);
            }
        } catch (Exception e) {
            Logger.getLogger(WorkOrderDomJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
        }
        return null;
    }
}
