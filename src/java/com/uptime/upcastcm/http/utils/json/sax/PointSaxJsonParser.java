/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.http.utils.json.sax;

import com.uptime.client.utils.SaxJsonParser;
import com.uptime.upcastcm.utils.vo.PointVO;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;

/**
 *
 * @author twilcox
 */
public class PointSaxJsonParser extends SaxJsonParser {
    private static PointSaxJsonParser instance = null;
    
    /**
     * Private Singleton class
     */
    private PointSaxJsonParser() {
    }

    /**
     * Returns an instance of the class
     * @return PointSaxJsonParser object
     */
    public static PointSaxJsonParser getInstance() {
        if (instance == null) {
            instance = new PointSaxJsonParser();
        }
        return instance; 
    }

    /**
     * Converts a json to a List Object of PointVO Objects
     * @param content, String Object, json 
     * @return List Object of PointVO Objects
     */
    public List<PointVO> populatePointsFromJson(String content) {
        List<PointVO> list = new ArrayList();
        String keyName;
        PointVO pointVO;
        boolean inArray;
         
        try {
            parser = Json.createParser(new StringReader(content));
            keyName = null;
            pointVO = null;
            inArray = false;
            
            while (parser.hasNext()) {
                switch((event = parser.next())) {
                    case START_ARRAY:
                        inArray = true;
                        break;
                    case END_ARRAY:
                        inArray = false;
                        keyName = null;
                        pointVO = null;
                        break;
                    case START_OBJECT:
                        pointVO = inArray ? new PointVO() : null;
                        break;
                    case END_OBJECT:
                        if(pointVO != null && inArray)
                            list.add(pointVO);
                        pointVO = null;
                        keyName = null;
                        break;
                    case KEY_NAME:
                            switch(parser.getString()) {
                                case "customerAccount":
                                    keyName = "customerAccount";
                                    break;
                                case "siteId":
                                    keyName = "siteId";
                                    break;
                                case "areaId":
                                    keyName = "areaId";
                                    break;
                                case "assetId":
                                    keyName = "assetId";
                                    break;
                                case "pointLocationId":
                                    keyName = "pointLocationId";
                                    break;
                                case "pointId":
                                    keyName = "pointId";
                                    break;
                                case "apSetId":
                                    keyName = "apSetId";
                                    break;
                                case "alSetId":
                                    keyName = "alSetId";
                                    break;
                                case "pointName":
                                    keyName = "pointName";
                                    break;
                                case "derivedTach":
                                    keyName ="derivedTach";
                                    break;
                                case "demodEnabled":
                                    keyName = "demodEnabled";
                                    break;
                                case "demodHighPass":
                                    keyName = "demodHighPass";
                                    break;
                                case "demodInterval":
                                    keyName = "demodInterval";
                                    break;
                                case "demodLowPass":
                                    keyName = "demodLowPass";
                                    break;
                                case "disabled":
                                    keyName = "disabled";
                                    break;
                                case "pointType":
                                    keyName = "pointType";
                                    break;
                                case "sensorChannelNum":
                                    keyName = "sensorChannelNum";
                                    break;
                                case "sensorOffset":
                                    keyName = "sensorOffset";
                                    break;
                                case "sensorSensitivity":
                                    keyName = "sensorSensitivity";
                                    break;
                                case "sensorType":
                                    keyName = "sensorType";
                                    break;
                                case "sensorUnits":
                                    keyName = "sensorUnits";
                                    break;
                                case "freqUnits":
                                    keyName = "freqUnits";
                                    break;
                                case "highAlert":
                                    keyName = "highAlert";
                                    break;
                                case "highAlertActive":
                                    keyName = "highAlertActive";
                                    break;
                                case "highFault":
                                    keyName = "highFault";
                                    break;
                                case "highFaultActive":
                                    keyName = "highFaultActive";
                                    break;
                                case "customized":
                                    keyName = "customized";
                                    break;
                                case "lowAlert":
                                    keyName = "lowAlert";
                                    break;
                                case "lowAlertActive":
                                    keyName = "lowAlertActive";
                                    break;
                                case "lowFault":
                                    keyName = "lowFault";
                                    break;
                                case "lowFaultActive":
                                    keyName = "lowFaultActive";
                                    break;
                                case "maxFreq":
                                    keyName = "maxFreq";
                                    break;
                                case "minFreq":
                                    keyName = "minFreq";
                                    break;
                                case "paramAmpFactor":
                                    keyName = "paramAmpFactor";
                                    break;
                                case "paramUnits":
                                    keyName = "paramUnits";
                                    break;
                                case "period":
                                    keyName = "period";
                                    break;
                                case "resolution":
                                    keyName = "resolution";
                                    break;
                                case "sampleRate":
                                    keyName = "sampleRate";
                                    break;    
                                case "paramName":
                                    keyName = "paramName";
                                    break;
                                case "paramType":
                                    keyName = "paramType";
                                    break;
                                 case "alarmEnabled":
                                    keyName = "alarmEnabled";
                                    break; 
                                 case "dwell":
                                    keyName = "dwell";
                                    break;
                                 case "fMax":
                                    keyName = "fMax";
                                    break;
                                default:
                                    keyName = null;
                                    break;      
                            }
                        break;
                    case VALUE_STRING:
                        if(pointVO != null && inArray && keyName != null) {
                            switch(keyName) {
                                case "customerAccount":
                                    pointVO.setCustomerAccount(parser.getString());
                                    break;
                                case "siteId":
                                    pointVO.setSiteId(UUID.fromString(parser.getString()));
                                    break;
                                case "areaId":
                                    pointVO.setAreaId(UUID.fromString(parser.getString()));
                                    break;
                                case "assetId":
                                    pointVO.setAssetId(UUID.fromString(parser.getString()));
                                    break;
                                case "pointLocationId":
                                    pointVO.setPointLocationId(UUID.fromString(parser.getString()));
                                    break;
                                case "pointId":
                                    pointVO.setPointId(UUID.fromString(parser.getString()));
                                    break;
                                case "apSetId":
                                    pointVO.setApSetId(UUID.fromString(parser.getString()));
                                    break;
                                case "alSetId":
                                    pointVO.setAlSetId(UUID.fromString(parser.getString()));
                                    break;
                                case "pointName":
                                    pointVO.setPointName(parser.getString());
                                    break;
                                case "pointType":
                                    pointVO.setPointType(parser.getString());
                                    break;
                                case "sensorType":
                                    pointVO.setSensorType(parser.getString());
                                    break;
                                case "sensorUnits":
                                    pointVO.setSensorUnits(parser.getString());
                                    break;
                                case "freqUnits":
                                    pointVO.setFreqUnits(parser.getString());
                                    break;
                                case "paramAmpFactor":
                                    pointVO.setParamAmpFactor(parser.getString());
                                    break;
                                case "paramUnits":
                                    pointVO.setParamUnits(parser.getString());
                                    break; 
                                case "paramName":
                                    pointVO.setParamName(parser.getString());
                                    break; 
                                case "paramType":
                                    pointVO.setParamType(parser.getString());
                                    break; 
                            }
                        }
                        break;
                    case VALUE_NUMBER:
                        if(pointVO != null && inArray && keyName != null) {
                            switch(keyName) {
                                case "sensorOffset":
                                    pointVO.setSensorOffset(parser.getBigDecimal().floatValue());
                                    break;
                                case "sensorSensitivity":
                                    pointVO.setSensorSensitivity(parser.getBigDecimal().floatValue());
                                    break;
                                case "sensorChannelNum":
                                    pointVO.setSensorChannelNum(parser.getInt());
                                    break;
                                case "highAlert":
                                    pointVO.setHighAlert(parser.getBigDecimal().floatValue());
                                    break;
                                case "highFault":
                                    pointVO.setHighFault(parser.getBigDecimal().floatValue());
                                    break;
                                case "lowAlert":
                                    pointVO.setLowAlert(parser.getBigDecimal().floatValue());
                                    break;
                                case "lowFault":
                                    pointVO.setLowFault(parser.getBigDecimal().floatValue());
                                    break; 
                                case "maxFreq":
                                    pointVO.setMaxFreq(parser.getBigDecimal().floatValue());
                                    break;
                                case "minFreq":
                                    pointVO.setMinFreq(parser.getBigDecimal().floatValue());
                                    break;
                                case "period":
                                    pointVO.setPeriod(parser.getBigDecimal().floatValue());
                                    break;
                                case "sampleRate":
                                    pointVO.setSampleRate(parser.getBigDecimal().floatValue());
                                    break;
                                case "resolution":
                                    pointVO.setResolution(parser.getInt());
                                    break; 
                                case "dwell":
                                    pointVO.setDwell(parser.getInt());
                                    break;
                                case "fMax":
                                    pointVO.setfMax(parser.getInt());
                                    break;
                            }
                        }
                        break;
                    case VALUE_TRUE:
                        if(pointVO != null && inArray && keyName != null) {
                            switch(keyName) {
                                case "disabled":
                                    pointVO.setDisabled(true);
                                    break;
                                case "highAlertActive":
                                    pointVO.setHighAlertActive(true);
                                    break;
                                case "highFaultActive":
                                    pointVO.setHighFaultActive(true);
                                    break;
                                case "customized":
                                    pointVO.setCustomized(true);
                                    break;
                                case "lowAlertActive":
                                    pointVO.setLowAlertActive(true);
                                    break;
                                case "lowFaultActive":
                                    pointVO.setLowFaultActive(true);
                                    break;
                                case "alarmEnabled":
                                    pointVO.setAlarmEnabled(Boolean.TRUE);
                                    break;
                            }
                        }
                        break;
                    case VALUE_FALSE:
                        if(pointVO != null && inArray && keyName != null) {
                            switch(keyName) {
                                case "disabled":
                                    pointVO.setDisabled(false);
                                    break;
                                case "highAlertActive":
                                    pointVO.setHighAlertActive(false);
                                    break;
                                case "highFaultActive":
                                    pointVO.setHighFaultActive(false);
                                    break;
                                case "customized":
                                    pointVO.setCustomized(false);
                                    break;
                                case "lowAlertActive":
                                    pointVO.setLowAlertActive(false);
                                    break;
                                case "lowFaultActive":
                                    pointVO.setLowFaultActive(false);
                                    break;
                                case "alarmEnabled":
                                    pointVO.setAlarmEnabled(Boolean.FALSE);
                                    break;
                            }
                        }
                        break;
                }
            }
        } catch (Exception e) {
            Logger.getLogger(PointSaxJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
            list = new ArrayList();
        }
        return list;
    }
}
