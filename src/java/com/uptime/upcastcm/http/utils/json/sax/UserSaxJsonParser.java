/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.http.utils.json.sax;

import com.uptime.client.utils.SaxJsonParser;
import com.uptime.upcastcm.utils.vo.AlarmNotificationVO;
import com.uptime.upcastcm.utils.vo.LdapUserVO;
import com.uptime.upcastcm.utils.vo.SiteUsersVO;
import com.uptime.upcastcm.utils.vo.UserPreferencesVO;
import com.uptime.upcastcm.utils.vo.UserSitesVO;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;

/**
 *
 * @author madhavi
 */
public class UserSaxJsonParser extends SaxJsonParser {

    private static UserSaxJsonParser instance = null;

    /**
     * Private Singleton class
     */
    private UserSaxJsonParser() {

    }

    /**
     * Returns an instance of the class
     *
     * @return UserSaxJsonParser object
     */
    public static UserSaxJsonParser getInstance() {
        if (instance == null) {
            instance = new UserSaxJsonParser();
        }
        return instance;
    }

    /**
     * Converts a json to an instance UserPreferencesVO Object
     *
     * @param content, String Object, json
     * @return instance of UserPreferencesVO Object
     */
    public UserPreferencesVO populateUserPreferenceFromJson(String content) {
        String keyName;
        UserPreferencesVO userPreferencesVO;
        boolean inArray;

        try {
            parser = Json.createParser(new StringReader(content));
            keyName = null;
            userPreferencesVO = null;
            inArray = false;

            while (parser.hasNext()) {
                switch ((event = parser.next())) {
                    case START_ARRAY:
                        inArray = true;
                        break;
                    case END_ARRAY:
                        inArray = false;
                        keyName = null;
                        break;
                    case START_OBJECT:
                        userPreferencesVO = inArray ? new UserPreferencesVO() : null;
                        break;
                    case END_OBJECT:
                        keyName = null;
                        break;
                    case KEY_NAME:
                        switch (parser.getString()) {
                            case "userId":
                                keyName = "userId";
                                break;
                            case "customerAccount":
                                keyName = "customerAccount";
                                break;
                            case "language":
                                keyName = "language";
                                break;
                            case "timezone":
                                keyName = "timezone";
                                break;
                            case "defaultSite":
                                keyName = "defaultSite";
                                break;
                            case "defaultSiteName":
                                keyName = "defaultSiteName";
                                break;
                            case "defaultUnits":
                                keyName = "defaultUnits";
                                break;
                        }
                        break;
                    case VALUE_STRING:
                        if (userPreferencesVO != null && inArray && keyName != null) {
                            switch (keyName) {
                                case "userId":
                                    userPreferencesVO.setUserId(parser.getString());
                                    break;
                                case "customerAccount":
                                    userPreferencesVO.setCustomerAccount(parser.getString());
                                    break;
                                case "language":
                                    userPreferencesVO.setLocaleDisplayName(parser.getString());
                                    userPreferencesVO.setLanguage(parser.getString());
                                    break;
                                case "timezone":
                                    userPreferencesVO.setTimezone(parser.getString());
                                    break;
                                case "defaultSite":
                                    userPreferencesVO.setDefaultSite(UUID.fromString(parser.getString()));
                                    break;
                                case "defaultSiteName":
                                    userPreferencesVO.setDefaultSiteName(parser.getString());
                                    break;
                                case "defaultUnits":
                                    userPreferencesVO.setDefaultUnits(parser.getString());
                                    break;
                            }
                        }
                        break;
                }
            }
        } catch (Exception e) {
            userPreferencesVO = null;
            Logger.getLogger(UserSaxJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
        }
        return userPreferencesVO;
    }

    /**
     * Converts a json to an instance UserPreferencesVO Object
     *
     * @param content, String Object, json
     * @return instance of UserPreferencesVO Object
     */
    public List<UserSitesVO> populateUserSitesFromJson(String content) {
        List<UserSitesVO> list = new ArrayList();
        String keyName;
        UserSitesVO userSitesVO;
        boolean inArray;

        try {
            parser = Json.createParser(new StringReader(content));
            keyName = null;
            userSitesVO = null;
            inArray = false;

            while (parser.hasNext()) {
                switch ((event = parser.next())) {
                    case START_ARRAY:
                        inArray = true;
                        break;
                    case END_ARRAY:
                        inArray = false;
                        keyName = null;
                        userSitesVO = null;
                        break;
                    case START_OBJECT:
                        userSitesVO = inArray ? new UserSitesVO() : null;
                        break;
                    case END_OBJECT:
                        if (userSitesVO != null && inArray) {
                            list.add(userSitesVO);
                        }
                        userSitesVO = null;
                        keyName = null;
                        break;
                    case KEY_NAME:
                        switch (parser.getString()) {
                            case "userId":
                                keyName = "userId";
                                break;
                            case "customerAccount":
                                keyName = "customerAccount";
                                break;
                            case "siteName":
                                keyName = "siteName";
                                break;
                            case "siteRole":
                                keyName = "siteRole";
                                break;
                            case "siteId":
                                keyName = "siteId";
                                break;
                        }
                        break;
                    case VALUE_STRING:
                        if (userSitesVO != null && inArray && keyName != null) {
                            switch (keyName) {
                                case "userId":
                                    userSitesVO.setUserId(parser.getString());
                                    break;
                                case "customerAccount":
                                    userSitesVO.setCustomerAccount(parser.getString());
                                    break;
                                case "siteName":
                                    userSitesVO.setSiteName(parser.getString());
                                    break;
                                case "siteRole":
                                    userSitesVO.setSiteRole(parser.getString());
                                    break;
                                case "siteId":
                                    userSitesVO.setSiteId(UUID.fromString(parser.getString()));
                                    break;
                            }
                        }
                        break;
                }
            }
        } catch (Exception e) {
            list = new ArrayList();
            Logger.getLogger(UserSaxJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
        }
        return list;
    }

    /**
     * Converts a json to an instance UserPreferencesVO Object
     *
     * @param content, String Object, json
     * @return list of SiteUsersVO Object
     */
    public List<SiteUsersVO> populateSiteUsersFromJson(String content) {
        List<SiteUsersVO> list = new ArrayList();
        String keyName;
        SiteUsersVO siteUsersVO;
        boolean inArray;

        try {
            parser = Json.createParser(new StringReader(content));
            keyName = null;
            siteUsersVO = null;
            inArray = false;

            while (parser.hasNext()) {
                switch ((event = parser.next())) {
                    case START_ARRAY:
                        inArray = true;
                        break;
                    case END_ARRAY:
                        inArray = false;
                        keyName = null;
                        siteUsersVO = null;
                        break;
                    case START_OBJECT:
                        siteUsersVO = inArray ? new SiteUsersVO() : null;
                        break;
                    case END_OBJECT:
                        if (siteUsersVO != null && inArray) {
                            list.add(siteUsersVO);
                        }
                        siteUsersVO = null;
                        keyName = null;
                        break;
                    case KEY_NAME:
                        switch (parser.getString()) {
                            case "userId":
                                keyName = "userId";
                                break;
                            case "customerAccount":
                                keyName = "customerAccount";
                                break;
                            case "firstName":
                                keyName = "firstName";
                                break;
                            case "lastName":
                                keyName = "lastName";
                                break;
                            case "siteRole":
                                keyName = "siteRole";
                                break;
                            case "siteName":
                                keyName = "siteName";
                                break;
                            case "siteId":
                                keyName = "siteId";
                                break;
                        }
                        break;
                    case VALUE_STRING:
                        if (siteUsersVO != null && inArray && keyName != null) {
                            switch (keyName) {
                                case "userId":
                                    siteUsersVO.setUserId(parser.getString());
                                    break;
                                case "customerAccount":
                                    siteUsersVO.setCustomerAccount(parser.getString());
                                    break;
                                case "lastName":
                                    siteUsersVO.setLastName(parser.getString());
                                    break;
                                case "firstName":
                                    siteUsersVO.setFirstName(parser.getString());
                                    break;
                                case "siteRole":
                                    siteUsersVO.setSiteRole(parser.getString());
                                    break;
                                case "siteId":
                                    siteUsersVO.setSiteId(UUID.fromString(parser.getString()));
                                    break;
                            }
                        }
                        break;
                }
            }
        } catch (Exception e) {
            list = new ArrayList();
            Logger.getLogger(UserSaxJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
        }
        return list;
    }

    /**
     * Converts a json to an instance UserPreferencesVO Object
     *
     * @param content, String Object, json
     * @return ldapUserVO Object
     */
    public LdapUserVO populateSiteUsersFromLdapJson(String content) {
        String keyName;
        LdapUserVO ldapUserVO = null;
        boolean inInnerArray;

        try {
            parser = Json.createParser(new StringReader(content));
            keyName = null;

            while (parser.hasNext()) {
                switch ((event = parser.next())) {
                    case START_OBJECT:
                        ldapUserVO = new LdapUserVO();
                        break;
                    case END_OBJECT:
                        keyName = null;
                        break;
                    case KEY_NAME:
                        switch (parser.getString()) {
                            case "userId":
                                keyName = "userId";
                                break;
                            case "uid":
                                keyName = "uid";
                                break;
                            case "cn":
                                keyName = "cn";
                                break;
                            case "mail":
                                keyName = "mail";
                                break;
                            case "firstName":
                                keyName = "firstName";
                                break;
                            case "lastName":
                                keyName = "lastName";
                                break;
                            case "company":
                                keyName = "company";
                                break;
                            case "userPassword":
                                keyName = "userPassword";
                                break;
                            case "givenName":
                                keyName = "givenName";
                                break;
                            case "sn":
                                keyName = "sn";
                                break;
                            case "groups":
                                inInnerArray = false;
                                do {
                                    if (parser.hasNext()) {
                                        switch((event = parser.next())) {
                                            case START_ARRAY:
                                                inInnerArray = true;
                                                break;
                                            case END_ARRAY:
                                                inInnerArray = false;
                                                break;
                                            case VALUE_STRING:
                                                if(inInnerArray && ldapUserVO != null) {
                                                    if (ldapUserVO.getGroups() == null) {
                                                        ldapUserVO.setGroups(new ArrayList());
                                                    }
                                                    ldapUserVO.getGroups().add(parser.getString());
                                                }
                                                break;
                                        }
                                    }
                                } while (inInnerArray == true);
                                keyName = null;
                                break;
                        }
                        break;
                    case VALUE_STRING:
                        if (ldapUserVO != null && keyName != null) {
                            switch (keyName) {
                                case "userId":
                                    ldapUserVO.setUserId(parser.getString());
                                    break;
                                case "uid":
                                    ldapUserVO.setUserId(parser.getString());
                                    break;
                                case "mail":
                                    ldapUserVO.setMail(parser.getString());
                                    break;
                                case "lastName":
                                    ldapUserVO.setLastName(parser.getString());
                                    break;
                                case "sn":
                                    ldapUserVO.setLastName(parser.getString());
                                    break;
                                case "firstName":
                                    ldapUserVO.setFirstName(parser.getString());
                                    break;
                                case "givenName":
                                    ldapUserVO.setFirstName(parser.getString());
                                    break;
                                case "cn":
                                    ldapUserVO.setFullName(parser.getString());
                                    break;
                                case "company":
                                    ldapUserVO.setCompany(parser.getString());
                                    break;
                                case "userPassword":
                                    ldapUserVO.setUserPassword(parser.getString());
                                    break;
                                
                            }
                        }
                        break;
                }
            }
        } catch (Exception e) {
            Logger.getLogger(UserSaxJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
        }
        return ldapUserVO;
    }

    /**
     * Converts a json to an instance AlarmNotificationVO Object
     *
     * @param content, String Object, json
     * @return instance of AlarmNotificationVO Object
     */
    public List<AlarmNotificationVO> populateAlarmNotificationFromJson(String content) {
        List<AlarmNotificationVO> list = new ArrayList();
        String keyName;
        AlarmNotificationVO alarmNotificationVO;
        boolean inArray;

        try {
            parser = Json.createParser(new StringReader(content));
            keyName = null;
            alarmNotificationVO = null;
            inArray = false;

            while (parser.hasNext()) {
                switch ((event = parser.next())) {
                    case START_ARRAY:
                        inArray = true;
                        break;
                    case END_ARRAY:
                        inArray = false;
                        keyName = null;
                        alarmNotificationVO = null;
                        break;
                    case START_OBJECT:
                        alarmNotificationVO = inArray ? new AlarmNotificationVO() : null;
                        break;
                    case END_OBJECT:
                        if (alarmNotificationVO != null && inArray) {
                            list.add(alarmNotificationVO);
                        }
                        alarmNotificationVO = null;
                        keyName = null;
                        break;
                    case KEY_NAME:
                        switch (parser.getString()) {
                            case "userId":
                                keyName = "userId";
                                break;
                            case "siteId":
                                keyName = "siteId";
                                break;
                            case "emailAddress":
                                keyName = "emailAddress";
                                break;
                            case "assetId":
                                keyName = "assetId";
                                break;
                            case "alert":
                                keyName = "alert";
                                break;
                            case "fault":
                                keyName = "fault";
                                break;
                        }
                        break;
                    case VALUE_STRING:
                        if (alarmNotificationVO != null && inArray && keyName != null) {
                            switch (keyName) {
                                case "userId":
                                    alarmNotificationVO.setUserId(parser.getString());
                                    break;
                                case "emailAddress":
                                    alarmNotificationVO.setEmailAddress(parser.getString());
                                    break;

                                case "assetId":
                                    alarmNotificationVO.setAssetId(UUID.fromString(parser.getString()));
                                    break;
                                case "siteId":
                                    alarmNotificationVO.setSiteId(UUID.fromString(parser.getString()));
                                    break;
                            }
                        }
                        break;
                    case VALUE_TRUE:
                        if (alarmNotificationVO != null && inArray && keyName != null) {
                            switch (keyName) {
                                case "alert":
                                    alarmNotificationVO.setAlert(true);
                                    break;
                                case "fault":
                                    alarmNotificationVO.setFault(true);
                                    break;
                            }
                            break;
                        }
                }
            }
        } catch (Exception e) {
            list = new ArrayList();
            Logger.getLogger(UserSaxJsonParser.class.getName()).log(Level.WARNING, e.getMessage(), e);
        }
        return list;
    }

    public static void main(String[] args) {
        String content = "{\"userId\":\"Test userId 111\",\"userPreferences\":[{\"userId\":\"Test userId 111\",\"customerAccount\":\"Test customerAccount 111\",\"language\":\"Test language 111\",\"timezone\":\"Test timezone 111\",\"defaultSite\":\"f35e2a60-67e1-11ec-a5da-8fb4174fa836\"}],\"outcome\":\"GET worked successfully.\"}";
        UserPreferencesVO uvo = UserSaxJsonParser.getInstance().populateUserPreferenceFromJson(content);
        System.out.println("CustomerAccount - " + uvo.getCustomerAccount());
        System.out.println("DefaultSite - " + uvo.getDefaultSite());
        System.out.println("Language - " + uvo.getLocaleDisplayName());
        System.out.println("Timezone - " + uvo.getTimezone());
        System.out.println("UserId - " + uvo.getUserId());

        content = "{\"userId\":\"Test userId 111\",\"userSites\":[{\"userId\":\"Test userId 111\",\"customerAccount\":\"Test customerAccount 111\",\"siteId\":\"f35e2a60-67e1-11ec-a5da-8fb4174fa836\",\"siteRole\":\"Test siteRole 111\"}],\"outcome\":\"GET worked successfully.\"}";
        List<UserSitesVO> siteList = UserSaxJsonParser.getInstance().populateUserSitesFromJson(content);
        if (siteList != null && !siteList.isEmpty()) {
            for (int x = 0; x < siteList.size(); x++) {
                UserSitesVO usvo = siteList.get(x);
                System.out.println("CustomerAccount - " + usvo.getCustomerAccount());
                System.out.println("SiteId - " + usvo.getSiteId());
                System.out.println("SiteRole - " + usvo.getSiteRole());
                System.out.println("UserId - " + usvo.getUserId());
            }
        }
    }
}
