/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.FaultFrequenciesClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import com.uptime.upcastcm.utils.helperclass.Filters;
import static com.uptime.upcastcm.utils.helperclass.GenericMessage.setNonAutoGrowlMsg;
import com.uptime.upcastcm.utils.interfaces.PresetDialogFields;
import com.uptime.upcastcm.utils.vo.FaultFrequenciesVO;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import org.primefaces.PrimeFaces;

/**
 *
 * @author twilcox
 */
public class CopyEditFaultFrequenciesBean implements Serializable, PresetDialogFields {
    private UserManageBean userManageBean;
    private FaultFrequenciesVO frequenciesVO;
    private int operationType;
    private List<FaultFrequenciesVO> frequenciesVOList, removedFrequenciesVOList;
    private boolean validationFailed = false, addToPreferred, enabled;
    private String setName, description, ffUnits;

    /**
     * Constructor
     */
    public CopyEditFaultFrequenciesBean() {
        frequenciesVO = new FaultFrequenciesVO();
        frequenciesVOList = new ArrayList();
        removedFrequenciesVOList = new ArrayList();
        validationFailed = false;
        addToPreferred = true; 
        enabled = false;

        try {
            userManageBean = ((UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean"));
        } catch (Exception e) {
            Logger.getLogger(CopyEditFaultFrequenciesBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            userManageBean = null;
        }
    }

    /**
     * Resets the values of this overlay page
     */
    @Override
    public void resetPage() {
        frequenciesVO = new FaultFrequenciesVO();
        frequenciesVOList = new ArrayList();
        removedFrequenciesVOList = new ArrayList();
        operationType = -1;
        setName = null;
        description = null;
        ffUnits = null;
        validationFailed = false;
        addToPreferred = true;
        enabled = false;

        frequenciesVO = new FaultFrequenciesVO();
        if (userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null) {
            frequenciesVO.setCustomerAccount(userManageBean.getCurrentSite().getCustomerAccount());
        }
    }

    /**
     * Presets fields based on the given info
     *
     * @param presets, Filters Object
     * @param object, Object
     */
    @Override
    public void presetFields(Filters presets, Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Presets fields based on the given info
     *
     * @param operationType, int
     * @param object, Object
     */
    @Override
    public void presetFields(int operationType, Object object) {
        if (userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null) {
            if (object != null && object instanceof FaultFrequenciesVO) {
                frequenciesVO = new FaultFrequenciesVO((FaultFrequenciesVO) object);
                frequenciesVO.setSiteId(userManageBean.getCurrentSite().getSiteId());
                frequenciesVO.setSiteName(userManageBean.getCurrentSite().getSiteName());
                
                if ((this.operationType = operationType) != 2) {
                    setName = frequenciesVO.getFfSetName();
                } else {
                    frequenciesVO.setFfSetName(null);
                    setName = null;
                }
                
                description = frequenciesVO.getFfSetDesc();
                ffUnits = frequenciesVO.getFfUnit();
                addToPreferred = true;
                frequenciesVOList.clear();

                // Site
                if (frequenciesVO != null && frequenciesVO.getSiteName() != null && frequenciesVO.getFfType().equalsIgnoreCase("Site")) {
                    frequenciesVOList.addAll(FaultFrequenciesClient.getInstance().findByCustomerSiteSetId(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), frequenciesVO.getSiteId(), frequenciesVO.getFfSetId()));
                } 

                // Global
                else if (frequenciesVO != null) {
                    if (!userManageBean.getCurrentSite().getCustomerAccount().equals(DEFAULT_UPTIME_ACCOUNT)) {
                        frequenciesVOList.addAll(FaultFrequenciesClient.getInstance().findByCustomerSetId(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), DEFAULT_UPTIME_ACCOUNT, frequenciesVO.getFfSetId()));
                    }
                    frequenciesVOList.addAll(FaultFrequenciesClient.getInstance().findByCustomerSetId(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), frequenciesVO.getFfSetId()));
                }
                
            } else {
                resetPage();
            }
            
            if (removedFrequenciesVOList != null) {
                removedFrequenciesVOList.clear();
            }
        }
    }

    public void addFaultFrequencies() {
        enabled = false;
        frequenciesVOList.add(new FaultFrequenciesVO());
    }

    /**
     * Presets fields based on the given info
     *
     * @param object, Object
     */
    @Override
    public void presetFields(Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Submit the given info to Cassandra
     */
    public void submit() {
        List<FaultFrequenciesVO> frequenciesVOUpdateList;
        List<FaultFrequenciesVO> frequenciesVOCreateList;
        UUID setId;

        if (userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null) {
            validationFailed = false;
            
            frequenciesVOList.stream().forEach(ffVO -> {
                if (this.setName == null || this.setName.trim().isEmpty() || ffVO.getFfName() == null || ffVO.getFfName().trim().isEmpty() || ffVO.getFfUnit() == null || ffVO.getFfUnit().trim().isEmpty()) {
                    validationFailed = true;
                }
            });
            
            if (validationFailed) {
                setNonAutoGrowlMsg("growl_validate_message_1");
            } else {
                frequenciesVOList.forEach(vo -> {
                    vo.setCustomerAccount(userManageBean.getCurrentSite().getCustomerAccount());
                    vo.setSiteName(userManageBean.getCurrentSite().getSiteName());
                    vo.setSiteId(userManageBean.getCurrentSite().getSiteId());
                    vo.setFfSetName(setName);
                    vo.setFfSetDesc(description);
                    vo.setFfUnit(ffUnits);
                    vo.setGlobal(false);
                });

                // For Copy
                if (this.operationType == 2) {
                    setId = UUID.randomUUID();
                    frequenciesVOList.forEach(vo -> {
                        vo.setFfSetId(setId);
                        vo.setFfId(UUID.randomUUID());
                    });

                    if (FaultFrequenciesClient.getInstance().createSiteFaultFrequencies(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), frequenciesVOList)) {
                        if (addToPreferred) {
                            FaultFrequenciesClient.getInstance().updateSiteFFSetFavoritesVOList(frequenciesVOList, new ArrayList());
                        }
                        if (!addToPreferred) {
                            setNonAutoGrowlMsg("growl_create_item_items");
                        }
                        Logger.getLogger(CopyEditFaultFrequenciesBean.class.getName()).log(Level.INFO, "Site Fault Frequencies created successfully. {0}", new Object[]{frequenciesVOList});
                    } else {
                        Logger.getLogger(CopyEditFaultFrequenciesBean.class.getName()).log(Level.INFO, "Error in creating Site Fault Frequencies. frequenciesVO.getSiteName()*********** {0}", new Object[]{frequenciesVOList.get(0).getSiteName()});
                    }
                } // For Edit
                else {
                    frequenciesVOUpdateList = new ArrayList();
                    frequenciesVOCreateList = new ArrayList();

                    frequenciesVOList.forEach(vo -> {
                        // Modified.
                        if (vo.getFfType() != null) {
                            frequenciesVOUpdateList.add(vo);
                        } // Created.
                        else {
                            vo.setFfId(UUID.randomUUID());
                            frequenciesVOCreateList.add(vo);
                        }
                    });

                    frequenciesVOCreateList.stream().forEach(vo -> vo.setFfSetId((!frequenciesVOUpdateList.isEmpty()) ? frequenciesVOUpdateList.get(0).getFfSetId() : removedFrequenciesVOList.get(0).getFfSetId()));

                    if (!frequenciesVOCreateList.isEmpty()) {
                        if (FaultFrequenciesClient.getInstance().createSiteFaultFrequencies(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), frequenciesVOCreateList)) {
                            Logger.getLogger(CopyEditFaultFrequenciesBean.class.getName()).log(Level.INFO, "Site Fault Frequencies created successfully. {0}", new Object[]{frequenciesVOCreateList});
                        } else {
                            Logger.getLogger(CopyEditFaultFrequenciesBean.class.getName()).log(Level.INFO, "Error in creating Site Fault Frequencies. frequenciesVO.getSiteName()*********** {0}", new Object[]{frequenciesVOCreateList.get(0).getSiteName()});
                        }
                    }

                    if (!frequenciesVOUpdateList.isEmpty()) {
                        if (FaultFrequenciesClient.getInstance().updateSiteFaultFrequencies(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), frequenciesVOUpdateList)) {
                            Logger.getLogger(CopyEditFaultFrequenciesBean.class.getName()).log(Level.INFO, "Site Fault Frequencies updated successfully. {0}", new Object[]{frequenciesVOUpdateList});
                        } else {
                            Logger.getLogger(CopyEditFaultFrequenciesBean.class.getName()).log(Level.INFO, "Error in updating Site Fault Frequencies. frequenciesVO.getSiteName()*********** {0}", new Object[]{frequenciesVOUpdateList.get(0).getSiteName()});
                        }
                    }

                    if (!removedFrequenciesVOList.isEmpty()) {
                        if (FaultFrequenciesClient.getInstance().deleteSiteFaultFrequencies(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), removedFrequenciesVOList)) {
                            Logger.getLogger(CopyEditFaultFrequenciesBean.class.getName()).log(Level.INFO, "Site Fault Frequencies deleted successfully. {0}", new Object[]{removedFrequenciesVOList});
                        } else {
                            Logger.getLogger(CopyEditFaultFrequenciesBean.class.getName()).log(Level.INFO, "Error in deleting Site Fault Frequencies. frequenciesVO.getSiteName()*********** {0}", new Object[]{removedFrequenciesVOList.get(0).getSiteName()});
                        }
                    }

                    removedFrequenciesVOList.clear();
                    setNonAutoGrowlMsg("growl_update_item_items");
                }
                ((PresetsBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("presetsBean")).resetFaultFrequenciesTab();
                ((CreateCustomFaultFrequecySetsBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("createCustomFaultFrequecySetsBean")).resetPage();
                PrimeFaces.current().ajax().update("presetsTabViewId:faultFrequenciesFormId");
                PrimeFaces.current().ajax().update("customFaultFrequencieySetsFormId");
                PrimeFaces.current().executeScript("PF('defaultSecondDlg').hide()");
                enabled = false;
            }
        }
    }

    public void updateBean() {
        Logger.getLogger(CopyEditFaultFrequenciesBean.class.getName()).log(Level.INFO, "CopyEditFaultFrequenciesBean.updateBean() called. ***********");
        
        enabled = true;
        if (setName == null || setName.trim().isEmpty()
                || ffUnits == null || ffUnits.trim().isEmpty()) {
            enabled = false;
            return;
        }
        if (frequenciesVOList.isEmpty()) {
            enabled = false;
        } else {
            frequenciesVOList.stream().filter(faultFrequenciesVO -> faultFrequenciesVO.getFfName() == null || faultFrequenciesVO.getFfName().trim().isEmpty() || faultFrequenciesVO.getFfValue() <= 0.0f).forEachOrdered(faultFrequenciesVO -> enabled = false);
        }
    }

    public void deleteApAlRow(FaultFrequenciesVO faultFrequenciesVO) {
        if (frequenciesVOList.size() > 1) {
            frequenciesVOList.remove(faultFrequenciesVO);
            if (faultFrequenciesVO.getFfType() != null && this.operationType == 1) {
                removedFrequenciesVOList.add(faultFrequenciesVO);
            }
        }
    }

    public FaultFrequenciesVO getFrequenciesVO() {
        return frequenciesVO;
    }

    public int getOperationType() {
        return operationType;
    }

    public void setOperationType(int operationType) {
        this.operationType = operationType;
    }

    public boolean isValidationFailed() {
        return validationFailed;
    }

    public String getSetName() {
        return setName;
    }

    public void setSetName(String setName) {
        this.setName = setName;
    }

    public String getDescription() {
        return description;
    }

    public String getFfUnits() {
        return ffUnits;
    }

    public void setFfUnits(String ffUnits) {
        this.ffUnits = ffUnits;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setFrequenciesVO(FaultFrequenciesVO frequenciesVO) {
        this.frequenciesVO = frequenciesVO;
    }

    public void setFrequenciesVOList(List<FaultFrequenciesVO> frequenciesVOList) {
        this.frequenciesVOList = frequenciesVOList;
    }

    public List<FaultFrequenciesVO> getFrequenciesVOList() {
        return frequenciesVOList;
    }

    public boolean isAddToPreferred() {
        return addToPreferred;
    }

    public void setAddToPreferred(boolean addToPreferred) {
        this.addToPreferred = addToPreferred;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
