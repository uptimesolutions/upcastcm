/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.PointClient;
import com.uptime.upcastcm.http.client.PointLocationClient;
import com.uptime.upcastcm.http.client.WorkOrderClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import static com.uptime.upcastcm.utils.enums.WorkOrderActionEnum.getActionItemList;
import static com.uptime.upcastcm.utils.enums.WorkOrderIssueEnum.getIssueItemList;
import static com.uptime.upcastcm.utils.enums.WorkOrderPriorityEnum.getPriorityItemList;
import com.uptime.upcastcm.utils.helperclass.Filters;
import com.uptime.upcastcm.utils.interfaces.PresetDialogFields;
import com.uptime.upcastcm.utils.vo.TrendDetailsVO;
import com.uptime.upcastcm.utils.vo.WorkOrderVO;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import org.primefaces.PrimeFaces;

/**
 *
 * @author twilcox
 */
public class EditWorkOrderBean implements Serializable, PresetDialogFields {
    private WorkOrderVO workOrderVO;
    private UserManageBean userManageBean;
    private NavigationBean navigationBean;
    private final List<SelectItem> assetList;
    private List<SelectItem> priorities, issueList, actionList;
    private Filters navigationFilter;

    /**
     * Constructor
     */
    public EditWorkOrderBean() {
        workOrderVO = new WorkOrderVO();
        assetList = new ArrayList(); 
        
        try {
            navigationBean = (NavigationBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("navigationBean");
            userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
            actionList = getActionItemList();
            issueList = getIssueItemList();
            priorities = getPriorityItemList();
        } catch (Exception e) {
            Logger.getLogger(EditWorkOrderBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            navigationBean = null;
            userManageBean = null;
        }
    }

    /**
     * Post Constructor
     */
    @PostConstruct
    public void init() {
        resetPage();
    }

    @Override
    public void resetPage() {
        workOrderVO = new WorkOrderVO();
        workOrderVO.setTrendDetails(new ArrayList());
        assetList.clear();
    }

    @Override
    public void presetFields(Filters presets, Object object) {
        List<WorkOrderVO> workOrderVOList;
        
        resetPage();
        
        if (userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null) {     
            if (presets != null && object != null && object instanceof WorkOrderVO) {
                navigationFilter = new Filters(presets);
                
                workOrderVO = new WorkOrderVO((WorkOrderVO)object);
                workOrderVO.setCustomerAccount(userManageBean.getCurrentSite().getCustomerAccount());
                workOrderVO.setNewPriority(workOrderVO.getPriority());
                workOrderVO.setSiteId(userManageBean.getCurrentSite().getSiteId());
                workOrderVO.setSiteName(userManageBean.getCurrentSite().getSiteName());
                workOrderVO.setTrendDetails(new ArrayList());

                if (!(workOrderVOList = WorkOrderClient.getInstance().getWorkOrderTrendsByCustomerSiteAreaAssetPriorityDate(ServiceRegistrarClient.getInstance().getServiceHostURL(WORK_ORDER_SERVICE_NAME), workOrderVO.getCustomerAccount(), workOrderVO.getSiteId(), workOrderVO.getAreaId(), workOrderVO.getAssetId(), workOrderVO.getPriority(), workOrderVO.getCreatedDate())).isEmpty()) {
                    workOrderVOList.stream().forEachOrdered(vo -> {
                        TrendDetailsVO trendDetailsVO;

                        try {
                            trendDetailsVO = new TrendDetailsVO();
                            trendDetailsVO.setApSetId(vo.getApSetId());
                            trendDetailsVO.setChannelType(vo.getChannelType());
                            trendDetailsVO.setParamName(vo.getParamName());
                            trendDetailsVO.setPointId(vo.getPointId());
                            trendDetailsVO.setPointName(PointClient.getInstance().getPointsByPK(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), workOrderVO.getCustomerAccount(), workOrderVO.getSiteId(), workOrderVO.getAreaId(), workOrderVO.getAssetId(), vo.getPointLocationId(), vo.getPointId(), vo.getApSetId()).get(0).getPointName());
                            trendDetailsVO.setPointLocationId(vo.getPointLocationId());
                            trendDetailsVO.setPointLocationName(PointLocationClient.getInstance().getPointLocationsByPK(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), workOrderVO.getCustomerAccount(), workOrderVO.getSiteId(), workOrderVO.getAreaId(), workOrderVO.getAssetId(), vo.getPointLocationId()).get(0).getPointLocationName());
                            trendDetailsVO.setRowId(vo.getRowId());
                            workOrderVO.getTrendDetails().add(trendDetailsVO);
                        } catch (Exception e) {
                            Logger.getLogger(EditWorkOrderBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
                        }
                    });
                }
            }
        }
    }

    @Override
    public void presetFields(int operationType, Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void presetFields(Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void updateBean() {
        Logger.getLogger(EditWorkOrderBean.class.getName()).log(Level.INFO, "workOrderVO.getCustomerAccount()*********** {0}", new Object[]{workOrderVO.getCustomerAccount()});
    }

    /**
     * Update the given work order
     */
    public void submit() {
        WorkOrderBean workOrderBean;
        
        if (workOrderVO != null) {
            try {
                // update Note If Priority was changed
                if (workOrderVO.getNewPriority() != workOrderVO.getPriority()) {
                    if (workOrderVO.getNotes() != null && !workOrderVO.getNotes().trim().isEmpty()) {
                        if (workOrderVO.getNotes().trim().endsWith(".")) {
                            workOrderVO.setNotes(workOrderVO.getNotes() + " Priority changed from " + workOrderVO.getPriority() + " to " + workOrderVO.getNewPriority() + ".");
                        } else {
                            workOrderVO.setNotes(workOrderVO.getNotes() + ". Priority changed from " + workOrderVO.getPriority() + " to " + workOrderVO.getNewPriority() + ".");
                        }
                    } else {
                        workOrderVO.setNotes("Priority changed from " + workOrderVO.getPriority() + " to " + workOrderVO.getNewPriority() + ".");
                    }
                }
                
                // update work order in DB
                if (WorkOrderClient.getInstance().updateWorkOrder(ServiceRegistrarClient.getInstance().getServiceHostURL(WORK_ORDER_SERVICE_NAME), workOrderVO)) {
                    if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("workOrderBean") == null) {
                        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("workOrderBean", new WorkOrderBean());
                        ((WorkOrderBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("workOrderBean")).init();
                    }
                    
                    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("workOrderBean", new WorkOrderBean());
                    workOrderBean = (WorkOrderBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("workOrderBean");
                    workOrderBean.init();
                    
                    if (navigationFilter != null) {
                        if (navigationFilter.getAsset() != null) {
                            workOrderBean.onSiteChange(false);
                            workOrderBean.getSelectedFilters().setAreaOver100(navigationFilter.getAreaOver100());
                            workOrderBean.getSelectedFilters().setArea(navigationFilter.getArea());
                            workOrderBean.onAreaChange(false);
                            workOrderBean.getSelectedFilters().setAssetOver100(navigationFilter.getAssetOver100());
                            workOrderBean.getSelectedFilters().setAsset(navigationFilter.getAsset());
                            workOrderBean.onAssetChange(true);
                        } else {
                            workOrderBean.onSiteChange(false);
                            workOrderBean.getSelectedFilters().setAreaOver100(navigationFilter.getAreaOver100());
                            workOrderBean.getSelectedFilters().setArea(navigationFilter.getArea());
                            workOrderBean.onAreaChange(true);
                        }
                    }
                    
                    PrimeFaces.current().executeScript("PF('defaultDlg').hide()");
                    PrimeFaces.current().executeScript("updateTreeNodes()");
                } else {
                    Logger.getLogger(EditWorkOrderBean.class.getName()).log(Level.INFO, "Error in updating work order. workOrderVO.toString()*********** {0}", new Object[]{workOrderVO.toString()});
                }
                
            } catch (Exception e) {
                Logger.getLogger(EditWorkOrderBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            }
        } else {
            Logger.getLogger(EditWorkOrderBean.class.getName()).log(Level.INFO, "WorkOrderVO is null");
        }
    }
    
    /**
     * Remove the given trend from the trendDetailsVO list
     * @param trendDetailsVO, TrendDetailsVO Object
     */
    public void removeTrend(TrendDetailsVO trendDetailsVO) {
        if (trendDetailsVO != null) {
            workOrderVO.getTrendDetails().remove(trendDetailsVO);
        }
    }
    
    /**
     * View the given trend
     * @param trendDetailsVO, TrendDetailsVO Object
     */
    public void viewTrendGraph(TrendDetailsVO trendDetailsVO) {
        Filters filters;
        AssetAnalysisWindowBean assetAnalysisWindowBean;
        
        try {  
            if ((assetAnalysisWindowBean = (AssetAnalysisWindowBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("assetAnalysisWindowBean")) == null) {
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("assetAnalysisWindowBean", new AssetAnalysisWindowBean());
                assetAnalysisWindowBean = (AssetAnalysisWindowBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("assetAnalysisWindowBean");
                assetAnalysisWindowBean.init();
            }
            
            filters = new Filters();
            filters.setRegion(userManageBean.getSelectedFilters().getRegion());
            filters.setCountry(userManageBean.getSelectedFilters().getCountry());
            filters.setSite(new SelectItem(workOrderVO.getSiteId(), workOrderVO.getSiteName()));
            filters.setArea(new SelectItem(workOrderVO.getAreaId(), workOrderVO.getAreaName()));
            filters.setAsset(new SelectItem(workOrderVO.getAssetId(), workOrderVO.getAssetName()));
            filters.setPointLocation(new SelectItem(trendDetailsVO.getPointLocationId(), trendDetailsVO.getPointLocationName()));
            filters.setPoint(new SelectItem(trendDetailsVO.getPointId(), trendDetailsVO.getPointName()));
            filters.setApSet(new SelectItem(trendDetailsVO.getApSetId()));
            
            // The AlSet UUID is being left null because the value needs to be looked up and it isn't used in the method loadTrendGraph.
            filters.setAlSetParam(new SelectItem(null, trendDetailsVO.getParamName()));
            
            assetAnalysisWindowBean.loadTrendGraph(filters, false);
        } catch (Exception e) {
            Logger.getLogger(EditWorkOrderBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }
    
    /**
     * Open the Add Trend Overlay
     */
    public void openAddTrendOverlay() {
        try {
            if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("addTrendBean") == null) {
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("addTrendBean", new AddTrendBean());
                ((AddTrendBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("addTrendBean")).init();
            }
            ((AddTrendBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("addTrendBean")).presetFields(workOrderVO);
            navigationBean.updateSecondDialog("add-trend");
        } catch (Exception e) {
            Logger.getLogger(EditWorkOrderBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    public WorkOrderVO getWorkOrderVO() {
        return workOrderVO;
    }

    public List<SelectItem> getPriorities() {
        return priorities;
    }

    public List<SelectItem> getIssueList() {
        return issueList;
    }

    public List<SelectItem> getActionList() {
        return actionList;
    }

    public List<SelectItem> getAssetList() {
        return assetList;
    }
}
