/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.TachometerClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import static com.uptime.upcastcm.utils.enums.TachReferenceUnitEnum.getTachReferenceUnitItemList;
import static com.uptime.upcastcm.utils.helperclass.GenericMessage.setNonAutoGrowlMsg;
import com.uptime.upcastcm.utils.vo.TachometerVO;
import java.io.Serializable;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import org.primefaces.PrimeFaces;

/**
 *
 * @author twilcox
 */
public class CreateTachometerBean implements Serializable {
    private List<SelectItem> tachReferenceUnitList;
    private TachometerVO tachometerVO;
    private UserManageBean userManageBean;
    private String siteOrGlobal;

    /**
     * Constructor
     */
    public CreateTachometerBean() {
        try {
            userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
            tachReferenceUnitList = getTachReferenceUnitItemList();
        } catch (Exception e) {
            Logger.getLogger(CreateTachometerBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            userManageBean = null;
        }
    }

    /**
     * Post Constructor
     */
    @PostConstruct
    public void init() {
        resetPage();
        if (userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null) {
            Logger.getLogger(CreateTachometerBean.class.getName()).log(Level.INFO, "userManageBean.getCustomerAccount() *********** {0}", userManageBean.getCurrentSite().getCustomerAccount());
        }
    }

    /**
     * Resets the values of this overlay page
     */
    public void resetPage() {
        siteOrGlobal = null;
        tachometerVO = new TachometerVO();
        tachometerVO.setTachType("Constant Speed");
        if (userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null) {
            tachometerVO.setCustomerAccount(userManageBean.getCurrentSite().getCustomerAccount());
        }
    }

    public void calculateReferenceRPM() {
        Double refRPM;
                
        if (tachometerVO.getFpm() > 0) {
            if (tachometerVO.getRollDiameter() > 0) {
                refRPM = (tachometerVO.getFpm() / (((tachometerVO.getRollDiameter()) * Math.PI) / 12));
                tachometerVO.setTachReferenceSpeed(refRPM.floatValue());
                PrimeFaces.current().ajax().update("createTachometersFormId");
                PrimeFaces.current().executeScript("PF('dlg3').hide()");
            } else {
                FacesContext.getCurrentInstance().addMessage("growlMessages", new FacesMessage("Roll Diameter should be greater than 0"));
            }
        } else {
            PrimeFaces.current().ajax().update("createTachometersFormId");
            PrimeFaces.current().executeScript("PF('dlg3').hide()");
        }
    }

    /**
     * Submit the given info to Cassandra
     */
    public void submit() {
        if (userManageBean != null && userManageBean.getCurrentSite() != null && tachometerVO != null
                && tachometerVO.getTachName() != null && !tachometerVO.getTachName().isEmpty()
                && tachometerVO.getTachType() != null && !tachometerVO.getTachType().isEmpty()
                && tachometerVO.getCustomerAccount() != null && !tachometerVO.getCustomerAccount().isEmpty()
                && siteOrGlobal != null) {
            tachometerVO.setTachId(UUID.randomUUID());
            
            if (siteOrGlobal.equalsIgnoreCase("site")) {
                tachometerVO.setSiteId(userManageBean.getCurrentSite().getSiteId());
                tachometerVO.setSiteName(userManageBean.getCurrentSite().getSiteName());
                if (TachometerClient.getInstance().createSiteTachometers(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), tachometerVO)) {
                    ((PresetsBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("presetsBean")).resetTachometersTab();
                    PrimeFaces.current().ajax().update("presetsTabViewId:tachometersFormId");
                    PrimeFaces.current().executeScript("PF('defaultDlg').hide()");
                    setNonAutoGrowlMsg("growl_create_item_items");
                }
            } else {
                tachometerVO.setSiteId(null);
                tachometerVO.setSiteName(null);
                if (TachometerClient.getInstance().createGlobalTachometers(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), tachometerVO)) {
                    userManageBean.getPresetUtil().clearGlobalTachList();
                    ((PresetsBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("presetsBean")).resetTachometersTab();
                    PrimeFaces.current().ajax().update("presetsTabViewId:tachometersFormId");
                    PrimeFaces.current().executeScript("PF('defaultDlg').hide()");
                    setNonAutoGrowlMsg("growl_create_item_items");
                }
            }
        }
    }

    public TachometerVO getTachometerVO() {
        return tachometerVO;
    }

    public void setTachometerVO(TachometerVO tachometerVO) {
        this.tachometerVO = tachometerVO;
    }

    public String getSiteOrGlobal() {
        return siteOrGlobal;
    }

    public void setSiteOrGlobal(String siteOrGlobal) {
        this.siteOrGlobal = siteOrGlobal;
    }

    public List<SelectItem> getTachReferenceUnitList() {
        return tachReferenceUnitList;
    }
}
