/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import static com.uptime.client.http.servlet.AbstractServiceManagerServlet.getPropertyEnvironment;
import static com.uptime.client.utils.JsonConverterUtil.DATE_FORMATTER;
import com.uptime.upcastcm.dao.PrestoDAO;
import com.uptime.upcastcm.http.client.AreaClient;
import com.uptime.upcastcm.http.client.AssetClient;
import com.uptime.upcastcm.http.client.PointClient;
import com.uptime.upcastcm.http.client.PointLocationClient;
import com.uptime.upcastcm.http.client.SiteClient;
import com.uptime.upcastcm.http.client.WorkOrderClient;
import com.uptime.upcastcm.http.utils.json.sax.AssetDeviceSaxJsonParser;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import com.uptime.upcastcm.utils.enums.WorkOrderActionEnum;
import com.uptime.upcastcm.utils.enums.WorkOrderIssueEnum;
import com.uptime.upcastcm.utils.helperclass.AbstractNavigation;
import com.uptime.upcastcm.utils.helperclass.Filters;
import com.uptime.upcastcm.utils.vo.AreaVO;
import com.uptime.upcastcm.utils.vo.AssetDeviceVO;
import com.uptime.upcastcm.utils.vo.AssetVO;
import com.uptime.upcastcm.utils.vo.PointLocationVO;
import com.uptime.upcastcm.utils.vo.PointVO;
import com.uptime.upcastcm.utils.vo.WorkOrderVO;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import org.primefaces.PrimeFaces;
import org.primefaces.event.NodeCollapseEvent;
import org.primefaces.event.NodeExpandEvent;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.TreeNode;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import java.nio.charset.StandardCharsets;
import java.time.ZoneId;
import org.primefaces.component.datatable.DataTable;

/**
 *
 * @author twilcox
 */
public class WorkOrderBean extends AbstractNavigation implements Serializable {

    private UserManageBean userManageBean;
    private final List<WorkOrderVO> areaTileList, openWorkOrdersList;
    private final Map<UUID, List<WorkOrderVO>> workOrderCountsMap;
    private final Map<WorkOrderVO, List<WorkOrderVO>> workOrderTrendsMap;
    private List<AssetDeviceVO> assetDeviceList;

    /**
     * Constructor
     */
    public WorkOrderBean() {
        super(ASSET);
        openWorkOrdersList = new ArrayList();
        workOrderTrendsMap = new HashMap();
        workOrderCountsMap = new HashMap();
        areaTileList = new ArrayList();
        assetDeviceList = null;
        
        try {
            userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
        } catch (Exception e) {
            Logger.getLogger(WorkOrderBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            userManageBean = null;
        }
    }

    /**
     * Post Constructor
     */
    @PostConstruct
    public void init() {
        resetPage();

        // Set the selectFilters
        try {
            if (userManageBean.getCurrentSite() != null) {
                selectedFilters.setRegion(userManageBean.getSelectedFilters().getRegion());
                selectedFilters.setCountry(userManageBean.getSelectedFilters().getCountry());
                selectedFilters.setSite(userManageBean.getSelectedFilters().getSite());
                onSiteChange(true);
            }
        } catch (Exception e) {
            Logger.getLogger(WorkOrderBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Reset fields values when navigates to Alarms page.
     */
    @Override
    public void resetPage() {
        setRegionCountrySiteMap();
        setSelectedFilters();
        openWorkOrdersList.clear();
        workOrderTrendsMap.clear();
        areaTileList.clear();
        assetList.clear();
        areaList.clear();
        workOrderCountsMap.clear();
        if (userManageBean != null) {
            treeContentMenuBuilder(ROOT, userManageBean.getResourceBundleString("breadcrumb_3"), null, null);
        }
        updateBreadcrumbModel();
        updateUI("content");
    }

    @Override
    public void onYearChange(boolean reloadUI) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * called when the region changed
     *
     * @param reloadUI
     */
    @Override
    public void onRegionChange(boolean reloadUI) {
        if (selectedFilters == null) {
            setSelectedFilters();
        }
        if (selectedFilters.getRegion() != null && !selectedFilters.getRegion().isEmpty()) {
            selectedFilters.clear(REGION);
            openWorkOrdersList.clear();
            workOrderTrendsMap.clear();
            areaTileList.clear();
            assetList.clear();
            areaList.clear();

            if (reloadUI) {
                updateTreeNodeRoot(REGION);
                updateBreadcrumbModel();
                updateUI("content");
            }
        } else {
            resetPage();
        }
    }

    /**
     * called when the country changed
     *
     * @param reloadUI
     */
    @Override
    public void onCountryChange(boolean reloadUI) {
        if (selectedFilters == null) {
            setSelectedFilters();
        }
        if (selectedFilters.getCountry() != null && !selectedFilters.getCountry().isEmpty()) {
            selectedFilters.clear(COUNTRY);
            openWorkOrdersList.clear();
            workOrderTrendsMap.clear();
            areaTileList.clear();
            assetList.clear();
            areaList.clear();

            if (reloadUI) {
                updateTreeNodeRoot(COUNTRY);
                updateBreadcrumbModel();
                updateUI("content");
            }
        } else {
            onRegionChange(reloadUI);
        }
    }

    /**
     * called when the site changed
     *
     * @param reloadUI
     */
    @Override
    public void onSiteChange(boolean reloadUI) {
        List<AreaVO> list;

        if (selectedFilters == null) {
            setSelectedFilters();
        }
        if (selectedFilters.getSite() != null && selectedFilters.getSite().getLabel() != null && !selectedFilters.getSite().getLabel().isEmpty() && selectedFilters.getSite().getValue() != null) {
            selectedFilters.clear(SITE);
            openWorkOrdersList.clear();
            workOrderTrendsMap.clear();
            assetList.clear();
            try {
                areaList.clear();
                list = AreaClient.getInstance().getAreaSiteByCustomerSite(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue());
                list.stream().sorted(Comparator.comparing(AreaVO::getAreaName)).forEachOrdered(area -> areaList.add(new SelectItem(area.getAreaId(), area.getAreaName())));
            } catch (Exception ex) {
                areaList.clear();
            }
            setAreaTileList((UUID) selectedFilters.getSite().getValue());

            if (reloadUI) {
                updateTreeNodeRoot(SITE);
                updateBreadcrumbModel();
                updateUI("content");
            }
        } else {
            onCountryChange(reloadUI);
        }
    }

    /**
     * called when the area changed
     *
     * @param reloadUI
     */
    @Override
    public void onAreaChange(boolean reloadUI) {
        List<AssetVO> list;

        if (selectedFilters == null) {
            setSelectedFilters();
        }
        if (selectedFilters.getArea() != null && selectedFilters.getArea().getLabel() != null && !selectedFilters.getArea().getLabel().isEmpty() && selectedFilters.getArea().getValue() != null) {
            selectedFilters.clear(AREA);
            workOrderTrendsMap.clear();
            areaTileList.clear();
            try {
                assetList.clear();
                list = AssetClient.getInstance().getAssetSiteAreaByCustomerSiteArea(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue());
                list.stream().sorted(Comparator.comparing(AssetVO::getAssetName)).forEachOrdered(asset -> assetList.add(new SelectItem(asset.getAssetId(), asset.getAssetName())));
            } catch (Exception ex) {
                assetList.clear();
            }
            try {
                openWorkOrdersList.clear();
                openWorkOrdersList.addAll(WorkOrderClient.getInstance().getOpenWorkOrdersByCustomerSiteArea(ServiceRegistrarClient.getInstance().getServiceHostURL(WORK_ORDER_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue()));
            } catch (Exception ex) {
                assetList.clear();
            }

            if (reloadUI) {
                updateTreeNodeRoot(AREA);
                updateBreadcrumbModel();
                updateUI("content");
            }
        } else {
            onSiteChange(reloadUI);
        }
    }

    /**
     * called when the asset changed
     *
     * @param reloadUI
     */
    @Override
    public void onAssetChange(boolean reloadUI) {
        if (selectedFilters == null) {
            setSelectedFilters();
        }
        if (selectedFilters.getAsset() != null && selectedFilters.getAsset().getLabel() != null && !selectedFilters.getAsset().getLabel().isEmpty()) {
            workOrderTrendsMap.clear();
            areaTileList.clear();
            try {
                openWorkOrdersList.clear();
                openWorkOrdersList.addAll(WorkOrderClient.getInstance().getOpenWorkOrdersByCustomerSiteAreaAsset(ServiceRegistrarClient.getInstance().getServiceHostURL(WORK_ORDER_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue(), (UUID) selectedFilters.getAsset().getValue()));
            } catch (Exception ex) {
                openWorkOrdersList.clear();
            }

            if (reloadUI) {
                updateTreeNodeRoot(ASSET);
                updateBreadcrumbModel();
                PrimeFaces.current().ajax().update("workOrderResultFormId:create-workorder-button");
                updateUI("content");
            }
        } else {
            onAreaChange(reloadUI);
        }
    }

    @Override
    public void onPointLocationChange(boolean reloadUI) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void onPointChange(boolean reloadUI) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void onApSetChange(boolean reloadUI) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void onAlSetParamChange(boolean reloadUI) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    /**
     * Action to take when the row is toggled
     *
     * @param workOrderVO
     */
    public void onRowToggle(WorkOrderVO workOrderVO) {
        List<WorkOrderVO> workOrderVOList;

        if (workOrderVO != null && !workOrderTrendsMap.containsKey(workOrderVO)) {
            workOrderVOList = WorkOrderClient.getInstance().getWorkOrderTrendsByCustomerSiteAreaAssetPriorityDate(ServiceRegistrarClient.getInstance().getServiceHostURL(WORK_ORDER_SERVICE_NAME), workOrderVO.getCustomerAccount(), workOrderVO.getSiteId(), workOrderVO.getAreaId(), workOrderVO.getAssetId(), workOrderVO.getPriority(), workOrderVO.getCreatedDate());

            workOrderVOList.stream().forEachOrdered(woVo -> {
                List<PointLocationVO> pointLocationVOList;
                List<PointVO> pointVOList;

                if (woVo.getPointLocationId() != null) {
                    if (!(pointLocationVOList = PointLocationClient.getInstance().getPointLocationsByPK(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), woVo.getCustomerAccount(), woVo.getSiteId(), woVo.getAreaId(), woVo.getAssetId(), woVo.getPointLocationId())).isEmpty()) {
                        woVo.setPointLocationName(pointLocationVOList.get(0).getPointLocationName());
                    }
                    if (woVo.getPointId() != null && !(pointVOList = PointClient.getInstance().getPointsByCustomerSiteAreaAssetPointLocationPoint(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), woVo.getCustomerAccount(), woVo.getSiteId(), woVo.getAreaId(), woVo.getAssetId(), woVo.getPointLocationId(), woVo.getPointId(), true)).isEmpty()) {
                        woVo.setPointName(pointVOList.get(0).getPointName());
                    }
                }
                woVo.setAreaName(workOrderVO.getAreaName());
                woVo.setAssetName(workOrderVO.getAssetName());
                woVo.setIssue(workOrderVO.getIssue());
            });
            workOrderTrendsMap.put(workOrderVO, workOrderVOList);
        }
    }

    /**
     * Update the UI based on the given value
     *
     * @param value
     */
    @Override
    public void updateUI(String value) {
        try {
            switch (value) {
                case "content":
                    PrimeFaces.current().ajax().update("workOrderFilterFormId");
                    PrimeFaces.current().ajax().update("workOrderResultFormId");
                    break;
                case "tree":
                    PrimeFaces.current().ajax().update("leftTreeFormId");
                    break;
                case "breadcrumb":
                    PrimeFaces.current().ajax().update("breadcrumbFormId");
                    break;
            }
        } catch (Exception e) {
            Logger.getLogger(WorkOrderBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Update the breadcrumbModel field from the NavigationBean
     */
    @Override
    public void updateBreadcrumbModel() {
        DefaultMenuItem index;

        if (navigationBean != null && userManageBean != null) {

            // Set Root
            navigationBean.setBreadcrumbModel(new DefaultMenuModel());
            index = new DefaultMenuItem();
            index.setValue(userManageBean.getResourceBundleString("breadcrumb_3"));
            index.setIcon("pi pi-fw pi-tag");
            index.setCommand("#{workOrderBean.resetPage()}");
            index.setOncomplete("updateTreeNodes()");
            navigationBean.getBreadcrumbModel().getElements().add(index);

            try {
                // Set Region
                if (selectedFilters.getRegion() != null && !selectedFilters.getRegion().isEmpty()) {
                    updateBreadcrumbModelHelper(selectedFilters.getRegion(), "#{workOrderBean.onRegionChange(true)}", navigationBean.getBreadcrumbModel().getElements());
                }

                // Set Country
                if (selectedFilters.getCountry() != null && !selectedFilters.getCountry().isEmpty()) {
                    updateBreadcrumbModelHelper(selectedFilters.getCountry(), "#{workOrderBean.onCountryChange(true)}", navigationBean.getBreadcrumbModel().getElements());
                }

                // Set Site
                if (selectedFilters.getSite() != null && selectedFilters.getSite().getLabel() != null && !selectedFilters.getSite().getLabel().isEmpty()) {
                    updateBreadcrumbModelHelper(selectedFilters.getSite().getLabel(), "#{workOrderBean.onSiteChange(true)}", navigationBean.getBreadcrumbModel().getElements());
                }

                // Set Area
                if (selectedFilters.getArea() != null && selectedFilters.getArea().getLabel() != null && !selectedFilters.getArea().getLabel().isEmpty()) {
                    updateBreadcrumbModelHelper(selectedFilters.getArea().getLabel(), "#{workOrderBean.onAreaChange(true)}", navigationBean.getBreadcrumbModel().getElements());
                }

                // Set Asset
                if (selectedFilters.getAsset() != null && selectedFilters.getAsset().getLabel() != null && !selectedFilters.getAsset().getLabel().isEmpty()) {
                    updateBreadcrumbModelHelper(selectedFilters.getAsset().getLabel(), "#{workOrderBean.onAssetChange(true)}", navigationBean.getBreadcrumbModel().getElements());
                }
            } catch (Exception e) {
                Logger.getLogger(ConfigurationBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);

                // Set Root
                navigationBean.setBreadcrumbModel(new DefaultMenuModel());
                index = new DefaultMenuItem();
                index.setValue(userManageBean.getResourceBundleString("breadcrumb_3"));
                index.setIcon("pi pi-fw pi-tag");
                index.setCommand("#{workOrderBean.resetPage()}");
                index.setOncomplete("updateTreeNodes()");
                navigationBean.getBreadcrumbModel().getElements().add(index);
            }
            updateUI("breadcrumb");
        }
    }

    /**
     * Build the treeNodeRoot field from the NavigationBean
     *
     * @param type, String Object
     * @param data, Object
     * @param parent, TreeNode Object
     * @param list, List Object of Objects
     */
    @Override
    public void treeContentMenuBuilder(String type, Object data, TreeNode parent, List<Object> list) {
        TreeNode newNode;
        final List<WorkOrderVO> workOrderCountsList;
        boolean useList = false;

        if (navigationBean != null && userManageBean != null) {
            if (type != null) {
                switch (type.toLowerCase()) {
                    case ROOT:
                        newNode = new DefaultTreeNode(ROOT, data, null);
                        navigationBean.setTreeNodeRoot(newNode);
                        if (regionCountrySiteMap.keySet() != null && !regionCountrySiteMap.keySet().isEmpty()) {
                            regionCountrySiteMap.keySet().stream().sorted().collect(Collectors.toList()).forEach(key -> treeContentMenuBuilder(REGION, key, navigationBean.getTreeNodeRoot(), null));
                        }
                        updateUI("tree");
                        break;
                    case REGION:
                        list = (data == null) ? new ArrayList(regionCountrySiteMap.keySet().stream().sorted().collect(Collectors.toList())) : new ArrayList();
                        treeContentMenuBuilderHelper(type, data, parent, list, true);
                        break;
                    case COUNTRY:
                        if (parent != null) {
                            list = (data == null) ? new ArrayList(regionCountrySiteMap.get((String) parent.getData()).keySet().stream().sorted().collect(Collectors.toList())) : new ArrayList();
                            treeContentMenuBuilderHelper(type, data, parent, list, true);
                        }
                        break;
                    case SITE_OVER_100:
                        if (parent != null) {
                            list = new ArrayList(regionCountrySiteMap.get((String) parent.getParent().getData()).get((String) parent.getData()).stream().sorted().collect(Collectors.toList()));
                            treeContentMenuBuilderHelper(type, data, SITE, parent, list, true);
                        }
                        break;
                    case SITE:
                        if (parent != null) {
                            list = (data == null) ? new ArrayList(regionCountrySiteMap.get((String) parent.getParent().getData()).get((String) parent.getData()).stream().sorted(Comparator.comparing(SelectItem::getLabel)).collect(Collectors.toList())) : new ArrayList();
                            treeContentMenuBuilderHelper(type, data, parent, list, true);
                        }
                        break;
                    case AREA_OVER_100:
                        if (list == null || list.isEmpty()) {
                            list = areaList != null ? new ArrayList(areaList) : new ArrayList();
                        } else {
                            useList = true;
                        }
                        treeContentMenuBuilderHelper(type, data, AREA, parent, list, useList);
                        break;
                    case AREA:
                        if (list == null || list.isEmpty()) {
                            list = areaList != null ? new ArrayList(areaList) : new ArrayList();
                        }

                        // Setting count for area
                        if (!list.isEmpty() && list.get(0) instanceof SelectItem) {
                            try {
                                if ((workOrderCountsList = getWorkOrderCountsList(parent)) != null && !workOrderCountsList.isEmpty()) {
                                    list.stream().forEachOrdered(item -> {
                                        AtomicInteger count = new AtomicInteger(0);

                                        workOrderCountsList.stream().filter(workOrderCounts -> workOrderCounts.getTotalCount() > 0 && workOrderCounts.getAreaId().equals((UUID) ((SelectItem) item).getValue())).forEachOrdered(workOrderCounts -> {
                                            count.set(count.get() + workOrderCounts.getTotalCount());
                                        });

                                        if (count.get() > 0) {
                                            ((SelectItem) item).setDescription(String.valueOf(count.get()));
                                        } else if (((SelectItem) item).getDescription() != null) {
                                            ((SelectItem) item).setDescription(null);
                                        }
                                    });
                                }
                            } catch (Exception e) {
                                Logger.getLogger(ConfigurationBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
                            }
                        }

                        treeContentMenuBuilderHelper(type, data, parent, list, true);
                        break;
                    case ASSET_OVER_100:
                        if (list == null || list.isEmpty()) {
                            list = assetList != null ? new ArrayList(assetList) : new ArrayList();
                        } else {
                            useList = true;
                        }
                        treeContentMenuBuilderHelper(type, data, ASSET, parent, list, useList);
                        break;
                    case ASSET:
                        if (list == null || list.isEmpty()) {
                            list = assetList != null ? new ArrayList(assetList) : new ArrayList();
                        }

                        // Setting count for asset
                        if (!list.isEmpty() && list.get(0) instanceof SelectItem) {
                            try {
                                if ((workOrderCountsList = getWorkOrderCountsList(parent)) != null && !workOrderCountsList.isEmpty()) {
                                    list.stream().forEachOrdered(item -> {
                                        for (WorkOrderVO workOrderCounts : workOrderCountsList) {
                                            if (workOrderCounts.getAssetId().equals((UUID) ((SelectItem) item).getValue())) {
                                                if (workOrderCounts.getTotalCount() > 0) {
                                                    ((SelectItem) item).setDescription(String.valueOf(workOrderCounts.getTotalCount()));
                                                } else if (((SelectItem) item).getDescription() != null) {
                                                    ((SelectItem) item).setDescription(null);
                                                }
                                                break;
                                            }
                                        }
                                    });
                                }
                            } catch (Exception e) {
                                Logger.getLogger(ConfigurationBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
                            }
                        }

                        treeContentMenuBuilderHelper(type, data, parent, list, false);
                        break;
                    case EMPTY:
                        if (parent != null) {
                            newNode = new DefaultTreeNode(type, data, parent);
                            newNode.setSelectable(false);
                        }
                        break;
                }
            } else {
                navigationBean.setTreeNodeRoot(new DefaultTreeNode(ROOT, new SelectItem(null, userManageBean.getResourceBundleString("breadcrumb_3")), null));
                updateUI("tree");
            }
        }
    }

    /**
     * Used for the NodeExpandEvent caused by the Nav Tree component
     *
     * @param event, NodeExpandEvent Object
     */
    @Override
    public void onNodeExpand(NodeExpandEvent event) {
        List<Object> list = new ArrayList();
        UUID siteId, areaId;

        try {
            if (event != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null && !userManageBean.getCurrentSite().getCustomerAccount().isEmpty()) {
                switch (event.getTreeNode().getType()) {
                    case REGION:
                        event.getTreeNode().getChildren().clear();
                        treeContentMenuBuilder(COUNTRY, null, event.getTreeNode(), null);
                        break;
                    case COUNTRY:
                        if (regionCountrySiteMap.get((String) event.getTreeNode().getParent().getData()).get((String) event.getTreeNode().getData()).size() > 100) {
                            event.getTreeNode().getChildren().clear();
                            treeContentMenuBuilder(SITE_OVER_100, null, event.getTreeNode(), null);
                        } else if (!regionCountrySiteMap.get((String) event.getTreeNode().getParent().getData()).get((String) event.getTreeNode().getData()).isEmpty()) {
                            event.getTreeNode().getChildren().clear();
                            treeContentMenuBuilder(SITE, null, event.getTreeNode(), null);
                        }
                        break;
                    case SITE:
                        siteId = (UUID) ((SelectItem) event.getTreeNode().getData()).getValue();
                        AreaClient.getInstance().getAreaSiteByCustomerSite(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), siteId).stream().sorted(Comparator.comparing(AreaVO::getAreaName)).forEachOrdered(vo -> list.add(new SelectItem(vo.getAreaId(), vo.getAreaName())));
                        if (list.size() > 100) {
                            event.getTreeNode().getChildren().clear();
                            treeContentMenuBuilder(AREA_OVER_100, null, event.getTreeNode(), list);
                        } else if (!list.isEmpty()) {
                            event.getTreeNode().getChildren().clear();
                            treeContentMenuBuilder(AREA, null, event.getTreeNode(), list);
                        }
                        break;
                    case AREA:
                        siteId = event.getTreeNode().getParent().getType().equals(SITE) ? (UUID) ((SelectItem) event.getTreeNode().getParent().getData()).getValue() : (UUID) ((SelectItem) event.getTreeNode().getParent().getParent().getData()).getValue();
                        areaId = (UUID) ((SelectItem) event.getTreeNode().getData()).getValue();
                        AssetClient.getInstance().getAssetSiteAreaByCustomerSiteArea(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), siteId, areaId).stream().sorted(Comparator.comparing(AssetVO::getAssetName)).forEachOrdered(vo -> list.add(new SelectItem(vo.getAssetId(), vo.getAssetName())));
                        if (list.size() > 100) {
                            event.getTreeNode().getChildren().clear();
                            treeContentMenuBuilder(ASSET_OVER_100, null, event.getTreeNode(), list);
                        } else if (!list.isEmpty()) {
                            event.getTreeNode().getChildren().clear();
                            treeContentMenuBuilder(ASSET, null, event.getTreeNode(), list);
                        }
                        break;
                }
            }
        } catch (Exception e) {
            Logger.getLogger(WorkOrderBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Used for the NodeCollapseEvent caused by the Nav Tree component
     *
     * @param event, NodeCollapseEvent Object
     */
    @Override
    public void onNodeCollapse(NodeCollapseEvent event) {
        try {
            if (event != null && (event.getTreeNode().getType().equals(REGION) || event.getTreeNode().getType().equals(COUNTRY) || event.getTreeNode().getType().equals(SITE) || event.getTreeNode().getType().equals(AREA))) {
                event.getTreeNode().getChildren().clear();
                treeContentMenuBuilder(EMPTY, null, event.getTreeNode(), null);
            }
        } catch (Exception e) {
            Logger.getLogger(WorkOrderBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Download Open Work Orders as CSV
     *
     * @return StreamedContent Object
     */
    public StreamedContent downloadOpenOrderCSV() {
        String[] header = {userManageBean.getResourceBundleString("label_priority"), userManageBean.getResourceBundleString("label_area_name"), userManageBean.getResourceBundleString("label_asset"), userManageBean.getResourceBundleString("label_asset_component"), userManageBean.getResourceBundleString("label_deviceSerialNumber"), userManageBean.getResourceBundleString("label_issue"),
            userManageBean.getResourceBundleString("label_action"), userManageBean.getResourceBundleString("label_description"), userManageBean.getResourceBundleString("label_notes"), userManageBean.getResourceBundleString("label_user"), userManageBean.getResourceBundleString("label_date_created")};
        final List<WorkOrderVO> list;
        StringBuilder csvData;
        InputStream stream;
        PrestoDAO prestoDAO;
        try {
            Logger.getLogger(WorkOrderBean.class.getName()).log(Level.INFO, "**downloadOpenOrderCSV called***");
            csvData = new StringBuilder();

            // Header row
            // Convert header to CSV format using StringBuilder
            for (int i = 0; i < header.length; i++) {
                csvData.append(header[i]);
                if (i != header.length - 1) {
                    csvData.append(",");
                }
            }
            csvData.append("\n");
            
            //get the device serial #s 
            if (assetDeviceList == null && getPropertyEnvironment() != null && (getPropertyEnvironment().endsWith("DEMO") || getPropertyEnvironment().endsWith("UAT") || getPropertyEnvironment().endsWith("PROD"))) {
                Logger.getLogger(WorkOrderBean.class.getName()).log(Level.INFO, "**prestoDAO called***");
                prestoDAO = new PrestoDAO();
                assetDeviceList = AssetDeviceSaxJsonParser.getInstance().populateAssetDeviceVOFromJson(prestoDAO.getDevicesByCustomerSite(userManageBean.getCurrentSite().getCustomerAccount(), userManageBean.getCurrentSite().getSiteName()));
            }

            // Convert fetched data to CSV format using StringBuilder.
            // Look up the list if openWorkOrdersList is null or empty, else use the field openWorkOrdersList
            if (openWorkOrdersList == null || openWorkOrdersList.isEmpty()) {
                if (selectedFilters != null) {
                    if (selectedFilters.getArea() != null && selectedFilters.getArea().getValue() != null && selectedFilters.getAsset() != null && selectedFilters.getAsset().getValue() != null) {
                        list = WorkOrderClient.getInstance().getOpenWorkOrdersByCustomerSiteAreaAsset(ServiceRegistrarClient.getInstance().getServiceHostURL(WORK_ORDER_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), userManageBean.getCurrentSite().getSiteId(), (UUID) selectedFilters.getArea().getValue(), (UUID) selectedFilters.getAsset().getValue());
                    } else if (selectedFilters.getArea() != null && selectedFilters.getArea().getValue() != null) {
                        list = WorkOrderClient.getInstance().getOpenWorkOrdersByCustomerSiteArea(ServiceRegistrarClient.getInstance().getServiceHostURL(WORK_ORDER_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), userManageBean.getCurrentSite().getSiteId(), (UUID) selectedFilters.getArea().getValue());
                    } else {
                        list = WorkOrderClient.getInstance().getOpenWorkOrdersByCustomerSite(ServiceRegistrarClient.getInstance().getServiceHostURL(WORK_ORDER_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), userManageBean.getCurrentSite().getSiteId());
                    }
                } else {
                    list = WorkOrderClient.getInstance().getOpenWorkOrdersByCustomerSite(ServiceRegistrarClient.getInstance().getServiceHostURL(WORK_ORDER_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), userManageBean.getCurrentSite().getSiteId());
                }

                if (assetDeviceList != null && !assetDeviceList.isEmpty()) {
                    for (WorkOrderVO wo : list) {
                        for (AssetDeviceVO adVO : assetDeviceList)  {
                            if (adVO.getAssetId() != null && adVO.getAssetId().equals(wo.getAssetId())) {
                                if (wo.getDeviceIds() == null)
                                    wo.setDeviceIds(adVO.getDeviceId());
                                else if (!wo.getDeviceIds().contains(adVO.getDeviceId())) {
                                    wo.setDeviceIds(wo.getDeviceIds() + ", " + adVO.getDeviceId());
                                }
                            }
                        }
                    }
                }
                list.stream().forEachOrdered(workOrder -> {
                    csvData
                            .append(workOrder.getPriority()).append(",")
                            .append(workOrder.getAreaName() != null ? workOrder.getAreaName() : "").append(",")
                            .append(workOrder.getAssetName() != null ? workOrder.getAssetName() : "").append(",")
                            .append(workOrder.getAssetComponent() != null ? workOrder.getAssetComponent() : "").append(",")
                            .append(workOrder.getDeviceIds() != null ? (workOrder.getDeviceIds().contains(",") ? "\"" + workOrder.getDeviceIds() + "\"" : workOrder.getDeviceIds()) : "").append(",")
                            .append(workOrder.getIssue() != null ? workOrder.getIssue() : "").append(",")
                            .append(workOrder.getAction() != null ? workOrder.getAction() : "").append(",")
                            .append(quoteIfNeeded(workOrder.getDescription() != null ? workOrder.getDescription().replaceAll("[\\s]+", " ") : "")).append(",") // Use the sanitized description
                            .append(quoteIfNeeded(workOrder.getNotes() != null ? workOrder.getNotes().replaceAll("[\\s]+", " ") : "")).append(",")
                            .append(workOrder.getUser() != null ? workOrder.getUser() : "").append(",")
                            .append(workOrder.getCreatedDate() != null ? DATE_FORMATTER.withZone(ZoneId.of("UTC")).format(workOrder.getCreatedDate()) : "").append("\n");
                });
            } else {
                
                if (assetDeviceList != null && !assetDeviceList.isEmpty()) {
                    for (WorkOrderVO wo : openWorkOrdersList) {
                        for (AssetDeviceVO adVO : assetDeviceList)  {
                            if (adVO.getAssetId() != null && adVO.getAssetId().equals(wo.getAssetId())) {
                                if (wo.getDeviceIds() == null)
                                    wo.setDeviceIds(adVO.getDeviceId());
                                else if (!wo.getDeviceIds().contains(adVO.getDeviceId())) {
                                    wo.setDeviceIds(wo.getDeviceIds() + ", " + adVO.getDeviceId());
                                }
                            }
                        }
                    }
                }
                openWorkOrdersList.stream().forEachOrdered(workOrder -> {
                    csvData
                            .append(workOrder.getPriority()).append(",")
                            .append(workOrder.getAreaName() != null ? workOrder.getAreaName() : "").append(",")
                            .append(workOrder.getAssetName() != null ? workOrder.getAssetName() : "").append(",")
                            .append(workOrder.getAssetComponent() != null ? workOrder.getAssetComponent() : "").append(",")
                            .append(workOrder.getDeviceIds() != null ? (workOrder.getDeviceIds().contains(",") ? "\"" + workOrder.getDeviceIds() + "\"" : workOrder.getDeviceIds()) : "").append(",")
                            .append(workOrder.getIssue() != null ? workOrder.getIssue() : "").append(",")
                            .append(workOrder.getAction() != null ? workOrder.getAction() : "").append(",")
                            .append(quoteIfNeeded(workOrder.getDescription() != null ? workOrder.getDescription().replaceAll("[\\s]+", " ") : "")).append(",") // Use the sanitized description
                            .append(quoteIfNeeded(workOrder.getNotes() != null ? workOrder.getNotes().replaceAll("[\\s]+", " ") : "")).append(",")
                            .append(workOrder.getUser() != null ? workOrder.getUser() : "").append(",")
                            .append(workOrder.getCreatedDate() != null ? DATE_FORMATTER.withZone(ZoneId.of("UTC")).format(workOrder.getCreatedDate()) : "").append("\n");
                });
            }

            // Convert CSV content to InputStream
            stream = new ByteArrayInputStream(csvData.toString().getBytes(StandardCharsets.UTF_8));

            // Create and return StreamedContent
            return DefaultStreamedContent.builder().name(userManageBean.getResourceBundleString("workorder_download_file_name")).contentType("text/csv").stream(() -> stream).build();
        } catch (Exception e) {
            Logger.getLogger(WorkOrderBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            return null;
        }
    }

    /**
     * Helper function to enclose text within double quotes if it contains
     * commas
     *
     * @param text, String Object
     * @return String Object
     */
    private String quoteIfNeeded(String text) {
        if (text != null && text.contains(",")) {
            return "\"" + text + "\"";
        }
        return text;
    }

    /**
     * Used for the NodeSelectEvent caused by the Nav Tree component
     *
     * @param event, NodeSelectEvent Object
     */
    @Override
    public void onNodeSelect(NodeSelectEvent event) {
        TreeNode parent;
        List<AreaVO> areaVOList;
        List<AssetVO> assetVOList;

        try {
            if (event != null) {
                parent = event.getTreeNode().getParent();
                do {
                    if (parent != null) {
                        switch (parent.getType()) {
                            case REGION:
                                selectedFilters.setRegion((String) parent.getData());
                                parent = null;
                                break;
                            case COUNTRY:
                                selectedFilters.setCountry((String) parent.getData());
                                break;
                            case SITE_OVER_100:
                                selectedFilters.setSiteOver100((String) parent.getData());
                                break;
                            case SITE:
                                selectedFilters.setSite((SelectItem) parent.getData());
                                break;
                            case AREA_OVER_100:
                                selectedFilters.setAreaOver100((String) parent.getData());
                                resetDataTable();
                                break;
                            case AREA:
                                selectedFilters.setArea((SelectItem) parent.getData());
                                resetDataTable();
                                break;
                            case ASSET_OVER_100:
                                selectedFilters.setAssetOver100((String) parent.getData());
                                break;
                        }
                        if (parent != null) {
                            parent = parent.getParent();
                        }
                    }
                } while (parent != null);

                switch (event.getTreeNode().getType()) {
                    case REGION:
                        selectedFilters.setRegion((String) event.getTreeNode().getData());
                        selectedFilters.clear(REGION);
                        openWorkOrdersList.clear();
                        areaTileList.clear();
                        areaList.clear();
                        assetList.clear();
                        break;
                    case COUNTRY:
                        selectedFilters.setCountry((String) event.getTreeNode().getData());
                        selectedFilters.clear(COUNTRY);
                        openWorkOrdersList.clear();
                        areaTileList.clear();
                        areaList.clear();
                        assetList.clear();
                        break;
                    case SITE:
                        selectedFilters.setSite((SelectItem) event.getTreeNode().getData());
                        selectedFilters.clear(SITE);
                        openWorkOrdersList.clear();
                        assetList.clear();
                        try {
                            areaList.clear();
                            areaVOList = AreaClient.getInstance().getAreaSiteByCustomerSite(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue());
                            areaVOList.stream().sorted(Comparator.comparing(AreaVO::getAreaName)).forEachOrdered(area -> areaList.add(new SelectItem(area.getAreaId(), area.getAreaName())));
                        } catch (Exception ex) {
                            areaList.clear();
                        }
                        setAreaTileList((UUID) selectedFilters.getSite().getValue());
                        break;
                    case AREA:
                        selectedFilters.setArea((SelectItem) event.getTreeNode().getData());
                        selectedFilters.clear(AREA);
                        areaTileList.clear();
                        try {
                            areaList.clear();
                            areaVOList = AreaClient.getInstance().getAreaSiteByCustomerSite(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue());
                            areaVOList.stream().sorted(Comparator.comparing(AreaVO::getAreaName)).forEachOrdered(area -> areaList.add(new SelectItem(area.getAreaId(), area.getAreaName())));
                        } catch (Exception ex) {
                            areaList.clear();
                        }
                        try {
                            assetList.clear();
                            assetVOList = AssetClient.getInstance().getAssetSiteAreaByCustomerSiteArea(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue());
                            assetVOList.stream().sorted(Comparator.comparing(AssetVO::getAssetName)).forEachOrdered(asset -> assetList.add(new SelectItem(asset.getAssetId(), asset.getAssetName())));
                        } catch (Exception ex) {
                            assetList.clear();
                        }
                        try {
                            openWorkOrdersList.clear();
                            openWorkOrdersList.addAll(WorkOrderClient.getInstance().getOpenWorkOrdersByCustomerSiteArea(ServiceRegistrarClient.getInstance().getServiceHostURL(WORK_ORDER_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue()));
                        } catch (Exception ex) {
                            openWorkOrdersList.clear();
                        }
                        break;
                    case ASSET:
                        selectedFilters.setAsset((SelectItem) event.getTreeNode().getData());
                        selectedFilters.clear(ASSET);
                        areaTileList.clear();
                        try {
                            areaList.clear();
                            areaVOList = AreaClient.getInstance().getAreaSiteByCustomerSite(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue());
                            areaVOList.stream().sorted(Comparator.comparing(AreaVO::getAreaName)).forEachOrdered(area -> areaList.add(new SelectItem(area.getAreaId(), area.getAreaName())));
                        } catch (Exception ex) {
                            areaList.clear();
                        }
                        try {
                            assetList.clear();
                            assetVOList = AssetClient.getInstance().getAssetSiteAreaByCustomerSiteArea(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue());
                            assetVOList.stream().sorted(Comparator.comparing(AssetVO::getAssetName)).forEachOrdered(asset -> assetList.add(new SelectItem(asset.getAssetId(), asset.getAssetName())));
                        } catch (Exception ex) {
                            assetList.clear();
                        }
                        try {
                            openWorkOrdersList.clear();
                            openWorkOrdersList.addAll(WorkOrderClient.getInstance().getOpenWorkOrdersByCustomerSiteAreaAsset(ServiceRegistrarClient.getInstance().getServiceHostURL(WORK_ORDER_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue(), (UUID) selectedFilters.getAsset().getValue()));
                        } catch (Exception ex) {
                            openWorkOrdersList.clear();
                        }
                        break;
                }
                updateBreadcrumbModel();
                updateUI("content");
            }
        } catch (Exception e) {
            Logger.getLogger(WorkOrderBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Sets the given field regionCountrySiteMap
     */
    @Override
    public void setRegionCountrySiteMap() {
        try {
            regionCountrySiteMap.clear();

            if (userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite() != null && !userManageBean.getCurrentSite().getCustomerAccount().isEmpty()) {
                SiteClient.getInstance().getSiteRegionCountryByCustomer(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount()).forEach(siteVO -> {
                    if (userManageBean.getCurrentSite().getSiteId().equals(siteVO.getSiteId())) {
                        if (regionCountrySiteMap.containsKey(siteVO.getRegion())) {
                            if (regionCountrySiteMap.get(siteVO.getRegion()).containsKey(siteVO.getCountry())) {
                                if (!regionCountrySiteMap.get(siteVO.getRegion()).get(siteVO.getCountry()).stream().anyMatch(site -> site.getValue().equals(siteVO.getSiteId()))) {
                                    regionCountrySiteMap.get(siteVO.getRegion()).get(siteVO.getCountry()).add(new SelectItem(siteVO.getSiteId(), siteVO.getSiteName()));
                                }
                            } else {
                                regionCountrySiteMap.get(siteVO.getRegion()).put(siteVO.getCountry(), new ArrayList());
                                regionCountrySiteMap.get(siteVO.getRegion()).get(siteVO.getCountry()).add(new SelectItem(siteVO.getSiteId(), siteVO.getSiteName()));
                            }
                        } else {
                            regionCountrySiteMap.put(siteVO.getRegion(), new HashMap());
                            regionCountrySiteMap.get(siteVO.getRegion()).put(siteVO.getCountry(), new ArrayList());
                            regionCountrySiteMap.get(siteVO.getRegion()).get(siteVO.getCountry()).add(new SelectItem(siteVO.getSiteId(), siteVO.getSiteName()));
                        }
                    }
                });
            }
        } catch (Exception ex) {
            regionCountrySiteMap.clear();
        }
    }

    /**
     * Return the WorkOrderCounts List Object based on the given TreeNode values
     *
     * @param siteId, UUID Object
     * @return List Object of WorkOrderVO Objects
     */
    private List<WorkOrderVO> getWorkOrderCountsList(Object value) {
        List<WorkOrderVO> list = new ArrayList();
        UUID siteId = null;
        TreeNode parent;

        if (value != null && userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null && !userManageBean.getCurrentSite().getCustomerAccount().isEmpty()) {
            if (value instanceof TreeNode) {
                parent = (TreeNode) value;
                do {
                    try {
                        switch (parent.getType()) {
                            case SITE:
                                siteId = ((UUID) ((SelectItem) parent.getData()).getValue());
                                break;
                            default:
                                parent = parent.getParent();
                                break;
                        }
                    } catch (Exception ex) {
                        break;
                    }
                } while (parent != null && siteId == null);
            } else if (value instanceof UUID) {
                siteId = (UUID) value;
            }

            if (workOrderCountsMap.get(siteId) != null) {
                list = workOrderCountsMap.get(siteId);
            } else {
                list = WorkOrderClient.getInstance().getWorkOrderCountsByCustomerSite(ServiceRegistrarClient.getInstance().getServiceHostURL(WORK_ORDER_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), siteId);
                workOrderCountsMap.put(siteId, list);
            }
        }
        return list;
    }

    /**
     * Called when a area tile is selected from the areaTileList field
     *
     * @param workOrderVO, WorkOrderVO Object
     */
    public void onAreaTileSelect(WorkOrderVO workOrderVO) {
        if (workOrderVO != null && workOrderVO.getAreaId() != null && workOrderVO.getAreaName() != null && !workOrderVO.getAreaName().isEmpty()) {
            selectedFilters.setArea(new SelectItem(workOrderVO.getAreaId(), workOrderVO.getAreaName(), String.valueOf(workOrderVO.getTotalCount())));
            onAreaChange(true);
        }
    }

    /**
     * Sets the areaTileList based on the Site Id
     *
     * @param siteId, UUID Object
     */
    private void setAreaTileList(UUID siteId) {
        List<WorkOrderVO> list, unsortedList;

        areaTileList.clear();
        if (siteId != null && (list = getWorkOrderCountsList(siteId)) != null && !list.isEmpty()) {
            try {
                unsortedList = new ArrayList();
                list.stream().forEachOrdered(item -> {
                    List<SelectItem> selectItemList = null;

                    if (!unsortedList.stream().anyMatch(vo -> vo.getAreaId().equals(item.getAreaId())) && !areaList.isEmpty() && (selectItemList = areaList.stream().filter(selectItem -> item.getAreaId().equals((UUID) selectItem.getValue())).collect(Collectors.toList())) != null && !selectItemList.isEmpty()) {
                        AtomicInteger count;
                        AtomicInteger priority1;
                        AtomicInteger priority2;
                        AtomicInteger priority3;
                        AtomicInteger priority4;
                        WorkOrderVO vo;

                        count = new AtomicInteger(0);
                        priority1 = new AtomicInteger(0);
                        priority2 = new AtomicInteger(0);
                        priority3 = new AtomicInteger(0);
                        priority4 = new AtomicInteger(0);

                        list.stream().filter(workOrderCounts -> workOrderCounts.getTotalCount() > 0 && workOrderCounts.getAreaId().equals(item.getAreaId())).forEachOrdered(workOrderCounts -> {
                            count.set(count.get() + workOrderCounts.getTotalCount());
                            priority1.set(priority1.get() + workOrderCounts.getPriority1());
                            priority2.set(priority2.get() + workOrderCounts.getPriority2());
                            priority3.set(priority3.get() + workOrderCounts.getPriority3());
                            priority4.set(priority4.get() + workOrderCounts.getPriority4());
                        });

                        if (count.get() > 0) {
                            vo = new WorkOrderVO();
                            vo.setAreaId(item.getAreaId());
                            vo.setAreaName(selectItemList.get(0).getLabel());
                            vo.setTotalCount(count.get());
                            vo.setPriority1(priority1.get());
                            vo.setPriority2(priority2.get());
                            vo.setPriority3(priority3.get());
                            vo.setPriority4(priority4.get());
                            unsortedList.add(vo);
                        }
                    }
                });

                areaTileList.addAll(unsortedList.stream().sorted(Comparator.comparingInt(WorkOrderVO::getPriority1).thenComparingInt(WorkOrderVO::getPriority2).thenComparingInt(WorkOrderVO::getPriority3).thenComparingInt(WorkOrderVO::getPriority4).reversed()).collect(Collectors.toList()));
            } catch (Exception e) {
                Logger.getLogger(AlarmsBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
                areaTileList.clear();
            }
        }
    }

    /**
     * Returns a style String Object based on the given priority
     *
     * @param priority, short
     * @return, String Object
     */
    public String getPriorityStyle(byte priority) {
        switch (priority) {
            case (byte) 1:
                return "background-image: radial-gradient(rgba(255,0,0,1), rgba(255,0,0,.3));";
            case (byte) 2:
                return "background-image: radial-gradient(rgba(255,127,0,1), rgba(255,127,0,.3));";
            case (byte) 3:
                return "background-image: radial-gradient(rgba(255,255,0,1), rgba(255,255,0,.3));";
            case (byte) 4:
                return "background-image: radial-gradient(rgba(0,255,0,1), rgba(0,255,0,.3));";
            default:
                return "background-image: radial-gradient(rgba(0,255,0,1), rgba(0,255,0,.3));";
        }
    }

    /**
     * Close the given work order
     *
     * @param workOrderVO, WorkOrderVO Object
     */
    public void close(WorkOrderVO workOrderVO) {
        LocalDateTime localDateTime;

        if (workOrderVO != null) {
            localDateTime = LocalDateTime.ofInstant(workOrderVO.getCreatedDate(), ZoneOffset.UTC);

            workOrderVO.setCloseDate(Instant.now().atZone(ZoneOffset.UTC).toInstant().truncatedTo(ChronoUnit.SECONDS));
            workOrderVO.setOpenedYear((short) localDateTime.getYear());

            if (WorkOrderClient.getInstance().closeWorkOrder(ServiceRegistrarClient.getInstance().getServiceHostURL(WORK_ORDER_SERVICE_NAME), workOrderVO)) {
                updateCount();
            }
        }
    }

    /**
     * Update the work order Count
     */
    public void updateCount() {
        if (userManageBean != null) {
            treeContentMenuBuilder(ROOT, userManageBean.getResourceBundleString("breadcrumb_3"), null, null);
            workOrderCountsMap.clear();
            try {
                if (selectedFilters.getAsset() != null && selectedFilters.getAsset().getLabel() != null && !selectedFilters.getAsset().getLabel().isEmpty()) {
                    onAssetChange(true);
                } else {
                    onAreaChange(true);
                }
            } catch (Exception e) {
                Logger.getLogger(WorkOrderBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            }
        }
    }

    /**
     * Open the overlay to view the Closed Work Orders
     */
    public void viewClosedWorkOrderPage() {
        if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("closedWorkOrderBean") == null) {
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("closedWorkOrderBean", new ClosedWorkOrderBean());
            ((ClosedWorkOrderBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("closedWorkOrderBean")).init();
        }
        navigationBean.updateDialog("closed-work-order");
    }

    /**
     * Open the overlay to create a new general Work Order
     */
    public void openWorkOrderPage() {
        if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("createWorkOrderBean") == null) {
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("createWorkOrderBean", new CreateWorkOrderBean());
        }
        ((CreateWorkOrderBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("createWorkOrderBean")).presetFields(selectedFilters, null);
        navigationBean.updateDialog("create-work-order");
    }

    /**
     * Edit the given work order
     *
     * @param workOrderVO, WorkOrderVO Object
     */
    public void edit(WorkOrderVO workOrderVO) {
        if (workOrderVO != null) {
            if ((EditWorkOrderBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("editWorkOrderBean") == null) {
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("editWorkOrderBean", new EditWorkOrderBean());
                ((EditWorkOrderBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("editWorkOrderBean")).init();
            }
            ((EditWorkOrderBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("editWorkOrderBean")).presetFields(selectedFilters, workOrderVO);
            navigationBean.updateDialog("edit-work-order");
            PrimeFaces.current().executeScript("PF('defaultDlg').show()");
        }
    }

    /**
     * View the trend graphs based on the given data
     *
     * @param workOrderVO, WorkOrderVO Object
     */
    public void viewTrendGraph(WorkOrderVO workOrderVO) {
        AssetAnalysisWindowBean assetAnalysisWindowBean;
        Filters filters;

        try {
            if ((assetAnalysisWindowBean = (AssetAnalysisWindowBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("assetAnalysisWindowBean")) == null) {
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("assetAnalysisWindowBean", new AssetAnalysisWindowBean());
                assetAnalysisWindowBean = (AssetAnalysisWindowBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("assetAnalysisWindowBean");
                assetAnalysisWindowBean.init();
            }

            filters = new Filters();
            filters.setRegion(userManageBean.getSelectedFilters().getRegion());
            filters.setCountry(userManageBean.getSelectedFilters().getCountry());
            filters.setSite(new SelectItem(workOrderVO.getSiteId(), workOrderVO.getSiteName()));
            filters.setArea(new SelectItem(workOrderVO.getAreaId(), workOrderVO.getAreaName()));
            filters.setAsset(new SelectItem(workOrderVO.getAssetId(), workOrderVO.getAssetName()));
            filters.setPointLocation(new SelectItem(workOrderVO.getPointLocationId(), workOrderVO.getPointLocationName()));
            filters.setPoint(new SelectItem(workOrderVO.getPointId(), workOrderVO.getPointName()));
            filters.setApSet(new SelectItem(workOrderVO.getApSetId()));

            // The AlSet UUID is being left null because the value needs to be looked up and it isn't used in the method loadTrendGraph.
            filters.setAlSetParam(new SelectItem(null, workOrderVO.getParamName()));

            assetAnalysisWindowBean.loadTrendGraph(filters, false);
        } catch (Exception e) {
            Logger.getLogger(WorkOrderBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    public List<WorkOrderVO> getOpenWorkOrdersList() {
        return openWorkOrdersList;
    }

    public List<WorkOrderVO> getWorkOrderTrendsList(WorkOrderVO workOrderVO) {
        return workOrderVO != null ? workOrderTrendsMap.get(workOrderVO) : null;
    }

    public void showCreatePage() {
        navigationBean.updateDialog("alarm-history");
    }

    public Map<UUID, List<WorkOrderVO>> getWorkOrderCountsMap() {
        return workOrderCountsMap;
    }

    public List<WorkOrderVO> getAreaTileList() {
        return areaTileList;
    }

    private void resetDataTable() {
        DataTable datatable;

        try {
            if ((datatable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("workOrderResultFormId:workOrderResultTableId")) != null) {
                datatable.reset();
            }
        } catch (Exception e) {
            Logger.getLogger(UserManageBean.class.getName()).log(Level.SEVERE, "***Error occured*** {0} ",e.getMessage());
        }
    }

    public String getActionLabelByValue(String value) {
        return WorkOrderActionEnum.getActionLabelByValue(value);
    }

    public String getIssueLabelByValue(String value) {
        return WorkOrderIssueEnum.getIssueLabelByValue(value);
    }
}
