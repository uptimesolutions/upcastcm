/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.SiteClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import static com.uptime.upcastcm.utils.enums.ContactRoleEnum.getRoleItemList;
import static com.uptime.upcastcm.utils.enums.CountryRegionEnum.getCountryList;
import static com.uptime.upcastcm.utils.enums.CountryRegionEnum.getRegionByCountry;
import static com.uptime.upcastcm.utils.enums.IndustryEnum.getIndustryItemList;
import com.uptime.upcastcm.utils.vo.SiteVO;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import com.uptime.upcastcm.utils.helperclass.Filters;
import com.uptime.upcastcm.utils.vo.SiteContactVO;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.primefaces.PrimeFaces;
import com.uptime.upcastcm.utils.interfaces.PresetDialogFields;
import java.util.Map;
import java.util.stream.Collectors;
import javax.faces.model.SelectItem;

/**
 *
 * @author kpati
 */
public class CreateSiteBean implements Serializable, PresetDialogFields {
    private UserManageBean userManageBean;
    private boolean samePhyscAddShipChk, samePhyscAddBillChk;
    private List<String> countries;
    private List<SelectItem> industries, roles;
    private List<SiteContactVO> contactList;
    private SiteVO siteVo;
    private SiteContactVO siteContactVO;

    /**
     * Constructor
     */
    public CreateSiteBean() {
        try {
            userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
            countries = getCountryList(true);
            industries = getIndustryItemList();
            roles = getRoleItemList();
        } catch (Exception e) {
            Logger.getLogger(CreateSiteBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            userManageBean = null;
        }

    }

    /**
     * Post Constructor
     */
    @PostConstruct
    public void init() {
        resetPage();
        if (userManageBean != null && userManageBean.getSelectedCustomersVO() != null && userManageBean.getSelectedCustomersVO().getCustomerAccount() != null) {
            Logger.getLogger(CreateSiteBean.class.getName()).log(Level.INFO, "**customerAccount : {0}", userManageBean.getSelectedCustomersVO().getCustomerAccount());
        }

    }

    @Override
    public void resetPage() {
        siteVo = new SiteVO();
        contactList = new ArrayList();

        samePhyscAddShipChk = false;
        samePhyscAddBillChk = false;
        
        if (userManageBean != null && userManageBean.getSelectedCustomersVO() != null && userManageBean.getSelectedCustomersVO().getCustomerAccount() != null) {
            siteVo.setCustomerAccount(userManageBean.getSelectedCustomersVO().getCustomerAccount());
        }
    }

    @Override
    public void presetFields(Filters presets, Object object) {
        if (presets != null) {
            siteVo.setRegion(presets.getRegion());
            siteVo.setCountry(presets.getCountry());
        }
    }

    @Override
    public void presetFields(int operationType, Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void presetFields(Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void onCountryChange() {
        this.siteVo.setRegion(getRegionByCountry(this.siteVo.getCountry(), true));
    }

    public void getLatLongByGeoLocation() {
        SiteVO latLongSiteVo;
        
        try {
            latLongSiteVo = SiteClient.getInstance().getLatLongByPhysicalAddress(this.siteVo.getPhysicalAddress1(), this.siteVo.getPhysicalCity(), this.siteVo.getPhysicalStateProvince(), this.siteVo.getPhysicalPostalCode());
            siteVo.setLatitude(latLongSiteVo.getLatitude());
            siteVo.setLongitude(latLongSiteVo.getLongitude());
        } catch (Exception e) {
            Logger.getLogger(CreateSiteBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    public void populatePhyscAddForShipp() {
        if (samePhyscAddShipChk) {
            siteVo.setShipAddress1(siteVo.getPhysicalAddress1());
            siteVo.setShipAddress2(siteVo.getPhysicalAddress2());
            siteVo.setShipCity(siteVo.getPhysicalCity());
            siteVo.setShipStateProvince(siteVo.getPhysicalStateProvince());
            siteVo.setShipPostalCode(siteVo.getPhysicalPostalCode());
        } else {
            siteVo.setShipAddress1(null);
            siteVo.setShipAddress2(null);
            siteVo.setShipCity(null);
            siteVo.setShipStateProvince(null);
            siteVo.setShipPostalCode(null);
        }
    }

    public void populatePhyscAddForBill() {
        if (samePhyscAddBillChk) {
            siteVo.setBillingAddress1(siteVo.getPhysicalAddress1());
            siteVo.setBillingAddress2(siteVo.getPhysicalAddress2());
            siteVo.setBillingCity(siteVo.getPhysicalCity());
            siteVo.setBillingStateProvince(siteVo.getPhysicalStateProvince());
            siteVo.setBillingPostalCode(siteVo.getPhysicalPostalCode());
        } else {
            siteVo.setBillingAddress1(null);
            siteVo.setBillingAddress2(null);
            siteVo.setBillingCity(null);
            siteVo.setBillingStateProvince(null);
            siteVo.setBillingPostalCode(null);
        }
    }


    public void updateBean() {
        Logger.getLogger(CreateSiteBean.class.getName()).log(Level.SEVERE, "************updateBean************");
    }

    /**
     * Add one new contact
     */
    public void onAddNewRow() {
        SiteContactVO newSiteContactVO;
        
        newSiteContactVO = new SiteContactVO();
        newSiteContactVO.setCustomerAccount(siteVo.getCustomerAccount().trim().toUpperCase());
        newSiteContactVO.setSiteName(siteVo.getSiteName().trim().toUpperCase());
        contactList.add(newSiteContactVO);
        siteVo.setSiteContactList(contactList);
    }

    /**
     * Delete the given contact
     * @param siteContactVO, SiteContactVO Object
     */
    public void onDeleteRow(SiteContactVO siteContactVO) {
        contactList.remove(siteContactVO);
        siteVo.setSiteContactList(contactList);
    }

    public void createSite() {
        Map<String, Long> couterMap;
        int startIndex, endIndex;
        String subString, modified, timeZone = null;
        Long value;
        
        try {
            if (userManageBean != null && userManageBean.getSelectedCustomersVO() != null && userManageBean.getSelectedCustomersVO().getCustomerAccount() != null) {
                siteVo.setSiteName(siteVo.getSiteName().trim().toUpperCase());
                siteVo.setCustomerAccount(userManageBean.getSelectedCustomersVO().getCustomerAccount());
                siteVo.setCountry(siteVo.getCountry().trim().toUpperCase());
                siteVo.setRegion(siteVo.getRegion().trim().toUpperCase());
                
                if (siteVo.getTimezone() != null && !siteVo.getTimezone().isEmpty() &&
                        siteVo.getTimezone().contains("(") && siteVo.getTimezone().contains(")")) {
                    timeZone = siteVo.getTimezone();
                    startIndex = timeZone.indexOf("(");
                    endIndex = timeZone.indexOf(")");
                    subString = timeZone.substring(startIndex, endIndex + 1);
                    modified = timeZone.replace(subString, "").trim();
                    
                    siteVo.setTimezone(modified);
                }

                if (siteVo.getSiteContactList() != null && !siteVo.getSiteContactList().isEmpty()) {
                    if (siteVo.getSiteContactList().stream().filter(c -> c.getContactEmail().isEmpty() && c.getContactPhone().isEmpty()).findFirst().orElse(null) != null) {
                        FacesContext.getCurrentInstance().addMessage("growlMessages", new FacesMessage(userManageBean.getResourceBundleString("growl_create_site_validate_contact")));
                        return;
                    } else {
                        couterMap = siteVo.getSiteContactList().stream().collect(Collectors.groupingBy(e -> e.getRole(), Collectors.counting()));
                        for (Map.Entry<String, Long> entry : couterMap.entrySet()) {
                            value = entry.getValue();

                            if (value > 1) {
                                FacesContext.getCurrentInstance().addMessage("growlMessages", new FacesMessage(userManageBean.getResourceBundleString("growl_validate_contact_role")));
                                return;
                            }
                        }
                        siteVo.getSiteContactList().forEach(c -> {
                            c.setContactName(c.getContactName().trim().toUpperCase());
                            c.setRole(c.getRole().trim().toUpperCase());
                        });
                    }
                }

                if (SiteClient.getInstance().createSite(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), siteVo)) {
                    userManageBean.setRegionCountrySiteMap();
                    PrimeFaces.current().ajax().update("topbarFilterFormId");
                    PrimeFaces.current().executeScript("PF('defaultDlg').hide()");
                } else {
                    if (timeZone != null) {
                        siteVo.setTimezone(timeZone);
                    }
                    Logger.getLogger(CreateSiteBean.class.getName()).log(Level.INFO, "siteVo.getSiteName()*********** {0}", new Object[]{siteVo.toString()});
                }
            }
        } catch (Exception e) {
            Logger.getLogger(CreateSiteBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            if (timeZone != null) {
                siteVo.setTimezone(timeZone);
            }
        }
    }

    public List<String> getCountries() {
        return countries;
    }

    public boolean isSamePhyscAddShipChk() {
        return samePhyscAddShipChk;
    }

    public void setSamePhyscAddShipChk(boolean samePhyscAddShipChk) {
        this.samePhyscAddShipChk = samePhyscAddShipChk;
    }

    public boolean isSamePhyscAddBillChk() {
        return samePhyscAddBillChk;
    }

    public void setSamePhyscAddBillChk(boolean samePhyscAddBillChk) {
        this.samePhyscAddBillChk = samePhyscAddBillChk;
    }

    public List<String> getTimezoneList() {
        try {
            if (userManageBean != null && userManageBean.getTimezoneList() != null) {
                return userManageBean.getTimezoneList().stream().filter(item -> !item.equalsIgnoreCase(TIME_ZONE_SITE_DEFAULT)).collect(Collectors.toList());
            }
        } catch (Exception e) {
            Logger.getLogger(CreateAssetBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
        return new ArrayList();
    }

    public SiteVO getSiteVo() {
        return siteVo;
    }

    public void setSiteVo(SiteVO siteVo) {
        this.siteVo = siteVo;
    }

    public SiteContactVO getSiteContactVO() {
        return siteContactVO;
    }

    public void setSiteContactVO(SiteContactVO siteContactVO) {
        this.siteContactVO = siteContactVO;
    }

    public List<SelectItem> getIndustries() {
        return industries;
    }

    public List<SelectItem> getRoles() {
        return roles;
    }

    public List<SiteContactVO> getContactList() {
        return contactList;
    }

    public void setContactList(List<SiteContactVO> contactList) {
        this.contactList = contactList;
    }

}
