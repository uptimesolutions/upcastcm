/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.APALSetClient;
import com.uptime.upcastcm.http.client.AreaClient;
import com.uptime.upcastcm.http.client.AssetClient;
import com.uptime.upcastcm.http.client.FaultFrequenciesClient;
import com.uptime.upcastcm.http.client.TachometerClient;
import com.uptime.upcastcm.http.utils.json.dom.PointLocationDomJsonParser;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import static com.uptime.upcastcm.utils.enums.ACSensorTypeEnum.getACSensorTypeItemList;
import static com.uptime.upcastcm.utils.enums.DCSensorTypeEnum.getDCSensorTypeItemList;
import static com.uptime.upcastcm.utils.enums.DwellEnum.getDwellItemList;
import static com.uptime.upcastcm.utils.enums.FmaxEnum.getFmaxItemList;
import static com.uptime.upcastcm.utils.enums.ParamUnitEnum.getACParamUnitItemListBySensorTypeParamType;
import static com.uptime.upcastcm.utils.enums.ParamUnitEnum.getDCParamUnitItemListBySensorType;
import static com.uptime.upcastcm.utils.enums.ResolutionEnum.getResolutionItemList;
import static com.uptime.upcastcm.utils.enums.RollDiameterUnitEnum.getRollDiameterUnitItemList;
import static com.uptime.upcastcm.utils.enums.TS1ACChannelNumberEnum.getTS1ACChannelNumberList;
import static com.uptime.upcastcm.utils.enums.TS1DCChannelNumberEnum.getTS1DCChannelNumberList;
import com.uptime.upcastcm.utils.helperclass.Utils;
import com.uptime.upcastcm.utils.vo.AreaVO;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import com.uptime.upcastcm.utils.singletons.CommonUtilSingleton;
import com.uptime.upcastcm.utils.vo.ApAlSetVO;
import com.uptime.upcastcm.utils.vo.AssetVO;
import com.uptime.upcastcm.utils.vo.ChannelVO;
import com.uptime.upcastcm.utils.vo.FaultFrequenciesVO;
import com.uptime.upcastcm.utils.vo.TachometerVO;
import java.util.Comparator;
import java.util.Map;
import java.util.UUID;
import java.util.function.Function;
import javax.faces.model.SelectItem;
import org.primefaces.PrimeFaces;

/**
 *
 * @author madhavi
 */
public class AddChannelBean implements Serializable {
    private ChannelVO channelVO;
    private List<ChannelVO> channelVOList;
    private List<SelectItem> acChannelNums, dcChannelNums, customerApSetVOList, dcSensorTypeList, acSensorTypeList, areaList, assetList, pointLocationNameList, faultFrequenciesList, tachometersList, rollDiameterUnitsList, alSetVOList, fmaxList, dwellList, resolutionList;
    private UserManageBean userManageBean;
    private List<ApAlSetVO> apAlSetsVOList;
    private Map<UUID, ApAlSetVO> customerApSetVOMap;
    private boolean tachValidationFailed;
    private int errorRow = -1;
    private List<FaultFrequenciesVO> siteFFSetFavorites;

    /**
     * Constructor
     */
    public AddChannelBean() {
        areaList = new ArrayList();
        pointLocationNameList = new ArrayList();
        customerApSetVOList = new ArrayList();
        alSetVOList = new ArrayList();
        apAlSetsVOList = new ArrayList();
        faultFrequenciesList = new ArrayList();
        tachometersList = new ArrayList();
        acChannelNums = new ArrayList();
        dcChannelNums = new ArrayList();
        channelVOList = new ArrayList();

        try {
            rollDiameterUnitsList = getRollDiameterUnitItemList();
            dcSensorTypeList = getDCSensorTypeItemList();
            acSensorTypeList = getACSensorTypeItemList();
            fmaxList = getFmaxItemList();
            dwellList = getDwellItemList();
            resolutionList = getResolutionItemList();
            userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
        } catch (Exception e) {
            Logger.getLogger(AddChannelBean.class.getName()).log(Level.SEVERE, "AddChannelBean Constructor Exception {0}", e.toString());
            userManageBean = null;
        }
    }

    /**
     * Post Constructor
     */
    @PostConstruct
    public void init() {
        resetPage();
    }

    /**
     * Resets the page and clears all previous entered values
     */
    public void resetPage() {
        channelVO = new ChannelVO();
        channelVO.setNewChannel(true);
        channelVO.setPointType("AC");
        channelVO.setAlarmEnabled(false);
        channelVO.setApAlSetVOs(new ArrayList());
        pointLocationNameList = new ArrayList();
        faultFrequenciesList = new ArrayList();
        tachometersList = new ArrayList();
        areaList = new ArrayList();
        channelVOList = new ArrayList();
        tachValidationFailed = false;
    }

    /**
     * Preset fields needed for this bean
     *
     * @param channelVOList, List Object of ChannelVO Objects
     */
    public void presetFields(List<ChannelVO> channelVOList) {
        try {
            resetPage();
            this.channelVOList = channelVOList;
            acChannelNums = new ArrayList();
            dcChannelNums = new ArrayList();
            siteFFSetFavorites = new ArrayList();

            getTS1ACChannelNumberList().stream().forEachOrdered(item -> {
                if (!(channelVOList != null && !channelVOList.isEmpty() && channelVOList.stream().anyMatch(vo -> vo.getPointType().equalsIgnoreCase("AC") && vo.getSensorChannelNum() == (Integer) item.getValue()))) {
                    acChannelNums.add(item);
                }
            });

            getTS1DCChannelNumberList().stream().forEachOrdered(item -> {
                if (!(channelVOList != null && !channelVOList.isEmpty() && channelVOList.stream().anyMatch(vo -> vo.getPointType().equalsIgnoreCase("DC") && vo.getSensorChannelNum() == (Integer) item.getValue()))) {
                    dcChannelNums.add(item);
                }
            });

            if (userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null && userManageBean.getCurrentSite().getSiteId() != null) {
                channelVO.setCustomerAccount(userManageBean.getCurrentSite().getCustomerAccount());
                channelVO.setSiteId(userManageBean.getCurrentSite().getSiteId());
                channelVO.setSiteName(userManageBean.getCurrentSite().getSiteName());

                // get point location name presets
                pointLocationNameList.addAll(userManageBean.getPresetUtil().populatePtLoNamesList(userManageBean, userManageBean.getCurrentSite().getCustomerAccount(), userManageBean.getCurrentSite().getSiteId()));

                // get fault frequencies presets
                siteFFSetFavorites = FaultFrequenciesClient.getInstance().getSiteFFSetFavoritesVOList(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), userManageBean.getCurrentSite().getSiteId());
                faultFrequenciesList.addAll(userManageBean.getPresetUtil().populateFFSetNameList(userManageBean, siteFFSetFavorites));
                
                // get tachometer presets
                tachometersList.addAll(userManageBean.getPresetUtil().populateTachometerList(userManageBean, userManageBean.getCurrentSite().getCustomerAccount(), userManageBean.getCurrentSite().getSiteId()));

                // set areaList field
                AreaClient.getInstance().getAreaSiteByCustomerSite(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), userManageBean.getCurrentSite().getSiteId()).stream().sorted(Comparator.comparing(AreaVO::getAreaName)).forEachOrdered(area -> areaList.add(new SelectItem(area.getAreaId(), area.getAreaName())));
            }
        } catch (Exception e) {
            Logger.getLogger(AddChannelBean.class.getName()).log(Level.SEVERE, "presetFields Exception {0}", e.toString());
            resetPage();
        }
    }

    /**
     * Action after a value is set/selected in the xhtml page
     */
    public void updateBean() {
        Logger.getLogger(AddChannelBean.class.getName()).log(Level.INFO, "channelVO.getPointType()*********** {0}", new Object[]{channelVO.getPointType()});
    }

    /**
     * Action after a Point Type is selected
     */
    public void onPointTypeChange() {
        customerApSetVOList = new ArrayList();
        apAlSetsVOList = new ArrayList();
        alSetVOList = new ArrayList();

        if (channelVO != null) {
            channelVO.setSensorType(null);
            channelVO.setApSetId(null);
            channelVO.setApSetName(null);
            channelVO.setAlSetId(null);
            channelVO.setAlSetName(null);
            channelVO.setCustomized(false);
        }
    }

    /**
     * Action after a Sensor Type is selected
     */
    public void onSensorSelect() {
        Logger.getLogger(AddChannelBean.class.getName()).log(Level.INFO, "AddChannelBean.onSensorSelect() - {0}", channelVO.getSensorType());
        List<SelectItem> selectItemList;
        List<ApAlSetVO> apAlSetBySensorList;

        try {
            customerApSetVOList = new ArrayList();
            apAlSetsVOList = new ArrayList();
            alSetVOList = new ArrayList();

            if (userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null && channelVO != null && channelVO.getSensorType() != null && !channelVO.getSensorType().isEmpty()) {
                userManageBean.getPresetUtil().clearGlobalApAlSetSensorList();
                userManageBean.getPresetUtil().clearSiteApAlSetSensorList();

                channelVO.setCustomized(false);
                channelVO.setApSetId(null);
                channelVO.setApSetName(null);
                channelVO.setAlSetId(null);
                channelVO.setAlSetName(null);

                if ((selectItemList = userManageBean.getPresetUtil().populateApSetSensorTypeList(userManageBean, userManageBean.getCurrentSite().getCustomerAccount(), channelVO.getSiteId(), channelVO.getSensorType())) != null && !selectItemList.isEmpty()) {
                    customerApSetVOList.addAll(selectItemList);
                }

                apAlSetBySensorList = new ArrayList();
                apAlSetBySensorList.addAll(Utils.getUniqueApAlSetList(userManageBean.getPresetUtil().getGlobalApAlSetsByCustomerSensorType(userManageBean.getCurrentSite().getCustomerAccount(), channelVO.getSensorType())));
                apAlSetBySensorList.addAll(Utils.getUniqueApAlSetList(userManageBean.getPresetUtil().getSiteApAlSetsByCustomerSiteIdSensorType(userManageBean.getCurrentSite().getCustomerAccount(), channelVO.getSiteId(), channelVO.getSensorType())));
                customerApSetVOMap = apAlSetBySensorList.stream().collect(Collectors.toMap(ApAlSetVO::getApSetId, Function.identity()));
            }
        } catch (Exception e) {
            Logger.getLogger(AddChannelBean.class.getName()).log(Level.SEVERE, "AddChannelBean Constructor Exception {0}", e.toString());
        }
    }

    /**
     * Action after an area is selected
     */
    public void onAreaChange() {
        List<AssetVO> assetVOList;

        try {
            assetList = new ArrayList();

            if (channelVO != null) {
                channelVO.setAssetId(null);
                channelVO.setAssetName(null);

                if (channelVO.getAreaId() != null) {
                    channelVO.setAreaName(areaList.stream().filter(item -> channelVO.getAreaId().equals((UUID) item.getValue())).map(SelectItem::getLabel).findFirst().get());
                    assetVOList = AssetClient.getInstance().getAssetSiteAreaByCustomerSiteArea(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), channelVO.getSiteId(), channelVO.getAreaId());
                    assetVOList.stream().sorted(Comparator.comparing(AssetVO::getAssetName)).forEachOrdered(asset -> assetList.add(new SelectItem(asset.getAssetId(), asset.getAssetName())));
                } else {
                    channelVO.setAreaName(null);
                }
            }
        } catch (Exception e) {
            Logger.getLogger(AddChannelBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);

            assetList = new ArrayList();
            if (channelVO != null) {
                channelVO.setAreaName(null);
                channelVO.setAreaId(null);
                channelVO.setAssetId(null);
                channelVO.setAssetName(null);
            }
        }
    }

    /**
     * Action after an asset is selected
     */
    public void onAssetChange() {
        try {
            if (channelVO != null) {
                if (channelVO.getAssetId() != null) {
                    channelVO.setAssetName(assetList.stream().filter(item -> channelVO.getAssetId().equals((UUID) item.getValue())).map(SelectItem::getLabel).findFirst().get());
                } else {
                    channelVO.setAssetName(null);
                }
            }
        } catch (Exception e) {
            Logger.getLogger(AddChannelBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            if (channelVO != null) {
                channelVO.setAssetId(null);
                channelVO.setAssetName(null);
            }
        }
    }

    /**
     * Action after a point location is selected
     */
    public void onPointLocationChange() {
        if (channelVO != null && channelVO.getPointLocationName() != null && channelVO.getSensorType() != null) {
            channelVO.setPointName(channelVO.getPointLocationName() + " " + CommonUtilSingleton.getInstance().getPointNameBySensorType(channelVO.getSensorType()));
        }
    }

    /**
     * Action after a Tach is selected
     */
    public void onTachChange() {
        List<TachometerVO> tachList;

        try {
            if (channelVO != null) {
                if (channelVO.getTachId() != null) {
                    if ((tachList = TachometerClient.getInstance().getGlobalTachometersByPK(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), DEFAULT_UPTIME_ACCOUNT, channelVO.getTachId())) == null || tachList.isEmpty()) {
                        if ((tachList = TachometerClient.getInstance().getGlobalTachometersByPK(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), channelVO.getCustomerAccount(), channelVO.getTachId())) == null || tachList.isEmpty()) {
                            tachList = TachometerClient.getInstance().getSiteTachometersByPK(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), channelVO.getCustomerAccount(), channelVO.getSiteId(), channelVO.getTachId());
                        }
                    }
                    channelVO.setTachReferenceUnits((tachList != null && !tachList.isEmpty() && tachList.get(0).getTachReferenceUnits() != null) ? tachList.get(0).getTachReferenceUnits() : null);
                }

                channelVO.setSpeedRatio(1);
                channelVO.setRollDiameter(0);
                channelVO.setRollDiameterUnits(null);

                tachValidationFailed = false;
                if (channelVO.getApSetId() != null) {
                    if (apAlSetsVOList != null) {
                        apAlSetsVOList.stream().forEach(al -> {
                            if ((al.getFrequencyUnits() != null && al.getFrequencyUnits().equalsIgnoreCase("Orders")) || Utils.validateParamList().contains(al.getParamType())) {
                                if (channelVO.getTachId() == null) {
                                    tachValidationFailed = true;
                                }
                            }
                        });
                    }
                }
            }
        } catch (Exception e) {
            Logger.getLogger(AddChannelBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Action after an ApSet is selected
     */
    public void onApSetNameSelect() {
        Logger.getLogger(AddChannelBean.class.getName()).log(Level.INFO, "AddChannelBean.onApSetNameSelect() ApSetId- {0}", channelVO.getApSetId());
        List<SelectItem> selectItem;

        try {
            apAlSetsVOList = new ArrayList();
            alSetVOList = new ArrayList();

            if (channelVO != null) {
                channelVO.setAlSetId(null);
                channelVO.setAlSetName(null);
                channelVO.setApSetName(null);
                channelVO.getApAlSetVOs().clear();

                if (userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null) {
                    if (channelVO.getApSetId() != null) {
                        apAlSetsVOList = APALSetClient.getInstance().getSiteApAlSetsByCustomerSiteIdApSetId(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), channelVO.getSiteId(), channelVO.getApSetId());
                        if (apAlSetsVOList == null || apAlSetsVOList.isEmpty()) {
                            apAlSetsVOList = APALSetClient.getInstance().getGlobalApAlSetsByCustomerApSetId(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), channelVO.getApSetId());

                            if (apAlSetsVOList == null || apAlSetsVOList.isEmpty()) {
                                apAlSetsVOList = APALSetClient.getInstance().getGlobalApAlSetsByCustomerApSetId(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), DEFAULT_UPTIME_ACCOUNT, channelVO.getApSetId());
                            }
                        }

                        if (apAlSetsVOList != null && !apAlSetsVOList.isEmpty()) {
                            Logger.getLogger(AddChannelBean.class.getName()).log(Level.INFO, "apAlSetsVOList size - {0}", apAlSetsVOList.size());

                            channelVO.setApSetName(apAlSetsVOList.get(0).getApSetName());
                            if ((selectItem = userManageBean.getPresetUtil().populateAlSetByApAlsetList(apAlSetsVOList)) != null && !selectItem.isEmpty()) {
                                alSetVOList.addAll(selectItem);
                            }
                        } else if (apAlSetsVOList == null || apAlSetsVOList.isEmpty()) {
                            FacesContext.getCurrentInstance().addMessage("growlMessages", new FacesMessage(userManageBean.getResourceBundleString("growl_create_point_3")));
                        }

                    }

                    tachValidationFailed = false;
                    if (apAlSetsVOList != null) {
                        apAlSetsVOList.stream().forEach(al -> {
                            if ((al.getFrequencyUnits() != null && al.getFrequencyUnits().equalsIgnoreCase("Orders")) || Utils.validateParamList().contains(al.getParamType())) {
                                if (channelVO.getTachId() == null) {
                                    tachValidationFailed = true;
                                }
                            }
                        });
                    }
                }
            }
        } catch (Exception e) {
            Logger.getLogger(AddChannelBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            apAlSetsVOList = new ArrayList();
            alSetVOList = new ArrayList();
        }
    }

    /**
     * Action after an AlSet is selected
     */
    public void onAlSetNameSelect() {
        List<ApAlSetVO> tempSelectedSiteApAlSetsVOList;

        if (apAlSetsVOList != null && !apAlSetsVOList.isEmpty() && channelVO != null && channelVO.getAlSetId() != null) {
            tempSelectedSiteApAlSetsVOList = apAlSetsVOList.stream().filter(a -> a.getAlSetId().compareTo(channelVO.getAlSetId()) == 0).collect(Collectors.toList());

            channelVO.getApAlSetVOs().clear();
            if (!tempSelectedSiteApAlSetsVOList.isEmpty()) {
                channelVO.setAlSetName(tempSelectedSiteApAlSetsVOList.get(0).getAlSetName());
                channelVO.setfMax(tempSelectedSiteApAlSetsVOList.get(0).getFmax());
                channelVO.setResolution(tempSelectedSiteApAlSetsVOList.get(0).getResolution());
                tempSelectedSiteApAlSetsVOList.forEach(apAlSetVo -> channelVO.getApAlSetVOs().add(new ApAlSetVO(apAlSetVo)));
            }
        }
    }

    /**
     * Action after submit button is clicked on the add channel overlay
     */
    public void submit() {
        try {
            if (channelVO != null) {
                channelVO.setFfSetIds(null);
                if (channelVO.getFfSetName() != null && !channelVO.getFfSetName().isEmpty()) {
                    channelVO.setFfSetIds(PointLocationDomJsonParser.getInstance().getJsonFromSelectedPointLocation(channelVO.getFfSetName(), siteFFSetFavorites));
                }            
                channelVOList.add(new ChannelVO(channelVO));
                
                channelVOList.forEach(channel -> {
                    if (channel.isAlarmEnabled()) {
                        channel.setAlarmEnabledValue(userManageBean.getResourceBundleString("label_true"));
                    } else {
                        channel.setAlarmEnabledValue(userManageBean.getResourceBundleString("label_false"));
                    }
                });
                
                PrimeFaces.current().ajax().update("addTS1FormId");
                PrimeFaces.current().ajax().update("editTS1FormId");
            }
        } catch (Exception e) {
            Logger.getLogger(AddChannelBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    public List<SelectItem> getACParamUnitList() {
        if (channelVO != null && channelVO.getSensorType() != null) {
            return getACParamUnitItemListBySensorTypeParamType(channelVO.getSensorType(), "waveform rms");
        }
        return new ArrayList();
    }

    public List<SelectItem> getDCParamUnitList() {
        if (channelVO != null && channelVO.getSensorType() != null) {
            return getDCParamUnitItemListBySensorType(channelVO.getSensorType());
        }
        return new ArrayList();
    }

    public List<ChannelVO> getChannelVOList() {
        return channelVOList;
    }

    public List<SelectItem> getCustomerApSetVOList() {
        return customerApSetVOList;
    }

    public void setCustomerApSetVOList(List<SelectItem> customerApSetVOList) {
        this.customerApSetVOList = customerApSetVOList;
    }

    public List<ApAlSetVO> getApAlSetsVOList() {
        return apAlSetsVOList;
    }

    public void setApAlSetsVOList(List<ApAlSetVO> apAlSetsVOList) {
        this.apAlSetsVOList = apAlSetsVOList;
    }

    public ChannelVO getChannelVO() {
        return channelVO;
    }

    public void setChannelVO(ChannelVO channelVO) {
        this.channelVO = channelVO;
    }

    public List<SelectItem> getAssetList() {
        return assetList;
    }

    public List<SelectItem> getFaultFrequenciesList() {
        return faultFrequenciesList;
    }

    public List<SelectItem> getTachometersList() {
        return tachometersList;
    }

    public List<SelectItem> getRollDiameterUnitsList() {
        return rollDiameterUnitsList;
    }

    public List<SelectItem> getAreaList() {
        return areaList;
    }

    public List<SelectItem> getPointLocationNameList() {
        return pointLocationNameList;
    }

    public List<SelectItem> getAlSetVOList() {
        return alSetVOList;
    }

    public void setAlSetVOList(List<SelectItem> alSetVOList) {
        this.alSetVOList = alSetVOList;
    }

    public Map<UUID, ApAlSetVO> getCustomerApSetVOMap() {
        return customerApSetVOMap;
    }

    public void setCustomerApSetVOMap(Map<UUID, ApAlSetVO> customerApSetVOMap) {
        this.customerApSetVOMap = customerApSetVOMap;
    }

    public List<SelectItem> getDcSensorTypeList() {
        return dcSensorTypeList;
    }

    public List<SelectItem> getAcSensorTypeList() {
        return acSensorTypeList;
    }

    public List<SelectItem> getFmaxList() {
        return fmaxList;
    }

    public List<SelectItem> getDwellList() {
        return dwellList;
    }

    public List<SelectItem> getResolutionList() {
        return resolutionList;
    }

    public boolean isTachValidationFailed() {
        return tachValidationFailed;
    }

    public void setTachValidationFailed(boolean tachValidationFailed) {
        this.tachValidationFailed = tachValidationFailed;
    }

    public int getErrorRow() {
        return errorRow;
    }

    public void setErrorRow(int errorRow) {
        this.errorRow = errorRow;
    }

    public List<SelectItem> getAcChannelNums() {
        return acChannelNums;
    }

    public List<SelectItem> getDcChannelNums() {
        return dcChannelNums;
    }
}
