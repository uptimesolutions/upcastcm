/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.LdapClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import com.uptime.upcastcm.utils.SessionUtils;
import com.uptime.upcastcm.utils.vo.LdapUserVO;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import org.primefaces.PrimeFaces;

/**
 *
 * @author kpati
 */
public class LoginBean implements Serializable {
    private LdapUserVO userVO, currentUserVO;
    private String custAccount, accountName;
    private final Map<String, String[]> requestParameterMap;

    /**
     * Constructor
     */
    public LoginBean() {
        custAccount = null;
        accountName = null;
        requestParameterMap = new HashMap();
        userVO = new LdapUserVO();
    }

    /**
     * Action during Login submit
     * @return String Object 
     */
    public String login() {
        HttpSession session;
        String returnValue = "";
        LoginBean instance;
        
        // lookup the user in Ldap server based on user name and user password
        try {
            Logger.getLogger(LoginBean.class.getName()).log(Level.INFO, "requestParameterMap.keySet(): {0}", requestParameterMap.keySet());
            
            // Set Customer Account if null and provided
            if (custAccount == null && requestParameterMap.containsKey("account_number")) {
                custAccount = requestParameterMap.get("account_number")[0];
                userVO.setCustomerAccount(custAccount);
                Logger.getLogger(LoginBean.class.getName()).log(Level.INFO, "LoginBean custAccount: {0}", custAccount);
            }
            
            // Set Customer Name if null and provided
            if (accountName == null && requestParameterMap.containsKey("account_name")) {
                accountName = requestParameterMap.get("account_name")[0];
                userVO.setCompany(accountName);
                Logger.getLogger(LoginBean.class.getName()).log(Level.INFO, "LoginBean accountName: {0}", accountName);
            }
            
            // Password and User Id are valid
            if (custAccount != null && !custAccount.isEmpty() && accountName != null && !accountName.isEmpty() && (currentUserVO = LdapClient.getInstance().validateAuthUsernamePassword(ServiceRegistrarClient.getInstance().getServiceHostURL(LDAP_SERVICE_NAME), userVO)) != null) {
                try {
                    Logger.getLogger(LoginBean.class.getName()).log(Level.INFO, "login successful for {0}", userVO.getUserId());
                    session = SessionUtils.getRequest().getSession(true);
                    session.setAttribute("nameId", currentUserVO.getUserId());
                    session.setAttribute("displayName", currentUserVO.getFullName());
                    session.setAttribute("company", accountName);
                    session.setAttribute("accountNumber", custAccount);
                    
                    if (currentUserVO.getGroups() != null && !currentUserVO.getGroups().isEmpty()) {
                        session.setAttribute("globalGroupList", currentUserVO.getGroups());
                    }
                    
                    if(requestParameterMap.get("page") != null && requestParameterMap.get("page").length > 0){
                        requestParameterMap.keySet().stream().filter(key -> key.equalsIgnoreCase("page") || key.contains("Id")).forEachOrdered(key -> session.setAttribute(key, requestParameterMap.get(key)[0]));
                        requestParameterMap.clear();
                    }
                    
                    returnValue = HOME_PAGE_REDIRECT;
                    
                } catch (Exception e) {
                    Logger.getLogger(LoginBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
                    
                    // Login failed try starting new session
                    Logger.getLogger(LoginBean.class.getName()).log(Level.INFO, "login failed for {0}", custAccount);
                    Logger.getLogger(LogoutBean.class.getName()).log(Level.INFO, "Old Session id {0}", SessionUtils.getRequest().getSession().getId());
                    instance = (LoginBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("loginBean"); 

                    FacesContext.getCurrentInstance().addMessage("loginMessages", new FacesMessage("Exception occured during login. Contact IT."));
                    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().clear();
                    SessionUtils.getSession().invalidate();
                    SessionUtils.getRequest().getSession(true);
                    SessionUtils.getRequest().getSession(true);
                    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("loginBean", instance);
                    
                }
            } 
            
            // Password and User Id aren't valid, but customer account and account name aren't null or empty
            // Start new session
            else if (custAccount != null && !custAccount.isEmpty() && accountName != null && !accountName.isEmpty()) {
                Logger.getLogger(LoginBean.class.getName()).log(Level.INFO, "login failed for {0}", custAccount);
                Logger.getLogger(LogoutBean.class.getName()).log(Level.INFO, "Old Session id {0}", SessionUtils.getRequest().getSession().getId());

                instance = (LoginBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("loginBean"); 

                FacesContext.getCurrentInstance().addMessage("loginMessages", new FacesMessage("The userid or password is incorrect."));
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().clear();
                SessionUtils.getSession().invalidate();
                SessionUtils.getRequest().getSession(true);
                SessionUtils.getRequest().getSession(true);
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("loginBean", instance);
            } 
            
            // customer account or account name are null or empty
            // Start new session and restart loginFilter
            else {
                Logger.getLogger(LoginBean.class.getName()).log(Level.INFO, "login failed");
                Logger.getLogger(LogoutBean.class.getName()).log(Level.INFO, "Old Session id {0}", SessionUtils.getRequest().getSession().getId());
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().clear();
                SessionUtils.getSession().invalidate();
                SessionUtils.getRequest().getSession(true);
                SessionUtils.getRequest().getSession(true);
                PrimeFaces.current().executeScript("document.location.reload();");
            }
            
        } catch (Exception e) {
            Logger.getLogger(LoginBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
        
        return returnValue;
    }

    public boolean isLoggedIn() {
        return currentUserVO != null;
    }

    public LdapUserVO getUserVO() {
        return userVO;
    }

    public void setUserVO(LdapUserVO userVO) {
        this.userVO = userVO;
    }

    public void setCurrentUserVO(LdapUserVO currentUserVO) {
        this.currentUserVO = currentUserVO;
    }

    public Map<String, String[]> getRequestParameterMap() {
        return requestParameterMap;
    }
}
