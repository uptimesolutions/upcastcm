/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.APALSetClient;
import com.uptime.upcastcm.http.client.AreaClient;
import com.uptime.upcastcm.http.client.AssetClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import static com.uptime.upcastcm.utils.enums.ACSensorTypeEnum.getACSensorTypeItemList;
import static com.uptime.upcastcm.utils.enums.DCSensorTypeEnum.getDCSensorTypeItemList;
import com.uptime.upcastcm.utils.helperclass.Filters;
import com.uptime.upcastcm.utils.vo.AreaVO;
import com.uptime.upcastcm.utils.vo.AssetVO;
import com.uptime.upcastcm.utils.vo.PointLocationVO;
import com.uptime.upcastcm.utils.vo.PointVO;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import com.uptime.upcastcm.utils.interfaces.PresetDialogFields;
import com.uptime.upcastcm.utils.vo.ApAlSetVO;
import com.uptime.upcastcm.utils.vo.AssetTypesVO;
import com.uptime.upcastcm.utils.vo.UserSitesVO;
import java.util.UUID;
import javax.faces.model.SelectItem;
import org.primefaces.PrimeFaces;

/**
 *
 * @author madhavi
 */
public class CreateAssetBean implements Serializable, PresetDialogFields {

    private UserManageBean userManageBean;
    private AssetVO assetVO;
    private List<SelectItem> acSensorTypeList, dcSensorTypeList;
    private String focusProperty;
    private List<SelectItem> customerApSetVOList;
    private ApAlSetVO apAlSet;
    private List<ApAlSetVO> siteApAlSetsVOList;
    private List<SelectItem> tachometerList, ptLoNamePresetList, faultFrequenciesList;
    private List<UserSitesVO> siteList;
    private List<AreaVO> areaList;
    private List<String> assetTypeList;

    /**
     * Constructor
     */
    public CreateAssetBean() {
        try {
            assetTypeList = new ArrayList();
            userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");

            List<AssetTypesVO> assetTypesVOList = new ArrayList();
            assetTypesVOList.addAll(AssetClient.getInstance().getGlobalAssetTypesByCustomer(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount()));
            if (!userManageBean.getCurrentSite().getCustomerAccount().equals(DEFAULT_UPTIME_ACCOUNT)) {
                assetTypesVOList.addAll(AssetClient.getInstance().getGlobalAssetTypesByCustomer(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), DEFAULT_UPTIME_ACCOUNT));
            }
            assetTypesVOList.stream().forEach(a -> assetTypeList.add(a.getAssetTypeName()));
            dcSensorTypeList = getDCSensorTypeItemList();
            acSensorTypeList = getACSensorTypeItemList();
        } catch (Exception e) {
            Logger.getLogger(CreateAssetBean.class.getName()).log(Level.SEVERE, "CreateAssetBean Constructor Exception {0}", e.toString());
            userManageBean = null;
        }
    }

    /**
     * Post Constructor
     */
    @PostConstruct
    public void init() {
        resetPage();
    }

    @Override
    public void resetPage() {
        tachometerList = new ArrayList();
        ptLoNamePresetList = new ArrayList();
        faultFrequenciesList = new ArrayList();
        customerApSetVOList = new ArrayList();
        siteList = new ArrayList(userManageBean.getUserSiteList());
        assetVO = new AssetVO();

        if (userManageBean != null && userManageBean.getCurrentSite() != null) {
            assetVO.setCustomerAccount(userManageBean.getCurrentSite().getCustomerAccount());
            assetVO.setSiteName(userManageBean.getCurrentSite().getSiteName());
            assetVO.setSiteId(userManageBean.getCurrentSite().getSiteId());
        }
        onSiteSelect();
        assetVO.setPointLocationList(new ArrayList<>());

        try {
            // get tachometer presets
            userManageBean.getPresetUtil().clearSiteTachList();
            tachometerList.addAll(userManageBean.getPresetUtil().populateTachometerList(userManageBean, assetVO.getCustomerAccount(), assetVO.getSiteId()));

            // get point locatin name presets
            userManageBean.getPresetUtil().clearSitePointLocationNameList();
            ptLoNamePresetList.addAll(userManageBean.getPresetUtil().populatePtLoNamesList(userManageBean, assetVO.getCustomerAccount(), assetVO.getSiteId()));

            // get fault frequencies presets
            userManageBean.getPresetUtil().clearSiteFaultFrequenciesList();
            //faultFrequenciesList.addAll(userManageBean.getPresetUtil().populateFFSetList(userManageBean, assetVO.getCustomerAccount(), assetVO.getSiteId()));
        } catch (Exception e) {
            Logger.getLogger(CreateAssetBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    @Override
    public void presetFields(Filters presets, Object object) {
        UserSitesVO userSitesVO;
        AreaVO areaVO;

        assetVO = new AssetVO();

        if (userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null) {
            assetVO.setCustomerAccount(userManageBean.getCurrentSite().getCustomerAccount());
            if (presets != null) {
                if (presets.getSite() != null) {
                    siteList = new ArrayList();
                    userSitesVO = new UserSitesVO();
                    userSitesVO.setSiteId((UUID) presets.getSite().getValue());
                    userSitesVO.setSiteName(presets.getSite().getLabel());
                    siteList.add(userSitesVO);

                    assetVO.setSiteName(userSitesVO.getSiteName());
                    assetVO.setSiteId(userSitesVO.getSiteId());
                }
                if (presets.getArea() != null) {
                    areaList = new ArrayList();
                    areaVO = new AreaVO();
                    areaVO.setAreaId((UUID) presets.getArea().getValue());
                    areaVO.setAreaName(presets.getArea().getLabel());
                    areaList.add(areaVO);

                    assetVO.setAreaName(areaVO.getAreaName());
                    assetVO.setAreaId(areaVO.getAreaId());
                }
                assetVO.setSampleInterval((short) 720);
            }
        }
    }

    @Override
    public void presetFields(int operationType, Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void presetFields(Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void onSiteSelect() {
        assetVO.setAreaId(null);

        tachometerList.clear();
        if (userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null) {
            areaList = AreaClient.getInstance().getAreaSiteByCustomerSite(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), assetVO.getSiteId());

            if (assetVO.getSiteId() != null) {
                try {
                    userManageBean.getPresetUtil().clearSiteTachList();
                    tachometerList.addAll(userManageBean.getPresetUtil().populateTachometerList(userManageBean, userManageBean.getCurrentSite().getCustomerAccount(), assetVO.getSiteId()));
                } catch (Exception e) {
                    Logger.getLogger(PresetsBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
                    tachometerList.clear();
                }
            }

            if (assetVO.getPointLocationList() != null && !assetVO.getPointLocationList().isEmpty()) {
                assetVO.getPointLocationList().stream().forEachOrdered(vo -> {
                    vo.setTachId(null);
                    vo.setTachName(null);
                });
            }
        }
    }

    public void updateBean() {
        if (assetVO.getSiteName() != null && !assetVO.getSiteName().isEmpty()
                && assetVO.getSiteName() != null && !assetVO.getSiteName().isEmpty()
                && assetVO.getAreaName() != null && !assetVO.getAreaName().isEmpty()
                && assetVO.getAssetType() != null && !assetVO.getAssetType().isEmpty()
                && assetVO.getAssetName() != null && !assetVO.getAssetName().isEmpty()
                && assetVO.getAssetId() != null
                && assetVO.getDescription() != null && !assetVO.getDescription().isEmpty()
                && assetVO.getSampleInterval() > 0) {
            focusProperty = "submitBtn";
        } else if (assetVO.getSiteName() != null && !assetVO.getSiteName().isEmpty()
                && assetVO.getSiteName() != null && !assetVO.getSiteName().isEmpty()
                && assetVO.getAreaName() != null && !assetVO.getAreaName().isEmpty()
                && assetVO.getAssetType() != null && !assetVO.getAssetType().isEmpty()
                && assetVO.getAssetName() != null && !assetVO.getAssetName().isEmpty()
                && assetVO.getAssetId() != null
                && assetVO.getDescription() != null && !assetVO.getDescription().isEmpty()
                && assetVO.getSampleInterval() <= 0) {
            focusProperty = "sampleInterval";
        } else if (assetVO.getSiteName() != null && !assetVO.getSiteName().isEmpty()
                && assetVO.getSiteName() != null && !assetVO.getSiteName().isEmpty()
                && assetVO.getAreaName() != null && !assetVO.getAreaName().isEmpty()
                && assetVO.getAssetType() != null && !assetVO.getAssetType().isEmpty()
                && assetVO.getAssetName() != null && !assetVO.getAssetName().isEmpty()
                && assetVO.getAssetId() != null) {
            focusProperty = "description";
        } else if (assetVO.getSiteName() != null && !assetVO.getSiteName().isEmpty()
                && assetVO.getSiteName() != null && !assetVO.getSiteName().isEmpty()
                && assetVO.getAreaName() != null && !assetVO.getAreaName().isEmpty()
                && assetVO.getAssetType() != null && !assetVO.getAssetType().isEmpty()
                && assetVO.getAssetName() != null && !assetVO.getAssetName().isEmpty()) {
            focusProperty = "assetId";
        } else if (assetVO.getSiteName() != null && !assetVO.getSiteName().isEmpty()
                && assetVO.getSiteName() != null && !assetVO.getSiteName().isEmpty()
                && assetVO.getAreaName() != null && !assetVO.getAreaName().isEmpty()
                && assetVO.getAssetType() != null && !assetVO.getAssetType().isEmpty()) {
            focusProperty = "assetName";
        } else if (assetVO.getSiteName() != null && !assetVO.getSiteName().isEmpty()
                && assetVO.getSiteName() != null && !assetVO.getSiteName().isEmpty()
                && assetVO.getAreaName() != null && !assetVO.getAreaName().isEmpty()) {
            focusProperty = "assetType";
        }
        if (assetVO.getPointLocationList() != null && !assetVO.getPointLocationList().isEmpty()) {

        }
    }

    public void onApSetNameSelect(PointVO pointVO) {
        apAlSet = new ApAlSetVO();

        if ((siteApAlSetsVOList = APALSetClient.getInstance().getSiteApAlSetsByCustomerSiteIdApSetId(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), assetVO.getCustomerAccount(), assetVO.getSiteId(), pointVO.getApSetId())) != null && !siteApAlSetsVOList.isEmpty()) {
            apAlSet = siteApAlSetsVOList.stream().filter(p -> (p.getApSetId().compareTo(pointVO.getApSetId()) == 0)).findFirst().orElse(null);
            pointVO.setApSetName(apAlSet.getApSetName());
        } else {
            pointVO.setApSetId(null);
            if (userManageBean != null) {
                FacesContext.getCurrentInstance().addMessage("growlMessages", new FacesMessage(userManageBean.getResourceBundleString("growl_create_point_3")));
            }
        }
    }

    public void onAlSetNameSelect(PointVO pointVO) {
        apAlSet = new ApAlSetVO();

        if (siteApAlSetsVOList != null && !siteApAlSetsVOList.isEmpty()) {
            apAlSet = siteApAlSetsVOList.stream().filter(p -> (p.getAlSetId().compareTo(pointVO.getAlSetId()) == 0)).findFirst().orElse(null);
            pointVO.setAlSetName(apAlSet.getAlSetName());
            pointVO.setDemodEnabled(apAlSet.isDemod());
            pointVO.setDemodHighPass(apAlSet.getDemodHighPass());
            pointVO.setDemodLowPass(apAlSet.getDemodLowPass());
            pointVO.setDwell(apAlSet.getDwell());
            pointVO.setfMax(apAlSet.getFmax());
            pointVO.setParamName(apAlSet.getParamName());
            pointVO.setParamType(apAlSet.getParamType());
            pointVO.setSensorType(apAlSet.getSensorType());
            Logger.getLogger(CreateAssetBean.class.getName()).log(Level.INFO, "onAddNewPointLocation apAlSet.getDemodHighPass()****: {0}*******apAlSet.getDemodLowPass():   {1}*******apAlSet.isDemod() {2}", new Object[]{apAlSet.getDemodHighPass(), apAlSet.getDemodLowPass(), apAlSet.isDemod()});
        }
    }

    /**
     * Add one new contact to the data-table
     */
    public void onAddNewPointLocation() {
        assetVO.getPointLocationList().add(new PointLocationVO());
        Logger.getLogger(CreateAssetBean.class.getName()).log(Level.INFO, "onAddNewPointLocation added new obj size: {0}", assetVO.getPointLocationList().size());

    }

    public void ondeletePointLocation(PointLocationVO plvo) {
        Logger.getLogger(CreateAssetBean.class.getName()).log(Level.INFO, "Before Removing Point Location Size:{0}", assetVO.getPointLocationList().size());
        assetVO.getPointLocationList().remove(plvo);

    }

    public void onAddNewPoint(PointLocationVO plvo) {
        PointVO pvo;

        if (plvo.getPointList() == null) {
            plvo.setPointList(new ArrayList());
        }

        pvo = new PointVO();
        pvo.setAlarmEnabled(plvo.isAlarmEnabled());
        pvo.setPointType("AC");
        plvo.getPointList().add(pvo);

        Logger.getLogger(CreateAssetBean.class.getName()).log(Level.INFO, "onAddNewPoint added new obj size: {0}", plvo.getPointList().size());
    }

    public void ondeletePoints(PointLocationVO plvo, PointVO pvo) {
        Logger.getLogger(CreateAreaBean.class.getName()).log(Level.INFO, "CreateAreaBean.ondeletePoints() called. ***********");
        List<PointVO> pvoList;

        if (plvo != null) {
            pvoList = plvo.getPointList();
            pvoList.remove(pvo);
        }
    }

    public void createAsset() {
        if (assetVO.getSiteId() != null) {
            Logger.getLogger(CreateAssetBean.class.getName()).log(Level.INFO, "Creating Asset {0}", new Object[]{assetVO.toString()});

            if (AssetClient.getInstance().createAsset(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), assetVO)) {
                Logger.getLogger(CreateAssetBean.class.getName()).log(Level.INFO, "Created Asset {0}", new Object[]{assetVO.toString()});
                FacesContext.getCurrentInstance().addMessage("nonAutoGrowlMessages", new FacesMessage(userManageBean.getResourceBundleString("growl_create_asset_2")));
               
                assetVO = new AssetVO();
                if (userManageBean != null && userManageBean.getCurrentSite() != null) {
                    assetVO.setCustomerAccount(userManageBean.getCurrentSite().getCustomerAccount());
                    assetVO.setSiteName(userManageBean.getCurrentSite().getSiteName());
                    assetVO.setSiteId(userManageBean.getCurrentSite().getSiteId());
                }
                assetVO.setPointLocationList(new ArrayList<>());

                if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("configurationBean") == null) {
                    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("configurationBean", new ConfigurationBean());
                    ((ConfigurationBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("configurationBean")).init();
                }
                ((ConfigurationBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("configurationBean")).onAreaChange(true);
                PrimeFaces.current().executeScript("PF('defaultDlg').hide()");
                PrimeFaces.current().executeScript("updateTreeNodes()");
            } else {
                Logger.getLogger(CreateAssetBean.class.getName()).log(Level.INFO, "assetVO*********** {0}", new Object[]{assetVO.toString()});
            }
        } else {
            Logger.getLogger(CreateAssetBean.class.getName()).log(Level.INFO, "asset id not set inassetVO*********** {0}", new Object[]{assetVO.toString()});
        }
    }

    public void onPointTypeChange(PointVO pvo, PointLocationVO plvo) {
        if (pvo != null && pvo.getPointType() != null && !pvo.getPointType().isEmpty()) {
            pvo.setSensorType(null);
            pvo.setSensorUnits(null);
            pvo.setApSetName(null);
            pvo.setApSetId(null);
            pvo.setAlSetId(null);
            pvo.setParamName(null);
            pvo.setfMax(0);
            updateBean();
        }
    }

    public void onSensorSelect(PointVO pvo, PointLocationVO plvo) {
        Logger.getLogger(CreateAssetBean.class.getName()).log(Level.INFO, "onSensorSelect() - {0}", pvo.getSensorType());
        List<SelectItem> selectItem;

        if (userManageBean != null && pvo.getSensorType() != null && !pvo.getSensorType().isEmpty()) {
            customerApSetVOList.clear();
            userManageBean.getPresetUtil().clearSiteApAlSetSensorList();
            if ((selectItem = userManageBean.getPresetUtil().populateApSetSensorTypeList(userManageBean, assetVO.getCustomerAccount(), assetVO.getSiteId(), pvo.getSensorType())) != null && !selectItem.isEmpty()) {
                customerApSetVOList.addAll(selectItem);
            }
        }
    }

    public void setAssetVO(AssetVO assetVO) {
        this.assetVO = assetVO;
    }

    public AssetVO getAssetVO() {
        return assetVO;
    }

    public List<UserSitesVO> getSiteList() {
        return siteList;
    }

    public String getFocusProperty() {
        return focusProperty;
    }

    public List<SelectItem> getCustomerApSetVOList() {
        return customerApSetVOList;
    }

    public List<ApAlSetVO> getSiteApAlSetsVOList() {
        return siteApAlSetsVOList;
    }

    public List<SelectItem> getTachometerList() {
        return tachometerList;
    }

    public List<SelectItem> getPtLoNamePresetList() {
        return ptLoNamePresetList;
    }

    public List<SelectItem> getFaultFrequenciesList() {
        return faultFrequenciesList;
    }

    public List<AreaVO> getAreaList() {
        return areaList;
    }

    public List<SelectItem> getAcSensorTypeList() {
        return acSensorTypeList;
    }

    public List<SelectItem> getDcSensorTypeList() {
        return dcSensorTypeList;
    }
    
     public List<String> getAssetTypeList() {
        return assetTypeList;
    }

}
