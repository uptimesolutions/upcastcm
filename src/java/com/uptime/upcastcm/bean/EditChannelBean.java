/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.FaultFrequenciesClient;
import com.uptime.upcastcm.http.client.TachometerClient;
import com.uptime.upcastcm.http.utils.json.dom.PointLocationDomJsonParser;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import static com.uptime.upcastcm.utils.enums.RollDiameterUnitEnum.getRollDiameterUnitItemList;
import com.uptime.upcastcm.utils.enums.TS1ACChannelNumberEnum;
import com.uptime.upcastcm.utils.enums.TS1DCChannelNumberEnum;
import com.uptime.upcastcm.utils.helperclass.Utils;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import javax.faces.context.FacesContext;
import com.uptime.upcastcm.utils.vo.ChannelVO;
import com.uptime.upcastcm.utils.vo.FaultFrequenciesVO;
import com.uptime.upcastcm.utils.vo.TachometerVO;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import javax.faces.model.SelectItem;
import org.primefaces.PrimeFaces;

/**
 *
 * @author twilcox
 */
public class EditChannelBean implements Serializable {

    private ChannelVO channelVO, originalChannelVO;
    private List<ChannelVO> channelVOList;
    private final List<SelectItem> faultFrequenciesList, tachometersList;
    private List<SelectItem> rollDiameterUnitsList;
    private UserManageBean userManageBean;
    private boolean tachValidationFailed;
    private List<FaultFrequenciesVO> siteFFSetFavorites;

    /**
     * Creates a new instance of EditChannelBean
     */
    public EditChannelBean() {
        faultFrequenciesList = new ArrayList();
        tachometersList = new ArrayList();

        try {
            rollDiameterUnitsList = getRollDiameterUnitItemList();
            userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
        } catch (Exception e) {
            Logger.getLogger(EditChannelBean.class.getName()).log(Level.SEVERE, "EditChannelBean Constructor Exception {0}", e.toString());
            userManageBean = null;
        }
    }

    /**
     * Post Constructor
     */
    @PostConstruct
    public void init() {
        resetPage();
    }

    public void resetPage() {
        channelVO = new ChannelVO();
        originalChannelVO = new ChannelVO();
        faultFrequenciesList.clear();
        tachometersList.clear();
        tachValidationFailed = false; // Need to verify this value
    }

    /**
     * Preset fields needed for this bean
     *
     * @param channelVO, ChannelVO Object
     * @param channelVOList, List Object of ChannelVO Objects
     */
    public void presetFields(ChannelVO channelVO, List<ChannelVO> channelVOList) {
        tachValidationFailed = false;
        faultFrequenciesList.clear();
        tachometersList.clear();
        try {
            if (channelVO != null && channelVOList != null && !channelVOList.isEmpty()) {
                this.channelVOList = channelVOList;
                if (channelVO.getApAlSetVOs() != null) {
                    channelVO.getApAlSetVOs().stream().forEach(al -> {
                        if ((al.getFrequencyUnits() != null && al.getFrequencyUnits().equalsIgnoreCase("Orders")) || Utils.validateParamList().contains(al.getParamType())) {
                            if (channelVO.getTachId() == null) {
                                tachValidationFailed = true;
                            }
                        }
                        if (channelVO.isAlarmEnabled()) {
                            channelVO.setAlarmEnabledValue(userManageBean.getResourceBundleString("label_true"));
                        } else {
                            channelVO.setAlarmEnabledValue(userManageBean.getResourceBundleString("label_false"));
                        }
                    });
                }
                if (userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null && userManageBean.getCurrentSite().getSiteId() != null) {
                    channelVO.setSiteName(userManageBean.getCurrentSite().getSiteName());
                    // get fault frequencies presets
                    // faultFrequenciesList.addAll(userManageBean.getPresetUtil().populateFFSetList(userManageBean, userManageBean.getCurrentSite().getCustomerAccount(), userManageBean.getCurrentSite().getSiteId()));
                    faultFrequenciesList.addAll(populateFFSetList());
                    Set<String> ffsetnames = new HashSet();
                    if (channelVO.getFfSetIds() != null) {
                        ffsetnames = PointLocationDomJsonParser.getInstance().populateFFSetFromJson(channelVO.getFfSetIds(),siteFFSetFavorites);
                    }
                    channelVO.setFfSetName(ffsetnames);
                    // get tachometer presets
                    tachometersList.addAll(userManageBean.getPresetUtil().populateTachometerList(userManageBean, userManageBean.getCurrentSite().getCustomerAccount(), userManageBean.getCurrentSite().getSiteId()));
                }
                originalChannelVO = channelVO;
                this.channelVO = new ChannelVO(originalChannelVO);
            }
        } catch (Exception e) {
            Logger.getLogger(EditChannelBean.class.getName()).log(Level.SEVERE, e.toString(), e);
        }
    }

    public List<SelectItem> populateFFSetList() {
        List<SelectItem> ffSetList = new ArrayList();
        siteFFSetFavorites = new ArrayList();
        try {
            siteFFSetFavorites = FaultFrequenciesClient.getInstance().getSiteFFSetFavoritesVOList(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), userManageBean.getCurrentSite().getSiteId());
            ffSetList = userManageBean.getPresetUtil().populateFFSetNameList(userManageBean,siteFFSetFavorites);
        } catch (Exception e) {
            Logger.getLogger(CreatePointLocationBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }

        return ffSetList;
    }

    /**
     * Action after a value is set/selected in the xhtml page
     */
    public void updateBean() {
        Logger.getLogger(EditChannelBean.class.getName()).log(Level.INFO, "EditChannelBean Update***********");
    }

    /**
     * Action after a Tach is selected
     */
    public void onTachChange() {
        List<TachometerVO> tachList;

        try {
            if (channelVO != null) {

                // set tachReferenceUnits if tachId != null
                if (channelVO.getTachId() != null) {
                    if ((tachList = TachometerClient.getInstance().getGlobalTachometersByPK(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), DEFAULT_UPTIME_ACCOUNT, channelVO.getTachId())) == null || tachList.isEmpty()) {
                        if ((tachList = TachometerClient.getInstance().getGlobalTachometersByPK(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), channelVO.getCustomerAccount(), channelVO.getTachId())) == null || tachList.isEmpty()) {
                            tachList = TachometerClient.getInstance().getSiteTachometersByPK(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), channelVO.getCustomerAccount(), channelVO.getSiteId(), channelVO.getTachId());
                        }
                    }
                    channelVO.setTachReferenceUnits((tachList != null && !tachList.isEmpty() && tachList.get(0).getTachReferenceUnits() != null) ? tachList.get(0).getTachReferenceUnits() : null);
                }

                channelVO.setSpeedRatio(1);
                channelVO.setRollDiameter(0);
                channelVO.setRollDiameterUnits(null);

                tachValidationFailed = false;
                if (channelVO.getApAlSetVOs() != null) {
                    channelVO.getApAlSetVOs().stream().forEach(al -> {
                        if ((al.getFrequencyUnits() != null && al.getFrequencyUnits().equalsIgnoreCase("Orders")) || Utils.validateParamList().contains(al.getParamType())) {
                            if (channelVO.getTachId() == null) {
                                tachValidationFailed = true;
                            }
                        }
                    });
                }
            }
        } catch (Exception e) {
            Logger.getLogger(EditChannelBean.class.getName()).log(Level.SEVERE, e.toString(), e);
        }
    }

    /**
     * Action after submit button is clicked on the add channel overlay
     */
    public void submit() {
        int index;

        try {
            index = channelVOList.indexOf(originalChannelVO);
            channelVOList.remove(originalChannelVO);
            channelVO.setFfSetIds(null);
            if (channelVO.getFfSetName() != null && !channelVO.getFfSetName().isEmpty()) {
                channelVO.setFfSetIds(PointLocationDomJsonParser.getInstance().getJsonFromSelectedPointLocation(channelVO.getFfSetName(), siteFFSetFavorites));
            }
            
            if (channelVO.isAlarmEnabled()) {
                channelVO.setAlarmEnabledValue(userManageBean.getResourceBundleString("label_true"));
            } else {
                channelVO.setAlarmEnabledValue(userManageBean.getResourceBundleString("label_false"));
            }

            channelVOList.add(index, channelVO);
            PrimeFaces.current().ajax().update("editTS1FormId");
        } catch (Exception e) {
            Logger.getLogger(EditChannelBean.class.getName()).log(Level.SEVERE, e.toString(), e);
        }
    }

    public ChannelVO getChannelVO() {
        return channelVO;
    }

    public List<SelectItem> getFaultFrequenciesList() {
        return faultFrequenciesList;
    }

    public List<SelectItem> getTachometersList() {
        return tachometersList;
    }

    public List<SelectItem> getRollDiameterUnitsList() {
        return rollDiameterUnitsList;
    }

    public boolean isTachValidationFailed() {
        return tachValidationFailed;
    }

    public String getTS1ACChannelNumberLabelByValue(int value) {
        return TS1ACChannelNumberEnum.getTS1ACChannelNumberLabelByValue(value);
    }

    public String getTS1DCChannelNumberLabelByValue(int value) {
        return TS1DCChannelNumberEnum.getTS1DCChannelNumberLabelByValue(value);
    }

    public List<FaultFrequenciesVO> getSiteFFSetFavorites() {
        return siteFFSetFavorites;
    }

}
