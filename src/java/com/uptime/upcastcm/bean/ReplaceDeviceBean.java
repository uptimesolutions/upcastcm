/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.featuresflag.enums.UptimeFeaturesEnum;
import com.uptime.upcastcm.http.client.APALSetClient;
import com.uptime.upcastcm.http.client.HwUnitToPointClient;
import com.uptime.upcastcm.http.client.ManageDeviceClient;
import com.uptime.upcastcm.http.client.PointClient;
import com.uptime.upcastcm.http.client.PointToHwUnitClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import com.uptime.upcastcm.utils.helperclass.Filters;
import com.uptime.upcastcm.utils.interfaces.PresetDialogFields;
import com.uptime.upcastcm.utils.singletons.CommonUtilSingleton;
import com.uptime.upcastcm.utils.vo.ApAlSetVO;
import com.uptime.upcastcm.utils.vo.DeviceVO;
import com.uptime.upcastcm.utils.vo.HwUnitVO;
import com.uptime.upcastcm.utils.vo.PointToHwVO;
import com.uptime.upcastcm.utils.vo.PointVO;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.primefaces.PrimeFaces;

/**
 *
 * @author kpati
 */
public class ReplaceDeviceBean implements Serializable, PresetDialogFields {

    private String serialNumberInput, serialNumber, deviceType, existingDeviceType, existingDeviceId, submitSuccess;
    private UserManageBean userManageBean;
    private DeviceVO replaceDeviceVO;
    private boolean invalidSmplIntrval, pointToHwUnitFound;
    private final List<PointVO> pointVOList;

    /**
     * Constructor
     */
    public ReplaceDeviceBean() {
        pointVOList = new ArrayList();

        try {
            userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
        } catch (Exception e) {
            Logger.getLogger(ReplaceDeviceBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            userManageBean = null;
        }
    }

    /**
     * Post Constructor
     */
    @PostConstruct
    public void init() {
        resetPage();
    }

    /**
     * Resets the values of this overlay page
     */
    @Override
    public void resetPage() {
        replaceDeviceVO = new DeviceVO();
        pointVOList.clear();
        serialNumber = null;
        serialNumberInput = null;
        existingDeviceId = "";
        existingDeviceType = null;
        deviceType = null;
        invalidSmplIntrval = false;
        pointToHwUnitFound = false;
        submitSuccess = "";

        if (userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null) {
            replaceDeviceVO.setCustomerAccount(userManageBean.getCurrentSite().getCustomerAccount());
        }
    }

    /**
     * Presets fields based on the given info
     *
     * @param presets, Object
     * @param object, Object
     */
    @Override
    public void presetFields(Filters presets, Object object) {
        List<PointToHwVO> pointToHwUnitVOs;

        try {
            if (presets != null && userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null) {
                if (presets.getSite() != null) {
                    replaceDeviceVO.setSiteId((UUID) presets.getSite().getValue());
                    replaceDeviceVO.setSiteName(presets.getSite().getLabel());
                }

                if (presets.getArea() != null) {
                    replaceDeviceVO.setAreaId((UUID) presets.getArea().getValue());
                    replaceDeviceVO.setAreaName(presets.getArea().getLabel());
                }

                if (presets.getAsset() != null) {
                    replaceDeviceVO.setAssetId((UUID) presets.getAsset().getValue());
                    replaceDeviceVO.setAssetName(presets.getAsset().getLabel());
                }

                if (presets.getPointLocation() != null) {
                    replaceDeviceVO.setPointLocationId((UUID) presets.getPointLocation().getValue());
                    replaceDeviceVO.setPointLocationName(presets.getPointLocation().getLabel());
                }

                if ((pointToHwUnitVOs = PointToHwUnitClient.getInstance().getPointToHwUnitByPointLocation(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), replaceDeviceVO.getSiteId(), replaceDeviceVO.getAreaId(), replaceDeviceVO.getAssetId(), replaceDeviceVO.getPointLocationId())) != null && !pointToHwUnitVOs.isEmpty()) {
                    for (PointToHwVO vo : pointToHwUnitVOs) {
                        if (vo.getDeviceId() != null && vo.getDeviceId().length() == 8) {
                            pointToHwUnitFound = true;
                            existingDeviceId = vo.getDeviceId();
                            existingDeviceType = CommonUtilSingleton.getInstance().getDeviceType(existingDeviceId);
                            break;
                        }
                    }
                }
            }
        } catch (Exception e) {
            Logger.getLogger(ReplaceDeviceBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            replaceDeviceVO = new DeviceVO();
            existingDeviceId = null;
            existingDeviceType = null;
            pointToHwUnitFound = false;
        }

    }

    /**
     * Presets fields based on the given info
     *
     * @param operationType, int
     * @param object, Object
     */
    @Override
    public void presetFields(int operationType, Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Presets fields based on the given info
     *
     * @param object, Object
     */
    @Override
    public void presetFields(Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * validate user input serial number.
     */
    public void validateSerialNum() {
        List<PointVO> pointsList;
        List<ApAlSetVO> apAlSetsVOList;
        final List<HwUnitVO> hwUnitVOlist;
        List<UUID> pointIds;
        HwUnitVO hwUnitVO;
        Pattern pattern;

        try {
            serialNumber = serialNumberInput;
            pointVOList.clear();
            replaceDeviceVO.setChannelType(null);
            replaceDeviceVO.setChannelNum(0);
            replaceDeviceVO.setPointId(null);
            replaceDeviceVO.setApSetId(null);

            Logger.getLogger(ReplaceDeviceBean.class.getName()).log(Level.INFO, "**serialNumber to be set : {0}", serialNumber);
            Logger.getLogger(ReplaceDeviceBean.class.getName()).log(Level.INFO, "**existingDeviceId to be set : {0}", existingDeviceId);
            if (userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null && existingDeviceId != null && !existingDeviceId.isEmpty()) {
                if (serialNumber.startsWith("001")) {
                    FacesContext.getCurrentInstance().addMessage("nonAutoGrowlMessages", new FacesMessage(userManageBean.getResourceBundleString("growl_install_device_validate")));
                } else {
                    if (UptimeFeaturesEnum.UptimeFeatures.DISPLAY_STORMX.isActive()) {
                        pattern = Pattern.compile(REGEX_INCLUDE_STORM_SERIAL_NUMBER);
                    } else {
                        pattern = Pattern.compile(REGEX_EXCLUDE_STORM_SERIAL_NUMBER);
                    }
                    if (serialNumber == null || !pattern.matcher(serialNumber).matches()) {
                        invalidSerialNumber();
                    } else {
                        deviceType = CommonUtilSingleton.getInstance().getDeviceType(serialNumber);

                        if (serialNumber.equals(existingDeviceId)) {
                            FacesContext.getCurrentInstance().addMessage("growlMessages", new FacesMessage(userManageBean.getResourceBundleString("growl_map_existing_2")));
                        } else if (!deviceType.equals(existingDeviceType)) {
                            FacesContext.getCurrentInstance().addMessage("growlMessages", new FacesMessage(userManageBean.getResourceBundleString("growl_match_devicetype_1")));
                        } else {
                            pointsList = new ArrayList();
                            hwUnitVOlist = new ArrayList();

                            //get the list of points for this pointLocation
                            pointsList.addAll(PointClient.getInstance().getPointsByCustomerSiteAreaAssetPointLocation(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), replaceDeviceVO.getSiteId(), replaceDeviceVO.getAreaId(), replaceDeviceVO.getAssetId(), replaceDeviceVO.getPointLocationId(), true));

                            //get the list of HwUnit for this deviceId associated to the pointLocation.
                            hwUnitVOlist.addAll(HwUnitToPointClient.getInstance().getHwUnitToPointByDevice(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), existingDeviceId));
                            pointIds = hwUnitVOlist.stream().map(HwUnitVO::getPointId).collect(Collectors.toList());

                            //check if points exist in the list of HwUnit
                            pointsList.forEach(points -> pointIds.stream().filter(pointId -> (points.getPointId().compareTo(pointId) == 0)).forEachOrdered(item -> pointVOList.add(points)));

                            if (!pointVOList.isEmpty()) {
                                CommonUtilSingleton.getInstance().sortPointsByPointType(pointVOList);
                                for (PointVO pointVO : pointVOList) {
                                    if ((apAlSetsVOList = APALSetClient.getInstance().getSiteApAlSetsByCustomerSiteIdApSetIdAlSetId(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), pointVO.getSiteId(), pointVO.getApSetId(), pointVO.getAlSetId())) == null || apAlSetsVOList.isEmpty()) {
                                        apAlSetsVOList = APALSetClient.getInstance().getGlobalApAlSetsByCustomerApSetIdAlSetId(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), pointVO.getApSetId(), pointVO.getAlSetId());
                                        if (apAlSetsVOList == null || apAlSetsVOList.isEmpty()) {
                                            apAlSetsVOList = APALSetClient.getInstance().getGlobalApAlSetsByCustomerApSetIdAlSetId(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), DEFAULT_UPTIME_ACCOUNT, pointVO.getApSetId(), pointVO.getAlSetId());
                                        }
                                    }
                                    if (apAlSetsVOList != null && !apAlSetsVOList.isEmpty()) {
                                        pointVO.setApSetName(apAlSetsVOList.get(0).getApSetName());
                                        pointVO.setAlSetName(apAlSetsVOList.get(0).getAlSetName());
                                    }
                                }

                                hwUnitVOlist.clear();
                                hwUnitVOlist.addAll(HwUnitToPointClient.getInstance().getHwUnitToPointByDevice(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), serialNumber));
                                if (!hwUnitVOlist.isEmpty()) {
                                    hwUnitVO = hwUnitVOlist.get(0);
                                    replaceDeviceVO.setChannelType(hwUnitVO.getChannelType());
                                    replaceDeviceVO.setChannelNum(hwUnitVO.getChannelNum());
                                    replaceDeviceVO.setPointId(hwUnitVO.getPointId());
                                    replaceDeviceVO.setApSetId(hwUnitVO.getApSetId());
                                    PrimeFaces.current().executeScript("PF('replaceDeviceDlg').show();");
                                } else {
                                    Logger.getLogger(ReplaceDeviceBean.class.getName()).log(Level.INFO, "*****serialNumber doesn't yet exist in hwunit_to_point table : {0}", serialNumber);
                                }
                            }
                        }
                    }
                }
            }

            PrimeFaces.current().ajax().update("replaceDeviceFormId");
        } catch (Exception e) {
            Logger.getLogger(ReplaceDeviceBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Reset values when Serial Number is changed
     */
    public void onSerialNumberChange() {
        if (!serialNumberInput.equalsIgnoreCase(serialNumber)) {
            replaceDeviceVO.setChannelType(null);
            replaceDeviceVO.setChannelNum(0);
            replaceDeviceVO.setPointId(null);
            replaceDeviceVO.setApSetId(null);
            invalidSmplIntrval = false;
            serialNumber = serialNumberInput;

            if (!pointVOList.isEmpty()) {
                pointVOList.clear();
                PrimeFaces.current().ajax().update("replaceDeviceFormId");
            }
        }
    }

    /**
     * Display message when Serial Number is invalid and reset values.
     */
    private void invalidSerialNumber() {
        onSerialNumberChange();
        FacesContext.getCurrentInstance().addMessage("growlMessages", new FacesMessage(userManageBean.getResourceBundleString("growl_valid_device_id")));
    }

    /**
     * called when use cancel to proceed further.
     */
    public void onCancel() {
        pointVOList.clear();
        replaceDeviceVO.setChannelType(null);
        replaceDeviceVO.setChannelNum(0);
        replaceDeviceVO.setPointId(null);
        replaceDeviceVO.setApSetId(null);
        invalidSmplIntrval = false;
        serialNumber = null;
        PrimeFaces.current().ajax().update("replaceDeviceFormId");
    }

    public void onConfirm() {
        if (deviceType != null && !deviceType.isEmpty()) {
            PrimeFaces.current().ajax().update("replaceDeviceFormId");
        }

    }

    public void updateBean() {
        Logger.getLogger(ReplaceDeviceBean.class.getName()).log(Level.INFO, "**updateBean called : {0}", existingDeviceId);
        if (pointToHwUnitFound) {
            PrimeFaces.current().executeScript("PF('noDeviceIdDlg').show()");
        }
    }

    /**
     * Submit the given info to Cassandra
     */
    public void submit() {
        Logger.getLogger(ReplaceDeviceBean.class.getName()).log(Level.INFO, "**serialNumber : {0} **existingDeviceId : {1}", new Object[]{serialNumber, existingDeviceId});
        Pattern pattern;

        if (UptimeFeaturesEnum.UptimeFeatures.DISPLAY_STORMX.isActive()) {
            pattern = Pattern.compile(REGEX_INCLUDE_STORM_SERIAL_NUMBER);
        } else {
            pattern = Pattern.compile(REGEX_EXCLUDE_STORM_SERIAL_NUMBER);
        }
        if (pattern.matcher(serialNumber).matches()) {
            if (replaceDeviceVO != null) {
                replaceDeviceVO.setExistingDeviceId(existingDeviceId);
                replaceDeviceVO.setNewDeviceId(serialNumber.toUpperCase());
                replaceDeviceVO.setPointList(pointVOList);

                if (ManageDeviceClient.getInstance().replaceDevice(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), replaceDeviceVO)) {
                    Logger.getLogger(CreateSiteBean.class.getName()).log(Level.INFO, "HwUnit, PointToHwUnit and siteBasestationDevice updated successfuly for existingDeviceId: {0} and serialNumber: {1}", new Object[]{existingDeviceId, serialNumber});

                    if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("configurationBean") == null) {
                        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("configurationBean", new ConfigurationBean());
                        ((ConfigurationBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("configurationBean")).init();
                    }
                    ((ConfigurationBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("configurationBean")).onPointLocationChange(true);
                    submitSuccess = "success";
                    PrimeFaces.current().executeScript("updateTreeNodes()");
                } else {
                    submitSuccess = "";
                    Logger.getLogger(CreateSiteBean.class.getName()).log(Level.INFO, "existingDeviceId*********** {0}", new Object[]{existingDeviceId});
                }
            } else {
                submitSuccess = "";
                Logger.getLogger(CreateSiteBean.class.getName()).log(Level.INFO, "***********Can't update data as {0}", "HwUnitToPoint didn't find");
            }
        } else {
            submitSuccess = "";
            Logger.getLogger(InstallDeviceBean.class.getName()).log(Level.INFO, "****onSerialNumberChange called**********{0}", serialNumber);
            invalidSerialNumber();
        }
    }

    public String getSerialNumberInput() {
        return serialNumberInput;
    }

    public void setSerialNumberInput(String serialNumberInput) {
        this.serialNumberInput = serialNumberInput;
    }

    public List<PointVO> getPointVOList() {
        return pointVOList;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public String getExistingDeviceId() {
        return existingDeviceId;
    }

    public void setExistingDeviceId(String existingDeviceId) {
        this.existingDeviceId = existingDeviceId;
    }

    public boolean isPointToHwUnitFound() {
        return pointToHwUnitFound;
    }

    public DeviceVO getReplaceDeviceVO() {
        return replaceDeviceVO;
    }

    public void setReplaceDeviceVO(DeviceVO replaceDeviceVO) {
        this.replaceDeviceVO = replaceDeviceVO;
    }

    public boolean isInvalidSmplIntrval() {
        return invalidSmplIntrval;
    }

    public String getSubmitSuccess() {
        return submitSuccess;
    }

    public void setSubmitSuccess(String submitSuccess) {
        this.submitSuccess = submitSuccess;
    }

}
