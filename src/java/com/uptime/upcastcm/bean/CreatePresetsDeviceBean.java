/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.featuresflag.enums.UptimeFeaturesEnum;
import com.uptime.upcastcm.http.client.APALSetClient;
import com.uptime.upcastcm.http.client.DevicePresetClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import static com.uptime.upcastcm.utils.enums.ACSensorTypeEnum.getACSensorTypeItemList;
import static com.uptime.upcastcm.utils.enums.DCSensorTypeEnum.getDCSensorTypeItemList;
import static com.uptime.upcastcm.utils.enums.DeviceTypeEnum.getDeviceTypeItemList;
import static com.uptime.upcastcm.utils.enums.ParamUnitEnum.getACParamUnitItemListBySensorTypeParamType;
import static com.uptime.upcastcm.utils.enums.ParamUnitEnum.getDCParamUnitItemListBySensorType;
import static com.uptime.upcastcm.utils.enums.ParamUnitEnum.getFrequencyPlusUnits;
import com.uptime.upcastcm.utils.helperclass.GenericMessage;
import static com.uptime.upcastcm.utils.helperclass.GenericMessage.setNonAutoGrowlMsg;
import com.uptime.upcastcm.utils.helperclass.Utils;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import com.uptime.upcastcm.utils.singletons.CommonUtilSingleton;
import com.uptime.upcastcm.utils.vo.ApAlSetVO;
import com.uptime.upcastcm.utils.vo.DevicePresetVO;
import com.uptime.upcastcm.utils.vo.DeviceVO;
import java.lang.reflect.Array;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import javax.faces.model.SelectItem;
import org.primefaces.PrimeFaces;

/**
 *
 * @author Joseph
 */
public class CreatePresetsDeviceBean implements Serializable {

    private List<DevicePresetVO> devicePresetsVOList;
    private boolean validationFailed = false, disable;
    private String setName, selectedDeviceType;
    private List<String> deviceTypeList;
    private UserManageBean userManageBean;
    private Map<String, List<ApAlSetVO>> deviceTypeMap;
    private Map<String, List<SelectItem>> selectDeviceTypeMap;
    private List<SelectItem> dcSensorTypeList, acSensorTypeList;
    private String siteOrGlobal;

    /**
     * Constructor
     */
    public CreatePresetsDeviceBean() {
        devicePresetsVOList = new ArrayList();
        deviceTypeMap = new HashMap();
        selectDeviceTypeMap = new HashMap();

        try {
            userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");

            //***************************For v1.0********************************
            // deviceTypeList = getDeviceTypeItemList();
            // For v1.0 only "MistLX" will be used.
            // For v1.5 adding "StormX".
            deviceTypeList = new ArrayList();
            if (UptimeFeaturesEnum.UptimeFeatures.DISPLAY_STORMX.isActive()) {
                deviceTypeList.add("MistLX");
                deviceTypeList.add("StormX");
            } else {
                deviceTypeList.add("MistLX");
            }

            //*******************************************************************
            dcSensorTypeList = getDCSensorTypeItemList();
            acSensorTypeList = getACSensorTypeItemList();
        } catch (Exception e) {
            Logger.getLogger(CreatePresetsDeviceBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            userManageBean = null;
        }
    }

    /**
     * Post Constructor
     */
    @PostConstruct
    public void init() {
        resetPage();
    }

    public void resetPage() {
        devicePresetsVOList = new ArrayList();
        deviceTypeMap = new HashMap();
        selectDeviceTypeMap = new HashMap();
        disable = false;
        setName = null;
        selectedDeviceType = null;
        siteOrGlobal = null;
    }

    public void updateBean() {
        Logger.getLogger(CreatePresetsDeviceBean.class.getName()).log(Level.INFO, "CreatePresetsDeviceBean.updateBean() called. ***********");
    }

    public void onSiteChange() {
        if (selectedDeviceType != null && !selectedDeviceType.trim().isEmpty()) {
            onDeviceTypeChange();
        }
    }

    public void onDeviceTypeChange() {
        List<DeviceVO> deviceVOs;

        if (userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null) {
            try {
                devicePresetsVOList.clear();
                deviceTypeMap.clear();
                selectDeviceTypeMap.clear();

                deviceVOs = CommonUtilSingleton.getInstance().populateDeviceTypeList(selectedDeviceType);
                deviceVOs.stream().map(deviceVO -> {
                    List<ApAlSetVO> apAlSetVOList, totalApAlSetVOList;
                    DevicePresetVO dpvo;
                    SelectItem item;

                    dpvo = new DevicePresetVO();
                    dpvo.setChannelNumberLabel(deviceVO.getChannelNumLabel());
                    dpvo.setChannelType(deviceVO.getChannelType());
                    dpvo.setChannelNumber((byte) deviceVO.getChannelNum());
                    dpvo.setRestrictions(deviceVO.getRestrictions());
                    dpvo.setApAlSetVOList(new ArrayList());
                    dpvo.setApAlSetSelectItemList(new ArrayList());

                    if (!selectedDeviceType.equalsIgnoreCase("TS1X")) {
                        item = CommonUtilSingleton.getInstance().populateSensitivityAndOffsetValues(selectedDeviceType, deviceVO.getChannelType(), deviceVO.getChannelNum());
                        dpvo.setSensorOffset(Float.valueOf(item.getValue().toString()));
                        dpvo.setSensorUnits(item.getDescription());
                        dpvo.setSensorSensitivity(Float.valueOf(item.getLabel()));

                        dpvo.setSensorType(deviceVO.getRestrictions());

                        if (deviceTypeMap.containsKey(dpvo.getSensorType())) {
                            dpvo.getApAlSetVOList().addAll(deviceTypeMap.get(dpvo.getSensorType()));
                            dpvo.getApAlSetSelectItemList().addAll(selectDeviceTypeMap.get(dpvo.getSensorType()));
                        } else {
                            totalApAlSetVOList = null;

                            // Get Uptime ApAl Presets
                            if (!userManageBean.getCurrentSite().getCustomerAccount().equals(DEFAULT_UPTIME_ACCOUNT) && (apAlSetVOList = APALSetClient.getInstance().getGlobalApAlSetsByCustomerSensorType(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), DEFAULT_UPTIME_ACCOUNT, deviceVO.getRestrictions())) != null && !apAlSetVOList.isEmpty()) {
                                totalApAlSetVOList = new ArrayList();
                                totalApAlSetVOList.addAll(apAlSetVOList);
                            }

                            // Get Global ApAl Presets
                            if ((apAlSetVOList = APALSetClient.getInstance().getGlobalApAlSetsByCustomerSensorType(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), deviceVO.getRestrictions())) != null && !apAlSetVOList.isEmpty()) {
                                if (totalApAlSetVOList == null) {
                                    totalApAlSetVOList = new ArrayList();
                                }
                                totalApAlSetVOList.addAll(apAlSetVOList);
                            }

                            if (totalApAlSetVOList != null && !totalApAlSetVOList.isEmpty()) {
                                dpvo.getApAlSetVOList().addAll(totalApAlSetVOList);
                                dpvo.getApAlSetSelectItemList().addAll(Utils.groupApSetList(Utils.getUniqueApAlSetList(totalApAlSetVOList), userManageBean.getResourceBundleString("label_global_apset")));
                            }

                            // Get Site ApAl Presets
                            if (siteOrGlobal != null && siteOrGlobal.equalsIgnoreCase("site") && (apAlSetVOList = APALSetClient.getInstance().getSiteApAlSetsByCustomerSiteIdSensorType(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), userManageBean.getCurrentSite().getSiteId(), deviceVO.getRestrictions())) != null && !apAlSetVOList.isEmpty()) {
                                dpvo.getApAlSetVOList().addAll(apAlSetVOList);
                                dpvo.getApAlSetSelectItemList().addAll(Utils.groupApSetList(Utils.getUniqueApAlSetList(apAlSetVOList), userManageBean.getResourceBundleString("label_site_apset")));
                            }

                            deviceTypeMap.put(dpvo.getSensorType(), dpvo.getApAlSetVOList());
                            selectDeviceTypeMap.put(dpvo.getSensorType(), dpvo.getApAlSetSelectItemList());
                        }
                    } else {
                        if (deviceVO.getChannelType().equalsIgnoreCase("AC")) {
                            dpvo.setSensorType(acSensorTypeList.get(0).getLabel());
                        } else {
                            dpvo.setSensorType(dcSensorTypeList.get(0).getLabel());
                        }
                        onSensorTypeChange(dpvo);
                    }

                    return dpvo;
                }).forEachOrdered(dpvo -> {
                    devicePresetsVOList.add(dpvo);
                });
            } catch (Exception e) {
                Logger.getLogger(CreatePresetsDeviceBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
                devicePresetsVOList.clear();
                deviceTypeMap.clear();
                selectDeviceTypeMap.clear();
            }
        }
    }

    /**
     * perform the given action when sensorType field is changed
     *
     * @param dpvo
     */
    public void onSensorTypeChange(DevicePresetVO dpvo) {
        List<ApAlSetVO> apAlSetVOList, totalApAlSetVOList;
        SelectItem item = null;

        if (userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null) {
            try {

                dpvo.setApAlSetVOList(new ArrayList());
                dpvo.setApAlSetSelectItemList(new ArrayList());
                if (dpvo.getSensorType() != null && !dpvo.getSensorType().trim().isEmpty()) {
                    if (selectedDeviceType.equalsIgnoreCase("TS1X")) {
                        item = CommonUtilSingleton.getInstance().populateSensitivityAndOffsetValuesForTS1X(dpvo.getChannelType(), dpvo.getSensorType());
                    } else {
                        item = CommonUtilSingleton.getInstance().populateSensitivityAndOffsetValues(selectedDeviceType, dpvo.getChannelType(), dpvo.getChannelNumber());
                    }

                    dpvo.setSensorOffset(Float.valueOf(item.getValue().toString()));
                    dpvo.setSensorUnits(item.getDescription());
                    dpvo.setSensorSensitivity(Float.valueOf(item.getLabel()));

                    if (deviceTypeMap.containsKey(dpvo.getSensorType())) {
                        dpvo.getApAlSetVOList().addAll(deviceTypeMap.get(dpvo.getSensorType()));
                        dpvo.getApAlSetSelectItemList().addAll(selectDeviceTypeMap.get(dpvo.getSensorType()));
                    } else {
                        totalApAlSetVOList = null;

                        // Get Uptime ApAl Presets
                        if (!userManageBean.getCurrentSite().getCustomerAccount().equals(DEFAULT_UPTIME_ACCOUNT) && (apAlSetVOList = APALSetClient.getInstance().getGlobalApAlSetsByCustomerSensorType(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), DEFAULT_UPTIME_ACCOUNT, dpvo.getSensorType())) != null && !apAlSetVOList.isEmpty()) {
                            totalApAlSetVOList = new ArrayList();
                            totalApAlSetVOList.addAll(apAlSetVOList);
                        }

                        // Get Global ApAl Presets
                        if ((apAlSetVOList = APALSetClient.getInstance().getGlobalApAlSetsByCustomerSensorType(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), dpvo.getSensorType())) != null && !apAlSetVOList.isEmpty()) {
                            if (totalApAlSetVOList == null) {
                                totalApAlSetVOList = new ArrayList();
                            }
                            totalApAlSetVOList.addAll(apAlSetVOList);
                        }

                        if (totalApAlSetVOList != null && !totalApAlSetVOList.isEmpty()) {
                            dpvo.getApAlSetVOList().addAll(apAlSetVOList);
                            dpvo.getApAlSetSelectItemList().addAll(Utils.groupApSetList(Utils.getUniqueApAlSetList(apAlSetVOList), userManageBean.getResourceBundleString("label_global_apset")));
                        }

                        // Get Site ApAl Presets
                        if (siteOrGlobal != null && siteOrGlobal.equalsIgnoreCase("site") && (apAlSetVOList = APALSetClient.getInstance().getSiteApAlSetsByCustomerSiteIdSensorType(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), userManageBean.getCurrentSite().getSiteId(), dpvo.getSensorType())) != null && !apAlSetVOList.isEmpty()) {
                            dpvo.getApAlSetVOList().addAll(apAlSetVOList);
                            dpvo.getApAlSetSelectItemList().addAll(Utils.groupApSetList(Utils.getUniqueApAlSetList(apAlSetVOList), userManageBean.getResourceBundleString("label_site_apset")));
                        }
                        deviceTypeMap.put(dpvo.getSensorType(), dpvo.getApAlSetVOList());
                        selectDeviceTypeMap.put(dpvo.getSensorType(), dpvo.getApAlSetSelectItemList());
                    }
                } else {
                    dpvo.setApSetId(null);
                    dpvo.setAlSetId(null);
                    dpvo.setSensorUnits(null);
                    dpvo.setSensorOffset(Float.valueOf("0"));
                    dpvo.setSensorSensitivity(Float.valueOf("0"));
                }
            } catch (Exception e) {
                Logger.getLogger(CreatePresetsDeviceBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
                dpvo.setApSetId(null);
                dpvo.setAlSetId(null);
                dpvo.setSensorUnits(null);
                dpvo.setSensorOffset(Float.valueOf("0"));
                dpvo.setSensorSensitivity(Float.valueOf("0"));
            }
        }
    }

    public void onApSetNameSelect(DevicePresetVO devicePresetsVO) {
        UUID apsetId;

        if (devicePresetsVO != null) {
            apsetId = devicePresetsVO.getApSetId();
            devicePresetsVO.setAlSetVOList(new ArrayList());
            devicePresetsVO.getAlSetVOList().addAll(devicePresetsVO.getApAlSetVOList().stream().filter(ap -> ap.getApSetId().compareTo(apsetId) == 0).collect(Collectors.toList()));
        }
    }

    public List<SelectItem> getACParamUnitList(DevicePresetVO devicePresetVO) {
        if (devicePresetVO != null && devicePresetVO.getSensorType() != null) {
            return getACParamUnitItemListBySensorTypeParamType(devicePresetVO.getSensorType(), "waveform rms");
        }
        return new ArrayList();
    }

    public List<SelectItem> getDCParamUnitList(DevicePresetVO devicePresetVO) {
        if (devicePresetVO != null) {
            return getDCParamUnitItemListBySensorType(devicePresetVO.getSensorType());
        }
        return new ArrayList();
    }

    public void submit() {
        UUID presetsId;

        if (userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null && !userManageBean.getCurrentSite().getCustomerAccount().isEmpty()) {
            validationFailed = false;
            devicePresetsVOList = devicePresetsVOList.stream().filter(p -> p.getSensorType() != null && !p.getSensorType().trim().isEmpty()).collect(Collectors.toList());

            if (devicePresetsVOList == null || devicePresetsVOList.isEmpty()) {
                GenericMessage.setNonAutoGrowlMsg("growl_validate_for_device_presets");
                onDeviceTypeChange();
                return;
            }

            devicePresetsVOList.stream().forEachOrdered(vo -> {
                if (this.setName == null || this.setName.trim().isEmpty() || siteOrGlobal == null || this.selectedDeviceType == null) {
                    validationFailed = true;
                }
                if (!validationFailed) {
                    if (devicePresetsVOList.stream().filter(p -> p.getApSetId() == null || p.getAlSetId() == null).findAny().isPresent()) {
                        validationFailed = true;
                    }
                }
            });

            if (validationFailed) {
                GenericMessage.setNonAutoGrowlMsg("growl_validate_message_for_device_presets");
            } else {
                presetsId = UUID.randomUUID();
                devicePresetsVOList.stream().forEachOrdered(vo -> {
                    vo.setCustomerAccount(userManageBean.getCurrentSite().getCustomerAccount());
                    vo.setName(setName);
                    vo.setSiteId(siteOrGlobal.equalsIgnoreCase("site") ? userManageBean.getCurrentSite().getSiteId() : null);
                    vo.setSiteName(siteOrGlobal.equalsIgnoreCase("site") ? userManageBean.getCurrentSite().getSiteName() : null);
                    vo.setDeviceType(selectedDeviceType);
                    vo.setDisabled(disable);
                    vo.setPresetId(presetsId);
                    if (vo.getChannelType().equalsIgnoreCase("AC") && selectedDeviceType.equalsIgnoreCase("StormX")) {
                        if (vo.getChannelNumberLabel().equalsIgnoreCase("X")) {
                            vo.setSensorLocalOrientation("X");
                        } else if (vo.getChannelNumberLabel().equalsIgnoreCase("Y")) {
                            vo.setSensorLocalOrientation("Y");
                        } else if (vo.getChannelNumberLabel().equalsIgnoreCase("Z")) {
                            vo.setSensorLocalOrientation("Z");                           
                        }
                         vo.setSensorSubType("Piezo");
                    }
                });

                if (siteOrGlobal.equalsIgnoreCase("site")) {
                    if (DevicePresetClient.getInstance().createDevicePresetVO(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), devicePresetsVOList)) {
                        setNonAutoGrowlMsg("growl_create_item_items");
                        Logger.getLogger(CreatePresetsDeviceBean.class.getName()).log(Level.INFO, "Site Device Presets created successfully.");
                    } else {
                        Logger.getLogger(CreatePresetsDeviceBean.class.getName()).log(Level.INFO, "Error in creating Site Device Presets.");
                    }
                } else if (siteOrGlobal.equalsIgnoreCase("global")) {
                    if (DevicePresetClient.getInstance().createGlobalDevicePresetVO(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), devicePresetsVOList)) {
                        setNonAutoGrowlMsg("growl_create_item_items");
                        Logger.getLogger(CreatePresetsDeviceBean.class.getName()).log(Level.INFO, "Global Fault Frequencies created successfully.");
                    } else {
                        Logger.getLogger(CreatePresetsDeviceBean.class.getName()).log(Level.INFO, "Error in creating Global Fault Frequencies.");
                    }
                }
                ((PresetsBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("presetsBean")).resetDevicePresetsTab();
                PrimeFaces.current().ajax().update("presetsTabViewId:devicesFormId");
                PrimeFaces.current().executeScript("PF('defaultDlg').hide()");
            }
        }
    }

    public List<SelectItem> getFrequencyUnitsList() {
        return getFrequencyPlusUnits();
    }

    public void addDevicePreset() {
        devicePresetsVOList.add(new DevicePresetVO());
    }

    public void deleteDevicePresetVO(DevicePresetVO devicePresetVO) {
        if (devicePresetsVOList.size() > 1) {
            devicePresetsVOList.remove(devicePresetVO);
        }
    }

    public void setDevicePresetsVOList(List<DevicePresetVO> devicePresetsVOList) {
        this.devicePresetsVOList = devicePresetsVOList;
    }

    public List<DevicePresetVO> getDevicePresetsVOList() {
        return devicePresetsVOList;
    }

    public List<String> getDeviceTypeList() {
        return deviceTypeList;
    }

    public boolean isDisable() {
        return disable;
    }

    public void setDisable(boolean disable) {
        this.disable = disable;
    }

    public boolean isValidationFailed() {
        return validationFailed;
    }

    public String getSetName() {
        return setName;
    }

    public void setSetName(String setName) {
        this.setName = setName;
    }

    public String getSelectedDeviceType() {
        return selectedDeviceType;
    }

    public void setSelectedDeviceType(String selectedDeviceType) {
        this.selectedDeviceType = selectedDeviceType;
    }

    public List<SelectItem> getDcSensorTypeList() {
        return dcSensorTypeList;
    }

    public List<SelectItem> getAcSensorTypeList() {
        return acSensorTypeList;
    }

    public String getSiteOrGlobal() {
        return siteOrGlobal;
    }

    public void setSiteOrGlobal(String siteOrGlobal) {
        this.siteOrGlobal = siteOrGlobal;
    }
}
