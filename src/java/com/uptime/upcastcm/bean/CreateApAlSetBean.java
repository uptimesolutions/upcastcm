/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.APALSetClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import static com.uptime.upcastcm.utils.enums.ACSensorTypeEnum.getACSensorTypeItemList;
import static com.uptime.upcastcm.utils.enums.AmpFactorsEnum.getAmpFactorItemListByParamType;
import static com.uptime.upcastcm.utils.enums.ParamUnitEnum.getFrequencyPlusUnits;
import static com.uptime.upcastcm.utils.enums.ParamUnitEnum.getACParamUnitItemListBySensorTypeParamType;
import static com.uptime.upcastcm.utils.enums.DCSensorTypeEnum.getDCSensorTypeItemList;
import static com.uptime.upcastcm.utils.enums.DemodHighAndLowPassEnum.getDemodItemList;
import static com.uptime.upcastcm.utils.enums.DemodHighAndLowPassEnum.getLowPassOptionsGreaterThanSelectedHighPass;
import static com.uptime.upcastcm.utils.enums.DwellEnum.getDwellItemList;
import static com.uptime.upcastcm.utils.enums.FmaxEnum.getFmaxItemList;
import static com.uptime.upcastcm.utils.enums.ParamTypeEnum.getParamTypeItemList;
import static com.uptime.upcastcm.utils.enums.ParamUnitEnum.getDCParamUnitItemListBySensorType;
import static com.uptime.upcastcm.utils.enums.ResolutionEnum.getResolutionItemList;
import static com.uptime.upcastcm.utils.helperclass.GenericMessage.missingFields;
import static com.uptime.upcastcm.utils.helperclass.GenericMessage.setNonAutoGrowlMsg;
import com.uptime.upcastcm.utils.vo.AlSetVO;
import com.uptime.upcastcm.utils.vo.ApAlSetVO;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import org.primefaces.PrimeFaces;
import static com.uptime.upcastcm.utils.helperclass.GenericMessage.nameAlreadyUsed;
import javax.faces.model.SelectItem;

/**
 *
 * @author twilcox
 */
public class CreateApAlSetBean implements Serializable {
    private ApAlSetVO apAlSetVO;
    private final List<ApAlSetVO> paramList;
    private List<SelectItem> dcSensorTypeList, acSensorTypeList, paramTypeList, dwellList, fmaxList, resolutionList, demodHighPassList;
    private final List<AlSetVO> alSetVOList, tmpAlSetVOList;
    private UserManageBean userManageBean;
    private boolean acSensor;
    private String siteOrGlobal;
    private boolean renderAlertMsg;
    
    /**
     * Constructor
     */
    public CreateApAlSetBean() {
        paramList = new ArrayList();
        alSetVOList = new ArrayList();
        tmpAlSetVOList = new ArrayList();
        acSensor = true;

        try {
            userManageBean = ((UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean"));
            dcSensorTypeList = getDCSensorTypeItemList();
            acSensorTypeList = getACSensorTypeItemList();
            paramTypeList = getParamTypeItemList();
            dwellList = getDwellItemList();
            fmaxList = getFmaxItemList();
            resolutionList = getResolutionItemList();
            demodHighPassList = getDemodItemList();
        } catch (Exception e) {
            Logger.getLogger(CreateApAlSetBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            userManageBean = null;
        }
    }

    /**
     * Post Constructor
     */
    @PostConstruct
    public void init() {
        resetPage();
    }

    /**
     * Resets the values of this overlay page
     */
    public void resetPage() {
        siteOrGlobal = null;
        paramList.clear();
        paramList.add(new ApAlSetVO());
        alSetVOList.clear();
        alSetVOList.add(new AlSetVO());
        alSetVOList.get(0).setAlSetList(new ArrayList());
        alSetVOList.get(0).getAlSetList().add(new ApAlSetVO());
        tmpAlSetVOList.clear();
        tmpAlSetVOList.add(new AlSetVO());
        tmpAlSetVOList.get(0).setAlSetList(new ArrayList());
        tmpAlSetVOList.get(0).getAlSetList().add(new ApAlSetVO());
        apAlSetVO = new ApAlSetVO();
        if (userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null) {
            apAlSetVO.setCustomerAccount(userManageBean.getCurrentSite().getCustomerAccount());
        }
        acSensor = true;
        renderAlertMsg = false;
    }

    /**
     * Submit the given info to Cassandra
     */
    public void submit() {
        if (userManageBean != null && userManageBean.getCurrentSite() != null) {
            if (submitValidation()) {
                apAlSetVO.setApSetId(UUID.randomUUID());
                apAlSetVO.setSiteId(siteOrGlobal.equalsIgnoreCase("site") ? userManageBean.getCurrentSite().getSiteId() : null);
                
                if (acSensor) {
                    alSetVOList.stream().forEachOrdered(alSet -> {
                        UUID alSetId = UUID.randomUUID();
                        ApAlSetVO paramRow, alRow;

                        for (int i = 0; i < alSet.getAlSetList().size(); i++) {
                            paramRow = paramList.get(i);
                            alRow = alSet.getAlSetList().get(i);
                            alRow.setParamName(paramRow.getParamName());
                            alRow.setParamType(paramRow.getParamType());
                            alRow.setParamUnits(paramRow.getParamUnits());
                            alRow.setParamAmpFactor(paramRow.getParamAmpFactor());
                            alRow.setFrequencyUnits(paramRow.getFrequencyUnits());
                            if (paramRow.getMinFrequency() == 0 && paramRow.getMaxFrequency() == 0) {
                                alRow.setMinFrequency(-1);
                                alRow.setMaxFrequency(-1);
                            } else {
                                alRow.setMinFrequency(paramRow.getMinFrequency());
                                alRow.setMaxFrequency(paramRow.getMaxFrequency());
                            }
                            alRow.setCustomerAccount(apAlSetVO.getCustomerAccount());
                            alRow.setSiteId(apAlSetVO.getSiteId());
                            alRow.setApSetName(apAlSetVO.getApSetName());
                            alRow.setApSetId(apAlSetVO.getApSetId());
                            alRow.setSensorType(apAlSetVO.getSensorType());
                            alRow.setFmax(apAlSetVO.getFmax());
                            alRow.setResolution(apAlSetVO.getResolution());
                            alRow.setPeriod(apAlSetVO.getPeriod());
                            alRow.setSampleRate(apAlSetVO.getSampleRate());
                            alRow.setDemod(apAlSetVO.isDemod());
                            alRow.setDemodHighPass(apAlSetVO.getDemodHighPass());
                            alRow.setDemodLowPass(apAlSetVO.getDemodLowPass());
                            alRow.setAlSetName(apAlSetVO.getAlSetName());
                            alRow.setAlSetId(alSetId);
                            alRow.setAlSetName(alSet.getAlSetName());
                        }
                    });
                } else {
                    alSetVOList.stream().forEachOrdered(alSet -> {
                        UUID alSetId = UUID.randomUUID();

                        alSet.getAlSetList().stream().forEachOrdered(alRow -> {
                            alRow.setParamName(apAlSetVO.getSensorType());
                            alRow.setParamUnits(apAlSetVO.getParamUnits());
                            alRow.setCustomerAccount(apAlSetVO.getCustomerAccount());
                            alRow.setSiteId(apAlSetVO.getSiteId());
                            alRow.setApSetName(apAlSetVO.getApSetName());
                            alRow.setApSetId(apAlSetVO.getApSetId());
                            alRow.setSensorType(apAlSetVO.getSensorType());
                            alRow.setFmax(apAlSetVO.getFmax());
                            alRow.setResolution(apAlSetVO.getResolution());
                            alRow.setPeriod(apAlSetVO.getPeriod());
                            alRow.setSampleRate(apAlSetVO.getSampleRate());
                            alRow.setDemod(apAlSetVO.isDemod());
                            alRow.setDemodHighPass(apAlSetVO.getDemodHighPass());
                            alRow.setDemodLowPass(apAlSetVO.getDemodLowPass());
                            alRow.setAlSetName(apAlSetVO.getAlSetName());
                            alRow.setAlSetId(alSetId);
                            alRow.setAlSetName(alSet.getAlSetName());
                        });
                    });
                }

                apAlSetVO.setDemod(false);
                apAlSetVO.setDemodHighPass(0);
                apAlSetVO.setDemodLowPass(0);
                apAlSetVO.setAlSetName(null);
                apAlSetVO.setParamUnits(null);

                if (APALSetClient.getInstance().createCustomerApSet(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), apAlSetVO)) {
                    if (siteOrGlobal.equalsIgnoreCase("global")) {
                        alSetVOList.stream().forEachOrdered(alSet -> alSet.getAlSetList().stream().forEachOrdered(apAlSet -> APALSetClient.getInstance().createGlobalApAlSets(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), apAlSet)));
                    } else if (siteOrGlobal.equalsIgnoreCase("site")) {
                        alSetVOList.stream().forEachOrdered(alSet -> alSet.getAlSetList().stream().forEachOrdered(apAlSet -> APALSetClient.getInstance().createSiteApAlSets(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), apAlSet)));
                    }
                    userManageBean.getPresetUtil().clearGlobalApAlSetSensorList();
                    ((PresetsBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("presetsBean")).resetAnalysisParametersTab();
                    PrimeFaces.current().ajax().update("presetsTabViewId:analysisParametersFormId");
                    PrimeFaces.current().executeScript("PF('defaultDlg').hide()");
                    setNonAutoGrowlMsg("growl_create_item_items");
                }
            } else {
                missingFields();
            }
        }
    }

    /**
     * Remove the given object from certain fields
     *
     * @param paramRow, ApAlSetVO Object
     * @param rowId, int
     */
    public void deleteParamRow(ApAlSetVO paramRow, int rowId) {
        if (paramRow != null && paramList.size() > 1) {
            try {
                paramList.remove(rowId);
                alSetVOList.stream().forEachOrdered(alSetVO -> alSetVO.getAlSetList().remove(rowId));
                tmpAlSetVOList.stream().forEachOrdered(alSetVO -> alSetVO.getAlSetList().remove(rowId));
                renderErrorMessage(); 
            } catch (Exception e) {
                Logger.getLogger(CreateApAlSetBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            }
        }
    }

    /**
     * Remove the given object from certain fields
     *
     * @param alSet, AlSetVO Object
     */
    public void deleteAlSet(AlSetVO alSet) {
        int index;

        if (tmpAlSetVOList != null && tmpAlSetVOList.size() > 1) {
            try {
                index = tmpAlSetVOList.indexOf(alSet);
                alSetVOList.remove(index);
                tmpAlSetVOList.remove(index);
                renderErrorMessage(); 
            } catch (Exception e) {
                Logger.getLogger(CreateApAlSetBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            }
        }
    }

    /**
     * Add a new object to certain fields
     */
    public void addParamRow() {
        paramList.add(new ApAlSetVO());
        alSetVOList.stream().forEachOrdered(alSetVO -> alSetVO.getAlSetList().add(new ApAlSetVO()));
        ApAlSetVO al = new ApAlSetVO();
        al.setAlSetId(UUID.randomUUID());
        tmpAlSetVOList.stream().forEachOrdered(alSetVO -> alSetVO.getAlSetList().add(al));
    }

    /**
     * Add a new object to certain fields
     */
    public void addAlSet() {
        AlSetVO alSetVO;
        ApAlSetVO al;
        
        alSetVO = new AlSetVO();
        alSetVO.setAlSetList(new ArrayList());
        
        for (int i = 0; i < paramList.size(); i++) {
            al = new ApAlSetVO();
            al.setAlSetId(UUID.randomUUID());
            al.setParamName(paramList.get(i).getParamName());
            alSetVO.getAlSetList().add(al);
        }
        
        alSetVO.setAlSetId(UUID.randomUUID());
        tmpAlSetVOList.add(alSetVO);
        
        alSetVO = new AlSetVO();
        alSetVO.setAlSetList(new ArrayList());
        
        for (int i = 0; i < paramList.size(); i++) {
            al = new ApAlSetVO();
            al.setParamName(paramList.get(i).getParamName());
            alSetVO.getAlSetList().add(al);
        }
        
        alSetVOList.add(alSetVO);
    }
    
    /**
     * perform the given action when acSensor field is changed
     */
    public void onApSetTypeChange() {
        ApAlSetVO tmpVO;
        
        if (userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null) {
            paramList.clear();
            paramList.add(new ApAlSetVO());
            alSetVOList.clear();
            alSetVOList.add(new AlSetVO());
            alSetVOList.get(0).setAlSetList(new ArrayList());
            alSetVOList.get(0).getAlSetList().add(new ApAlSetVO());
            tmpAlSetVOList.clear();
            tmpAlSetVOList.add(new AlSetVO());
            tmpAlSetVOList.get(0).setAlSetList(new ArrayList());
            tmpAlSetVOList.get(0).getAlSetList().add(new ApAlSetVO());

            tmpVO = new ApAlSetVO();
            tmpVO.setApSetName(apAlSetVO.getApSetName());
            tmpVO.setCustomerAccount(userManageBean.getCurrentSite().getCustomerAccount());
            tmpVO.setSiteId(apAlSetVO.getSiteId());

            apAlSetVO = new ApAlSetVO(tmpVO);
        }
    }

    /**
     * perform the given action when sensorType field is changed
     */
    public void onSensorTypeChange() {
        List<ApAlSetVO> tmpList;
        ApAlSetVO newParamRow;

        apAlSetVO.setParamUnits(null);
        if (apAlSetVO.getSensorType() == null) {
            paramList.clear();
            paramList.add(new ApAlSetVO());
            alSetVOList.clear();
            alSetVOList.add(new AlSetVO());
            alSetVOList.get(0).setAlSetList(new ArrayList());
            alSetVOList.get(0).getAlSetList().add(new ApAlSetVO());
            tmpAlSetVOList.clear();
            tmpAlSetVOList.add(new AlSetVO());
            tmpAlSetVOList.get(0).setAlSetList(new ArrayList());
            tmpAlSetVOList.get(0).getAlSetList().add(new ApAlSetVO());
        } else {
            if (!acSensor) {
                tmpList = new ArrayList();
                for (ApAlSetVO param : paramList) {
                    newParamRow = new ApAlSetVO();
                    newParamRow.setParamName(apAlSetVO.getSensorType());
                    tmpList.add(newParamRow);
                }
                alSetVOList.forEach(alSetVO -> alSetVO.getAlSetList().forEach(vo -> vo.setParamName(apAlSetVO.getSensorType())));
                tmpAlSetVOList.forEach(alSetVO -> alSetVO.getAlSetList().forEach(vo -> vo.setParamName(apAlSetVO.getSensorType())));
                
                paramList.clear();
                paramList.addAll(tmpList);
            } else {
                alSetVOList.clear();
                alSetVOList.add(new AlSetVO());
                alSetVOList.get(0).setAlSetList(new ArrayList());
                
                tmpAlSetVOList.clear();
                tmpAlSetVOList.add(new AlSetVO());
                tmpAlSetVOList.get(0).setAlSetList(new ArrayList());
                
                tmpList = new ArrayList();
                for (int i = 0; i < paramList.size(); i++) {
                    newParamRow = new ApAlSetVO();
                    newParamRow.setParamName(paramList.get(i).getParamName());
                    tmpList.add(newParamRow);
                    
                    alSetVOList.get(0).getAlSetList().add(new ApAlSetVO());
                    tmpAlSetVOList.get(0).getAlSetList().add(new ApAlSetVO());
                    alSetVOList.get(0).getAlSetList().get(i).setParamName(paramList.get(i).getParamName());
                    tmpAlSetVOList.get(0).getAlSetList().get(i).setParamName(paramList.get(i).getParamName());
                }
                paramList.clear();
                paramList.addAll(tmpList);
            }
        }
    }

    /**
     * perform the given action when paramType field is changed
     *
     * @param paramRow, ApAlSetVO Object
     */
    public void onParamTypeChange(ApAlSetVO paramRow) {
        if (paramRow != null) {
            paramRow.setParamUnits(null);
            paramRow.setParamAmpFactor(null);
            paramRow.setFrequencyUnits(null);
            paramRow.setMinFrequency(0);
            paramRow.setMaxFrequency(0);
            if (paramRow.getParamType() != null) {
                switch (paramRow.getParamType().toLowerCase()) {
                    case "waveform true peak":
                        paramRow.setParamAmpFactor("Peak");
                        break;
                    case "waveform peak-to-peak":
                        paramRow.setParamAmpFactor("Peak-to-Peak");
                        break;
                    case "waveform rms":
                        paramRow.setParamAmpFactor("RMS");
                        break;
                    case "spectral band total amplitude":
                    case "spectral band peak amplitude":
                        paramRow.setMaxFrequency((float) 0.01);
                        break;
                }
            }
        }

    }

    /**
     * perform the given action when the demod field is changed
     */
    public void onDemodChange() {
        if (apAlSetVO != null && !apAlSetVO.isDemod()) {
            apAlSetVO.setDemodHighPass(0);
            apAlSetVO.setDemodLowPass(0);
        }
    }

    /**
     * perform the given action when the fmax field is changed in the given
     * Object
     */
    public void onFmaxChange() {
        if (apAlSetVO != null) {
            switch (apAlSetVO.getFmax()) {
                case 61:
                    apAlSetVO.setSampleRate((float) 156.25);
                    break;
                case 122:
                    apAlSetVO.setSampleRate((float) 312.5);
                    break;
                case 244:
                    apAlSetVO.setSampleRate(625);
                    break;
                case 488:
                    apAlSetVO.setSampleRate(1250);
                    break;
                case 977:
                    apAlSetVO.setSampleRate(2500);
                    break;
                case 1953:
                    apAlSetVO.setSampleRate(5000);
                    break;
                case 3906:
                    apAlSetVO.setSampleRate(10000);
                    break;
                case 7813:
                    apAlSetVO.setSampleRate(20000);
                    break;
                default:
                    apAlSetVO.setSampleRate(0);
            }

            if (apAlSetVO.getResolution() > 0 && apAlSetVO.getSampleRate() > 0) {
                onResolutionChange();
            }
        }
    }

    /**
     * perform the given action when the resolution field is changed in the
     * given Object
     */
    public void onResolutionChange() {
        if (apAlSetVO != null && apAlSetVO.getSampleRate() > 0) {
            switch (apAlSetVO.getResolution()) {
                case 100:
                    apAlSetVO.setPeriod((1 / apAlSetVO.getSampleRate()) * 256);
                    break;
                case 200:
                    apAlSetVO.setPeriod((1 / apAlSetVO.getSampleRate()) * 512);
                    break;
                case 400:
                    apAlSetVO.setPeriod((1 / apAlSetVO.getSampleRate()) * 1024);
                    break;
                case 800:
                    apAlSetVO.setPeriod((1 / apAlSetVO.getSampleRate()) * 2048);
                    break;
                case 1600:
                    apAlSetVO.setPeriod((1 / apAlSetVO.getSampleRate()) * 4096);
                    break;
                case 3200:
                    apAlSetVO.setPeriod((1 / apAlSetVO.getSampleRate()) * 8192);
                    break;
                case 6400:
                    apAlSetVO.setPeriod((1 / apAlSetVO.getSampleRate()) * 16384);
                    break;
                case 12800:
                    apAlSetVO.setPeriod((1 / apAlSetVO.getSampleRate()) * 32768);
                    break;
                case 25600:
                    apAlSetVO.setPeriod((1 / apAlSetVO.getSampleRate()) * 65536);
                    break;
                case 51200:
                    apAlSetVO.setPeriod((1 / apAlSetVO.getSampleRate()) * 131072);
                    break;
                default:
                    apAlSetVO.setPeriod(0);
            }
        }
    }

    /**
     * perform the given action when the minFrequency or maxFrequency field is
     * changed in the given Object
     *
     * @param paramRow, ApAlSetVO Object
     */
    public void onMinOrMaxFrequencyChange(ApAlSetVO paramRow) {
        if (paramRow != null) {
            if (paramRow.getMinFrequency() < 0) {
                paramRow.setMinFrequency(0);
            }
            if (paramRow.getMaxFrequency() <= paramRow.getMinFrequency()) {
                paramRow.setMaxFrequency(paramRow.getMinFrequency() + (float) 0.01);
            }
        }
    }

    /**
     * perform the given action when the dwell field is changed in the given
     * Object
     *
     * @param alSet, AlSetVO Object
     * @param alRow, ApAlSetVO Object
     */
    public void onDwellChange(AlSetVO alSet, ApAlSetVO alRow) {
        ApAlSetVO apAlSetVOListItem;

        if (alRow != null && (apAlSetVOListItem = getApAlSetVOFromAlSetVOListBasedOnTmpAlSetVOListObjects(alSet, alRow)) != null) {
            apAlSetVOListItem.setDwell(alRow.getDwell());
        }
    }
    
    /**
     * Validate Alarm Limits
     * @param alSetVO, AlSetVO object
     * @param alRow, ApAlSetVO object
     * @param rowId, int
     */
    public void alarmLimitValidation(AlSetVO alSetVO, ApAlSetVO alRow, int rowId) {
        boolean lfValidationFailed, laValidationFailed, haValidationFailed, hfValidationFailed;
        ApAlSetVO apAlSetVOListItem = getApAlSetVOFromAlSetVOListBasedOnTmpAlSetVOListObjects(alSetVO, alRow);

        if (alSetVO != null && alSetVO.getAlSetList() != null && !alSetVO.getAlSetList().isEmpty()) {
            ApAlSetVO apAlByPoint = alSetVO.getAlSetList().get(rowId);
            // Validate Alarm Limits
            if (apAlByPoint != null) {

                // Validates the lowFault field of the given Object
                if (apAlByPoint.isLowFaultActive()) {
                    if (apAlByPoint.getLowFault() <= 0) {
                        lfValidationFailed = true;
                    } else if (apAlByPoint.isLowAlertActive()) {
                        lfValidationFailed = apAlByPoint.getLowFault() >= apAlByPoint.getLowAlert();
                    } else if (apAlByPoint.isHighAlertActive()) {
                        lfValidationFailed = apAlByPoint.getLowFault() >= apAlByPoint.getHighAlert();
                    } else if (apAlByPoint.isHighFaultActive()) {
                        lfValidationFailed = apAlByPoint.getLowFault() >= apAlByPoint.getHighFault();
                    } else {
                        lfValidationFailed = false;
                    }
                } else {
                    lfValidationFailed = false;
                }

                // Validates the lowAlert field of the given Object
                if (apAlByPoint.isLowAlertActive()) {
                    if (apAlByPoint.getLowAlert() <= 0) {
                        laValidationFailed = true;
                    } else if (apAlByPoint.isLowFaultActive() && apAlByPoint.isHighAlertActive()) {
                        laValidationFailed = !(apAlByPoint.getLowAlert() > apAlByPoint.getLowFault() && apAlByPoint.getLowAlert() < apAlByPoint.getHighAlert());
                    } else if (apAlByPoint.isLowFaultActive()) {
                        if (apAlByPoint.isLowFaultActive() && apAlByPoint.isHighFaultActive()) {
                            laValidationFailed = !(apAlByPoint.getLowAlert() > apAlByPoint.getLowFault() && apAlByPoint.getLowAlert() < apAlByPoint.getHighFault());
                        } else {
                            laValidationFailed = apAlByPoint.getLowAlert() <= apAlByPoint.getLowFault();
                        }
                    } else if (apAlByPoint.isHighAlertActive()) {
                        laValidationFailed = apAlByPoint.getLowAlert() >= apAlByPoint.getHighAlert();
                    } else if (apAlByPoint.isHighFaultActive()) {
                        laValidationFailed = apAlByPoint.getLowAlert() >= apAlByPoint.getHighFault();
                    } else {
                        laValidationFailed = false;
                    }
                } else {
                    laValidationFailed = false;
                }

                // Validates the highAlert field of the given Object
                if (apAlByPoint.isHighAlertActive()) {
                    if (apAlByPoint.getHighAlert() <= 0) {
                        haValidationFailed = true;
                    } else if (apAlByPoint.isLowAlertActive() && apAlByPoint.isHighFaultActive()) {
                        haValidationFailed = !(apAlByPoint.getHighAlert() > apAlByPoint.getLowAlert() && apAlByPoint.getHighAlert() < apAlByPoint.getHighFault());
                    } else if (apAlByPoint.isHighFaultActive()) {
                        if (apAlByPoint.isLowFaultActive() && apAlByPoint.isHighFaultActive()) {
                            haValidationFailed = !(apAlByPoint.getHighAlert() > apAlByPoint.getLowFault() && apAlByPoint.getHighAlert() < apAlByPoint.getHighFault());
                        } else {
                            haValidationFailed = apAlByPoint.getHighAlert() >= apAlByPoint.getHighFault();
                        }
                    } else if (apAlByPoint.isLowAlertActive()) {
                        haValidationFailed = apAlByPoint.getHighAlert() <= apAlByPoint.getLowAlert();
                    } else if (apAlByPoint.isLowFaultActive()) {
                        haValidationFailed = apAlByPoint.getHighAlert() <= apAlByPoint.getLowFault();
                    } else {
                        haValidationFailed = false;
                    }
                } else {
                    haValidationFailed = false;
                }

                // Validates the highFault field of the given Object
                if (apAlByPoint.isHighFaultActive()) {
                    if (apAlByPoint.getHighFault() <= 0) {
                        hfValidationFailed = true;
                    } else if (apAlByPoint.isHighAlertActive()) {
                        hfValidationFailed = apAlByPoint.getHighFault() <= apAlByPoint.getHighAlert();
                    } else if (apAlByPoint.isLowAlertActive()) {
                        hfValidationFailed = apAlByPoint.getHighFault() <= apAlByPoint.getLowAlert();
                    } else if (apAlByPoint.isLowFaultActive()) {
                        hfValidationFailed = apAlByPoint.getHighFault() <= apAlByPoint.getLowFault();
                    } else {
                        hfValidationFailed = false;
                    }
                } else {
                    hfValidationFailed = false;
                }

                alRow.setLowFaultFailure(lfValidationFailed);
                alRow.setLowAlertFailure(laValidationFailed);
                alRow.setHighAlertFailure(haValidationFailed);
                alRow.setHighFaultFailure(hfValidationFailed);

                if (!lfValidationFailed) {
                    apAlSetVOListItem.setLowFaultActive(apAlByPoint.isLowFaultActive());
                    apAlSetVOListItem.setLowFault(apAlByPoint.getLowFault());
                }

                if (!laValidationFailed) {
                    apAlSetVOListItem.setLowAlertActive(apAlByPoint.isLowAlertActive());
                    apAlSetVOListItem.setLowAlert(apAlByPoint.getLowAlert());
                }

                if (!haValidationFailed) {
                    apAlSetVOListItem.setHighAlertActive(apAlByPoint.isHighAlertActive());
                    apAlSetVOListItem.setHighAlert(apAlByPoint.getHighAlert());
                }

                if (!hfValidationFailed) {
                    apAlSetVOListItem.setHighFaultActive(apAlByPoint.isHighFaultActive());
                    apAlSetVOListItem.setHighFault(apAlByPoint.getHighFault());
                }
                renderErrorMessage();
            }
        }
    }

    public void renderErrorMessage() {
        renderAlertMsg = false;
        for (AlSetVO alSet : tmpAlSetVOList) {
            List<ApAlSetVO> alSetList = alSet.getAlSetList();
            if (alSetList != null) {
                for (ApAlSetVO row : alSetList) {
                    if (row.isLowFaultFailure()
                            || row.isLowAlertFailure()
                            || row.isHighAlertFailure()
                            || row.isHighFaultFailure()) {
                        renderAlertMsg = true;
                        break;
                    }
                }
            }
            if (renderAlertMsg) {
                break;
            }
        }
    }
    
    /**
     * Validates the given paramName for the given Object
     *
     * @param paramRow, ApAlSetVO Object
     */
    public void paramNameValidation(ApAlSetVO paramRow) {
        int count = 0, index;

        try {
            if (paramRow != null) {
                index = paramList.indexOf(paramRow);
                if (paramRow.getParamName() != null && !paramRow.getParamName().isEmpty()) {
                    for (ApAlSetVO ap : paramList) {
                        if (ap.getParamName() != null && ap.getParamName().equalsIgnoreCase(paramRow.getParamName())) {
                            ++count;
                        }
                        if (count > 1) {
                            String paramName=paramRow.getParamName();
                            paramRow.setParamName(null);
                            alSetVOList.stream().forEachOrdered(alSetVO -> alSetVO.getAlSetList().get(index).setParamName(null));
                            tmpAlSetVOList.stream().forEachOrdered(alSetVO -> alSetVO.getAlSetList().get(index).setParamName(null));
                            nameAlreadyUsed(paramName);
                            break;
                        }
                    }

                    if (count == 1) {
                        alSetVOList.stream().forEachOrdered(alSetVO -> alSetVO.getAlSetList().get(index).setParamName(paramRow.getParamName()));
                        tmpAlSetVOList.stream().forEachOrdered(alSetVO -> alSetVO.getAlSetList().get(index).setParamName(paramRow.getParamName()));
                    }
                } else {
                    alSetVOList.stream().forEachOrdered(alSetVO -> alSetVO.getAlSetList().get(index).setParamName(null));
                    tmpAlSetVOList.stream().forEachOrdered(alSetVO -> alSetVO.getAlSetList().get(index).setParamName(null));
                }
            }
        } catch (Exception e) {
            Logger.getLogger(CreateApAlSetBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Validates the given alSetName for the given Object
     *
     * @param alSet, AlSetVO Object
     */
    public void alSetNameValidation(AlSetVO alSet) {
        String alSetName;
        int count = 0, index;

        try {
            if (alSet != null) {
                index = tmpAlSetVOList.indexOf(alSet);
                if (alSet.getAlSetName() != null && !alSet.getAlSetName().isEmpty()) {
                    for (AlSetVO vo : tmpAlSetVOList) {
                        if (vo.getAlSetName() != null && vo.getAlSetName().equalsIgnoreCase(alSet.getAlSetName())) {
                            ++count;
                        }
                        if (count > 1) {
                            alSetName = alSet.getAlSetName();
                            alSet.setAlSetName(null);
                            if (index >= 0) {
                                alSetVOList.get(index).setAlSetName(null);
                            }
                            nameAlreadyUsed(alSetName);
                            break;
                        }
                    }

                    if (count == 1 && index >= 0) {
                        alSetVOList.get(index).setAlSetName(alSet.getAlSetName());
                    }
                } else if (index >= 0) {
                    alSetVOList.get(index).setAlSetName(null);
                }
            }
        } catch (Exception e) {
            Logger.getLogger(CreateApAlSetBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Validates if items can be pushed to Cassandra
     *
     * @return
     */
    public boolean submitValidation() {
        if (userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null 
                && siteOrGlobal != null && !siteOrGlobal.isEmpty() && apAlSetVO != null && apAlSetVO.getCustomerAccount() != null 
                && apAlSetVO.getCustomerAccount().equals(userManageBean.getCurrentSite().getCustomerAccount())
                && apAlSetVO.getApSetName() != null && !apAlSetVO.getApSetName().isEmpty()
                && apAlSetVO.getSensorType() != null && !apAlSetVO.getSensorType().isEmpty()) {
            if (apAlSetVO.getSensorType().equals("Acceleration") ||
                apAlSetVO.getSensorType().equals("Velocity") ||
                apAlSetVO.getSensorType().equals("Displacement") ||
                apAlSetVO.getSensorType().equals("Ultrasonic")) {

                if (apAlSetVO.getFmax() > 0 && apAlSetVO.getResolution() > 0 && apAlSetVO.getPeriod() > 0 && apAlSetVO.getSampleRate() > 0) {
                    if (alSetVOList.stream().anyMatch(alSet -> alSet.getAlSetName() == null || alSet.getAlSetName().isEmpty())) {
                        return false;
                    }

                    for (ApAlSetVO apAlSet : paramList) {
                        if (apAlSet.getParamName() != null && !apAlSet.getParamName().isEmpty()
                                && apAlSet.getParamType() != null && !apAlSet.getParamType().isEmpty()) {
                            switch (apAlSet.getParamType().toLowerCase()) {
                                case "waveform true peak":
                                case "waveform peak-to-peak":
                                case "waveform rms":
                                case "total spectral energy":
                                case "total non sync energy":
                                case "total sub sync energy":
                                case "demodulation":
                                case "peak amplitude":    
                                    if (!(apAlSet.getParamUnits() != null && !apAlSet.getParamUnits().isEmpty()
                                            && apAlSet.getParamAmpFactor() != null && !apAlSet.getParamAmpFactor().isEmpty()
                                            && (apAlSet.getFrequencyUnits() == null || apAlSet.getFrequencyUnits().isEmpty())
                                            && apAlSet.getMaxFrequency() == 0 && apAlSet.getMinFrequency() == 0)) {
                                        return false;
                                    }
                                    break;
                                case "waveform crest factor":
                                    if (!((apAlSet.getParamUnits() == null || apAlSet.getParamUnits().isEmpty())
                                            && (apAlSet.getParamAmpFactor() == null || apAlSet.getParamAmpFactor().isEmpty())
                                            && (apAlSet.getFrequencyUnits() == null || apAlSet.getFrequencyUnits().isEmpty())
                                            && apAlSet.getMaxFrequency() == 0 && apAlSet.getMinFrequency() == 0)) {
                                        return false;
                                    }
                                    break;
                                case "tach speed":
                                    if (!(apAlSet.getParamUnits() != null && !apAlSet.getParamUnits().isEmpty()
                                            && (apAlSet.getParamAmpFactor() == null || apAlSet.getParamAmpFactor().isEmpty())
                                            && (apAlSet.getFrequencyUnits() == null || apAlSet.getFrequencyUnits().isEmpty())
                                            && apAlSet.getMaxFrequency() == 0 && apAlSet.getMinFrequency() == 0)) {
                                        return false;
                                    }
                                    break;
                                case "spectral band total amplitude":
                                case "spectral band peak amplitude":
                                    if (!(apAlSet.getParamUnits() != null && !apAlSet.getParamUnits().isEmpty()
                                            && apAlSet.getParamAmpFactor() != null && !apAlSet.getParamAmpFactor().isEmpty()
                                            && apAlSet.getFrequencyUnits() != null && !apAlSet.getFrequencyUnits().isEmpty()
                                            && apAlSet.getMaxFrequency() > apAlSet.getMinFrequency() && apAlSet.getMinFrequency() >= 0)) {
                                        return false;
                                    }
                                    break;
                                default:
                                    return false;
                            }
                        } else {
                            return false;
                        }
                    }
                    return true;
                }
            } else if (apAlSetVO.getSensorType().equals("Temperature") ||
                apAlSetVO.getSensorType().equals("Frequency") ||
                apAlSetVO.getSensorType().equals("Pressure") ||
                apAlSetVO.getSensorType().equals("Voltage") ||
                apAlSetVO.getSensorType().equals("Current") ||
                apAlSetVO.getSensorType().equals("Phase") ||
                apAlSetVO.getSensorType().equals("User-Defined") ||
                apAlSetVO.getSensorType().equals("Count") ||
                apAlSetVO.getSensorType().equals("Relative Humidity") ||
                apAlSetVO.getSensorType().equals("Temperature RTD")) {
                
                return !(apAlSetVO.getParamUnits() == null || apAlSetVO.getParamUnits().isEmpty() ||alSetVOList.stream().anyMatch(alSet -> alSet.getAlSetName() == null || alSet.getAlSetName().isEmpty()) ||
                        paramList.size() != 1 || paramList.get(0).getParamName() == null || !paramList.get(0).getParamName().equals(apAlSetVO.getSensorType()));
            }
        }
        return false;
    }

    /**
     * Disable the Param Units UI Field based on the given Object
     *
     * @param paramRow, ApAlSetVO Object
     * @return boolean, true if disable, else false
     */
    public boolean disableParamUnits(ApAlSetVO paramRow) {
        if (paramRow != null && paramRow.getParamType() != null && !paramRow.getParamType().isEmpty()) {
            switch (paramRow.getParamType().toLowerCase()) {
                case "tach speed":
                case "total spectral energy":
                case "total non sync energy":
                case "total sub sync energy":
                case "demodulation":
                case "spectral band total amplitude":
                case "spectral band peak amplitude":
                case "peak amplitude":
                case "waveform true peak":
                case "waveform peak-to-peak":
                case "waveform rms":
                    return false;
                case "waveform crest factor":
                    return true;
            }
        }
        return true;
    }

    /**
     * Disable the Param Amp Factor UI Field based on the given Object
     *
     * @param paramRow, ApAlSetVO Object
     * @return boolean, true if disable, else false
     */
    public boolean disableParamAmpFactor(ApAlSetVO paramRow) {
        if (paramRow != null && paramRow.getParamType() != null && !paramRow.getParamType().isEmpty()) {
            switch (paramRow.getParamType().toLowerCase()) {
                case "total spectral energy":
                case "total non sync energy":
                case "total sub sync energy":
                case "demodulation":
                case "spectral band total amplitude":
                case "spectral band peak amplitude":
                case "peak amplitude":
                    return false;
                case "waveform true peak":
                case "waveform peak-to-peak":
                case "waveform rms":
                case "waveform crest factor":
                case "tach speed":
                    return true;
            }
        }
        return true;
    }

    /**
     * Disable the Frequency UI Field based on the given Object
     *
     * @param paramRow, ApAlSetVO Object
     * @return boolean, true if disable, else false
     */
    public boolean disableFrequency(ApAlSetVO paramRow) {
        if (paramRow != null && paramRow.getParamType() != null && !paramRow.getParamType().isEmpty()) {
            switch (paramRow.getParamType().toLowerCase()) {
                case "waveform true peak":
                case "waveform peak-to-peak":
                case "waveform rms":
                case "waveform crest factor":
                case "tach speed":
                case "total spectral energy":
                case "total non sync energy":
                case "total sub sync energy":
                case "demodulation":
                case "peak amplitude":    
                    return true;
                case "spectral band total amplitude":
                case "spectral band peak amplitude":
                    return false;
            }
        }
        return true;
    }

    /**
     * Return an Object from alSetVOList based on the given info from tmpAlSetVOList
     *
     * @param alSet, AlSetVO Object
     * @param alRow, ApAlSetVO Object
     * @return ApAlSetVO Object
     */
    public ApAlSetVO getApAlSetVOFromAlSetVOListBasedOnTmpAlSetVOListObjects(AlSetVO alSet, ApAlSetVO alRow) {
        int setIndex, rowIndex;
        
        if (alSet != null && alRow != null && tmpAlSetVOList.size() > 0) {
            try {
                setIndex = tmpAlSetVOList.indexOf(alSet);
                rowIndex = tmpAlSetVOList.get(setIndex).getAlSetList().indexOf(alRow);
                return alSetVOList.get(setIndex).getAlSetList().get(rowIndex);
            } catch (Exception e) {
                Logger.getLogger(CreateApAlSetBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            }
        }
        return null;
    }

    public List<SelectItem> getACParamUnitList(ApAlSetVO paramRow) {
        if (apAlSetVO != null && paramRow != null) {
            return getACParamUnitItemListBySensorTypeParamType(apAlSetVO.getSensorType(), paramRow.getParamType());
        }
        return null;
    }
    
    public List<SelectItem> getDCParamUnitList() {
        if (apAlSetVO != null) {
            return getDCParamUnitItemListBySensorType(apAlSetVO.getSensorType());
        }
        return null;
    }

    public List<SelectItem> getParamAmpFactorList(ApAlSetVO paramRow) {
        try {
            return getAmpFactorItemListByParamType(paramRow.getParamType());
        } catch (Exception e) {
            return new ArrayList();
        }
    }

    public List<SelectItem> getDemodLowPassList() {
        if (apAlSetVO != null) {
            return getLowPassOptionsGreaterThanSelectedHighPass((int)apAlSetVO.getDemodHighPass());
        } else {
            return getLowPassOptionsGreaterThanSelectedHighPass(0);
        }
    }

    public List<SelectItem> getFrequencyUnitsList() {
        return getFrequencyPlusUnits();
    }

    public ApAlSetVO getApAlSetVO() {
        return apAlSetVO;
    }

    public void setApAlSetVO(ApAlSetVO apAlSetVO) {
        this.apAlSetVO = apAlSetVO;
    }

    public List<ApAlSetVO> getParamList() {
        return paramList;
    }

    public List<AlSetVO> getTmpAlSetVOList() {
        return tmpAlSetVOList;
    }

    public boolean isAcSensor() {
        return acSensor;
    }

    public void setAcSensor(boolean acSensor) {
        this.acSensor = acSensor;
    }

    public List<SelectItem> getDcSensorTypeList() {
        return dcSensorTypeList;
    }

    public List<SelectItem> getAcSensorTypeList() {
        return acSensorTypeList;
    }

    public List<SelectItem> getParamTypeList() {
        return paramTypeList;
    }

    public List<SelectItem> getDwellList() {
        return dwellList;
    }

    public List<SelectItem> getFmaxList() {
        return fmaxList;
    }

    public List<SelectItem> getResolutionList() {
        return resolutionList;
    }

    public List<SelectItem> getDemodHighPassList() {
        return demodHighPassList;
    }

    public List<AlSetVO> getAlSetVOList() {
        return alSetVOList;
    }

    public String getSiteOrGlobal() {
        return siteOrGlobal;
    }

    public void setSiteOrGlobal(String siteOrGlobal) {
        this.siteOrGlobal = siteOrGlobal;
    }

    public boolean isRenderAlertMsg() {
        return renderAlertMsg;
    }

    public void setRenderAlertMsg(boolean renderAlertMsg) {
        this.renderAlertMsg = renderAlertMsg;
    }

}
