/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.TachometerClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import static com.uptime.upcastcm.utils.enums.TachReferenceUnitEnum.getTachReferenceUnitItemList;
import com.uptime.upcastcm.utils.helperclass.Filters;
import static com.uptime.upcastcm.utils.helperclass.GenericMessage.setNonAutoGrowlMsg;
import com.uptime.upcastcm.utils.interfaces.PresetDialogFields;
import com.uptime.upcastcm.utils.vo.TachometerVO;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import org.primefaces.PrimeFaces;

/**
 *
 * @author twilcox
 */
public class CopyEditTachometersBean implements Serializable, PresetDialogFields {
    private TachometerVO tachometerVO;
    private List<SelectItem> tachReferenceUnitList;
    private int operationType;
    private UserManageBean userManageBean;
    private boolean validationFailed;

    /**
     * Constructor
     */
    public CopyEditTachometersBean() {
        try {
            userManageBean = ((UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean"));
            tachReferenceUnitList = getTachReferenceUnitItemList();
        } catch (Exception e) {
            Logger.getLogger(CopyEditTachometersBean.class.getName()).log(Level.SEVERE, "CopyEditPointLocationNameBean Constructor Exception {0}", e.toString());
            userManageBean = null;
        }
    }

    /**
     * Post Constructor
     */
    @PostConstruct
    public void init() {
        resetPage();
        if (userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null) {
            Logger.getLogger(CopyEditTachometersBean.class.getName()).log(Level.INFO, "**customerAccount to be set : {0}", userManageBean.getCurrentSite().getCustomerAccount());
        }
    }

    /**
     * Resets the values of this overlay page
     */
    @Override
    public void resetPage() {
        validationFailed = true;
        tachometerVO = null;
        operationType = -1;
        
        tachometerVO = new TachometerVO();
        if (userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null) {
            tachometerVO.setCustomerAccount(userManageBean.getCurrentSite().getCustomerAccount());
        }
    }

    /**
     * Presets fields based on the given info
     *
     * @param presets, Filters Object
     * @param object, Object
     */
    @Override
    public void presetFields(Filters presets, Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Presets fields based on the given info
     *
     * @param operationType, int
     * @param object, Object
     */
    @Override
    public void presetFields(int operationType, Object object) {
        if (userManageBean != null && userManageBean.getCurrentSite() != null && object != null && object instanceof TachometerVO) {
            tachometerVO = new TachometerVO((TachometerVO) object);
            tachometerVO.setSiteId(userManageBean.getCurrentSite().getSiteId());
            tachometerVO.setSiteName(userManageBean.getCurrentSite().getSiteName());
            if ((this.operationType = operationType) == 2) {
                tachometerVO.setTachName(null);
                tachometerVO.setTachId(null);
            }
        }
    }

    /**
     * Presets fields based on the given info
     *
     * @param object, Object
     */
    @Override
    public void presetFields(Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Submit the given info to Cassandra
     */
    public void submit() {
        validationFailed = true;
        
        if (userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null) {
            if (tachometerVO != null && tachometerVO.getCustomerAccount() != null && tachometerVO.getCustomerAccount().equals(userManageBean.getCurrentSite().getCustomerAccount())
                    && tachometerVO.getTachName() != null && !tachometerVO.getTachName().isEmpty()) {
                validationFailed = false;
                
                // Edit
                if (operationType == 1) {
                    if (TachometerClient.getInstance().updateSiteTachometers(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), tachometerVO)) {
                        ((PresetsBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("presetsBean")).resetTachometersTab();
                        PrimeFaces.current().ajax().update("presetsTabViewId:tachometersFormId");
                        PrimeFaces.current().executeScript("PF('defaultDlg').hide()");
                        setNonAutoGrowlMsg("growl_update_item_items");
                    }
                } 
                
                // Copy
                else {
                    if (TachometerClient.getInstance().createSiteTachometers(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), tachometerVO)) {
                        ((PresetsBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("presetsBean")).resetTachometersTab();
                        PrimeFaces.current().ajax().update("presetsTabViewId:tachometersFormId");
                        PrimeFaces.current().executeScript("PF('defaultDlg').hide()");
                        setNonAutoGrowlMsg("growl_create_item_items");
                    }
                }
                userManageBean.getPresetUtil().clearGlobalTachList();
                userManageBean.getPresetUtil().clearSiteTachList();
            }
        }
    }

    /**
     * Update the form based on the given tachometer type
     */
    public void onTachTypeChange() {
        if (tachometerVO != null && tachometerVO.getTachType() != null) {
            switch (tachometerVO.getTachType()) {
                case "Constant Speed":
                    break;
            }
        }
    }

    /**
     * Calculates the Reference RPM
     */
    public void calculateReferenceRPM() {
        Double refRPM;
        
        if (tachometerVO != null && tachometerVO.getFpm() > 0) {
            if (tachometerVO.getRollDiameter() > 0) {
                refRPM = (tachometerVO.getFpm() / (((tachometerVO.getRollDiameter()) * Math.PI) / 12));
                tachometerVO.setTachReferenceSpeed(refRPM.floatValue());
                PrimeFaces.current().ajax().update("copyEditTachometersFormId");
                PrimeFaces.current().executeScript("PF('calc').hide()");
            } else {
                FacesContext.getCurrentInstance().addMessage("growlMessages", new FacesMessage("Roll Diameter should be greater than 0"));
            }
        } else {
            PrimeFaces.current().ajax().update("copyEditTachometersFormId");
            PrimeFaces.current().executeScript("PF('calc').hide()");
        }
    }

    public TachometerVO getTachometerVO() {
        return tachometerVO;
    }

    public int getOperationType() {
        return operationType;
    }

    public void setOperationType(int operationType) {
        this.operationType = operationType;
    }

    public void setTachometerVO(TachometerVO tachometerVO) {
        this.tachometerVO = tachometerVO;
    }

    public boolean isValidationFailed() {
        return validationFailed;
    }

    public List<SelectItem> getTachReferenceUnitList() {
        return tachReferenceUnitList;
    }
    
}
