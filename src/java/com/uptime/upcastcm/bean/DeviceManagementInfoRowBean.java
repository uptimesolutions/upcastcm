/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.bean;

import com.uptime.upcastcm.utils.vo.ChannelVO;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author twilcox
 */
public class DeviceManagementInfoRowBean implements Serializable {
    private ChannelVO channelVO;

    /**
     * Constructor
     */
    public DeviceManagementInfoRowBean() {
        channelVO = null;
    }

    /**
     * Clear Page
     */
    public void resetPage() {
        channelVO = null;
    }
    
    public void presetFields(ChannelVO channelVO) {
        this.channelVO = null;
        
        if (channelVO != null) {
            Logger.getLogger(DeviceManagementInfoRowBean.class.getName()).log(Level.WARNING, "********{0}", channelVO.toString());
            this.channelVO = new ChannelVO(channelVO);
        } else {
            Logger.getLogger(DeviceManagementInfoRowBean.class.getName()).log(Level.WARNING, "********ChannelVO Object is null");
        }
    }

    public ChannelVO getChannelVO() {
        return channelVO;
    }
}
