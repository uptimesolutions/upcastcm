/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.APALSetClient;
import com.uptime.upcastcm.http.client.AreaClient;
import com.uptime.upcastcm.http.client.AssetClient;
import com.uptime.upcastcm.http.client.AssetPresetClient;
import com.uptime.upcastcm.http.client.DevicePresetClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import static com.uptime.upcastcm.utils.helperclass.GenericMessage.failedUpload;
import static com.uptime.upcastcm.utils.helperclass.GenericMessage.successfulUpload;
import static com.uptime.upcastcm.utils.helperclass.GenericMessage.setNonAutoGrowlMsg;
import static com.uptime.upcastcm.utils.helperclass.Utils.distinctByKeys;
import com.uptime.upcastcm.utils.singletons.CommonUtilSingleton;
import com.uptime.upcastcm.utils.vo.ApAlSetVO;
import com.uptime.upcastcm.utils.vo.AreaVO;
import com.uptime.upcastcm.utils.vo.AssetPresetVO;
import com.uptime.upcastcm.utils.vo.AssetVO;
import com.uptime.upcastcm.utils.vo.DevicePresetVO;
import com.uptime.upcastcm.utils.vo.PointLocationVO;
import com.uptime.upcastcm.utils.vo.PointVO;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;
import org.primefaces.PrimeFaces;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.file.UploadedFile;

/**
 *
 * @author twilcox
 */
public class ImportConfigUploadBean implements Serializable {

    private final List<AssetPresetVO> siteAssetPresetVOList, globalAssetPresetVOList;
    private final List<DevicePresetVO> devicePresetsList;

    private transient UploadedFile file;

    private UserManageBean userManageBean;
    private List<AreaVO> areaVOList;
    private List<String[]> failedRows;
    private String uploadedFileName;
    private int areaIndex, assetIndex, assetTemplateIndex;
    private boolean reqColumnsExists;

    private final String NAME_REGEX = "^[^-\\s][a-zA-Z0-9_\\s-/,.#%=()\\[\\]:;]+$";

    /**
     * Creates a new instance of ImportConfigUploadBean
     */
    public ImportConfigUploadBean() {
        areaVOList = new ArrayList();
        siteAssetPresetVOList = new ArrayList();
        globalAssetPresetVOList = new ArrayList();
        devicePresetsList = new ArrayList();
        failedRows = new ArrayList();
        uploadedFileName = null;
        file = null;
        areaIndex = 0;
        assetIndex = 1;
        assetTemplateIndex = 2;

        try {
            userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
        } catch (Exception e) {
            Logger.getLogger(ImportConfigUploadBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            userManageBean = null;
        }
    }

    public StreamedContent downloadCsvTemplate() {
        reqColumnsExists = false;
        failedRows.clear();
        areaIndex = 0;
        assetIndex = 1;
        assetTemplateIndex = 2;
        return downloadCsv();
    }

    public StreamedContent downloadCsv() {
        StringBuilder csvData;
        InputStream stream;
        String[] header, row;
        String errorFileName;
        int size = areaIndex;
        
        try {
            csvData = new StringBuilder();
            if (assetIndex > size) {
                size = assetIndex;
            }
            if (assetTemplateIndex > size) {
                size = assetTemplateIndex;
            }
            if (!reqColumnsExists) {
                header = new String[size + 1];
                header[areaIndex] = "Area_Name";
                header[assetIndex] = "Asset_Name";
                header[assetTemplateIndex] = "Asset_Template_Name";

                // Convert header to CSV format using StringBuilder
                for (int i = 0; i < header.length; i++) {
                    csvData.append(header[i]);
                    if (i != header.length - 1) {
                        csvData.append(",");
                    }
                }
                csvData.append("\n");
            }

            for (String[] failedRow : failedRows) {
                row = new String[size + 1];
                if (failedRow.length > areaIndex && failedRow[areaIndex] != null && !failedRow[areaIndex].trim().isEmpty()) {
                    row[areaIndex] = failedRow[areaIndex];
                }
                if (failedRow.length > assetIndex && failedRow[assetIndex] != null && !failedRow[assetIndex].trim().isEmpty()) {
                    row[assetIndex] = failedRow[assetIndex];
                }
                if (failedRow.length > assetTemplateIndex && failedRow[assetTemplateIndex] != null && !failedRow[assetTemplateIndex].trim().isEmpty()) {
                    row[assetTemplateIndex] = failedRow[assetTemplateIndex];
                }
                csvData.append(String.join(",", row)).append("\n");
            }

            // Convert CSV content to InputStream
            stream = new ByteArrayInputStream(csvData.toString().getBytes(StandardCharsets.UTF_8));

            // Create and return StreamedContent
            errorFileName = "";
            if (uploadedFileName != null && !uploadedFileName.isEmpty()) {
                errorFileName = uploadedFileName.split("\\.")[0];
            }

            return DefaultStreamedContent.builder().name(failedRows.isEmpty() ? "Bulk_Upload_Template.csv" : (errorFileName + "_Errors.csv")).contentType("text/csv").stream(() -> stream).build();
        } catch (Exception e) {
            Logger.getLogger(ImportConfigUploadBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
        return null;
    }

    public void uploadFile() {
        int bytesRead;
        byte[] buffer;

        try {
            if (file != null && file.getFileName() != null && !file.getFileName().isEmpty()) {
                uploadedFileName = file.getFileName();
                try ( InputStream inputStream = file.getInputStream();  FileOutputStream outputStream = new FileOutputStream(uploadedFileName)) {
                    buffer = new byte[1024];
                    while ((bytesRead = inputStream.read(buffer)) != -1) {
                        outputStream.write(buffer, 0, bytesRead);
                    }
                } catch (IOException e) {
                    Logger.getLogger(ImportConfigUploadBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
                }
                PrimeFaces.current().executeScript("PF('importConfigDialog').show()");
            } else {
                setNonAutoGrowlMsg("import_configuration_validation_message");
            }
        } catch (Exception e) {
            Logger.getLogger(ImportConfigUploadBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    public void clearFile() {
        File delFile;

        try {
            if (uploadedFileName != null && !uploadedFileName.isEmpty()) {
                file = null;

                delFile = new File(uploadedFileName);
                delFile.delete();

                uploadedFileName = null;
            }
        } catch (Exception e) {
            Logger.getLogger(ImportConfigUploadBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    public void upload() {
        boolean areaNameExits, assetNameExists, assetTemlateNameExists;
        String line;
        String[] metadata;

        reqColumnsExists = false;
        if (uploadedFileName != null && !uploadedFileName.isEmpty()) {
            areaNameExits = false;
            assetNameExists = false;
            assetTemlateNameExists = false;

            try ( BufferedReader br = new BufferedReader(new FileReader(uploadedFileName))) {
                line = br.readLine(); // Skip Header

                failedRows = new ArrayList();
                metadata = line.split(",");
                if (metadata.length >= 3) {
                    for (int i = 0; i < metadata.length; i++) {
                        if (!areaNameExits && metadata[i].equalsIgnoreCase("Area_Name")) {
                            areaIndex = i;
                            areaNameExits = true;
                        } else if (!assetNameExists && metadata[i].equalsIgnoreCase("Asset_Name")) {
                            assetIndex = i;
                            assetNameExists = true;
                        } else if (!assetTemlateNameExists && metadata[i].equalsIgnoreCase("Asset_Template_Name")) {
                            assetTemplateIndex = i;
                            assetTemlateNameExists = true;
                        }
                    }
                        
                    if (!(areaNameExits && assetNameExists && assetTemlateNameExists)) {
                        reqColumnsExists = true;
                        failedRows.add(metadata);
                        while ((line = br.readLine()) != null) {
                            metadata = line.split(",");
                            failedRows.add(metadata);
                        }
                    }
                    
                } else {
                    reqColumnsExists = true;
                    failedRows.add(metadata);
                    while ((line = br.readLine()) != null) {
                        metadata = line.split(",");
                        failedRows.add(metadata);
                    }
                }

                if (failedRows.isEmpty()) {
                    while ((line = br.readLine()) != null) {
                        metadata = line.split(",");
                        if (metadata.length >= 3) {
                            if (!isValid(metadata)) {
                                failedRows.add(metadata);
                            } else {
                                if (!csvToAsset(metadata)) {
                                    failedRows.add(metadata);
                                }
                            }
                        } else {
                            failedRows.add(metadata);
                        }
                    }
                } 

                // Generate a CSV file with failed rows
                if (!failedRows.isEmpty()) {
                    failedUpload();
                    PrimeFaces.current().executeScript("downloadErrorCsv()");
                } else {
                    File delFile = new File(uploadedFileName);
                    delFile.delete();
                    uploadedFileName = null;
                    successfulUpload();
                }

                ((ConfigurationBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("configurationBean")).onSiteChange(true);
            } catch (Exception e) {
                Logger.getLogger(ImportConfigUploadBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            }
        }
    }

    private boolean csvToAsset(String[] metadata) throws Exception {
        String areaName, assetName, assetTemplateName;
        List<DevicePresetVO> selectedSiteDevicePresetList;
        List<AssetPresetVO> assetPresetVOs;
        List<PointLocationVO> plvos;
        AssetPresetVO assetPresetVO;
        boolean response;
        AssetVO assetVO;
        AreaVO areaVO;
        UUID areaId;

        try {
            if (areaVOList == null || areaVOList.isEmpty()) {
                areaVOList = AreaClient.getInstance().getAreaSiteByCustomerSite(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), userManageBean.getCurrentSite().getSiteId());
            }

            areaName = metadata[areaIndex];
            assetName = metadata[assetIndex];
            assetTemplateName = metadata[assetTemplateIndex];

            if (siteAssetPresetVOList.isEmpty()) {
                siteAssetPresetVOList.addAll(userManageBean.getPresetUtil().getSiteAssetPresetList(userManageBean.getCurrentSite().getCustomerAccount(), userManageBean.getCurrentSite().getSiteId()));
            }

            if ((assetPresetVOs = siteAssetPresetVOList.stream().filter(assetpreset -> assetpreset.getAssetPresetName().equals(assetTemplateName)).collect(Collectors.toList())) != null && !assetPresetVOs.isEmpty()) {
                assetPresetVO = new AssetPresetVO(assetPresetVOs.get(0));
            } else {
                if (globalAssetPresetVOList.isEmpty()) {
                    globalAssetPresetVOList.addAll(AssetPresetClient.getInstance().getGlobalAssetPresetsByCustomer(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount()));
                    if (!userManageBean.getCurrentSite().getCustomerAccount().equals(DEFAULT_UPTIME_ACCOUNT)) {
                        globalAssetPresetVOList.addAll(AssetPresetClient.getInstance().getGlobalAssetPresetsByCustomer(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), DEFAULT_UPTIME_ACCOUNT));
                    }
                }
                if (!globalAssetPresetVOList.isEmpty()) {
                    assetPresetVOs = globalAssetPresetVOList.stream().filter(assetpreset -> assetpreset.getAssetPresetName().equals(assetTemplateName)).collect(Collectors.toList());
                }

                if (assetPresetVOs != null && !assetPresetVOs.isEmpty()) {
                    assetPresetVO = new AssetPresetVO(assetPresetVOs.get(0));
                } else {
                    return false;
                }
            }

            response = false;

            // Create new area if needed
            if ((areaVO = areaVOList.stream().filter(area -> area.getAreaName().equals(areaName)).findAny().orElse(null)) == null) {
                areaId = UUID.randomUUID();

                areaVO = new AreaVO();
                areaVO.setAreaId(areaId);
                areaVO.setAreaName(areaName);
                areaVO.setCustomerAccount(userManageBean.getCurrentSite().getCustomerAccount());
                areaVO.setSiteId(userManageBean.getCurrentSite().getSiteId());
                areaVO.setScheduleVOList(new ArrayList());

                if (AreaClient.getInstance().createArea(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), areaVO)) {
                    areaVOList.add(areaVO);
                    response = true;
                }
            } else {
                areaId = areaVO.getAreaId();
                response = true;
            }

            if (response) {

                // Get device presets
                if (devicePresetsList.isEmpty()) {
                    devicePresetsList.addAll(DevicePresetClient.getInstance().getDevicePresetVOs(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), userManageBean.getCurrentSite().getSiteId()));

                    // Get Uptime Device Presets
                    if (!userManageBean.getCurrentSite().getCustomerAccount().equals(DEFAULT_UPTIME_ACCOUNT)) {
                        devicePresetsList.addAll(DevicePresetClient.getInstance().getGlobalDevicePresetsByCustomer(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), DEFAULT_UPTIME_ACCOUNT));
                    }

                    // Get Global Device Presets
                    devicePresetsList.addAll(DevicePresetClient.getInstance().getGlobalDevicePresetsByCustomer(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount()));
                }

                // Set Asset
                assetVO = new AssetVO();
                assetVO.setCustomerAccount(userManageBean.getCurrentSite().getCustomerAccount());
                assetVO.setSiteId(userManageBean.getCurrentSite().getSiteId());
                assetVO.setAreaId(areaId);
                assetVO.setAssetName(assetName);
                assetVO.setAlarmEnabled(false);
                assetVO.setAssetType(assetPresetVO.getAssetTypeName());
                assetVO.setSampleInterval(assetPresetVO.getSampleInterval());
                assetVO.setDescription(assetPresetVO.getDescription());

                plvos = new ArrayList();
                for (AssetPresetVO apvo : assetPresetVOs) {
                    List<PointVO> pointVOs;
                    PointLocationVO plvo;

                    // Set Point Location
                    plvo = new PointLocationVO();
                    plvo.setSampleInterval((short) apvo.getSampleInterval());
                    plvo.setPointLocationName(apvo.getPointLocationName());
                    plvo.setTachId(apvo.getTachId());
                    plvo.setFfSetIds(apvo.getFfSetIds());
                    plvo.setFfSetNames(apvo.getFfSetNames());
                    plvo.setSpeedRatio(apvo.getSpeedRatio());
                    plvo.setRollDiameter(apvo.getRollDiameter());
                    plvo.setRollDiameterUnits(apvo.getRollDiameterUnits());
                    plvo.setBasestationPortNum((short) 702);

                    selectedSiteDevicePresetList = new ArrayList();
                    selectedSiteDevicePresetList.addAll(devicePresetsList.stream().filter(dp -> dp.getDeviceType().equalsIgnoreCase(apvo.getDevicePresetType()) && dp.getPresetId().toString().equalsIgnoreCase(apvo.getDevicePresetId().toString())).collect(Collectors.toList()));

                    pointVOs = new ArrayList();
                    for (DevicePresetVO dpVO : selectedSiteDevicePresetList) {
                        PointVO pvo;
                        List<ApAlSetVO> apAlSetVOList;

                        pvo = new PointVO();
                        pvo.setCustomerAccount(userManageBean.getCurrentSite().getCustomerAccount());
                        pvo.setSiteId(userManageBean.getCurrentSite().getSiteId());
                        pvo.setPointName(apvo.getPointLocationName() + " " + CommonUtilSingleton.getInstance().getPointNameBySensorType(dpVO.getSensorType()));
                        pvo.setSensorUnits(CommonUtilSingleton.getInstance().getSensorUnitBySensorType(dpVO.getSensorType()));

                        if ((apAlSetVOList = APALSetClient.getInstance().getGlobalApAlSetsByCustomerApSetIdAlSetId(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), dpVO.getCustomerAccount(), dpVO.getApSetId(), dpVO.getAlSetId())) != null && !apAlSetVOList.isEmpty()) {
                            pvo.setApSetName(apAlSetVOList.get(0).getApSetName());
                            pvo.setAlSetName(apAlSetVOList.get(0).getAlSetName());
                        } else if ((apAlSetVOList = APALSetClient.getInstance().getGlobalApAlSetsByCustomerApSetIdAlSetId(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), DEFAULT_UPTIME_ACCOUNT, dpVO.getApSetId(), dpVO.getAlSetId())) != null && !apAlSetVOList.isEmpty()) {
                            pvo.setApSetName(apAlSetVOList.get(0).getApSetName());
                            pvo.setAlSetName(apAlSetVOList.get(0).getAlSetName());
                        } else if (userManageBean.getCurrentSite().getSiteId() != null && (apAlSetVOList = APALSetClient.getInstance().getSiteApAlSetsByCustomerSiteIdApSetIdAlSetId(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), dpVO.getCustomerAccount(), userManageBean.getCurrentSite().getSiteId(), dpVO.getApSetId(), dpVO.getAlSetId())) != null && !apAlSetVOList.isEmpty()) {
                            pvo.setApSetName(apAlSetVOList.get(0).getApSetName());
                            pvo.setAlSetName(apAlSetVOList.get(0).getAlSetName());
                        } else {
                            throw new Exception("ApAlSet not found. AP set UUID:" + dpVO.getApSetId().toString() + ", AL set UUID:" + dpVO.getAlSetId().toString() + ".");
                        }

                        pvo.setAlSetId(dpVO.getAlSetId());
                        pvo.setAlarmEnabled(dpVO.isAlarmEnabled());
                        pvo.setApSetId(dpVO.getApSetId());
                        pvo.setDisabled(dpVO.isDisabled());
                        pvo.setPointType(dpVO.getChannelType());
                        pvo.setSensorChannelNum(dpVO.getChannelNumber());
                        pvo.setSensorOffset(dpVO.getSensorOffset());
                        pvo.setSensorSensitivity(dpVO.getSensorSensitivity());
                        pvo.setSensorType(dpVO.getSensorType());
                        pvo.setSensorUnits(dpVO.getSensorUnits());
                        pvo.setApAlSetVOs(apAlSetVOList);

                        pointVOs.add(pvo);
                    }

                    plvo.setPointList(pointVOs);
                    plvos.add(plvo);
                }
                assetVO.setPointLocationList(plvos);

                return AssetClient.getInstance().createAsset(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), assetVO);
            }
        } catch (Exception e) {
            Logger.getLogger(ImportConfigUploadBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
        return false;
    }

    private boolean isValid(String[] metadata) {
        String areaName, assetName, assetTemplateName;

        try {
            areaName = metadata[areaIndex];
            assetName = metadata[assetIndex];
            assetTemplateName = metadata[assetTemplateIndex];

            return !(areaName == null || areaName.trim().isEmpty() || !areaName.matches(NAME_REGEX)
                    || assetName == null || assetName.trim().isEmpty() || !assetName.matches(NAME_REGEX)
                    || assetTemplateName == null || assetTemplateName.trim().isEmpty() || !assetTemplateName.matches(NAME_REGEX));
        } catch (Exception e) {
            Logger.getLogger(ImportConfigUploadBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
        return false;
    }

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }
}
