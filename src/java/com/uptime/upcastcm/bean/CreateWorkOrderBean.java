/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.AssetClient;
import com.uptime.upcastcm.http.client.PointClient;
import com.uptime.upcastcm.http.client.PointLocationClient;
import com.uptime.upcastcm.http.client.WorkOrderClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import static com.uptime.upcastcm.utils.enums.WorkOrderActionEnum.getActionItemList;
import static com.uptime.upcastcm.utils.enums.WorkOrderIssueEnum.getIssueItemList;
import static com.uptime.upcastcm.utils.enums.WorkOrderPriorityEnum.getPriorityItemList;
import com.uptime.upcastcm.utils.helperclass.Filters;
import com.uptime.upcastcm.utils.helperclass.GenericMessage;
import com.uptime.upcastcm.utils.interfaces.PresetDialogFields;
import com.uptime.upcastcm.utils.vo.AlarmsVO;
import com.uptime.upcastcm.utils.vo.PointLocationVO;
import com.uptime.upcastcm.utils.vo.PointVO;
import com.uptime.upcastcm.utils.vo.TrendDetailsVO;
import com.uptime.upcastcm.utils.vo.WorkOrderVO;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import org.primefaces.PrimeFaces;

/**
 *
 * @author twilcox
 */
public class CreateWorkOrderBean implements Serializable, PresetDialogFields {
    private WorkOrderVO workOrderVO;
    private UserManageBean userManageBean;
    private NavigationBean navigationBean;
    private final List<SelectItem> assetList;
    private List<SelectItem> priorities, issueList, actionList;
    private UUID alSetId;
    private boolean assetPreset, fromWorkOrder;
    private Filters navigationFilter;

    /**
     * Creates a new instance of CreateWorkOrderBean
     */
    public CreateWorkOrderBean() {
        workOrderVO = new WorkOrderVO();
        assetList = new ArrayList(); 

        try {
            navigationBean = (NavigationBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("navigationBean");
            userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
            actionList = getActionItemList();
            issueList = getIssueItemList();
            priorities = getPriorityItemList();
        } catch (Exception e) {
            Logger.getLogger(CreateAssetBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            navigationBean = null;
            userManageBean = null;
        }
    }

    /**
     * Post Constructor
     */
    @PostConstruct
    public void init() {
        resetPage();
    }

    @Override
    public void resetPage() {
        workOrderVO = new WorkOrderVO();
        workOrderVO.setTrendDetails(new ArrayList());
        assetList.clear();
        alSetId = null;
        assetPreset = true;
        fromWorkOrder = false;
    }

    @Override
    public void presetFields(Filters presets, Object object) {
        List<PointLocationVO> pointLocationVOList;
        List<PointVO> pointVOList;
        TrendDetailsVO trendDetailsVO;
        AlarmsVO alarmsVO;
        
        resetPage();
        
        if (userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null) {
            workOrderVO.setUser(userManageBean.getUserName());
            workOrderVO.setCustomerAccount(userManageBean.getCurrentSite().getCustomerAccount());
        
            if (presets != null) { 
                navigationFilter = new Filters(presets);
                // Set Site
                if (presets.getSite() != null) {
                    workOrderVO.setSiteId((UUID) presets.getSite().getValue());
                    workOrderVO.setSiteName(presets.getSite().getLabel());
                }

                // Set Area
                if (presets.getArea() != null) {
                    workOrderVO.setAreaName(presets.getArea().getLabel());
                    workOrderVO.setAreaId((UUID) presets.getArea().getValue());
                }

                // From Alarms
                if (object != null && object instanceof AlarmsVO) {
                    alarmsVO = (AlarmsVO) object;
                    workOrderVO.setAssetName(alarmsVO.getAssetName());
                    workOrderVO.setAssetId(alarmsVO.getAssetId());
                    workOrderVO.setPointId(alarmsVO.getPointId());
                    workOrderVO.setPointLocationId(alarmsVO.getPointLocationId());
                    workOrderVO.setApSetId(alarmsVO.getApSetId());
                    
                    if (workOrderVO.getAssetId() != null) {
                        if (workOrderVO.getPointLocationId() != null) { 
                            if (!(pointLocationVOList = PointLocationClient.getInstance().getPointLocationsByPK(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), workOrderVO.getCustomerAccount(), workOrderVO.getSiteId(), workOrderVO.getAreaId(), workOrderVO.getAssetId(), workOrderVO.getPointLocationId())).isEmpty()) {
                                workOrderVO.setPointLocationName(pointLocationVOList.get(0).getPointLocationName());
                            }
                            
                            if (workOrderVO.getPointId() != null) { 
                                if (!(pointVOList = PointClient.getInstance().getPointsByCustomerSiteAreaAssetPointLocationPoint(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), workOrderVO.getCustomerAccount(), workOrderVO.getSiteId(), workOrderVO.getAreaId(), workOrderVO.getAssetId(), workOrderVO.getPointLocationId(), workOrderVO.getPointId(), true)).isEmpty()) {
                                    workOrderVO.setPointName(pointVOList.get(0).getPointName());
                                    workOrderVO.setChannelType(pointVOList.get(0).getPointType());
                                    alSetId = pointVOList.get(0).getAlSetId();
                                }
                                
                                if (workOrderVO.getApSetId() != null && alarmsVO.getParamName() != null && !alarmsVO.getParamName().isEmpty()) {
                                    trendDetailsVO = new TrendDetailsVO();
                                    trendDetailsVO.setApSetId(workOrderVO.getApSetId());
                                    trendDetailsVO.setChannelType(workOrderVO.getChannelType());
                                    trendDetailsVO.setParamName(alarmsVO.getParamName());
                                    trendDetailsVO.setPointId(workOrderVO.getPointId());
                                    trendDetailsVO.setPointName(workOrderVO.getPointName());
                                    trendDetailsVO.setPointLocationId(workOrderVO.getPointLocationId());
                                    trendDetailsVO.setPointLocationName(workOrderVO.getPointLocationName());
                                    workOrderVO.getTrendDetails().add(trendDetailsVO);
                                }
                            }
                        }
                    }
                } 

                // From WorkOrder
                else {
                    fromWorkOrder = true;
                    
                    if (presets.getAsset() != null) {
                        workOrderVO.setAssetName(presets.getAsset().getLabel());
                        workOrderVO.setAssetId((UUID) presets.getAsset().getValue());
                    }
                }
                
                // Set assetList field
                if (workOrderVO.getAssetId() == null) {
                    AssetClient.getInstance().getAssetSiteAreaByCustomerSiteArea(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), workOrderVO.getSiteId(), workOrderVO.getAreaId()).stream().forEachOrdered(asset -> assetList.add(new SelectItem(asset.getAssetId(), asset.getAssetName())));
                    assetPreset = false;
                }
            } 
        }
    }

    @Override
    public void presetFields(int operationType, Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void presetFields(Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void updateBean() {
        Logger.getLogger(CreateWorkOrderBean.class.getName()).log(Level.INFO, "workOrderVO.getCustomerAccount()*********** {0}", new Object[]{workOrderVO.getCustomerAccount()});
    }

    /**
     * Save the given work order
     */
    public void submit() {
        if (workOrderVO != null) {
            if (WorkOrderClient.getInstance().createOpenWorkOrder(ServiceRegistrarClient.getInstance().getServiceHostURL(WORK_ORDER_SERVICE_NAME), workOrderVO)) {
                try {                   
                    GenericMessage.setNonAutoGrowlMsg("growl_create_work_order");
                    
                    // From Work Order
                    if (fromWorkOrder) {
                        try {
                            if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("workOrderBean") == null) {
                                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("workOrderBean", new WorkOrderBean());
                                ((WorkOrderBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("workOrderBean")).init();
                            }
                            ((WorkOrderBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("workOrderBean")).updateCount();
                            PrimeFaces.current().executeScript("PF('defaultDlg').hide()");
                            PrimeFaces.current().executeScript("updateTreeNodes()");
                        } catch (Exception e) {
                            Logger.getLogger(CreateWorkOrderBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
                        }
                    }  
                    
                    // From Alarms
                    else {
                        PrimeFaces.current().executeScript("PF('defaultDlg').hide()");
                    }
                } catch (Exception e) {
                    Logger.getLogger(CreateWorkOrderBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
                }
            }
        }
    }
    
    /**
     * Remove the given trend from the trendDetailsVO list
     * @param trendDetailsVO, TrendDetailsVO Object
     */
    public void removeTrend(TrendDetailsVO trendDetailsVO) {
        if (trendDetailsVO != null) {
            workOrderVO.getTrendDetails().remove(trendDetailsVO);
        }
    }
    
    /**
     * View the given trend
     *
     * @param trendDetailsVO, TrendDetailsVO Object
     */
    public void viewTrendGraph(TrendDetailsVO trendDetailsVO) {
        Filters filters;
        AssetAnalysisWindowBean assetAnalysisWindowBean;
        
        try {  
            if ((assetAnalysisWindowBean = (AssetAnalysisWindowBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("assetAnalysisWindowBean")) == null) {
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("assetAnalysisWindowBean", new AssetAnalysisWindowBean());
                assetAnalysisWindowBean = (AssetAnalysisWindowBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("assetAnalysisWindowBean");
                assetAnalysisWindowBean.init();
            }
            
            filters = new Filters();
            filters.setRegion(userManageBean.getSelectedFilters().getRegion());
            filters.setCountry(userManageBean.getSelectedFilters().getCountry());
            filters.setSite(new SelectItem(workOrderVO.getSiteId(), workOrderVO.getSiteName()));
            filters.setArea(new SelectItem(workOrderVO.getAreaId(), workOrderVO.getAreaName()));
            filters.setAsset(new SelectItem(workOrderVO.getAssetId(), workOrderVO.getAssetName()));
            filters.setPointLocation(new SelectItem(trendDetailsVO.getPointLocationId(), trendDetailsVO.getPointLocationName()));
            filters.setPoint(new SelectItem(trendDetailsVO.getPointId(), trendDetailsVO.getPointName()));
            filters.setApSet(new SelectItem(trendDetailsVO.getApSetId()));
            
            // The AlSet UUID is being left null because the value needs to be looked up and it isn't used in the method loadTrendGraph.
            filters.setAlSetParam(new SelectItem(null, trendDetailsVO.getParamName()));
            
            assetAnalysisWindowBean.loadTrendGraph(filters, false);
        } catch (Exception e) {
            Logger.getLogger(CreateWorkOrderBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }
    
    /**
     * Open the Add Trend Overlay
     */
    public void openAddTrendOverlay() {
        try {
            if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("addTrendBean") == null) {
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("addTrendBean", new AddTrendBean());
                ((AddTrendBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("addTrendBean")).init();
            }
            ((AddTrendBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("addTrendBean")).presetFields(alSetId);
            ((AddTrendBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("addTrendBean")).presetFields(workOrderVO);
            navigationBean.updateSecondDialog("add-trend");
        } catch (Exception e) {
            Logger.getLogger(CreateWorkOrderBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }
    
    /**
     * Action after an asset is selected
     */
    public void onAssetChange() {
        try {
            if (workOrderVO.getAssetId() != null) {
                workOrderVO.setAssetName(assetList.stream().filter(item -> workOrderVO.getAssetId().equals((UUID)item.getValue())).map(SelectItem::getLabel).findFirst().get());
            } else {
                workOrderVO.setAssetName(null);
            }
        } catch (Exception e) {
            Logger.getLogger(CreateWorkOrderBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            workOrderVO.setAssetName(null);
            workOrderVO.setAssetId(null);
        }
        workOrderVO.setTrendDetails(new ArrayList());
    }

    public WorkOrderVO getWorkOrderVO() {
        return workOrderVO;
    }

    public List<SelectItem> getPriorities() {
        return priorities;
    }

    public List<SelectItem> getIssueList() {
        return issueList;
    }

    public List<SelectItem> getActionList() {
        return actionList;
    }

    public List<SelectItem> getAssetList() {
        return assetList;
    }

    public boolean isAssetPreset() {
        return assetPreset;
    }
}
