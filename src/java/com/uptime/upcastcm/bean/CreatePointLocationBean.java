/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.FaultFrequenciesClient;
import com.uptime.upcastcm.http.client.PointLocationClient;
import com.uptime.upcastcm.http.client.TachometerClient;
import com.uptime.upcastcm.http.utils.json.dom.PointLocationDomJsonParser;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import static com.uptime.upcastcm.utils.enums.RollDiameterUnitEnum.getRollDiameterUnitItemList;
import com.uptime.upcastcm.utils.helperclass.Filters;
import com.uptime.upcastcm.utils.helperclass.Utils;
import com.uptime.upcastcm.utils.vo.PointLocationVO;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import com.uptime.upcastcm.utils.interfaces.PresetDialogFields;
import com.uptime.upcastcm.utils.vo.AssetVO;
import com.uptime.upcastcm.utils.vo.FaultFrequenciesVO;
import com.uptime.upcastcm.utils.vo.TachometerVO;
import java.util.UUID;
import java.util.stream.Collectors;
import javax.faces.model.SelectItem;
import org.primefaces.PrimeFaces;

/**
 *
 * @author twilcox
 */
public class CreatePointLocationBean implements Serializable, PresetDialogFields {
    
    private PointLocationVO pointLocationVO;
    private List<String> siteNames, areaNameList, assetNameList;
    private UserManageBean userManageBean;
    private List<SelectItem> tachometersList, faultFrequenciesList, ptLoNamePresetList;
    private List<SelectItem> rollDiameterUnitsList;
    private List<FaultFrequenciesVO> siteFFSetFavorites;

    /**
     * Constructor
     */
    public CreatePointLocationBean() {
        siteNames = new ArrayList();
        areaNameList = new ArrayList();
        
        try {
            userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
            rollDiameterUnitsList = getRollDiameterUnitItemList();
        } catch (Exception e) {
            Logger.getLogger(CreatePointLocationBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            userManageBean = null;
        }
    }

    /**
     * Post Constructor
     */
    @PostConstruct
    public void init() {
        resetPage();
        if (userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null) {
            Logger.getLogger(CreatePointLocationBean.class.getName()).log(Level.INFO, "**customerAccount to be set : {0}", userManageBean.getCurrentSite().getCustomerAccount());
        }
    }
    
    @Override
    public void resetPage() {
        pointLocationVO = new PointLocationVO();
        if (userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null) {
            pointLocationVO.setCustomerAccount(userManageBean.getCurrentSite().getCustomerAccount());
        }
    }
    
    @Override
    public void presetFields(Filters presets, Object object) {
        assetNameList = new ArrayList();
        tachometersList = new ArrayList();
        ptLoNamePresetList = new ArrayList();
        faultFrequenciesList = new ArrayList();
        siteNames.clear();
        areaNameList.clear();
        pointLocationVO.setBasestationPortNum((short) 702);
        AssetVO assetvo = (AssetVO) object;
        if (assetvo != null) {
            pointLocationVO.setSampleInterval((short) assetvo.getSampleInterval());
        }
        if (presets != null) {
            if (presets.getSite() != null) {
                pointLocationVO.setSiteName(presets.getSite().getLabel());
                pointLocationVO.setSiteId((UUID) presets.getSite().getValue());
                siteNames.add(presets.getSite().getLabel());
            }
            if (presets.getArea() != null) {
                pointLocationVO.setAreaName(presets.getArea().getLabel());
                pointLocationVO.setAreaId((UUID) presets.getArea().getValue());
                areaNameList.add(presets.getArea().getLabel());
            }
            if (presets.getAsset() != null) {
                pointLocationVO.setAssetName(presets.getAsset().getLabel());
                pointLocationVO.setAssetId((UUID) presets.getAsset().getValue());
                assetNameList.add(pointLocationVO.getAssetName());
            }
            if (presets.getPointLocation() != null) {
                pointLocationVO.setPointLocationName(presets.getPointLocation().getLabel());
                pointLocationVO.setPointLocationId((UUID) presets.getPointLocation().getValue());
            }
        }
        try {
            // get tachometer presets
            userManageBean.getPresetUtil().clearSiteTachList();
            tachometersList.addAll(userManageBean.getPresetUtil().populateTachometerList(userManageBean, userManageBean.getCurrentSite().getCustomerAccount(), pointLocationVO.getSiteId()));

            // get point locatin name presets
            userManageBean.getPresetUtil().clearSitePointLocationNameList();
            ptLoNamePresetList.addAll(userManageBean.getPresetUtil().populatePtLoNamesList(userManageBean, userManageBean.getCurrentSite().getCustomerAccount(), pointLocationVO.getSiteId()));
            
            faultFrequenciesList = populateFFSetList();
        } catch (Exception e) {
            Logger.getLogger(CreatePointLocationBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
        
    }
    
    @Override
    public void presetFields(int operationType, Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public void presetFields(Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public void updateBean() {
        Logger.getLogger(CreatePointLocationBean.class.getName()).log(Level.INFO, "pointLocationVO.getSiteName()*********** {0}", new Object[]{pointLocationVO.getSiteName()});
    }
    
    public List<SelectItem> populateFFSetList() {
        List<SelectItem> ffSetList = new ArrayList();
        siteFFSetFavorites = new ArrayList();
        try {
            siteFFSetFavorites = FaultFrequenciesClient.getInstance().getSiteFFSetFavoritesVOList(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), userManageBean.getCurrentSite().getSiteId());
            ffSetList = userManageBean.getPresetUtil().populateFFSetNameList(userManageBean, siteFFSetFavorites);
        } catch (Exception e) {
            Logger.getLogger(CreatePointLocationBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
        
        return ffSetList;
    }
    
    public void onTachChange() {
        List<TachometerVO> tachList;
        
        if (pointLocationVO.getTachId() != null) {
            if ((tachList = TachometerClient.getInstance().getGlobalTachometersByPK(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), DEFAULT_UPTIME_ACCOUNT, pointLocationVO.getTachId())) == null || tachList.isEmpty()) {
                if ((tachList = TachometerClient.getInstance().getGlobalTachometersByPK(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), pointLocationVO.getCustomerAccount(), pointLocationVO.getTachId())) == null || tachList.isEmpty()) {
                    tachList = TachometerClient.getInstance().getSiteTachometersByPK(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), pointLocationVO.getCustomerAccount(), pointLocationVO.getSiteId(), pointLocationVO.getTachId());
                }
            }
            pointLocationVO.setTachReferenceUnits((tachList != null && !tachList.isEmpty() && tachList.get(0).getTachReferenceUnits() != null) ? tachList.get(0).getTachReferenceUnits() : null);
        }
        pointLocationVO.setSpeedRatio(1);
        pointLocationVO.setRollDiameter(0);
        pointLocationVO.setRollDiameterUnits(null);
    }
    
    public void submit() {
        try {
            pointLocationVO.setFfSetIds(null);
            if (!pointLocationVO.getFfSetName().isEmpty()) {
                pointLocationVO.setFfSetIds(PointLocationDomJsonParser.getInstance().getJsonFromSelectedPointLocation(pointLocationVO.getFfSetName(), siteFFSetFavorites));
            }
            
            if (PointLocationClient.getInstance().createPointLocation(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), pointLocationVO)) {
                pointLocationVO = new PointLocationVO();
                if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("configurationBean") == null) {
                    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("configurationBean", new ConfigurationBean());
                    ((ConfigurationBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("configurationBean")).init();
                }
                ((ConfigurationBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("configurationBean")).onAssetChange(true);
                PrimeFaces.current().executeScript("PF('defaultDlg').hide()");
                PrimeFaces.current().executeScript("updateTreeNodes()");
            }
        } catch (Exception e) {
            Logger.getLogger(CreatePointLocationBean.class.getName()).log(Level.SEVERE, "Exception while updating point location", e);
        }
    }
    
    public PointLocationVO getPointLocationVO() {
        return pointLocationVO;
    }
    
    public void setPointLocationVO(PointLocationVO pointLocationVO) {
        this.pointLocationVO = pointLocationVO;
    }
    
    public List<String> getSiteNames() {
        return siteNames;
    }
    
    public List<String> getAssetNameList() {
        return assetNameList;
    }
    
    public List<SelectItem> getFaultFrequenciesList() {
        return faultFrequenciesList;
    }
    
    public List<SelectItem> getTachometersList() {
        return tachometersList;
    }
    
    public List<SelectItem> getPtLoNamePresetList() {
        return ptLoNamePresetList;
    }
    
    public List<String> getAreaNameList() {
        return areaNameList;
    }
    
    public List<SelectItem> getRollDiameterUnitsList() {
        return rollDiameterUnitsList;
    }
    
    public List<FaultFrequenciesVO> getSiteFFSetFavorites() {
        return siteFFSetFavorites;
    }
    
}
