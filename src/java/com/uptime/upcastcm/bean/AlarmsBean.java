/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.APALSetClient;
import com.uptime.upcastcm.http.client.AlarmsClient;
import com.uptime.upcastcm.http.client.AreaClient;
import com.uptime.upcastcm.http.client.AssetClient;
import com.uptime.upcastcm.http.client.PointClient;
import com.uptime.upcastcm.http.client.PointLocationClient;
import com.uptime.upcastcm.http.client.SiteClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import com.uptime.upcastcm.utils.helperclass.AbstractNavigation;
import com.uptime.upcastcm.utils.helperclass.Filters;
import com.uptime.upcastcm.utils.vo.AlarmsVO;
import com.uptime.upcastcm.utils.vo.ApAlSetVO;
import com.uptime.upcastcm.utils.vo.AreaVO;
import com.uptime.upcastcm.utils.vo.AssetVO;
import com.uptime.upcastcm.utils.vo.PointLocationVO;
import com.uptime.upcastcm.utils.vo.PointVO;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import org.primefaces.PrimeFaces;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.event.NodeCollapseEvent;
import org.primefaces.event.NodeExpandEvent;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.ToggleSelectEvent;
import org.primefaces.event.UnselectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.FilterMeta;
import org.primefaces.model.TreeNode;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.timeline.TimelineEvent;
import org.primefaces.model.timeline.TimelineGroup;
import org.primefaces.model.timeline.TimelineModel;

/**
 *
 * @author kpati
 */
public class AlarmsBean extends AbstractNavigation implements Serializable {
    private String dummy, areaFilterSelect, eventStyle, alarmType;
    private UserManageBean userManageBean;
    private List<AlarmsVO> filteredAlarms, selectedAlarms;
    private final List<AlarmsVO> areaTileList, alarmsVOList;
    private final Map<UUID, List<AlarmsVO>> alarmsCountsMap;
    private AlarmsVO alarmsVO;
    private PointVO pointVO;
    private List<FilterMeta> filterBy;
    private List<String> alarmTypes, priorityList, issuesList, actionNeededList;

    private boolean selectable, zoomable, moveable, stackEvents, showCurrentTime, showNavigation, displayAreaTable, displayAssetTable, displayPLTable, displayPointTable;
    private Map<String, ApAlSetVO> apAlSetVOMap;
    private TimelineModel<String, String> model;
    boolean showHistory, dontClearDataTableFilter;
    
    private final DecimalFormat DECIMAL_FORMAT;
    
    /**
     * Constructor
     */
    public AlarmsBean() {
        super(POINT);
        selectedAlarms = new ArrayList();
        alarmsCountsMap = new HashMap();
        areaTileList = new ArrayList();
        alarmTypes = new ArrayList();
        priorityList = new ArrayList();
        issuesList = new ArrayList();
        actionNeededList = new ArrayList();
        alarmsVOList = new ArrayList();
        model = new TimelineModel<>();
        eventStyle = "box";
        stackEvents = true;
        selectable = true;
        zoomable = true;
        moveable = true;
        showCurrentTime = true;
        showNavigation = false;
        showHistory = false;
        dontClearDataTableFilter = false;
        apAlSetVOMap = new HashMap();
        hideDataTable();

        DECIMAL_FORMAT = new DecimalFormat("#.####");
        try {
            userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
        } catch (Exception e) {
            Logger.getLogger(ConfigurationBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            userManageBean = null;
        }

    }

    /**
     * Post Constructor
     */
    @PostConstruct
    public void init() {
        resetPage();

        // Set the selectFilters
        try {
            if (userManageBean.getCurrentSite() != null) {
                selectedFilters.setRegion(userManageBean.getSelectedFilters().getRegion());
                selectedFilters.setCountry(userManageBean.getSelectedFilters().getCountry());
                selectedFilters.setSite(userManageBean.getSelectedFilters().getSite());
                onSiteChange(true);
            }
        } catch (Exception e) {
            Logger.getLogger(AlarmsBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    public void updateBean() {
        Logger.getLogger(AlarmsBean.class.getName()).log(Level.INFO, "AlarmsBean.updateBean() called. ***********");
    }

    /**
     * Reset fields values when navigates to Alarms page.
     */
    @Override
    public void resetPage() {
        setRegionCountrySiteMap();
        setSelectedFilters();
        pointVO = new PointVO();
        areaList.clear();
        assetList.clear();
        pointLocationList.clear();
        pointList.clear();
        alarmsCountsMap.clear();
        areaTileList.clear();
        alarmsVOList.clear();
        showHistory = false;
        selectedAlarms = new ArrayList();
        hideDataTable();
        alarmsVO = new AlarmsVO();

        if (userManageBean != null) {
            alarmsVO.setSiteName(userManageBean.getCurrentSite().getSiteName());
            alarmsVO.setSiteId(userManageBean.getCurrentSite().getSiteId());
            treeContentMenuBuilder(ROOT, userManageBean.getResourceBundleString("breadcrumb_2"), null, null);
        }
        updateBreadcrumbModel();
        updateUI("content");
        clearDataTables();
    }
    
    /**
     * Clear the DataTables
     */
    public void clearDataTables() {
        DataTable datatable;
        
        PrimeFaces.current().executeScript("PF('dtAlarms').unselectAllRows()");
        if (!dontClearDataTableFilter) {
            try {
                if ((datatable = (DataTable)FacesContext.getCurrentInstance().getViewRoot().findComponent("alarm-form:dt-alarms-area")) != null) {
                    datatable.reset();
                }
                if ((datatable = (DataTable)FacesContext.getCurrentInstance().getViewRoot().findComponent("alarm-form:dt-alarms-asset")) != null) {
                    datatable.reset();
                }
                if ((datatable = (DataTable)FacesContext.getCurrentInstance().getViewRoot().findComponent("alarm-form:dt-alarms-pointlocation")) != null) {
                    datatable.reset();
                }
                if ((datatable = (DataTable)FacesContext.getCurrentInstance().getViewRoot().findComponent("alarm-form:dt-alarms-point")) != null) {
                    datatable.reset();
                }
            } catch (Exception e) {
                Logger.getLogger(AlarmsBean.class.getName()).log(Level.WARNING, e.getMessage(), e);
            }
        }
    }

    /**
     * called when the year changed
     * @param reloadUI
     */
    @Override
    public void onYearChange(boolean reloadUI) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    /**
     * called when the region changed
     * @param reloadUI
     */
    @Override
    public void onRegionChange(boolean reloadUI) {
        if (selectedFilters == null) {
            setSelectedFilters();
        }
        if (selectedFilters.getRegion() != null && !selectedFilters.getRegion().isEmpty()) {
            selectedFilters.clear(REGION);
            areaList.clear();
            assetList.clear();
            pointLocationList.clear();
            pointList.clear();
            areaTileList.clear();
            alarmsVOList.clear();
            showHistory = false;
            selectedAlarms = new ArrayList();
            hideDataTable();
            
            if (reloadUI) {
                updateTreeNodeRoot(REGION);
                updateBreadcrumbModel();
                updateUI("content");
                clearDataTables();
            }
        } else {
            resetPage();
        }
    }

    /**
     * called when the country changed
     * @param reloadUI
     */
    @Override
    public void onCountryChange(boolean reloadUI) {
        if (selectedFilters == null) {
            setSelectedFilters();
        }
        if (selectedFilters.getCountry() != null && !selectedFilters.getCountry().isEmpty()) {
            selectedFilters.clear(COUNTRY);
            areaList.clear();
            assetList.clear();
            pointLocationList.clear();
            pointList.clear();
            areaTileList.clear();
            alarmsVOList.clear();
            showHistory = false;
            selectedAlarms = new ArrayList();
            hideDataTable();
            
            if (reloadUI) {
                updateTreeNodeRoot(COUNTRY);
                updateBreadcrumbModel();
                updateUI("content");
                clearDataTables();
            }
        } else {
            onRegionChange(reloadUI);
        }
    }

    /**
     * called when the site changed
     * @param reloadUI
     */
    @Override
    public void onSiteChange(boolean reloadUI) {
        List<AreaVO> list;

        if (selectedFilters == null) {
            setSelectedFilters();
        }
        if (selectedFilters.getSite() != null && selectedFilters.getSite().getLabel() != null && !selectedFilters.getSite().getLabel().isEmpty() && selectedFilters.getSite().getValue() != null) {
            selectedFilters.clear(SITE);
            assetList.clear();
            pointLocationList.clear();
            pointList.clear();
            alarmsVOList.clear();
            showHistory = false;
            selectedAlarms = new ArrayList();
            hideDataTable();
            try {
                areaList.clear();
                list = AreaClient.getInstance().getAreaSiteByCustomerSite(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue());
                list.stream().sorted(Comparator.comparing(AreaVO::getAreaName)).forEachOrdered(area -> areaList.add(new SelectItem(area.getAreaId(), area.getAreaName())));
            } catch (Exception ex) {
                areaList.clear();
            }
            setAreaTileList((UUID) selectedFilters.getSite().getValue());
            
            if (reloadUI) {
                updateTreeNodeRoot(SITE);
                updateBreadcrumbModel();
                updateUI("content");
                clearDataTables();
            }
        } else {
            onCountryChange(reloadUI);
        }

    }

    /**
     * called when the area changed
     * @param reloadUI
     */
    @Override
    public void onAreaChange(boolean reloadUI) {
        List<AssetVO> list;
        List<PointLocationVO> pointLocationVOlist;

        if (selectedFilters == null) {
            setSelectedFilters();
        }
        if (selectedFilters.getArea() != null && selectedFilters.getArea().getLabel() != null && !selectedFilters.getArea().getLabel().isEmpty() && selectedFilters.getArea().getValue() != null) {
            selectedFilters.clear(AREA);
            pointLocationList.clear();
            pointList.clear();
            areaTileList.clear();
            showHistory = false;
            selectedAlarms = new ArrayList();
            try {
                assetList.clear();
                list = AssetClient.getInstance().getAssetSiteAreaByCustomerSiteArea(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue());
                list.stream().sorted(Comparator.comparing(AssetVO::getAssetName)).forEachOrdered(asset -> assetList.add(new SelectItem(asset.getAssetId(), asset.getAssetName())));    
            } catch (Exception ex) {
                assetList.clear();
            }
            try {
                pointLocationList.clear();
                pointLocationVOlist = PointLocationClient.getInstance().getPointLocationsByCustomerSiteArea(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue());
                pointLocationVOlist.stream().sorted(Comparator.comparing(PointLocationVO::getPointLocationName)).forEachOrdered(pointLocation -> pointLocationList.add(new SelectItem(pointLocation.getPointLocationId(), pointLocation.getPointLocationName())));
            } catch (Exception ex) {
                Logger.getLogger(AlarmsBean.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
                pointLocationList.clear();
            }
            updateAlarmsVOListByAreaId();
            
            if (reloadUI) {
                updateTreeNodeRoot(AREA);
                updateBreadcrumbModel();
                updateUI("content");
                clearDataTables();
            }
        } else {
            onSiteChange(reloadUI);
        }
    }

    /**
     * called when the asset changed
     * @param reloadUI
     */
    @Override
    public void onAssetChange(boolean reloadUI) {
        List<PointLocationVO> list;

        if (selectedFilters == null) {
            setSelectedFilters();
        }
        if (selectedFilters.getAsset() != null && selectedFilters.getAsset().getLabel() != null && !selectedFilters.getAsset().getLabel().isEmpty() && selectedFilters.getAsset().getValue() != null) {
            selectedFilters.clear(ASSET);
            pointList.clear();
            areaTileList.clear();
            showHistory = false;
            selectedAlarms = new ArrayList();
            try {
                pointLocationList.clear();
                list = PointLocationClient.getInstance().getPointLocationsByCustomerSiteAreaAsset(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue(), (UUID) selectedFilters.getAsset().getValue());
                list.stream().sorted(Comparator.comparing(PointLocationVO::getPointLocationName)).forEachOrdered(pointLocation -> pointLocationList.add(new SelectItem(pointLocation.getPointLocationId(), pointLocation.getPointLocationName())));
            } catch (Exception ex) {
                Logger.getLogger(AlarmsBean.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
                pointLocationList.clear();
            }
            updateAlarmsVOListByAssetId();
            
            if (reloadUI) {
                updateTreeNodeRoot(ASSET);
                updateBreadcrumbModel();
                updateUI("content");
                clearDataTables();
            }
        } else {
            onAreaChange(reloadUI);
        }
    }

    /**
     * called when the pointLocation changed
     * @param reloadUI
     */
    @Override
    public void onPointLocationChange(boolean reloadUI) {
        List<PointVO> list;

        if (selectedFilters == null) {
            setSelectedFilters();
        }
        if (selectedFilters.getPointLocation() != null && selectedFilters.getPointLocation().getLabel() != null && !selectedFilters.getPointLocation().getLabel().isEmpty() && selectedFilters.getPointLocation().getValue() != null) {
            selectedFilters.clear(POINT_LOCATION);
            areaTileList.clear();
            model = new TimelineModel<>();
            showHistory = false;
            selectedAlarms = new ArrayList();
            try {
                pointList.clear();
                list = PointClient.getInstance().getPointsByCustomerSiteAreaAssetPointLocation(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue(), (UUID) selectedFilters.getAsset().getValue(), (UUID) selectedFilters.getPointLocation().getValue(), true);
                list.stream().sorted(Comparator.comparing(PointVO::getPointName)).forEachOrdered(point -> pointList.add(new SelectItem(point.getPointId(), point.getPointName())));
            } catch (Exception ex) {
                pointList.clear();
            }
            updateAlarmsVOListByPointLocationId();
            
            if (reloadUI) {
                updateTreeNodeRoot(POINT_LOCATION);
                updateBreadcrumbModel();
                updateUI("content");
                clearDataTables();
            }
        } else {
            onAssetChange(reloadUI);
        }
    }

    /**
     * called when the point changed
     * @param reloadUI
     */
    @Override
    public void onPointChange(boolean reloadUI) {
        if (selectedFilters == null) {
            setSelectedFilters();
        }
        if (selectedFilters.getPoint() != null && selectedFilters.getPoint().getLabel() != null && !selectedFilters.getPoint().getLabel().isEmpty() && selectedFilters.getPoint().getValue() != null) {
            areaTileList.clear();
            selectedAlarms = new ArrayList();
            updateAlarmsVOListByPointId();
            
            if (reloadUI) {
                updateTreeNodeRoot(POINT);
                updateBreadcrumbModel();
                updateUI("content");
                clearDataTables();
            }
        } else {
            onPointLocationChange(reloadUI);
        }
    }

    @Override
    public void onApSetChange(boolean reloadUI) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
    @Override
    public void onAlSetParamChange(boolean reloadUI) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    /**
     * Update the UI based on the given value
     *
     * @param value
     */
    @Override
    public void updateUI(String value) {
        try {
            switch (value) {
                case "content":
                    PrimeFaces.current().ajax().update("alarm-form");
                    PrimeFaces.current().ajax().update("alarmsFilterFormId");
                    break;
                case "tree":
                    PrimeFaces.current().ajax().update("leftTreeFormId");
                    break;
                case "breadcrumb":
                    PrimeFaces.current().ajax().update("breadcrumbFormId");
                    break;
            }
        } catch (Exception e) {
            Logger.getLogger(AlarmsBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Update the breadcrumbModel field from the NavigationBean
     */
    @Override
    public void updateBreadcrumbModel() {
        DefaultMenuItem index;

        if (navigationBean != null && userManageBean != null) {
            
            // Set Root
            navigationBean.setBreadcrumbModel(new DefaultMenuModel());
            index = new DefaultMenuItem();
            index.setValue(userManageBean.getResourceBundleString("breadcrumb_2"));
            index.setIcon("pi pi-fw pi-bell");
            index.setCommand("#{alarmsBean.resetPage()}");
            index.setOncomplete("updateTreeNodes()");
            navigationBean.getBreadcrumbModel().getElements().add(index);

            try {
                // Set Region
                if (selectedFilters.getRegion() != null && !selectedFilters.getRegion().isEmpty()) {
                    updateBreadcrumbModelHelper(selectedFilters.getRegion(), "#{alarmsBean.onRegionChange(true)}", navigationBean.getBreadcrumbModel().getElements());
                }

                // Set Country
                if (selectedFilters.getCountry() != null && !selectedFilters.getCountry().isEmpty()) {
                    updateBreadcrumbModelHelper(selectedFilters.getCountry(), "#{alarmsBean.onCountryChange(true)}", navigationBean.getBreadcrumbModel().getElements());
                }

                // Set Site
                if (selectedFilters.getSite() != null && selectedFilters.getSite().getLabel() != null && !selectedFilters.getSite().getLabel().isEmpty()) {
                    updateBreadcrumbModelHelper(selectedFilters.getSite().getLabel(), "#{alarmsBean.onSiteChange(true)}", navigationBean.getBreadcrumbModel().getElements());
                }

                // Set Area
                if (selectedFilters.getArea() != null && selectedFilters.getArea().getLabel() != null && !selectedFilters.getArea().getLabel().isEmpty()) {
                    updateBreadcrumbModelHelper(selectedFilters.getArea().getLabel(), "#{alarmsBean.onAreaChangeFromFilters()}", navigationBean.getBreadcrumbModel().getElements());
                }
                // Set Asset
                if (selectedFilters.getAsset() != null && selectedFilters.getAsset().getLabel() != null && !selectedFilters.getAsset().getLabel().isEmpty()) {
                    updateBreadcrumbModelHelper(selectedFilters.getAsset().getLabel(), "#{alarmsBean.onAssetChange(true)}", navigationBean.getBreadcrumbModel().getElements());
                }

                // Set Point Location
                if (selectedFilters.getPointLocation() != null && selectedFilters.getPointLocation().getLabel() != null && !selectedFilters.getPointLocation().getLabel().isEmpty()) {
                    updateBreadcrumbModelHelper(selectedFilters.getPointLocation().getLabel(), "#{alarmsBean.onPointLocationChange(true)}", navigationBean.getBreadcrumbModel().getElements());
                }

                // Set Point
                if (selectedFilters.getPoint() != null && selectedFilters.getPoint().getLabel() != null && !selectedFilters.getPoint().getLabel().isEmpty()) {
                    updateBreadcrumbModelHelper(selectedFilters.getPoint().getLabel(), "#{alarmsBean.onPointChange(true)}", navigationBean.getBreadcrumbModel().getElements());
                }
            } catch (Exception e) {
                Logger.getLogger(ConfigurationBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);

                // Set Root
                navigationBean.setBreadcrumbModel(new DefaultMenuModel());
                index = new DefaultMenuItem();
                index.setValue(userManageBean.getResourceBundleString("breadcrumb_2"));
                index.setIcon("pi pi-fw pi-bell");
                index.setCommand("#{alarmsBean.resetPage()}");
                index.setOncomplete("updateTreeNodes()");
                navigationBean.getBreadcrumbModel().getElements().add(index);
            }
            updateUI("breadcrumb");
        }
    }

    /**
     * Clear the current row selections
     */
    public void clearRowSelection() {
        selectedAlarms = new ArrayList();
        PrimeFaces.current().executeScript("PF('dtAlarms').unselectAllRows()");
        PrimeFaces.current().ajax().update("alarm-form:dt-alarms-area");
        PrimeFaces.current().ajax().update(":alarm-form:delete-alarm-button");
        PrimeFaces.current().ajax().update(":alarm-form:acknowledge-alarm-button");
        PrimeFaces.current().ajax().update(":alarm-form:delete-alarm-button1");
        PrimeFaces.current().ajax().update(":alarm-form:acknowledge-alarm-button1");
    }

    /**
     * Left Tree builder
     *
     * @param type, String Object
     * @param data, Object
     * @param parent, TreeNode Object
     * @param list, List Object of Objects
     */
    @Override
    public void treeContentMenuBuilder(String type, Object data, TreeNode parent, List<Object> list) {
        TreeNode newNode;
        boolean useList = false;
        List<AlarmsVO> alarmCountsList;

        if (navigationBean != null && userManageBean != null) {
            if (type != null) {
                switch (type.toLowerCase()) {
                    case ROOT:
                        newNode = new DefaultTreeNode(ROOT, data, null);
                        navigationBean.setTreeNodeRoot(newNode);
                        if (regionCountrySiteMap.keySet() != null && !regionCountrySiteMap.keySet().isEmpty()) {
                            regionCountrySiteMap.keySet().stream().sorted().collect(Collectors.toList()).forEach(key -> treeContentMenuBuilder(REGION, key, navigationBean.getTreeNodeRoot(), null));
                        }
                        updateUI("tree");
                        break;
                    case REGION:
                        list = (data == null) ? new ArrayList(regionCountrySiteMap.keySet().stream().sorted().collect(Collectors.toList())) : new ArrayList();
                        treeContentMenuBuilderHelper(type, data, parent, list, true);
                        break;
                    case COUNTRY:
                        if (parent != null) {
                            list = (data == null) ? new ArrayList(regionCountrySiteMap.get((String) parent.getData()).keySet().stream().sorted().collect(Collectors.toList())) : new ArrayList();
                            treeContentMenuBuilderHelper(type, data, parent, list, true);
                        }
                        break;
                    case SITE_OVER_100:
                        if (parent != null) {
                            list = new ArrayList(regionCountrySiteMap.get((String) parent.getParent().getData()).get((String) parent.getData()).stream().sorted().collect(Collectors.toList()));
                            treeContentMenuBuilderHelper(type, data, SITE, parent, list, true);
                        }
                        break;
                    case SITE:
                        if (parent != null) {
                            list = (data == null) ? new ArrayList(regionCountrySiteMap.get((String) parent.getParent().getData()).get((String) parent.getData()).stream().sorted(Comparator.comparing(SelectItem::getLabel)).collect(Collectors.toList())) : new ArrayList();
                            treeContentMenuBuilderHelper(type, data, parent, list, true);
                        }
                        break;
                    case AREA_OVER_100:
                        if (list == null || list.isEmpty()) {
                            list = areaList != null ? new ArrayList(areaList) : new ArrayList();
                        } else {
                            useList = true;
                        }
                        treeContentMenuBuilderHelper(type, data, AREA, parent, list, useList);
                        break;
                    case AREA:
                        if (list == null || list.isEmpty()) {
                            list = areaList != null ? new ArrayList(areaList) : new ArrayList();
                        }

                        // Setting count for area
                        if (!list.isEmpty() && list.get(0) instanceof SelectItem) {
                            try {
                                if ((alarmCountsList = getAlarmsCountsList(parent)) != null && !alarmCountsList.isEmpty()) {
                                    list.stream().forEachOrdered(item -> {
                                        AtomicInteger count = new AtomicInteger(0);

                                        alarmCountsList.stream().filter(alarmCounts -> alarmCounts.getAlarmCount() > 0 && alarmCounts.getAreaId().equals((UUID) ((SelectItem) item).getValue())).forEachOrdered(alarmCounts -> count.set(count.get() + alarmCounts.getAlarmCount()));
                                        if (count.get() > 0) {
                                            ((SelectItem) item).setDescription(String.valueOf(count.get()));
                                        } else if (((SelectItem) item).getDescription() != null) {
                                            ((SelectItem) item).setDescription(null);
                                        }
                                    });
                                }
                            } catch (Exception e) {
                                Logger.getLogger(AlarmsBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
                            }
                        }
                        
                        treeContentMenuBuilderHelper(type, data, parent, list, true);
                        break;
                    case ASSET_OVER_100:
                        if (list == null || list.isEmpty()) {
                            list = assetList != null ? new ArrayList(assetList) : new ArrayList();
                        } else {
                            useList = true;
                        }
                        treeContentMenuBuilderHelper(type, data, ASSET, parent, list, useList);
                        break;
                    case ASSET:
                        if (list == null || list.isEmpty()) {
                            list = assetList != null ? new ArrayList(assetList) : new ArrayList();
                        }

                        // Setting count for asset
                        if (!list.isEmpty() && list.get(0) instanceof SelectItem) {
                            try {
                                if ((alarmCountsList = getAlarmsCountsList(parent)) != null && !alarmCountsList.isEmpty()) {
                                    list.stream().forEachOrdered(item -> {
                                        AtomicInteger count = new AtomicInteger(0);

                                        alarmCountsList.stream().filter(alarmCounts -> alarmCounts.getAlarmCount() > 0 && alarmCounts.getAssetId().equals((UUID) ((SelectItem) item).getValue())).forEachOrdered(alarmCounts -> count.set(count.get() + alarmCounts.getAlarmCount()));
                                        if (count.get() > 0) {
                                            ((SelectItem) item).setDescription(String.valueOf(count.get()));
                                        } else if (((SelectItem) item).getDescription() != null) {
                                            ((SelectItem) item).setDescription(null);
                                        }
                                    });
                                }
                            } catch (Exception e) {
                                Logger.getLogger(AlarmsBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
                            }
                        }
                        
                        treeContentMenuBuilderHelper(type, data, parent, list, true);
                        break;
                    case POINT_LOCATION_OVER_100:
                        if (list == null || list.isEmpty()) {
                            list = pointLocationList != null ? new ArrayList(pointLocationList) : new ArrayList();
                        } else {
                            useList = true;
                        }
                        treeContentMenuBuilderHelper(type, data, POINT_LOCATION, parent, list, useList);
                        break;
                    case POINT_LOCATION:
                        if (list == null || list.isEmpty()) {
                            list = pointLocationList != null ? new ArrayList(pointLocationList) : new ArrayList();
                        }
                        
                        // Setting count for point_location
                        if (!list.isEmpty() && list.get(0) instanceof SelectItem) {
                            try {
                                if ((alarmCountsList = getAlarmsCountsList(parent)) != null && !alarmCountsList.isEmpty()) {
                                    list.stream().forEachOrdered(item -> {
                                        AtomicInteger count = new AtomicInteger(0);

                                        alarmCountsList.stream().filter(alarmCounts -> alarmCounts.getAlarmCount() > 0 && alarmCounts.getPointLocationId().equals((UUID) ((SelectItem) item).getValue())).forEachOrdered(alarmCounts -> count.set(count.get() + alarmCounts.getAlarmCount()));
                                        if (count.get() > 0) {
                                            ((SelectItem) item).setDescription(String.valueOf(count.get()));
                                        } else if (((SelectItem) item).getDescription() != null) {
                                            ((SelectItem) item).setDescription(null);
                                        }
                                    });
                                }
                            } catch (Exception e) {
                                Logger.getLogger(AlarmsBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
                            }
                        }

                        treeContentMenuBuilderHelper(type, data, parent, list, true);
                        break;
                    case POINT_OVER_100:
                        if (list == null || list.isEmpty()) {
                            list = pointList != null ? new ArrayList(pointList) : new ArrayList();
                        } else {
                            useList = true;
                        }
                        treeContentMenuBuilderHelper(type, data, POINT, parent, list, useList);
                        break;
                    case POINT:
                        if (list == null || list.isEmpty()) {
                            list = pointList != null ? new ArrayList(pointList) : new ArrayList();
                        }

                        // Setting count for point
                        if (!list.isEmpty() && list.get(0) instanceof SelectItem) {
                            try {
                                if ((alarmCountsList = getAlarmsCountsList(parent)) != null && !alarmCountsList.isEmpty()) {
                                    list.stream().forEachOrdered(item -> {
                                        alarmCountsList.stream().filter(alarmCounts -> alarmCounts.getAlarmCount() > 0 && alarmCounts.getPointId().equals((UUID) ((SelectItem) item).getValue())).forEachOrdered(alarmCounts -> {
                                            if (alarmCounts.getAlarmCount() > 0) {
                                                ((SelectItem) item).setDescription(String.valueOf(alarmCounts.getAlarmCount()));
                                            } else if (((SelectItem) item).getDescription() != null) {
                                                ((SelectItem) item).setDescription(null);
                                            }
                                        });
                                    });
                                }
                            } catch (Exception e) {
                                Logger.getLogger(AlarmsBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
                            }
                        }
                        
                        treeContentMenuBuilderHelper(type, data, parent, list, false);
                        break;
                    case EMPTY:
                        if (parent != null) {
                            newNode = new DefaultTreeNode(type, data, parent);
                            newNode.setSelectable(false);
                        }
                        break;
                }
            } else {
                navigationBean.setTreeNodeRoot(new DefaultTreeNode(ROOT, new SelectItem(null, userManageBean.getResourceBundleString("breadcrumb_2")), null));
                updateUI("tree");
            }
        }
    }

    /**
     * Used for the NodeExpandEvent caused by the Nav Tree component
     *
     * @param event, NodeExpandEvent Object
     */
    @Override
    public void onNodeExpand(NodeExpandEvent event) {
        TreeNode parent;
        List<Object> list = new ArrayList();
        UUID siteId = null;
        UUID areaId = null;
        UUID assetId = null;
        UUID pointLocationId;

        try {
            if (event != null && userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null) {
                switch (event.getTreeNode().getType()) {
                    case REGION:
                        event.getTreeNode().getChildren().clear();
                        treeContentMenuBuilder(COUNTRY, null, event.getTreeNode(), null);
                        break;
                    case COUNTRY:
                        if (regionCountrySiteMap.get((String) event.getTreeNode().getParent().getData()).get((String) event.getTreeNode().getData()).size() > 100) {
                            event.getTreeNode().getChildren().clear();
                            treeContentMenuBuilder(SITE_OVER_100, null, event.getTreeNode(), null);
                        } else if (!regionCountrySiteMap.get((String) event.getTreeNode().getParent().getData()).get((String) event.getTreeNode().getData()).isEmpty()) {
                            event.getTreeNode().getChildren().clear();
                            treeContentMenuBuilder(SITE, null, event.getTreeNode(), null);
                        }
                        break;
                    case SITE:
                        siteId = (UUID) ((SelectItem) event.getTreeNode().getData()).getValue();
                        AreaClient.getInstance().getAreaSiteByCustomerSite(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), siteId).stream().sorted(Comparator.comparing(AreaVO::getAreaName)).forEachOrdered(vo -> list.add(new SelectItem(vo.getAreaId(), vo.getAreaName())));
                        if (list.size() > 100) {
                            event.getTreeNode().getChildren().clear();
                            treeContentMenuBuilder(AREA_OVER_100, null, event.getTreeNode(), list);
                        } else if (!list.isEmpty()) {
                            event.getTreeNode().getChildren().clear();
                            treeContentMenuBuilder(AREA, null, event.getTreeNode(), list);
                        }
                        break;
                    case AREA:
                        siteId = event.getTreeNode().getParent().getType().equals(SITE) ? (UUID) ((SelectItem) event.getTreeNode().getParent().getData()).getValue() : (UUID) ((SelectItem) event.getTreeNode().getParent().getParent().getData()).getValue();
                        areaId = (UUID) ((SelectItem) event.getTreeNode().getData()).getValue();
                        AssetClient.getInstance().getAssetSiteAreaByCustomerSiteArea(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), siteId, areaId).stream().sorted(Comparator.comparing(AssetVO::getAssetName)).forEachOrdered(vo -> list.add(new SelectItem(vo.getAssetId(), vo.getAssetName())));
                        if (list.size() > 100) {
                            event.getTreeNode().getChildren().clear();
                            treeContentMenuBuilder(ASSET_OVER_100, null, event.getTreeNode(), list);
                        } else if (!list.isEmpty()) {
                            event.getTreeNode().getChildren().clear();
                            treeContentMenuBuilder(ASSET, null, event.getTreeNode(), list);
                        }
                        break;
                    case ASSET:
                        parent = event.getTreeNode().getParent();
                        do {
                            if (parent.getType().equals(SITE)) {
                                siteId = (UUID) ((SelectItem) parent.getData()).getValue();
                            } else if (parent.getType().equals(AREA)) {
                                areaId = (UUID) ((SelectItem) parent.getData()).getValue();
                            }
                            parent = parent.getParent();
                        } while (siteId == null || areaId == null);
                        assetId = (UUID) ((SelectItem) event.getTreeNode().getData()).getValue();
                        PointLocationClient.getInstance().getPointLocationsByCustomerSiteAreaAsset(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), siteId, areaId, assetId).stream().sorted(Comparator.comparing(PointLocationVO::getPointLocationName)).forEachOrdered(vo -> list.add(new SelectItem(vo.getPointLocationId(), vo.getPointLocationName())));
                        if (list.size() > 100) {
                            event.getTreeNode().getChildren().clear();
                            treeContentMenuBuilder(POINT_LOCATION_OVER_100, null, event.getTreeNode(), list);
                        } else if (!list.isEmpty()) {
                            event.getTreeNode().getChildren().clear();
                            treeContentMenuBuilder(POINT_LOCATION, null, event.getTreeNode(), list);
                        }
                        break;
                    case POINT_LOCATION:
                        parent = event.getTreeNode().getParent();
                        do {
                            switch (parent.getType()) {
                                case SITE:
                                    siteId = (UUID) ((SelectItem) parent.getData()).getValue();
                                    break;
                                case AREA:
                                    areaId = (UUID) ((SelectItem) parent.getData()).getValue();
                                    break;
                                case ASSET:
                                    assetId = (UUID) ((SelectItem) parent.getData()).getValue();
                                    break;
                                default:
                                    break;
                            }
                            parent = parent.getParent();
                        } while (siteId == null || areaId == null || assetId == null);
                        pointLocationId = (UUID) ((SelectItem) event.getTreeNode().getData()).getValue();
                        PointClient.getInstance().getPointsByCustomerSiteAreaAssetPointLocation(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), siteId, areaId, assetId, pointLocationId, true).stream().sorted(Comparator.comparing(PointVO::getPointName)).forEachOrdered(vo -> list.add(new SelectItem(vo.getPointId(), vo.getPointName())));
                        if (list.size() > 100) {
                            event.getTreeNode().getChildren().clear();
                            treeContentMenuBuilder(POINT_OVER_100, null, event.getTreeNode(), list);
                        } else if (!list.isEmpty()) {
                            event.getTreeNode().getChildren().clear();
                            treeContentMenuBuilder(POINT, null, event.getTreeNode(), list);
                        }
                        break;
                }
            }
        } catch (Exception e) {
            Logger.getLogger(AlarmsBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Used for the NodeCollapseEvent caused by the Nav Tree component
     *
     * @param event, NodeCollapseEvent Object
     */
    @Override
    public void onNodeCollapse(NodeCollapseEvent event) {
        try {
            if (event != null && (event.getTreeNode().getType().equals(REGION) || event.getTreeNode().getType().equals(COUNTRY) || event.getTreeNode().getType().equals(SITE) || event.getTreeNode().getType().equals(AREA) || event.getTreeNode().getType().equals(ASSET) || event.getTreeNode().getType().equals(POINT_LOCATION))) {
                event.getTreeNode().getChildren().clear();
                treeContentMenuBuilder(EMPTY, null, event.getTreeNode(), null);
            }
        } catch (Exception e) {
            Logger.getLogger(AlarmsBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Used for the NodeSelectEvent caused by the Nav Tree component
     *
     * @param event, NodeSelectEvent Object
     */
    @Override
    public void onNodeSelect(NodeSelectEvent event) {
        TreeNode parent;
        final List<AreaVO> areaVOList = new ArrayList();
        final List<AssetVO> assetVOList = new ArrayList();
        final List<PointLocationVO> pointLocationVOList = new ArrayList();
        final List<PointVO> pointVOList = new ArrayList();

        try {
            if (event != null) {
                alarmType = null;
                
                parent = event.getTreeNode().getParent();
                do {
                    if (parent != null) {
                        switch (parent.getType()) {
                            case REGION:
                                selectedFilters.setRegion((String) parent.getData());
                                parent = null;
                                break;
                            case COUNTRY:
                                selectedFilters.setCountry((String) parent.getData());
                                break;
                            case SITE_OVER_100:
                                selectedFilters.setSiteOver100((String) parent.getData());
                                break;
                            case SITE:
                                selectedFilters.setSite((SelectItem) parent.getData());
                                break;
                            case AREA_OVER_100:
                                selectedFilters.setAreaOver100((String) parent.getData());
                                break;
                            case AREA:
                                selectedFilters.setArea((SelectItem) parent.getData());
                                break;
                            case ASSET_OVER_100:
                                selectedFilters.setAssetOver100((String) parent.getData());
                                break;
                            case ASSET:
                                selectedFilters.setAsset((SelectItem) parent.getData());
                                break;
                            case POINT_LOCATION_OVER_100:
                                selectedFilters.setPointLocationOver100((String) parent.getData());
                                break;
                            case POINT_LOCATION:
                                selectedFilters.setPointLocation((SelectItem) parent.getData());
                                break;
                            case POINT_OVER_100:
                                selectedFilters.setPointOver100((String) parent.getData());
                                break;
                        }
                        if (parent != null) {
                            parent = parent.getParent();
                        }
                    }
                } while (parent != null);

                switch (event.getTreeNode().getType()) {
                    case REGION:
                        selectedFilters.setRegion((String) event.getTreeNode().getData());
                        selectedFilters.clear(REGION);
                        areaList.clear();
                        assetList.clear();
                        pointLocationList.clear();
                        pointList.clear();
                        areaTileList.clear();
                        alarmsVOList.clear();
                        showHistory = false;
                        selectedAlarms = new ArrayList();
                        hideDataTable();
                        break;
                    case COUNTRY:
                        selectedFilters.setCountry((String) event.getTreeNode().getData());
                        selectedFilters.clear(COUNTRY);
                        areaList.clear();
                        assetList.clear();
                        pointLocationList.clear();
                        pointList.clear();
                        areaTileList.clear();
                        alarmsVOList.clear();
                        showHistory = false;
                        selectedAlarms = new ArrayList();
                        hideDataTable();
                        break;
                    case SITE:
                        selectedFilters.setSite((SelectItem) event.getTreeNode().getData());
                        selectedFilters.clear(SITE);
                        areaList.clear();
                        assetList.clear();
                        pointLocationList.clear();
                        pointList.clear();
                        alarmsVOList.clear();
                        showHistory = false;
                        selectedAlarms = new ArrayList();
                        hideDataTable();
                        SelectItem item = (SelectItem) event.getTreeNode().getData();
                        alarmsVO.setSiteName(item.getLabel());
                        alarmsVO.setSiteId(UUID.fromString(item.getValue().toString()));
                        try {
                            areaList.clear();
                            areaVOList.addAll(AreaClient.getInstance().getAreaSiteByCustomerSite(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue()));
                            areaVOList.stream().sorted(Comparator.comparing(AreaVO::getAreaName)).forEachOrdered(area -> areaList.add(new SelectItem(area.getAreaId(), area.getAreaName())));
                        } catch (Exception ex) {
                            areaList.clear();
                        }
                        setAreaTileList((UUID) selectedFilters.getSite().getValue());
                        break;
                    case AREA:
                        selectedFilters.setArea((SelectItem) event.getTreeNode().getData());
                        selectedFilters.clear(AREA);
                        pointLocationList.clear();
                        pointList.clear();
                        areaTileList.clear();
                        showHistory = false;
                        selectedAlarms = new ArrayList();
                        try {
                            areaList.clear();
                            areaVOList.addAll(AreaClient.getInstance().getAreaSiteByCustomerSite(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue()));
                            areaVOList.stream().sorted(Comparator.comparing(AreaVO::getAreaName)).forEachOrdered(area -> areaList.add(new SelectItem(area.getAreaId(), area.getAreaName())));
                        } catch (Exception ex) {
                            areaList.clear();
                        }
                        try {
                            assetList.clear();
                            assetVOList.addAll(AssetClient.getInstance().getAssetSiteAreaByCustomerSiteArea(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue()));
                            assetVOList.stream().sorted(Comparator.comparing(AssetVO::getAssetName)).forEachOrdered(asset -> assetList.add(new SelectItem(asset.getAssetId(), asset.getAssetName())));
                        } catch (Exception ex) {
                            assetList.clear();
                        }
                         try {
                            pointLocationList.clear();
                            pointLocationVOList.addAll(PointLocationClient.getInstance().getPointLocationsByCustomerSiteArea(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue()));
                            pointLocationVOList.stream().sorted(Comparator.comparing(PointLocationVO::getPointLocationName)).forEachOrdered(pointLocation -> pointLocationList.add(new SelectItem(pointLocation.getPointLocationId(), pointLocation.getPointLocationName())));
                        } catch (Exception ex) {
                            pointLocationList.clear();
                        }
                        updateAlarmsVOListByAreaId();
                        break;
                    case ASSET:
                        selectedFilters.setAsset((SelectItem) event.getTreeNode().getData());
                        selectedFilters.clear(ASSET);
                        pointList.clear();
                        areaTileList.clear();
                        showHistory = false;
                        selectedAlarms = new ArrayList();
                        try {
                            areaList.clear();
                            areaVOList.addAll(AreaClient.getInstance().getAreaSiteByCustomerSite(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue()));
                            areaVOList.stream().sorted(Comparator.comparing(AreaVO::getAreaName)).forEachOrdered(area -> areaList.add(new SelectItem(area.getAreaId(), area.getAreaName())));
                        } catch (Exception ex) {
                            areaList.clear();
                        }
                        try {
                            assetList.clear();
                            assetVOList.addAll(AssetClient.getInstance().getAssetSiteAreaByCustomerSiteArea(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue()));
                            assetVOList.stream().sorted(Comparator.comparing(AssetVO::getAssetName)).forEachOrdered(asset -> assetList.add(new SelectItem(asset.getAssetId(), asset.getAssetName())));
                        } catch (Exception ex) {
                            assetList.clear();
                        }
                        try {
                            pointLocationList.clear();
                            pointLocationVOList.addAll(PointLocationClient.getInstance().getPointLocationsByCustomerSiteAreaAsset(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue(), (UUID) selectedFilters.getAsset().getValue()));
                            pointLocationVOList.stream().sorted(Comparator.comparing(PointLocationVO::getPointLocationName)).forEachOrdered(pointLocation -> pointLocationList.add(new SelectItem(pointLocation.getPointLocationId(), pointLocation.getPointLocationName())));
                        } catch (Exception ex) {
                            pointLocationList.clear();
                        }
                        updateAlarmsVOListByAssetId();
                        break;
                    case POINT_LOCATION:
                        selectedFilters.setPointLocation((SelectItem) event.getTreeNode().getData());
                        selectedFilters.clear(POINT_LOCATION);
                        areaTileList.clear();
                        model = new TimelineModel<>();
                        showHistory = false;
                        selectedAlarms = new ArrayList();
                        try {
                            areaList.clear();
                            areaVOList.addAll(AreaClient.getInstance().getAreaSiteByCustomerSite(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue()));
                            areaVOList.stream().sorted(Comparator.comparing(AreaVO::getAreaName)).forEachOrdered(area -> areaList.add(new SelectItem(area.getAreaId(), area.getAreaName())));
                        } catch (Exception ex) {
                            areaList.clear();
                        }
                        try {
                            assetList.clear();
                            assetVOList.addAll(AssetClient.getInstance().getAssetSiteAreaByCustomerSiteArea(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue()));
                            assetVOList.stream().sorted(Comparator.comparing(AssetVO::getAssetName)).forEachOrdered(asset -> assetList.add(new SelectItem(asset.getAssetId(), asset.getAssetName())));
                        } catch (Exception ex) {
                            assetList.clear();
                        }
                        try {
                            pointLocationList.clear();
                            pointLocationVOList.addAll(PointLocationClient.getInstance().getPointLocationsByCustomerSiteAreaAsset(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue(), (UUID) selectedFilters.getAsset().getValue()));
                            pointLocationVOList.stream().sorted(Comparator.comparing(PointLocationVO::getPointLocationName)).forEachOrdered(pointLocation -> pointLocationList.add(new SelectItem(pointLocation.getPointLocationId(), pointLocation.getPointLocationName())));
                        } catch (Exception ex) {
                            pointLocationList.clear();
                        }
                        try {
                            pointList.clear();
                            pointVOList.addAll(PointClient.getInstance().getPointsByCustomerSiteAreaAssetPointLocation(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue(), (UUID) selectedFilters.getAsset().getValue(), (UUID) selectedFilters.getPointLocation().getValue(), true));
                            pointVOList.stream().sorted(Comparator.comparing(PointVO::getPointName)).forEachOrdered(point -> pointList.add(new SelectItem(point.getPointId(), point.getPointName())));
                        } catch (Exception ex) {
                            pointList.clear();
                        }

                        updateAlarmsVOListByPointLocationId();
                        break;
                    case POINT:
                        selectedFilters.setPoint((SelectItem) event.getTreeNode().getData());
                        selectedFilters.clear(POINT);
                        areaTileList.clear();
                        showHistory = false;
                        selectedAlarms = new ArrayList();
                        try {
                            areaList.clear();
                            areaVOList.addAll(AreaClient.getInstance().getAreaSiteByCustomerSite(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue()));
                            areaVOList.stream().sorted(Comparator.comparing(AreaVO::getAreaName)).forEachOrdered(area -> areaList.add(new SelectItem(area.getAreaId(), area.getAreaName())));
                        } catch (Exception ex) {
                            areaList.clear();
                        }
                        try {
                            assetList.clear();
                            assetVOList.addAll(AssetClient.getInstance().getAssetSiteAreaByCustomerSiteArea(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue()));
                            assetVOList.stream().sorted(Comparator.comparing(AssetVO::getAssetName)).forEachOrdered(asset -> assetList.add(new SelectItem(asset.getAssetId(), asset.getAssetName())));
                        } catch (Exception ex) {
                            assetList.clear();
                        }
                        try {
                            pointLocationList.clear();
                            pointLocationVOList.addAll(PointLocationClient.getInstance().getPointLocationsByCustomerSiteArea(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue()));
                            pointLocationVOList.stream().sorted(Comparator.comparing(PointLocationVO::getPointLocationName)).forEachOrdered(pointLocation -> pointLocationList.add(new SelectItem(pointLocation.getPointLocationId(), pointLocation.getPointLocationName())));
                        } catch (Exception ex) {
                            pointLocationList.clear();
                        }
                        try {
                            pointList.clear();
                            pointVOList.addAll(PointClient.getInstance().getPointsByCustomerSiteAreaAssetPointLocation(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue(), (UUID) selectedFilters.getAsset().getValue(), (UUID) selectedFilters.getPointLocation().getValue(), true));
                            pointVOList.stream().sorted(Comparator.comparing(PointVO::getPointName)).forEachOrdered(point -> pointList.add(new SelectItem(point.getPointId(), point.getPointName())));
                        } catch (Exception ex) {
                            pointList.clear();
                        }
                        updateAlarmsVOListByPointId();
                        break;
                }
                updateBreadcrumbModel();
                updateUI("content");
                clearDataTables();
            }
        } catch (Exception e) {
            Logger.getLogger(AlarmsBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Sets the given field regionCountrySiteMap
     */
    @Override
    public void setRegionCountrySiteMap() {
        try {
            regionCountrySiteMap.clear();

            if (userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null) {
                SiteClient.getInstance().getSiteRegionCountryByCustomer(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount()).forEach(siteVO -> {
                    if (userManageBean.getCurrentSite().getSiteId().equals(siteVO.getSiteId())) {
                        if (regionCountrySiteMap.containsKey(siteVO.getRegion())) {
                            if (regionCountrySiteMap.get(siteVO.getRegion()).containsKey(siteVO.getCountry())) {
                                if (!regionCountrySiteMap.get(siteVO.getRegion()).get(siteVO.getCountry()).stream().anyMatch(site -> site.getValue().equals(siteVO.getSiteId()))) {
                                    regionCountrySiteMap.get(siteVO.getRegion()).get(siteVO.getCountry()).add(new SelectItem(siteVO.getSiteId(), siteVO.getSiteName()));
                                }
                            } else {
                                regionCountrySiteMap.get(siteVO.getRegion()).put(siteVO.getCountry(), new ArrayList());
                                regionCountrySiteMap.get(siteVO.getRegion()).get(siteVO.getCountry()).add(new SelectItem(siteVO.getSiteId(), siteVO.getSiteName()));
                            }
                        } else {
                            regionCountrySiteMap.put(siteVO.getRegion(), new HashMap());
                            regionCountrySiteMap.get(siteVO.getRegion()).put(siteVO.getCountry(), new ArrayList());
                            regionCountrySiteMap.get(siteVO.getRegion()).get(siteVO.getCountry()).add(new SelectItem(siteVO.getSiteId(), siteVO.getSiteName()));
                        }
                    }
                });
            }
        } catch (Exception ex) {
            regionCountrySiteMap.clear();
        }
    }

    /**
     * Update area when site changes.
     */
    public void updateAlarmsVOListByAreaId() {
        AtomicInteger count;
        List<AlarmsVO> list;
        
        try {
            alarmsVOList.clear();
            if (selectedFilters != null && selectedFilters.getSite() != null && selectedFilters.getArea() != null) {
                list = new ArrayList();
                list.addAll(AlarmsClient.getInstance().getAlarmsByCustomerSiteArea(ServiceRegistrarClient.getInstance().getServiceHostURL(ALARM_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue()));
                list = sortAlarmsByTemparature(list);
                alarmsVOList.addAll(list.stream().filter(vo -> !vo.isAcknowledged()).collect(Collectors.toList()));
                count = new AtomicInteger(0);
                alarmsVOList.stream().forEachOrdered(vo -> {
                    vo.setAreaName(selectedFilters.getArea().getLabel());
                    vo.setTrendValueFormatted(Float.valueOf(DECIMAL_FORMAT.format(vo.getTrendValue())));
                    vo.setIndex(count.incrementAndGet());
                    assetList.stream().filter(selectItem -> (vo.getAssetId().compareTo((UUID) selectItem.getValue()) == 0)).forEachOrdered(selectItem -> vo.setAssetName(selectItem.getLabel()));
                    pointLocationList.stream().filter(selectItem -> (vo.getPointLocationId().compareTo((UUID) selectItem.getValue()) == 0)).forEachOrdered(selectItem -> vo.setPointLocationName(selectItem.getLabel()));
                });
            }
            
            displayAreaTable = true;
            displayAssetTable = false;
            displayPLTable = false;
            displayPointTable = false;
        } catch (Exception e) {
            Logger.getLogger(AlarmsBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Update AlarmsVOList when Area changes.
     */
    public void updateAlarmsVOListByAssetId() {
        AtomicInteger count;
        List<AlarmsVO> list;
        
        try {
            alarmsVOList.clear();
            if (selectedFilters != null && selectedFilters.getSite() != null && selectedFilters.getArea() != null && selectedFilters.getAsset() != null) {
                list = sortAlarmsByTemparature(AlarmsClient.getInstance().getAlarmsByCustomerSiteAreaAsset(ServiceRegistrarClient.getInstance().getServiceHostURL(ALARM_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue(), (UUID) selectedFilters.getAsset().getValue()));
                alarmsVOList.addAll(list.stream().filter(vo -> !vo.isAcknowledged()).collect(Collectors.toList()));
                count = new AtomicInteger(0);
                alarmsVOList.stream().forEachOrdered(vo -> {
                    vo.setTrendValueFormatted(Float.valueOf(DECIMAL_FORMAT.format(vo.getTrendValue())));
                    vo.setAreaName(selectedFilters.getArea().getLabel());
                    vo.setAssetName(selectedFilters.getAsset().getLabel());
                    vo.setPointName(null);
                    vo.setIndex(count.incrementAndGet());
                    pointLocationList.stream().filter(selectItem -> (vo.getPointLocationId().compareTo((UUID) selectItem.getValue()) == 0)).forEachOrdered(selectItem -> vo.setPointLocationName(selectItem.getLabel()));
                });
            }
            displayAreaTable = false;
            displayAssetTable = true;
            displayPLTable = false;
            displayPointTable = false;
        } catch (Exception e) {
            Logger.getLogger(AlarmsBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Update AlarmsVOList when Asset changes.
     */
    public void updateAlarmsVOListByPointLocationId() {
        AtomicInteger count;
        List<AlarmsVO> list;

        try {
            alarmsVOList.clear();
            if (selectedFilters != null && selectedFilters.getSite() != null && selectedFilters.getArea() != null && selectedFilters.getAsset() != null && selectedFilters.getPointLocation() != null) {
                list = sortAlarmsByTemparature(AlarmsClient.getInstance().getAlarmsByCustomerSiteAreaAssetPointLocation(ServiceRegistrarClient.getInstance().getServiceHostURL(ALARM_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue(), (UUID) selectedFilters.getAsset().getValue(), (UUID) selectedFilters.getPointLocation().getValue()));
                alarmsVOList.addAll(list.stream().filter(vo -> !vo.isAcknowledged()).collect(Collectors.toList()));
                count = new AtomicInteger(0);
                alarmsVOList.stream().forEachOrdered(vo -> {
                    vo.setTrendValueFormatted(Float.valueOf(DECIMAL_FORMAT.format(vo.getTrendValue())));
                    vo.setAreaName(selectedFilters.getArea().getLabel());
                    vo.setAssetName(selectedFilters.getAsset().getLabel());
                    vo.setPointLocationName(selectedFilters.getPointLocation().getLabel());
                    vo.setIndex(count.incrementAndGet());
                    pointList.stream().filter(selectItem -> (vo.getPointId().compareTo((UUID) selectItem.getValue()) == 0)).forEachOrdered(selectItem -> vo.setPointName(selectItem.getLabel()));
                });
            }
            displayAreaTable = false;
            displayAssetTable = false;
            displayPLTable = true;
            displayPointTable = false;
        } catch (Exception e) {
            Logger.getLogger(AlarmsBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Update AlarmsVOList when Asset changes.
     */
    public void updateAlarmsVOListByPointId() {
        AtomicInteger count;
        List<AlarmsVO> list;

        try {
            alarmsVOList.clear();
            if (selectedFilters != null && selectedFilters.getSite() != null && selectedFilters.getArea() != null && selectedFilters.getAsset() != null && selectedFilters.getPointLocation() != null && selectedFilters.getPoint() != null) {
                list = sortAlarmsByTemparature(AlarmsClient.getInstance().getAlarmsByCustomerSiteAreaAssetPointLocationPoint(ServiceRegistrarClient.getInstance().getServiceHostURL(ALARM_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue(), (UUID) selectedFilters.getAsset().getValue(), (UUID) selectedFilters.getPointLocation().getValue(), (UUID) selectedFilters.getPoint().getValue()));
                alarmsVOList.addAll(list.stream().filter(vo -> !vo.isAcknowledged()).collect(Collectors.toList()));
                count = new AtomicInteger(0);
                alarmsVOList.stream().forEachOrdered(vo -> {
                    vo.setTrendValueFormatted(Float.valueOf(DECIMAL_FORMAT.format(vo.getTrendValue())));
                    vo.setAreaName(selectedFilters.getArea().getLabel());
                    vo.setAssetName(selectedFilters.getAsset().getLabel());
                    vo.setPointLocationName(selectedFilters.getPointLocation().getLabel());
                    vo.setPointName(selectedFilters.getPoint().getLabel());
                    vo.setIndex(count.incrementAndGet());
                });
            }
            displayAreaTable = false;
            displayAssetTable = false;
            displayPLTable = false;
            displayPointTable = true;
        } catch (Exception e) {
            Logger.getLogger(AlarmsBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * sort Temperature items first and then place the remaining items in the list
     * 
     * @param list of AlarmsVO
     * @return list of AlarmsVO Objects
     */
    private List<AlarmsVO> sortAlarmsByTemparature(List<AlarmsVO> list){
        List<AlarmsVO> tempList, withoutTempList;
        
        try {
            if ((tempList = list.stream().filter(vo -> vo.getSensorType().equalsIgnoreCase("Temperature")).collect(Collectors.toList())) != null && !tempList.isEmpty() &&
                    (withoutTempList = list.stream().filter(vo -> !vo.getSensorType().equalsIgnoreCase("Temperature")).collect(Collectors.toList())) != null && !withoutTempList.isEmpty()) {
                list = new ArrayList();
                list.addAll(tempList.stream().filter(vo -> vo.getAlarmType().contains("FAULT")).collect(Collectors.toList()));
                list.addAll(tempList.stream().filter(vo -> vo.getAlarmType().contains("ALERT")).collect(Collectors.toList()));
                list.addAll(withoutTempList);
            }
        } catch (Exception e) {
            Logger.getLogger(AlarmsBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    
        return list;
    }
    
    /**
     * Return temperature row style class Strings
     * 
     * @param alarmsVO, AlarmsVO Object
     * @return String Object
     */
    public String setTemperatureRowStyleClass(AlarmsVO alarmsVO) {
        if (alarmsVO != null && alarmsVO.getSensorType() != null && alarmsVO.getSensorType().equalsIgnoreCase("Temperature") && alarmsVO.getAlarmType() != null) {
            if (alarmsVO.getAlarmType().contains("ALERT")) {
                return "row-temperature-alert";
            } else if (alarmsVO.getAlarmType().contains("FAULT")) {
                return "row-temperature-fault";
            }
        }
        return "";
    }
    
    /**
     * Return the AlarmCounts List Object based on the given TreeNode values or Site UUID
     *
     * @param siteId, UUID Object
     * @return List Object of AlarmsVO Objects
     */
    private List<AlarmsVO> getAlarmsCountsList(Object value) {
        List<AlarmsVO> list = new ArrayList();
        UUID siteId = null;
        TreeNode parent;

        if (value != null) {
            if (value instanceof TreeNode) {
                parent = (TreeNode) value;
                do {
                    try {
                        switch (parent.getType()) {
                            case SITE:
                                siteId = ((UUID) ((SelectItem) parent.getData()).getValue());
                                break;
                            default:
                                parent = parent.getParent();
                                break;
                        }
                    } catch (Exception ex) {
                        break;
                    }
                } while (parent != null && siteId == null);
            } else if (value instanceof UUID) {
                siteId = (UUID) value;
            }

            if (userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null && siteId != null) {
                if (alarmsCountsMap.get(siteId) != null) {
                    list = alarmsCountsMap.get(siteId);
                } else {
                    list = AlarmsClient.getInstance().getAlarmCountByCustomerSite(ServiceRegistrarClient.getInstance().getServiceHostURL(ALARM_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), siteId);
                    alarmsCountsMap.put(siteId, list);
                }
            }
        }
        return list;
    }

    /**
     * Called when a area tile is selected from the areaTileList field
     *
     * @param alarmsVO, AlarmsVO Object
     * @param alarmType
     */
    public void onAreaTileSelect(AlarmsVO alarmsVO, String alarmType) {
        if (alarmsVO != null && alarmsVO.getAreaId() != null && alarmsVO.getAreaName() != null && !alarmsVO.getAreaName().isEmpty()) {
            areaFilterSelect = alarmsVO.getAreaName();
            this.alarmType = alarmType;
            selectedFilters.setArea(new SelectItem(alarmsVO.getAreaId(), alarmsVO.getAreaName(), String.valueOf(alarmsVO.getAlarmCount())));
            onAreaChange(true);
        }
    }

    /**
     * Called when a area is selected from the Filter field
     */
    public void onAreaChangeFromFilters() {       
        alarmType = null; 
        onAreaChange(true);
    }
    
    /**
     * Called when a point is clicked
     *
     * @return a String of page name
     */
    public String onPointNameClik() {
        ConfigurationBean configurationBean;
        Filters configFilters;
        String value;

        value = navigationBean.updatePage("configuration");
        configurationBean = (ConfigurationBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("configurationBean");

        configFilters = configurationBean.getSelectedFilters();
        configFilters.setRegion(selectedFilters.getRegion());
        configFilters.setCountry(selectedFilters.getCountry());
        configFilters.setSiteOver100(selectedFilters.getSiteOver100());
        configFilters.setSite(selectedFilters.getSite());
        configFilters.setAreaOver100(selectedFilters.getAreaOver100());
        configFilters.setArea(selectedFilters.getArea());

        configurationBean.onAreaChange(false);

        configFilters.setAsset(new SelectItem(pointVO.getAssetId(), pointVO.getAssetName()));
        configurationBean.onAssetChange(false);

        configFilters.setPointLocation(new SelectItem(pointVO.getPointLocationId(), pointVO.getPointLocationName()));
        configurationBean.onPointLocationChange(false);

        configFilters.setPoint(new SelectItem(pointVO.getPointId(), pointVO.getPointName()));
        configurationBean.onPointChange(true);

        return value;
    }

    /**
     * Sets the areaTileList based on the Site Id
     *
     * @param siteId, UUID Object
     */
    private void setAreaTileList(UUID siteId) {
        List<AlarmsVO> list;

        areaTileList.clear();
        if (siteId != null && (list = getAlarmsCountsList(siteId)) != null && !list.isEmpty()) {
            try {
                list.stream().forEachOrdered(item -> {
                    AtomicInteger count, alertCount, faultCount;
                    List<SelectItem> selectItemList = null;
                    AlarmsVO avo;
                    
                    if (!areaTileList.stream().anyMatch(vo -> vo.getAreaId().equals(item.getAreaId())) && !areaList.isEmpty() && (selectItemList = areaList.stream().filter(selectItem -> item.getAreaId().equals((UUID) selectItem.getValue())).collect(Collectors.toList())) != null && !selectItemList.isEmpty()) {
                        count = new AtomicInteger(0);
                        alertCount = new AtomicInteger(0);
                        faultCount = new AtomicInteger(0);
                        
                        list.stream().filter(alarmCounts -> alarmCounts.getAlarmCount() > 0 && alarmCounts.getAreaId().equals(item.getAreaId())).forEachOrdered(alarmCounts -> { 
                            count.set(count.get() + alarmCounts.getAlarmCount());
                            alertCount.set(alertCount.get() + alarmCounts.getAlertCount());
                            faultCount.set(faultCount.get() + alarmCounts.getFaultCount());
                        });
                        
                        if (count.get() > 0) {
                            avo = new AlarmsVO();
                            avo.setAreaId(item.getAreaId());
                            avo.setAreaName(selectItemList.get(0).getLabel());
                            avo.setAlarmCount(count.get());
                            avo.setAlertCount(alertCount.get());
                            avo.setFaultCount(faultCount.get());
                            areaTileList.add(avo);
                        }
                    }
                });
            } catch (Exception e) {
                Logger.getLogger(AlarmsBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
                areaTileList.clear();
            }
        }
    }

    /**
     * Hide all dataTables
     */
    private void hideDataTable() {
        displayAreaTable = false;
        displayAssetTable = false;
        displayPLTable = false;
        displayPointTable = false;
    }

    /**
     * Delete button message.
     *
     * @return String Object
     */
    public String deleteButtonMessage() {
        if (userManageBean != null) {
            return (selectedAlarms != null && !selectedAlarms.isEmpty()) ? userManageBean.getResourceBundleString("label_delete_button_message") + " (" + selectedAlarms.size() + ")" : userManageBean.getResourceBundleString("label_delete");
        }
        return null;
    }

    /**
     * Acknowledge button message.
     *
     * @return String Object
     */
    public String acknowledgeButtonMessage() {
        if (userManageBean != null) {
            return (selectedAlarms != null && !selectedAlarms.isEmpty()) ? userManageBean.getResourceBundleString("label_acknowledge_button_message") + " (" + selectedAlarms.size() + ")" : userManageBean.getResourceBundleString("label_acknowledge");
        }
        return null;
    }

    /**
     * Delete the selected alarms.
     */
    public void deleteSelectedAlarms() {
    }

    /**
     * Delete the selected alarm.
     *
     * @param avo, AlarmsVO Object
     */
    public void deleteAlarm(AlarmsVO avo) {
    }

    /**
     * Acknowledge the selected alarms.
     */
    public void acknowledgeSelectedAlarms() {
        Logger.getLogger(AlarmsBean.class.getName()).log(Level.INFO, "AlarmsBean.acknowledgeSelectedAlarms() called. Alarms updated: {0}", selectedAlarms);
        
        if (selectedAlarms != null && !selectedAlarms.isEmpty()) {
            selectedAlarms.stream().forEach(avo -> avo.setAcknowledged(true));
            if (AlarmsClient.getInstance().updateAlarmList(ServiceRegistrarClient.getInstance().getServiceHostURL(ALARM_SERVICE_NAME), selectedAlarms)) {
                updateCount(false);
                selectedAlarms = new ArrayList();
                if (areaTileList.isEmpty()) {
                    PrimeFaces.current().executeScript("PF('dtAlarms').unselectAllRows()");
                }
            }
        }
    }

    /**
     * Update the Alarm Count
     * @param dontClearDataTableFilter, boolean
     */
    public void updateCount(boolean dontClearDataTableFilter) {
        alarmsCountsMap.clear();
        
        try {
            treeContentMenuBuilder(ROOT, userManageBean.getResourceBundleString("breadcrumb_2"), null, null);
            
            this.dontClearDataTableFilter = dontClearDataTableFilter;
            if (selectedFilters.getPoint() != null && selectedFilters.getPoint().getLabel() != null && !selectedFilters.getPoint().getLabel().isEmpty()) {
                onPointChange(true);
            } else if (selectedFilters.getPointLocation() != null && selectedFilters.getPointLocation().getLabel() != null && !selectedFilters.getPointLocation().getLabel().isEmpty()) {
                onPointLocationChange(true);
            } else if (selectedFilters.getAsset() != null && selectedFilters.getAsset().getLabel() != null && !selectedFilters.getAsset().getLabel().isEmpty()) {
                onAssetChange(true);
            } else {
                onAreaChange(true);
            }
        } catch (Exception e) {
            Logger.getLogger(WorkOrderBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
        this.dontClearDataTableFilter = false;
    }

    /**
     * Alarm History the selected alarm.
     *
     */
    public void viewAlarmHistory() {
        Set<String> pointNameList, paramNameList;
        TimelineGroup<String> childGroup;
        List<String> childGroupIds;
        List<AlarmsVO> historyList;
        int currentYear;

        alarmTypes = new ArrayList();
        currentYear = LocalDate.now().getYear();

        if (userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null && showHistory && selectedFilters.getPointLocation().getValue() != null && (historyList = AlarmsClient.getInstance().viewAlarmsHistory(ServiceRegistrarClient.getInstance().getServiceHostURL(ALARM_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue(), currentYear, (UUID) selectedFilters.getArea().getValue(), (UUID) selectedFilters.getAsset().getValue(), (UUID) selectedFilters.getPointLocation().getValue())) != null && historyList.size() > 0) {
            model = new TimelineModel<>();

            // Fetch point names
            pointNameList = new HashSet();
            historyList.forEach(alarmVO -> {
                pointList.stream().filter(selectItem -> (alarmVO.getPointId().compareTo((UUID) selectItem.getValue()) == 0)).forEachOrdered(selectItem -> {
                    alarmVO.setPointName(selectItem.getLabel());
                    pointNameList.add(selectItem.getLabel());
                });
            });

            // create nested groups (Main and child groups)
            // add nested groups to the model
            for (String pointName : pointNameList) {
                paramNameList = historyList.stream().filter(h -> pointName.equalsIgnoreCase(h.getPointName())).map(AlarmsVO::getParamName).collect(Collectors.toSet());
                childGroupIds = new ArrayList();
                for (String param : paramNameList) {
                    childGroup = new TimelineGroup(pointName + param, param, 2);
                    childGroupIds.add(pointName + param);
                    model.addGroup(childGroup);
                }
                model.addGroup(new TimelineGroup<>(pointName, pointName, pointName, 1, childGroupIds));
            }

            // iterate over groups
            // add data(event) to the model
            historyList.forEach(alarmVO -> {
                String timestamp, alarmStyleName;
                TimelineEvent event;
                LocalDateTime start;

                if ((timestamp = userManageBean.getTzOffsetString(alarmVO.getInstantDateTimestamp(), 2)) != null) {
                    try {
                        start = LocalDateTime.parse(timestamp, DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssZ"));
                        alarmStyleName = alarmVO.getAlarmType();
                        alarmStyleName = alarmStyleName.replaceAll("\\s", "");

                        event = TimelineEvent.<String>builder()
                                .data(alarmVO.getAlarmType())
                                .startDate(start)
                                .endDate(start.plusHours(4))
                                .editable(false)
                                .group(alarmVO.getPointName() + alarmVO.getParamName())
                                .title(alarmVO.getAlarmType())
                                .styleClass(alarmStyleName.toLowerCase())
                                .build();

                        model.add(event);
                    } catch (Exception ex) {
                        System.out.println("com.uptime.upcastcm.bean.AlarmsBean.getAlarmHistory()" + ex.getMessage());
                    }
                }
            });
            
        } else {
            model = null;
        }
        PrimeFaces.current().ajax().update("alarm-form");
    }

    /**
     * View the trend graphs based on the given data
     */
    public void viewTrendGraph() {
        Filters filters;
        AssetAnalysisWindowBean assetAnalysisWindowBean;
        String tmp;
        int index;
        AlarmsVO avo;
        Instant timeStamp;
        
        try {
            if ((tmp = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("alarm-form:indexId")) != null) {
                index = Integer.parseInt(tmp);
                avo = new AlarmsVO(alarmsVOList.stream().filter(vo -> vo.getIndex() == index).findFirst().get());
                timeStamp = avo.getInstantDateTimestamp();
                
                if ((assetAnalysisWindowBean = (AssetAnalysisWindowBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("assetAnalysisWindowBean")) == null) {
                    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("assetAnalysisWindowBean", new AssetAnalysisWindowBean());
                    assetAnalysisWindowBean = (AssetAnalysisWindowBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("assetAnalysisWindowBean");
                    assetAnalysisWindowBean.init();
                }

                loadPointDetails(avo);
                
                filters = new Filters();
                filters.setRegion(selectedFilters.getRegion());
                filters.setCountry(selectedFilters.getCountry());
                filters.setSite(selectedFilters.getSite());
                filters.setArea(selectedFilters.getArea());
                filters.setAsset(new SelectItem(pointVO.getAssetId(), pointVO.getAssetName()));
                filters.setPointLocation(new SelectItem(pointVO.getPointLocationId(), pointVO.getPointLocationName()));
                filters.setPoint(new SelectItem(pointVO.getPointId(), pointVO.getPointName()));
                filters.setApSet(new SelectItem(pointVO.getApSetId(), pointVO.getApSetName()));
                filters.setAlSetParam(new SelectItem(pointVO.getAlSetId(), avo.getParamName()));
                
                assetAnalysisWindowBean.setTimestamp(String.valueOf(timeStamp.toEpochMilli()));
                assetAnalysisWindowBean.loadTrendGraph(filters, true);
            }
        } catch (Exception ex) {
            Logger.getLogger(AlarmsBean.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        }
        
        
    }

    /**
     * Acknowledge the selected alarm.
     */
    public void acknowledgeAlarm() {
        String tmp;
        int index;
        AlarmsVO avo;
        
        try {
            if ((tmp = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("alarm-form:indexId")) != null) {
                index = Integer.parseInt(tmp);
                avo = alarmsVOList.stream().filter(vo -> vo.getIndex() == index).findFirst().get();
                avo.setAcknowledged(true);
                
                Logger.getLogger(AlarmsBean.class.getName()).log(Level.INFO, "AlarmsBean.acknowledgeAlarm() called. Alarm updated: {0}", avo);
                if (AlarmsClient.getInstance().updateAlarm(ServiceRegistrarClient.getInstance().getServiceHostURL(ALARM_SERVICE_NAME), avo)) {
                    updateCount(true);
                    selectedAlarms = new ArrayList();
                } else {
                    avo.setAcknowledged(false);
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(AlarmsBean.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

    /**
     * Create WorkOrder for the selected alarm
     */
    public void createWorkOrder() {
        String tmp;
        int index;
        AlarmsVO avo;
        
        try {
            if ((tmp = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("alarm-form:indexId")) != null) {
                index = Integer.parseInt(tmp);
                avo = new AlarmsVO(alarmsVOList.stream().filter(vo -> vo.getIndex() == index).findFirst().get());
            } else {
                avo = new AlarmsVO();
            }
        } catch (Exception ex) {
            Logger.getLogger(AlarmsBean.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
            avo = new AlarmsVO();
        }
        
        if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("createWorkOrderBean") == null) {
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("createWorkOrderBean", new CreateWorkOrderBean());
            ((CreateWorkOrderBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("createWorkOrderBean")).init();
        }
        ((CreateWorkOrderBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("createWorkOrderBean")).presetFields(selectedFilters, avo);
        navigationBean.updateDialog("create-work-order");
    }

    /**
     * Information of the selected alarm.
     */
    public void infoOfSelectedAlarm() {
        String tmp;
        int index;
        AlarmsVO avo;
        
        try {
            if ((tmp = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("alarm-form:indexId")) != null) {
                index = Integer.parseInt(tmp);
                avo = new AlarmsVO(alarmsVOList.stream().filter(vo -> vo.getIndex() == index).findFirst().get());
            } else {
                avo = new AlarmsVO();
            }
        } catch (Exception ex) {
            Logger.getLogger(AlarmsBean.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
            avo = new AlarmsVO();
        }
        
        loadPointDetails(avo);
        navigationBean.updateDialog("alarm-details");
    }

    /**
     * Add the PointVO info based on the given info
     * 
     * @param avo, AlarmsVO Object
     */
    private void loadPointDetails(AlarmsVO avo) {
        List<PointLocationVO> pointlocations;
        List<PointVO> pointVOList;
        
        if (userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null && (pointVOList = PointClient.getInstance().getApALByPoints_bySiteAreaAssetPointLocationPointApSet(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), avo.getSiteId(), avo.getAreaId(), avo.getAssetId(), avo.getPointLocationId(), avo.getPointId(), avo.getApSetId())) != null && !pointVOList.isEmpty()) {
            for (PointVO pvo : pointVOList) {
                if (Objects.equals("Temperature", avo.getSensorType())) {
                    pointVO = pvo;
                } else {
                    if (Objects.equals(pvo.getParamName(), avo.getParamName())) {
                        pointVO = pvo;
                        break;
                    }
                }
            }
            pointVO.setSiteName(avo.getSiteName());
            pointVO.setAreaName(avo.getAreaName());
            pointVO.setAssetName(avo.getAssetName());
            pointVO.setAssetId(avo.getAssetId());

            pointlocations = new ArrayList();
            if (pointLocationList.isEmpty()) {
                pointlocations.addAll(PointLocationClient.getInstance().getPointLocationsByPK(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), avo.getSiteId(), avo.getAreaId(), avo.getAssetId(), avo.getPointLocationId()));
            }
            if (!pointlocations.isEmpty()) {
                pointVO.setPointLocationName(pointlocations.get(0).getPointLocationName());
                pointVO.setPointLocationId(pointlocations.get(0).getPointLocationId());
            } else {
                pointVO.setPointLocationName(avo.getPointLocationName());
                pointVO.setPointLocationId(avo.getPointLocationId());
            }

            pointVOList = new ArrayList();
            pointVOList.addAll(PointClient.getInstance().getPointsByPK(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), avo.getSiteId(), avo.getAreaId(), avo.getAssetId(), avo.getPointLocationId(), avo.getPointId(), avo.getApSetId()));
            if (!pointVOList.isEmpty()) {
                pointVO.setPointName(pointVOList.get(0).getPointName());
                pointVO.setPointId(pointVOList.get(0).getPointId());
                pointVO.setPointType(pointVOList.get(0).getPointType());
            } else {
                pointVO.setPointName(avo.getPointName());
                pointVO.setPointId(avo.getPointId());
            }

            setupApAlSetVOs();

        } 
    }

    /**
     * Setup ApAlSetVO Object list
     */
    private void setupApAlSetVOs() {
        List<ApAlSetVO> list;

        if (apAlSetVOMap.containsKey(pointVO.getApSetId().toString() + "," + pointVO.getAlSetId().toString())) {
            pointVO.setApSetName(apAlSetVOMap.get(pointVO.getApSetId().toString() + "," + pointVO.getAlSetId().toString()).getApSetName());
            pointVO.setAlSetName(apAlSetVOMap.get(pointVO.getApSetId().toString() + "," + pointVO.getAlSetId().toString()).getAlSetName());
        } else if (userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null) {
            if ((list = APALSetClient.getInstance().getSiteApAlSetsByCustomerSiteIdApSetIdAlSetId(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), pointVO.getSiteId(), pointVO.getApSetId(), pointVO.getAlSetId())) == null || list.isEmpty()) {
                if ((list = APALSetClient.getInstance().getGlobalApAlSetsByCustomerApSetIdAlSetId(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), pointVO.getApSetId(), pointVO.getAlSetId())) == null || list.isEmpty()) {
                    list = APALSetClient.getInstance().getGlobalApAlSetsByCustomerApSetIdAlSetId(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), DEFAULT_UPTIME_ACCOUNT, pointVO.getApSetId(), pointVO.getAlSetId());
                }
            }
            
            if (list != null && !list.isEmpty()) {
                pointVO.setApSetName(list.get(0).getApSetName());
                pointVO.setAlSetName(list.get(0).getAlSetName());
                pointVO.setApAlSetVOs(list);
                apAlSetVOMap.put(pointVO.getApSetId().toString() + "," + pointVO.getAlSetId().toString(), list.get(0));
            }
        }
    }

    /**
     * Converts the given instant to a String
     *
     * @param instant, Instant Object
     * @return, String Object
     */
    public String dateFormatter(Instant instant) {
        try {
            return LocalDateTime.ofInstant(instant, (userManageBean.getZoneId() != null) ? userManageBean.getZoneId() : ZoneOffset.UTC).format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * Action to occur on toggle select all event
     *
     * @param event
     */
    public void dataTableSelectAllRows(ToggleSelectEvent event) {
        if (event.isSelected()) {
            selectedAlarms = new ArrayList();
            alarmsVOList.stream().filter(vo -> (!vo.isAcknowledged())).forEachOrdered(vo -> selectedAlarms.add(new AlarmsVO(vo)));
        } else {
            selectedAlarms = new ArrayList();
        }
    }

    /**
     * Action to occur on row select event
     *
     * @param event
     */
    public void onRowSelect(SelectEvent<AlarmsVO> event) {
        if (selectedAlarms == null) {
            selectedAlarms = new ArrayList();
        }
        
        if (event != null && event.getObject() != null && !selectedAlarms.contains(event.getObject())) {
            selectedAlarms.add(event.getObject());
        }
    }

    /**
     * Action to occur on row unselect event
     *
     * @param event
     */
    public void onRowUnselect(UnselectEvent<AlarmsVO> event) {
        if (selectedAlarms != null && event != null && event.getObject() != null) {
            selectedAlarms.remove(event.getObject());
        }
    }
    
    /**
     * Return result header based on the given value
     * 
     * @param filterLevel, String Object
     * @return String Object
     */
    public String getResultHeader(String filterLevel) {
        StringBuilder sb;
        
        if (filterLevel != null) {
            try {
                sb = new StringBuilder();
                
                switch (filterLevel.toLowerCase()) {
                    case AREA:
                        if (selectedFilters.getArea() != null && selectedFilters.getArea().getLabel() != null && !selectedFilters.getArea().getLabel().isEmpty()) {
                            sb.append(selectedFilters.getArea().getLabel()).append(" - ").append(userManageBean.getResourceBundleString("label_active_alarms"));
                            return sb.toString();
                        }
                        break;
                    case ASSET:
                        if (selectedFilters.getAsset() != null && selectedFilters.getAsset().getLabel() != null && !selectedFilters.getAsset().getLabel().isEmpty()) {
                            sb.append(selectedFilters.getAsset().getLabel()).append(" - ").append(userManageBean.getResourceBundleString("label_active_alarms"));
                            return sb.toString();
                        }
                        break;
                    case POINT_LOCATION:
                        if (selectedFilters.getPointLocation()!= null && selectedFilters.getPointLocation().getLabel() != null && !selectedFilters.getPointLocation().getLabel().isEmpty()) {
                            sb.append(selectedFilters.getPointLocation().getLabel()).append(" - ").append(userManageBean.getResourceBundleString("label_active_alarms"));
                            return sb.toString();
                        }
                        break;
                    case POINT:
                        if (selectedFilters.getPoint()!= null && selectedFilters.getPoint().getLabel() != null && !selectedFilters.getPoint().getLabel().isEmpty()) {
                            sb.append(selectedFilters.getPoint().getLabel()).append(" - ").append(userManageBean.getResourceBundleString("label_active_alarms"));
                            return sb.toString();
                        }
                        break;
                }
            } catch (Exception ex) {
                Logger.getLogger(AlarmsBean.class.getName()).log(Level.WARNING, ex.getMessage(), ex);
            }
        }
        return userManageBean.getResourceBundleString("label_active_alarms");
    }

    public String getAreaFilterSelect() {
        return areaFilterSelect;
    }

    public void setAreaFilterSelect(String areaFilterSelect) {
        this.areaFilterSelect = areaFilterSelect;
    }

    public List<FilterMeta> getFilterBy() {
        return filterBy;
    }

    public void setFilterBy(List<FilterMeta> filterBy) {
        this.filterBy = filterBy;
    }

    public List<AlarmsVO> getAlarmsVOList() {
        return alarmsVOList;
    }

    public List<AlarmsVO> getFilteredAlarms() {
        return filteredAlarms;
    }

    public void setFilteredAlarms(List<AlarmsVO> filteredAlarms) {
        this.filteredAlarms = filteredAlarms;
    }

    public List<String> getAlarmTypes() {
        return alarmTypes;
    }

    public void setAlarmTypes(List<String> alarmTypes) {
        this.alarmTypes = alarmTypes;
    }

    public boolean isShowCurrentTime() {
        return showCurrentTime;
    }

    public void setShowCurrentTime(boolean showCurrentTime) {
        this.showCurrentTime = showCurrentTime;
    }

    public boolean isSelectable() {
        return selectable;
    }
    public void setSelectable(boolean selectable) {
        this.selectable = selectable;
    }
    public boolean isZoomable() {
        return zoomable;
    }
    public void setZoomable(boolean zoomable) {
        this.zoomable = zoomable;
    }
    public boolean isMoveable() {
        return moveable;
    }
    public void setMoveable(boolean moveable) {
        this.moveable = moveable;
    }

    public boolean isStackEvents() {
        return stackEvents;
    }

    public void setStackEvents(boolean stackEvents) {
        this.stackEvents = stackEvents;
    }

    public boolean isShowNavigation() {
        return showNavigation;
    }

    public void setShowNavigation(boolean showNavigation) {
        this.showNavigation = showNavigation;
    }

    public String getDummy() {
        return dummy;
    }

    public void setDummy(String dummy) {
        this.dummy = dummy;
    }

    public String getEventStyle() {
        return eventStyle;
    }

    public void setEventStyle(String eventStyle) {
        this.eventStyle = eventStyle;
    }

    public boolean isDisplayAreaTable() {
        return displayAreaTable;
    }

    public boolean isDisplayAssetTable() {
        return displayAssetTable;
    }

    public boolean isDisplayPLTable() {
        return displayPLTable;
    }

    public boolean isDisplayPointTable() {
        return displayPointTable;
    }

    public PointVO getPointVO() {
        return pointVO;
    }

    public void setPointVO(PointVO pointVO) {
        this.pointVO = pointVO;
    }

    public List<String> getPriorityList() {
        return priorityList;
    }

    public List<String> getIssuesList() {
        return issuesList;
    }

    public List<String> getActionNeededList() {
        return actionNeededList;
    }

    public List<AlarmsVO> getSelectedAlarms() {
        return selectedAlarms;
    }

    public void setSelectedAlarms(List<AlarmsVO> selectedAlarms) {
        this.selectedAlarms = selectedAlarms;
    }

    public List<AlarmsVO> getAreaTileList() {
        return areaTileList;
    }

    public boolean isShowHistory() {
        return showHistory;
    }

    public void setShowHistory(boolean showHistory) {
        this.showHistory = showHistory;
    }

    public TimelineModel<String, String> getModel() {
        return model;
    }

    public String getAlarmType() {
        return alarmType;
    }

    public void setAlarmType(String alarmType) {
        this.alarmType = alarmType;
    }
}
