/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.PointClient;
import com.uptime.upcastcm.http.client.PointLocationClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import com.uptime.upcastcm.utils.helperclass.Filters;
import com.uptime.upcastcm.utils.interfaces.PresetDialogFields;
import com.uptime.upcastcm.utils.vo.PointLocationVO;
import com.uptime.upcastcm.utils.vo.PointVO;
import com.uptime.upcastcm.utils.vo.TrendDetailsVO;
import com.uptime.upcastcm.utils.vo.WorkOrderVO;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import org.primefaces.PrimeFaces;

/**
 *
 * @author twilcox
 */
public class AddTrendBean implements Serializable, PresetDialogFields {
    private UserManageBean userManageBean;
    private WorkOrderVO workOrderVO;
    private final List<SelectItem> pointLocationList, pointList;
    private final List<String> paramList;
    private UUID alSetId, selectedPointLocation, selectedPoint;
    private String selectedParam;

    /**
     * Creates a new instance of AddTrendBean
     */
    public AddTrendBean() {
        pointLocationList = new ArrayList(); 
        pointList = new ArrayList(); 
        paramList = new ArrayList(); 

        try {
            userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
        } catch (Exception e) {
            Logger.getLogger(AddTrendBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            userManageBean = null;
        }
    }

    /**
     * Post Constructor
     */
    @PostConstruct
    public void init() {
        resetPage();
    }

    @Override
    public void resetPage() {
        pointLocationList.clear();
        pointList.clear();
        paramList.clear();
        selectedPointLocation = null;
        selectedPoint = null;
        selectedParam = null;
        workOrderVO = null;
        alSetId = null;
    }

    @Override
    public void presetFields(Filters presets, Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void presetFields(int operationType, Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void presetFields(Object object) {
        try {
            if (object != null) {
                if (object instanceof WorkOrderVO) {
                    pointLocationList.clear();
                    pointList.clear();
                    paramList.clear();
                    selectedPointLocation = null;
                    selectedPoint = null;
                    selectedParam = null;
                    workOrderVO = (WorkOrderVO)object;

                    if (workOrderVO.getPointLocationId() != null) {
                        selectedPointLocation = workOrderVO.getPointLocationId();
                        pointLocationList.add(new SelectItem(workOrderVO.getPointLocationId(), workOrderVO.getPointLocationName()));

                        if (workOrderVO.getPointId() != null && alSetId != null) {
                            selectedPoint = workOrderVO.getPointId();
                            pointList.add(new SelectItem(workOrderVO.getPointId(), workOrderVO.getPointName(), workOrderVO.getApSetId().toString() + "," + alSetId.toString() + "," + workOrderVO.getChannelType()));
                            PointClient.getInstance().getApALByPoints_bySiteAreaAssetPointLocationPointApSetAlSet(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), workOrderVO.getCustomerAccount(), workOrderVO.getSiteId(), workOrderVO.getAreaId(), workOrderVO.getAssetId(), selectedPointLocation, selectedPoint, workOrderVO.getApSetId(), alSetId).stream().sorted(Comparator.comparing(PointVO::getParamName)).forEachOrdered(point -> paramList.add(point.getParamName()));
                        }
                    } else {
                        PointLocationClient.getInstance().getPointLocationsByCustomerSiteAreaAsset(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), workOrderVO.getCustomerAccount(), workOrderVO.getSiteId(), workOrderVO.getAreaId(), workOrderVO.getAssetId()).stream().sorted(Comparator.comparing(PointLocationVO::getPointLocationName)).forEachOrdered(pointLocation -> pointLocationList.add(new SelectItem(pointLocation.getPointLocationId(), pointLocation.getPointLocationName())));
                    }
                } else if (object instanceof UUID) {
                    alSetId = (UUID)object;
                }
            }
        } catch (Exception e) {
            Logger.getLogger(AddTrendBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }
    
    /**
     * Add the given trend to a trend to the trendDetailsVO list
     */
    public void addTrend() {
        SelectItem pointLocation, point;
        TrendDetailsVO trendDetailsVO;
        String[] elements;
        
        try {
            if (selectedPointLocation != null && selectedPoint != null && selectedParam != null && !selectedParam.isEmpty()) {
                pointLocation = pointLocationList.stream().filter(item -> selectedPointLocation.equals((UUID)item.getValue())).findFirst().get();
                point = pointList.stream().filter(item -> selectedPoint.equals((UUID)item.getValue())).findFirst().get();
                
                if ((elements = point.getDescription().split(",")).length == 3 && !workOrderVO.getTrendDetails().stream().anyMatch(trend -> trend.getPointLocationId().equals(selectedPointLocation) && trend.getPointId().equals(selectedPoint) && trend.getParamName().equals(selectedParam))) {
                    trendDetailsVO = new TrendDetailsVO();
                    trendDetailsVO.setApSetId(UUID.fromString(elements[0]));
                    trendDetailsVO.setChannelType(elements[2]);
                    trendDetailsVO.setParamName(selectedParam);
                    trendDetailsVO.setPointId(selectedPoint);
                    trendDetailsVO.setPointName(point.getLabel());
                    trendDetailsVO.setPointLocationId(selectedPointLocation);
                    trendDetailsVO.setPointLocationName(pointLocation.getLabel());
                    workOrderVO.getTrendDetails().add(trendDetailsVO);
                    
                    selectedPointLocation = null;
                    selectedPoint = null;
                    selectedParam = null;
                    
                    PrimeFaces.current().ajax().update("createWorkOrderFormId");
                    PrimeFaces.current().ajax().update("editWorkOrderFormId");
                    PrimeFaces.current().executeScript("PF('defaultSecondDlg').hide()");
                } else {
                    FacesContext.getCurrentInstance().addMessage("growlMessages", new FacesMessage(userManageBean.getResourceBundleString("growl_given_trend_already_included")));
                }
            }
        } catch (Exception e) {
            Logger.getLogger(AddTrendBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }
    
    /**
     * Action after a point location is selected
     */
    public void onPointLocationChange() {
        pointList.clear();
        paramList.clear();
        selectedPoint = null;
        selectedParam = null;
        
        if (workOrderVO != null && selectedPointLocation != null) {
            PointClient.getInstance().getPointsByCustomerSiteAreaAssetPointLocation(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), workOrderVO.getCustomerAccount(), workOrderVO.getSiteId(), workOrderVO.getAreaId(), workOrderVO.getAssetId(), selectedPointLocation, true).stream().sorted(Comparator.comparing(PointVO::getPointName)).forEachOrdered(point -> pointList.add(new SelectItem(point.getPointId(), point.getPointName(), point.getApSetId().toString() + "," + point.getAlSetId().toString() + "," + point.getPointType())));
        }
    }
    
    /**
     * Action after a point is selected
     */
    public void onPointChange() {
        SelectItem point;
        String[] elements;
        paramList.clear();
        selectedParam = null;
        
        try {
            if (workOrderVO != null && selectedPointLocation != null && selectedPoint != null) {
                point = pointList.stream().filter(item -> selectedPoint.equals((UUID)item.getValue())).findFirst().get();
                elements = point.getDescription().split(",");
                PointClient.getInstance().getApALByPoints_bySiteAreaAssetPointLocationPointApSetAlSet(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), workOrderVO.getCustomerAccount(), workOrderVO.getSiteId(), workOrderVO.getAreaId(), workOrderVO.getAssetId(), selectedPointLocation, selectedPoint, UUID.fromString(elements[0]), UUID.fromString(elements[1])).stream().sorted(Comparator.comparing(PointVO::getParamName)).forEachOrdered(item -> paramList.add(item.getParamName()));
            }
        } catch (Exception e) {
            Logger.getLogger(AddTrendBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            paramList.clear();
            selectedParam = null;
        }
    }

    public List<SelectItem> getPointLocationList() {
        return pointLocationList;
    }

    public List<SelectItem> getPointList() {
        return pointList;
    }

    public List<String> getParamList() {
        return paramList;
    }

    public UUID getSelectedPointLocation() {
        return selectedPointLocation;
    }

    public void setSelectedPointLocation(UUID selectedPointLocation) {
        this.selectedPointLocation = selectedPointLocation;
    }

    public UUID getSelectedPoint() {
        return selectedPoint;
    }

    public void setSelectedPoint(UUID selectedPoint) {
        this.selectedPoint = selectedPoint;
    }

    public String getSelectedParam() {
        return selectedParam;
    }

    public void setSelectedParam(String selectedParam) {
        this.selectedParam = selectedParam;
    }
}
