/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.AssetClient;
import com.uptime.upcastcm.http.client.ManageDeviceClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import com.uptime.upcastcm.utils.enums.TS1ACChannelNumberEnum;
import com.uptime.upcastcm.utils.enums.TS1DCChannelNumberEnum;
import com.uptime.upcastcm.utils.helperclass.Filters;
import com.uptime.upcastcm.utils.interfaces.PresetDialogFields;
import com.uptime.upcastcm.utils.vo.AssetVO;
import com.uptime.upcastcm.utils.vo.ChannelVO;
import com.uptime.upcastcm.utils.vo.PointLocationVO;
import com.uptime.upcastcm.utils.vo.PointVO;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.primefaces.PrimeFaces;

/**
 *
 * @author twilcox
 */
public class EditTS1Bean implements Serializable, PresetDialogFields {
    private String serialNumber;
    private short basestationPortNum;
    private short sampleInterval;
    private final List<ChannelVO> channelVOList;
    private UserManageBean userManageBean;
    private NavigationBean navigationBean;
    private boolean hwDisabled;

    /**
     * Constructor
     */
    public EditTS1Bean() {
        channelVOList = new ArrayList();
        hwDisabled = false;
        
        try {
            userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
            navigationBean = (NavigationBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("navigationBean");
        } catch (Exception e) {
            Logger.getLogger(EditTS1Bean.class.getName()).log(Level.SEVERE, "EditTS1Bean Constructor Exception {0}", e.toString());
            userManageBean = null;
            navigationBean = null;
        }
    }

    /**
     * Post Constructor
     */
    @PostConstruct
    public void init() {
        resetPage();
        
        if (userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null) {
            Logger.getLogger(EditTS1Bean.class.getName()).log(Level.INFO, "**customerAccount to be set : {0}", userManageBean.getCurrentSite().getCustomerAccount());
        }
    }

    /**
     * Resets the page and clears all previous entered values
     */
    @Override
    public void resetPage() {
        serialNumber = null;
        basestationPortNum = 0;
        sampleInterval = 0;
        channelVOList.clear();
        hwDisabled = false;
    }

    @Override
    public void presetFields(Filters presets, Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void presetFields(int operationType, Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void presetFields(Object object) {
        resetPage();
        
        try {
            if (object != null && object instanceof String) {
                serialNumber = (String)object;

                if (userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null && userManageBean.getCurrentSite().getSiteId() != null) {
                    channelVOList.addAll(ManageDeviceClient.getInstance().getDeviceManagementRowsByCustomerSiteIdDevice(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), userManageBean.getCurrentSite().getSiteId(), serialNumber, true));
                    if (!channelVOList.isEmpty()) {
                        hwDisabled = channelVOList.get(0).getHwDisabled();
                        basestationPortNum = channelVOList.get(0).getBasestationPortNum();
                        sampleInterval = channelVOList.get(0).getSampleInterval();
                        
                        channelVOList.stream().forEach(a -> {
                    if (a.isAlarmEnabled()) {
                        a.setAlarmEnabledValue(userManageBean.getResourceBundleString("label_true"));
                    } else {
                        a.setAlarmEnabledValue(userManageBean.getResourceBundleString("label_false"));
                    }
                });
                    }
                }
            }
        } catch (Exception e) {
            Logger.getLogger(EditTS1Bean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            resetPage();
        }
    }

    /**
     * Action when Add Channel button is clicked Open the Add Channel Overlay
     */
    public void addChannel() {
        try {
            if (channelVOList.size() <= 24) {
                if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("addChannelBean") == null) {
                    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("addChannelBean", new AddChannelBean());
                    ((AddChannelBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("addChannelBean")).init();
                }
                ((AddChannelBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("addChannelBean")).presetFields(channelVOList);

                navigationBean.updateSecondDialog("add-channel");
                PrimeFaces.current().executeScript("PF('defaultSecondDlg').show()");
            } else {
                FacesContext.getCurrentInstance().addMessage("growlMessages", new FacesMessage(userManageBean.getResourceBundleString("message_sensor_channel_already_added")));
            }
        } catch (Exception e) {
            Logger.getLogger(EditTS1Bean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Set and display the Info Overlay
     *
     * @param channelVO, ChannelVO Object
     */
    public void viewInfo(ChannelVO channelVO) {
        try {
            if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("deviceManagementInfoRowBean") == null) {
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("deviceManagementInfoRowBean", new DeviceManagementInfoRowBean());
            }
            ((DeviceManagementInfoRowBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("deviceManagementInfoRowBean")).presetFields(channelVO);
            navigationBean.updateSecondDialog("device-management-info-row");
        } catch (Exception e) {
            Logger.getLogger(EditTS1Bean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Set and display the Edit Channel Overlay
     *
     * @param channelVO, ChannelVO Object
     */
    public void editChannel(ChannelVO channelVO) {
        try {
            if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("editChannelBean") == null) {
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("editChannelBean", new EditChannelBean());
                ((EditChannelBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("editChannelBean")).init();
            }
            if(channelVO.getAlarmEnabledValue().equalsIgnoreCase(userManageBean.getResourceBundleString("label_true"))){
                channelVO.setAlarmEnabled(true);
            }else{
                channelVO.setAlarmEnabled(false);
            }
            ((EditChannelBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("editChannelBean")).presetFields(channelVO, channelVOList);
            navigationBean.updateSecondDialog("edit-channel");
        } catch (Exception e) {
            Logger.getLogger(EditTS1Bean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Delete a channel
     *
     * @param cVO, ChannelVO Object
     */
    public void delete(ChannelVO cVO) {
        if (cVO != null && channelVOList.contains(cVO)) {
            channelVOList.remove(cVO);
        }
    }

    /**
     * Action after edit button is clicked on the Edit TS1 overlay
     */
    public void editTS1() {
        AssetVO assetVO = null;
        PointLocationVO plvo;
        PointVO pvo;
        List<AssetVO> assetVOList, newAssetVOList;
        List<PointVO> pointVoList;
        List<PointLocationVO> pointLocationVoList;
        HashMap<UUID, List<AssetVO>> assetMap;

        if (!channelVOList.isEmpty()) {
            newAssetVOList = new ArrayList();
            assetMap = new HashMap();
            
            for (ChannelVO cvo : channelVOList) {
                plvo = new PointLocationVO();
                plvo.setSiteId(cvo.getSiteId());
                plvo.setAreaId(cvo.getAreaId());
                plvo.setAssetId(cvo.getAssetId());
                plvo.setSiteName(cvo.getSiteName());
                plvo.setAreaName(cvo.getAreaName());
                plvo.setAssetId(cvo.getAssetId());
                plvo.setAssetName(cvo.getAssetName());
                plvo.setPointLocationName(cvo.getPointLocationName());
                plvo.setDescription(cvo.getPointLocationDesc());
                plvo.setPointLocationId(cvo.getPointLocationId());
                plvo.setBasestationPortNum(basestationPortNum);
                plvo.setAlarmEnabled(cvo.isAlarmEnabled());
                plvo.setTachId(cvo.getTachId());
                plvo.setTachName(cvo.getTachName());
                plvo.setDeviceSerialNumber(serialNumber);
                plvo.setFfSetIds(cvo.getFfSetIds());
                plvo.setFfSetNames(cvo.getFfSetNames());
                plvo.setRollDiameter(cvo.getRollDiameter());
                plvo.setRollDiameterUnits(cvo.getRollDiameterUnits());
                plvo.setSampleInterval(sampleInterval);
                plvo.setSpeedRatio(cvo.getSpeedRatio());

                pvo = new PointVO();
                pvo.setAlSetId(cvo.getAlSetId());
                pvo.setAlSetName(cvo.getAlSetName());
                pvo.setAlarmEnabled(cvo.isAlarmEnabled());
                pvo.setApSetId(cvo.getApSetId());
                pvo.setApSetName(cvo.getApSetName());
                pvo.setAreaId(cvo.getAreaId());
                pvo.setAreaName(cvo.getAreaName());
                pvo.setAssetId(cvo.getAssetId());
                pvo.setAssetName(cvo.getAssetName());
                pvo.setCustomerAccount(cvo.getCustomerAccount());
                pvo.setCustomized(cvo.isCustomized());
                pvo.setDisabled(hwDisabled);
                pvo.setPointLocationName(cvo.getPointLocationName());
                pvo.setPointLocationId(cvo.getPointLocationId());
                pvo.setPointName(cvo.getPointName());
                pvo.setPointId(cvo.getPointId());
                pvo.setPointType(cvo.getPointType());
                pvo.setSensorChannelNum(cvo.getSensorChannelNum());
                pvo.setSensorType(cvo.getSensorType());
                pvo.setSensorOffset(cvo.getSensorOffset());
                pvo.setSensorSensitivity(cvo.getSensorSensitivity());
                pvo.setSensorUnits(cvo.getSensorUnits());
                pvo.setSiteId(cvo.getSiteId());
                pvo.setSiteName(cvo.getSiteName());
                pvo.setApAlSetVOs(cvo.getApAlSetVOs());

                assetVOList = null;
                if (assetMap.isEmpty() || assetMap.get(cvo.getAssetId()) == null) {
                    assetVOList = AssetClient.getInstance().getAssetSiteAreaByCustomerSiteArea(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), userManageBean.getCurrentSite().getSiteId(), cvo.getAreaId());
                    assetMap.put(cvo.getAssetId(), assetVOList);
                } else {
                    if (!assetMap.isEmpty()) {
                        assetVOList = assetMap.get(cvo.getAssetId());
                    }
                }
                
                if (!newAssetVOList.isEmpty()) {
                    assetVO = newAssetVOList.stream().filter(aVO -> (cvo.getAssetId().equals(aVO.getAssetId()))).findFirst().orElse(null);
                }
                
                if (assetVO != null) {
                    if (assetVO.getPointLocationList() != null && !assetVO.getPointLocationList().isEmpty()) {
                        pointVoList = new ArrayList();
                        pointVoList.add(pvo);
                        plvo.setPointList(pointVoList);
                        assetVO.getPointLocationList().add(plvo);
                    }
                } else if (assetVOList != null) {
                    if ((assetVO = assetVOList.stream().filter(aVO -> (cvo.getAssetId().equals(aVO.getAssetId()))).collect(Collectors.toList()).get(0)) != null) {
                        pointVoList = new ArrayList();
                        pointVoList.add(pvo);
                        plvo.setPointList(pointVoList);
                        pointLocationVoList = new ArrayList();
                        pointLocationVoList.add(plvo);
                        assetVO.setPointLocationList(pointLocationVoList);
                        newAssetVOList.add(assetVO);
                    }
                }
            }

            try {
                Logger.getLogger(EditTS1Bean.class.getName()).log(Level.INFO, "newAssetVOList size - {0}", new Object[]{newAssetVOList.size()});
                if (ManageDeviceClient.getInstance().modifyTS1Device(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), newAssetVOList, hwDisabled)) {
                    Logger.getLogger(EditTS1Bean.class.getName()).log(Level.INFO, "Modified TS1");
                    
                    ((DeviceManagementBean)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("deviceManagementBean")).search();
                    
                    PrimeFaces.current().executeScript("PF('defaultDlg').hide()");
                    PrimeFaces.current().ajax().update("nonAutoGrowlId");
                    PrimeFaces.current().ajax().update("deviceManagementResultFormId");
                }
            } catch (Exception e) {
                Logger.getLogger(EditTS1Bean.class.getName()).log(Level.SEVERE, "Exception while creating TS1 device", e);
            }
        }
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public short getBasestationPortNum() {
        return basestationPortNum;
    }

    public void setBasestationPortNum(short basestationPortNum) {
        this.basestationPortNum = basestationPortNum;
    }

    public short getSampleInterval() {
        return sampleInterval;
    }

    public void setSampleInterval(short sampleInterval) {
        this.sampleInterval = sampleInterval;
    }

    public List<ChannelVO> getChannelVOList() {
        return channelVOList;
    }
    
    public String getTS1ACChannelNumberLabelByValue(int value) {
        return TS1ACChannelNumberEnum.getTS1ACChannelNumberLabelByValue(value);
    }
    
    public String getTS1DCChannelNumberLabelByValue(int value) {
        return TS1DCChannelNumberEnum.getTS1DCChannelNumberLabelByValue(value);
    }
}
