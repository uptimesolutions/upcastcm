/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.AssetClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import com.uptime.upcastcm.utils.helperclass.Filters;
import com.uptime.upcastcm.utils.vo.AreaVO;
import com.uptime.upcastcm.utils.vo.AssetVO;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import com.uptime.upcastcm.utils.interfaces.PresetDialogFields;
import com.uptime.upcastcm.utils.vo.AssetTypesVO;
import com.uptime.upcastcm.utils.vo.UserSitesVO;
import java.util.UUID;
import org.primefaces.PrimeFaces;

/**
 *
 * @author madhavi
 */
public class EditAssetBean implements Serializable, PresetDialogFields {

    private UserManageBean userManageBean;
    private AssetVO assetVO;
    private String focusProperty;
    private List<String> assetTypeList;
    private List<AreaVO> areaList;
    private List<UserSitesVO> siteList;
    private Filters presets;

    /**
     * Constructor
     */
    public EditAssetBean() {
        try {
            assetTypeList = new ArrayList();
            userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");

            List<AssetTypesVO> assetTypesVOList = new ArrayList();
            assetTypesVOList.addAll(AssetClient.getInstance().getGlobalAssetTypesByCustomer(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount()));
            if (!userManageBean.getCurrentSite().getCustomerAccount().equals(DEFAULT_UPTIME_ACCOUNT)) {
                assetTypesVOList.addAll(AssetClient.getInstance().getGlobalAssetTypesByCustomer(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), DEFAULT_UPTIME_ACCOUNT));
            }
            assetTypesVOList.stream().forEach(a -> assetTypeList.add(a.getAssetTypeName()));

        } catch (Exception e) {
            Logger.getLogger(EditAssetBean.class.getName()).log(Level.SEVERE, "CreateAssetBean Constructor Exception {0}", e.toString());
            userManageBean = null;
        }
    }

    /**
     * Post Constructor
     */
    @PostConstruct
    public void init() {
        resetPage();
    }

    @Override
    public void resetPage() {
        presets = null;
        assetVO = new AssetVO();
        if (userManageBean != null && userManageBean.getCurrentSite() != null) {
            assetVO.setCustomerAccount(userManageBean.getCurrentSite().getCustomerAccount());
            assetVO.setSiteName(userManageBean.getCurrentSite().getSiteName());
            assetVO.setSiteId(userManageBean.getCurrentSite().getSiteId());
        }
        assetVO.setPointLocationList(new ArrayList<>());
    }

    @Override
    public void presetFields(Filters presets, Object object) {
        UserSitesVO userSitesVO;
        AreaVO areaVO;

        assetVO = object != null && object instanceof AssetVO ? new AssetVO((AssetVO) object) : new AssetVO();

        if (presets != null) {
            if (presets.getSite() != null) {
                siteList = new ArrayList();
                userSitesVO = new UserSitesVO();
                userSitesVO.setSiteId((UUID) presets.getSite().getValue());
                userSitesVO.setSiteName(presets.getSite().getLabel());
                siteList.add(userSitesVO);
                assetVO.setSiteName(presets.getSite().getLabel());
                assetVO.setSiteId((UUID) presets.getSite().getValue());
            }
            if (presets.getArea() != null) {
                areaList = new ArrayList();
                areaVO = new AreaVO();
                areaVO.setAreaId((UUID) presets.getArea().getValue());
                areaVO.setAreaName(presets.getArea().getLabel());
                areaList.add(areaVO);
                assetVO.setAreaName(presets.getArea().getLabel());
                assetVO.setAreaId((UUID) presets.getArea().getValue());
            }
            
            this.presets = presets;
        }
    }

    @Override
    public void presetFields(int operationType, Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void presetFields(Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void updateBean() {
        if (assetVO.getSiteName() != null && !assetVO.getSiteName().isEmpty()
                && assetVO.getSiteName() != null && !assetVO.getSiteName().isEmpty()
                && assetVO.getAreaName() != null && !assetVO.getAreaName().isEmpty()
                && assetVO.getAssetType() != null && !assetVO.getAssetType().isEmpty()
                && assetVO.getAssetName() != null && !assetVO.getAssetName().isEmpty()
                && assetVO.getAssetId() != null
                && assetVO.getDescription() != null && !assetVO.getDescription().isEmpty()
                && assetVO.getSampleInterval() > 0) {
            focusProperty = "submitBtn";
        } else if (assetVO.getSiteName() != null && !assetVO.getSiteName().isEmpty()
                && assetVO.getSiteName() != null && !assetVO.getSiteName().isEmpty()
                && assetVO.getAreaName() != null && !assetVO.getAreaName().isEmpty()
                && assetVO.getAssetType() != null && !assetVO.getAssetType().isEmpty()
                && assetVO.getAssetName() != null && !assetVO.getAssetName().isEmpty()
                && assetVO.getAssetId() != null
                && assetVO.getDescription() != null && !assetVO.getDescription().isEmpty()
                && assetVO.getSampleInterval() <= 0) {
            focusProperty = "sampleInterval";
        } else if (assetVO.getSiteName() != null && !assetVO.getSiteName().isEmpty()
                && assetVO.getSiteName() != null && !assetVO.getSiteName().isEmpty()
                && assetVO.getAreaName() != null && !assetVO.getAreaName().isEmpty()
                && assetVO.getAssetType() != null && !assetVO.getAssetType().isEmpty()
                && assetVO.getAssetName() != null && !assetVO.getAssetName().isEmpty()
                && assetVO.getAssetId() != null) {
            focusProperty = "description";
        } else if (assetVO.getSiteName() != null && !assetVO.getSiteName().isEmpty()
                && assetVO.getSiteName() != null && !assetVO.getSiteName().isEmpty()
                && assetVO.getAreaName() != null && !assetVO.getAreaName().isEmpty()
                && assetVO.getAssetType() != null && !assetVO.getAssetType().isEmpty()
                && assetVO.getAssetName() != null && !assetVO.getAssetName().isEmpty()) {
            focusProperty = "assetId";
        } else if (assetVO.getSiteName() != null && !assetVO.getSiteName().isEmpty()
                && assetVO.getSiteName() != null && !assetVO.getSiteName().isEmpty()
                && assetVO.getAreaName() != null && !assetVO.getAreaName().isEmpty()
                && assetVO.getAssetType() != null && !assetVO.getAssetType().isEmpty()) {
            focusProperty = "assetName";
        } else if (assetVO.getSiteName() != null && !assetVO.getSiteName().isEmpty()
                && assetVO.getSiteName() != null && !assetVO.getSiteName().isEmpty()
                && assetVO.getAreaName() != null && !assetVO.getAreaName().isEmpty()) {
            focusProperty = "assetType";
        } else if (assetVO.getSiteName() != null && !assetVO.getSiteName().isEmpty()
                && assetVO.getSiteName() != null && !assetVO.getSiteName().isEmpty()
                && assetVO.getAreaName() != null && !assetVO.getAreaName().isEmpty()
                && assetVO.getAssetType() != null && !assetVO.getAssetType().isEmpty()
                && assetVO.getAssetName() != null && !assetVO.getAssetName().isEmpty()
                && assetVO.getDescription() != null && !assetVO.getDescription().isEmpty()
                && assetVO.getSampleInterval() > 0) {
            focusProperty = "submitBtn";
        }

    }

    public void updateAsset() {
        // Verify that al_set_id is not null for all points.
        if (assetVO.isAlarmEnabled() && !validateAlSetforPoints(assetVO)) {
            if (userManageBean != null) {
                FacesContext.getCurrentInstance().addMessage("growlMessages", new FacesMessage(userManageBean.getResourceBundleString("growl_validate_al_set_name_message")));
            }
            return;
        }
        if (AssetClient.getInstance().updateAsset(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), assetVO)) {
            Logger.getLogger(EditAssetBean.class.getName()).log(Level.INFO, "Updated Asset {0}", new Object[]{assetVO.toString()});
            assetVO = new AssetVO();
            
            if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("configurationBean") == null) {
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("configurationBean", new ConfigurationBean());
                ((ConfigurationBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("configurationBean")).init();
            }
            
            if (presets != null) {
                if (presets.getAsset() != null) {
                    ((ConfigurationBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("configurationBean")).onAssetChange(true);
                } else {
                    ((ConfigurationBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("configurationBean")).onAreaChange(true);
                }
            }

            PrimeFaces.current().executeScript("updateTreeNodes()");
        } else {
            Logger.getLogger(EditAssetBean.class.getName()).log(Level.INFO, "assetVO*********** {0}", new Object[]{assetVO.toString()});
        }
    }

    public void setAssetVO(AssetVO assetVO) {
        this.assetVO = assetVO;
    }

    public AssetVO getAssetVO() {
        return assetVO;
    }

    public List<UserSitesVO> getSiteList() {
        return siteList;
    }

    public List<String> getAssetTypeList() {
        return assetTypeList;
    }

    public String getFocusProperty() {
        return focusProperty;
    }

    private boolean validateAlSetforPoints(AssetVO assetVO) {
        return AssetClient.getInstance().validateAlSetforPoints(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), assetVO.getCustomerAccount(), assetVO.getSiteId(), assetVO.getAreaId(), assetVO.getAssetId(), true);
    }

    public List<AreaVO> getAreaList() {
        return areaList;
    }

}
