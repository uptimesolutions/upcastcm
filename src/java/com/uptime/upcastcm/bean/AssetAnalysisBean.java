/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.bean;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.uptime.client.http.client.ServiceRegistrarClient;
import static com.uptime.client.utils.JsonConverterUtil.DATE_TIME_OFFSET_FORMAT;
import com.uptime.upcastcm.dao.PrestoDAO;
import com.uptime.upcastcm.http.client.APALSetClient;
import com.uptime.upcastcm.http.client.AlarmsClient;
import com.uptime.upcastcm.http.client.ApALByPointClient;
import com.uptime.upcastcm.http.client.AreaClient;
import com.uptime.upcastcm.http.client.AssetClient;
import com.uptime.upcastcm.http.client.PointClient;
import com.uptime.upcastcm.http.client.PointLocationClient;
import com.uptime.upcastcm.http.client.SiteClient;
import com.uptime.upcastcm.http.client.WorkOrderClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import com.uptime.upcastcm.utils.helperclass.AbstractNavigation;
import com.uptime.upcastcm.utils.helperclass.Filters;
import com.uptime.upcastcm.utils.vo.AlarmsVO;
import com.uptime.upcastcm.utils.vo.ApAlSetVO;
import com.uptime.upcastcm.utils.vo.AreaVO;
import com.uptime.upcastcm.utils.vo.AssetVO;
import com.uptime.upcastcm.utils.vo.CustomHeatMapData;
import com.uptime.upcastcm.utils.vo.HeatmapDataVO;
import com.uptime.upcastcm.utils.vo.PointLocationVO;
import com.uptime.upcastcm.utils.vo.PointVO;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import org.primefaces.PrimeFaces;
import org.primefaces.event.NodeCollapseEvent;
import org.primefaces.event.NodeExpandEvent;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;

/**
 *
 * @author gsingh
 */
public class AssetAnalysisBean extends AbstractNavigation implements Serializable {

    private UserManageBean userManageBean;

    private int pointCount, alarmCount, workOrderCount;
    private boolean assertInfo;
    private double activeAlarmsPercentage = 0;
    String assetType;
    String heatMapJson;
    String xLabel, yLabel, customDataJson;

    /**
     * Constructor
     */
    public AssetAnalysisBean() {
        super(AL_SET_PARAM);
        assertInfo = false;
        try {
            userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
        } catch (Exception e) {
            Logger.getLogger(AssetAnalysisBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            userManageBean = null;
        }
    }

    /**
     * Post Constructor
     */
    @PostConstruct
    public void init() {
        resetPage();
        // Set the selectFilters
        try {
            if (userManageBean.getCurrentSite() != null) {
                selectedFilters.setRegion(userManageBean.getSelectedFilters().getRegion());
                selectedFilters.setCountry(userManageBean.getSelectedFilters().getCountry());
                selectedFilters.setSite(userManageBean.getSelectedFilters().getSite());
                onSiteChange(true);
            }
        } catch (Exception e) {
            Logger.getLogger(AssetAnalysisBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * reset AssetAnalysis Page
     */
    @Override
    public void resetPage() {
        setRegionCountrySiteMap();
        setSelectedFilters();
        areaList.clear();
        assetList.clear();
        pointLocationList.clear();
        pointList.clear();
        apSetList.clear();
        alSetParamList.clear();
        assertInfo = false;
        updateBreadcrumbModel();
        if (userManageBean != null) {
            treeContentMenuBuilder(ROOT, new SelectItem(null, userManageBean.getResourceBundleString("breadcrumb_7")), null, null);
        }
        updateUI("content");
    }

    /**
     * called when the year changed
     *
     * @param reloadUI
     */
    @Override
    public void onYearChange(boolean reloadUI) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * called when the region change
     *
     * @param reloadUI
     */
    @Override
    public void onRegionChange(boolean reloadUI) {
        if (selectedFilters == null) {
            setSelectedFilters();
        }
        if (selectedFilters.getRegion() != null && !selectedFilters.getRegion().isEmpty()) {
            selectedFilters.clear(REGION);
            areaList.clear();
            assetList.clear();
            pointLocationList.clear();
            pointList.clear();
            apSetList.clear();
            alSetParamList.clear();

            if (reloadUI) {
                updateTreeNodeRoot(REGION);
                updateBreadcrumbModel();
                updateUI("content");
            }
        } else {
            resetPage();
        }
    }

    /**
     * called when the country changed
     *
     * @param reloadUI
     */
    @Override
    public void onCountryChange(boolean reloadUI) {
        if (selectedFilters == null) {
            setSelectedFilters();
        }
        if (selectedFilters.getCountry() != null && !selectedFilters.getCountry().isEmpty()) {
            selectedFilters.clear(COUNTRY);
            areaList.clear();
            assetList.clear();
            pointLocationList.clear();
            pointList.clear();
            apSetList.clear();
            alSetParamList.clear();

            if (reloadUI) {
                updateTreeNodeRoot(COUNTRY);
                updateBreadcrumbModel();
                updateUI("content");
            }
        } else {
            onRegionChange(reloadUI);
        }
    }

    /**
     * called when the site changed
     *
     * @param reloadUI
     */
    @Override
    public void onSiteChange(boolean reloadUI) {
        List<AreaVO> list;

        if (selectedFilters == null) {
            setSelectedFilters();
        }
        if (selectedFilters.getSite() != null && selectedFilters.getSite().getLabel() != null && !selectedFilters.getSite().getLabel().isEmpty() && selectedFilters.getSite().getValue() != null) {
            selectedFilters.clear(SITE);
            assetList.clear();
            pointLocationList.clear();
            pointList.clear();
            apSetList.clear();
            alSetParamList.clear();
            try {
                areaList.clear();
                list = AreaClient.getInstance().getAreaSiteByCustomerSite(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue());
                list.stream().sorted(Comparator.comparing(AreaVO::getAreaName)).forEachOrdered(area -> areaList.add(new SelectItem(area.getAreaId(), area.getAreaName())));
            } catch (Exception ex) {
                areaList.clear();
            }

            if (reloadUI) {
                updateTreeNodeRoot(SITE);
                updateBreadcrumbModel();
                updateUI("content");
            }
        } else {
            onCountryChange(reloadUI);
        }
    }

    /**
     * called when the area changed
     *
     * @param reloadUI
     */
    @Override
    public void onAreaChange(boolean reloadUI) {
        List<AssetVO> list;

        if (selectedFilters == null) {
            setSelectedFilters();
        }
        if (selectedFilters.getArea() != null && selectedFilters.getArea().getLabel() != null && !selectedFilters.getArea().getLabel().isEmpty() && selectedFilters.getArea().getValue() != null) {
            selectedFilters.clear(AREA);
            pointLocationList.clear();
            pointList.clear();
            apSetList.clear();
            alSetParamList.clear();
            try {
                assetList.clear();
                list = AssetClient.getInstance().getAssetSiteAreaByCustomerSiteArea(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue());
                list.stream().sorted(Comparator.comparing(AssetVO::getAssetName)).forEachOrdered(asset -> assetList.add(new SelectItem(asset.getAssetId(), asset.getAssetName())));
            } catch (Exception ex) {
                assetList.clear();
            }

            if (reloadUI) {
                updateTreeNodeRoot(AREA);
                updateBreadcrumbModel();
                updateUI("content");
            }
        } else {
            onSiteChange(reloadUI);
        }
    }

    /**
     * called when the asset changed
     *
     * @param reloadUI
     */
    @Override
    public void onAssetChange(boolean reloadUI) {
        List<PointLocationVO> list;
        List<AssetVO> assetVOList = new ArrayList();
        if (selectedFilters == null) {
            setSelectedFilters();
        }
        if (selectedFilters.getAsset() != null && selectedFilters.getAsset().getLabel() != null && !selectedFilters.getAsset().getLabel().isEmpty() && selectedFilters.getAsset().getValue() != null) {
            selectedFilters.clear(ASSET);
            pointList.clear();
            apSetList.clear();
            alSetParamList.clear();
            try {

                assetList.clear();
                assetVOList.addAll(AssetClient.getInstance().getAssetSiteAreaByCustomerSiteArea(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue()));
                assetVOList.stream().sorted(Comparator.comparing(AssetVO::getAssetName)).forEachOrdered(asset -> assetList.add(new SelectItem(asset.getAssetId(), asset.getAssetName())));

                Optional<AssetVO> selectedAsset = assetVOList.stream()
                        .filter(asset -> selectedFilters.getAsset().getValue().equals(asset.getAssetId()) && selectedFilters.getAsset().getLabel().equals(asset.getAssetName()))
                        .findFirst();

                if (selectedAsset.isPresent()) {
                    assetType = selectedAsset.get().getAssetType();
                }

                pointLocationList.clear();
                list = PointLocationClient.getInstance().getPointLocationsByCustomerSiteAreaAsset(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue(), (UUID) selectedFilters.getAsset().getValue());
                list.stream().sorted(Comparator.comparing(PointLocationVO::getPointLocationName)).forEachOrdered(pointLocation -> pointLocationList.add(new SelectItem(pointLocation.getPointLocationId(), pointLocation.getPointLocationName())));
            } catch (Exception ex) {
                Logger.getLogger(AssetAnalysisBean.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
                pointLocationList.clear();
            }
            assetInfo();
            if (reloadUI) {
                updateTreeNodeRoot(ASSET);
                updateBreadcrumbModel();
                updateUI("content");
            }
        } else {
            onAreaChange(reloadUI);
        }
    }

    /**
     * called when the point location changed
     *
     * @param reloadUI
     */
    @Override
    public void onPointLocationChange(boolean reloadUI) {
        List<PointVO> list;

        if (selectedFilters == null) {
            setSelectedFilters();
        }
        if (selectedFilters.getPointLocation() != null && selectedFilters.getPointLocation().getLabel() != null && !selectedFilters.getPointLocation().getLabel().isEmpty() && selectedFilters.getPointLocation().getValue() != null) {
            selectedFilters.clear(POINT_LOCATION);
            apSetList.clear();
            alSetParamList.clear();
            try {
                pointList.clear();
                list = PointClient.getInstance().getPointsByCustomerSiteAreaAssetPointLocation(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue(), (UUID) selectedFilters.getAsset().getValue(), (UUID) selectedFilters.getPointLocation().getValue(), true);
                list.stream().sorted(Comparator.comparing(PointVO::getPointName)).forEachOrdered(point -> pointList.add(new SelectItem(point.getPointId(), point.getPointName())));
            } catch (Exception ex) {
                Logger.getLogger(AssetAnalysisBean.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
                pointList.clear();
            }

            if (reloadUI) {
                updateTreeNodeRoot(POINT_LOCATION);
                updateBreadcrumbModel();
                updateUI("content");
            }
        } else {
            onAssetChange(reloadUI);
        }
    }

    /**
     * called when the point changed
     *
     * @param reloadUI
     */
    @Override
    public void onPointChange(boolean reloadUI) {
        List<PointVO> pointVOList;
        List<SelectItem> list;

        if (selectedFilters == null) {
            setSelectedFilters();
        }
        if (selectedFilters.getPoint() != null && selectedFilters.getPoint().getLabel() != null && !selectedFilters.getPoint().getLabel().isEmpty() && selectedFilters.getPoint().getValue() != null) {
            selectedFilters.clear(POINT);
            alSetParamList.clear();
            try {
                apSetList.clear();

                list = new ArrayList();
                pointVOList = PointClient.getInstance().getPointsByCustomerSiteAreaAssetPointLocationPoint(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue(), (UUID) selectedFilters.getAsset().getValue(), (UUID) selectedFilters.getPointLocation().getValue(), (UUID) selectedFilters.getPoint().getValue(), false);
                pointVOList.stream().forEachOrdered(point -> {
                    List<ApAlSetVO> apAlSetsVOList;

                    if ((apAlSetsVOList = APALSetClient.getInstance().getSiteApAlSetsByCustomerSiteIdApSetIdAlSetId(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), point.getSiteId(), point.getApSetId(), point.getAlSetId())) == null || apAlSetsVOList.isEmpty()) {
                        if ((apAlSetsVOList = APALSetClient.getInstance().getGlobalApAlSetsByCustomerApSetIdAlSetId(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), point.getApSetId(), point.getAlSetId())) == null || apAlSetsVOList.isEmpty()) {
                            apAlSetsVOList = APALSetClient.getInstance().getGlobalApAlSetsByCustomerApSetIdAlSetId(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), DEFAULT_UPTIME_ACCOUNT, point.getApSetId(), point.getAlSetId());
                        }
                    }

                    if (apAlSetsVOList != null && !apAlSetsVOList.isEmpty()) {
                        list.add(new SelectItem(apAlSetsVOList.get(0).getApSetId(), apAlSetsVOList.get(0).getApSetName()));
                    }
                });

                list.stream().sorted(Comparator.comparing(SelectItem::getLabel)).forEachOrdered(item -> apSetList.add(item));
            } catch (Exception ex) {
                Logger.getLogger(AssetAnalysisBean.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
                apSetList.clear();
            }

            if (reloadUI) {
                updateTreeNodeRoot(POINT);
                updateBreadcrumbModel();
                updateUI("content");
            }
        } else {
            onPointLocationChange(reloadUI);
        }
    }

    /**
     * called when the apSet changed
     *
     * @param reloadUI
     */
    @Override
    public void onApSetChange(boolean reloadUI) {
        List<PointVO> list;

        if (selectedFilters == null) {
            setSelectedFilters();
        }
        if (selectedFilters.getApSet() != null && selectedFilters.getApSet().getLabel() != null && !selectedFilters.getApSet().getLabel().isEmpty() && selectedFilters.getApSet().getValue() != null) {
            selectedFilters.clear(AP_SET);
            try {
                alSetParamList.clear();
                list = ApALByPointClient.getInstance().getApALByPoints_bySiteAreaAssetPointLocationPointApSet(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue(), (UUID) selectedFilters.getAsset().getValue(), (UUID) selectedFilters.getPointLocation().getValue(), (UUID) selectedFilters.getPoint().getValue(), (UUID) selectedFilters.getApSet().getValue());
                list.stream().sorted(Comparator.comparing(PointVO::getPointName)).forEachOrdered(point -> alSetParamList.add(new SelectItem(point.getAlSetId(), point.getParamName())));
            } catch (Exception ex) {
                Logger.getLogger(AssetAnalysisBean.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
                alSetParamList.clear();
            }

            if (reloadUI) {
                updateTreeNodeRoot(AP_SET);
                updateBreadcrumbModel();
                updateUI("content");
            }
        } else {
            onPointChange(reloadUI);
        }
    }

    /**
     * called when the alSetParam changed
     *
     * @param reloadUI
     */
    @Override
    public void onAlSetParamChange(boolean reloadUI) {
        AssetAnalysisWindowBean assetAnalysisWindowBean;

        if (selectedFilters == null) {
            setSelectedFilters();
        }
        if (selectedFilters.getAlSetParam() != null && selectedFilters.getAlSetParam().getLabel() != null && !selectedFilters.getAlSetParam().getLabel().isEmpty() && selectedFilters.getAlSetParam().getValue() != null) {

            try {
                if ((assetAnalysisWindowBean = (AssetAnalysisWindowBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("assetAnalysisWindowBean")) == null) {
                    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("assetAnalysisWindowBean", new AssetAnalysisWindowBean());
                    assetAnalysisWindowBean = (AssetAnalysisWindowBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("assetAnalysisWindowBean");
                    assetAnalysisWindowBean.init();
                }
                assetAnalysisWindowBean.loadTrendGraph(selectedFilters, false);
            } catch (Exception ex) {
                Logger.getLogger(AssetAnalysisBean.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
            }

            if (reloadUI) {
                updateTreeNodeRoot(AL_SET_PARAM);
                updateBreadcrumbModel();
                updateUI("content");
            }
        } else {
            onPointChange(reloadUI);
        }
    }

    /**
     * Update the UI based on the given value
     *
     * @param value
     */
    @Override
    public void updateUI(String value) {
        try {
            switch (value) {
                case "content":
                    PrimeFaces.current().ajax().update("assetAnalysisPanelId");
                    break;
                case "tree":
                    PrimeFaces.current().ajax().update("leftTreeFormId");
                    break;
                case "breadcrumb":
                    PrimeFaces.current().ajax().update("breadcrumbFormId");
                    break;
            }
        } catch (Exception e) {
            Logger.getLogger(AssetAnalysisBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Update the breadcrumbModel field from the NavigationBean
     */
    @Override
    public void updateBreadcrumbModel() {
        DefaultMenuItem index;

        if (navigationBean != null && userManageBean != null) {

            // Set Root
            navigationBean.setBreadcrumbModel(new DefaultMenuModel());
            index = new DefaultMenuItem();
            index.setValue(userManageBean.getResourceBundleString("breadcrumb_7"));
            index.setIcon("pi pi-fw pi-chart-line");
            index.setCommand("#{assetAnalysisBean.resetPage()}");
            index.setOncomplete("updateTreeNodes()");
            navigationBean.getBreadcrumbModel().getElements().add(index);

            try {
                // Set Region
                if (selectedFilters.getRegion() != null && !selectedFilters.getRegion().isEmpty()) {
                    updateBreadcrumbModelHelper(selectedFilters.getRegion(), "#{assetAnalysisBean.onRegionChange(true)}", navigationBean.getBreadcrumbModel().getElements());
                    assertInfo = false;
                }

                // Set Country
                if (selectedFilters.getCountry() != null && !selectedFilters.getCountry().isEmpty()) {
                    updateBreadcrumbModelHelper(selectedFilters.getCountry(), "#{assetAnalysisBean.onCountryChange(true)}", navigationBean.getBreadcrumbModel().getElements());
                    assertInfo = false;
                }

                // Set Site
                if (selectedFilters.getSite() != null && selectedFilters.getSite().getLabel() != null && !selectedFilters.getSite().getLabel().isEmpty()) {
                    updateBreadcrumbModelHelper(selectedFilters.getSite().getLabel(), "#{assetAnalysisBean.onSiteChange(true)}", navigationBean.getBreadcrumbModel().getElements());
                    assertInfo = false;
                }

                // Set Area
                if (selectedFilters.getArea() != null && selectedFilters.getArea().getLabel() != null && !selectedFilters.getArea().getLabel().isEmpty()) {
                    updateBreadcrumbModelHelper(selectedFilters.getArea().getLabel(), "#{assetAnalysisBean.onAreaChange(true)}", navigationBean.getBreadcrumbModel().getElements());
                    assertInfo = false;
                }

                // Set Asset
                if (selectedFilters.getAsset() != null && selectedFilters.getAsset().getLabel() != null && !selectedFilters.getAsset().getLabel().isEmpty()) {
                    updateBreadcrumbModelHelper(selectedFilters.getAsset().getLabel(), "#{assetAnalysisBean.onAssetChange(true)}", navigationBean.getBreadcrumbModel().getElements());
                    assertInfo = true;
                }

                // Set Point Location
                if (selectedFilters.getPointLocation() != null && selectedFilters.getPointLocation().getLabel() != null && !selectedFilters.getPointLocation().getLabel().isEmpty()) {
                    updateBreadcrumbModelHelper(selectedFilters.getPointLocation().getLabel(), "#{assetAnalysisBean.onPointLocationChange(true)}", navigationBean.getBreadcrumbModel().getElements());
                    assertInfo = false;
                }

                // Set Point
                if (selectedFilters.getPoint() != null && selectedFilters.getPoint().getLabel() != null && !selectedFilters.getPoint().getLabel().isEmpty()) {
                    updateBreadcrumbModelHelper(selectedFilters.getPoint().getLabel(), "#{assetAnalysisBean.onPointChange(true)}", navigationBean.getBreadcrumbModel().getElements());
                    assertInfo = false;
                }

                // Set Ap Set
                if (selectedFilters.getApSet() != null && selectedFilters.getApSet().getLabel() != null && !selectedFilters.getApSet().getLabel().isEmpty()) {
                    updateBreadcrumbModelHelper(selectedFilters.getApSet().getLabel(), "#{assetAnalysisBean.onApSetChange(true)}", navigationBean.getBreadcrumbModel().getElements());
                    assertInfo = false;
                }

                // Set Ap, Al, And Param
                if (selectedFilters.getAlSetParam() != null && selectedFilters.getAlSetParam().getLabel() != null && !selectedFilters.getAlSetParam().getLabel().isEmpty()) {
                    updateBreadcrumbModelHelper(selectedFilters.getAlSetParam().getLabel(), "#{assetAnalysisBean.onAlSetParamChange(true)}", navigationBean.getBreadcrumbModel().getElements());
                    assertInfo = false;
                }
            } catch (Exception e) {
                Logger.getLogger(AssetAnalysisBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);

                // Set Root
                navigationBean.setBreadcrumbModel(new DefaultMenuModel());
                index = new DefaultMenuItem();
                index.setValue(userManageBean.getResourceBundleString("breadcrumb_7"));
                index.setIcon("pi pi-fw pi-chart-line");
                index.setCommand("#{assetAnalysisBean.resetPage()}");
                index.setOncomplete("updateTreeNodes()");
                navigationBean.getBreadcrumbModel().getElements().add(index);
            }
            updateUI("breadcrumb");
        }
    }

    /**
     * Build the treeNodeRoot field from the NavigationBean
     *
     * @param type, String Object
     * @param data, Object
     * @param parent, TreeNode Object
     * @param list, List Object of Objects
     */
    @Override
    public void treeContentMenuBuilder(String type, Object data, TreeNode parent, List<Object> list) {
        TreeNode newNode;
        boolean useList = false;

        if (navigationBean != null && userManageBean != null) {
            if (type != null) {
                switch (type.toLowerCase()) {
                    case ROOT:
                        newNode = new DefaultTreeNode(ROOT, data, null);
                        navigationBean.setTreeNodeRoot(newNode);
                        if (regionCountrySiteMap.keySet() != null && !regionCountrySiteMap.keySet().isEmpty()) {
                            regionCountrySiteMap.keySet().stream().sorted().collect(Collectors.toList()).forEach(key -> treeContentMenuBuilder(REGION, key, navigationBean.getTreeNodeRoot(), null));
                        }
                        updateUI("tree");
                        break;
                    case REGION:
                        list = (data == null) ? new ArrayList(regionCountrySiteMap.keySet().stream().sorted().collect(Collectors.toList())) : new ArrayList();
                        treeContentMenuBuilderHelper(type, data, parent, list, true);
                        break;
                    case COUNTRY:
                        if (parent != null) {
                            list = (data == null) ? new ArrayList(regionCountrySiteMap.get((String) parent.getData()).keySet().stream().sorted().collect(Collectors.toList())) : new ArrayList();
                            treeContentMenuBuilderHelper(type, data, parent, list, true);
                        }
                        break;
                    case SITE_OVER_100:
                        list = new ArrayList(regionCountrySiteMap.get((String) parent.getParent().getData()).get((String) parent.getData()).stream().sorted(Comparator.comparing(SelectItem::getLabel)).collect(Collectors.toList()));
                        treeContentMenuBuilderHelper(type, data, SITE, parent, list, true);
                        break;
                    case SITE:
                        if (parent != null) {
                            list = (data == null) ? new ArrayList(regionCountrySiteMap.get((String) parent.getParent().getData()).get((String) parent.getData()).stream().sorted(Comparator.comparing(SelectItem::getLabel)).collect(Collectors.toList())) : new ArrayList();
                            treeContentMenuBuilderHelper(type, data, parent, list, true);
                        }
                        break;
                    case AREA_OVER_100:
                        if (list == null || list.isEmpty()) {
                            list = areaList != null ? new ArrayList(areaList) : new ArrayList();
                        } else {
                            useList = true;
                        }
                        treeContentMenuBuilderHelper(type, data, AREA, parent, list, useList);
                        break;
                    case AREA:
                        if (list == null || list.isEmpty()) {
                            list = areaList != null ? new ArrayList(areaList) : new ArrayList();
                        }
                        treeContentMenuBuilderHelper(type, data, parent, list, true);
                        break;
                    case ASSET_OVER_100:
                        if (list == null || list.isEmpty()) {
                            list = assetList != null ? new ArrayList(assetList) : new ArrayList();
                        } else {
                            useList = true;
                        }
                        treeContentMenuBuilderHelper(type, data, ASSET, parent, list, useList);
                        break;
                    case ASSET:
                        if (list == null || list.isEmpty()) {
                            list = assetList != null ? new ArrayList(assetList) : new ArrayList();
                        }
                        treeContentMenuBuilderHelper(type, data, parent, list, true);
                        break;
                    case POINT_LOCATION_OVER_100:
                        if (list == null || list.isEmpty()) {
                            list = pointLocationList != null ? new ArrayList(pointLocationList) : new ArrayList();
                        } else {
                            useList = true;
                        }
                        treeContentMenuBuilderHelper(type, data, POINT_LOCATION, parent, list, useList);
                        break;
                    case POINT_LOCATION:
                        if (list == null || list.isEmpty()) {
                            list = pointLocationList != null ? new ArrayList(pointLocationList) : new ArrayList();
                        }
                        treeContentMenuBuilderHelper(type, data, parent, list, true);
                        break;
                    case POINT_OVER_100:
                        if (list == null || list.isEmpty()) {
                            list = pointList != null ? new ArrayList(pointList) : new ArrayList();
                        } else {
                            useList = true;
                        }
                        treeContentMenuBuilderHelper(type, data, POINT, parent, list, useList);
                        break;
                    case POINT:
                        if (list == null || list.isEmpty()) {
                            list = pointList != null ? new ArrayList(pointList) : new ArrayList();
                        }
                        treeContentMenuBuilderHelper(type, data, parent, list, true);
                        break;
                    case AP_SET_OVER_100:
                        if (list == null || list.isEmpty()) {
                            list = apSetList != null ? new ArrayList(apSetList) : new ArrayList();
                        } else {
                            useList = true;
                        }
                        treeContentMenuBuilderHelper(type, data, AP_SET, parent, list, useList);
                        break;
                    case AP_SET:
                        if (list == null || list.isEmpty()) {
                            list = apSetList != null ? new ArrayList(apSetList) : new ArrayList();
                        }
                        treeContentMenuBuilderHelper(type, data, parent, list, true);
                        break;
                    case AL_SET_PARAM_OVER_100:
                        if (list == null || list.isEmpty()) {
                            list = alSetParamList != null ? new ArrayList(alSetParamList) : new ArrayList();
                        } else {
                            useList = true;
                        }
                        treeContentMenuBuilderHelper(type, data, AL_SET_PARAM, parent, list, useList);
                        break;
                    case AL_SET_PARAM:
                        if (list == null || list.isEmpty()) {
                            list = alSetParamList != null ? new ArrayList(alSetParamList) : new ArrayList();
                        }
                        treeContentMenuBuilderHelper(type, data, parent, list, false);
                        break;
                    case EMPTY:
                        if (parent != null) {
                            newNode = new DefaultTreeNode(type, data, parent);
                            newNode.setSelectable(false);
                        }
                        break;
                }
            } else {
                navigationBean.setTreeNodeRoot(new DefaultTreeNode(ROOT, new SelectItem(null, userManageBean.getResourceBundleString("breadcrumb_4")), null));
                updateUI("tree");
            }
        }
    }

    /**
     * Used for the NodeExpandEvent caused by the Nav Tree component
     *
     * @param event, NodeExpandEvent Object
     */
    @Override
    public void onNodeExpand(NodeExpandEvent event) {
        TreeNode parent;
        List<Object> list = new ArrayList();
        List<SelectItem> selectItemlist = new ArrayList();
        List<PointVO> ptList;
        List<ApAlSetVO> apAlByPtList;
        UUID siteId = null;
        UUID areaId = null;
        UUID assetId = null;
        UUID pointLocationId = null;
        UUID pointId = null;
        UUID alSetId;

        try {
            if (event != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null) {
                switch (event.getTreeNode().getType()) {
                    case REGION:
                        event.getTreeNode().getChildren().clear();
                        treeContentMenuBuilder(COUNTRY, null, event.getTreeNode(), null);
                        assertInfo = false;
                        break;
                    case COUNTRY:
                        if (regionCountrySiteMap.get((String) event.getTreeNode().getParent().getData()).get((String) event.getTreeNode().getData()).size() > 100) {
                            event.getTreeNode().getChildren().clear();
                            treeContentMenuBuilder(SITE_OVER_100, null, event.getTreeNode(), null);
                        } else if (!regionCountrySiteMap.get((String) event.getTreeNode().getParent().getData()).get((String) event.getTreeNode().getData()).isEmpty()) {
                            event.getTreeNode().getChildren().clear();
                            treeContentMenuBuilder(SITE, null, event.getTreeNode(), null);
                        }
                        assertInfo = false;
                        break;
                    case SITE:
                        siteId = (UUID) ((SelectItem) event.getTreeNode().getData()).getValue();
                        AreaClient.getInstance().getAreaSiteByCustomerSite(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), siteId).stream().sorted(Comparator.comparing(AreaVO::getAreaName)).forEachOrdered(vo -> list.add(new SelectItem(vo.getAreaId(), vo.getAreaName())));
                        if (list.size() > 100) {
                            event.getTreeNode().getChildren().clear();
                            treeContentMenuBuilder(AREA_OVER_100, null, event.getTreeNode(), list);
                        } else if (!list.isEmpty()) {
                            event.getTreeNode().getChildren().clear();
                            treeContentMenuBuilder(AREA, null, event.getTreeNode(), list);
                        }
                        assertInfo = false;
                        break;
                    case AREA:
                        siteId = event.getTreeNode().getParent().getType().equals(SITE) ? (UUID) ((SelectItem) event.getTreeNode().getParent().getData()).getValue() : (UUID) ((SelectItem) event.getTreeNode().getParent().getParent().getData()).getValue();
                        areaId = (UUID) ((SelectItem) event.getTreeNode().getData()).getValue();
                        AssetClient.getInstance().getAssetSiteAreaByCustomerSiteArea(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), siteId, areaId).stream().sorted(Comparator.comparing(AssetVO::getAssetName)).forEachOrdered(vo -> list.add(new SelectItem(vo.getAssetId(), vo.getAssetName())));
                        if (list.size() > 100) {
                            event.getTreeNode().getChildren().clear();
                            treeContentMenuBuilder(ASSET_OVER_100, null, event.getTreeNode(), list);
                        } else if (!list.isEmpty()) {
                            event.getTreeNode().getChildren().clear();
                            treeContentMenuBuilder(ASSET, null, event.getTreeNode(), list);
                        }
                        assertInfo = false;
                        break;
                    case ASSET:
                        parent = event.getTreeNode().getParent();
                        do {
                            if (parent.getType().equals(SITE)) {
                                siteId = (UUID) ((SelectItem) parent.getData()).getValue();
                            } else if (parent.getType().equals(AREA)) {
                                areaId = (UUID) ((SelectItem) parent.getData()).getValue();
                            }
                            parent = parent.getParent();
                        } while (siteId == null || areaId == null);
                        assetId = (UUID) ((SelectItem) event.getTreeNode().getData()).getValue();
                        PointLocationClient.getInstance().getPointLocationsByCustomerSiteAreaAsset(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), siteId, areaId, assetId).stream().sorted(Comparator.comparing(PointLocationVO::getPointLocationName)).forEachOrdered(vo -> list.add(new SelectItem(vo.getPointLocationId(), vo.getPointLocationName())));
                        if (list.size() > 100) {
                            event.getTreeNode().getChildren().clear();
                            treeContentMenuBuilder(POINT_LOCATION_OVER_100, null, event.getTreeNode(), list);
                        } else if (!list.isEmpty()) {
                            event.getTreeNode().getChildren().clear();
                            treeContentMenuBuilder(POINT_LOCATION, null, event.getTreeNode(), list);
                        }
                        assertInfo = true;
                        break;
                    case POINT_LOCATION:
                        parent = event.getTreeNode().getParent();
                        do {
                            switch (parent.getType()) {
                                case SITE:
                                    siteId = (UUID) ((SelectItem) parent.getData()).getValue();
                                    break;
                                case AREA:
                                    areaId = (UUID) ((SelectItem) parent.getData()).getValue();
                                    break;
                                case ASSET:
                                    assetId = (UUID) ((SelectItem) parent.getData()).getValue();
                                    break;
                                default:
                                    break;
                            }
                            parent = parent.getParent();
                        } while (siteId == null || areaId == null || assetId == null);
                        pointLocationId = (UUID) ((SelectItem) event.getTreeNode().getData()).getValue();
                        PointClient.getInstance().getPointsByCustomerSiteAreaAssetPointLocation(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), siteId, areaId, assetId, pointLocationId, true).stream().sorted(Comparator.comparing(PointVO::getPointName)).forEachOrdered(vo -> list.add(new SelectItem(vo.getPointId(), vo.getPointName())));
                        if (list.size() > 100) {
                            event.getTreeNode().getChildren().clear();
                            treeContentMenuBuilder(POINT_OVER_100, null, event.getTreeNode(), list);
                        } else if (!list.isEmpty()) {
                            event.getTreeNode().getChildren().clear();
                            treeContentMenuBuilder(POINT, null, event.getTreeNode(), list);
                        }
                        assertInfo = false;
                        break;
                    case POINT:
                        parent = event.getTreeNode().getParent();
                        do {
                            switch (parent.getType()) {
                                case SITE:
                                    siteId = (UUID) ((SelectItem) parent.getData()).getValue();
                                    break;
                                case AREA:
                                    areaId = (UUID) ((SelectItem) parent.getData()).getValue();
                                    break;
                                case ASSET:
                                    assetId = (UUID) ((SelectItem) parent.getData()).getValue();
                                    break;
                                case POINT_LOCATION:
                                    pointLocationId = (UUID) ((SelectItem) parent.getData()).getValue();
                                    break;
                                default:
                                    break;
                            }
                            parent = parent.getParent();
                        } while (siteId == null || areaId == null || assetId == null || pointLocationId == null);
                        pointId = (UUID) ((SelectItem) event.getTreeNode().getData()).getValue();

                        // here first need to query to points table to get ap_set_id.
                        ptList = PointClient.getInstance().getPointsByCustomerSiteAreaAssetPointLocationPoint(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), siteId, areaId, assetId, pointLocationId, pointId, false);
                        for (PointVO point : ptList) {
                            if ((apAlByPtList = APALSetClient.getInstance().getSiteApAlSetsByCustomerSiteIdApSetIdAlSetId(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), siteId, point.getApSetId(), point.getAlSetId())) == null || apAlByPtList.isEmpty()) {
                                apAlByPtList = APALSetClient.getInstance().getGlobalApAlSetsByCustomerApSetIdAlSetId(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), point.getApSetId(), point.getAlSetId());
                                if (apAlByPtList == null || apAlByPtList.isEmpty()) {
                                    apAlByPtList = APALSetClient.getInstance().getGlobalApAlSetsByCustomerApSetIdAlSetId(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), DEFAULT_UPTIME_ACCOUNT, point.getApSetId(), point.getAlSetId());
                                }
                            }

                            if (apAlByPtList != null && !apAlByPtList.isEmpty()) {
                                if (!point.isDisabled()) {
                                    selectItemlist.add(new SelectItem(apAlByPtList.get(0).getApSetId(), apAlByPtList.get(0).getApSetName(), PRE_NODE_ICON));
                                } else {
                                    selectItemlist.add(new SelectItem(apAlByPtList.get(0).getApSetId(), apAlByPtList.get(0).getApSetName()));
                                }
                            }
                        }

                        selectItemlist.stream().sorted(Comparator.comparing(SelectItem::getLabel)).forEachOrdered(item -> list.add(item));
                        if (list.size() > 100) {
                            event.getTreeNode().getChildren().clear();
                            treeContentMenuBuilder(AP_SET_OVER_100, null, event.getTreeNode(), list);
                        } else if (!list.isEmpty()) {
                            event.getTreeNode().getChildren().clear();
                            treeContentMenuBuilder(AP_SET, null, event.getTreeNode(), list);
                        }
                        assertInfo = false;
                        break;
                    case AP_SET:
                        parent = event.getTreeNode().getParent();
                        do {
                            switch (parent.getType()) {
                                case SITE:
                                    siteId = (UUID) ((SelectItem) parent.getData()).getValue();
                                    break;
                                case AREA:
                                    areaId = (UUID) ((SelectItem) parent.getData()).getValue();
                                    break;
                                case ASSET:
                                    assetId = (UUID) ((SelectItem) parent.getData()).getValue();
                                    break;
                                case POINT_LOCATION:
                                    pointLocationId = (UUID) ((SelectItem) parent.getData()).getValue();
                                    break;
                                case POINT:
                                    pointId = (UUID) ((SelectItem) parent.getData()).getValue();
                                    break;
                                default:
                                    break;
                            }
                            parent = parent.getParent();
                        } while (siteId == null || areaId == null || assetId == null || pointLocationId == null || pointId == null);
                        alSetId = (UUID) ((SelectItem) event.getTreeNode().getData()).getValue();
                        ApALByPointClient.getInstance().getApALByPoints_bySiteAreaAssetPointLocationPointApSet(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), siteId, areaId, assetId, pointLocationId, pointId, alSetId).stream().sorted(Comparator.comparing(PointVO::getParamName)).forEachOrdered(point -> list.add(new SelectItem(point.getAlSetId(), point.getParamName())));
                        if (list.size() > 100) {
                            event.getTreeNode().getChildren().clear();
                            treeContentMenuBuilder(AL_SET_PARAM_OVER_100, null, event.getTreeNode(), list);
                        } else if (!list.isEmpty()) {
                            event.getTreeNode().getChildren().clear();
                            treeContentMenuBuilder(AL_SET_PARAM, null, event.getTreeNode(), list);
                        }
                        assertInfo = false;
                        break;
                }
            }
        } catch (Exception e) {
            Logger.getLogger(AssetAnalysisBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Used for the NodeCollapseEvent caused by the Nav Tree component
     *
     * @param event, NodeCollapseEvent Object
     */
    @Override
    public void onNodeCollapse(NodeCollapseEvent event) {
        try {
            if (event != null && (event.getTreeNode().getType().equals(REGION) || event.getTreeNode().getType().equals(COUNTRY) || event.getTreeNode().getType().equals(SITE) || event.getTreeNode().getType().equals(AREA) || event.getTreeNode().getType().equals(ASSET) || event.getTreeNode().getType().equals(POINT_LOCATION) || event.getTreeNode().getType().equals(POINT) || event.getTreeNode().getType().equals(AP_SET))) {
                event.getTreeNode().getChildren().clear();
                treeContentMenuBuilder(EMPTY, null, event.getTreeNode(), null);
            }
        } catch (Exception e) {
            Logger.getLogger(AssetAnalysisBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Used for the NodeSelectEvent caused by the Nav Tree component
     *
     * @param event, NodeSelectEvent Object
     */
    @Override
    public void onNodeSelect(NodeSelectEvent event) {
        TreeNode parent;
        final List<AreaVO> areaVOList = new ArrayList();
        final List<AssetVO> assetVOList = new ArrayList();
        final List<PointLocationVO> pointLocationVOList = new ArrayList();
        final List<PointVO> pointVOList = new ArrayList(), apAlByPointList = new ArrayList();
        final List<SelectItem> list = new ArrayList();
        AssetAnalysisWindowBean assetAnalysisWindowBean;
        long startTime = System.currentTimeMillis();
        try {
            if (event != null) {
                parent = event.getTreeNode().getParent();
                do {
                    if (parent != null) {
                        switch (parent.getType()) {
                            case REGION:
                                selectedFilters.setRegion((String) parent.getData());
                                parent = null;
                                assertInfo = false;
                                break;
                            case COUNTRY:
                                selectedFilters.setCountry((String) parent.getData());
                                assertInfo = false;
                                break;
                            case SITE_OVER_100:
                                selectedFilters.setSiteOver100((String) parent.getData());
                                assertInfo = false;
                                break;
                            case SITE:
                                selectedFilters.setSite((SelectItem) parent.getData());
                                assertInfo = false;
                                break;
                            case AREA_OVER_100:
                                selectedFilters.setAreaOver100((String) parent.getData());
                                assertInfo = false;
                                break;
                            case AREA:
                                selectedFilters.setArea((SelectItem) parent.getData());
                                assertInfo = false;
                                break;
                            case ASSET_OVER_100:
                                selectedFilters.setAssetOver100((String) parent.getData());
                                assertInfo = false;
                                break;
                            case ASSET:
                                selectedFilters.setAsset((SelectItem) parent.getData());
                                assertInfo = true;
                                break;
                            case POINT_LOCATION_OVER_100:
                                selectedFilters.setPointLocationOver100((String) parent.getData());
                                assertInfo = false;
                                break;
                            case POINT_LOCATION:
                                selectedFilters.setPointLocation((SelectItem) parent.getData());
                                assertInfo = false;
                                break;
                            case POINT_OVER_100:
                                selectedFilters.setPointOver100((String) parent.getData());
                                assertInfo = false;
                                break;
                            case POINT:
                                selectedFilters.setPoint((SelectItem) parent.getData());
                                assertInfo = false;
                                break;
                            case AP_SET_OVER_100:
                                selectedFilters.setApSetOver100((String) parent.getData());
                                assertInfo = false;
                                break;
                            case AP_SET:
                                selectedFilters.setApSet((SelectItem) parent.getData());
                                assertInfo = false;
                                break;
                            case AL_SET_PARAM_OVER_100:
                                selectedFilters.setAlSetParamOver100((String) parent.getData());
                                assertInfo = false;
                                break;
                        }
                        if (parent != null) {
                            parent = parent.getParent();
                        }
                    }
                } while (parent != null);

                switch (event.getTreeNode().getType()) {
                    case REGION:
                        selectedFilters.setRegion((String) event.getTreeNode().getData());
                        selectedFilters.clear(REGION);
                        areaList.clear();
                        assetList.clear();
                        pointLocationList.clear();
                        pointList.clear();
                        apSetList.clear();
                        alSetParamList.clear();
                        assertInfo = false;
                        break;
                    case COUNTRY:
                        selectedFilters.setCountry((String) event.getTreeNode().getData());
                        selectedFilters.clear(COUNTRY);
                        areaList.clear();
                        assetList.clear();
                        pointLocationList.clear();
                        pointList.clear();
                        apSetList.clear();
                        alSetParamList.clear();
                        assertInfo = false;
                        break;
                    case SITE:
                        selectedFilters.setSite((SelectItem) event.getTreeNode().getData());
                        selectedFilters.clear(SITE);
                        assetList.clear();
                        pointLocationList.clear();
                        pointList.clear();
                        apSetList.clear();
                        alSetParamList.clear();
                        assertInfo = false;
                        try {
                            areaList.clear();
                            areaVOList.addAll(AreaClient.getInstance().getAreaSiteByCustomerSite(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue()));
                            areaVOList.stream().sorted(Comparator.comparing(AreaVO::getAreaName)).forEachOrdered(area -> areaList.add(new SelectItem(area.getAreaId(), area.getAreaName())));
                        } catch (Exception ex) {
                            areaList.clear();
                        }
                        break;
                    case AREA:
                        selectedFilters.setArea((SelectItem) event.getTreeNode().getData());
                        selectedFilters.clear(AREA);
                        pointLocationList.clear();
                        pointList.clear();
                        apSetList.clear();
                        alSetParamList.clear();
                        assertInfo = false;
                        try {
                            areaList.clear();
                            areaVOList.addAll(AreaClient.getInstance().getAreaSiteByCustomerSite(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue()));
                            areaVOList.stream().sorted(Comparator.comparing(AreaVO::getAreaName)).forEachOrdered(area -> areaList.add(new SelectItem(area.getAreaId(), area.getAreaName())));
                        } catch (Exception ex) {
                            areaList.clear();
                        }
                        try {
                            assetList.clear();
                            assetVOList.addAll(AssetClient.getInstance().getAssetSiteAreaByCustomerSiteArea(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue()));
                            assetVOList.stream().sorted(Comparator.comparing(AssetVO::getAssetName)).forEachOrdered(asset -> assetList.add(new SelectItem(asset.getAssetId(), asset.getAssetName())));
                        } catch (Exception ex) {
                            assetList.clear();
                        }
                        break;
                    case ASSET:
                        selectedFilters.setAsset((SelectItem) event.getTreeNode().getData());
                        selectedFilters.clear(ASSET);
                        pointList.clear();
                        apSetList.clear();
                        alSetParamList.clear();
                        try {
                            areaList.clear();
                            areaVOList.addAll(AreaClient.getInstance().getAreaSiteByCustomerSite(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue()));
                            areaVOList.stream().sorted(Comparator.comparing(AreaVO::getAreaName)).forEachOrdered(area -> areaList.add(new SelectItem(area.getAreaId(), area.getAreaName())));
                            assetInfo();
                        } catch (Exception ex) {
                            areaList.clear();
                        }
                        try {
                            assetList.clear();
                            assetVOList.addAll(AssetClient.getInstance().getAssetSiteAreaByCustomerSiteArea(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue()));
                            assetVOList.stream().sorted(Comparator.comparing(AssetVO::getAssetName)).forEachOrdered(asset -> assetList.add(new SelectItem(asset.getAssetId(), asset.getAssetName())));

                            Optional<AssetVO> selectedAsset = assetVOList.stream()
                                    .filter(asset -> selectedFilters.getAsset().getValue().equals(asset.getAssetId()) && selectedFilters.getAsset().getLabel().equals(asset.getAssetName()))
                                    .findFirst();

                            if (selectedAsset.isPresent()) {
                                assetType = selectedAsset.get().getAssetType();
                            }
                        } catch (Exception ex) {
                            assetList.clear();
                        }
                        try {
                            pointLocationList.clear();
                            pointLocationVOList.addAll(PointLocationClient.getInstance().getPointLocationsByCustomerSiteAreaAsset(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue(), (UUID) selectedFilters.getAsset().getValue()));
                            pointLocationVOList.stream().sorted(Comparator.comparing(PointLocationVO::getPointLocationName)).forEachOrdered(pointLocation -> pointLocationList.add(new SelectItem(pointLocation.getPointLocationId(), pointLocation.getPointLocationName())));
                        } catch (Exception ex) {
                            pointLocationList.clear();
                        }
                        assertInfo = true;

                        // Query to Presto-db
                    /*    PrestoDAO prestoDAO = new PrestoDAO();

                        String outputJson = prestoDAO.getPointLocationsByAsset(userManageBean.getCurrentSite().getCustomerAccount(), selectedFilters.getSite().getLabel(), selectedFilters.getArea().getLabel(), selectedFilters.getAsset().getLabel());
                        HeatmapDataVO heatmapData = extractHeatmapData(outputJson);
                        heatMapJson = getHeatMapChartJson(heatmapData, selectedFilters.getArea().getLabel(), selectedFilters.getAsset().getLabel());
                        PrimeFaces.current().ajax().update("heatMapArea");  */

                        break;
                    case POINT_LOCATION:
                        selectedFilters.setPointLocation((SelectItem) event.getTreeNode().getData());
                        selectedFilters.clear(POINT_LOCATION);
                        apSetList.clear();
                        alSetParamList.clear();
                        assertInfo = true;
                        try {
                            areaList.clear();
                            areaVOList.addAll(AreaClient.getInstance().getAreaSiteByCustomerSite(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue()));
                            areaVOList.stream().sorted(Comparator.comparing(AreaVO::getAreaName)).forEachOrdered(area -> areaList.add(new SelectItem(area.getAreaId(), area.getAreaName())));
                        } catch (Exception ex) {
                            areaList.clear();
                        }
                        try {
                            assetList.clear();
                            assetVOList.addAll(AssetClient.getInstance().getAssetSiteAreaByCustomerSiteArea(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue()));
                            assetVOList.stream().sorted(Comparator.comparing(AssetVO::getAssetName)).forEachOrdered(asset -> assetList.add(new SelectItem(asset.getAssetId(), asset.getAssetName())));
                        } catch (Exception ex) {
                            assetList.clear();
                        }
                        try {
                            pointLocationList.clear();
                            pointLocationVOList.addAll(PointLocationClient.getInstance().getPointLocationsByCustomerSiteAreaAsset(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue(), (UUID) selectedFilters.getAsset().getValue()));
                            pointLocationVOList.stream().sorted(Comparator.comparing(PointLocationVO::getPointLocationName)).forEachOrdered(pointLocation -> pointLocationList.add(new SelectItem(pointLocation.getPointLocationId(), pointLocation.getPointLocationName())));
                        } catch (Exception ex) {
                            pointLocationList.clear();
                        }
                        try {
                            pointList.clear();
                            pointVOList.addAll(PointClient.getInstance().getPointsByCustomerSiteAreaAssetPointLocation(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue(), (UUID) selectedFilters.getAsset().getValue(), (UUID) selectedFilters.getPointLocation().getValue(), true));
                            pointVOList.stream().sorted(Comparator.comparing(PointVO::getPointName)).forEachOrdered(point -> pointList.add(new SelectItem(point.getPointId(), point.getPointName())));
                        } catch (Exception ex) {
                            pointList.clear();
                        }
                        break;
                    case POINT:
                        selectedFilters.setPoint((SelectItem) event.getTreeNode().getData());
                        selectedFilters.clear(POINT);
                        alSetParamList.clear();
                        assertInfo = true;
                        try {
                            areaList.clear();
                            areaVOList.addAll(AreaClient.getInstance().getAreaSiteByCustomerSite(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue()));
                            areaVOList.stream().sorted(Comparator.comparing(AreaVO::getAreaName)).forEachOrdered(area -> areaList.add(new SelectItem(area.getAreaId(), area.getAreaName())));
                        } catch (Exception ex) {
                            areaList.clear();
                        }
                        try {
                            assetList.clear();
                            assetVOList.addAll(AssetClient.getInstance().getAssetSiteAreaByCustomerSiteArea(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue()));
                            assetVOList.stream().sorted(Comparator.comparing(AssetVO::getAssetName)).forEachOrdered(asset -> assetList.add(new SelectItem(asset.getAssetId(), asset.getAssetName())));
                        } catch (Exception ex) {
                            assetList.clear();
                        }
                        try {
                            pointLocationList.clear();
                            pointLocationVOList.addAll(PointLocationClient.getInstance().getPointLocationsByCustomerSiteAreaAsset(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue(), (UUID) selectedFilters.getAsset().getValue()));
                            pointLocationVOList.stream().sorted(Comparator.comparing(PointLocationVO::getPointLocationName)).forEachOrdered(pointLocation -> pointLocationList.add(new SelectItem(pointLocation.getPointLocationId(), pointLocation.getPointLocationName())));
                        } catch (Exception ex) {
                            pointLocationList.clear();
                        }
                        try {
                            pointList.clear();
                            pointVOList.addAll(PointClient.getInstance().getPointsByCustomerSiteAreaAssetPointLocation(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue(), (UUID) selectedFilters.getAsset().getValue(), (UUID) selectedFilters.getPointLocation().getValue(), true));
                            pointVOList.stream().sorted(Comparator.comparing(PointVO::getPointName)).forEachOrdered(point -> pointList.add(new SelectItem(point.getPointId(), point.getPointName())));
                        } catch (Exception ex) {
                            pointList.clear();
                        }
                        try {
                            apSetList.clear();
                            pointVOList.addAll(PointClient.getInstance().getPointsByCustomerSiteAreaAssetPointLocationPoint(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue(), (UUID) selectedFilters.getAsset().getValue(), (UUID) selectedFilters.getPointLocation().getValue(), (UUID) selectedFilters.getPoint().getValue(), false));
                            pointVOList.stream().forEachOrdered(point -> {
                                List<ApAlSetVO> apAlSetsVOList;

                                if ((apAlSetsVOList = APALSetClient.getInstance().getSiteApAlSetsByCustomerSiteIdApSetIdAlSetId(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), point.getSiteId(), point.getApSetId(), point.getAlSetId())) == null || apAlSetsVOList.isEmpty()) {
                                    if ((apAlSetsVOList = APALSetClient.getInstance().getGlobalApAlSetsByCustomerApSetIdAlSetId(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), point.getApSetId(), point.getAlSetId())) == null || apAlSetsVOList.isEmpty()) {
                                        apAlSetsVOList = APALSetClient.getInstance().getGlobalApAlSetsByCustomerApSetIdAlSetId(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), DEFAULT_UPTIME_ACCOUNT, point.getApSetId(), point.getAlSetId());
                                    }
                                }

                                if (apAlSetsVOList != null && !apAlSetsVOList.isEmpty()) {
                                    list.add(new SelectItem(apAlSetsVOList.get(0).getApSetId(), apAlSetsVOList.get(0).getApSetName()));
                                }
                            });

                            list.stream().sorted(Comparator.comparing(SelectItem::getLabel)).forEachOrdered(item -> apSetList.add(item));
                        } catch (Exception ex) {
                            apSetList.clear();
                        }
                        break;
                    case AP_SET:
                        selectedFilters.setApSet((SelectItem) event.getTreeNode().getData());
                        selectedFilters.clear(AP_SET);
                        assertInfo = true;
                        try {
                            areaList.clear();
                            areaVOList.addAll(AreaClient.getInstance().getAreaSiteByCustomerSite(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue()));
                            areaVOList.stream().sorted(Comparator.comparing(AreaVO::getAreaName)).forEachOrdered(area -> areaList.add(new SelectItem(area.getAreaId(), area.getAreaName())));
                        } catch (Exception ex) {
                            areaList.clear();
                        }
                        try {
                            assetList.clear();
                            assetVOList.addAll(AssetClient.getInstance().getAssetSiteAreaByCustomerSiteArea(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue()));
                            assetVOList.stream().sorted(Comparator.comparing(AssetVO::getAssetName)).forEachOrdered(asset -> assetList.add(new SelectItem(asset.getAssetId(), asset.getAssetName())));
                        } catch (Exception ex) {
                            assetList.clear();
                        }
                        try {
                            pointLocationList.clear();
                            pointLocationVOList.addAll(PointLocationClient.getInstance().getPointLocationsByCustomerSiteAreaAsset(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue(), (UUID) selectedFilters.getAsset().getValue()));
                            pointLocationVOList.stream().sorted(Comparator.comparing(PointLocationVO::getPointLocationName)).forEachOrdered(pointLocation -> pointLocationList.add(new SelectItem(pointLocation.getPointLocationId(), pointLocation.getPointLocationName())));
                        } catch (Exception ex) {
                            pointLocationList.clear();
                        }
                        try {
                            pointList.clear();
                            pointVOList.addAll(PointClient.getInstance().getPointsByCustomerSiteAreaAssetPointLocation(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue(), (UUID) selectedFilters.getAsset().getValue(), (UUID) selectedFilters.getPointLocation().getValue(), true));
                            pointVOList.stream().sorted(Comparator.comparing(PointVO::getPointName)).forEachOrdered(point -> pointList.add(new SelectItem(point.getPointId(), point.getPointName())));
                        } catch (Exception ex) {
                            pointList.clear();
                        }
                        try {
                            apSetList.clear();
                            pointVOList.addAll(PointClient.getInstance().getPointsByCustomerSiteAreaAssetPointLocationPoint(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue(), (UUID) selectedFilters.getAsset().getValue(), (UUID) selectedFilters.getPointLocation().getValue(), (UUID) selectedFilters.getPoint().getValue(), false));
                            pointVOList.stream().forEachOrdered(point -> {
                                List<ApAlSetVO> apAlSetsVOList;

                                if ((apAlSetsVOList = APALSetClient.getInstance().getSiteApAlSetsByCustomerSiteIdApSetIdAlSetId(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), point.getSiteId(), point.getApSetId(), point.getAlSetId())) == null || apAlSetsVOList.isEmpty()) {
                                    if ((apAlSetsVOList = APALSetClient.getInstance().getGlobalApAlSetsByCustomerApSetIdAlSetId(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), point.getApSetId(), point.getAlSetId())) == null || apAlSetsVOList.isEmpty()) {
                                        apAlSetsVOList = APALSetClient.getInstance().getGlobalApAlSetsByCustomerApSetIdAlSetId(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), DEFAULT_UPTIME_ACCOUNT, point.getApSetId(), point.getAlSetId());
                                    }
                                }

                                if (apAlSetsVOList != null && !apAlSetsVOList.isEmpty()) {
                                    list.add(new SelectItem(apAlSetsVOList.get(0).getApSetId(), apAlSetsVOList.get(0).getApSetName()));
                                }
                            });

                            list.stream().sorted(Comparator.comparing(SelectItem::getLabel)).forEachOrdered(item -> apSetList.add(item));
                        } catch (Exception ex) {
                            apSetList.clear();
                        }
                        try {
                            alSetParamList.clear();
                            apAlByPointList.addAll(ApALByPointClient.getInstance().getApALByPoints_bySiteAreaAssetPointLocationPointApSet(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue(), (UUID) selectedFilters.getAsset().getValue(), (UUID) selectedFilters.getPointLocation().getValue(), (UUID) selectedFilters.getPoint().getValue(), (UUID) selectedFilters.getApSet().getValue()));
                            apAlByPointList.stream().sorted(Comparator.comparing(PointVO::getPointName)).forEachOrdered(point -> alSetParamList.add(new SelectItem(point.getAlSetId(), point.getParamName())));
                        } catch (Exception ex) {
                            alSetParamList.clear();
                        }
                        break;
                    case AL_SET_PARAM:
                        selectedFilters.setAlSetParam((SelectItem) event.getTreeNode().getData());
                        Logger.getLogger(AssetAnalysisBean.class.getName()).log(Level.INFO, "Timer1: {0}", System.currentTimeMillis() - startTime);
                        assertInfo = true;
                        try {
                            areaList.clear();
                            areaVOList.addAll(AreaClient.getInstance().getAreaSiteByCustomerSite(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue()));
                            areaVOList.stream().sorted(Comparator.comparing(AreaVO::getAreaName)).forEachOrdered(area -> areaList.add(new SelectItem(area.getAreaId(), area.getAreaName())));
                        } catch (Exception ex) {
                            areaList.clear();
                        }
                        try {
                            assetList.clear();
                            assetVOList.addAll(AssetClient.getInstance().getAssetSiteAreaByCustomerSiteArea(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue()));
                            assetVOList.stream().sorted(Comparator.comparing(AssetVO::getAssetName)).forEachOrdered(asset -> assetList.add(new SelectItem(asset.getAssetId(), asset.getAssetName())));
                        } catch (Exception ex) {
                            assetList.clear();
                        }
                        try {
                            pointLocationList.clear();
                            pointLocationVOList.addAll(PointLocationClient.getInstance().getPointLocationsByCustomerSiteAreaAsset(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue(), (UUID) selectedFilters.getAsset().getValue()));
                            pointLocationVOList.stream().sorted(Comparator.comparing(PointLocationVO::getPointLocationName)).forEachOrdered(pointLocation -> pointLocationList.add(new SelectItem(pointLocation.getPointLocationId(), pointLocation.getPointLocationName())));
                        } catch (Exception ex) {
                            pointLocationList.clear();
                        }
                        try {
                            pointList.clear();
                            pointVOList.addAll(PointClient.getInstance().getPointsByCustomerSiteAreaAssetPointLocation(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue(), (UUID) selectedFilters.getAsset().getValue(), (UUID) selectedFilters.getPointLocation().getValue(), true));
                            pointVOList.stream().sorted(Comparator.comparing(PointVO::getPointName)).forEachOrdered(point -> pointList.add(new SelectItem(point.getPointId(), point.getPointName())));
                        } catch (Exception ex) {
                            pointList.clear();
                        }
                        try {
                            apSetList.clear();
                            pointVOList.addAll(PointClient.getInstance().getPointsByCustomerSiteAreaAssetPointLocationPoint(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue(), (UUID) selectedFilters.getAsset().getValue(), (UUID) selectedFilters.getPointLocation().getValue(), (UUID) selectedFilters.getPoint().getValue(), false));
                            pointVOList.stream().forEachOrdered(point -> {
                                List<ApAlSetVO> apAlSetsVOList;

                                if ((apAlSetsVOList = APALSetClient.getInstance().getSiteApAlSetsByCustomerSiteIdApSetIdAlSetId(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), point.getSiteId(), point.getApSetId(), point.getAlSetId())) == null || apAlSetsVOList.isEmpty()) {
                                    if ((apAlSetsVOList = APALSetClient.getInstance().getGlobalApAlSetsByCustomerApSetIdAlSetId(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), point.getApSetId(), point.getAlSetId())) == null || apAlSetsVOList.isEmpty()) {
                                        apAlSetsVOList = APALSetClient.getInstance().getGlobalApAlSetsByCustomerApSetIdAlSetId(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), DEFAULT_UPTIME_ACCOUNT, point.getApSetId(), point.getAlSetId());
                                    }
                                }

                                if (apAlSetsVOList != null && !apAlSetsVOList.isEmpty()) {
                                    list.add(new SelectItem(apAlSetsVOList.get(0).getApSetId(), apAlSetsVOList.get(0).getApSetName()));
                                }
                            });

                            list.stream().sorted(Comparator.comparing(SelectItem::getLabel)).forEachOrdered(item -> apSetList.add(item));
                        } catch (Exception ex) {
                            apSetList.clear();
                        }
                        try {
                            alSetParamList.clear();
                            apAlByPointList.addAll(ApALByPointClient.getInstance().getApALByPoints_bySiteAreaAssetPointLocationPointApSet(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue(), (UUID) selectedFilters.getAsset().getValue(), (UUID) selectedFilters.getPointLocation().getValue(), (UUID) selectedFilters.getPoint().getValue(), (UUID) selectedFilters.getApSet().getValue()));
                            apAlByPointList.stream().sorted(Comparator.comparing(PointVO::getPointName)).forEachOrdered(point -> alSetParamList.add(new SelectItem(point.getAlSetId(), point.getParamName())));
                        } catch (Exception ex) {
                            alSetParamList.clear();
                        }

                        if ((assetAnalysisWindowBean = (AssetAnalysisWindowBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("assetAnalysisWindowBean")) == null) {
                            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("assetAnalysisWindowBean", new AssetAnalysisWindowBean());
                            assetAnalysisWindowBean = (AssetAnalysisWindowBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("assetAnalysisWindowBean");
                            assetAnalysisWindowBean.init();
                        }
                        Logger.getLogger(AssetAnalysisBean.class.getName()).log(Level.INFO, "Timer2: {0}", System.currentTimeMillis() - startTime);
                        assetAnalysisWindowBean.loadTrendGraph(selectedFilters, false);
                        Logger.getLogger(AssetAnalysisBean.class.getName()).log(Level.INFO, "Timer3: {0}", System.currentTimeMillis() - startTime);
                        break;
                }

                updateBreadcrumbModel();
                updateUI("content");
            }
            Logger.getLogger(AssetAnalysisBean.class.getName()).log(Level.INFO, "Timer4: {0}", System.currentTimeMillis() - startTime);
        } catch (Exception e) {
            Logger.getLogger(AssetAnalysisBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Sets the given field regionCountrySiteMap
     */
    @Override
    public void setRegionCountrySiteMap() {
        try {
            regionCountrySiteMap.clear();

            if (userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null) {
                SiteClient.getInstance().getSiteRegionCountryByCustomer(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount()).forEach(siteVO -> {
                    if (userManageBean.getCurrentSite().getSiteId().equals(siteVO.getSiteId())) {
                        if (regionCountrySiteMap.containsKey(siteVO.getRegion())) {
                            if (regionCountrySiteMap.get(siteVO.getRegion()).containsKey(siteVO.getCountry())) {
                                if (!regionCountrySiteMap.get(siteVO.getRegion()).get(siteVO.getCountry()).stream().anyMatch(site -> site.getValue().equals(siteVO.getSiteId()))) {
                                    regionCountrySiteMap.get(siteVO.getRegion()).get(siteVO.getCountry()).add(new SelectItem(siteVO.getSiteId(), siteVO.getSiteName()));
                                }
                            } else {
                                regionCountrySiteMap.get(siteVO.getRegion()).put(siteVO.getCountry(), new ArrayList());
                                regionCountrySiteMap.get(siteVO.getRegion()).get(siteVO.getCountry()).add(new SelectItem(siteVO.getSiteId(), siteVO.getSiteName()));
                            }
                        } else {
                            regionCountrySiteMap.put(siteVO.getRegion(), new HashMap());
                            regionCountrySiteMap.get(siteVO.getRegion()).put(siteVO.getCountry(), new ArrayList());
                            regionCountrySiteMap.get(siteVO.getRegion()).get(siteVO.getCountry()).add(new SelectItem(siteVO.getSiteId(), siteVO.getSiteName()));
                        }
                    }
                });
            }
        } catch (Exception ex) {
            regionCountrySiteMap.clear();
        }
    }

    public void assetInfo() {
        List<UUID> points;
        List<PointVO> pointsList = new ArrayList();
        List<AlarmsVO> activeAlaramList = new ArrayList();
        Set<UUID> uniqueAlarms = new HashSet<>();
        pointCount = 0;
        alarmCount = 0;
        workOrderCount = 0;

        if (userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null && userManageBean.getCurrentSite().getSiteId() != null) {

            // Get point count
            try {

                pointsList = PointClient.getInstance().getPointsByCustomerSiteAreaAsset(
                        ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME),
                        userManageBean.getCurrentSite().getCustomerAccount(),
                        userManageBean.getCurrentSite().getSiteId(),
                        (UUID) selectedFilters.getArea().getValue(),
                        (UUID) selectedFilters.getAsset().getValue(),
                        true
                );
                pointCount = pointsList.size();

            } catch (Exception e) {
                Logger.getLogger(HomeBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
                pointCount = 0;
            }

            // Extract pointId and add to points list
            points = pointsList.stream()
                    .map(PointVO::getPointId)
                    .collect(Collectors.toList());

            // Get alarm count
            try {

                activeAlaramList = AlarmsClient.getInstance().getAlarmsByCustomerSiteAreaAsset(ServiceRegistrarClient.getInstance().getServiceHostURL(ALARM_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), userManageBean.getCurrentSite().getSiteId(), (UUID) selectedFilters.getArea().getValue(), (UUID) selectedFilters.getAsset().getValue());

                // Count unique alarms based on pointId in activeAlaramList
                if (!points.isEmpty()) {
                    uniqueAlarms = activeAlaramList.stream()
                            .map(AlarmsVO::getPointId)
                            .filter(points::contains)
                            .collect(Collectors.toSet());
                    alarmCount = uniqueAlarms.size();

                    // Calculate percentage
                    if (pointCount > 0) {
                        activeAlarmsPercentage = BigDecimal.valueOf((double) alarmCount / pointCount * 100)
                                .setScale(2, RoundingMode.HALF_UP)
                                .doubleValue();
                    }

                }

            } catch (Exception e) {
                Logger.getLogger(HomeBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
                alarmCount = 0;
            }

            // Get work order count
            try {
                workOrderCount = WorkOrderClient.getInstance().getOpenWorkOrdersByCustomerSiteAreaAsset(ServiceRegistrarClient.getInstance().getServiceHostURL(WORK_ORDER_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), userManageBean.getCurrentSite().getSiteId(), (UUID) selectedFilters.getArea().getValue(), (UUID) selectedFilters.getAsset().getValue()).size();
            } catch (Exception e) {
                Logger.getLogger(HomeBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
                workOrderCount = 0;

            }

        }

    }

    /**
     * Configure the trend chart options
     *
     * @param heatmapData
     * @param areaName
     * @param assetName
     * @return heatmap chart options
     */
    public String getHeatMapChartJson(HeatmapDataVO heatmapData, String areaName, String assetName) {
        StringBuilder sb;

        sb = new StringBuilder();

        sb.append("{");

        sb.append("chart: { type: 'heatmap', marginTop: 50, marginBottom: 50 }");
        sb.append(",title: { text: 'Asset Summary: Vibration | ").append(areaName).append(" > ").append(assetName).append("' }");
        sb.append(",xAxis: { title: { \"text\": \"Analysis Parameters\", style: { fontWeight: 'bold' } }, categories: ");
        sb.append(wrapWithDoubleQuotes(heatmapData.getParamNames()));
        sb.append("}");

        sb.append(",yAxis: { categories:");
        sb.append(wrapWithDoubleQuotes(heatmapData.getPointNames()));
        sb.append(", title: { \"text\": \"Points\", style: { fontWeight: 'bold' } }, reversed: true }");

        sb.append(",colorAxis: { min: 0, minColor: '#FFFFFF', maxColor: Highcharts.getOptions().colors[3] }");

        sb.append(",legend: { align: 'right', layout: 'vertical', margin: 0, verticalAlign: 'top', y: 50, symbolHeight: 280 }");

        sb.append(",series: [{ borderWidth: 1, data: ");

        sb.append(heatmapData.getDataset());

        sb.append(", customData:");

        sb.append(convertMapToJson(heatmapData.getCustomDataSet()));

        sb.append(", dataLabels: { \"enabled\": true, color: '#000000', formatter: function() { return this.point.value === null ? 'N/A' : this.point.value; } }");

        sb.append(",point: { events: {");

        //   sb.append(" mouseOver: function() { if (this.value == null) { this.setState(''); } },");
        sb.append("click: function() { if (this.value === null) { return; } var xLabel = this.series.xAxis.categories[this.x]; var yLabel = this.series.yAxis.categories[this.y]; var customPointData = this.series.options.customData[this.index]; viewTrendGraph(customPointData, xLabel, yLabel); }");

        sb.append(" } }");

        sb.append(",nullColor: '#e0e0e0' }]");

        //  sb.append(",tooltip: { enabled: false  }");
        sb.append("}");

        return sb.toString();
    }

    public String getHeatMapJson() {
        return heatMapJson;
    }

    public Instant getInstantDateTimestamp(String sampleTimestamp) {
        if (sampleTimestamp != null && !sampleTimestamp.isEmpty()) {
            try {
                return LocalDateTime.parse(sampleTimestamp, DateTimeFormatter.ofPattern(DATE_TIME_OFFSET_FORMAT)).atZone(ZoneOffset.UTC).toInstant();
            } catch (Exception e) {
                Logger.getLogger(AlarmsVO.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            }
        }
        return null;
    }
    
    public HeatmapDataVO extractHeatmapData(String jsonString) {

        JsonObject jsonObject = new Gson().fromJson(jsonString, JsonObject.class);

        Map<String, Map<String, Double>> pointParamValues = new HashMap<>();
        Set<String> paramNamesSet = new HashSet<>();
        List<String> pointNames = new ArrayList<>();

        // Custom Data List
        List<Map<String, Object>> customDataSet = new ArrayList<>();

        // Iterate through pointLocations
        JsonArray pointLocations = jsonObject.getAsJsonArray("pointLocations");
        for (int i = 0; i < pointLocations.size(); i++) {
            JsonObject pointLocation = pointLocations.get(i).getAsJsonObject();
            JsonArray points = pointLocation.getAsJsonArray("points");

            // Iterate through points
            for (int j = 0; j < points.size(); j++) {
                JsonObject point = points.get(j).getAsJsonObject();
                String pointName = point.get("pointName").getAsString();
                String lastSampled = point.get("lastSampled").getAsString();
                lastSampled = lastSampled.replace("Z", "+0000");
                
                if (lastSampled != null && !lastSampled.isEmpty() && (userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean")) != null) {
                    lastSampled = userManageBean.getTzOffsetString(getInstantDateTimestamp(lastSampled), 1);
                }
                
                // Format pointName with timestamp
                String formattedPointName = pointName + "<br>" + lastSampled;
                pointNames.add(formattedPointName);

                // Map for this point's params
                Map<String, Double> paramValues = new HashMap<>();

                JsonArray params = point.getAsJsonArray("params");
                for (int k = 0; k < params.size(); k++) {
                    JsonObject param = params.get(k).getAsJsonObject();
                    String paramName = param.get("paramName").getAsString();
                    paramNamesSet.add(paramName);
                    Double value = param.has("value") ? param.get("value").getAsDouble() : null; // Handle missing values
                    paramValues.put(paramName, value);

                    // Custom data object creation
                    Map<String, Object> customDataMap = new HashMap<>();
                    customDataMap.put("value", value);
                    customDataMap.put("pointLocationId", pointLocation.get("pointLocationId").getAsString());
                    customDataMap.put("pointId", point.get("pointId").getAsString());
                    customDataMap.put("apSetId", point.get("apSetId").getAsString());

                    customDataSet.add(customDataMap);
                }
                pointParamValues.put(formattedPointName, paramValues);
            }
        }
        // Convert the set of paramNames to a list and sort to maintain order
        List<String> paramNames = new ArrayList<>(paramNamesSet);
        //Collections.sort(paramNames);

        // Generate the dataset in the [column, row, value] format
        List<List<Object>> dataset = new ArrayList<>();
        int count = 0;
        for (int row = 0; row < pointNames.size(); row++) {
            String pointName = pointNames.get(row);
            Map<String, Double> paramValues = pointParamValues.get(pointName);

            for (int col = 0; col < paramNames.size(); col++) {
                String paramName = paramNames.get(col);
                Double value = paramValues.getOrDefault(paramName, null);  // Default to null if value is missing
                List<Object> entry = Arrays.asList(col, row, value);
                dataset.add(entry);
                if (value == null) {
                    customDataSet.add(count, null);
                }
                count++;
            }
        }
        return new HeatmapDataVO(paramNames, pointNames, dataset, customDataSet);
    }

    public static List<String> wrapWithDoubleQuotes(List<String> list) {
        List<String> quotedList = new ArrayList<>();
        for (String item : list) {
            quotedList.add("\"" + item + "\"");
        }
        return quotedList;
    }

    public static String convertMapToJson(List<Map<String, Object>> mapList) {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(mapList);
    }

    public void viewTrendGraph() {

        Filters filters;
        AssetAnalysisWindowBean assetAnalysisWindowBean;
        //  Instant timeStamp;
        try {
            //  timeStamp = avo.getInstantDateTimestamp();
            if ((assetAnalysisWindowBean = (AssetAnalysisWindowBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("assetAnalysisWindowBean")) == null) {
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("assetAnalysisWindowBean", new AssetAnalysisWindowBean());
                assetAnalysisWindowBean = (AssetAnalysisWindowBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("assetAnalysisWindowBean");
                assetAnalysisWindowBean.init();
            }
            filters = new Filters();
            filters.setRegion(selectedFilters.getRegion());
            filters.setCountry(selectedFilters.getCountry());
            filters.setSite(selectedFilters.getSite());
            filters.setArea(selectedFilters.getArea());
            filters.setAsset(selectedFilters.getAsset());

            String pointName = yLabel.split("<br>")[0];
            String timeStamp = yLabel.split("<br>")[1];

            CustomHeatMapData customData = null;
            if (customDataJson != null && !customDataJson.isEmpty()) {
                customData = new Gson().fromJson(customDataJson, CustomHeatMapData.class);
            }
            if (customData != null) {
                filters.setPointLocation(new SelectItem(UUID.fromString(customData.getPointLocationId()), ""));
                filters.setPoint(new SelectItem(UUID.fromString(customData.getPointId()), ""));
                filters.setApSet(new SelectItem(UUID.fromString(customData.getApSetId()), ""));

                filters.setAlSetParam(new SelectItem(null, xLabel));
                //  assetAnalysisWindowBean.setTimestamp(String.valueOf(timeStamp.toEpochMilli()));
                assetAnalysisWindowBean.loadTrendGraph(filters, false);
            }
        } catch (Exception ex) {
            Logger.getLogger(AlarmsBean.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

    public int getPointCount() {
        return pointCount;
    }

    public int getAlarmCount() {
        return alarmCount;
    }

    public int getWorkOrderCount() {
        return workOrderCount;
    }

    public boolean isAssertInfo() {
        return assertInfo;
    }

    public double getActiveAlarmsPercentage() {
        return activeAlarmsPercentage;
    }

    public String getAssetType() {
        return assetType;
    }

    public void setAssetType(String assetType) {
        this.assetType = assetType;
    }

    public String getxLabel() {
        return xLabel;
    }

    public void setxLabel(String xLabel) {
        this.xLabel = xLabel;
    }

    public String getyLabel() {
        return yLabel;
    }

    public void setyLabel(String yLabel) {
        this.yLabel = yLabel;
    }

    public String getCustomDataJson() {
        return customDataJson;
    }

    public void setCustomDataJson(String customDataJson) {
        this.customDataJson = customDataJson;
    }

}
