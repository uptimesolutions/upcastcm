/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.FaultFrequenciesClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import static com.uptime.upcastcm.utils.enums.ParamUnitEnum.getFrequencyPlusUnits;
import static com.uptime.upcastcm.utils.helperclass.GenericMessage.setNonAutoGrowlMsg;
import com.uptime.upcastcm.utils.vo.FaultFrequenciesVO;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import org.primefaces.PrimeFaces;

/**
 *
 * @author Joseph
 */
public class CreateFaultFrequenciesBean implements Serializable {

    private List<SelectItem> frequencyUnitsList;
    private List<FaultFrequenciesVO> frequenciesVOList;
    private String setName, description, siteOrGlobal, ffUnits;
    private UserManageBean userManageBean;
    private boolean addToPreferred = true, enabled = false;

    /**
     * Constructor
     */
    public CreateFaultFrequenciesBean() {
        try {
            userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
            frequencyUnitsList = getFrequencyPlusUnits();
        } catch (Exception e) {
            Logger.getLogger(CreateFaultFrequenciesBean.class.getName()).log(Level.SEVERE, "CreateFaultFrequenciesBean Constructor Exception {0}", e.toString());
            userManageBean = null;
        }
    }

    /**
     * Post Constructor
     */
    @PostConstruct
    public void init() {
        resetPage();
    }

    /**
     * Reset Overlay
     */
    public void resetPage() {
        setName = null;
        description = null;
        siteOrGlobal = null;
        ffUnits = null;
        addToPreferred = true;
        frequenciesVOList = new ArrayList();
        frequenciesVOList.add(new FaultFrequenciesVO());
        enabled = false;
    }

    public void updateBean() {
        enabled = true;
        Logger.getLogger(CreateFaultFrequenciesBean.class.getName()).log(Level.INFO, "CreateFaultFrequenciesBean.updateBean() called. ***********");

        if (setName == null || setName.trim().isEmpty()
                || siteOrGlobal == null || siteOrGlobal.trim().isEmpty()
                || ffUnits == null || ffUnits.trim().isEmpty()) {
            enabled = false;
            return;
        }
        if (frequenciesVOList.isEmpty()) {
            enabled = false;
        } else {
            for (FaultFrequenciesVO faultFrequenciesVO : frequenciesVOList) {
                if (faultFrequenciesVO.getFfName() == null || faultFrequenciesVO.getFfName().trim().isEmpty()
                        || faultFrequenciesVO.getFfValue() <= 0.0f) {
                    enabled = false;
                }
            }
        }
    }

    /**
     * Submit new items to Cassandra
     */
    public void submit() {
        UUID setId;

        if (userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null) {

            // Validating needed fields
            for (FaultFrequenciesVO vo : frequenciesVOList) {
                if (setName == null || setName.trim().isEmpty() || siteOrGlobal == null
                        || ffUnits == null || ffUnits.trim().isEmpty()
                        || vo.getFfName() == null || vo.getFfName().trim().isEmpty()) {
                    setNonAutoGrowlMsg("growl_validate_message_1");
                    return;
                }
            }

            setId = UUID.randomUUID();
            if (siteOrGlobal.equalsIgnoreCase("site")) {
                frequenciesVOList.stream().forEachOrdered(vo -> {
                    vo.setFfSetId(setId);
                    vo.setFfId(UUID.randomUUID());
                    vo.setCustomerAccount(userManageBean.getCurrentSite().getCustomerAccount());
                    vo.setSiteName(userManageBean.getCurrentSite().getSiteName());
                    vo.setSiteId(userManageBean.getCurrentSite().getSiteId());
                    vo.setFfSetName(setName);
                    vo.setFfSetDesc(description);
                    vo.setFfUnit(ffUnits);
                    vo.setGlobal(false);
                });
                if (FaultFrequenciesClient.getInstance().createSiteFaultFrequencies(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), frequenciesVOList)) {
                    if (addToPreferred) {
                        FaultFrequenciesClient.getInstance().updateSiteFFSetFavoritesVOList(frequenciesVOList, new ArrayList());
                    }
                    if (!addToPreferred) {
                        setNonAutoGrowlMsg("growl_create_item_items");
                    }
                    Logger.getLogger(CreateFaultFrequenciesBean.class.getName()).log(Level.INFO, "Site Fault Frequencies created successfully.");
                } else {
                    Logger.getLogger(CreateFaultFrequenciesBean.class.getName()).log(Level.INFO, "Error in creating Site Fault Frequencies.");
                }
            } else {
                frequenciesVOList.stream().forEachOrdered(vo -> {
                    vo.setFfSetId(setId);
                    vo.setFfId(UUID.randomUUID());
                    vo.setCustomerAccount(userManageBean.getCurrentSite().getCustomerAccount());
                    vo.setSiteName(null);
                    vo.setSiteId(null);
                    vo.setFfSetName(setName);
                    vo.setFfSetDesc(description);
                    vo.setFfUnit(ffUnits);
                });
                if (FaultFrequenciesClient.getInstance().createGlobalFaultFrequencies(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), frequenciesVOList)) {
                    if (addToPreferred) {
                        frequenciesVOList.stream().forEachOrdered(vo -> {
                            vo.setSiteId(userManageBean.getCurrentSite().getSiteId());
                            vo.setGlobal(true);
                        });
                        FaultFrequenciesClient.getInstance().updateSiteFFSetFavoritesVOList(frequenciesVOList, new ArrayList());
                    }
                    if (!addToPreferred) {
                        setNonAutoGrowlMsg("growl_create_item_items");
                    }
                    Logger.getLogger(CreateFaultFrequenciesBean.class.getName()).log(Level.INFO, "Global Fault Frequencies created successfully.");
                    userManageBean.getPresetUtil().clearGlobalFaultFrequenciesList();
                } else {
                    Logger.getLogger(CreateFaultFrequenciesBean.class.getName()).log(Level.INFO, "Error in creating Global Fault Frequencies.");
                }
            }

            ((PresetsBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("presetsBean")).resetPage();
            ((CreateCustomFaultFrequecySetsBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("createCustomFaultFrequecySetsBean")).resetPage();
            PrimeFaces.current().ajax().update("customFaultFrequencieySetsFormId");
            PrimeFaces.current().executeScript("PF('defaultSecondDlg').hide()");
            enabled = false;
        }
    }

    /**
     * Add new FaultFrequenciesVO Object to the frequenciesVOList field
     */
    public void addFaultFrequencies() {
        enabled = false;
        frequenciesVOList.add(new FaultFrequenciesVO());
    }

    /**
     * Remove the given FaultFrequenciesVO Object from the frequenciesVOList field
     *
     * @param faultFrequenciesVO, FaultFrequenciesVO Object
     */
    public void deleteFaultFrequencies(FaultFrequenciesVO faultFrequenciesVO) {
        if (frequenciesVOList.size() > 1) {
            frequenciesVOList.remove(faultFrequenciesVO);
        }
    }

    public List<SelectItem> getFrequencyUnitsList() {
        return frequencyUnitsList;
    }

    public void setFrequenciesVOList(List<FaultFrequenciesVO> frequenciesVOList) {
        this.frequenciesVOList = frequenciesVOList;
    }

    public List<FaultFrequenciesVO> getFrequenciesVOList() {
        return frequenciesVOList;
    }

    public String getSetName() {
        return setName;
    }

    public void setSetName(String setName) {
        this.setName = setName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSiteOrGlobal() {
        return siteOrGlobal;
    }

    public void setSiteOrGlobal(String siteOrGlobal) {
        this.siteOrGlobal = siteOrGlobal;
    }

    public String getFfUnits() {
        return ffUnits;
    }

    public void setFfUnits(String ffUnits) {
        this.ffUnits = ffUnits;
    }

    public boolean isAddToPreferred() {
        return addToPreferred;
    }

    public void setAddToPreferred(boolean addToPreferred) {
        this.addToPreferred = addToPreferred;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

}
