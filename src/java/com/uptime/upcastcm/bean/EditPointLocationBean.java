/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.APALSetClient;
import com.uptime.upcastcm.http.client.FaultFrequenciesClient;
import com.uptime.upcastcm.http.client.PointClient;
import com.uptime.upcastcm.http.client.PointLocationClient;
import com.uptime.upcastcm.http.client.TachometerClient;
import com.uptime.upcastcm.http.utils.json.dom.PointLocationDomJsonParser;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import static com.uptime.upcastcm.utils.enums.RollDiameterUnitEnum.getRollDiameterUnitItemList;
import com.uptime.upcastcm.utils.helperclass.Filters;
import com.uptime.upcastcm.utils.helperclass.Utils;
import com.uptime.upcastcm.utils.interfaces.PresetDialogFields;
import com.uptime.upcastcm.utils.vo.ApAlSetVO;
import com.uptime.upcastcm.utils.vo.FaultFrequenciesVO;
import com.uptime.upcastcm.utils.vo.PointLocationVO;
import com.uptime.upcastcm.utils.vo.PointVO;
import com.uptime.upcastcm.utils.vo.TachometerVO;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import org.primefaces.PrimeFaces;

/**
 *
 * @author Mohd Juned Alam
 */
public class EditPointLocationBean implements Serializable, PresetDialogFields {

    private UserManageBean userManageBean;
    private PointLocationVO pointLocationVO,pointLocation;
    private List<String> siteNames, areaNameList, assetNameList;
    private List<SelectItem> tachometersList, faultFrequenciesList, ptLoNamePresetList;
    private List<SelectItem> rollDiameterUnitsList;

    private boolean enableEditPointSaveBtn;
    private List<FaultFrequenciesVO> siteFFSetFavorites;
    private List<PointVO> pointVOList;
    private boolean tachValidationFailed;
    private List<ApAlSetVO> sitePointApAlSetsVOList;
    private UUID tachId;
    private Filters presets;

    /**
     * Constructor
     */
    public EditPointLocationBean() {
        siteNames = new ArrayList();
        areaNameList = new ArrayList();
        pointVOList = new ArrayList();
        enableEditPointSaveBtn = false;
        tachValidationFailed=false;
        sitePointApAlSetsVOList = new ArrayList();

        try {
            userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
            rollDiameterUnitsList = getRollDiameterUnitItemList();
        } catch (Exception e) {
            Logger.getLogger(EditPointLocationBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            userManageBean = null;
        }
    }

    /**
     * Post Constructor
     */
    @PostConstruct
    public void init() {
        resetPage();
    }

    @Override
    public void resetPage() {
        presets = null;
        if (pointLocationVO == null) {
            pointLocationVO = new PointLocationVO();
        }
        if (userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null) {
            pointLocationVO.setCustomerAccount(userManageBean.getCurrentSite().getCustomerAccount());
        }
    }

    @Override
    public void presetFields(Filters presets, Object object) {
        List<TachometerVO> tachList;
        Set<String> ffsetnames;

        assetNameList = new ArrayList();
        tachometersList = new ArrayList();
        ptLoNamePresetList = new ArrayList();
        faultFrequenciesList = new ArrayList();
        pointVOList = new ArrayList();
        tachValidationFailed = false;
        siteNames.clear();
        areaNameList.clear();
        sitePointApAlSetsVOList.clear();

        pointLocationVO = object != null && object instanceof PointLocationVO ? new PointLocationVO((PointLocationVO) object) : new PointLocationVO();
        if (presets != null) {
            if (presets.getSite() != null) {
                pointLocationVO.setSiteName(presets.getSite().getLabel());
                pointLocationVO.setSiteId((UUID) presets.getSite().getValue());
                siteNames.add(presets.getSite().getLabel());
            }
            if (presets.getAsset() != null) {
                pointLocationVO.setAssetName(presets.getAsset().getLabel());
                pointLocationVO.setAssetId((UUID) presets.getAsset().getValue());
                areaNameList.add(presets.getArea().getLabel());
                assetNameList.add(pointLocationVO.getAssetName());
            }
            if (presets.getArea() != null) {
                pointLocationVO.setAreaName(presets.getArea().getLabel());
                pointLocationVO.setAreaId((UUID) presets.getArea().getValue());
                areaNameList.add(presets.getArea().getLabel());
            }

            this.presets = presets;
        }

        try {
            // get tachometer presets
            userManageBean.getPresetUtil().clearSiteTachList();
            tachometersList.addAll(userManageBean.getPresetUtil().populateTachometerList(userManageBean, userManageBean.getCurrentSite().getCustomerAccount(), pointLocationVO.getSiteId()));

            // get point locatin name presets
            userManageBean.getPresetUtil().clearSitePointLocationNameList();
            ptLoNamePresetList.addAll(userManageBean.getPresetUtil().populatePtLoNamesList(userManageBean, userManageBean.getCurrentSite().getCustomerAccount(), pointLocationVO.getSiteId()));

            faultFrequenciesList = populateFFSetList();

            if (pointLocationVO != null) {
                tachId = pointLocationVO.getTachId();   
                enableEditPointSaveBtn = 0 != pointLocationVO.getSampleInterval();
                
                ffsetnames = new HashSet();
                if (pointLocationVO.getFfSetIds() != null) {
                    ffsetnames = PointLocationDomJsonParser.getInstance().populateFFSetFromJson(pointLocationVO.getFfSetIds(), siteFFSetFavorites);
                }
                pointLocationVO.setFfSetName(ffsetnames);
            }

            // set tachReferenceUnits if tachId != null
            if (pointLocationVO.getTachId() != null) {
                if ((tachList = TachometerClient.getInstance().getGlobalTachometersByPK(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), DEFAULT_UPTIME_ACCOUNT, pointLocationVO.getTachId())) == null || tachList.isEmpty()) {
                    if ((tachList = TachometerClient.getInstance().getGlobalTachometersByPK(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), pointLocationVO.getCustomerAccount(), pointLocationVO.getTachId())) == null || tachList.isEmpty()) {
                        tachList = TachometerClient.getInstance().getSiteTachometersByPK(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), pointLocationVO.getCustomerAccount(), pointLocationVO.getSiteId(), pointLocationVO.getTachId());
                    }
                }
                pointLocationVO.setTachReferenceUnits((tachList != null && !tachList.isEmpty() && tachList.get(0).getTachReferenceUnits() != null) ? tachList.get(0).getTachReferenceUnits() : null);
            }
        } catch (Exception e) {
            Logger.getLogger(EditPointLocationBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }

    }

    public List<SelectItem> populateFFSetList() {
        List<SelectItem> ffSetList = new ArrayList();
        siteFFSetFavorites = new ArrayList();
        try {
            siteFFSetFavorites = FaultFrequenciesClient.getInstance().getSiteFFSetFavoritesVOList(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), userManageBean.getCurrentSite().getSiteId());
            ffSetList = userManageBean.getPresetUtil().populateFFSetNameList(userManageBean,siteFFSetFavorites);
        } catch (Exception e) {
            Logger.getLogger(CreatePointLocationBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }

        return ffSetList;
    }

    public void updateBean() {
        Logger.getLogger(EditPointLocationBean.class.getName()).log(Level.INFO, "pointLocationVO.getSiteName()*********** {0}", new Object[]{pointLocationVO.getSiteName()});
    }

    public void onTachChange() {
        List<TachometerVO> tachList;
        tachValidationFailed = false; 
           pointVOList = PointClient.getInstance().getPointsByCustomerSiteAreaAssetPointLocation(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), pointLocationVO.getCustomerAccount(), pointLocationVO.getSiteId(), pointLocationVO.getAreaId(), pointLocationVO.getAssetId(), pointLocationVO.getPointLocationId(), true);
          if (!pointVOList.isEmpty() && pointVOList != null) {

            if (userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null) {
                for (PointVO pointVo : pointVOList) {
                    PointClient.getInstance().getPointsByCustomerSiteAreaAssetPointLocationPointWithApsets(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), pointVo.getSiteId(), pointVo.getAreaId(), pointVo.getAssetId(), pointVo.getPointLocationId(), pointVo.getPointId()).forEach(point -> {
                        sitePointApAlSetsVOList.addAll(point.getApAlSetVOs());
                    });
                }
                if (!sitePointApAlSetsVOList.isEmpty()) {
                    sitePointApAlSetsVOList.stream().forEach(al -> {
                        if ((al.getFrequencyUnits() != null && al.getFrequencyUnits().equalsIgnoreCase("Orders")) || Utils.validateParamList().contains(al.getParamType())) {
                            if (pointLocationVO == null) {
                                pointLocationVO = PointLocationClient.getInstance().getPointLocationsByPK(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), pointLocationVO.getSiteId(), pointLocationVO.getAreaId(), pointLocationVO.getAssetId(), pointLocationVO.getPointLocationId()).get(0);
                            }
                            if (pointLocationVO.getTachId() == null) {
                                tachValidationFailed = true;
                            }
                        }
                    });
                }
            }
        } 

        if (pointLocationVO.getTachId() != null) {
            if ((tachList = TachometerClient.getInstance().getGlobalTachometersByPK(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), DEFAULT_UPTIME_ACCOUNT, pointLocationVO.getTachId())) == null || tachList.isEmpty()) {
                if ((tachList = TachometerClient.getInstance().getGlobalTachometersByPK(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), pointLocationVO.getCustomerAccount(), pointLocationVO.getTachId())) == null || tachList.isEmpty()) {
                    tachList = TachometerClient.getInstance().getSiteTachometersByPK(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), pointLocationVO.getCustomerAccount(), pointLocationVO.getSiteId(), pointLocationVO.getTachId());
                }
            }
            pointLocationVO.setTachReferenceUnits((tachList != null && !tachList.isEmpty() && tachList.get(0).getTachReferenceUnits() != null) ? tachList.get(0).getTachReferenceUnits() : null);
        }
        if (Math.signum(pointLocationVO.getSpeedRatio()) == 0) {
            pointLocationVO.setSpeedRatio(1);
        }
        pointLocationVO.setRollDiameter(0);
        pointLocationVO.setRollDiameterUnits(null);
    }

    public void updatePointLocation() {
        try {
            pointLocationVO.setPointList(null);
            pointLocationVO.setFfSetIds(null);
            if (!pointLocationVO.getFfSetName().isEmpty()) {
                pointLocationVO.setFfSetIds(PointLocationDomJsonParser.getInstance().getJsonFromSelectedPointLocation(pointLocationVO.getFfSetName(), siteFFSetFavorites));
            }

            if (PointLocationClient.getInstance().updatePointLocation(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), pointLocationVO)) {
                pointLocationVO = new PointLocationVO();
                
                if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("configurationBean") == null) {
                    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("configurationBean", new ConfigurationBean());
                    ((ConfigurationBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("configurationBean")).init();
                }
            
                if (presets != null) {
                    if (presets.getPointLocation() != null) {
                        ((ConfigurationBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("configurationBean")).onPointLocationChange(true);
                    } else {
                        ((ConfigurationBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("configurationBean")).onAssetChange(true);
                    }
                }
                
                PrimeFaces.current().executeScript("updateTreeNodes()");
            }
        } catch (Exception e) {
            Logger.getLogger(EditPointLocationBean.class.getName()).log(Level.SEVERE, "Exception while updating point location", e);
        }
    }

    public void enableEditPointSaveBtn() {
        enableEditPointSaveBtn = true;
    }

    @Override
    public void presetFields(Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void presetFields(int operationType, Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public PointLocationVO getPointLocationVO() {
        return pointLocationVO;
    }

    public void setPointLocationVO(PointLocationVO pointLocationVO) {
        this.pointLocationVO = pointLocationVO;
    }

    public List<String> getSiteNames() {
        return siteNames;
    }

    public void setSiteNames(List<String> siteNames) {
        this.siteNames = siteNames;
    }

    public List<String> getAssetNameList() {
        return assetNameList;
    }

    public List<SelectItem> getFaultFrequenciesList() {
        return faultFrequenciesList;
    }

    public List<SelectItem> getTachometersList() {
        return tachometersList;
    }

    public List<SelectItem> getPtLoNamePresetList() {
        return ptLoNamePresetList;
    }

    public List<String> getAreaNameList() {
        return areaNameList;
    }

    public void setAreaNameList(List<String> areaNameList) {
        this.areaNameList = areaNameList;
    }

    public List<SelectItem> getRollDiameterUnitsList() {
        return rollDiameterUnitsList;
    }

    public boolean isEnableEditPointSaveBtn() {
        return enableEditPointSaveBtn;
    }

    public void setEnableEditPointSaveBtn(boolean enableEditPointSaveBtn) {
        this.enableEditPointSaveBtn = enableEditPointSaveBtn;
    }

    public List<FaultFrequenciesVO> getSiteFFSetFavorites() {
        return siteFFSetFavorites;
    }

    public List<PointVO> getPointVOList() {
        return pointVOList;
    }

    public void setPointVOList(List<PointVO> pointVOList) {
        this.pointVOList = pointVOList;
    }

    public boolean isTachValidationFailed() {
        return tachValidationFailed;
    }

    public void setTachValidationFailed(boolean tachValidationFailed) {
        this.tachValidationFailed = tachValidationFailed;
    }

    public PointLocationVO getPointLocation() {
        return pointLocation;
    }

    public void setPointLocation(PointLocationVO pointLocation) {
        this.pointLocation = pointLocation;
    }

    public List<ApAlSetVO> getSitePointApAlSetsVOList() {
        return sitePointApAlSetsVOList;
    }

    public void setSitePointApAlSetsVOList(List<ApAlSetVO> sitePointApAlSetsVOList) {
        this.sitePointApAlSetsVOList = sitePointApAlSetsVOList;
    }

    public UUID getTachId() {
        return tachId;
    }

    public void setTachId(UUID tachId) {
        this.tachId = tachId;
    }
    
    
    
    

}
