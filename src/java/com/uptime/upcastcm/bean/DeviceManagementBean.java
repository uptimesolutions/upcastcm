/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.featuresflag.enums.UptimeFeaturesEnum;
import com.uptime.upcastcm.http.client.APALSetClient;
import com.uptime.upcastcm.http.client.ApALByPointClient;
import com.uptime.upcastcm.http.client.CustomerClient;
import com.uptime.upcastcm.http.client.HwUnitToPointClient;
import com.uptime.upcastcm.http.client.ManageDeviceClient;
import com.uptime.upcastcm.http.client.PointLocationClient;
import com.uptime.upcastcm.http.client.SiteClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import com.uptime.upcastcm.utils.enums.TS1ACChannelNumberEnum;
import com.uptime.upcastcm.utils.enums.TS1DCChannelNumberEnum;
import com.uptime.upcastcm.utils.vo.ApAlSetVO;
import com.uptime.upcastcm.utils.vo.ChannelVO;
import com.uptime.upcastcm.utils.vo.CustomersVO;
import com.uptime.upcastcm.utils.vo.DeviceVO;
import com.uptime.upcastcm.utils.vo.HwUnitVO;
import com.uptime.upcastcm.utils.vo.PointVO;
import com.uptime.upcastcm.utils.vo.SiteVO;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.primefaces.PrimeFaces;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.FilterMeta;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author twilcox
 */
public class DeviceManagementBean implements Serializable {

    private final List<ChannelVO> channelVOList;
    private UserManageBean userManageBean;
    private NavigationBean navigationBean;
    private String serialNumber, customerName, region, country, siteName;
    private Boolean serialNumberNotInUse;
    private final List<DeviceVO> deviceVOList;
    private List<DeviceVO> filteredDevices;
    private List<FilterMeta> filterBy;

    /**
     * Creates a new instance of DeviceManagementBean
     */
    public DeviceManagementBean() {
        channelVOList = new ArrayList();
        deviceVOList = new ArrayList();

        try {
            navigationBean = (NavigationBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("navigationBean");
            userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
        } catch (Exception e) {
            Logger.getLogger(DeviceManagementBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            navigationBean = null;
            userManageBean = null;
        }
    }

    /**
     * Post Constructor
     */
    @PostConstruct
    public void init() {
        resetPage();

    }

    /**
     * Reset full page
     */
    public void resetPage() {
        channelVOList.clear();
        deviceVOList.clear();
        serialNumber = null;
        serialNumberNotInUse = null;
        customerName = null;
        region = null;
        country = null;
        siteName = null;
        try {
            PrimeFaces.current().ajax().update("deviceManagementTopControlFormId");
            PrimeFaces.current().ajax().update("deviceManagementResultFormId");
        } catch (Exception e) {
            Logger.getLogger(DeviceManagementBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * On serial number change, reset the page except for the serial number
     */
    public void onSerialNumberChange() {
        channelVOList.clear();
        deviceVOList.clear();
        serialNumberNotInUse = null;
        customerName = null;
        region = null;
        country = null;
        siteName = null;
    }

    /**
     * Search for the given Serial Number
     */
    public void search() {
        List<HwUnitVO> hwUnitVOList;
        Pattern pattern;
        CustomersVO customersVO;
        List<SiteVO> siteVOList;

        try {
            channelVOList.clear();
            deviceVOList.clear();
            serialNumberNotInUse = null;
            customerName = null;
            region = null;
            country = null;
            siteName = null;

            if (userManageBean != null && userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null && userManageBean.getCurrentSite().getSiteId() != null && serialNumber != null && !serialNumber.isEmpty()) {
                serialNumber = serialNumber.toUpperCase();
                if (UptimeFeaturesEnum.UptimeFeatures.DISPLAY_STORMX.isActive()) {
                    pattern = Pattern.compile(REGEX_INCLUDE_STORM_SERIAL_NUMBER);
                } else {
                    pattern = Pattern.compile(REGEX_EXCLUDE_STORM_SERIAL_NUMBER);
                }
                if (pattern.matcher(serialNumber).matches()) {
                    channelVOList.addAll(ManageDeviceClient.getInstance().getDeviceManagementRowsByCustomerSiteIdDevice(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), userManageBean.getCurrentSite().getSiteId(), serialNumber, false));
                      channelVOList.stream().forEach(a -> {
                            if (a.isAlarmEnabled()) {
                                a.setAlarmEnabledValue(userManageBean.getResourceBundleString("label_true"));
                            } else {
                                a.setAlarmEnabledValue(userManageBean.getResourceBundleString("label_false"));
                            }
                        });
                    if (channelVOList.isEmpty()) {
                        if (!(serialNumberNotInUse = !((hwUnitVOList = HwUnitToPointClient.getInstance().getHwUnitToPointByDevice(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), serialNumber)) != null && !hwUnitVOList.isEmpty())) && userManageBean.isGlobalGroups()) {
                            // Get customerName, siteName, country, and region, if device in use by another location
                            try {
                                if ((customersVO = CustomerClient.getInstance().getCustomersByPK(ServiceRegistrarClient.getInstance().getServiceHostURL(USER_PREFERENCE_SERVICE_NAME), hwUnitVOList.get(0).getCustomerAccount())) != null) {
                                    customerName = customersVO.getCustomerName();
                                    if ((siteVOList = SiteClient.getInstance().getSiteByCustomerSite(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), hwUnitVOList.get(0).getCustomerAccount(), hwUnitVOList.get(0).getSiteId().toString())) != null && !siteVOList.isEmpty()) {
                                        siteName = siteVOList.get(0).getSiteName();
                                        country = siteVOList.get(0).getCountry();
                                        region = siteVOList.get(0).getRegion();
                                    }
                                }
                            } catch (Exception e) {
                                Logger.getLogger(DeviceManagementBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
                                customerName = null;
                                region = null;
                                country = null;
                                siteName = null;
                            }
                        }
                    }
                } else {
                    FacesContext.getCurrentInstance().addMessage("growlMessages", new FacesMessage(userManageBean.getResourceBundleString("growl_valid_device_id")));
                }
            }
        } catch (Exception e) {
            Logger.getLogger(DeviceManagementBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            channelVOList.clear();
            serialNumberNotInUse = null;
        }
    }

    /**
     * Search for all devices for the given site
     */
    public void searchForDeviceLocations() {
        DataTable datatable;

        try {
            try {
                if ((datatable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("deviceManagementResultFormId:deviceLocationTableId")) != null) {
                    datatable.reset();
                }
            } catch (Exception e) {
                Logger.getLogger(DeviceManagementBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            }

            channelVOList.clear();
            deviceVOList.clear();
            serialNumberNotInUse = null;
            customerName = null;
            region = null;
            country = null;
            siteName = null;

            if (userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null) {
                deviceVOList.addAll(ManageDeviceClient.getInstance().getDeviceLocationsByCustomerSiteName(userManageBean.getCurrentSite().getCustomerAccount(), userManageBean.getCurrentSite().getSiteName()));

                deviceVOList.stream().forEach(a -> {
                    if ("null".equalsIgnoreCase(a.getNewDeviceId()) || a.getNewDeviceId() == null || a.getNewDeviceId().trim().isEmpty()) {
                        a.setNewDeviceId(userManageBean.getResourceBundleString("label_no_device_assigned"));
                    }
                });

                for (int i = 0; i < deviceVOList.size(); i++) {
                    deviceVOList.get(i).setIndex(i);
                }
            }
        } catch (Exception e) {
            Logger.getLogger(DeviceManagementBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }
  

    /**
     * download DeviceLocations as CSV
     *
     * @return StreamedContent Object
     */
    public StreamedContent downloadDeviceLocationCSV() {
        String[] header = {userManageBean.getResourceBundleString("label_serial_number"), userManageBean.getResourceBundleString("label_area"), userManageBean.getResourceBundleString("label_asset"), userManageBean.getResourceBundleString("label_point_location")};
        StringBuilder csvData;
        InputStream stream;

        try {
            csvData = new StringBuilder();
            // Header row
            // Convert header to CSV format using StringBuilder
            for (int i = 0; i < header.length; i++) {
                csvData.append(header[i]);
                if (i != header.length - 1) {
                    csvData.append(",");
                }
            }
            csvData.append("\n");
            // Convert the deviceVOList to a CSV format using StringBuilder.
            if (deviceVOList != null && !deviceVOList.isEmpty()) {
            deviceVOList.stream()
                .filter(device -> device.getNewDeviceId() != null && (device.getNewDeviceId().startsWith("00") || device.getNewDeviceId().startsWith("BB") || device.getNewDeviceId().startsWith("BA")))
                .forEachOrdered(device -> {
                    csvData
                        .append(device.getNewDeviceId()).append(",")
                        .append(device.getAreaName() != null ? device.getAreaName() : "").append(",")
                        .append(device.getAssetName() != null ? device.getAssetName() : "").append(",")
                        .append(device.getPointLocationName() != null ? device.getPointLocationName() : "").append("\n");
                });
        }
            
            // Convert CSV content to InputStream
            stream = new ByteArrayInputStream(csvData.toString().getBytes(StandardCharsets.UTF_8));
            // Create and return StreamedContent
            return DefaultStreamedContent.builder().name(userManageBean.getResourceBundleString("device_location_download_file_name")).contentType("text/csv").stream(() -> stream).build();
        } catch (Exception e) {
            Logger.getLogger(DeviceManagementBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            return null;
        }
    }

    /**
     * Set and display the Info Overlay
     *
     * @param channelVO, ChannelVO Object
     */
    public void viewInfo(ChannelVO channelVO) {
        List<ApAlSetVO> apAlSetsVOList;
        List<PointVO> pointVOlist;
        if (channelVO != null) {
            try {
                // Set Sample Interval
                if (channelVO.getSampleInterval() == 0) {
                    channelVO.setSampleInterval(PointLocationClient.getInstance().getPointLocationsByPK(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), channelVO.getCustomerAccount(), channelVO.getSiteId(), channelVO.getAreaId(), channelVO.getAssetId(), channelVO.getPointLocationId()).get(0).getSampleInterval());
                }
                // Set ApAlSetVO List && AlSetId
                if (channelVO.getApAlSetVOs() == null || channelVO.getApAlSetVOs().isEmpty()) {
                    channelVO.setApAlSetVOs(new ArrayList());
                    if ((pointVOlist = ApALByPointClient.getInstance().getApALByPoints_bySiteAreaAssetPointLocationPointApSet(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), channelVO.getCustomerAccount(), channelVO.getSiteId(), channelVO.getAreaId(), channelVO.getAssetId(), channelVO.getPointLocationId(), channelVO.getPointId(), channelVO.getApSetId())) != null) {
                        pointVOlist.forEach(vo -> channelVO.getApAlSetVOs().add(new ApAlSetVO(vo)));
                    }
                    if (!channelVO.getApAlSetVOs().isEmpty()) {
                        channelVO.setAlSetId(channelVO.getApAlSetVOs().get(0).getAlSetId());
                    }
                }
                // Set ApSet Name and AlSet Name
                if ((channelVO.getApSetName() == null || channelVO.getApSetName().isEmpty() || channelVO.getAlSetName() == null || channelVO.getAlSetName().isEmpty()) && channelVO.getApAlSetVOs() != null && !channelVO.getApAlSetVOs().isEmpty()) {
                    if ((apAlSetsVOList = APALSetClient.getInstance().getSiteApAlSetsByCustomerSiteIdApSetIdAlSetId(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), channelVO.getSiteId(), channelVO.getApSetId(), channelVO.getAlSetId())) == null || apAlSetsVOList.isEmpty()) {
                        apAlSetsVOList = APALSetClient.getInstance().getGlobalApAlSetsByCustomerApSetIdAlSetId(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), channelVO.getApSetId(), channelVO.getAlSetId());
                        if (apAlSetsVOList == null || apAlSetsVOList.isEmpty()) {
                            apAlSetsVOList = APALSetClient.getInstance().getGlobalApAlSetsByCustomerApSetIdAlSetId(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), DEFAULT_UPTIME_ACCOUNT, channelVO.getApSetId(), channelVO.getAlSetId());
                        }
                    }
                    if (apAlSetsVOList != null && !apAlSetsVOList.isEmpty()) {
                        channelVO.setApSetName(apAlSetsVOList.get(0).getApSetName());
                        channelVO.setAlSetName(apAlSetsVOList.get(0).getAlSetName());
                    }
                }

                // Update Info Dialog
                if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("deviceManagementInfoRowBean") == null) {
                    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("deviceManagementInfoRowBean", new DeviceManagementInfoRowBean());
                }
                ((DeviceManagementInfoRowBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("deviceManagementInfoRowBean")).presetFields(channelVO);
                navigationBean.updateDialog("device-management-info-row");

            } catch (Exception e) {
                Logger.getLogger(DeviceManagementBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            }
        }
    }

    /**
     * Opens the install overlay for the given device
     */
    public void installDevice() {
        char[] prefix;

        if (serialNumber != null && !serialNumber.isEmpty()) {
            try {
                prefix = new char[3];
                serialNumber.getChars(0, 3, prefix, 0);

                switch (String.valueOf(prefix)) {
                    case "001": // TS1 - all start with "001"
                        if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("createTS1Bean") == null) {
                            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("createTS1Bean", new CreateTS1Bean());
                            ((CreateTS1Bean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("createTS1Bean")).init();
                        }
                        ((CreateTS1Bean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("createTS1Bean")).setSerialNumber(serialNumber);
                        navigationBean.updateDialog("create-ts1");
                        PrimeFaces.current().executeScript("PF('defaultDlg').show()");
                        break;
                    default: // Other Devices will be added later
                        FacesContext.getCurrentInstance().addMessage("growlMessages", new FacesMessage(userManageBean.getResourceBundleString("label_install_device_is_currently_only_for_a_ts1")));
                        break;
                }
            } catch (Exception e) {
                Logger.getLogger(DeviceManagementBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            }
        }
    }

    /**
     * Enable the device
     */
    public void enableDevice() {
        char[] prefix;
        DeviceVO dvo;

        if (serialNumber != null && !serialNumber.isEmpty()) {
            try {
                prefix = new char[2];
                serialNumber.getChars(0, 2, prefix, 0);

                switch (String.valueOf(prefix)) {
                    case "00": // TS1 - all start with "001", so first two digits are "00"
                        if (serialNumber.startsWith("001")) {
                            dvo = new DeviceVO();
                            dvo.setExistingDeviceId(serialNumber);
                            if (ManageDeviceClient.getInstance().enableDevice(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), dvo)) {
                                Logger.getLogger(DeviceManagementBean.class.getName()).log(Level.INFO, "Device enabled for DeviceId: {0} ", serialNumber);
                                this.search();
                            } else {
                                Logger.getLogger(CreateSiteBean.class.getName()).log(Level.INFO, "Device enable failed for {0}", serialNumber);
                            }
                        }
                        break;
                    case "BB": // MistLx - all start with "BB"
                        dvo = new DeviceVO();
                        dvo.setExistingDeviceId(serialNumber);
                        if (ManageDeviceClient.getInstance().enableDevice(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), dvo)) {
                            Logger.getLogger(DeviceManagementBean.class.getName()).log(Level.INFO, "Device enabled for DeviceId: {0} ", serialNumber);
                            this.search();
                        } else {
                            Logger.getLogger(CreateSiteBean.class.getName()).log(Level.INFO, "Device enable failed for {0}", serialNumber);
                        }
                        break;
                }
            } catch (Exception e) {
                Logger.getLogger(DeviceManagementBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            }
        }
    }

    /**
     * Disable the device
     */
    public void disableDevice() {
        char[] prefix;
        DeviceVO dvo;

        if (serialNumber != null && !serialNumber.isEmpty()) {
            try {
                prefix = new char[2];
                serialNumber.getChars(0, 2, prefix, 0);

                switch (String.valueOf(prefix)) {
                    case "00": // TS1 - all start with "001", so first two digits are "00"
                        if (serialNumber.startsWith("001")) {
                            dvo = new DeviceVO();
                            dvo.setExistingDeviceId(serialNumber);
                            if (ManageDeviceClient.getInstance().disableDevice(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), dvo)) {
                                Logger.getLogger(DeviceManagementBean.class.getName()).log(Level.INFO, "Device disabled for DeviceId: {0} ", serialNumber);
                                this.search();
                            } else {
                                Logger.getLogger(CreateSiteBean.class.getName()).log(Level.INFO, "Device disable failed for {0}", serialNumber);
                            }
                        }
                        break;
                    case "BB": // MistLx - all start with "BB"
                        dvo = new DeviceVO();
                        dvo.setExistingDeviceId(serialNumber);
                        if (ManageDeviceClient.getInstance().disableDevice(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), dvo)) {
                            Logger.getLogger(DeviceManagementBean.class.getName()).log(Level.INFO, "Device disabled for DeviceId: {0} ", serialNumber);
                            this.search();
                        } else {
                            Logger.getLogger(CreateSiteBean.class.getName()).log(Level.INFO, "Device disable failed for {0}", serialNumber);
                        }
                        break;
                }
            } catch (Exception e) {
                Logger.getLogger(DeviceManagementBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            }
        }
    }

    /**
     * Opens the modify overlay for the given device
     */
    public void modifyDevice() {
        char[] prefix;

        if (serialNumber != null && !serialNumber.isEmpty()) {
            try {
                prefix = new char[3];
                serialNumber.getChars(0, 3, prefix, 0);

                switch (String.valueOf(prefix)) {
                    case "001": // TS1 - all start with "001"
                        if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("editTS1Bean") == null) {
                            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("editTS1Bean", new EditTS1Bean());
                            ((EditTS1Bean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("editTS1Bean")).init();
                        }
                        ((EditTS1Bean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("editTS1Bean")).presetFields(serialNumber);
                        navigationBean.updateDialog("edit-ts1");
                        PrimeFaces.current().executeScript("PF('defaultDlg').show()");
                        break;
                }
            } catch (Exception e) {
                Logger.getLogger(DeviceManagementBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            }
        }
    }

    /**
     * Remove the device
     */
    public void removeDevice() {
        char[] prefix;
        DeviceVO dvo;

        if (serialNumber != null && !serialNumber.isEmpty()) {
            try {
                prefix = new char[2];
                serialNumber.getChars(0, 2, prefix, 0);

                switch (String.valueOf(prefix)) {
                    case "00": // TS1 - all start with "001"
                        // TO DO
                        break;
                    case "BB": // Mist - all start with "BB"
                        dvo = new DeviceVO();
                        dvo.setExistingDeviceId(serialNumber);
                        if (ManageDeviceClient.getInstance().removeDevice(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), dvo)) {
                            Logger.getLogger(DeviceManagementBean.class.getName()).log(Level.INFO, "Device removed for DeviceId: {0} ", serialNumber);
                            this.search();
                        } else {
                            Logger.getLogger(CreateSiteBean.class.getName()).log(Level.INFO, "Device removal failed for {0}", serialNumber);
                        }
                        break;
                }
            } catch (Exception e) {
                Logger.getLogger(DeviceManagementBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            }
        }
    }

    /**
     * Opens the replace overlay for the given device
     */
    public void replaceDevice() {
        char[] prefix;

        if (serialNumber != null && !serialNumber.isEmpty()) {
            try {
                prefix = new char[3];
                serialNumber.getChars(0, 3, prefix, 0);

                switch (String.valueOf(prefix)) {
                    case "001": // TS1 - all start with "001"
                        // TO DO
                        break;
                }
            } catch (Exception e) {
                Logger.getLogger(DeviceManagementBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            }
        }
    }

    /**
     * Checks if the given serial number and action matches the devices that are
     * supported
     *
     * @param action, String Object, action being performed
     * @return boolean, true if the device is supported, else false
     */
    public boolean isSupportedDevice(String action) {
        char[] prefix;

        if (serialNumber != null && !serialNumber.isEmpty() && action != null && !action.isEmpty()) {
            try {
                prefix = new char[2];
                serialNumber.getChars(0, 2, prefix, 0);

                switch (String.valueOf(prefix)) {
                    case "00": // TS1 - all start with "001", so first two digits are "00"
                        switch (action.toLowerCase()) {
                            case "enable":
                            case "disable":
                            case "modify":
                                return serialNumber.startsWith("001");
                        }
                        break;
                    case "BB": // MistLx - all start with "BB"
                        switch (action.toLowerCase()) {
                            case "enable":
                            case "disable":
                            case "remove":
                                return true;
                        }
                        break;
                }
            } catch (Exception e) {
                Logger.getLogger(DeviceManagementBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            }
        }
        return false;
    }

    /**
     * Returns the hwDisabled field value
     *
     * @return Boolean, true if device is disabled, else false
     */
    public Boolean getHwDisabled() {
        if (!channelVOList.isEmpty()) {
            return channelVOList.get(0).getHwDisabled();
        }
        return null;
    }

    /**
     * Return a built String Object containing the device point count
     *
     * @return String Object
     */
    public String getPointsInUseString() {
        StringBuilder sb;
        try {
            sb = new StringBuilder();
            sb.append("(").append(channelVOList.size()).append(") ").append(userManageBean.getResourceBundleString("label_points_in_use_what_to_do"));
        } catch (Exception e) {
            Logger.getLogger(DeviceManagementBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            return null;
        }
        return sb.toString();
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public List<ChannelVO> getChannelVOList() {
        return channelVOList;
    }

    public String getTS1ACChannelNumberLabelByValue(int value) {
        return TS1ACChannelNumberEnum.getTS1ACChannelNumberLabelByValue(value);
    }

    public String getTS1DCChannelNumberLabelByValue(int value) {
        return TS1DCChannelNumberEnum.getTS1DCChannelNumberLabelByValue(value);
    }

    public Boolean getSerialNumberNotInUse() {
        return serialNumberNotInUse;
    }

    public String getCustomerName() {
        return customerName;
    }

    public String getRegion() {
        return region;
    }

    public String getCountry() {
        return country;
    }

    public String getSiteName() {
        return siteName;
    }

    public List<DeviceVO> getDeviceVOList() {
        return deviceVOList;
    }

    public List<DeviceVO> getFilteredDevices() {
        return filteredDevices;
    }

    public void setFilteredDevices(List<DeviceVO> filteredDevices) {
        this.filteredDevices = filteredDevices;
    }

    public List<FilterMeta> getFilterBy() {
        return filterBy;
    }

    public void setFilterBy(List<FilterMeta> filterBy) {
        this.filterBy = filterBy;
    }
}
