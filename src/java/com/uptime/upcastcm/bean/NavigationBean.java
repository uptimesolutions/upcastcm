/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.bean;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import org.primefaces.PrimeFaces;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.event.NodeCollapseEvent;
import org.primefaces.event.NodeExpandEvent;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.TreeNode;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.MenuModel;

/**
 *
 * @author twilcox
 */
public class NavigationBean implements Serializable {

    private String menuMode, page, dialogCard, secondDialogCard, thirdDialogCard, dialogHeader, secondDialogHeader, thirdDialogHeader, dialogContent, secondDialogContent, thirdDialogContent, customDialogStyle, secondCustomDialogStyle, thirdCustomDialogStyle, navigationIcon, navigationPreNodeIcon;
    private transient TreeNode treeNodeRoot, selectedNode;
    private transient MenuModel breadcrumbModel;
    private boolean rightPanelButton, topbarControlAddButton, leftTreePanelButton;

    /**
     * Constructor
     */
    public NavigationBean() {
        menuMode = "layout-static";
        page = "home";
        navigationIcon = null;
        navigationPreNodeIcon = null;
    }

    /**
     * Change the menuMode between "layout-slim" and "layout-static"
     */
    public void changeMenuMode() {
        PrimeFaces pf = PrimeFaces.current();

        if (menuMode.equalsIgnoreCase("layout-slim")) {
            menuMode = "layout-static layout-static-active";
            pf.executeScript("PrimeFaces.RomaConfigurator.changeMenuMode('layout-static layout-static-active');");
        } else {
            menuMode = "layout-slim";
            pf.executeScript("PrimeFaces.RomaConfigurator.changeMenuMode('layout-slim');");
        }
        pf.ajax().update("menuFormId");
    }

    /**
     * Update page based on the given page
     *
     * @param page, String Object
     * @return String object
     */
    public String updatePage(String page) {
        UserManageBean userManageBean;
        DefaultMenuItem index = new DefaultMenuItem();

        if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean") == null) {
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("userManageBean", new UserManageBean());
            ((UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean")).init();
        }
        userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
        try {
            breadcrumbModel = new DefaultMenuModel();
            if (page != null) {
                switch (this.page = page) {
                    case "alarms":
                        rightPanelButton = false;
                        leftTreePanelButton = true;
                        topbarControlAddButton = false;
                        navigationIcon = "pi pi-fw pi-bell";
                        navigationPreNodeIcon = null;

                        if ((AlarmsBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("alarmsBean") == null) {
                            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("alarmsBean", new AlarmsBean());
                            ((AlarmsBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("alarmsBean")).init();
                        } else {
                            ((AlarmsBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("alarmsBean")).init();
                        }
                        return this.page;
                    case "configuration":
                        rightPanelButton = false;
                        leftTreePanelButton = true;
                        topbarControlAddButton = true;
                        navigationIcon = "fa fa-wrench";
                        navigationPreNodeIcon = null;

                        if ((ConfigurationBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("configurationBean") == null) {
                            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("configurationBean", new ConfigurationBean());
                            ((ConfigurationBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("configurationBean")).init();
                        } else {
                            ((ConfigurationBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("configurationBean")).init();
                        }

                        return this.page;
                    case "home":
                        rightPanelButton = false;
                        leftTreePanelButton = false;
                        topbarControlAddButton = false;
                        navigationIcon = "pi pi-fw pi-home";
                        navigationPreNodeIcon = null;

                        if ((HomeBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("homeBean") == null) {
                            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("homeBean", new HomeBean());
                            ((HomeBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("homeBean")).init();
                        } else {
                            ((HomeBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("homeBean")).resetPage();
                        }

                        index.setValue(userManageBean.getResourceBundleString("breadcrumb_1"));
                        index.setIcon(navigationIcon);
                        breadcrumbModel.getElements().add(index);

                        return this.page;
                    case "work-order":
                        rightPanelButton = false;
                        leftTreePanelButton = true;
                        topbarControlAddButton = true;
                        navigationIcon = "pi pi-fw pi-tag";
                        navigationPreNodeIcon = null;

                        if ((WorkOrderBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("workOrderBean") == null) {
                            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("workOrderBean", new WorkOrderBean());
                            ((WorkOrderBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("workOrderBean")).init();
                        } else {
                            ((WorkOrderBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("workOrderBean")).init();
                        }

                        return this.page;
                    case "presets":
                        rightPanelButton = false;
                        leftTreePanelButton = false;
                        topbarControlAddButton = false;
                        navigationIcon = "pi pi-fw pi-sliders-h";
                        navigationPreNodeIcon = null;

                        index.setValue(userManageBean.getResourceBundleString("breadcrumb_5"));
                        index.setIcon(navigationIcon);
                        breadcrumbModel.getElements().add(index);

                        if ((PresetsBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("presetsBean") == null) {
                            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("presetsBean", new PresetsBean());
                            ((PresetsBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("presetsBean")).init();
                        } else {
                            ((PresetsBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("presetsBean")).init();
                        }

                        return this.page;
                    case "reports":
                        rightPanelButton = false;
                        leftTreePanelButton = false;
                        topbarControlAddButton = false;
                        navigationIcon = "pi pi-fw pi-chart-bar";
                        navigationPreNodeIcon = null;

                        index.setValue(userManageBean.getResourceBundleString("breadcrumb_6"));
                        index.setIcon(navigationIcon);
                        breadcrumbModel.getElements().add(index);

                        return this.page;
                    case "asset-analysis":
                        rightPanelButton = false;
                        leftTreePanelButton = true;
                        topbarControlAddButton = false;
                        navigationIcon = "pi pi-fw pi-chart-line";
                        navigationPreNodeIcon = "fa fa-flash";

                        if ((AssetAnalysisBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("assetAnalysisBean") == null) {
                            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("assetAnalysisBean", new AssetAnalysisBean());
                            ((AssetAnalysisBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("assetAnalysisBean")).init();
                        } else {
                            ((AssetAnalysisBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("assetAnalysisBean")).init();
                        }

                        return this.page;
                    case "users":
                        rightPanelButton = false;
                        leftTreePanelButton = false;
                        topbarControlAddButton = false;
                        navigationIcon = "pi pi-users";
                        navigationPreNodeIcon = null;

                        index.setValue(userManageBean.getResourceBundleString("breadcrumb_8"));
                        index.setIcon(navigationIcon);
                        breadcrumbModel.getElements().add(index);

                        if ((UsersBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("usersBean") == null) {
                            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("usersBean", new UsersBean());
                            ((UsersBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("usersBean")).init();
                        } else {
                            ((UsersBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("usersBean")).init();
                        }

                        return this.page;
                    case "device-management":
                        rightPanelButton = false;
                        leftTreePanelButton = false;
                        topbarControlAddButton = false;
                        navigationIcon = "fa fa-rss";
                        navigationPreNodeIcon = null;

                        index.setValue(userManageBean.getResourceBundleString("label_device_management"));
                        index.setIcon(navigationIcon);
                        breadcrumbModel.getElements().add(index);

                        if ((DeviceManagementBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("deviceManagementBean") == null) {
                            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("deviceManagementBean", new DeviceManagementBean());
                            ((DeviceManagementBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("deviceManagementBean")).init();
                        } else {
                            ((DeviceManagementBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("deviceManagementBean")).init();
                        }

                        return this.page;
                    case "system-status":
                        rightPanelButton = false;
                        leftTreePanelButton = false;
                        topbarControlAddButton = false;
                        navigationIcon = "fa fa-heartbeat";
                        navigationPreNodeIcon = null;

                        index.setValue(userManageBean.getResourceBundleString("breadcrumb_10"));
                        index.setIcon(navigationIcon);
                        breadcrumbModel.getElements().add(index);

                        if ((SystemStatusBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("systemStatusBean") == null) {
                            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("systemStatusBean", new SystemStatusBean());
                            ((SystemStatusBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("systemStatusBean")).init();
                        } else {
                            ((SystemStatusBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("systemStatusBean")).init();
                        }

                        return this.page;
                    case "subscriptions":
                        rightPanelButton = false;
                        leftTreePanelButton = false;
                        topbarControlAddButton = false;
                        navigationIcon = "fa fa-regular fa-link";
                        navigationPreNodeIcon = null;

                        index.setValue(userManageBean.getResourceBundleString("breadcrumb_11"));
                        index.setIcon(navigationIcon);
                        breadcrumbModel.getElements().add(index);

                        if ((SubscriptionBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("subscriptionBean") == null) {
                            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("subscriptionBean", new SubscriptionBean());
                            ((SubscriptionBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("subscriptionBean")).init();
                        } else {
                            ((SubscriptionBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("subscriptionBean")).init();
                        }

                        return this.page;
                }
            }
            throw new Exception();
        } catch (Exception e) {
            this.page = "home";
            rightPanelButton = false;
            leftTreePanelButton = false;
            topbarControlAddButton = false;
            navigationIcon = "pi pi-fw pi-home";
            navigationPreNodeIcon = null;
            breadcrumbModel = new DefaultMenuModel();

            if ((HomeBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("homeBean") == null) {
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("homeBean", new HomeBean());
                ((HomeBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("homeBean")).init();
            } else {
                ((HomeBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("homeBean")).resetPage();
            }

            index.setValue(userManageBean.getResourceBundleString("breadcrumb_1"));
            index.setIcon(navigationIcon);
            breadcrumbModel.getElements().add(index);

            return this.page;
        }
    }

    /**
     * Update the global dialog based on the given card
     *
     * @param card, String Object
     */
    public void updateDialog(String card) {
        UserManageBean userManageBean;

        if (card != null) {
            customDialogStyle = "vertical-center-align";
            userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");

            switch (card.toLowerCase()) {
                case "create-area":
                    dialogCard = "create-area";
                    dialogHeader = userManageBean.getResourceBundleString("dialog_createArea_header");
                    dialogContent = "/user/card/create-area.xhtml";
                    break;
                case "edit-area":
                    dialogCard = "edit-area";
                    dialogHeader = userManageBean.getResourceBundleString("dialog_editArea_header");
                    dialogContent = "/user/card/edit-area.xhtml";
                    break;
                case "create-asset-to-point":
                    dialogCard = "create-asset-to-point";
                    dialogHeader = userManageBean.getResourceBundleString("dialog_createAsset_header");
                    dialogContent = "/user/card/create-asset-to-point.xhtml";
                    break;
                case "create-asset":
                    dialogCard = "create-asset";
                    dialogHeader = userManageBean.getResourceBundleString("dialog_createAsset_header");
                    dialogContent = "/user/card/create-asset.xhtml";
                    break;
                case "edit-asset":
                    dialogCard = "edit-asset";
                    dialogHeader = userManageBean.getResourceBundleString("dialog_editAsset_header");
                    dialogContent = "/user/card/edit-asset.xhtml";
                    break;
                case "create-site":
                    dialogCard = "create-site";
                    dialogHeader = userManageBean.getResourceBundleString("dialog_createSite_header");
                    dialogContent = "/user/card/create-site.xhtml";
                    break;
                case "edit-site":
                    dialogCard = "edit-site";
                    dialogHeader = userManageBean.getResourceBundleString("dialog_editSite_header");
                    dialogContent = "/user/card/edit-site.xhtml";
                    break;
                case "create-point-location":
                    dialogCard = "create-point-location";
                    dialogHeader = userManageBean.getResourceBundleString("dialog_createPointLocation_header");
                    dialogContent = "/user/card/create-point-location.xhtml";
                    break;
                case "create-point":
                    dialogCard = "create-point";
                    dialogHeader = userManageBean.getResourceBundleString("dialog_createPoint_header");
                    dialogContent = "/user/card/create-point.xhtml";
                    break;
                case "create-ts1":
                    dialogCard = "create-ts1";
                    dialogHeader = userManageBean.getResourceBundleString("dialog_create_ts1_header");
                    dialogContent = "/user/card/create-ts1.xhtml";
                    break;
                case "edit-ts1":
                    dialogCard = "edit-ts1";
                    dialogHeader = userManageBean.getResourceBundleString("dialog_edit_sensor_header");
                    dialogContent = "/user/card/edit-ts1.xhtml";
                    break;
                case "install-device":
                    dialogCard = "install-device";
                    dialogHeader = userManageBean.getResourceBundleString("dialog_install_device_header");
                    dialogContent = "/user/card/install-device.xhtml";
                    break;
                case "import-config":
                    dialogCard = "import-config";
                    dialogHeader = userManageBean.getResourceBundleString("label_import_configuration");
                    dialogContent = "/user/card/import-config-upload.xhtml";
                    break;
                case "replace-device":
                    dialogCard = "replace-device";
                    dialogHeader = userManageBean.getResourceBundleString("label_replace_device");
                    dialogContent = "/user/card/replace-device.xhtml";
                    break;
                case "create-ap-al-set":
                    dialogCard = "create-ap-al-set";
                    dialogHeader = userManageBean.getResourceBundleString("dialog_createAnalysisParameters_header");
                    dialogContent = "/user/card/create-ap-al-set.xhtml";
                    break;
                case "logout":
                    dialogCard = "logout";
                    dialogHeader = userManageBean.getResourceBundleString("dialog_logout_header");
                    dialogContent = "/user/card/logout.xhtml";
                    break;
                case "user-preferences":
                    dialogCard = "user-preferences";
                    dialogHeader = userManageBean.getResourceBundleString("dialog_preferences_header");
                    dialogContent = "/user/card/user-preferences.xhtml";
                    break;
                case "create-tachometer":
                    dialogCard = "create-tachometer";
                    dialogHeader = userManageBean.getResourceBundleString("dialog_createTachometer_header");
                    dialogContent = "/user/card/create-tachometer.xhtml";
                    break;
                case "copy-tachometers":
                    dialogCard = "copy-tachometers";
                    dialogHeader = userManageBean.getResourceBundleString("dialog_copyTachometer_header");
                    dialogContent = "/user/card/copy-edit-tachometers.xhtml";
                    break;
                case "edit-tachometers":
                    dialogCard = "edit-tachometers";
                    dialogHeader = userManageBean.getResourceBundleString("dialog_editTachometer_header");
                    dialogContent = "/user/card/copy-edit-tachometers.xhtml";
                    break;
                case "copy-edit-ap-al-set":
                    dialogCard = "copy-edit-ap-al-set";
                    dialogHeader = userManageBean.getResourceBundleString("dialog_copyEditAnalysisParameters_header");
                    dialogContent = "/user/card/copy-edit-ap-al-set.xhtml";
                    break;
                case "edit-point":
                    dialogCard = "update-point";
                    dialogHeader = userManageBean.getResourceBundleString("dialog_updatePoint_header");
                    dialogContent = "/user/card/edit-point.xhtml";
                    break;
                case "global-sites":
                    dialogCard = "global-sites";
                    dialogHeader = userManageBean.getResourceBundleString("dialog_GlobalSites_header");
                    dialogContent = "/user/card/global-sites.xhtml";
                    break;
                case "alarm-details":
                    dialogCard = "alarm-details";
                    dialogHeader = userManageBean.getResourceBundleString("label_alarm_details");
                    dialogContent = "/user/card/alarm-details.xhtml";
                    break;
                case "alarm-history":
                    dialogCard = "alarm-history";
                    dialogHeader = userManageBean.getResourceBundleString("label_alarm_history");
                    dialogContent = "/user/card/alarm-history.xhtml";
                    break;
                case "edit-point-location":
                    dialogCard = "edit-point-location";
                    dialogHeader = userManageBean.getResourceBundleString("label_edit_point_location");
                    dialogContent = "/user/card/edit-point-location.xhtml";
                    break;
                case "create-presets-device":
                    dialogCard = "create-presets-device";
                    dialogHeader = userManageBean.getResourceBundleString("label_create_device");
                    dialogContent = "/user/card/create-presets-device.xhtml";
                    break;
                case "copy-presets-device":
                    dialogCard = "copy-presets-device";
                    dialogHeader = userManageBean.getResourceBundleString("label_copy_presets_device");
                    dialogContent = "/user/card/copy-edit-presets-device.xhtml";
                    break;
                case "edit-presets-device":
                    dialogCard = "edit-presets-device";
                    dialogHeader = userManageBean.getResourceBundleString("label_edit_presets_device");
                    dialogContent = "/user/card/copy-edit-presets-device.xhtml";
                    break;
                case "create-point-location-name":
                    dialogCard = "create-point-location-name";
                    dialogHeader = userManageBean.getResourceBundleString("label_create_point_location_name");
                    dialogContent = "/user/card/create-point-location-name.xhtml";
                    break;
                case "copy-point-location-name":
                    dialogCard = "copy-point-location-name";
                    dialogHeader = userManageBean.getResourceBundleString("label_copy_point_location_name");
                    dialogContent = "/user/card/copy-edit-point-location-name.xhtml";
                    break;
                case "edit-point-location-name":
                    dialogCard = "edit-point-location-name";
                    dialogHeader = userManageBean.getResourceBundleString("label_edit_point_location_name");
                    dialogContent = "/user/card/copy-edit-point-location-name.xhtml";
                    break;
                case "edit-work-order":
                    dialogCard = "edit-work-order";
                    dialogHeader = userManageBean.getResourceBundleString("dialog_edit_work_order_header");
                    dialogContent = "/user/card/edit-work-order.xhtml";
                    break;
                case "create-work-order":
                    dialogCard = "create-work-order";
                    dialogHeader = userManageBean.getResourceBundleString("label_create_workorder");
                    dialogContent = "/user/card/create-work-order.xhtml";
                    break;
                case "closed-work-order":
                    dialogCard = "closed-work-order";
                    dialogHeader = userManageBean.getResourceBundleString("label_closed_workorders");
                    dialogContent = "/user/card/closed-work-order.xhtml";
                    break;
                case "create-users":
                    dialogCard = "create-users";
                    dialogHeader = userManageBean.getResourceBundleString("label_create_user");
                    dialogContent = "/user/card/create-users.xhtml";
                    break;
                case "plot-display-options":
                    dialogCard = "plot-display-options";
                    dialogHeader = userManageBean.getResourceBundleString("label_plot_display_options");
                    dialogContent = "/user/card/plot-display-options.xhtml";
                    break;
                case "device-management-info-row":
                    dialogCard = "device-management-info-row";
                    dialogHeader = userManageBean.getResourceBundleString("device_management_label_info");
                    dialogContent = "/user/card/device-management-info-row.xhtml";
                    break;
                case "role-information":
                    dialogCard = "role-information";
                    dialogHeader = userManageBean.getResourceBundleString("dialog_header_user_roles");
                    dialogContent = "/user/card/role-information.xhtml";
                    break;
                case "create-bearing-catalog":
                    dialogCard = "create-bearing-catalog";
                    dialogHeader = userManageBean.getResourceBundleString("label_bearing_catalog");
                    dialogContent = "/user/card/create-bearing-catalog.xhtml";
                    break;
                case "create-custom-fault-frequecy-sets":
                    dialogCard = "create-custom-fault-frequecy-sets";
                    dialogHeader = userManageBean.getResourceBundleString("label_custom_fault_frequency_sets");
                    dialogContent = "/user/card/create-custom-fault-frequecy-sets.xhtml";
                    break;
                case "system-status-data-airbase":
                    dialogCard = "system-status-data";
                    dialogHeader = userManageBean.getResourceBundleString("label_airbase_status");
                    dialogContent = "/user/card/system-status-data.xhtml";
                    break;
                case "system-status-data-echobase":
                    dialogCard = "system-status-data";
                    dialogHeader = userManageBean.getResourceBundleString("label_echobase_status");
                    dialogContent = "/user/card/system-status-data.xhtml";
                    break;
                case "device-status":
                    dialogCard = "system-status-data";
                    dialogHeader = userManageBean.getResourceBundleString("label_device_status");
                    dialogContent = "/user/card/system-status-data.xhtml";
                    break;
                case "device-low-battery":
                    dialogCard = "system-status-data";
                    dialogHeader = userManageBean.getResourceBundleString("label_device_low_battery");
                    dialogContent = "/user/card/system-status-data.xhtml";
                    break;
                case "system-status-matrics-disabled-sensor":
                    dialogCard = "system-status-data";
                    dialogHeader = userManageBean.getResourceBundleString("label_disabled_sensors");
                    dialogContent = "/user/card/system-status-data.xhtml";
                    break;
                case "add-al-set":
                    dialogCard = "add-al-set";
                    dialogHeader = userManageBean.getResourceBundleString("label_add_al_set");
                    dialogContent = "/user/card/add-al-set.xhtml";
                    break;
                case "add-subscription":
                    dialogCard = "add-subscription";
                    dialogHeader = userManageBean.getResourceBundleString("label_add_subscription");
                    dialogContent = "/user/card/add-subscription.xhtml";
                    break;
                case "edit-subscription":
                    dialogCard = "edit-subscription";
                    dialogHeader = userManageBean.getResourceBundleString("label_edit_subscription");
                    dialogContent = "/user/card/edit-subscription.xhtml";
                    break;
                case "create-presets-asset":
                    dialogCard = "create-presets-asset";
                    dialogHeader = userManageBean.getResourceBundleString("label_create_presets_asset");
                    dialogContent = "/user/card/create-presets-asset.xhtml";
                    break;
                case "edit-presets-asset":
                    dialogCard = "edit-presets-asset";
                    dialogHeader = userManageBean.getResourceBundleString("label_edit_presets_asset");
                    dialogContent = "/user/card/copy-edit-presets-asset.xhtml";
                    break;
                case "copy-presets-asset":
                    dialogCard = "copy-presets-asset";
                    dialogHeader = userManageBean.getResourceBundleString("label_copy_presets_asset");
                    dialogContent = "/user/card/copy-edit-presets-asset.xhtml";
                    break;
                case "modify-alarm-status":
                    dialogCard = "modify-alarm-status";
                    dialogHeader = userManageBean.getResourceBundleString("btn_modify_alarm_status");
                    dialogContent = "/user/card/modify-alarm-status.xhtml";
                    break;
                default:
                    customDialogStyle = "";
                    dialogCard = "";
                    dialogHeader = "";
                    dialogContent = "/user/card/empty.xhtml";
                    break;

            }
        } else {
            customDialogStyle = "";
            dialogCard = "";
            dialogHeader = "";
            dialogContent = "/user/card/empty.xhtml";
        }
        PrimeFaces.current().ajax().update("defaultDialogContainerId");
    }

    /**
     * Update the second global dialog based on the given card
     *
     * @param card, String Object
     */
    public void updateSecondDialog(String card) {
        UserManageBean userManageBean;

        if (card != null) {
            secondCustomDialogStyle = "vertical-center-align";
            userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");

            switch (card.toLowerCase()) {
                case "device-management-info-row":
                    secondDialogCard = "device-management-info-row";
                    secondDialogHeader = userManageBean.getResourceBundleString("device_management_label_info");
                    secondDialogContent = "/user/card/device-management-info-row.xhtml";
                    break;
                case "add-trend":
                    secondDialogCard = "add-trend";
                    secondDialogHeader = userManageBean.getResourceBundleString("label_add_trend");
                    secondDialogContent = "/user/card/add-trend.xhtml";
                    break;
                case "edit-channel":
                    secondDialogCard = "edit-channel";
                    secondDialogHeader = userManageBean.getResourceBundleString("dialog_edit_sensor_header");
                    secondDialogContent = "/user/card/edit-channel.xhtml";
                    break;
                case "add-channel":
                    secondDialogCard = "add-channel";
                    secondDialogHeader = userManageBean.getResourceBundleString("dialog_add_Sensor_header");
                    secondDialogContent = "/user/card/add-channel.xhtml";
                    break;
                case "edit-installdevice-presets":
                    secondDialogCard = "edit-installdevice-presets";
                    secondDialogHeader = userManageBean.getResourceBundleString("dialog_install_device_header");
                    secondDialogContent = "/user/card/edit-installdevice-presets.xhtml";
                case "edit-point":
                    secondDialogCard = "edit-point";
                    secondDialogHeader = userManageBean.getResourceBundleString("dialog_updatePoint_header");
                    secondDialogContent = "/user/card/edit-installdevice-presets.xhtml";
                    break;
                case "create-fault-frequencies":
                    secondDialogCard = "create-fault-frequencies";
                    secondDialogHeader = userManageBean.getResourceBundleString("dialog_create_fault_frequency_header");
                    secondDialogContent = "/user/card/create-fault-frequencies.xhtml";
                    break;
                case "edit-fault-frequencies":
                    secondDialogCard = "edit-fault-frequencies";
                    secondDialogHeader = userManageBean.getResourceBundleString("dialog_edit_site_fault_frequency_header");
                    secondDialogContent = "/user/card/copy-edit-fault-frequencies.xhtml";
                    break;
                case "copy-fault-frequencies":
                    secondDialogCard = "copy-fault-frequencies";
                    secondDialogHeader = userManageBean.getResourceBundleString("dialog_copy_site_fault_frequency_header");
                    secondDialogContent = "/user/card/copy-edit-fault-frequencies.xhtml";
                    break;
                case "add-preset-point-location":
                    secondDialogCard = "add-preset-point-location";
                    secondDialogHeader = userManageBean.getResourceBundleString("dialog_createPointLocation_header");
                    secondDialogContent = "/user/card/add-preset-point-location.xhtml";
                    break;
                case "edit-preset-point-location":
                    secondDialogCard = "edit-preset-point-location";
                    secondDialogHeader = userManageBean.getResourceBundleString("label_edit_point_location");
                    secondDialogContent = "/user/card/copy-edit-preset-point-location.xhtml";
                    break;
                case "copy-preset-point-location":
                    secondDialogCard = "copy-preset-point-location";
                    secondDialogHeader = userManageBean.getResourceBundleString("label_copy_point_location");
                    secondDialogContent = "/user/card/copy-edit-preset-point-location.xhtml";
                    break;
                default:
                    secondCustomDialogStyle = "";
                    secondDialogCard = "";
                    secondDialogHeader = "";
                    secondDialogContent = "/user/card/empty.xhtml";
                    break;

            }
        } else {
            secondCustomDialogStyle = "";
            secondDialogCard = "";
            secondDialogHeader = "";
            secondDialogContent = "/user/card/empty.xhtml";
        }
        PrimeFaces.current().ajax().update("defaultSecondDialogContainerId");
    }

    /**
     * Update the third global dialog based on the given card
     *
     * @param card, String Object
     */
    public void updateThirdDialog(String card) {
        UserManageBean userManageBean;

        if (card != null) {
            thirdCustomDialogStyle = "vertical-center-align";
            userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");

            switch (card.toLowerCase()) {
                default:
                    thirdDialogCard = "";
                    thirdDialogHeader = "";
                    thirdDialogContent = "/user/card/empty.xhtml";
                    break;

            }
        } else {
            thirdDialogCard = "";
            thirdDialogHeader = "";
            thirdDialogContent = "/user/card/empty.xhtml";
        }
        PrimeFaces.current().ajax().update("defaultThirdDialogContainerId");
    }

    /**
     * Action to take when global Dialog box closed
     */
    public void closeDialog() {
        try {
            if (dialogCard != null) {
                switch (dialogCard) {
                    case "create-area":
                        ((CreateAreaBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("createAreaBean")).resetPage();
                        break;
                    case "edit-area":
                        ((EditAreaBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("editAreaBean")).resetPage();
                        break;
                    case "edit-asset-to-point":
                    case "create-asset-to-point":
                        ((CreateAssetBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("createAssetBean")).resetPage();
                        break;
                    case "create-asset":
                        ((CreateAssetBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("createAssetBean")).resetPage();
                        break;
                    case "edit-asset":
                        ((EditAssetBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("editAssetBean")).resetPage();
                        break;
                    case "create-site":
                        ((CreateSiteBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("createSiteBean")).resetPage();
                        break;
                    case "edit-site":
                        ((EditSiteBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("editSiteBean")).resetPage();
                        break;
                    case "create-point-location":
                        ((CreatePointLocationBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("createPointLocationBean")).resetPage();
                        break;
                    case "edit-point-location":
                        ((EditPointLocationBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("editPointLocationBean")).resetPage();
                        break;
                    case "create-point":
                        ((CreatePointBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("createPointBean")).resetPage();
                        break;
                    case "update-point":
                        ((EditPointBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("editPointBean")).resetPage();
                        break;
                    case "copy-tachometers":
                        ((CopyEditTachometersBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("copyEditTachometersBean")).resetPage();
                        break;
                    case "edit-tachometers":
                        ((CopyEditTachometersBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("copyEditTachometersBean")).resetPage();
                        break;
                    case "create-tachometer":
                        ((CreateTachometerBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("createTachometerBean")).resetPage();
                        break;
                    case "edit-fault-frequencies":
                        ((CopyEditFaultFrequenciesBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("copyEditFaultFrequenciesBean")).resetPage();
                        break;
                    case "copy-fault-frequencies":
                        ((CopyEditFaultFrequenciesBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("copyEditFaultFrequenciesBean")).resetPage();
                        break;
                    case "create-fault-frequencies":
                        ((CreateFaultFrequenciesBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("createFaultFrequenciesBean")).resetPage();
                        break;
                    case "create-ap-al-set":
                        ((CreateApAlSetBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("createApAlSetBean")).resetPage();
                        break;
                    case "copy-edit-ap-al-set":
                        ((CopyEditApAlSetBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("copyEditApAlSetBean")).resetPage();
                        break;
                    case "alarm-history":
                        ((AlarmsBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("alarmsBean")).clearRowSelection();
                        break;
                    case "create-presets-device":
                        ((CreatePresetsDeviceBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("createPresetsDeviceBean")).resetPage();
                        break;
                    case "copy-presets-device":
                    case "edit-presets-device":
                        ((CopyEditPresetsDeviceBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("copyEditPresetsDeviceBean")).resetPage();
                        break;
                    case "create-point-location-name":
                        ((CreatePointLocationNameBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("createPointLocationNameBean")).resetPage();
                        break;
                    case "copy-point-location-name":
                        ((CopyEditPointLocationNameBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("copyEditPointLocationNameBean")).resetPage();
                        break;
                    case "edit-point-location-name":
                        ((CopyEditPointLocationNameBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("copyEditPointLocationNameBean")).resetPage();
                        break;
                    case "edit-work-order":
                        ((EditWorkOrderBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("editWorkOrderBean")).resetPage();
                        break;
                    case "create-work-order":
                        ((CreateWorkOrderBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("createWorkOrderBean")).resetPage();
                        break;
                    case "closed-work-order":
                        ((ClosedWorkOrderBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("closedWorkOrderBean")).resetPage();
                        break;
                    case "create-users":
                        ((CreateUsersBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("createUsersBean")).resetPage();
                        break;
                    case "user-preferences":
                        ((UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean")).resetUserPreferenceTabs();
                        break;
                    case "install-device":
                        ((InstallDeviceBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("installDeviceBean")).resetPage();
                        break;
                    case "replace-device":
                        ((ReplaceDeviceBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("replaceDeviceBean")).resetPage();
                        break;
                    case "device-management-info-row":
                        ((DeviceManagementInfoRowBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("deviceManagementInfoRowBean")).resetPage();
                        break;
                    case "edit-ts1":
                        ((EditTS1Bean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("editTS1Bean")).resetPage();
                        break;
                    case "create-ts1":
                        ((CreateTS1Bean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("createTS1Bean")).resetPage();
                        break;
                    case "create-bearing-catalog":
                        ((CreateBearingCatalogBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("createBearingCatalogBean")).resetPage();
                        break;
                    case "add-al-set":
                        ((AddAlSetBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("addAlSetBean")).resetPage();
                        break;
                    case "plot-display-options":
                        break;
                    case "logout":
                        break;
                    case "role-information":
                        break;
                    case "import-config":
                        break;
                    case "create-custom-fault-frequecy-sets":
                        break;
                    case "system-status-data-airbase":
                        break;
                    case "system-status-data-echobase":
                        break;
                    case "device-status":
                        break;
                    case "device-low-battery":
                        break;
                    case "system-status-matrics-disabled-sensor":
                        break;
                    default:
                        break;
                }
            }
        } catch (Exception e) {
            Logger.getLogger(NavigationBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Action to take when second global Dialog box closed
     */
    public void closeSecondDialog() {
        try {
            if (secondDialogCard != null) {
                switch (secondDialogCard) {
                    case "device-management-info-row":
                        ((DeviceManagementInfoRowBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("deviceManagementInfoRowBean")).resetPage();
                        break;
                    case "add-trend":
                        ((AddTrendBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("addTrendBean")).resetPage();
                        break;
                    case "edit-channel":
                        ((EditChannelBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("editChannelBean")).resetPage();
                        break;
                    case "add-channel":
                        ((AddChannelBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("addChannelBean")).resetPage();
                        break;
                    case "edit-installdevice-presets":
                        ((EditInstallDevicePresetsBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("editInstallDevicePresetsBean")).resetPage();
                        break;
                    case "edit-fault-frequencies":
                        ((CopyEditFaultFrequenciesBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("copyEditFaultFrequenciesBean")).resetPage();
                        break;
                    case "copy-fault-frequencies":
                        ((CopyEditFaultFrequenciesBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("copyEditFaultFrequenciesBean")).resetPage();
                        break;
                    case "create-fault-frequencies":
                        ((CreateFaultFrequenciesBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("createFaultFrequenciesBean")).resetPage();
                        break;
                    default:
                        break;
                }
            }
        } catch (Exception e) {
            Logger.getLogger(NavigationBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Action to take when third global Dialog box closed
     */
    public void closeThirdDialog() {
        try {
            if (thirdDialogCard != null) {
                switch (thirdDialogCard) {
                    default:
                        break;
                }
            }
        } catch (Exception e) {
            Logger.getLogger(NavigationBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Action to be taken upon the Node Expand Event
     *
     * @param event, NodeExpandEvent Object
     */
    public void onNodeExpand(NodeExpandEvent event) {
        try {
            switch (page) {
                case "alarms":
                    ((AlarmsBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("alarmsBean")).onNodeExpand(event);
                    break;
                case "configuration":
                    ((ConfigurationBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("configurationBean")).onNodeExpand(event);
                    break;
                case "home":
                    break;
                case "work-order":
                    ((WorkOrderBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("workOrderBean")).onNodeExpand(event);
                    break;
                case "reports":
                    break;
                case "asset-analysis":
                    ((AssetAnalysisBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("assetAnalysisBean")).onNodeExpand(event);
                    break;
            }
        } catch (Exception e) {
            Logger.getLogger(NavigationBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Action to be taken upon the Node Collapse Event
     *
     * @param event, NodeCollapseEvent Object
     */
    public void onNodeCollapse(NodeCollapseEvent event) {
        try {
            switch (page) {
                case "alarms":
                    ((AlarmsBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("alarmsBean")).onNodeCollapse(event);
                    break;
                case "configuration":
                    ((ConfigurationBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("configurationBean")).onNodeCollapse(event);
                    break;
                case "home":
                    break;
                case "work-order":
                    ((WorkOrderBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("workOrderBean")).onNodeCollapse(event);
                    break;
                case "reports":
                    break;
                case "asset-analysis":
                    ((AssetAnalysisBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("assetAnalysisBean")).onNodeCollapse(event);
                    break;
            }
        } catch (Exception e) {
            Logger.getLogger(NavigationBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    public void onNodeSelect(NodeSelectEvent event) {
        try {
            switch (page) {
                case "alarms":
                    ((AlarmsBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("alarmsBean")).onNodeSelect(event);
                    break;
                case "configuration":
                    ((ConfigurationBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("configurationBean")).onNodeSelect(event);
                    resetDataTable();
                    break;
                case "home":
                    break;
                case "work-order":
                    ((WorkOrderBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("workOrderBean")).onNodeSelect(event);
                    resetDataTable();
                    break;
                case "reports":
                    break;
                case "asset-analysis":
                    ((AssetAnalysisBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("assetAnalysisBean")).onNodeSelect(event);
                    break;
            }
        } catch (Exception e) {
            Logger.getLogger(NavigationBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    public String getMenuMode() {
        return menuMode;
    }

    public MenuModel getBreadcrumbModel() {
        return breadcrumbModel;
    }

    public void setBreadcrumbModel(MenuModel breadcrumbModel) {
        this.breadcrumbModel = breadcrumbModel;
    }

    public boolean isRightPanelButton() {
        return rightPanelButton;
    }

    public boolean isLeftTreePanelButton() {
        return leftTreePanelButton;
    }

    public boolean isTopbarControlAddButton() {
        return topbarControlAddButton;
    }

    public TreeNode getTreeNodeRoot() {
        return treeNodeRoot;
    }

    public void setTreeNodeRoot(TreeNode treeNodeRoot) {
        this.treeNodeRoot = treeNodeRoot;
    }

    public TreeNode getSelectedNode() {
        return selectedNode;
    }

    public void setSelectedNode(TreeNode selectedNode) {
        this.selectedNode = selectedNode;
    }

    public String getPage() {
        return page;
    }

    public String getDialogHeader() {
        return dialogHeader;
    }

    public String getDialogContent() {
        return dialogContent;
    }

    public String getCustomDialogStyle() {
        return customDialogStyle;
    }

    public String getSecondDialogHeader() {
        return secondDialogHeader;
    }

    public String getSecondDialogContent() {
        return secondDialogContent;
    }

    public String getSecondCustomDialogStyle() {
        return secondCustomDialogStyle;
    }

    public String getThirdDialogHeader() {
        return thirdDialogHeader;
    }

    public String getThirdDialogContent() {
        return thirdDialogContent;
    }

    public String getThirdCustomDialogStyle() {
        return thirdCustomDialogStyle;
    }

    public String getNavigationIcon() {
        return navigationIcon;
    }

    public String getNavigationPreNodeIcon() {
        return navigationPreNodeIcon;
    }

    private void resetDataTable() {
        DataTable datatable;

        try {
            if (page.equals("work-order")) {
                if ((datatable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("workOrderResultFormId:workOrderResultTableId")) != null) {
                    datatable.reset();
                }
            } else if(page.equals("configuration")){
                if ((datatable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("configurationResultFormId:assetDataTableId")) != null) {
                    datatable.reset();
                }
            }
        } catch (Exception e) {
            Logger.getLogger(UserManageBean.class.getName()).log(Level.SEVERE, "***Error occured*** {0} ", e.getMessage());
        }
    }
}
