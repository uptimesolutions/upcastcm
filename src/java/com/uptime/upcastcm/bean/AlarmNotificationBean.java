/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.AlarmNotificationClient;
import com.uptime.upcastcm.http.client.AssetClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import com.uptime.upcastcm.utils.vo.AlarmNotificationVO;
import com.uptime.upcastcm.utils.vo.AssetVO;
import com.uptime.upcastcm.utils.vo.UserPreferencesVO;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.primefaces.PrimeFaces;

/**
 *
 * @author aswani
 */
public class AlarmNotificationBean implements Serializable {
    private AlarmNotificationVO alarmNotificationVO;
    private Set<UUID> assetIds, initialAssets;
    private String siteId;
    private List<AssetVO> assetVOList;
    private UserPreferencesVO userPreferencesVO;
    private boolean createNotification = false;
    private boolean selectAll = false;
    boolean enableCreate = false;

    /**
     * Constructor
     */
    public AlarmNotificationBean() {
        Logger.getLogger(AlarmNotificationBean.class.getName()).log(Level.SEVERE, "In AlarmNotificationBean");
    }

    public void reset() {
        createNotification = false;
        selectAll = false;
        initialAssets = new HashSet();
        assetIds = new HashSet();
        assetVOList = new ArrayList();
        alarmNotificationVO = new AlarmNotificationVO();
        if (userPreferencesVO != null && userPreferencesVO.getDefaultSite() != null) {
            siteId = userPreferencesVO.getDefaultSite().toString();
        }
        
        onSiteChange();
    }

    public void onSiteChange() {
        List<AlarmNotificationVO> alNoteList;
        
        if (userPreferencesVO.getCustomerAccount() != null && userPreferencesVO.getUserId() != null && siteId != null && siteId.length() > 0) {
            alarmNotificationVO.setSiteId(UUID.fromString(siteId));
            assetVOList = AssetClient.getInstance().getAssetSiteAreaByCustomerSite(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userPreferencesVO.getCustomerAccount(), alarmNotificationVO.getSiteId().toString());
            initialAssets = new HashSet();
            if (!(alNoteList = AlarmNotificationClient.getInstance().getUserSiteListByUserIdSiteId(ServiceRegistrarClient.getInstance().getServiceHostURL(USER_PREFERENCE_SERVICE_NAME), userPreferencesVO.getUserId(), siteId)).isEmpty()) {
                createNotification = false;
                assetIds = alNoteList.stream().map(alNote -> alNote.getAssetId()).filter(i -> i != null).collect(Collectors.toSet());
                initialAssets.addAll(assetIds);
                alarmNotificationVO = alNoteList.get(0);
                selectAll = assetIds.isEmpty();
            } else {
                initialAssets.clear();
                createNotification = true;
                alarmNotificationVO = new AlarmNotificationVO();
                alarmNotificationVO.setSiteId(UUID.fromString(siteId));
            }
        }
        updateBean();
    }

    /**
     * Saves the changes made to the alarms notification tables
     */
    public void saveAlarmNotification() {
        UserManageBean userManageBean;
        Set<UUID> deleteSet;
        Object assetId;
        boolean success;
        
        if (userPreferencesVO.getUserId() != null) {
            if (emailValidation()) { 
                alarmNotificationVO.setUserId(userPreferencesVO.getUserId());
                alarmNotificationVO.setCreateAssetIds(assetIds);
                
                if (createNotification) {
                    if (!assetIds.isEmpty()) {
                        assetId = assetIds.iterator().next();
                        alarmNotificationVO.setAssetId(UUID.fromString(assetId.toString()));
                    }
                    success = AlarmNotificationClient.getInstance().createAlarmNotification(ServiceRegistrarClient.getInstance().getServiceHostURL(USER_PREFERENCE_SERVICE_NAME), alarmNotificationVO);
                } else if (!selectAll) {
                    deleteSet = new HashSet();
                    
                    alarmNotificationVO.setSiteId(UUID.fromString(siteId));
                    if (!assetIds.isEmpty()) {
                        assetId = assetIds.iterator().next();
                        alarmNotificationVO.setAssetId(UUID.fromString(assetId.toString()));
                    }

                    initialAssets.stream().filter(id -> !assetIds.contains(id.toString())).forEachOrdered(id -> deleteSet.add(id));
                    alarmNotificationVO.setDeleteAssetIds(deleteSet);
                    success = AlarmNotificationClient.getInstance().updateAlarmNotification(ServiceRegistrarClient.getInstance().getServiceHostURL(USER_PREFERENCE_SERVICE_NAME), alarmNotificationVO);
                } else {
                    deleteSet = new HashSet();
                    
                    initialAssets.stream().filter(id -> !assetIds.contains(id.toString())).forEachOrdered(id -> deleteSet.add(id));
                    alarmNotificationVO.setDeleteAssetIds(deleteSet);
                    alarmNotificationVO.setCreateAssetIds(assetIds);
                    alarmNotificationVO.setAssetId(null);
                    
                    success = AlarmNotificationClient.getInstance().updateAlarmNotification(ServiceRegistrarClient.getInstance().getServiceHostURL(USER_PREFERENCE_SERVICE_NAME), alarmNotificationVO);
                }

                if (success) {
                    PrimeFaces.current().ajax().update("nonAutoGrowlId");
                    PrimeFaces.current().ajax().update("menuFormId");
                    PrimeFaces.current().ajax().update("breadcrumbFormId");
                    PrimeFaces.current().ajax().update("topbarControlFormId");
                    PrimeFaces.current().ajax().update("selectSiteFormId");
                    PrimeFaces.current().ajax().update("contentId");
                    PrimeFaces.current().executeScript("PF('defaultDlg').hide()");
                }
            } else if ((userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean")) != null) {
                FacesContext.getCurrentInstance().addMessage("growlMessages", new FacesMessage(userManageBean.getResourceBundleString("growl_invalid_email_or_emails")));
            }
        }
    }
    
    /**
     * Validates the email given by the user using a regex statement
     * @return boolean, true if validated, else false
     */
    public boolean emailValidation() {
        Pattern pattern = Pattern.compile(REGEX_EMAIL);
        
        try {
            if (alarmNotificationVO.getEmailAddress() != null && !alarmNotificationVO.getEmailAddress().isEmpty()) {
                for (String email : alarmNotificationVO.getEmailAddress().split(",")) {
                    if (email == null || email.isEmpty() || !pattern.matcher(email.trim()).matches()) {
                        return false;
                    }
                }
                return true;
            }
        } catch (Exception e) {
            Logger.getLogger(AlarmNotificationBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
        return false;
    }

    public void updateBean() {
        Logger.getLogger(AlarmNotificationBean.class.getName()).log(Level.INFO, "update bean.");

        if (siteId == null || alarmNotificationVO.getEmailAddress() == null || alarmNotificationVO.getEmailAddress().trim().isEmpty()) {
            enableCreate = true;
            return;
        }
        if (selectAll) {
            enableCreate = false;
            return;
        }
        enableCreate = (assetIds == null || assetIds.isEmpty());
    }

    public boolean isSelectAll() {
        return selectAll;
    }

    public void setSelectAll(boolean selectAll) {
        if (selectAll) {
            assetIds = new HashSet<>();
        }
        this.selectAll = selectAll;
    }

    public void setAssetVOList(List<AssetVO> assetVOList) {
        this.assetVOList = assetVOList;
    }

    public String getSiteId() {
        return siteId;
    }

    public void setSiteId(String siteId) {
        this.siteId = siteId;
    }

    public boolean isEnableCreate() {
        return enableCreate;
    }

    public void setEnableCreate(boolean enableCreate) {
        this.enableCreate = enableCreate;
    }

    public Set<UUID> getAssetIds() {
        return assetIds;
    }

    public void setAlarmNotificationVO(AlarmNotificationVO alarmNotificationVO) {
        this.alarmNotificationVO = alarmNotificationVO;
    }

    public void setAssetIds(Set<UUID> assetIds) {
        this.assetIds = assetIds;
    }

    public List<AssetVO> getAssetVOList() {
        return assetVOList;
    }

    public AlarmNotificationVO getAlarmNotificationVO() {
        return alarmNotificationVO;
    }

    public UserPreferencesVO getUserPreferencesVO() {
        return userPreferencesVO;
    }

    public void setUserPreferencesVO(UserPreferencesVO userPreferencesVO) {
        this.userPreferencesVO = userPreferencesVO;
    }

    public Set<UUID> getInitialAssets() {
        return initialAssets;
    }

    public void setInitialAssets(Set<UUID> initialAssets) {
        this.initialAssets = initialAssets;
    }

    public boolean isCreateNotification() {
        return createNotification;
    }

    public void setCreateNotification(boolean createNotification) {
        this.createNotification = createNotification;
    }
    
    
}
