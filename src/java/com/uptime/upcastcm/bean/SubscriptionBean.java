/**
 * @author aswani
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.KafkaProxyClient;
import com.uptime.upcastcm.http.client.SubscriptionsClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.CONFIGURATION_SERVICE_NAME;
import static com.uptime.upcastcm.utils.ApplicationConstants.KAFKA_PROXY_SERVICE_NAME;
import com.uptime.upcastcm.utils.enums.SubscriptionsEnum;
import com.uptime.upcastcm.utils.vo.SubscriptionsVO;
import com.uptime.upcastcm.utils.vo.UserSitesVO;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import org.primefaces.PrimeFaces;

/**
 *
 * @author aswani
 */
public class SubscriptionBean implements Serializable {

    private UserManageBean userManageBean;
    private NavigationBean navigationBean;
    private List<SubscriptionsVO> subscriptionsVOs = null;
    private SubscriptionsVO subscriptionsVO = null;
    private List<UserSitesVO> siteList;
    private List<SelectItem> subscriptionsTypes;
    private List<SelectItem> mqProtocol;
    private List<String> preExistingSubTypes;
    private boolean uptimeHostedSelected, subscriptionTypeSelected, wavehookUrlData, mqTopicNameProvided;

    public SubscriptionBean() {
        userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
        navigationBean = (NavigationBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("navigationBean");
        siteList = new ArrayList();
        siteList.add(userManageBean.getCurrentSite());
        subscriptionsTypes = SubscriptionsEnum.getSubscriptionsTypes();
        mqProtocol = SubscriptionsEnum.getMQProtocols();
        subscriptionsVO = new SubscriptionsVO();
    }

    /**
     * Post Constructor
     */
    @PostConstruct
    public void init() {
        Logger.getLogger(SubscriptionBean.class.getName()).log(Level.INFO, "***init***");
        subscriptionsVOs = SubscriptionsClient.getInstance().getSubscriptionsByCustomerSite(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME),
                userManageBean.getCurrentSite().getCustomerAccount(), userManageBean.getCurrentSite().getSiteId().toString());

    }

    public void addSubscription() {
        if (navigationBean != null) {
            navigationBean.updateDialog("add-subscription");
        }
        subscriptionsVO = new SubscriptionsVO();
        subscriptionsVO.setSiteId(userManageBean.getCurrentSite().getSiteId());
        subscriptionsVO.setCustomerAccount(userManageBean.getCurrentSite().getCustomerAccount());
        subscriptionsVO.setInternal(true);
        subscriptionsVO.setMqProtocol("MQTT");
        uptimeHostedSelected = true;
        wavehookUrlData = false;
        subscriptionTypeSelected = false;
        mqTopicNameProvided = false;

        if (subscriptionsVOs != null) {
            preExistingSubTypes = subscriptionsVOs.stream().map(e -> e.getSubscriptionType()).collect(Collectors.toList());
            subscriptionsTypes.forEach(action -> {
                action.setDisabled(preExistingSubTypes.contains(action.getValue()));
            });
        } else {
            subscriptionsTypes.forEach(action -> {
                action.setDisabled(false);
            });
        }
    }

    public void createSubscription() {
        SubscriptionsVO createSubscriptionObj = getSubscriptionVoObj();
        Logger.getLogger(SubscriptionBean.class.getName()).log(Level.INFO, "createSubscriptionObj {0}", createSubscriptionObj);
        boolean result = SubscriptionsClient.getInstance().createSubscriptions(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), createSubscriptionObj);
        if (result) {
            if (subscriptionsVOs == null) {
                subscriptionsVOs = new ArrayList();
            }
            subscriptionsVOs.add(createSubscriptionObj);
            subscriptionsVO = new SubscriptionsVO();
        }
//        PrimeFaces.current().ajax().update("dt-subscription");
    }

    public void updateSubscription() {
        SubscriptionsVO updateSubscriptionObj = getSubscriptionVoObj();
        Logger.getLogger(SubscriptionBean.class.getName()).log(Level.INFO, "updateSubscriptionObj {0}", updateSubscriptionObj);
        boolean result = SubscriptionsClient.getInstance().updateSubscriptions(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), updateSubscriptionObj);
        if (result) {
            subscriptionsVOs = SubscriptionsClient.getInstance().getSubscriptionsByCustomerSite(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME),
                    userManageBean.getCurrentSite().getCustomerAccount(), userManageBean.getCurrentSite().getSiteId().toString());
            subscriptionsVO = new SubscriptionsVO();
        }
    }

    public void deleteSubscription() {
        Logger.getLogger(SubscriptionBean.class.getName()).log(Level.INFO, "subscriptionsVO {0}", subscriptionsVO);
        boolean result = SubscriptionsClient.getInstance().deleteSubscriptions(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), subscriptionsVO);
        if (result) {
            subscriptionsVOs = null;
            subscriptionsVO = new SubscriptionsVO();
            subscriptionsTypes.forEach(action -> {
                action.setDisabled(false);
            });
        }
    }

    public void deleteSubscription(SubscriptionsVO sVO) {
        Logger.getLogger(SubscriptionBean.class.getName()).log(Level.INFO, "subscriptionsVO {0}", sVO);
        boolean result = SubscriptionsClient.getInstance().deleteSubscriptions(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), sVO);
        if (result) {
            Iterator<SubscriptionsVO> iterator = subscriptionsVOs.iterator();
            while (iterator.hasNext()) {
                SubscriptionsVO item = iterator.next();
                if (sVO.getCustomerAccount().equalsIgnoreCase(item.getCustomerAccount()) && sVO.getSiteId().toString().equalsIgnoreCase(item.getSiteId().toString()) && sVO.getSubscriptionType().equalsIgnoreCase(item.getSubscriptionType())) {
                    iterator.remove(); // Safe removal using iterator
                }
            }
            subscriptionsVO = new SubscriptionsVO();
            subscriptionsTypes.forEach(action -> {
                action.setDisabled(false);
            });
        }
    }

    public void editSubscription(SubscriptionsVO subscriptionVO) {
        this.subscriptionsVO = new SubscriptionsVO(subscriptionVO);
        if (navigationBean != null) {
            navigationBean.updateDialog("edit-subscription");
        }
        uptimeHostedSelected = (subscriptionsVO.isInternal() && (subscriptionsVO.getWebhookUrl() == null || subscriptionsVO.getWebhookUrl().isEmpty()));
        subscriptionTypeSelected = subscriptionsVO.getSubscriptionType() != null && !subscriptionsVO.getSubscriptionType().isEmpty();
        wavehookUrlData = subscriptionsVO.getWebhookUrl() != null && !subscriptionsVO.getWebhookUrl().isEmpty();
        mqTopicNameProvided = subscriptionsVO.getMqQueueName() != null && !subscriptionsVO.getMqQueueName().isEmpty();
        subscriptionsTypes.forEach(action -> {
            action.setDisabled(false);
        });
    }

    public void republishSubscriptions() {
        Logger.getLogger(SubscriptionBean.class.getName()).log(Level.INFO, "republishSubscriptions");
        KafkaProxyClient.getInstance().postSubscriptionsToKafka(ServiceRegistrarClient.getInstance().getServiceHostURL(KAFKA_PROXY_SERVICE_NAME), subscriptionsVOs);
    }

    public void cancleSubscriptions() {
        Logger.getLogger(SubscriptionBean.class.getName()).log(Level.INFO, "cancleSubscriptions");
        SubscriptionsClient.getInstance().cancelSubscriptions(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), subscriptionsVOs.get(0));
        subscriptionsVOs = new ArrayList<>();
    }

    public void updateBean() {
        Logger.getLogger("update bean called ");
        if (subscriptionsVO != null) {
            Logger.getLogger(SubscriptionBean.class.getName()).log(Level.INFO, "updateBean", subscriptionsVO.getMqProtocol());
            if (subscriptionsVO.isInternal() && (subscriptionsVO.getWebhookUrl() == null || subscriptionsVO.getWebhookUrl().isEmpty())) {
                uptimeHostedSelected = true;
                subscriptionsVO.setMqProtocol("MQTT");
            } else {
                uptimeHostedSelected = false;
            }
            subscriptionTypeSelected = subscriptionsVO.getSubscriptionType() != null && !subscriptionsVO.getSubscriptionType().isEmpty();
            wavehookUrlData = subscriptionsVO.getWebhookUrl() != null && !subscriptionsVO.getWebhookUrl().isEmpty();
            mqTopicNameProvided = subscriptionsVO.getMqQueueName() != null && !subscriptionsVO.getMqQueueName().isEmpty();
            Logger.getLogger(SubscriptionBean.class.getName()).log(Level.INFO, "updateBean {0}", mqTopicNameProvided);
        }

    }

    public SubscriptionsVO getSubscriptionVoObj() {
        SubscriptionsVO subscriptionObj = new SubscriptionsVO();
        subscriptionObj.setSiteId(subscriptionsVO.getSiteId());
        subscriptionObj.setCustomerAccount(subscriptionsVO.getCustomerAccount());
        if (subscriptionTypeSelected && wavehookUrlData) {
            subscriptionObj.setSubscriptionType(subscriptionsVO.getSubscriptionType());
            subscriptionObj.setWebhookUrl(subscriptionsVO.getWebhookUrl());
        } else if (subscriptionTypeSelected && !wavehookUrlData && uptimeHostedSelected) {
            subscriptionObj.setSubscriptionType(subscriptionsVO.getSubscriptionType());
            subscriptionObj.setInternal(subscriptionsVO.isInternal());
            subscriptionObj.setMqProtocol(subscriptionsVO.getMqProtocol());
            subscriptionObj.setMqQueueName(subscriptionsVO.getMqQueueName());
        } else if (subscriptionTypeSelected && !wavehookUrlData && !uptimeHostedSelected) {
            subscriptionObj.setSubscriptionType(subscriptionsVO.getSubscriptionType());
            subscriptionObj.setInternal(subscriptionsVO.isInternal());
            subscriptionObj.setMqProtocol(subscriptionsVO.getMqProtocol());
            subscriptionObj.setMqConnectString(subscriptionsVO.getMqConnectString());
            subscriptionObj.setMqUser(subscriptionsVO.getMqUser());
            subscriptionObj.setMqPwd(subscriptionsVO.getMqPwd());
            subscriptionObj.setMqQueueName(subscriptionsVO.getMqQueueName());
            subscriptionObj.setMqClientId(subscriptionsVO.getMqClientId());
        }
        
        return subscriptionObj;
    }

    public List<SubscriptionsVO> getSubscriptionsVOs() {
        return subscriptionsVOs;
    }

    public void setSubscriptionsVOs(List<SubscriptionsVO> subscriptionsVOs) {
        this.subscriptionsVOs = subscriptionsVOs;
    }

    public SubscriptionsVO getSubscriptionsVO() {
        return subscriptionsVO;
    }

    public void setSubscriptionsVO(SubscriptionsVO subscriptionsVO) {
        this.subscriptionsVO = subscriptionsVO;
    }

    public List<UserSitesVO> getSiteList() {
        return siteList;
    }

    public void setSiteList(List<UserSitesVO> siteList) {
        this.siteList = siteList;
    }

    public List<SelectItem> getSubscriptionsTypes() {
        return subscriptionsTypes;
    }

    public List<SelectItem> getMqProtocol() {
        return mqProtocol;
    }

    public boolean isUptimeHostedSelected() {
        return uptimeHostedSelected;
    }

    public void setUptimeHostedSelected(boolean uptimeHostedSelected) {
        this.uptimeHostedSelected = uptimeHostedSelected;
    }

    public boolean isWavehookUrlData() {
        return wavehookUrlData;
    }

    public void setWavehookUrlData(boolean wavehookUrlData) {
        this.wavehookUrlData = wavehookUrlData;
    }

    public boolean isSubscriptionTypeSelected() {
        return subscriptionTypeSelected;
    }

    public void setSubscriptionTypeSelected(boolean subscriptionTypeSelected) {
        this.subscriptionTypeSelected = subscriptionTypeSelected;
    }

    public boolean isMqTopicNameProvided() {
        return mqTopicNameProvided;
    }

    public void setMqTopicNameProvided(boolean mqTopicNameProvided) {
        this.mqTopicNameProvided = mqTopicNameProvided;
    }

}
