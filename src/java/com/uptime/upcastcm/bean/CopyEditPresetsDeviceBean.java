/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.APALSetClient;
import com.uptime.upcastcm.http.client.DevicePresetClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import static com.uptime.upcastcm.utils.enums.ACSensorTypeEnum.getACSensorTypeItemList;
import static com.uptime.upcastcm.utils.enums.DCSensorTypeEnum.getDCSensorTypeItemList;
import static com.uptime.upcastcm.utils.enums.DeviceTypeEnum.getDeviceTypeItemList;
import static com.uptime.upcastcm.utils.enums.ParamUnitEnum.getACParamUnitItemListBySensorTypeParamType;
import static com.uptime.upcastcm.utils.enums.ParamUnitEnum.getDCParamUnitItemListBySensorType;
import static com.uptime.upcastcm.utils.enums.ParamUnitEnum.getFrequencyPlusUnits;
import com.uptime.upcastcm.utils.helperclass.Filters;
import com.uptime.upcastcm.utils.helperclass.GenericMessage;
import static com.uptime.upcastcm.utils.helperclass.GenericMessage.setNonAutoGrowlMsg;
import com.uptime.upcastcm.utils.helperclass.Utils;
import com.uptime.upcastcm.utils.interfaces.PresetDialogFields;
import com.uptime.upcastcm.utils.singletons.CommonUtilSingleton;
import com.uptime.upcastcm.utils.vo.ApAlSetVO;
import com.uptime.upcastcm.utils.vo.DevicePresetVO;
import com.uptime.upcastcm.utils.vo.DeviceVO;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import org.primefaces.PrimeFaces;

/**
 *
 * @author Joseph
 */
public class CopyEditPresetsDeviceBean implements Serializable, PresetDialogFields {
    private List<DevicePresetVO> devicePresetsVOList;
    private boolean validationFailed = false, disable = false;
    private String setName, selectedDeviceType;
    private List<String> deviceTypeList;
    private DevicePresetVO devicePresetVO;
    private int operationType;
    private UserManageBean userManageBean;
    private Map<String, List<ApAlSetVO>> deviceTypeMap;
    private Map<String, List<SelectItem>> selectDeviceTypeMap;
    private List<SelectItem> dcSensorTypeList, acSensorTypeList;

    /**
     * Constructor
     */
    public CopyEditPresetsDeviceBean() {
        devicePresetsVOList = new ArrayList();
        deviceTypeMap = new HashMap();
        selectDeviceTypeMap = new HashMap();

        try {
            userManageBean = ((UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean"));

            //***************************For v1.0********************************
            // deviceTypeList = getDeviceTypeItemList();
            // For v1.0 only "MistLX" will be used.
            // For v1.5 adding "StormX".
            deviceTypeList = new ArrayList();
            deviceTypeList.add("MistLX");
            deviceTypeList.add("StormX");

            //*******************************************************************
            dcSensorTypeList = getDCSensorTypeItemList();
            acSensorTypeList = getACSensorTypeItemList();
        } catch (Exception e) {
            Logger.getLogger(CopyEditPresetsDeviceBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            userManageBean = null;
        }
    }

    /**
     * Post Constructor
     */
    @PostConstruct
    public void init() {
        resetPage();
    }

    @Override
    public void resetPage() {
        devicePresetsVOList = new ArrayList();
        deviceTypeMap = new HashMap();
        selectDeviceTypeMap = new HashMap();
        operationType = -1;
        setName = null;
        selectedDeviceType = null;
        devicePresetVO = new DevicePresetVO();
    }

    @Override
    public void presetFields(Filters presets, Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void presetFields(int operationType, Object object) {
        if (userManageBean != null && userManageBean.getCurrentSite() != null && object != null && object instanceof DevicePresetVO) {
            this.operationType = operationType;
            devicePresetVO = new DevicePresetVO((DevicePresetVO) object);
            setName = this.operationType != 2 ? devicePresetVO.getName() : null;
            disable = devicePresetVO.isDisabled();
            selectedDeviceType = devicePresetVO.getDeviceType();
            Logger.getLogger(CreatePresetsDeviceBean.class.getName()).log(Level.INFO, "selectedDeviceType *********** : {0}", selectedDeviceType);
            onDeviceTypeChange();
        } else {
            resetPage();
        }
    }

    @Override
    public void presetFields(Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void updateBean() {
        Logger.getLogger(CopyEditPresetsDeviceBean.class.getName()).log(Level.INFO, "CopyEditPresetsDeviceBean.updateBean() called. ***********");
    }

    public void onDeviceTypeChange() {
        List<DevicePresetVO> newDevicePresetsVOList;
        List<DeviceVO> deviceVOs;

        if (userManageBean != null) {
            try {
                devicePresetsVOList.clear();
                deviceTypeMap.clear();
                selectDeviceTypeMap.clear();

                deviceVOs = CommonUtilSingleton.getInstance().populateDeviceTypeList(this.selectedDeviceType);
                if (devicePresetVO != null && devicePresetVO.getName() != null && devicePresetVO.getSiteId() != null && devicePresetVO.getType().equalsIgnoreCase("Site")) {
                    devicePresetsVOList.addAll(DevicePresetClient.getInstance().getSiteDevicePresetVOs(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), devicePresetVO.getCustomerAccount(), devicePresetVO.getSiteId(), devicePresetVO.getDeviceType(), devicePresetVO.getPresetId()));
                } else if (devicePresetVO != null) { // Global
                    if (!userManageBean.getCurrentSite().getCustomerAccount().equals(DEFAULT_UPTIME_ACCOUNT)) {
                        devicePresetsVOList.addAll(DevicePresetClient.getInstance().getGlobalDevicePresetVOs(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), DEFAULT_UPTIME_ACCOUNT, devicePresetVO.getDeviceType(), devicePresetVO.getPresetId()));
                    }
                    devicePresetsVOList.addAll(DevicePresetClient.getInstance().getGlobalDevicePresetVOs(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), devicePresetVO.getCustomerAccount(), devicePresetVO.getDeviceType(), devicePresetVO.getPresetId()));
                }

                newDevicePresetsVOList = new ArrayList();
                deviceVOs.stream().map(deviceVO -> {
                    List<ApAlSetVO> apAlSetVOList, totalApAlSetVOList;
                    DevicePresetVO dpvo;

                    dpvo = devicePresetsVOList.stream().filter(dpv -> (byte) deviceVO.getChannelNum() == dpv.getChannelNumber() && deviceVO.getChannelType().equalsIgnoreCase(dpv.getChannelType())).findAny().orElse(new DevicePresetVO());
                    dpvo.setChannelNumberLabel(deviceVO.getChannelNumLabel());
                    dpvo.setChannelType(deviceVO.getChannelType());
                    dpvo.setChannelNumber((byte) deviceVO.getChannelNum());
                    dpvo.setRestrictions(deviceVO.getRestrictions());
                    dpvo.setApAlSetVOList(new ArrayList());
                    dpvo.setApAlSetSelectItemList(new ArrayList());

                    if (!selectedDeviceType.equalsIgnoreCase("TS1X")) {
                        dpvo.setSensorType(deviceVO.getRestrictions());

                        if (deviceTypeMap.containsKey(dpvo.getSensorType())) {
                            dpvo.getApAlSetVOList().addAll(deviceTypeMap.get(dpvo.getSensorType()));
                            dpvo.getApAlSetSelectItemList().addAll(selectDeviceTypeMap.get(dpvo.getSensorType()));
                        } else {
                            totalApAlSetVOList = null;

                            // Get Uptime ApAl Presets
                            if (!userManageBean.getCurrentSite().getCustomerAccount().equals(DEFAULT_UPTIME_ACCOUNT) && (apAlSetVOList = APALSetClient.getInstance().getGlobalApAlSetsByCustomerSensorType(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), DEFAULT_UPTIME_ACCOUNT, deviceVO.getRestrictions())) != null && !apAlSetVOList.isEmpty()) {
                                totalApAlSetVOList = new ArrayList();
                                totalApAlSetVOList.addAll(apAlSetVOList);
                            }

                            // Get Global ApAl Presets
                            if ((apAlSetVOList = APALSetClient.getInstance().getGlobalApAlSetsByCustomerSensorType(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), deviceVO.getRestrictions())) != null && !apAlSetVOList.isEmpty()) {
                                if (totalApAlSetVOList == null) {
                                    totalApAlSetVOList = new ArrayList();
                                }
                                totalApAlSetVOList.addAll(apAlSetVOList);
                            }

                            if (totalApAlSetVOList != null && !totalApAlSetVOList.isEmpty()) {
                                dpvo.getApAlSetVOList().addAll(totalApAlSetVOList);
                                dpvo.getApAlSetSelectItemList().addAll(Utils.groupApSetList(Utils.getUniqueApAlSetList(totalApAlSetVOList), userManageBean.getResourceBundleString("label_global_apset")));
                            }

                            // Get Site ApAl Presets
                            if (devicePresetVO.getSiteId() != null && (apAlSetVOList = APALSetClient.getInstance().getSiteApAlSetsByCustomerSiteIdSensorType(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), devicePresetVO.getCustomerAccount(), devicePresetVO.getSiteId(), deviceVO.getRestrictions())) != null && !apAlSetVOList.isEmpty()) {
                                dpvo.getApAlSetVOList().addAll(apAlSetVOList);
                                dpvo.getApAlSetSelectItemList().addAll(Utils.groupApSetList(Utils.getUniqueApAlSetList(apAlSetVOList), userManageBean.getResourceBundleString("label_site_apset")));
                            }

                            deviceTypeMap.put(dpvo.getSensorType(), dpvo.getApAlSetVOList());
                            selectDeviceTypeMap.put(dpvo.getSensorType(), dpvo.getApAlSetSelectItemList());
                        }
                    } else {
                        onSensorTypeChange(dpvo);
                    }

                    return dpvo;
                }).forEachOrdered(dpvo -> {
                    onApSetNameSelect(dpvo);
                    newDevicePresetsVOList.add(dpvo);
                });
                devicePresetsVOList.clear();
                devicePresetsVOList.addAll(newDevicePresetsVOList);
            } catch (Exception e) {
                Logger.getLogger(CopyEditPresetsDeviceBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
                devicePresetsVOList.clear();
                deviceTypeMap.clear();
                selectDeviceTypeMap.clear();
            }
        }
    }

    public void sensorTypeChange(DevicePresetVO dpvo) {
        SelectItem item;
        
        if (dpvo.getSensorType() != null && !dpvo.getSensorType().trim().isEmpty()) {
            if (selectedDeviceType.equalsIgnoreCase("TS1X")) {
                item = CommonUtilSingleton.getInstance().populateSensitivityAndOffsetValuesForTS1X(dpvo.getChannelType(), dpvo.getSensorType());
            } else {
                item = CommonUtilSingleton.getInstance().populateSensitivityAndOffsetValues(selectedDeviceType, dpvo.getChannelType(), dpvo.getChannelNumber());
            }
            dpvo.setSensorOffset(Float.valueOf(item.getValue().toString()));
            dpvo.setSensorUnits(item.getDescription());
            dpvo.setSensorSensitivity(Float.valueOf(item.getLabel()));
        }
        onSensorTypeChange(dpvo);
    }

    /**
     * perform the given action when sensorType field is changed
     *
     * @param dpvo
     */
    public void onSensorTypeChange(DevicePresetVO dpvo) {
        List<ApAlSetVO> apAlSetVOList, totalApAlSetVOList;

        if (userManageBean != null && dpvo != null) {
            try {
                dpvo.setApAlSetVOList(new ArrayList());
                dpvo.setApAlSetSelectItemList(new ArrayList());
                if (dpvo.getSensorType() != null && !dpvo.getSensorType().trim().isEmpty()) {
                    if (deviceTypeMap.containsKey(dpvo.getSensorType())) {
                        dpvo.getApAlSetVOList().addAll(deviceTypeMap.get(dpvo.getSensorType()));
                        dpvo.getApAlSetSelectItemList().addAll(selectDeviceTypeMap.get(dpvo.getSensorType()));
                    } else {
                        totalApAlSetVOList = null;

                        // Get Uptime ApAl Presets
                        if (!userManageBean.getCurrentSite().getCustomerAccount().equals(DEFAULT_UPTIME_ACCOUNT) && (apAlSetVOList = APALSetClient.getInstance().getGlobalApAlSetsByCustomerSensorType(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), DEFAULT_UPTIME_ACCOUNT, dpvo.getSensorType())) != null && !apAlSetVOList.isEmpty()) {
                            totalApAlSetVOList = new ArrayList();
                            totalApAlSetVOList.addAll(apAlSetVOList);
                        }

                        // Get Global ApAl Presets
                        if ((apAlSetVOList = APALSetClient.getInstance().getGlobalApAlSetsByCustomerSensorType(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), dpvo.getSensorType())) != null && !apAlSetVOList.isEmpty()) {
                            if (totalApAlSetVOList == null) {
                                totalApAlSetVOList = new ArrayList();
                            }
                            totalApAlSetVOList.addAll(apAlSetVOList);
                        }

                        if (totalApAlSetVOList != null && !totalApAlSetVOList.isEmpty()) {
                            dpvo.getApAlSetVOList().addAll(apAlSetVOList);
                            dpvo.getApAlSetSelectItemList().addAll(Utils.groupApSetList(Utils.getUniqueApAlSetList(apAlSetVOList), userManageBean.getResourceBundleString("label_global_apset")));
                        }

                        // Get Site ApAl Presets
                        if (devicePresetVO.getSiteId() != null && (apAlSetVOList = APALSetClient.getInstance().getSiteApAlSetsByCustomerSiteIdSensorType(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), devicePresetVO.getCustomerAccount(), devicePresetVO.getSiteId(), dpvo.getSensorType())) != null && !apAlSetVOList.isEmpty()) {
                            dpvo.getApAlSetVOList().addAll(apAlSetVOList);
                            dpvo.getApAlSetSelectItemList().addAll(Utils.groupApSetList(Utils.getUniqueApAlSetList(apAlSetVOList), userManageBean.getResourceBundleString("label_site_apset")));
                        }
                        deviceTypeMap.put(dpvo.getSensorType(), dpvo.getApAlSetVOList());
                        selectDeviceTypeMap.put(dpvo.getSensorType(), dpvo.getApAlSetSelectItemList());
                    }
                } else {
                    dpvo.setApSetId(null);
                    dpvo.setAlSetId(null);
                    dpvo.setSensorOffset(Float.valueOf("0"));
                    dpvo.setSensorUnits(null);
                    dpvo.setSensorSensitivity(Float.valueOf("0"));
                }
            } catch (Exception e) {
                Logger.getLogger(CopyEditPresetsDeviceBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
                dpvo.setApSetId(null);
                dpvo.setAlSetId(null);
                dpvo.setSensorOffset(Float.valueOf("0"));
                dpvo.setSensorUnits(null);
                dpvo.setSensorSensitivity(Float.valueOf("0"));
            }
        }
    }

    public void onApSetNameSelect(DevicePresetVO devicePresetsVO) {
        UUID apsetId;

        if (devicePresetsVO != null) {
            apsetId = devicePresetsVO.getApSetId();
            devicePresetsVO.setAlSetVOList(new ArrayList());
            if (apsetId != null) {
                devicePresetsVO.getAlSetVOList().addAll(devicePresetsVO.getApAlSetVOList().stream().filter(ap -> ap.getApSetId().compareTo(apsetId) == 0).collect(Collectors.toList()));
            }
        }
    }

    public List<SelectItem> getACParamUnitList(DevicePresetVO devicePresetVO) {
        if (devicePresetVO != null && devicePresetVO.getSensorType() != null) {
            return getACParamUnitItemListBySensorTypeParamType(devicePresetVO.getSensorType(), "waveform rms");
        }
        return null;
    }

    public List<SelectItem> getDCParamUnitList(DevicePresetVO devicePresetVO) {
        if (devicePresetVO != null) {
            return getDCParamUnitItemListBySensorType(devicePresetVO.getSensorType());
        }
        return null;
    }

    public void addDevicePreset() {
        devicePresetsVOList.add(new DevicePresetVO());
    }

    public void deleteApAlRow(DevicePresetVO devicePresetVO) {
        if (devicePresetsVOList.size() > 1) {
            devicePresetsVOList.remove(devicePresetVO);
        }
    }

    public void submit() {
        UUID presetId;

        validationFailed = false;
        devicePresetsVOList = devicePresetsVOList.stream().filter(p -> p.getSensorType() != null && !p.getSensorType().trim().isEmpty()).collect(Collectors.toList());
        if (devicePresetsVOList == null || devicePresetsVOList.isEmpty()) {
            GenericMessage.setNonAutoGrowlMsg("growl_validate_for_device_presets");
            return;
        }

        devicePresetsVOList.stream().forEach(vo -> {
            if (this.setName == null || this.setName.trim().isEmpty() || this.selectedDeviceType == null) {
                validationFailed = true;
            }
            if (!validationFailed && devicePresetsVOList.stream().filter(p -> p.getApSetId() == null || p.getAlSetId() == null).findAny().isPresent()) {
                validationFailed = true;
            }
        });

        if (validationFailed) {
            GenericMessage.setNonAutoGrowlMsg("growl_validate_message_for_device_presets");
        } else if (userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null) {

            // For Copy
            if (this.operationType == 2) {
                presetId = UUID.randomUUID();
                devicePresetsVOList.stream().forEachOrdered(vo -> {
                    vo.setPresetId(presetId);
                    vo.setCustomerAccount(userManageBean.getCurrentSite().getCustomerAccount());
                    vo.setSiteId(userManageBean.getCurrentSite().getSiteId());
                    vo.setSiteName(userManageBean.getCurrentSite().getSiteName());
                    vo.setName(setName);
                    vo.setDeviceType(selectedDeviceType);
                    vo.setDisabled(disable);
                });

                if (DevicePresetClient.getInstance().createDevicePresetVO(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), devicePresetsVOList)) {
                    setNonAutoGrowlMsg("growl_create_item_items");
                    Logger.getLogger(CreatePresetsDeviceBean.class.getName()).log(Level.INFO, "Site Device Presets created successfully.");
                } else {
                    Logger.getLogger(CreatePresetsDeviceBean.class.getName()).log(Level.INFO, "Error in creating Site Device Presets.");
                }
            } // For Edit
            else {
                presetId = devicePresetsVOList.stream().filter(p -> p.getPresetId() != null).map(DevicePresetVO::getPresetId).findAny().orElse(null);
                devicePresetsVOList.stream().forEachOrdered(vo -> {
                    vo.setPresetId(presetId);
                    vo.setCustomerAccount(userManageBean.getCurrentSite().getCustomerAccount());
                    vo.setSiteId(userManageBean.getCurrentSite().getSiteId());
                    vo.setSiteName(userManageBean.getCurrentSite().getSiteName());
                    vo.setName(setName);
                    vo.setDeviceType(selectedDeviceType);
                    vo.setDisabled(disable);
                });

                if (DevicePresetClient.getInstance().updateSiteDevicePresetVO(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), devicePresetsVOList)) {
                    setNonAutoGrowlMsg("growl_update_item_items");
                    Logger.getLogger(CreatePresetsDeviceBean.class.getName()).log(Level.INFO, "Site Device Presets updated successfully.");
                } else {
                    Logger.getLogger(CreatePresetsDeviceBean.class.getName()).log(Level.INFO, "Error in updating Site Device Presets.");
                }
            }

            ((PresetsBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("presetsBean")).resetDevicePresetsTab();
            PrimeFaces.current().ajax().update("presetsTabViewId:devicesFormId");
            PrimeFaces.current().executeScript("PF('defaultDlg').hide()");
        }
    }

    public List<SelectItem> getFrequencyUnitsList() {
        return getFrequencyPlusUnits();
    }

    public List<DevicePresetVO> getDevicePresetsVOList() {
        return devicePresetsVOList;
    }

    public List<String> getDeviceTypeList() {
        return deviceTypeList;
    }

    public boolean isDisable() {
        return disable;
    }

    public void setDisable(boolean disable) {
        this.disable = disable;
    }

    public boolean isValidationFailed() {
        return validationFailed;
    }

    public String getSetName() {
        return setName;
    }

    public void setSetName(String setName) {
        this.setName = setName;
    }

    public int getOperationType() {
        return operationType;
    }

    public void setOperationType(int operationType) {
        this.operationType = operationType;
    }

    public String getSelectedDeviceType() {
        return selectedDeviceType;
    }

    public void setSelectedDeviceType(String selectedDeviceType) {
        this.selectedDeviceType = selectedDeviceType;
    }

    public DevicePresetVO getDevicePresetVO() {
        return devicePresetVO;
    }

    public void setDevicePresetVO(DevicePresetVO devicePresetVO) {
        this.devicePresetVO = devicePresetVO;
    }

    public void setDevicePresetsVOList(List<DevicePresetVO> devicePresetsVOList) {
        this.devicePresetsVOList = devicePresetsVOList;
    }

    public List<SelectItem> getDcSensorTypeList() {
        return dcSensorTypeList;
    }

    public List<SelectItem> getAcSensorTypeList() {
        return acSensorTypeList;
    }
}
