/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.AlarmsClient;
import com.uptime.upcastcm.http.client.AreaClient;
import com.uptime.upcastcm.http.client.AssetClient;
import com.uptime.upcastcm.http.client.PointLocationClient;
import com.uptime.upcastcm.http.client.WorkOrderClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import com.uptime.upcastcm.utils.vo.AreaVO;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;

/**
 *
 * @author twilcox
 */
public class HomeBean implements Serializable {
    private UserManageBean userManageBean;
    private int areaCount, assetCount, pointLocationCount, alarmCount, alertCount, faultCount, workOrderCount, priority1Count, priority2Count, priority3Count, priority4Count;
    private boolean siteSelected;

    /**
     * Creates a new instance of HomeBean
     */
    public HomeBean() {
        try {
            userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
        } catch (Exception e) {
            Logger.getLogger(HomeBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            userManageBean = null;
        }
    }

    /**
     * Post Constructor
     */
    @PostConstruct
    public void init() {
        resetPage();
    }

    public void resetPage() {
        List<AreaVO> areaVOList;
        
        areaCount = 0;
        assetCount = 0;
        pointLocationCount = 0;
        alarmCount = 0;
        alertCount = 0; 
        faultCount = 0;
        workOrderCount = 0;
        priority1Count = 0; 
        priority2Count = 0; 
        priority3Count = 0; 
        priority4Count = 0;
        siteSelected = false;
        
        if (userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null && userManageBean.getCurrentSite().getSiteId() != null) {
            siteSelected = true;
            
            // Get area count
            try {
                areaVOList = AreaClient.getInstance().getAreaSiteByCustomerSite(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), userManageBean.getCurrentSite().getSiteId());
                areaCount = areaVOList.size();
            } catch (Exception e) {
                Logger.getLogger(HomeBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
                areaVOList = new ArrayList();
                areaCount = 0;
            }
            
            if (areaCount > 0) {
                
                // Get asset count
                try {
                    assetCount = AssetClient.getInstance().getAssetSiteAreaByCustomerSite(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), userManageBean.getCurrentSite().getSiteId().toString()).size();
                } catch (Exception e) {
                    Logger.getLogger(HomeBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
                    assetCount = 0;
                }
                
                if (assetCount > 0) {
                    
                    // Get point location count
                    try {
                        for (AreaVO areaVO : areaVOList) {
                            pointLocationCount += PointLocationClient.getInstance().getPointLocationsByCustomerSiteArea(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), userManageBean.getCurrentSite().getSiteId(), areaVO.getAreaId()).size();
                        }
                    } catch (Exception e) {
                        Logger.getLogger(HomeBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
                        pointLocationCount = 0;
                    }
                    
                    // Get work order count
                    try {
                        WorkOrderClient.getInstance().getWorkOrderCountsByCustomerSite(ServiceRegistrarClient.getInstance().getServiceHostURL(WORK_ORDER_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), userManageBean.getCurrentSite().getSiteId()).forEach(workOrderVO -> {
                            workOrderCount += workOrderVO.getTotalCount();
                            priority1Count += workOrderVO.getPriority1(); 
                            priority2Count += workOrderVO.getPriority2(); 
                            priority3Count += workOrderVO.getPriority3(); 
                            priority4Count += workOrderVO.getPriority4();
                        });
                    } catch (Exception e) {
                        Logger.getLogger(HomeBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
                        workOrderCount = 0;
                        priority1Count = 0; 
                        priority2Count = 0; 
                        priority3Count = 0; 
                        priority4Count = 0;
                    }
                    
                    if (pointLocationCount > 0) {
                    
                        // Get alarm count
                        try {
                            AlarmsClient.getInstance().getAlarmCountByCustomerSite(ServiceRegistrarClient.getInstance().getServiceHostURL(ALARM_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), userManageBean.getCurrentSite().getSiteId()).forEach(alarmsVO -> {
                                alarmCount += alarmsVO.getAlarmCount();
                                alertCount += alarmsVO.getAlertCount();
                                faultCount += alarmsVO.getFaultCount();
                            });
                        } catch (Exception e) {
                            Logger.getLogger(HomeBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
                            alarmCount = 0;
                            alertCount = 0; 
                            faultCount = 0;
                        }
                    }
                }
            }
        }
    }

    public int getAreaCount() {
        return areaCount;
    }

    public int getAssetCount() {
        return assetCount;
    }

    public int getPointLocationCount() {
        return pointLocationCount;
    }

    public int getAlarmCount() {
        return alarmCount;
    }

    public int getAlertCount() {
        return alertCount;
    }

    public int getFaultCount() {
        return faultCount;
    }

    public int getWorkOrderCount() {
        return workOrderCount;
    }

    public int getPriority1Count() {
        return priority1Count;
    }

    public int getPriority2Count() {
        return priority2Count;
    }

    public int getPriority3Count() {
        return priority3Count;
    }

    public int getPriority4Count() {
        return priority4Count;
    }

    public boolean isSiteSelected() {
        return siteSelected;
    }
}
