/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.APALSetClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import com.uptime.upcastcm.utils.helperclass.Utils;
import static com.uptime.upcastcm.utils.helperclass.Utils.distinctByKeys;
import com.uptime.upcastcm.utils.vo.ApAlSetVO;
import com.uptime.upcastcm.utils.vo.DevicePresetVO;
import com.uptime.upcastcm.utils.vo.PointLocationVO;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.primefaces.PrimeFaces;

/**
 *
 * @author kpati
 */
public class EditInstallDevicePresetsBean implements Serializable {

    private UserManageBean userManageBean;

    private PointLocationVO pointLocationVO;
    private DevicePresetVO devicePresetVO;
    private boolean fromInstallDevice, failedTachValidation;

    public EditInstallDevicePresetsBean() {
        pointLocationVO = null;
        devicePresetVO = null;

        try {
            userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
        } catch (Exception e) {
            Logger.getLogger(EditInstallDevicePresetsBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            userManageBean = null;
        }
    }

    /**
     * Post Constructor
     */
    @PostConstruct
    public void init() {
        resetPage();
    }

    public void resetPage() {
        failedTachValidation = false;
        pointLocationVO = null;
        devicePresetVO = null;
    }

    /**
     * Preset fields needed for this bean
     *
     * @param devicePresetVO, DevicePresetVO Objects
     * @param fromInstallDevice
     * @@param pointLocationVO, PointLocationVO Objects
     */
    public void presetFields(DevicePresetVO devicePresetVO, PointLocationVO pointLocationVO, boolean fromInstallDevice) {
        List<ApAlSetVO> apAlSetVOList, totalApAlSetVOList;

        resetPage();
        if (devicePresetVO != null && pointLocationVO != null) {
            this.pointLocationVO = new PointLocationVO(pointLocationVO);
            this.fromInstallDevice = fromInstallDevice;
            if (userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null && userManageBean.getCurrentSite().getSiteId() != null) {
                this.devicePresetVO = new DevicePresetVO(devicePresetVO);
                this.devicePresetVO.setApAlSetVOList(new ArrayList());
                this.devicePresetVO.setApAlSetSelectItemList(new ArrayList());

                try {
                    totalApAlSetVOList = null;

                    // Get Uptime ApAl Presets
                    if (!userManageBean.getCurrentSite().getCustomerAccount().equals(DEFAULT_UPTIME_ACCOUNT) && (apAlSetVOList = APALSetClient.getInstance().getGlobalApAlSetsAllByCustomerSensorType(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), DEFAULT_UPTIME_ACCOUNT, this.devicePresetVO.getSensorType())) != null && !apAlSetVOList.isEmpty()) {
                        totalApAlSetVOList = new ArrayList();
                        totalApAlSetVOList.addAll(apAlSetVOList);
                    }

                    // Get Global ApAl Presets
                    if ((apAlSetVOList = APALSetClient.getInstance().getGlobalApAlSetsAllByCustomerSensorType(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), this.devicePresetVO.getSensorType())) != null && !apAlSetVOList.isEmpty()) {
                        if (totalApAlSetVOList == null) {
                            totalApAlSetVOList = new ArrayList();
                        }
                        totalApAlSetVOList.addAll(apAlSetVOList);
                    }

                    if (totalApAlSetVOList != null && !totalApAlSetVOList.isEmpty()) {
                        this.devicePresetVO.getApAlSetVOList().addAll(totalApAlSetVOList);
                        this.devicePresetVO.getApAlSetSelectItemList().addAll(Utils.groupApSetList(Utils.getUniqueApAlSetList(totalApAlSetVOList), userManageBean.getResourceBundleString("label_global_apset")));
                    }

                    // Get Site ApAl Presets
                    if ((apAlSetVOList = APALSetClient.getInstance().getSiteApAlSetsAllByCustomerSiteIdSensorType(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), userManageBean.getCurrentSite().getSiteId(), this.devicePresetVO.getSensorType())) != null && !apAlSetVOList.isEmpty()) {
                        this.devicePresetVO.getApAlSetVOList().addAll(apAlSetVOList);
                        this.devicePresetVO.getApAlSetSelectItemList().addAll(Utils.groupApSetList(Utils.getUniqueApAlSetList(apAlSetVOList), userManageBean.getResourceBundleString("label_site_apset")));
                    }

                    onApSetNameSelect(false);
                } catch (Exception e) {
                    Logger.getLogger(EditInstallDevicePresetsBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
                    this.devicePresetVO.setApAlSetSelectItemList(new ArrayList());
                    this.devicePresetVO.setApAlSetVOList(new ArrayList());
                }
            }
        }
    }

    public void updateBean() {
        Logger.getLogger(EditInstallDevicePresetsBean.class.getName()).log(Level.INFO, "*EditInstallDevicePresetsBean*{0}", "updateBean Called**");
    }

    /**
     * perform the given action when an ApSet Name
     *
     * @param clearAlSet, boolean, if true clear AlSetId and AlSetName, else do not
     */
    public void onApSetNameSelect(boolean clearAlSet) {
        ApAlSetVO selectedApSet;
        List<ApAlSetVO> tmpList;

        if (devicePresetVO != null && pointLocationVO != null) {
            devicePresetVO.setApSetName(null);
            devicePresetVO.setAlSetVOList(null);

            if (clearAlSet) {
                devicePresetVO.setAlSetId(null);
                devicePresetVO.setAlSetName(null);
            }

            try {
                if (devicePresetVO.getApAlSetVOList() != null && !devicePresetVO.getApAlSetVOList().isEmpty() && devicePresetVO.getApSetId() != null) {
                    if ((selectedApSet = devicePresetVO.getApAlSetVOList().stream().filter(a -> a.getApSetId().compareTo(devicePresetVO.getApSetId()) == 0).distinct().findFirst().orElse(null)) != null) {
                        devicePresetVO.setApSetName(selectedApSet.getApSetName());
                    }
                    
                    //Check if AP Set requires a tach and if a tach is provided
                    failedTachValidation = false;
                    tmpList = new ArrayList();
                    for (ApAlSetVO apAlSetVO : devicePresetVO.getApAlSetVOList().stream().filter(ap -> ap.getApSetId() != null && ap.getApSetId().equals(devicePresetVO.getApSetId())).collect(Collectors.toList())) {
                        if ((apAlSetVO.getFrequencyUnits() != null && apAlSetVO.getFrequencyUnits().equalsIgnoreCase("Orders")) || Utils.validateParamList().contains(apAlSetVO.getParamType())) {
                            failedTachValidation = pointLocationVO.getTachId() == null;
                            
                            if (failedTachValidation) {
                                break;
                            }
                        }
                        
                        tmpList.add(apAlSetVO);
                    }
                    
                    if (!failedTachValidation) {
                        devicePresetVO.setAlSetVOList(new ArrayList());
                        devicePresetVO.getAlSetVOList().addAll(tmpList.stream().filter(ap -> ap.getApSetId() != null && ap.getApSetId().equals(devicePresetVO.getApSetId())).filter(distinctByKeys(ApAlSetVO::getAlSetId)).collect(Collectors.toList()));
                    }
                }
            } catch (Exception e) {
                Logger.getLogger(EditInstallDevicePresetsBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            }
        }
    }

    public void submit() {
        if (fromInstallDevice) {
            if ((InstallDeviceBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("installDeviceBean") == null) {
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("installDeviceBean", new InstallDeviceBean());
            }
        } else {
            if ((CreatePointBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("createPointBean") == null) {
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("createPointBean", new CreatePointBean());
            }
        }
        if (devicePresetVO != null) {
            if (devicePresetVO.getPointName().isEmpty() || devicePresetVO.getPointName() == null || devicePresetVO.getPointName().length() == 0) {
                FacesContext.getCurrentInstance().addMessage("growlMessages", new FacesMessage(userManageBean.getResourceBundleString("message_point_name_required")));
            } else {
                for (ApAlSetVO vo : devicePresetVO.getAlSetVOList()) {
                    if (vo.getAlSetId().equals(devicePresetVO.getAlSetId())) {
                        devicePresetVO.setAlSetName(vo.getAlSetName());
                        break;
                    }
                }
                
                if (fromInstallDevice) {
                    devicePresetVO.setAlSetVOList(new ArrayList());
                    devicePresetVO.getAlSetVOList().addAll(devicePresetVO.getApAlSetVOList().stream().filter(ap -> ap.getApSetId() != null && ap.getApSetId().equals(devicePresetVO.getApSetId())).filter(ap -> ap.getAlSetId() != null && ap.getAlSetId().equals(devicePresetVO.getAlSetId())).collect(Collectors.toList()));
                    
                    devicePresetVO.setApAlSetVOList(null);
                    devicePresetVO.setApAlSetSelectItemList(null);
                    
                    ((InstallDeviceBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("installDeviceBean")).presetFields(devicePresetVO);
                } else {
                    devicePresetVO.setAlSetVOList(new ArrayList());
                    devicePresetVO.getAlSetVOList().addAll(devicePresetVO.getApAlSetVOList().stream().filter(ap -> ap.getApSetId() != null && ap.getApSetId().equals(devicePresetVO.getApSetId())).filter(ap -> ap.getAlSetId() != null && ap.getAlSetId().equals(devicePresetVO.getAlSetId())).collect(Collectors.toList()));
                    
                    devicePresetVO.setApAlSetVOList(null);
                    devicePresetVO.setApAlSetSelectItemList(null);
                    
                    ((CreatePointBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("createPointBean")).presetFields(devicePresetVO);
                }
            }
        }

        PrimeFaces.current().executeScript("PF('defaultSecondDlg').hide()");
    }

    public DevicePresetVO getDevicePresetVO() {
        return devicePresetVO;
    }

    public boolean isFailedTachValidation() {
        return failedTachValidation;
    }
}
