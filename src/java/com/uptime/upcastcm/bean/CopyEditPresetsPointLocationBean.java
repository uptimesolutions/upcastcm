/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.DevicePresetClient;
import com.uptime.upcastcm.http.client.FaultFrequenciesClient;
import com.uptime.upcastcm.http.client.TachometerClient;
import com.uptime.upcastcm.http.utils.json.dom.PointLocationDomJsonParser;
import com.uptime.upcastcm.utils.vo.AssetPresetPointLocationVO;
import com.uptime.upcastcm.utils.vo.TachometerVO;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import com.uptime.upcastcm.utils.enums.ACSensorTypeEnum;
import com.uptime.upcastcm.utils.enums.DCSensorTypeEnum;
import static com.uptime.upcastcm.utils.enums.RollDiameterUnitEnum.getRollDiameterUnitItemList;
import com.uptime.upcastcm.utils.helperclass.Utils;
import com.uptime.upcastcm.utils.singletons.CommonUtilSingleton;
import com.uptime.upcastcm.utils.vo.ApAlSetVO;
import com.uptime.upcastcm.utils.vo.AssetPresetVO;
import com.uptime.upcastcm.utils.vo.DevicePresetVO;
import com.uptime.upcastcm.utils.vo.DeviceVO;
import com.uptime.upcastcm.utils.vo.FaultFrequenciesVO;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.faces.model.SelectItemGroup;
import org.primefaces.PrimeFaces;

/**
 *
 * @author joseph
 */
public class CopyEditPresetsPointLocationBean implements Serializable {

    private UserManageBean userManageBean;
    private List<SelectItem> pointLocationNameList, tachometersList, faultFrequenciesList, rollDiameterUnitsList, deviceTypesList;
    private AssetPresetPointLocationVO assetPresetPointLocationVO, originalassetPresetPointLocationVO;
    private List<AssetPresetPointLocationVO> assetPresetPointLocationVOList;
    private List<FaultFrequenciesVO> siteFFSetFavorites;
    private String selectedDeviceType;
    private UUID selectedDevicePresetId;
    private List<DevicePresetVO> devicePresetsVOList;
    private Map<String, List<ApAlSetVO>> deviceTypeMap;
    private Map<String, List<SelectItem>> selectDeviceTypeMap;
    private List<String> deviceTypeList;
    private List<DevicePresetVO> devicePresetList, distinctDevicePresetList;
    private AssetPresetVO assetPresetVO;
    private int operationType;
    private List<SelectItem> list;

    /**
     * Creates a new instance of AddTrendBean
     */
    public CopyEditPresetsPointLocationBean() {

        try {
            pointLocationNameList = new ArrayList();
            tachometersList = new ArrayList();
            faultFrequenciesList = new ArrayList();
            devicePresetsVOList = new ArrayList();
            deviceTypeMap = new HashMap();
            selectDeviceTypeMap = new HashMap();
            devicePresetList = new ArrayList();
            //  filteredDevicePresetList = new ArrayList();
            distinctDevicePresetList = new ArrayList();
            deviceTypesList = new ArrayList();

            userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
            rollDiameterUnitsList = getRollDiameterUnitItemList();
            deviceTypeList = new ArrayList();
            deviceTypeList.add("MistLX");

        } catch (Exception e) {
            Logger.getLogger(CopyEditPresetsPointLocationBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            userManageBean = null;
        }
    }

    /**
     * Post Constructor
     */
    @PostConstruct
    public void init() {
        resetPage();
    }

    public void resetPage() {
        devicePresetsVOList = new ArrayList();
        deviceTypeMap = new HashMap();
        selectDeviceTypeMap = new HashMap();
        assetPresetPointLocationVO = new AssetPresetPointLocationVO();
        originalassetPresetPointLocationVO = new AssetPresetPointLocationVO();
        pointLocationNameList = new ArrayList();
        tachometersList = new ArrayList();
        faultFrequenciesList = new ArrayList();
        assetPresetPointLocationVOList = new ArrayList();
        selectedDeviceType = null;
        selectedDevicePresetId = null;
        devicePresetList.clear();
        deviceTypesList = new ArrayList();
        distinctDevicePresetList.clear();
    }

    public void presetFields(int operationType, AssetPresetPointLocationVO assetPresetPointLocationVO, AssetPresetVO assetPresetVO) {
        if (userManageBean != null && userManageBean.getCurrentSite() != null && assetPresetPointLocationVO != null) {
            resetPage();
            this.operationType = operationType;
            this.originalassetPresetPointLocationVO = assetPresetPointLocationVO;
            this.assetPresetPointLocationVO = new AssetPresetPointLocationVO(assetPresetPointLocationVO);
            siteFFSetFavorites = new ArrayList();
            this.assetPresetVO = assetPresetVO;

            if (assetPresetVO.getType().equalsIgnoreCase("site")) {
                pointLocationNameList.addAll(userManageBean.getPresetUtil().populatePtLoNamesList(userManageBean, userManageBean.getCurrentSite().getCustomerAccount(), userManageBean.getCurrentSite().getSiteId()));
            } else {
                pointLocationNameList.addAll(userManageBean.getPresetUtil().populatePtLoNamesList(userManageBean, userManageBean.getCurrentSite().getCustomerAccount(), null));
            }

            // pointLocationNameList.addAll(userManageBean.getPresetUtil().populatePtLoNamesList(userManageBean, userManageBean.getCurrentSite().getCustomerAccount(), userManageBean.getCurrentSite().getSiteId()));
            if (this.operationType == 2) {
                this.assetPresetPointLocationVO.setPointLocationName(null);
            }
            List<String> pointlocationNameList = new ArrayList();
            this.assetPresetVO.getAssetPresetPointLocationVOList().stream().forEach(p -> {
                if (this.operationType == 2 || !assetPresetPointLocationVO.getPointLocationName().equalsIgnoreCase(p.getPointLocationName())) {
                    pointlocationNameList.add(p.getPointLocationName());
                }
            });
            pointLocationNameList.stream().forEachOrdered(item -> {
                list = new ArrayList();
                SelectItemGroup ig = (SelectItemGroup) item;
                SelectItem[] itemArray = ig.getSelectItems();
                for (int i = 0; i < itemArray.length; i++) {
                    SelectItem item1 = itemArray[i];
                    if (!pointlocationNameList.contains(item1.getValue().toString())) {
                        list.add(item1);
                    }
                }
                ig.setSelectItems(list.stream().toArray(SelectItem[]::new));
            });
            
            if (assetPresetVO.getType().equalsIgnoreCase("site")) {
                tachometersList.addAll(userManageBean.getPresetUtil().populateTachometerList(userManageBean, userManageBean.getCurrentSite().getCustomerAccount(), userManageBean.getCurrentSite().getSiteId()));
            } else {
                tachometersList.addAll(userManageBean.getPresetUtil().populateTachometerList(userManageBean, userManageBean.getCurrentSite().getCustomerAccount(), null));
            }

            siteFFSetFavorites = FaultFrequenciesClient.getInstance().getSiteFFSetFavoritesVOList(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), userManageBean.getCurrentSite().getSiteId());
            faultFrequenciesList.addAll(userManageBean.getPresetUtil().populateFFSetNameList(userManageBean, siteFFSetFavorites));
            selectedDeviceType = assetPresetPointLocationVO.getDeviceType();
            selectedDevicePresetId = assetPresetPointLocationVO.getDeviceId();
            onTachChange(false);
            onDeviceTypeChange();
            onDeviceTemplateChange();
            Set<String> ffsetnames = new HashSet();
            if (assetPresetPointLocationVO.getFfSetIds() != null) {
                ffsetnames = PointLocationDomJsonParser.getInstance().populateFFSetFromJson(assetPresetPointLocationVO.getFfSetIds(), siteFFSetFavorites);
            }
            this.assetPresetPointLocationVO.setFfSetName(ffsetnames);

        }
    }

    public void onDeviceTypeChange() {
        List<DevicePresetVO> devicesPresetsLists;

        devicePresetList.clear();
        distinctDevicePresetList.clear();
        deviceTypesList.clear();

        try {
            devicesPresetsLists = new ArrayList();
            if (!userManageBean.getCurrentSite().getCustomerAccount().equals(DEFAULT_UPTIME_ACCOUNT)) {
                devicesPresetsLists.addAll(DevicePresetClient.getInstance().getGlobalDevicePresetsByCustomerDeviceType(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), DEFAULT_UPTIME_ACCOUNT, selectedDeviceType));
            }
            devicesPresetsLists.addAll(DevicePresetClient.getInstance().getGlobalDevicePresetsByCustomerDeviceType(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), selectedDeviceType));
            devicePresetList.addAll(devicesPresetsLists.stream().filter(Utils.distinctByKeys(DevicePresetVO::getSensorType, DevicePresetVO::getName, DevicePresetVO::getDeviceType, DevicePresetVO::getPresetId)).collect(Collectors.toList()));

            devicePresetList.addAll(DevicePresetClient.getInstance().getSiteDeviceByCustomerSiteDeviceType(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), userManageBean.getCurrentSite().getSiteId().toString(), selectedDeviceType));

            //distinctDevicePresetList.addAll(devicePresetList.stream().filter(Utils.distinctByKeys(DevicePresetVO::getName, DevicePresetVO::getDeviceType, DevicePresetVO::getPresetId)).collect(Collectors.toList()));
            deviceTypesList.addAll(userManageBean.getPresetUtil().populateDevicePresetList(userManageBean, devicePresetList));

        } catch (Exception e) {
            Logger.getLogger(PresetsBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            devicePresetList.clear();
        }
    }

    public void onDeviceTemplateChange() {
        List<DevicePresetVO> list;
        List<DeviceVO> deviceVOs;

        this.distinctDevicePresetList.clear();
        if (selectedDevicePresetId != null) {
            deviceVOs = CommonUtilSingleton.getInstance().populateDeviceTypeList(selectedDeviceType);
            list = devicePresetList.stream().filter(device -> device.getDeviceType().equalsIgnoreCase(selectedDeviceType) && device.getPresetId().compareTo(selectedDevicePresetId) == 0).collect(Collectors.toList());
            this.assetPresetPointLocationVO.setDeviceType(selectedDeviceType);
            this.assetPresetPointLocationVO.setDeviceName(list.get(0).getName());
            this.assetPresetPointLocationVO.setDeviceId(selectedDevicePresetId);
            list.forEach(device -> {
                device.setPointName(this.assetPresetPointLocationVO.getPointLocationName() + " " + CommonUtilSingleton.getInstance().getPointNameBySensorType(device.getSensorType()));
                if (device.isAlarmEnabled()) {
                    device.setAlarmEnabledValue(userManageBean.getResourceBundleString("label_true"));
                } else {
                    device.setAlarmEnabledValue(userManageBean.getResourceBundleString("label_false"));
                }
            });
            this.distinctDevicePresetList.addAll(list);
            this.assetPresetPointLocationVO.setDistinctDevicePresetList(new ArrayList<>(distinctDevicePresetList));
        }
    }

    /**
     * Action after a Tach is selected
     */
    public void onTachChange(boolean updateRollDiameter) {
        List<TachometerVO> tachList;

        try {
            if (assetPresetPointLocationVO != null) {
                if (assetPresetPointLocationVO.getTachId() != null) {
                    if ((tachList = TachometerClient.getInstance().getGlobalTachometersByPK(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), DEFAULT_UPTIME_ACCOUNT, assetPresetPointLocationVO.getTachId())) == null || tachList.isEmpty()) {
                        if ((tachList = TachometerClient.getInstance().getGlobalTachometersByPK(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), assetPresetPointLocationVO.getCustomerAccount(), assetPresetPointLocationVO.getTachId())) == null || tachList.isEmpty()) {
                            tachList = TachometerClient.getInstance().getSiteTachometersByPK(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), assetPresetPointLocationVO.getCustomerAccount(), assetPresetPointLocationVO.getSiteId(), assetPresetPointLocationVO.getTachId());
                        }
                    }
                    assetPresetPointLocationVO.setTachReferenceUnits((tachList != null && !tachList.isEmpty() && tachList.get(0).getTachReferenceUnits() != null) ? tachList.get(0).getTachReferenceUnits() : null);
                }
                if (updateRollDiameter) {
                    assetPresetPointLocationVO.setSpeedRatio(0);
                    assetPresetPointLocationVO.setRollDiameter(0);
                    assetPresetPointLocationVO.setRollDiameterUnits(null);
                }
                 if (assetPresetPointLocationVO.getTachReferenceUnits().equalsIgnoreCase("RPM")) {
                    assetPresetPointLocationVO.setSpeedRatio(1);
                }
                if (assetPresetPointLocationVO.getTachReferenceUnits().equalsIgnoreCase("FPM")) {
                    assetPresetPointLocationVO.setRollDiameter(1);
                }
            }
        } catch (Exception e) {
            Logger.getLogger(CopyEditPresetsPointLocationBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Action after a value is set/selected in the xhtml page
     */
    public void updateBean() {
        //Logger.getLogger(AddChannelBean.class.getName()).log(Level.INFO, "assetPresetPointLocationVO.getPointType()*********** {0}", new Object[]{assetPresetPointLocationVO.getPointType()});
    }

    /**
     * Action after submit button is clicked on the add point location overlay
     */
    public void submit() {
        try {
            if (this.assetPresetPointLocationVO != null) {
                this.assetPresetPointLocationVO.setFfSetIds(null);
                if (assetPresetPointLocationVO.getFfSetName() != null && !assetPresetPointLocationVO.getFfSetName().isEmpty()) {
                    assetPresetPointLocationVO.setFfSetIds(PointLocationDomJsonParser.getInstance().getJsonFromSelectedPointLocation(assetPresetPointLocationVO.getFfSetName(), siteFFSetFavorites));
                }
                List<String> ffsetnames = new ArrayList();
                if (this.assetPresetPointLocationVO.getFfSetIds() != null) {
                    ffsetnames = PointLocationDomJsonParser.getInstance().populateFFSetFromJson(assetPresetPointLocationVO.getFfSetIds());
                }
//                tachometersList.stream().filter(f -> f.getValue().toString().equalsIgnoreCase(assetPresetPointLocationVO.getTachId().toString())).forEach(asset -> {
//                    assetPresetPointLocationVO.setTachName(asset.getLabel());
//                });
                tachometersList.stream().forEachOrdered(item -> {
                    list = new ArrayList();
                    SelectItemGroup ig = (SelectItemGroup) item;
                    SelectItem[] itemArray = ig.getSelectItems();
                    for (int i = 0; i < itemArray.length; i++) {
                        SelectItem item1 = itemArray[i];
                        if (item1.getValue().toString().equalsIgnoreCase(assetPresetPointLocationVO.getTachId().toString())) {
                            assetPresetPointLocationVO.setTachName(item1.getLabel());
                        }
                    }
                });
                this.assetPresetPointLocationVO.setFfSetNames(String.join(", ", ffsetnames));
                if (assetPresetPointLocationVO.getSpeedRatio() > 0f) {
                    assetPresetPointLocationVO.setRollDiameterText(null);
                    assetPresetPointLocationVO.setRollDiameterUnits(null);
                    assetPresetPointLocationVO.setSpeedRatioText(String.valueOf(assetPresetPointLocationVO.getSpeedRatio()));
                } else {
                    assetPresetPointLocationVO.setSpeedRatioText(null);
                    assetPresetPointLocationVO.setRollDiameterText(String.valueOf(assetPresetPointLocationVO.getRollDiameter()));
                }
                this.assetPresetPointLocationVOList = this.assetPresetVO.getAssetPresetPointLocationVOList() != null ? assetPresetVO.getAssetPresetPointLocationVOList() : new ArrayList();
                if (this.operationType == 2) {
                    this.assetPresetPointLocationVOList.add(new AssetPresetPointLocationVO(this.assetPresetPointLocationVO));
                } else {
                    int index = assetPresetPointLocationVOList.indexOf(this.originalassetPresetPointLocationVO);
                    this.assetPresetPointLocationVOList.remove(this.originalassetPresetPointLocationVO);
                    this.assetPresetPointLocationVOList.add(index, this.assetPresetPointLocationVO);
                }

                assetPresetVO.setAssetPresetPointLocationVOList(this.assetPresetPointLocationVOList);
                 PrimeFaces.current().ajax().update("copyEditPresetsAssetFormId:pointLocationDatatableId");
                PrimeFaces.current().ajax().update("copyEditPresetsAssetFormId:assetPresetSaveId");
                PrimeFaces.current().ajax().update("createAssetsFormId:pointLocationDatatableId");
                PrimeFaces.current().ajax().update("createAssetsFormId:assetPresetSaveId2");
            }
        } catch (Exception e) {
            Logger.getLogger(AddChannelBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    public List<SelectItem> getPointLocationNameList() {
        return pointLocationNameList;
    }

    public List<SelectItem> getTachometersList() {
        return tachometersList;
    }

    public AssetPresetPointLocationVO getAssetPresetPointLocationVO() {
        return assetPresetPointLocationVO;
    }

    public void setAssetPresetPointLocationVO(AssetPresetPointLocationVO assetPresetPointLocationVO) {
        this.assetPresetPointLocationVO = assetPresetPointLocationVO;
    }

    public List<AssetPresetPointLocationVO> getAssetPresetPointLocationVOList() {
        return assetPresetPointLocationVOList;
    }

    public void setAssetPresetPointLocationVOList(List<AssetPresetPointLocationVO> assetPresetPointLocationVOList) {
        this.assetPresetPointLocationVOList = assetPresetPointLocationVOList;
    }

    public List<SelectItem> getFaultFrequenciesList() {
        return faultFrequenciesList;
    }

    public void setFaultFrequenciesList(List<SelectItem> faultFrequenciesList) {
        this.faultFrequenciesList = faultFrequenciesList;
    }

    public List<SelectItem> getRollDiameterUnitsList() {
        return rollDiameterUnitsList;
    }

    public List<String> getDeviceTypeList() {
        return deviceTypeList;
    }

    public String getSelectedDeviceType() {
        return selectedDeviceType;
    }

    public void setSelectedDeviceType(String selectedDeviceType) {
        this.selectedDeviceType = selectedDeviceType;
    }

    public List<FaultFrequenciesVO> getSiteFFSetFavorites() {
        return siteFFSetFavorites;
    }

    public void setSiteFFSetFavorites(List<FaultFrequenciesVO> siteFFSetFavorites) {
        this.siteFFSetFavorites = siteFFSetFavorites;
    }

    public List<DevicePresetVO> getDevicePresetsVOList() {
        return devicePresetsVOList;
    }

    public void setDevicePresetsVOList(List<DevicePresetVO> devicePresetsVOList) {
        this.devicePresetsVOList = devicePresetsVOList;
    }

    public Map<String, List<ApAlSetVO>> getDeviceTypeMap() {
        return deviceTypeMap;
    }

    public void setDeviceTypeMap(Map<String, List<ApAlSetVO>> deviceTypeMap) {
        this.deviceTypeMap = deviceTypeMap;
    }

    public Map<String, List<SelectItem>> getSelectDeviceTypeMap() {
        return selectDeviceTypeMap;
    }

    public void setSelectDeviceTypeMap(Map<String, List<SelectItem>> selectDeviceTypeMap) {
        this.selectDeviceTypeMap = selectDeviceTypeMap;
    }

    public List<DevicePresetVO> getDevicePresetList() {
        return devicePresetList;
    }

    public void setDevicePresetList(List<DevicePresetVO> devicePresetList) {
        this.devicePresetList = devicePresetList;
    }

    public List<DevicePresetVO> getDistinctDevicePresetList() {
        return distinctDevicePresetList;
    }

    public void setDistinctDevicePresetList(List<DevicePresetVO> distinctDevicePresetList) {
        this.distinctDevicePresetList = distinctDevicePresetList;
    }

    public List<SelectItem> getDeviceTypesList() {
        return deviceTypesList;
    }

    public UUID getSelectedDevicePresetId() {
        return selectedDevicePresetId;
    }

    public void setSelectedDevicePresetId(UUID selectedDevicePresetId) {
        this.selectedDevicePresetId = selectedDevicePresetId;
    }

    public int getOperationType() {
        return operationType;
    }

    public AssetPresetVO getAssetPresetVO() {
        return assetPresetVO;
    }

}
