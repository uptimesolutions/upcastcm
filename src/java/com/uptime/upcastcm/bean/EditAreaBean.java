/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.AreaClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import static com.uptime.upcastcm.utils.enums.HourEnum.getHourItemList;
import static com.uptime.upcastcm.utils.enums.WeekDayEnum.getDayItemList;
import com.uptime.upcastcm.utils.helperclass.Filters;
import com.uptime.upcastcm.utils.helperclass.Utils;
import com.uptime.upcastcm.utils.vo.AreaVO;
import com.uptime.upcastcm.utils.vo.DayVO;
import com.uptime.upcastcm.utils.vo.AreaConfigScheduleVO;
import java.io.Serializable;
import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import com.uptime.upcastcm.utils.interfaces.PresetDialogFields;
import com.uptime.upcastcm.utils.vo.UserSitesVO;
import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import javax.faces.model.SelectItem;
import org.apache.commons.lang3.StringUtils;
import org.primefaces.PrimeFaces;

/**
 *
 * @author Joseph
 */
public class EditAreaBean implements Serializable, PresetDialogFields {
    private UserManageBean userManageBean;
    private AreaVO areaVO;
    private boolean indoor, outdoor, scheduleBtnClick,scheduleContiFlag;
    private List<String> hrsList;
    private List<UserSitesVO> userSiteList;
    private List<AreaConfigScheduleVO> scheduleVOList, deleteScheduleVOList;
    private boolean validationFailed= false;

    /**
     * Constructor
     */
    public EditAreaBean() {
        try {
            userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
            hrsList = getHourItemList();
        } catch (Exception e) {
            userManageBean = null;
        }
    }

    /**
     * Post Constructor
     */
    @PostConstruct
    public void init() {
        resetPage();
    }

    @Override
    public void resetPage() {
        userSiteList = new ArrayList();
        indoor = false;
        outdoor = false;
        areaVO = new AreaVO();
        if (userManageBean != null && userManageBean.getCurrentSite() != null) {
            areaVO.setCustomerAccount(userManageBean.getCurrentSite().getCustomerAccount());
            areaVO.setSiteName(userManageBean.getCurrentSite().getSiteName());
            areaVO.setSiteId(userManageBean.getCurrentSite().getSiteId());
            userSiteList.add(userManageBean.getCurrentSite());
        }
        scheduleVOList = new ArrayList();
        deleteScheduleVOList = new ArrayList();
        areaVO.setScheduleVOList(scheduleVOList);
        scheduleBtnClick = false;
         
        
    }

    @Override
    public void presetFields(Filters presets, Object object) {
        
        deleteScheduleVOList = new ArrayList();
        
        if (object != null && object instanceof AreaVO) {
            //AreaVO without ScheduleVO
            areaVO = new AreaVO((AreaVO) object);

            //Get the AreaVO including ScheduleVO
            if ((areaVO = AreaClient.getInstance().getAreaList(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), areaVO.getCustomerAccount(), areaVO.getSiteId(), areaVO.getAreaId())) == null) {
                areaVO = new AreaVO((AreaVO) object);
            }
        } else {
            areaVO = new AreaVO();
        }
        
        if (areaVO.getEnvironment() != null && !areaVO.getEnvironment().equals("null")) {
            String[] envArray = areaVO.getEnvironment().split(",");
            indoor = Boolean.parseBoolean(envArray[0]);
            outdoor = Boolean.parseBoolean(envArray[1]);
        }
        if (presets != null && presets.getSite() != null) {
            userSiteList = new ArrayList();
            UserSitesVO userSitesVO = new UserSitesVO();
            userSitesVO.setSiteId((UUID) presets.getSite().getValue());
            userSitesVO.setSiteName(presets.getSite().getLabel());
            userSiteList.add(userSitesVO);
            
            areaVO.setSiteName(userSitesVO.getSiteName());
            areaVO.setSiteId(userSitesVO.getSiteId());
        }
        if (areaVO.getScheduleVOList() == null || areaVO.getScheduleVOList().isEmpty()) {
            scheduleVOList = new ArrayList();
            areaVO.setScheduleVOList(scheduleVOList);
        } else {
            scheduleVOList = new ArrayList();
            scheduleVOList.addAll(areaVO.getScheduleVOList().stream().filter(Utils.distinctByKeys(AreaConfigScheduleVO::getSiteName, AreaConfigScheduleVO::getAreaName, AreaConfigScheduleVO::getScheduleName, AreaConfigScheduleVO::getSchDescription)).collect(Collectors.toList()));
            scheduleVOList.forEach(svo -> svo.setDayVOList(prepareDayVOList(svo)));
            areaVO.setScheduleVOList(scheduleVOList);
        }
        
        if(areaVO != null && areaVO.getDescription() != null && areaVO.getDescription().equals("null")){
            areaVO.setDescription("");
        }
        
    }


    @Override
    public void presetFields(int operationType, Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void presetFields(Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void onHrsChange(AreaConfigScheduleVO asvo, DayVO svo) {
         scheduleContiFlag=false;
        Logger.getLogger(EditAreaBean.class.getName()).log(Level.INFO, "com.uptime.upcastcm.bean.AreaBean.onHrsChange() called ******");
        if (asvo.getType() == null || !asvo.getType().equalsIgnoreCase("New")) {
            asvo.setType("Update");
        }
        if (svo.getType().equalsIgnoreCase("DB")) {
            svo.setType("Update");
        }
        if (asvo.getDayVOList() != null) {
            DayVO day = asvo.getDayVOList().stream().filter(s -> s.getDay().equalsIgnoreCase(svo.getDay())).findAny().orElse(null);
            if (day.getSelectedHrsList().isEmpty()) {
                day.setType("Delete");
            } else {
                if (day.getType() == null || !day.getType().equalsIgnoreCase("New")) {
                    day.setType("Update");
                }
            }
        }
    }

    public void isContinousChange(AreaConfigScheduleVO svo) {
         scheduleContiFlag=false;
        svo.setDayVOList(prepareDayVOList());
    }

    public void updateArea() {
        try {
            if (!deleteScheduleVOList.isEmpty()) {
                areaVO.getScheduleVOList().addAll(deleteScheduleVOList);
            }
            validationFailed = false;
            if (!processAreaVo(areaVO)) {
               validationFailed = true; 
                return;
            }
            areaVO.setEnvironment(indoor + "," + outdoor);
            Logger.getLogger(EditAreaBean.class.getName()).log(Level.INFO, "EditAreaBean.updateArea(). areaVO: {0}", areaVO);
            if (AreaClient.getInstance().updateArea(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), areaVO)) {
                Logger.getLogger(EditAreaBean.class.getName()).log(Level.INFO, "Area updated successfully. {0}", new Object[]{areaVO});
                areaVO = new AreaVO();
                
                if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("configurationBean") == null) {
                    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("configurationBean", new ConfigurationBean());
                    ((ConfigurationBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("configurationBean")).init();
                }
                
                ((ConfigurationBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("configurationBean")).onSiteChange(true);
                PrimeFaces.current().executeScript("PF('defaultDlg').hide()");
                PrimeFaces.current().executeScript("updateTreeNodes()");
                
            } else {
                Logger.getLogger(EditAreaBean.class.getName()).log(Level.INFO, "Error in updating area. areaVO.getAreaName()*********** {0}", new Object[]{areaVO.getAreaName()});
            }
            scheduleBtnClick = false;
            
        } catch (Exception ex) {
        }

    }

    public void updateBean() {
        Logger.getLogger(EditAreaBean.class.getName()).log(Level.INFO, "CreateAreaBean.updateBean() called. ***********");
        if(areaVO != null && areaVO.getAreaName() != null && !areaVO.getAreaName().isEmpty()){
            scheduleBtnClick = false;
        }
    }

    public void updateBean(AreaConfigScheduleVO svo) {
        Logger.getLogger(EditAreaBean.class.getName()).log(Level.INFO, "CreateAreaBean.updateBean() called. ***********");
        if (svo.getType() == null || !svo.getType().equalsIgnoreCase("New")) {
            svo.setType("Update");
            svo.getDayVOList().stream().forEach(d -> {
                d.setType("Update");
            });
        }
        areaVO.getScheduleVOList().forEach(sVO -> {
            scheduleBtnClick = sVO.getScheduleName() == null || sVO.getScheduleName().isEmpty();
        });
        Logger.getLogger(CreateAreaBean.class.getName()).log(Level.INFO, "CreateAreaBean.updateScheduleName() called. ***********{0}", scheduleBtnClick);
    }

    public void updateClimateControlled() {
        Logger.getLogger(EditAreaBean.class.getName()).log(Level.INFO, "CreateAreaBean.updateCliemateControlled() called. ***********");
        if (!indoor) {
            areaVO.setClimateControlled(false);
        }
    }
    
    public void addSchedule() {
        Logger.getLogger(EditAreaBean.class.getName()).log(Level.INFO, "addSchedule()-----------");
        AreaConfigScheduleVO svo = new AreaConfigScheduleVO();
        svo.setDayVOList(prepareDayVOList());
        svo.setType("New");
        scheduleVOList.add(svo);
        scheduleBtnClick = true;
    }

    public void deleteSchedule(AreaConfigScheduleVO svo) {
        Logger.getLogger(EditAreaBean.class.getName()).log(Level.INFO, "deleteSchedule()-----------");
        scheduleVOList.remove(svo);
        if (svo.getType() == null || !svo.getType().equalsIgnoreCase("New")) {
            svo.setType("Delete");
            svo.getDayVOList().forEach(action -> {
                action.setType("Delete");
            });
            deleteScheduleVOList.add(svo);
        }
        scheduleBtnClick = false;
    }

    private List<DayVO> prepareDayVOList() {
        List<DayVO> dayVOList;
        DayVO day;
        
        dayVOList = new ArrayList();
        for (SelectItem item : getDayItemList()) {
            day = new DayVO();
            day.setDay(((DayOfWeek)item.getValue()).toString());
            day.setLabel(item.getLabel());
            day.setType("New");
            dayVOList.add(day);
        }

        return dayVOList;
    }

    private List<DayVO> prepareDayVOList(AreaConfigScheduleVO scVO) {
        List<AreaConfigScheduleVO> areaConfigScheduleVOs;
        List<DayVO> dayVOList;

        dayVOList = prepareDayVOList();
        if ((areaConfigScheduleVOs = areaVO.getScheduleVOList().stream().filter(a -> a.getScheduleName().equalsIgnoreCase(scVO.getScheduleName()) && a.getSchDescription().equalsIgnoreCase(scVO.getSchDescription())).collect(Collectors.toList())) != null && !areaConfigScheduleVOs.isEmpty()) {
            for (int i = 0; i < 7; i++) {
                for (AreaConfigScheduleVO vo : areaConfigScheduleVOs) {
                    if (vo.getDayOfWeek() == (i + 1)) {
                        dayVOList.get(i).setSelectedHrsList(Arrays.asList(vo.getHoursOfDay().split(",")));
                        dayVOList.get(i).setType("DB");
                        break;
                    }
                }
            }
        }

        return dayVOList;
    }

    private boolean processAreaVo(AreaVO areaVO) {
        List<AreaConfigScheduleVO> scheduleVOListTmp = new ArrayList();
        AreaConfigScheduleVO svoTmp;
        if (areaVO.getScheduleVOList() != null && !areaVO.getScheduleVOList().isEmpty()) {
            for (AreaConfigScheduleVO svo : areaVO.getScheduleVOList()) {
               scheduleContiFlag = true;
                boolean  valid = true;
                if (svo.getScheduleId() == null) {
                    svo.setScheduleId(UUID.randomUUID());
                }
                if (svo.isContinuous()) {
                     scheduleContiFlag = false;
                    List<AreaConfigScheduleVO> areaConfigScheduleVOs = AreaClient.getInstance().getAreaConfigScheduleByCustomerSiteAreaSchedule(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), areaVO.getCustomerAccount(), areaVO.getSiteId(), areaVO.getAreaId(), svo.getScheduleId());
                    if (areaConfigScheduleVOs != null && !areaConfigScheduleVOs.isEmpty()
                            && (areaConfigScheduleVOs.stream().filter(f -> f.getDayOfWeek() == 8).findAny().orElse(null) == null)) {
                        for (AreaConfigScheduleVO acsVO : areaConfigScheduleVOs) {
                            svoTmp = new AreaConfigScheduleVO();
                            svoTmp.setCustomerAccount(acsVO.getCustomerAccount());
                            svoTmp.setSiteId(acsVO.getSiteId());
                            svoTmp.setSiteName(acsVO.getSiteName());
                            svoTmp.setAreaId(acsVO.getAreaId());
                            svoTmp.setAreaName(acsVO.getAreaName());
                            svoTmp.setScheduleId(acsVO.getScheduleId());
                            svoTmp.setScheduleName(acsVO.getScheduleName());
                            svoTmp.setSchDescription(acsVO.getSchDescription());
                            svoTmp.setDayOfWeek(acsVO.getDayOfWeek());
                            svoTmp.setHoursOfDay(acsVO.getHoursOfDay());
                            svoTmp.setContinuous(acsVO.isContinuous());
                            svoTmp.setDayVOList(acsVO.getDayVOList());
                            svoTmp.setType("Delete");
                            scheduleVOListTmp.add(svoTmp);
                            if (svo.getType() != null && svo.getType().equalsIgnoreCase("Delete")) {
                                valid = false;
                            } else {
                                valid = true;
                                svo.setType("New");
                            }
                        }
                    }
                    if (valid) {
                        svoTmp = new AreaConfigScheduleVO();
                        svoTmp.setCustomerAccount(areaVO.getCustomerAccount());
                        svoTmp.setSiteId(areaVO.getSiteId());
                        svoTmp.setSiteName(areaVO.getSiteName());
                        svoTmp.setAreaId(areaVO.getAreaId());
                        svoTmp.setAreaName(areaVO.getAreaName());
                        svoTmp.setScheduleId(svo.getScheduleId());
                        svoTmp.setScheduleName(svo.getScheduleName());
                        svoTmp.setSchDescription(svo.getSchDescription());
                        svoTmp.setDayOfWeek((byte) 8);
                        svoTmp.setHoursOfDay(null);
                        svoTmp.setContinuous(svo.isContinuous());
                        svoTmp.setDayVOList(null);
                        svoTmp.setType(svo.getType());
                        scheduleVOListTmp.add(svoTmp);
                    }
                } else {
                    List<AreaConfigScheduleVO> areaConfigScheduleVOs = AreaClient.getInstance().getAreaConfigScheduleByCustomerSiteAreaSchedule(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), areaVO.getCustomerAccount(), areaVO.getSiteId(), areaVO.getAreaId(), svo.getScheduleId());
                    if (areaConfigScheduleVOs != null && !areaConfigScheduleVOs.isEmpty()
                            && (areaConfigScheduleVOs.stream().filter(f -> f.getDayOfWeek() == 8).findAny().orElse(null) != null)) {
                        for (AreaConfigScheduleVO acsVO : areaConfigScheduleVOs) {
                            scheduleContiFlag = true;
                            svoTmp = new AreaConfigScheduleVO();
                            svoTmp.setCustomerAccount(acsVO.getCustomerAccount());
                            svoTmp.setSiteId(acsVO.getSiteId());
                            svoTmp.setSiteName(acsVO.getSiteName());
                            svoTmp.setAreaId(acsVO.getAreaId());
                            svoTmp.setAreaName(acsVO.getAreaName());
                            svoTmp.setScheduleId(acsVO.getScheduleId());
                            svoTmp.setScheduleName(acsVO.getScheduleName());
                            svoTmp.setSchDescription(acsVO.getSchDescription());
                            svoTmp.setDayOfWeek(acsVO.getDayOfWeek());
                            svoTmp.setHoursOfDay(acsVO.getHoursOfDay());
                            svoTmp.setContinuous(acsVO.isContinuous());
                            svoTmp.setDayVOList(acsVO.getDayVOList());
                            svoTmp.setType("Delete");
                            scheduleVOListTmp.add(svoTmp);
                            if (svo.getType() != null && svo.getType().equalsIgnoreCase("Delete")) {
                                valid = false;
                            }
                        }
                    }
                    if (valid) {
                        for (DayVO dvo : svo.getDayVOList()) {
                            if (dvo.getSelectedHrsList() != null && !dvo.getSelectedHrsList().isEmpty()) {
                                if (!dvo.getSelectedHrsList().isEmpty()) {
                                    scheduleContiFlag = false;
                                }
                                svoTmp = new AreaConfigScheduleVO();
                                svoTmp.setCustomerAccount(areaVO.getCustomerAccount());
                                svoTmp.setSiteId(areaVO.getSiteId());
                                svoTmp.setSiteName(areaVO.getSiteName());
                                svoTmp.setAreaId(areaVO.getAreaId());
                                svoTmp.setAreaName(areaVO.getAreaName());
                                svoTmp.setScheduleId(svo.getScheduleId());
                                svoTmp.setScheduleName(svo.getScheduleName());
                                svoTmp.setSchDescription(svo.getSchDescription());
                                svoTmp.setDayOfWeek((byte) DayOfWeek.valueOf(dvo.getDay()).getValue());
                                if (!dvo.getSelectedHrsList().isEmpty()) {
                                    svoTmp.setHoursOfDay(String.join(",", dvo.getSelectedHrsList()));
                                }
                                svoTmp.setContinuous(svo.isContinuous());
                                svoTmp.setDayVOList(null);
                                svoTmp.setType(dvo.getType());
                                scheduleVOListTmp.add(svoTmp);
                            } else if (dvo.getType().equalsIgnoreCase("Delete")) {
                                List<AreaConfigScheduleVO> areaConfigSchedules = AreaClient.getInstance().getAreaConfigScheduleByPK(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), areaVO.getCustomerAccount(), areaVO.getSiteId(), areaVO.getAreaId(), svo.getScheduleId(), (byte) DayOfWeek.valueOf(dvo.getDay()).getValue());
                                if (areaConfigSchedules != null && !areaConfigSchedules.isEmpty()) {
                                    svoTmp = new AreaConfigScheduleVO();
                                    svoTmp.setCustomerAccount(areaVO.getCustomerAccount());
                                    svoTmp.setSiteId(areaVO.getSiteId());
                                    svoTmp.setSiteName(areaVO.getSiteName());
                                    svoTmp.setAreaId(areaVO.getAreaId());
                                    svoTmp.setAreaName(areaVO.getAreaName());
                                    svoTmp.setScheduleId(svo.getScheduleId());
                                    svoTmp.setScheduleName(svo.getScheduleName());
                                    svoTmp.setSchDescription(svo.getSchDescription());
                                    svoTmp.setDayOfWeek((byte) DayOfWeek.valueOf(dvo.getDay()).getValue());
                                    if (!dvo.getSelectedHrsList().isEmpty()) {
                                        svoTmp.setHoursOfDay(String.join(",", dvo.getSelectedHrsList()));
                                    }
                                    svoTmp.setContinuous(svo.isContinuous());
                                    svoTmp.setDayVOList(null);
                                    svoTmp.setType(dvo.getType());
                                    scheduleVOListTmp.add(svoTmp);
                                }
                            }
                        }
                    }
                    if (scheduleContiFlag) {
                        if (userManageBean != null) {
                            FacesContext.getCurrentInstance().addMessage("growlMessages", new FacesMessage(svo.getScheduleName() + ": " + userManageBean.getResourceBundleString("growl_create_area_1")));
                        }
                        return false;
                    }
                }
            }
            areaVO.setScheduleVOList(scheduleVOListTmp);

        }

        return true;
    }

    public AreaVO getAreaVO() {
        return areaVO;
    }

    public void setAreaVO(AreaVO areaVO) {
        this.areaVO = areaVO;
    }

    public List<String> getHrsList() {
        return hrsList;
    }

    public boolean isIndoor() {
        return indoor;
    }

    public void setIndoor(boolean indoor) {
        this.indoor = indoor;
    }

    public boolean isOutdoor() {
        return outdoor;
    }

    public void setOutdoor(boolean outdoor) {
        this.outdoor = outdoor;
    }

    public List<UserSitesVO> getUserSiteList() {
        return userSiteList;
    }

    public void setUserSiteList(List<UserSitesVO> userSiteList) {
        this.userSiteList = userSiteList;
    }

    public List<AreaConfigScheduleVO> getScheduleVOList() {
        return scheduleVOList;
    }

    public void setScheduleVOList(List<AreaConfigScheduleVO> scheduleVOList) {
        this.scheduleVOList = scheduleVOList;
    }

    public List<AreaConfigScheduleVO> getDeleteScheduleVOList() {
        return deleteScheduleVOList;
    }

    public void setDeleteScheduleVOList(List<AreaConfigScheduleVO> deleteScheduleVOList) {
        this.deleteScheduleVOList = deleteScheduleVOList;
    }

    public boolean isValidationFailed() {
        return validationFailed;
    }

    public void setValidationFailed(boolean validationFailed) {
        this.validationFailed = validationFailed;
    }

    public boolean isScheduleBtnClick() {
        return scheduleBtnClick;
    }

    public boolean isScheduleContiFlag() {
        return scheduleContiFlag;
    }

}
