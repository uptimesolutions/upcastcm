/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.ManageDeviceClient;
import com.uptime.upcastcm.http.client.MonitoringClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import static com.uptime.upcastcm.utils.enums.DonutChartBGColorEnum.getDonutChartBGColorItemList;
import com.uptime.upcastcm.utils.vo.DeviceBaseStationsVO;
import com.uptime.upcastcm.utils.vo.EchoDiagnosticsVO;
import com.uptime.upcastcm.utils.vo.MistDiagnosticsVO;
import com.uptime.upcastcm.utils.vo.AirbaseStatusVO;
import com.uptime.upcastcm.utils.vo.DeviceStatusVO;
import com.uptime.upcastcm.utils.vo.GatewayLastUploadVO;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.text.DecimalFormat;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import org.primefaces.model.charts.ChartData;
import org.primefaces.model.charts.donut.DonutChartDataSet;
import org.primefaces.model.charts.donut.DonutChartModel;
import org.primefaces.model.charts.donut.DonutChartOptions;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.event.ItemSelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author gopal
 */
public class SystemStatusBean implements Serializable {

    private NavigationBean navigationBean;
    private UserManageBean userManageBean;

    private final List<AirbaseStatusVO> airbaseStatusVOList;
    private final List<EchoDiagnosticsVO> echoDiagnosticsList;
    private final List<MistDiagnosticsVO> deviceStatusList, lowBatteryDeviceList;
    private final List<DeviceBaseStationsVO> deviceBaseStationsList;
    private final DecimalFormat decimalFormatter;

    private List<AirbaseStatusVO> filteredAirbaseStatusVOList;
    private List<EchoDiagnosticsVO> filteredEchoDiagnosticsList;
    private List<MistDiagnosticsVO> filteredDeviceStatusList;
    private List<String> donutBGColors;

    private DonutChartModel airbaseStatusDonutModel, echobaseStatusDonutModel, sensorStatusDonutModel;
    private boolean renderAirbaseStatus, renderEchobase, renderSensorStatus, renderLowBattery, renderDisabledSensor;
    private int numberOfDisabledSensor, numberOfLowBatteries;
    private double airbasePersentOfGreenValues, echoPersentOfGreenValues, mistPersentOfGreenValues;
    private boolean displayAirbasePersent, displayEchoPersent, displayMistPersent;
    private DateTime today;

    /**
     * Constructor
     */
    public SystemStatusBean() {
        numberOfDisabledSensor = 0;
        airbaseStatusVOList = new ArrayList();
        echoDiagnosticsList = new ArrayList();
        deviceStatusList = new ArrayList();
        deviceBaseStationsList = new ArrayList();
        lowBatteryDeviceList = new ArrayList();
        decimalFormatter = new DecimalFormat("#.#");

        try {
            donutBGColors = getDonutChartBGColorItemList();
            navigationBean = (NavigationBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("navigationBean");
            userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
        } catch (Exception e) {
            Logger.getLogger(SystemStatusBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            navigationBean = null;
            userManageBean = null;

        }
    }

    /**
     * Post Constructor
     */
    @PostConstruct
    public void init() {
        List<GatewayLastUploadVO> gatewayLastUploadVOList;
        List<DeviceStatusVO> deviceList;
        Pattern pattern;

        try {
            displayAirbasePersent = false;
            displayEchoPersent = false;
            displayMistPersent = false;
            airbasePersentOfGreenValues = 0;
            echoPersentOfGreenValues = 0;
            mistPersentOfGreenValues = 0;
            numberOfDisabledSensor = 0;
            numberOfLowBatteries = 0;
            airbaseStatusVOList.clear();
            echoDiagnosticsList.clear();
            deviceStatusList.clear();
            deviceBaseStationsList.clear();
            lowBatteryDeviceList.clear();
            filteredAirbaseStatusVOList = new ArrayList();
            filteredEchoDiagnosticsList = new ArrayList();
            filteredDeviceStatusList = new ArrayList();
            airbaseStatusDonutModel = new DonutChartModel();
            echobaseStatusDonutModel = new DonutChartModel();
            sensorStatusDonutModel = new DonutChartModel();
            today = null;

            if (userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null && userManageBean.getCurrentSite().getSiteId() != null && userManageBean.getZoneId() != null) {
                today = new DateTime().withZone(DateTimeZone.forID(userManageBean.getZoneId().getId()));
                
                // Setting echoDiagnosticsList and airbaseStatusVOList
                pattern = Pattern.compile(REGEX_ECHO_SERIAL_NUMBER);
                MonitoringClient.getInstance().getGatewayLastUploadByCustAccSite(ServiceRegistrarClient.getInstance().getServiceHostURL(MONITORING_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), userManageBean.getCurrentSite().getSiteId()).stream().forEachOrdered(gatewayLastUploadVO -> {
                    if (gatewayLastUploadVO.getDeviceId() != null && !gatewayLastUploadVO.getDeviceId().isEmpty() && gatewayLastUploadVO.getLastUpload() != null) {
                        if (pattern.matcher(gatewayLastUploadVO.getDeviceId()).matches()) {
                            echoDiagnosticsList.add(new EchoDiagnosticsVO(gatewayLastUploadVO));
                        } else {
                            airbaseStatusVOList.add(new AirbaseStatusVO(gatewayLastUploadVO));
                        }
                    }
                });
                
                airbaseStatusVOList.stream().filter(vo -> vo.getLastSampleReceived() != null).forEach(vo -> vo.setLastSampleReceivedWithSpecfZone(vo.getLastSampleReceived().atZone(userManageBean.getZoneId()).truncatedTo(ChronoUnit.SECONDS)));
                //Logger.getLogger(SystemStatusBean.class.getName()).log(Level.INFO, "airbaseStatusVOList: {0}", airbaseStatusVOList);
                createAirbaseStatusDonutModel();

                echoDiagnosticsList.stream().filter(vo -> vo.getLastUpdate() != null).forEach(vo -> vo.setLastUpdateWithSpecfZone(vo.getLastUpdate().atZone(userManageBean.getZoneId()).truncatedTo(ChronoUnit.SECONDS)));
                //Logger.getLogger(SystemStatusBean.class.getName()).log(Level.INFO, "echoDiagnosticsList: {0}", echoDiagnosticsList);
                createEchobaseStatusDonutModel();

                // Fetch the Device Status data from the Config service
                // Iterate over each DeviceStatusVO in deviceStatusList
                if ((deviceList = ManageDeviceClient.getInstance().getDeviceStatusByCustomerSite(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), userManageBean.getCurrentSite().getSiteName())) != null && !deviceList.isEmpty()) {
                    MistDiagnosticsVO deviceObject;
                    DeviceBaseStationsVO deviceBaseStation;
                    
                    for (DeviceStatusVO device : deviceList) {
                        
                        // Create deviceBaseStation Object for deviceBaseStationsList
                        if (device.isDisabled()) {
                            deviceBaseStation = new DeviceBaseStationsVO();
                            deviceBaseStation.setDeviceId(device.getDeviceId());
                            deviceBaseStationsList.add(deviceBaseStation);
                        } 
                        
                        // Create deviceObject Object for deviceStatusList
                        else {
                            deviceObject = new MistDiagnosticsVO();
                            deviceObject.setCustomerAccount(device.getCustomerAcct());
                            deviceObject.setSiteName(device.getSiteName());
                            deviceObject.setAreaName(device.getAreaName());
                            deviceObject.setAssetName(device.getAssetName());
                            deviceObject.setPointLocationName(device.getPointLocationName());
                            deviceObject.setDeviceId(device.getDeviceId());
                            deviceObject.setLastSample(device.getLastSample());
                            if (device.getLastSample() != null) {
                                deviceObject.setLastSampleWithSpecfZone(device.getLastSample().atZone(userManageBean.getZoneId()).truncatedTo(ChronoUnit.SECONDS));
                            }
                            if (device.getDeviceId() != null && device.getDeviceId().startsWith("00")) {
                                deviceObject.setBatteryStatus("N/A");  // TS1x Brick devices
                            } else if (device.getLastSample() == null) {
                                deviceObject.setBatteryStatus(userManageBean.getResourceBundleString("label_not_sampled"));  // Not sampled
                            } else {
                                deviceObject.setBatteryStatus(String.valueOf(device.getBatteryVoltage()));
                            }
                            deviceStatusList.add(deviceObject);
                        }
                        
                        // Create deviceObject Object for lowBatteryDeviceList
                        if (device.getDeviceId() != null && !device.getDeviceId().startsWith("00") && device.getLastSample() != null && device.getBatteryVoltage() < 3.19) {
                            deviceObject = new MistDiagnosticsVO();
                            deviceObject.setCustomerAccount(device.getCustomerAcct());
                            deviceObject.setSiteName(device.getSiteName());
                            deviceObject.setAreaName(device.getAreaName());
                            deviceObject.setAssetName(device.getAssetName());
                            deviceObject.setPointLocationName(device.getPointLocationName());
                            deviceObject.setDeviceId(device.getDeviceId());
                            deviceObject.setMistlxBattery(device.getBatteryVoltage());
                            deviceObject.setLastSample(device.getLastSample());
                            if (device.getLastSample() != null) {
                                deviceObject.setLastSampleWithSpecfZone(device.getLastSample().atZone(userManageBean.getZoneId()).truncatedTo(ChronoUnit.SECONDS));
                            }
                            lowBatteryDeviceList.add(deviceObject);
                        }
                        
                    }
                    numberOfDisabledSensor = deviceBaseStationsList.size();
                    numberOfLowBatteries = lowBatteryDeviceList.size();
                    //Logger.getLogger(SystemStatusBean.class.getName()).log(Level.INFO, "deviceStatusList: {0}", deviceStatusList);
                    //Logger.getLogger(SystemStatusBean.class.getName()).log(Level.INFO, "lowBatteryDeviceList: {0}", lowBatteryDeviceList);
                    //Logger.getLogger(SystemStatusBean.class.getName()).log(Level.INFO, "deviceBaseStationsList: {0}", deviceBaseStationsList);
                }
            }
            createSensorStatusDonutModel();
            
        } catch (Exception e) {
            Logger.getLogger(SystemStatusBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            numberOfDisabledSensor = 0;
            numberOfLowBatteries = 0;
            airbaseStatusVOList.clear();
            echoDiagnosticsList.clear();
            deviceStatusList.clear();
            deviceBaseStationsList.clear();
            lowBatteryDeviceList.clear();
            filteredAirbaseStatusVOList = new ArrayList();
            filteredEchoDiagnosticsList = new ArrayList();
            filteredDeviceStatusList = new ArrayList();
            airbaseStatusDonutModel = new DonutChartModel();
            echobaseStatusDonutModel = new DonutChartModel();
            sensorStatusDonutModel = new DonutChartModel();
            today = null;
        }
    }


    /**
     * Creates a donut model representing the status of airbases.
     *
     */
    public void createAirbaseStatusDonutModel() {
        int last24hrsRecordCount, last24hrsTo48hrsRecordCount, before48hrsRecordCount;
        DateTime yesterday, dayBeforeYesterday, dt;
        DonutChartDataSet dataSet;
        List<Number> values;
        List<String> labels;
        ChartData data;

        try {
            yesterday = today.minusDays(1);
            dayBeforeYesterday = today.minusDays(2);
            last24hrsRecordCount = 0;
            last24hrsTo48hrsRecordCount = 0;
            before48hrsRecordCount = 0;

            for (AirbaseStatusVO tmp : airbaseStatusVOList) {
                dt = toDateTime(tmp.getLastSampleReceivedWithSpecfZone());

                if (compareDates(dt, yesterday, true)) {
                    last24hrsRecordCount++;
                } else if (compareDates(dt, yesterday, dayBeforeYesterday)) {
                    last24hrsTo48hrsRecordCount++;
                } else if (compareDates(dt, dayBeforeYesterday, false)) {
                    before48hrsRecordCount++;
                }
            }

            airbaseStatusDonutModel = new DonutChartModel();
            airbaseStatusDonutModel.setOptions(new DonutChartOptions());

            if ((last24hrsRecordCount + last24hrsTo48hrsRecordCount + before48hrsRecordCount) > 0) {
                displayAirbasePersent = true;
                airbasePersentOfGreenValues = (((double)last24hrsRecordCount) / ((double)(last24hrsRecordCount + last24hrsTo48hrsRecordCount + before48hrsRecordCount))) * 100d;
            } else {
                displayAirbasePersent = false;
                airbasePersentOfGreenValues = 0d;
            }
            
            values = new ArrayList();
            values.add(last24hrsRecordCount);
            values.add(last24hrsTo48hrsRecordCount);
            values.add(before48hrsRecordCount);

            dataSet = new DonutChartDataSet();
            dataSet.setData(values);
            dataSet.setBackgroundColor(donutBGColors);

            labels = new ArrayList();
            labels.add(userManageBean.getResourceBundleString("label_name_active"));
            labels.add(userManageBean.getResourceBundleString("label_name_interrupted"));
            labels.add(userManageBean.getResourceBundleString("label_name_inactive"));

            data = new ChartData();
            data.addChartDataSet(dataSet);
            data.setLabels(labels);

            airbaseStatusDonutModel.setData(data);
        } catch (Exception e) {
            Logger.getLogger(SystemStatusBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Creates a donut model representing the status of Echobases.
     *
     */
    public void createEchobaseStatusDonutModel() {
        int last168hrsRecordCount, last168hrsTo240hrsRecordCount, before240hrsRecordCount;
        DateTime lastSevenDays, dayBeforeTenDays, dt;
        DonutChartDataSet dataSet;
        List<Number> values;
        List<String> labels;
        ChartData data;

        try {
            lastSevenDays = today.minusDays(7);
            dayBeforeTenDays = today.minusDays(10);
            last168hrsRecordCount = 0;
            last168hrsTo240hrsRecordCount = 0;
            before240hrsRecordCount = 0;

            for (EchoDiagnosticsVO tmp : echoDiagnosticsList) {
                dt = toDateTime(tmp.getLastUpdateWithSpecfZone());

                if (compareDates(dt, lastSevenDays, true)) {
                    last168hrsRecordCount++;
                } else if (compareDates(dt, lastSevenDays, dayBeforeTenDays)) {
                    last168hrsTo240hrsRecordCount++;
                } else if (compareDates(dt, dayBeforeTenDays, false)) {
                    before240hrsRecordCount++;
                }
            }

            echobaseStatusDonutModel = new DonutChartModel();
            echobaseStatusDonutModel.setOptions(new DonutChartOptions());

            if ((last168hrsRecordCount + last168hrsTo240hrsRecordCount + before240hrsRecordCount) > 0) {
                displayEchoPersent = true;
                echoPersentOfGreenValues = (((double)last168hrsRecordCount) / ((double)(last168hrsRecordCount + last168hrsTo240hrsRecordCount + before240hrsRecordCount))) * 100d;
            } else {
                displayEchoPersent = false;
                echoPersentOfGreenValues = 0d;
            }

            values = new ArrayList();
            values.add(last168hrsRecordCount);
            values.add(last168hrsTo240hrsRecordCount);
            values.add(before240hrsRecordCount);

            dataSet = new DonutChartDataSet();
            dataSet.setData(values);
            dataSet.setBackgroundColor(donutBGColors);

            labels = new ArrayList();
            labels.add(userManageBean.getResourceBundleString("label_name_active"));
            labels.add(userManageBean.getResourceBundleString("label_name_interrupted"));
            labels.add(userManageBean.getResourceBundleString("label_name_inactive"));

            data = new ChartData();
            data.addChartDataSet(dataSet);
            data.setLabels(labels);

            echobaseStatusDonutModel.setData(data);
        } catch (Exception e) {
            Logger.getLogger(SystemStatusBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Creates a donut model representing the status of Devices.
     *
     */
    public void createSensorStatusDonutModel() {
        int lastTwoDaysRecordCount, lastTwoDaysToSevenDaysRecordCount, beforeSevenDaysRecordCount;
        DateTime lastTwoDays, lastSevenDays, dt;
        DonutChartDataSet dataSet;
        List<Number> values;
        List<String> labels;
        ChartData data;

        try {
            lastTwoDays = today.minusDays(2);
            lastSevenDays = today.minusDays(7);
            lastTwoDaysRecordCount = 0;
            lastTwoDaysToSevenDaysRecordCount = 0;
            beforeSevenDaysRecordCount = 0;

            for (MistDiagnosticsVO tmp : deviceStatusList) {
                if (tmp.getLastSample() != null) {
                    dt = toDateTime(tmp.getLastSampleWithSpecfZone());

                    if (compareDates(dt, lastTwoDays, true)) {
                        lastTwoDaysRecordCount++;
                    } else if (compareDates(dt, lastTwoDays, lastSevenDays)) {
                        lastTwoDaysToSevenDaysRecordCount++;
                    } else if (compareDates(dt, lastSevenDays, false)) {
                        beforeSevenDaysRecordCount++;
                    }

                } else {
                    beforeSevenDaysRecordCount++;
                }
            }

            sensorStatusDonutModel = new DonutChartModel();
            sensorStatusDonutModel.setOptions(new DonutChartOptions());

            if ((lastTwoDaysRecordCount + lastTwoDaysToSevenDaysRecordCount + beforeSevenDaysRecordCount) > 0) {
                displayMistPersent = true;
                mistPersentOfGreenValues = (((double)lastTwoDaysRecordCount) / ((double)(lastTwoDaysRecordCount + lastTwoDaysToSevenDaysRecordCount + beforeSevenDaysRecordCount))) * 100d;
            } else {
                displayMistPersent = false;
                mistPersentOfGreenValues = 0d;
            }

            values = new ArrayList();
            values.add(lastTwoDaysRecordCount);
            values.add(lastTwoDaysToSevenDaysRecordCount);
            values.add(beforeSevenDaysRecordCount);

            dataSet = new DonutChartDataSet();
            dataSet.setData(values);
            dataSet.setBackgroundColor(donutBGColors);

            labels = new ArrayList();
            labels.add(userManageBean.getResourceBundleString("label_name_active"));
            labels.add(userManageBean.getResourceBundleString("label_name_interrupted"));
            labels.add(userManageBean.getResourceBundleString("label_name_inactive"));

            data = new ChartData();
            data.addChartDataSet(dataSet);
            data.setLabels(labels);

            sensorStatusDonutModel.setData(data);
        } catch (Exception e) {
            Logger.getLogger(SystemStatusBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Handles the click event on the airbase donut model.
     *
     * @param event
     */
    public void handleAirbaseDonutClick(ItemSelectEvent event) {
        DateTime dt, yesterday, dayBeforeYesterday;
        DataTable datatable;
        
        try {
            if ((datatable = (DataTable)FacesContext.getCurrentInstance().getViewRoot().findComponent("airbaseStatusDataTableId")) != null) {
                datatable.reset();
            }
        } catch (Exception e) {}

        try {
            renderEchobase = false;
            renderAirbaseStatus = true;
            renderSensorStatus = false;
            renderLowBattery = false;
            renderDisabledSensor = false;
            filteredAirbaseStatusVOList = new ArrayList();

            yesterday = today.minusDays(1);
            dayBeforeYesterday = today.minusDays(2);
            switch (event.getItemIndex()) {
                case 0:   // green clicked
                    for (AirbaseStatusVO tmp : airbaseStatusVOList) {
                        dt = toDateTime(tmp.getLastSampleReceivedWithSpecfZone());
                        if (compareDates(dt, yesterday, true)) {
                            filteredAirbaseStatusVOList.add(tmp);
                        }
                    }
                    break;
                case 1:     // yellow clicked
                    for (AirbaseStatusVO tmp : airbaseStatusVOList) {
                        dt = toDateTime(tmp.getLastSampleReceivedWithSpecfZone());
                        if (compareDates(dt, yesterday, dayBeforeYesterday)) {
                            filteredAirbaseStatusVOList.add(tmp);
                        }
                    }
                    break;
                case 2:     // red clicked
                    for (AirbaseStatusVO tmp : airbaseStatusVOList) {
                        dt = toDateTime(tmp.getLastSampleReceivedWithSpecfZone());
                        if (compareDates(dt, dayBeforeYesterday, false)) {
                            filteredAirbaseStatusVOList.add(tmp);
                        }
                    }
                    break;
                default:
                    throw new AssertionError();
            }

            navigationBean.updateDialog("system-status-data-airbase");
        } catch (Exception e) {
            Logger.getLogger(SystemStatusBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Handles the click event on the Echobase donut model.
     *
     * @param event
     */
    public void handleEchobaseDonutClick(ItemSelectEvent event) {
        DateTime dt, lastSevenDays, dayBeforeTenDays;
        DataTable datatable;
        
        try {
            if ((datatable = (DataTable)FacesContext.getCurrentInstance().getViewRoot().findComponent("echobaseStatusDataTableId")) != null) {
                datatable.reset();
            }
        } catch (Exception e) {}

        try {
            renderEchobase = true;
            renderAirbaseStatus = false;
            renderSensorStatus = false;
            renderLowBattery = false;
            renderDisabledSensor = false;
            filteredEchoDiagnosticsList = new ArrayList();

            lastSevenDays = today.minusDays(7);
            dayBeforeTenDays = today.minusDays(10);
            switch (event.getItemIndex()) {
                case 0:   // green clicked
                    for (EchoDiagnosticsVO tmp : echoDiagnosticsList) {
                        dt = toDateTime(tmp.getLastUpdateWithSpecfZone());
                        if (compareDates(dt, lastSevenDays, true)) {
                            filteredEchoDiagnosticsList.add(tmp);
                        }
                    }
                    break;
                case 1:     // yellow clicked
                    for (EchoDiagnosticsVO tmp : echoDiagnosticsList) {
                        dt = toDateTime(tmp.getLastUpdateWithSpecfZone());
                        if (compareDates(dt, lastSevenDays, dayBeforeTenDays)) {
                            filteredEchoDiagnosticsList.add(tmp);
                        }
                    }
                    break;
                case 2:     // red clicked
                    for (EchoDiagnosticsVO tmp : echoDiagnosticsList) {
                        dt = toDateTime(tmp.getLastUpdateWithSpecfZone());
                        if (compareDates(dt, dayBeforeTenDays, false)) {
                            filteredEchoDiagnosticsList.add(tmp);
                        }
                    }
                    break;
                default:
                    throw new AssertionError();
            }

            navigationBean.updateDialog("system-status-data-echobase");
        } catch (Exception e) {
            Logger.getLogger(SystemStatusBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Handles the click event on the Devices donut model.
     *
     * @param event
     */
    public void handleSensorStatusDonutClick(ItemSelectEvent event) {
        DateTime dt, lastTwoDays, dayBeforeSevenDays;
        DataTable datatable;
        
        try {
            if ((datatable = (DataTable)FacesContext.getCurrentInstance().getViewRoot().findComponent("mistSensorStatusDataTableId")) != null) {
                datatable.reset();
            }
        } catch (Exception e) {}

        try {
            renderEchobase = false;
            renderAirbaseStatus = false;
            renderLowBattery = false;
            renderDisabledSensor = false;
            renderSensorStatus = true;
            filteredDeviceStatusList = new ArrayList();

            lastTwoDays = today.minusDays(2);
            dayBeforeSevenDays = today.minusDays(7);
            switch (event.getItemIndex()) {
                case 0:   // green clicked
                    for (MistDiagnosticsVO tmp : deviceStatusList) {
                        if (tmp.getLastSample() != null) {
                            dt = toDateTime(tmp.getLastSampleWithSpecfZone());
                            if (compareDates(dt, lastTwoDays, true)) {
                                filteredDeviceStatusList.add(tmp);
                            }
                        }
                    }
                    break;
                case 1:     // yellow clicked
                    for (MistDiagnosticsVO tmp : deviceStatusList) {
                        if (tmp.getLastSample() != null) {
                            dt = toDateTime(tmp.getLastSampleWithSpecfZone());
                            if (compareDates(dt, lastTwoDays, dayBeforeSevenDays)) {
                                filteredDeviceStatusList.add(tmp);
                            }
                        }
                    }
                    break;
                case 2:     // red clicked
                    for (MistDiagnosticsVO tmp : deviceStatusList) {
                        if (tmp.getLastSample() != null) {
                            dt = toDateTime(tmp.getLastSampleWithSpecfZone());
                            if (compareDates(dt, dayBeforeSevenDays, false)) {
                                filteredDeviceStatusList.add(tmp);
                            }
                        } else {
                                filteredDeviceStatusList.add(tmp);
                        }
                    }
                    break;
                default:
                    throw new AssertionError();
            }

            navigationBean.updateDialog("device-status");
        } catch (Exception e) {
            Logger.getLogger(SystemStatusBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Handles the click event on the Devices donut model.
     */
    public void handleLowBetteryClick() {
        DataTable datatable;
        
        try {
            if ((datatable = (DataTable)FacesContext.getCurrentInstance().getViewRoot().findComponent("lowBatteryDataTableId")) != null) {
                datatable.reset();
            }
        } catch (Exception e) {}
        
        try {
            renderEchobase = false;
            renderAirbaseStatus = false;
            renderSensorStatus = false;
            renderDisabledSensor = false;
            renderLowBattery = true;

            navigationBean.updateDialog("device-low-battery");
        } catch (Exception e) {
            Logger.getLogger(SystemStatusBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Handles the click event on the Devices donut model.
     */
    public void handleDisabledSensorClick() {
        DataTable datatable;
        
        try {
            if ((datatable = (DataTable)FacesContext.getCurrentInstance().getViewRoot().findComponent("disabledSensorDataTableId")) != null) {
                datatable.reset();
            }
        } catch (Exception e) {}
        
        try {
            renderEchobase = false;
            renderAirbaseStatus = false;
            renderSensorStatus = false;
            renderLowBattery = false;
            renderDisabledSensor = true;

            navigationBean.updateDialog("system-status-matrics-disabled-sensor");
        } catch (Exception e) {
            Logger.getLogger(SystemStatusBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * download as CSV
     *
     * @return StreamedContent Object
     */
    public StreamedContent downloadCSVFile() {
        StringBuilder csvData;
        InputStream stream;
        String csvFileName = null;

        try {
            csvData = new StringBuilder();

            // Convert the filteredAirbaseStatusVOList to a CSV format using StringBuilder.
            if (filteredAirbaseStatusVOList != null && !filteredAirbaseStatusVOList.isEmpty() && renderAirbaseStatus) {
                csvFileName = userManageBean.getResourceBundleString("file_name_airbase_status");
                String[] header = {userManageBean.getResourceBundleString("label_siteName"), userManageBean.getResourceBundleString("label_basestation_id"), (userManageBean.getResourceBundleString("label_last_sample_received") + " - (" + userManageBean.getZoneId().getId() + ")")};
                
                // Header row
                // Convert header to CSV format using StringBuilder
                for (int i = 0; i < header.length; i++) {
                    csvData.append(header[i]);
                    if (i != header.length - 1) {
                        csvData.append(",");
                    }
                }
                
                csvData.append("\n");
                filteredAirbaseStatusVOList.stream().forEachOrdered(abvo -> {
                    csvData
                            .append(abvo.getSiteName() != null ? abvo.getSiteName() : "").append(",")
                            .append(abvo.getBaseStationId() != null ? abvo.getBaseStationId() : "").append(",")
                            .append(abvo.getFormatedLastSampleReceived() != null ? abvo.getFormatedLastSampleReceived() : "").append("\n");
                });
            }

            // Convert the filteredEchoDiagnosticsList to a CSV format using StringBuilder.
            if (filteredEchoDiagnosticsList != null && !filteredEchoDiagnosticsList.isEmpty() && renderEchobase) {
                csvFileName = userManageBean.getResourceBundleString("file_name_echobase_status");
                String[] header = {userManageBean.getResourceBundleString("label_siteName"), userManageBean.getResourceBundleString("label_device_id"), (userManageBean.getResourceBundleString("label_last_updated") + " - (" + userManageBean.getZoneId().getId() + ")")};
                    
                // Header row
                // Convert header to CSV format using StringBuilder
                for (int i = 0; i < header.length; i++) {
                    csvData.append(header[i]);
                    if (i != header.length - 1) {
                        csvData.append(",");
                    }
                }
                
                csvData.append("\n");
                filteredEchoDiagnosticsList.stream().forEachOrdered(edvo -> {
                    csvData
                            .append(edvo.getSiteName() != null ? edvo.getSiteName() : "").append(",")
                            .append(edvo.getDeviceId() != null ? edvo.getDeviceId() : "").append(",")
                            .append(edvo.getFormatedLastUpdate() != null ? edvo.getFormatedLastUpdate() : "").append("\n");
                });
            }

            // Convert the filteredDeviceStatusList to a CSV format using StringBuilder.
            if (filteredDeviceStatusList != null && !filteredDeviceStatusList.isEmpty() && renderSensorStatus) {
                csvFileName = userManageBean.getResourceBundleString("file_name_sensors_status");
                String[] header = {userManageBean.getResourceBundleString("label_siteName"), userManageBean.getResourceBundleString("label_area_name"), userManageBean.getResourceBundleString("label_asset_name"), userManageBean.getResourceBundleString("label_point_locations_name"), userManageBean.getResourceBundleString("label_device_id"), (userManageBean.getResourceBundleString("label_last_sample_received") + " - (" + userManageBean.getZoneId().getId() + ")"),userManageBean.getResourceBundleString("label_battery_voltage")};
                
                // Header row
                // Convert header to CSV format using StringBuilder
                for (int i = 0; i < header.length; i++) {
                    csvData.append(header[i]);
                    if (i != header.length - 1) {
                        csvData.append(",");
                    }
                }
                
                csvData.append("\n");
                filteredDeviceStatusList.stream().forEachOrdered(edvo -> {
                    csvData
                            .append(edvo.getSiteName() != null ? edvo.getSiteName() : "").append(",")
                            .append(edvo.getAreaName() != null ? (edvo.getAreaName().contains(",") ? "\"" + edvo.getAreaName() + "\"" : edvo.getAreaName()) : "").append(",")
                            .append(edvo.getAssetName() != null ? (edvo.getAssetName().contains(",") ? "\"" + edvo.getAssetName() + "\"" : edvo.getAssetName()) : "").append(",")
                            .append(edvo.getPointLocationName() != null ? (edvo.getPointLocationName().contains(",") ? "\"" + edvo.getPointLocationName() + "\"" : edvo.getPointLocationName()) : "").append(",")
                            .append(edvo.getDeviceId() != null ? edvo.getDeviceId() : "").append(",")
                            .append(edvo.getFormatedLastSample() != null ? edvo.getFormatedLastSample() : "").append(",")
                            .append(edvo.getBatteryStatus() != null ? edvo.getBatteryStatus() : "")
                            .append("\n");
                });
            }

            // Convert the lowBatteryDeviceList to a CSV format using StringBuilder.
            if (lowBatteryDeviceList != null && !lowBatteryDeviceList.isEmpty() && renderLowBattery) {
                csvFileName = userManageBean.getResourceBundleString("file_name_low_battery");
                String[] header = {userManageBean.getResourceBundleString("label_siteName"), userManageBean.getResourceBundleString("label_area_name"), userManageBean.getResourceBundleString("label_asset_name"), userManageBean.getResourceBundleString("label_point_locations_name"), userManageBean.getResourceBundleString("label_device_id"), userManageBean.getResourceBundleString("label_battery_voltage"), (userManageBean.getResourceBundleString("label_last_sample_received") + " - (" + userManageBean.getZoneId().getId() + ")")};
                // Header row
                // Convert header to CSV format using StringBuilder
                
                for (int i = 0; i < header.length; i++) {
                    csvData.append(header[i]);
                    if (i != header.length - 1) {
                        csvData.append(",");
                    }
                }
                
                csvData.append("\n");
                lowBatteryDeviceList.stream().forEachOrdered(edvo -> {
                    csvData
                            .append(edvo.getSiteName() != null ? edvo.getSiteName() : "").append(",")
                            .append(edvo.getAreaName() != null ? (edvo.getAreaName().contains(",") ? "\"" + edvo.getAreaName() + "\"" : edvo.getAreaName()) : "").append(",")
                            .append(edvo.getAssetName() != null ? (edvo.getAssetName().contains(",") ? "\"" + edvo.getAssetName() + "\"" : edvo.getAssetName()) : "").append(",")
                            .append(edvo.getPointLocationName() != null ? (edvo.getPointLocationName().contains(",") ? "\"" + edvo.getPointLocationName() + "\"" : edvo.getPointLocationName()) : "").append(",")
                            .append(edvo.getDeviceId() != null ? edvo.getDeviceId() : "").append(",")
                            .append(edvo.getMistlxBattery()).append(",")
                            .append(edvo.getFormatedLastSample() != null ? edvo.getFormatedLastSample() : "")
                            .append("\n");
                });
            }

            // Convert the deviceBaseStationsList to a CSV format using StringBuilder.
            if (deviceBaseStationsList != null && !deviceBaseStationsList.isEmpty() && renderDisabledSensor) {
                csvFileName = userManageBean.getResourceBundleString("file_name_disabled_sensors");
                String[] header = {userManageBean.getResourceBundleString("label_device_id")};
                
                // Header row
                // Convert header to CSV format using StringBuilder
                for (int i = 0; i < header.length; i++) {
                    csvData.append(header[i]);
                    if (i != header.length - 1) {
                        csvData.append(",");
                    }
                }
                
                csvData.append("\n");
                deviceBaseStationsList.stream().forEachOrdered(edvo -> {
                    csvData
                            .append(edvo.getDeviceId() != null ? edvo.getDeviceId() : "").append("\n");
                });
            }
            
            // Convert CSV content to InputStream
            stream = new ByteArrayInputStream(csvData.toString().getBytes(StandardCharsets.UTF_8));
            
            // Create and return StreamedContent
            return DefaultStreamedContent.builder().name(csvFileName).contentType("text/csv").stream(() -> stream).build();
        } catch (Exception e) {
            Logger.getLogger(DeviceManagementBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            return null;
        }
    }

    public boolean compareDates(DateTime sampleTime, DateTime from, DateTime to) {
        try {
            if (sampleTime != null && from != null && to != null) {
                return sampleTime.compareTo(from) < 0 && sampleTime.compareTo(to) >= 0;
            }
        } catch (Exception e) {
            Logger.getLogger(SystemStatusBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
        return false;
    }

    public boolean compareDates(DateTime sampleTime, DateTime controlTime, boolean before) {
        try {
            if (sampleTime != null && controlTime != null) {
                if(before) {
                    return sampleTime.compareTo(controlTime) >= 0;
                } else {
                    return sampleTime.compareTo(controlTime) < 0;
                }
            }
        } catch (Exception e) {
            Logger.getLogger(SystemStatusBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
        return false;
    }

    public DateTime toDateTime(ZonedDateTime sampleTime) {
        try {
            if (userManageBean != null && userManageBean.getZoneId() != null && sampleTime != null ) {
                return new DateTime(sampleTime.toInstant().toEpochMilli(), DateTimeZone.forID(userManageBean.getZoneId().getId()));
            }
        } catch (Exception e) {
            Logger.getLogger(SystemStatusBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
        return null;
    }

    public String getAirbasePersentOfGreenValues() {
        try {
            return decimalFormatter.format(airbasePersentOfGreenValues) + "%";
        } catch (Exception ex) {}
        return "0%";
    }

    public String getEchoPersentOfGreenValues() {
        try {
            return decimalFormatter.format(echoPersentOfGreenValues) + "%";
        } catch (Exception ex) {}
        return "0%";
    }

    public String getMistPersentOfGreenValues() {
        try {
            return decimalFormatter.format(mistPersentOfGreenValues) + "%";
        } catch (Exception ex) {}
        return "0%";
    }

    public DonutChartModel getAirbaseStatusDonutModel() {
        return airbaseStatusDonutModel;
    }

    public void setAirbaseStatusDonutModel(DonutChartModel airbaseStatusDonutModel) {
        this.airbaseStatusDonutModel = airbaseStatusDonutModel;
    }

    public DonutChartModel getEchobaseStatusDonutModel() {
        return echobaseStatusDonutModel;
    }

    public void setEchobaseStatusDonutModel(DonutChartModel echobaseStatusDonutModel) {
        this.echobaseStatusDonutModel = echobaseStatusDonutModel;
    }

    public List<AirbaseStatusVO> getFilteredAirbaseStatusVOList() {
        return filteredAirbaseStatusVOList;
    }

    public void setFilteredAirbaseStatusVOList(List<AirbaseStatusVO> filteredAirbaseStatusVOList) {
        this.filteredAirbaseStatusVOList = filteredAirbaseStatusVOList;
    }

    public List<EchoDiagnosticsVO> getFilteredEchoDiagnosticsList() {
        return filteredEchoDiagnosticsList;
    }

    public void setFilteredEchoDiagnosticsList(List<EchoDiagnosticsVO> filteredEchoDiagnosticsList) {
        this.filteredEchoDiagnosticsList = filteredEchoDiagnosticsList;
    }

    public boolean isRenderAirbaseStatus() {
        return renderAirbaseStatus;
    }

    public void setRenderAirbaseStatus(boolean renderAirbaseStatus) {
        this.renderAirbaseStatus = renderAirbaseStatus;
    }

    public boolean isRenderEchobase() {
        return renderEchobase;
    }

    public void setRenderEchobase(boolean renderEchobase) {
        this.renderEchobase = renderEchobase;
    }

    public boolean isRenderSensorStatus() {
        return renderSensorStatus;
    }

    public void setRenderSensorStatus(boolean renderSensorStatus) {
        this.renderSensorStatus = renderSensorStatus;
    }

    public DonutChartModel getSensorStatusDonutModel() {
        return sensorStatusDonutModel;
    }

    public void setSensorStatusDonutModel(DonutChartModel sensorStatusDonutModel) {
        this.sensorStatusDonutModel = sensorStatusDonutModel;
    }

    public List<MistDiagnosticsVO> getFilteredDeviceStatusList() {
        return filteredDeviceStatusList;
    }

    public void setFilteredDeviceStatusList(List<MistDiagnosticsVO> filteredDeviceStatusList) {
        this.filteredDeviceStatusList = filteredDeviceStatusList;
    }

    public boolean isRenderLowBattery() {
        return renderLowBattery;
    }

    public int getNumberOfDisabledSensor() {
        return numberOfDisabledSensor;
    }

    public int getNumberOfLowBatteries() {
        return numberOfLowBatteries;
    }

    public List<DeviceBaseStationsVO> getDeviceBaseStationsList() {
        return deviceBaseStationsList;
    }

    public boolean isRenderDisabledSensor() {
        return renderDisabledSensor;
    }

    public List<MistDiagnosticsVO> getLowBatteryDeviceList() {
        return lowBatteryDeviceList;
    }

    public boolean isDisplayAirbasePersent() {
        return displayAirbasePersent;
    }

    public void setDisplayAirbasePersent(boolean displayAirbasePersent) {
        this.displayAirbasePersent = displayAirbasePersent;
    }

    public boolean isDisplayEchoPersent() {
        return displayEchoPersent;
    }

    public void setDisplayEchoPersent(boolean displayEchoPersent) {
        this.displayEchoPersent = displayEchoPersent;
    }

    public boolean isDisplayMistPersent() {
        return displayMistPersent;
    }

    public void setDisplayMistPersent(boolean displayMistPersent) {
        this.displayMistPersent = displayMistPersent;
    }
}
