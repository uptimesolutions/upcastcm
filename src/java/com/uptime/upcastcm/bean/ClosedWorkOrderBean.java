/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.WorkOrderClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import com.uptime.upcastcm.utils.enums.WorkOrderActionEnum;
import static com.uptime.upcastcm.utils.enums.WorkOrderActionEnum.getActionItemList;
import com.uptime.upcastcm.utils.enums.WorkOrderIssueEnum;
import static com.uptime.upcastcm.utils.enums.WorkOrderIssueEnum.getIssueItemList;
import com.uptime.upcastcm.utils.enums.WorkOrderPriorityEnum;
import static com.uptime.upcastcm.utils.enums.WorkOrderPriorityEnum.getPriorityItemList;
import com.uptime.upcastcm.utils.vo.WorkOrderVO;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.model.FilterMeta;

/**
 *
 * @author twilcox
 */
public class ClosedWorkOrderBean implements Serializable {
    private UserManageBean userManageBean;
    private Short selectedYear;
    private final short MIN_SELECTED_YEAR;
    private short maxSelectedYear;
    private final List<WorkOrderVO> closedWorkOrderList;
    private List<WorkOrderVO> filteredClosedWorkOrderList;
    private List<FilterMeta> filterBy;
    private List<SelectItem> priorityList, issueList, actionList;

    /**
     * Constructor
     */
    public ClosedWorkOrderBean() {
        MIN_SELECTED_YEAR = 2020;
        closedWorkOrderList = new ArrayList();
        
        try {
            priorityList = getPriorityItemList();
            issueList = getIssueItemList();
            actionList = getActionItemList();
            userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
        } catch (Exception e) {
            Logger.getLogger(ClosedWorkOrderBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            userManageBean = null;
        }
    }

    /**
     * Post Constructor
     */
    @PostConstruct
    public void init() {
        resetPage();
    }

    /**
     * Reset fields values when navigates to ClosedWorkOrder page.
     */
    public void resetPage() {
        List<WorkOrderVO> list;
        
        selectedYear = 2020;
        maxSelectedYear = 2020;
        closedWorkOrderList.clear();
        
        try {
            selectedYear = (userManageBean != null && userManageBean.getZoneId() != null) ? (short)ZonedDateTime.now(userManageBean.getZoneId()).getYear() : MIN_SELECTED_YEAR;
            maxSelectedYear = selectedYear;
            list = WorkOrderClient.getInstance().getClosedWorkOrdersByCustomerSiteYear(ServiceRegistrarClient.getInstance().getServiceHostURL(WORK_ORDER_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), userManageBean.getCurrentSite().getSiteId(), selectedYear);
            closedWorkOrderList.addAll(list.stream().sorted(Comparator.comparing(WorkOrderVO::getFormattedCloseDateString).reversed()).collect(Collectors.toList()));
        } catch (Exception e) {
            Logger.getLogger(ClosedWorkOrderBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            selectedYear = MIN_SELECTED_YEAR;
            closedWorkOrderList.clear();
        }
    }
    
    /**
     * Increment the year and look up the closed work orders for the new year
     */
    public void incrementSelectedYear() {
        DataTable datatable;
        List<WorkOrderVO> list;
        
        closedWorkOrderList.clear();
        if (selectedYear != null && selectedYear < maxSelectedYear) {
            selectedYear = ++selectedYear;
        
            // reset datatable filters
            try {
                datatable = (DataTable)FacesContext.getCurrentInstance().getViewRoot().findComponent("closedWorkOrderFormId:closedWorkOrderTableId");
                datatable.reset();
            } catch (Exception e) {
                Logger.getLogger(ClosedWorkOrderBean.class.getName()).log(Level.WARNING, e.getMessage(), e);
            }
            
            try {
                list = WorkOrderClient.getInstance().getClosedWorkOrdersByCustomerSiteYear(ServiceRegistrarClient.getInstance().getServiceHostURL(WORK_ORDER_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), userManageBean.getCurrentSite().getSiteId(), selectedYear);
                closedWorkOrderList.addAll(list.stream().sorted(Comparator.comparing(WorkOrderVO::getFormattedCloseDateString).reversed()).collect(Collectors.toList()));
            } catch (Exception e) {
                Logger.getLogger(ClosedWorkOrderBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
                closedWorkOrderList.clear();
            }
        }
    }
    
    
    /**
     * Decrement the year and look up the closed work orders for the new year
     */
    public void decrementSelectedYear() {
        DataTable datatable;
        List<WorkOrderVO> list;
        
        closedWorkOrderList.clear();
        if (selectedYear != null && selectedYear > MIN_SELECTED_YEAR) {
            selectedYear = --selectedYear;
        
            // reset datatable filters
            try {
                datatable = (DataTable)FacesContext.getCurrentInstance().getViewRoot().findComponent("closedWorkOrderFormId:closedWorkOrderTableId");
                datatable.reset();
            } catch (Exception e) {
                Logger.getLogger(ClosedWorkOrderBean.class.getName()).log(Level.WARNING, e.getMessage(), e);
            }
            
            try {
                list = WorkOrderClient.getInstance().getClosedWorkOrdersByCustomerSiteYear(ServiceRegistrarClient.getInstance().getServiceHostURL(WORK_ORDER_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), userManageBean.getCurrentSite().getSiteId(), selectedYear);
                closedWorkOrderList.addAll(list.stream().sorted(Comparator.comparing(WorkOrderVO::getFormattedCloseDateString).reversed()).collect(Collectors.toList()));
            } catch (Exception e) {
                Logger.getLogger(ClosedWorkOrderBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
                closedWorkOrderList.clear();
            }
        }
    }

    /**
     * Return the Priority List
     * @return List Object of SelectItem Objects
     */
    public List<SelectItem> getPriorityList() {
        try {
            return priorityList.stream().filter(item -> item.getValue() != null && closedWorkOrderList.stream().anyMatch(vo -> vo.getPriority() == ((byte)item.getValue()))).collect(Collectors.toList());
        } catch (Exception e) {
            Logger.getLogger(ClosedWorkOrderBean.class.getName()).log(Level.WARNING, e.getMessage(), e);
        }
        return new ArrayList();
    }

    /**
     * Return the Issue List
     * @return List Object of SelectItem Objects
     */
    public List<SelectItem> getIssueList() {
        try {
            return issueList.stream().filter(item -> item.getValue() != null && closedWorkOrderList.stream().anyMatch(vo -> vo.getIssue() != null && vo.getIssue().equals((String)item.getValue()))).collect(Collectors.toList());
        } catch (Exception e) {
            Logger.getLogger(ClosedWorkOrderBean.class.getName()).log(Level.WARNING, e.getMessage(), e);
        }
        return new ArrayList();
    }

    /**
     * Return the Action List
     * @return List Object of SelectItem Objects
     */
    public List<SelectItem> getActionList() {
        try {
            return actionList.stream().filter(item -> item.getValue() != null && closedWorkOrderList.stream().anyMatch(vo -> vo.getAction() != null && vo.getAction().equals((String)item.getValue()))).collect(Collectors.toList());
        } catch (Exception e) {
            Logger.getLogger(ClosedWorkOrderBean.class.getName()).log(Level.WARNING, e.getMessage(), e);
        }
        return new ArrayList();
    }
    
    public Short getSelectedYear() {
        return selectedYear;
    }

    public List<WorkOrderVO> getClosedWorkOrderList() {
        return closedWorkOrderList;
    }

    public short getMinSelectedYear() {
        return MIN_SELECTED_YEAR;
    }

    public short getMaxSelectedYear() {
        return maxSelectedYear;
    }

    public List<FilterMeta> getFilterBy() {
        return filterBy;
    }

    public void setFilterBy(List<FilterMeta> filterBy) {
        this.filterBy = filterBy;
    }

    public List<WorkOrderVO> getFilteredClosedWorkOrderList() {
        return filteredClosedWorkOrderList;
    }

    public void setFilteredClosedWorkOrderList(List<WorkOrderVO> filteredClosedWorkOrderList) {
        this.filteredClosedWorkOrderList = filteredClosedWorkOrderList;
    }

    public String getActionLabelByValue(String value) {
        return WorkOrderActionEnum.getActionLabelByValue(value);
    }
    
    public String getIssueLabelByValue(String value) {
        return WorkOrderIssueEnum.getIssueLabelByValue(value);
    }
    
    public String getPriorityLabelByValue(byte value) {
        return WorkOrderPriorityEnum.getPriorityLabelByValue(value);
    }
}
