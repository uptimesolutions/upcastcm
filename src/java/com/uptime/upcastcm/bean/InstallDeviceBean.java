/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.featuresflag.enums.UptimeFeaturesEnum;
import com.uptime.upcastcm.http.client.APALSetClient;
import com.uptime.upcastcm.http.client.PointClient;
import com.uptime.upcastcm.http.client.DevicePresetClient;
import com.uptime.upcastcm.http.client.HwUnitToPointClient;
import com.uptime.upcastcm.http.client.ManageDeviceClient;
import com.uptime.upcastcm.http.client.PointLocationClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import com.uptime.upcastcm.utils.helperclass.Filters;
import com.uptime.upcastcm.utils.helperclass.Utils;
import static com.uptime.upcastcm.utils.helperclass.Utils.distinctByKeys;
import com.uptime.upcastcm.utils.interfaces.PresetDialogFields;
import com.uptime.upcastcm.utils.singletons.CommonUtilSingleton;
import com.uptime.upcastcm.utils.vo.ApAlSetVO;
import com.uptime.upcastcm.utils.vo.PointVO;
import com.uptime.upcastcm.utils.vo.DevicePresetVO;
import com.uptime.upcastcm.utils.vo.DeviceVO;
import com.uptime.upcastcm.utils.vo.HwUnitVO;
import com.uptime.upcastcm.utils.vo.PointLocationVO;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.faces.model.SelectItemGroup;
import org.primefaces.PrimeFaces;

/**
 *
 * @author kpati
 */
public class InstallDeviceBean implements Serializable, PresetDialogFields {

    private UserManageBean userManageBean;
    private NavigationBean navigationBean;

    private final List<PointVO> pointVOList;
    private final List<DevicePresetVO> devicePresetsList, selectedSiteDevicePresetList;
    private final List<SelectItem> devicePresetsSelectItemList;
    private final List<Integer> acChannelNums, dcChannelNums;

    private DevicePresetVO modifiedDevicePresetVO;
    private PointLocationVO pointLocationVO;
    private DeviceVO installDevice;
    private String serialNumber, serialNumberInput, existDeviceSerialNumber, newDeviceType, submitSuccess;
    private UUID selectedPresetId;
    private boolean displayPoint, displayChannelNumDropDown, tachValidationFailed;

    /**
     * Constructor
     */
    public InstallDeviceBean() {
        pointVOList = new ArrayList();
        devicePresetsSelectItemList = new ArrayList();
        devicePresetsList = new ArrayList();
        selectedSiteDevicePresetList = new ArrayList();
        acChannelNums = new ArrayList();
        dcChannelNums = new ArrayList();
        acChannelNums.addAll(CommonUtilSingleton.getInstance().getAcChannelNumbersList());
        dcChannelNums.addAll(CommonUtilSingleton.getInstance().getDcChannelNumbersList());

        try {
            userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
            navigationBean = (NavigationBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("navigationBean");
        } catch (Exception e) {
            Logger.getLogger(InstallDeviceBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            userManageBean = null;
            navigationBean = null;
        }

    }

    /**
     * Post Constructor
     */
    @PostConstruct
    public void init() {
        if (userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null) {
            Logger.getLogger(InstallDeviceBean.class.getName()).log(Level.INFO, "**init InstallDeviceBean called : {0}", userManageBean.getCurrentSite().getCustomerAccount());
        }
        resetPage();
    }

    @Override
    public void resetPage() {
        devicePresetsSelectItemList.clear();
        selectedSiteDevicePresetList.clear();
        devicePresetsList.clear();
        pointVOList.clear();
        serialNumber = null;
        serialNumberInput = null;
        newDeviceType = null;
        displayPoint = false;
        displayChannelNumDropDown = false;
        existDeviceSerialNumber = null;
        selectedPresetId = null;
        pointLocationVO = null;
        installDevice = new DeviceVO();
        submitSuccess = "";
        modifiedDevicePresetVO = null;
        tachValidationFailed = false;
    }

    @Override
    public void presetFields(Filters presets, Object object) {

        try {
            if (presets != null && userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null) {
                this.pointLocationVO = new PointLocationVO((PointLocationVO) object);
                installDevice.setCustomerAccount(userManageBean.getCurrentSite().getCustomerAccount());
                installDevice.setSiteId(userManageBean.getCurrentSite().getSiteId());
                installDevice.setSiteName(userManageBean.getCurrentSite().getSiteName());

                if (presets.getArea() != null) {
                    installDevice.setAreaId((UUID) presets.getArea().getValue());
                    installDevice.setAreaName(presets.getArea().getLabel());
                }

                if (presets.getAsset() != null) {
                    installDevice.setAssetId((UUID) presets.getAsset().getValue());
                    installDevice.setAssetName(presets.getAsset().getLabel());
                }

                if (presets.getPointLocation() != null) {
                    installDevice.setPointLocationId((UUID) presets.getPointLocation().getValue());
                    installDevice.setPointLocationName(presets.getPointLocation().getLabel());
                }

                installDevice.setBasestationPortNum(pointLocationVO.getBasestationPortNum());

                pointVOList.addAll(PointClient.getInstance().getPointsByCustomerSiteAreaAssetPointLocation(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), installDevice.getSiteId(), installDevice.getAreaId(), installDevice.getAssetId(), installDevice.getPointLocationId(), true));

                if (!pointVOList.isEmpty() && pointLocationVO != null && pointLocationVO.getDeviceSerialNumber() != null && !pointLocationVO.getDeviceSerialNumber().isEmpty()) {
                    existDeviceSerialNumber = pointLocationVO.getDeviceSerialNumber();
                }
            }
        } catch (Exception e) {
            Logger.getLogger(InstallDeviceBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    @Override
    public void presetFields(int operationType, Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void presetFields(Object object) {
        DevicePresetVO originalDevicePresetVO;
        int itemIndex;
        
        try {
            modifiedDevicePresetVO = null;
            if ((object instanceof DevicePresetVO)) {
                modifiedDevicePresetVO = (DevicePresetVO) object;
            }

            // Update selectedSiteDevicePresetList feild
            if (modifiedDevicePresetVO != null) {
                //find the Original Object that has been modified.
                originalDevicePresetVO = selectedSiteDevicePresetList.stream().filter(dp -> dp.getPresetId().compareTo(modifiedDevicePresetVO.getPresetId()) == 0 && dp.getChannelNumber() == modifiedDevicePresetVO.getChannelNumber() && dp.getDeviceType().equals(modifiedDevicePresetVO.getDeviceType()) && dp.getChannelType().equals(modifiedDevicePresetVO.getChannelType())).findFirst().orElse(null);

                //find the index of the Object that has been modified.
                if ((itemIndex = selectedSiteDevicePresetList.indexOf(originalDevicePresetVO)) != -1) {
                    //replace the Object
                    selectedSiteDevicePresetList.set(itemIndex, modifiedDevicePresetVO);
                }
            }


            // set tachValidationFailed field
            // Check if AP Sets require a tach and if a tach is provided
            tachValidationFailed = false;
            outerloop:
            for (DevicePresetVO vo: selectedSiteDevicePresetList) {
                for (ApAlSetVO al: vo.getAlSetVOList()) {
                    if ((al.getFrequencyUnits() != null && al.getFrequencyUnits().equalsIgnoreCase("Orders")) || Utils.validateParamList().contains(al.getParamType())) {
                        tachValidationFailed = pointLocationVO.getTachId() == null;

                        if (tachValidationFailed) {
                            break outerloop;
                        }
                    }
                }
            }
            
        } catch (Exception e) {
            Logger.getLogger(InstallDeviceBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
        
        //update UI
        PrimeFaces.current().ajax().update("installDeviceFormId");
    }


    /**
     * Sets the given field regionCountrySiteMap
     */
    public void updateBean() {
        Logger.getLogger(InstallDeviceBean.class.getName()).log(Level.INFO, "** updateBean is called: ");
    }

    /**
     * Validate the Serial Number and the point associated with it
     */
    public void validateSerialNum() {
        Logger.getLogger(InstallDeviceBean.class.getName()).log(Level.INFO, "**serialNumber to be set : {0}", serialNumber);
        List<DevicePresetVO> devicePresetVOList, totalDevicePresetVOList;
        List<ApAlSetVO> siteApAlSetsVOList;
        Pattern pattern;
        List<HwUnitVO> hwUnitVOlist;

        try {
            serialNumber = serialNumberInput;
            devicePresetsList.clear();
            devicePresetsSelectItemList.clear();
            selectedPresetId = null;
            newDeviceType = null;
            displayPoint = false;
            if (userManageBean != null && userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null && serialNumber != null && !serialNumber.isEmpty()) {
                if (UptimeFeaturesEnum.UptimeFeatures.DISPLAY_STORMX.isActive()) {
                    pattern = Pattern.compile(REGEX_INCLUDE_STORM_SERIAL_NUMBER);
                } else {
                    pattern = Pattern.compile(REGEX_EXCLUDE_STORM_SERIAL_NUMBER);
                }
                if (serialNumber.startsWith("001")) {
                    FacesContext.getCurrentInstance().addMessage("nonAutoGrowlMessages", new FacesMessage(userManageBean.getResourceBundleString("growl_install_device_validate")));
                } else {
                    if (pattern.matcher(serialNumber).matches()) {
                        if ((newDeviceType = CommonUtilSingleton.getInstance().getDeviceType(serialNumber)) != null && !newDeviceType.isEmpty()) {

                            // Check if the serial number to be installed already associated with other point location.
                            if ((hwUnitVOlist = HwUnitToPointClient.getInstance().getHwUnitToPointByDevice(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), serialNumber)) != null && !hwUnitVOlist.isEmpty()) {
                                PrimeFaces.current().executeScript("PF('attSLNWithOthrPLDlg').show();");
                            } else {

                                // Check if Point exists
                                if (!pointVOList.isEmpty()) {

                                    // Check if the Point List is valid for the given deviceType
                                    if (!CommonUtilSingleton.getInstance().isValidPointListForInstallNewDevice(pointVOList, serialNumber)) {
                                        PrimeFaces.current().executeScript("PF('installDeviceNoValidPointDlg').show();");
                                    } else {
                                        //For StormX deviceType, Check if channel number are missing.
                                        if (newDeviceType.equals("StormX")) {
                                            //fetch the fresh pointList as this method is being called again.
                                            if (displayChannelNumDropDown) {
                                                pointVOList.clear();
                                                pointVOList.addAll(PointClient.getInstance().getPointsByCustomerSiteAreaAssetPointLocation(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), installDevice.getSiteId(), installDevice.getAreaId(), installDevice.getAssetId(), installDevice.getPointLocationId(), true));
                                            }
                                            displayChannelNumDropDown = !CommonUtilSingleton.getInstance().isMissingChannelNumberForStormXDevice(pointVOList, serialNumber);
                                        }

                                        for (PointVO pointVO : pointVOList) {
                                            // Set required point fields
                                            CommonUtilSingleton.getInstance().updatePointForInstallNewDevice(pointVO, serialNumber);

                                            // Sets the ApSet Names and the AlSet Names for the each pointVO in the field pointVOList
                                            if (pointVO.getApSetId() != null && pointVO.getAlSetId() != null) {
                                                if ((siteApAlSetsVOList = APALSetClient.getInstance().getSiteApAlSetsByCustomerSiteIdApSetIdAlSetId(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), pointVO.getSiteId(), pointVO.getApSetId(), pointVO.getAlSetId())) == null || siteApAlSetsVOList.isEmpty()) {
                                                    siteApAlSetsVOList = APALSetClient.getInstance().getGlobalApAlSetsByCustomerApSetIdAlSetId(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), pointVO.getApSetId(), pointVO.getAlSetId());
                                                    if (siteApAlSetsVOList == null || siteApAlSetsVOList.isEmpty()) {
                                                        siteApAlSetsVOList = APALSetClient.getInstance().getGlobalApAlSetsByCustomerApSetIdAlSetId(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), DEFAULT_UPTIME_ACCOUNT, pointVO.getApSetId(), pointVO.getAlSetId());
                                                    }
                                                }
                                                if (siteApAlSetsVOList != null && !siteApAlSetsVOList.isEmpty()) {
                                                    pointVO.setApSetName(siteApAlSetsVOList.get(0).getApSetName());
                                                    pointVO.setAlSetName(siteApAlSetsVOList.get(0).getAlSetName());
                                                }
                                            }
                                        }

                                        CommonUtilSingleton.getInstance().sortPointsByPointType(pointVOList);
                                        if (displayChannelNumDropDown) {
                                            //setting the valid channel num for all AC (Acceleration) channel.
                                            pointVOList.stream().filter(p -> (p.getPointType().equals("AC"))).forEachOrdered(p -> {
                                                p.setSensorChannelNum(p.getPointSerialNumber() + 4);
                                            });
                                        }
                                        installDevice.setPointList(pointVOList);
                                        installDevice.setNewDeviceId(serialNumber);
                                        installDevice.setDbpointListExists(true);
                                        displayPoint = existDeviceSerialNumber == null || existDeviceSerialNumber.isEmpty();
                                    }

                                } else if (userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null) {
                                    installDevice.setNewDeviceId(serialNumber);

                                    totalDevicePresetVOList = null;

                                    // Get Uptime Device Presets
                                    if (!userManageBean.getCurrentSite().getCustomerAccount().equals(DEFAULT_UPTIME_ACCOUNT)) {
                                        devicePresetVOList = DevicePresetClient.getInstance().getGlobalDevicePresetsByCustomer(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), DEFAULT_UPTIME_ACCOUNT);
                                        if ((devicePresetVOList = devicePresetVOList.stream().filter(dpVO -> dpVO.getDeviceType().equals(newDeviceType)).collect(Collectors.toList())) != null && !devicePresetVOList.isEmpty()) {
                                            totalDevicePresetVOList = new ArrayList();
                                            totalDevicePresetVOList.addAll(devicePresetVOList);
                                        }
                                    }

                                    // Get Global Device Presets
                                    devicePresetVOList = DevicePresetClient.getInstance().getGlobalDevicePresetsByCustomer(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount());
                                    if ((devicePresetVOList = devicePresetVOList.stream().filter(dpVO -> dpVO.getDeviceType().equals(newDeviceType)).collect(Collectors.toList())) != null && !devicePresetVOList.isEmpty()) {
                                        if (totalDevicePresetVOList == null) {
                                            totalDevicePresetVOList = new ArrayList();
                                        }
                                        totalDevicePresetVOList.addAll(devicePresetVOList);
                                    }

                                    if (totalDevicePresetVOList != null && !totalDevicePresetVOList.isEmpty()) {
                                        devicePresetsList.addAll(totalDevicePresetVOList);
                                        groupAndAddToDevicePresetsSelectItemList(totalDevicePresetVOList, userManageBean.getResourceBundleString("label_global_device_preset"));
                                    }

                                    // Get Site Device Presets
                                    if ((devicePresetVOList = DevicePresetClient.getInstance().getSiteDeviceByCustomerSiteDeviceType(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), installDevice.getSiteId().toString(), newDeviceType)) != null && !devicePresetVOList.isEmpty()) {
                                        devicePresetsList.addAll(devicePresetVOList);
                                        groupAndAddToDevicePresetsSelectItemList(devicePresetVOList, userManageBean.getResourceBundleString("label_site_device_preset"));
                                    }
                                }
                            }
                        }
                    } else {
                        invalidSerialNumber();
                    }
                }
            }
        } catch (Exception e) {
            Logger.getLogger(InstallDeviceBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            devicePresetsList.clear();
            devicePresetsSelectItemList.clear();
            selectedPresetId = null;
            newDeviceType = null;
            displayPoint = false;
            serialNumber = null;
        }
    }

    /**
     * Reset values when Serial Number is changed
     */
    public void onSerialNumberChange() {
        if (!serialNumberInput.equalsIgnoreCase(serialNumber)) {
            devicePresetsList.clear();
            devicePresetsSelectItemList.clear();
            selectedPresetId = null;
            newDeviceType = null;
            serialNumber = serialNumberInput;

            if (displayPoint || !selectedSiteDevicePresetList.isEmpty()) {
                selectedSiteDevicePresetList.clear();
                displayPoint = false;
                PrimeFaces.current().ajax().update("installDeviceFormId");
            }
        }
    }

    /**
     * Set values when Ac Channel Number is selected.
     *
     * @param pointVO, PointVO Object.
     */
    public void onACChannelNumSelect(PointVO pointVO) {
        Logger.getLogger(InstallDeviceBean.class.getName()).log(Level.INFO, "** updateBean onACChannelNumSelect is called pointVO.getSensorChannelNum: {0}", pointVO.getSensorChannelNum());
    }

    /**
     * Display message when Serial Number is invalid and reset values.
     */
    private void invalidSerialNumber() {
        onSerialNumberChange();
        FacesContext.getCurrentInstance().addMessage("growlMessages", new FacesMessage(userManageBean.getResourceBundleString("growl_valid_device_id")));
    }

    /**
     * This is a helper method for the method validateSerialNum. Group and Sort
     * the given list, add the group to the field devicePresetsSelectItemList
     *
     * @param devicePresetVOs
     * @param dpType
     * @return
     */
    private void groupAndAddToDevicePresetsSelectItemList(List<DevicePresetVO> devicePresetVOs, String dpType) {
        SelectItem[] dpSetArr;
        SelectItemGroup apSetGroup;
        List<SelectItem> dpSetList;

        if (devicePresetVOs != null && !devicePresetVOs.isEmpty()) {

            // Verfy the items in the list are distinced and sort be the Device Name
            devicePresetVOs = devicePresetVOs.stream().filter(distinctByKeys(DevicePresetVO::getName, DevicePresetVO::getDeviceType, DevicePresetVO::getPresetId)).collect(Collectors.toList());
            devicePresetVOs.sort(Comparator.comparing(DevicePresetVO::getName));

            // Create Group List of SelectItem Objects
            dpSetArr = new SelectItem[devicePresetVOs.size()];
            for (int i = 0; i < devicePresetVOs.size(); i++) {
                dpSetArr[i] = new SelectItem(devicePresetVOs.get(i).getPresetId(), devicePresetVOs.get(i).getName());
            }
            apSetGroup = new SelectItemGroup(dpType); // Set Group Name
            apSetGroup.setSelectItems(dpSetArr); // Add Items to group
            dpSetList = new ArrayList();
            dpSetList.add(apSetGroup); // Add Group to List

            devicePresetsSelectItemList.addAll(dpSetList);
        }
    }

    /**
     * Submit new install device and close overlay
     */
    public void submit() {
        List<PointVO> list;
        //Pattern pattern = Pattern.compile(REGEX_SERIAL_NUMBER);
        Pattern pattern;
        boolean isValidStormXChhanel;
        if (UptimeFeaturesEnum.UptimeFeatures.DISPLAY_STORMX.isActive()) {
            pattern = Pattern.compile(REGEX_INCLUDE_STORM_SERIAL_NUMBER);
        } else {
            pattern = Pattern.compile(REGEX_EXCLUDE_STORM_SERIAL_NUMBER);
        }
        if (pattern.matcher(serialNumber).matches()) {
            try {
                if (installDevice.getCustomerAccount() != null && !installDevice.getCustomerAccount().isEmpty()
                        && installDevice.getSiteId() != null && installDevice.getAreaId() != null
                        && installDevice.getAssetId() != null && installDevice.getPointLocationId() != null) {
                    //Default value for all deviceType
                    isValidStormXChhanel = true;
                    if (selectedSiteDevicePresetList != null && !selectedSiteDevicePresetList.isEmpty()) {
                        list = new ArrayList();
                        selectedSiteDevicePresetList.stream().forEachOrdered(dpVO -> {
                            PointVO pointVO;

                            pointVO = new PointVO();
                            pointVO.setCustomerAccount(installDevice.getCustomerAccount());
                            pointVO.setSiteId(installDevice.getSiteId());
                            pointVO.setAlSetId(dpVO.getAlSetId());
                            pointVO.setAlSetName(dpVO.getAlSetName());
                            pointVO.setAlarmEnabled(dpVO.isAlarmEnabled());
                            pointVO.setApSetId(dpVO.getApSetId());
                            pointVO.setApSetName(dpVO.getApSetName());
                            pointVO.setAreaId(installDevice.getAreaId());
                            pointVO.setAssetId(installDevice.getAssetId());
                            pointVO.setPointLocationId(installDevice.getPointLocationId());
                            pointVO.setDisabled(dpVO.isDisabled());
                            pointVO.setPointName(dpVO.getPointName());
                            pointVO.setPointType(dpVO.getChannelType());
                            pointVO.setSensorChannelNum(dpVO.getChannelNumber());
                            pointVO.setSensorOffset(dpVO.getSensorOffset());
                            pointVO.setSensorSensitivity(dpVO.getSensorSensitivity());
                            pointVO.setSensorType(dpVO.getSensorType());
                            pointVO.setSensorUnits(dpVO.getSensorUnits());
                            if (newDeviceType.equals("StormX")) {
                                pointVO.setSensorSubType(dpVO.getSensorSubType());
                                pointVO.setSensorLocalOrientation(dpVO.getSensorLocalOrientation());
                            }
                            list.add(pointVO);
                        });

                        installDevice.setPointList(list);
                        installDevice.setDbpointListExists(false);
                        installDevice.setDeviceType(newDeviceType);
                    } else {
                        installDevice.getPointList().stream().forEachOrdered(point -> point.setApAlSetVOs(null));
                        if (newDeviceType.equals("StormX") && displayChannelNumDropDown) {
                            if (isValidStormXChhanel = isValidSelectedChannelNumberForAccl(installDevice.getPointList())) {
                                installDevice.getPointList().forEach(p -> {
                                    if (p.getSensorType().equals("Acceleration")) {
                                        if (p.getSensorLocalOrientation() == null || p.getSensorLocalOrientation().isEmpty()) {
                                            updateStormXChannelNumSpecificFields(p);
                                        }
                                    }
                                });
                            }
                        }

                    }
                    if (isValidStormXChhanel) {
                        if (ManageDeviceClient.getInstance().installDevice(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), installDevice)) {
                            if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("configurationBean") == null) {
                                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("configurationBean", new ConfigurationBean());
                                ((ConfigurationBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("configurationBean")).init();
                            }
                            ((ConfigurationBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("configurationBean")).onPointLocationChange(true);
                            submitSuccess = "success";
                            PrimeFaces.current().executeScript("updateTreeNodes()");
                        } else {
                            Logger.getLogger(InstallDeviceBean.class.getName()).log(Level.INFO, "Can't install installDevice*********** {0}", installDevice.toString());
                        }
                    } else {
                        FacesContext.getCurrentInstance().addMessage("nonAutoGrowlMessages", new FacesMessage(userManageBean.getResourceBundleString("growl_install_device_channelnum_validate")));
                        Logger.getLogger(InstallDeviceBean.class.getName()).log(Level.INFO, "Can't install installDevice as isValidStormXChhanel*********** {0}", isValidStormXChhanel);
                    }
                }
            } catch (Exception e) {
                submitSuccess = "";
                Logger.getLogger(InstallDeviceBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            }
        } else {
            submitSuccess = "";
            Logger.getLogger(InstallDeviceBean.class.getName()).log(Level.INFO, "****onSerialNumberChange called**********{0}", serialNumber);
            invalidSerialNumber();
        }
    }

    /**
     * Delete Current PointList and close overlay
     */
    public void deletePointList() {
        if (pointLocationVO != null) {
            pointLocationVO.setPointList(pointVOList);
            if (PointLocationClient.getInstance().deleteAllPointsPointLocation(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), pointLocationVO)) {
                if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("configurationBean") == null) {
                    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("configurationBean", new ConfigurationBean());
                    ((ConfigurationBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("configurationBean")).init();
                }
                ((ConfigurationBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("configurationBean")).onPointLocationChange(true);
                PrimeFaces.current().executeScript("PF('defaultDlg').hide()");
                PrimeFaces.current().executeScript("updateTreeNodes()");
            } else {
                Logger.getLogger(CreateSiteBean.class.getName()).log(Level.INFO, "Can't delete pointList size*********** {0}", new Object[]{pointLocationVO.getPointList().size()});
            }
        } else {
            Logger.getLogger(CreateSiteBean.class.getName()).log(Level.INFO, "Can't delete pointList*********** {0}", "PointLocation is Null");
        }
    }

    /**
     * Edit the given Point
     *
     * @param devicePresetVO, DevicePresetVO Object
     */
    public void edit(DevicePresetVO devicePresetVO) {
        if (devicePresetVO != null) {
            if ((EditInstallDevicePresetsBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("editInstallDevicePresetsBean") == null) {
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("editInstallDevicePresetsBean", new EditInstallDevicePresetsBean());
                ((EditInstallDevicePresetsBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("editInstallDevicePresetsBean")).init();
            }
            ((EditInstallDevicePresetsBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("editInstallDevicePresetsBean")).presetFields(devicePresetVO, pointLocationVO, true);
            navigationBean.updateSecondDialog("edit-installdevice-presets");
            PrimeFaces.current().executeScript("PF('defaultSecondDlg').show()");
        }
    }


    /**
     * perform the given action when a Device Preset is selected
     */
    public void onDevicePresetSelect() {
        selectedSiteDevicePresetList.clear();
        tachValidationFailed = false;

        try {
            if (selectedPresetId != null) {
                selectedSiteDevicePresetList.addAll(devicePresetsList.stream().filter(dp -> dp.getPresetId() != null && dp.getPresetId().equals(selectedPresetId)).collect(Collectors.toList()));
                if (!selectedSiteDevicePresetList.isEmpty() && !selectedSiteDevicePresetList.get(0).getSensorType().equals("TS1X")) {
                    selectedSiteDevicePresetList.stream().forEachOrdered(vo -> {
                        List<ApAlSetVO> apAlSetVOList;

                        vo.setPointName(pointLocationVO.getPointLocationName() + " " + CommonUtilSingleton.getInstance().getPointNameBySensorType(vo.getSensorType()));
                        vo.setSensorUnits(CommonUtilSingleton.getInstance().getSensorUnitBySensorType(vo.getSensorType()));
                        vo.setApAlSetVOList(new ArrayList());
                        vo.setApAlSetSelectItemList(new ArrayList());
                        if ((apAlSetVOList = APALSetClient.getInstance().getGlobalApAlSetsByCustomerApSetIdAlSetId(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), vo.getCustomerAccount(), vo.getApSetId(), vo.getAlSetId())) != null && !apAlSetVOList.isEmpty()) {
                            vo.setApSetName(apAlSetVOList.get(0).getApSetName());
                            vo.setAlSetName(apAlSetVOList.get(0).getAlSetName());
                        } else if ((apAlSetVOList = APALSetClient.getInstance().getGlobalApAlSetsByCustomerApSetIdAlSetId(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), DEFAULT_UPTIME_ACCOUNT, vo.getApSetId(), vo.getAlSetId())) != null && !apAlSetVOList.isEmpty()) {
                            vo.setApSetName(apAlSetVOList.get(0).getApSetName());
                            vo.setAlSetName(apAlSetVOList.get(0).getAlSetName());
                        } else if (installDevice.getSiteId() != null && (apAlSetVOList = APALSetClient.getInstance().getSiteApAlSetsByCustomerSiteIdApSetIdAlSetId(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), vo.getCustomerAccount(), installDevice.getSiteId(), vo.getApSetId(), vo.getAlSetId())) != null && !apAlSetVOList.isEmpty()) {
                            vo.setApSetName(apAlSetVOList.get(0).getApSetName());
                            vo.setAlSetName(apAlSetVOList.get(0).getAlSetName());
                        }
                        
                        vo.setAlSetVOList(apAlSetVOList.stream().filter(ap -> ap.getApSetId() != null && ap.getApSetId().equals(vo.getApSetId())).filter(ap -> ap.getAlSetId() != null && ap.getAlSetId().equals(vo.getAlSetId())).collect(Collectors.toList()));
                        
                        //Check if AP Set requires a tach and if a tach is provided
                        if (!tachValidationFailed) {
                            for (ApAlSetVO al: vo.getAlSetVOList()) {
                                if ((al.getFrequencyUnits() != null && al.getFrequencyUnits().equalsIgnoreCase("Orders")) || Utils.validateParamList().contains(al.getParamType())) {
                                    tachValidationFailed = pointLocationVO.getTachId() == null;

                                    if (tachValidationFailed) {
                                        break;
                                    }
                                }
                            }
                        }
                        
                    });
                } else {
                    throw new Exception("Invalid Sensor Type or no items found");
                }
            }

        } catch (Exception e) {
            Logger.getLogger(InstallDeviceBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            selectedSiteDevicePresetList.clear();
        }
    }

    private void updateStormXChannelNumSpecificFields(PointVO pointVO) {
        //set Channel number specific fileds
        switch (pointVO.getSensorChannelNum()) {
            case 5:
                pointVO.setSensorLocalOrientation("X");
                break;
            case 6:
                pointVO.setSensorLocalOrientation("Y");
                break;
            case 7:
                pointVO.setSensorLocalOrientation("Z");
                break;
            default:
                break;
        }
    }

    /**
     * Is selected channel are unique
     *
     * @param List<PointVO>, List of PointVO
     */
    private boolean isValidSelectedChannelNumberForAccl(List<PointVO> pointVOList) {
        final Set<Integer> channelnumberSet = new HashSet();
        pointVOList.stream().filter(p -> (p.getSensorType().equals("Acceleration"))).forEachOrdered(p -> {
            channelnumberSet.add(p.getSensorChannelNum());
        });
        return channelnumberSet.size() == 3;
    }

    public String getSerialNumberInput() {
        return serialNumberInput;
    }

    public void setSerialNumberInput(String serialNumberInput) {
        this.serialNumberInput = serialNumberInput;
    }

    public List<DevicePresetVO> getSelectedSiteDevicePresetList() {
        return selectedSiteDevicePresetList;
    }

    public boolean isDisplayPoint() {
        return displayPoint;
    }

    public boolean isDisplayChannelNumDropDown() {
        return displayChannelNumDropDown;
    }

    public UUID getSelectedPresetId() {
        return selectedPresetId;
    }

    public void setSelectedPresetId(UUID selectedPresetId) {
        this.selectedPresetId = selectedPresetId;
    }

    public List<SelectItem> getDevicePresetsSelectItemList() {
        return devicePresetsSelectItemList;
    }

    public UUID getSiteId() {
        return installDevice.getSiteId();
    }

    public void setSiteId(UUID siteId) {
        installDevice.setSiteId(siteId);
    }

    public List<PointVO> getPointVOList() {
        return pointVOList;
    }

    public List<DevicePresetVO> getDevicePresetsList() {
        return devicePresetsList;
    }

    public String getExistDeviceSerialNumber() {
        return existDeviceSerialNumber;
    }

    public void setExistDeviceSerialNumber(String existDeviceSerialNumber) {
        this.existDeviceSerialNumber = existDeviceSerialNumber;
    }

    public String getSubmitSuccess() {
        return submitSuccess;
    }

    public void setSubmitSuccess(String submitSuccess) {
        this.submitSuccess = submitSuccess;
    }

    public List<Integer> getAcChannelNums() {
        return acChannelNums;
    }

    public List<Integer> getDcChannelNums() {
        return dcChannelNums;
    }

    public boolean isTachValidationFailed() {
        return tachValidationFailed;
    }

}
