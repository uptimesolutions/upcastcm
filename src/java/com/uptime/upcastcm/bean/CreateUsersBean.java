/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import static com.uptime.client.http.servlet.AbstractServiceManagerServlet.getPropertyValue;
import com.uptime.upcastcm.http.client.LdapClient;
import com.uptime.upcastcm.http.client.UserClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import static com.uptime.upcastcm.utils.EmailUtil.createAndSendPasswordResetEmail;
import static com.uptime.upcastcm.utils.enums.UserGroupEnum.getListOfSelectItemsFromSiteItems;
import com.uptime.upcastcm.utils.vo.LdapUserVO;
import com.uptime.upcastcm.utils.vo.SiteUsersVO;
import com.uptime.upcastcm.utils.vo.UserPreferencesVO;
import java.io.Serializable;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.application.FacesMessage;
import javax.faces.model.SelectItem;
import org.primefaces.PrimeFaces;

/**
 *
 * @author twilcox
 */
public class CreateUsersBean implements Serializable {
    private UserManageBean userManageBean;
    private SiteUsersVO siteUsersVO;
    private LdapUserVO ldapUserVO;
    private boolean addToLDAP;

    private final List<SelectItem> userRoles;

    /**
     * Constructor
     */
    public CreateUsersBean() {
        userRoles = new ArrayList();

        try {
            userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
            userRoles.addAll(getListOfSelectItemsFromSiteItems());
        } catch (Exception e) {
            Logger.getLogger(CreateUsersBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            userManageBean = null;
        }
    }

    /**
     * Post Constructor
     */
    @PostConstruct
    public void init() {
        resetPage();
    }

    /**
     * Resets the values of this overlay page
     */
    public void resetPage() {
        siteUsersVO = new SiteUsersVO();
        ldapUserVO = null;
        addToLDAP = false;
    }

    /**
     * Check user exists in Ldap
     */
    public void isUserExist() {
        List<SiteUsersVO> cassUserList;

        // User exist in Ldap. Now check in C* 
        if (userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null && (ldapUserVO = LdapClient.getInstance().getUserAccountByUserId(ServiceRegistrarClient.getInstance().getServiceHostURL(LDAP_SERVICE_NAME), siteUsersVO.getUserId(), userManageBean.getCurrentSite().getCustomerAccount())) != null) {
            if ((cassUserList = UserClient.getInstance().getSiteUsersByPK(ServiceRegistrarClient.getInstance().getServiceHostURL(USER_PREFERENCE_SERVICE_NAME), siteUsersVO.getUserId(), userManageBean.getCurrentSite().getSiteId().toString(), userManageBean.getCurrentSite().getCustomerAccount())) != null && !cassUserList.isEmpty()) { // user exist in C* too. 
                FacesContext.getCurrentInstance().addMessage("growlMessages", new FacesMessage(userManageBean.getResourceBundleString("growl_user_already_exist") + siteUsersVO.getUserId()));

                siteUsersVO = new SiteUsersVO();
                PrimeFaces.current().ajax().update("createUsersFormId:EmailId");
                PrimeFaces.current().ajax().update("createUsersFormId:fnId");
                PrimeFaces.current().ajax().update("createUsersFormId:lnId");
                PrimeFaces.current().ajax().update("createUsersFormId:roleId");
            } // User does not exist in C* 
            else {
                siteUsersVO.setFirstName(ldapUserVO.getFirstName());
                siteUsersVO.setLastName(ldapUserVO.getLastName());
                PrimeFaces.current().ajax().update("createUsersFormId:fnId");
                PrimeFaces.current().ajax().update("createUsersFormId:lnId");
            }
        } // New user. Allow creation.
        else {
            Logger.getLogger(CreateUsersBean.class.getName()).log(Level.INFO, "New User:{0}", siteUsersVO.getUserId());
        }
    }

    /**
     * Create a User
     */
    public void createUser() {
        StringBuilder growlMsg;
        UserPreferencesVO upVO;
        LdapUserVO newUser;
        UsersBean usersBean;
        int userPreference = -1; 
        int ldap = -1; 
        int email = -1;

        if (userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null && userManageBean.getSelectedCustomersVO() != null) {
            siteUsersVO.setCustomerAccount(userManageBean.getCurrentSite().getCustomerAccount());
            siteUsersVO.setSiteId(userManageBean.getCurrentSite().getSiteId());

            if (UserClient.getInstance().createUsers(ServiceRegistrarClient.getInstance().getServiceHostURL(USER_PREFERENCE_SERVICE_NAME), siteUsersVO)) {
                Logger.getLogger(CreateUsersBean.class.getName()).log(Level.INFO, "createUser: User created successfully");

                // Insert into user-preference table, if new user
                if (UserClient.getInstance().getUserPreferencesByPK(ServiceRegistrarClient.getInstance().getServiceHostURL(USER_PREFERENCE_SERVICE_NAME), siteUsersVO.getUserId()) == null) {
                    upVO = new UserPreferencesVO();
                    upVO.setUserId(siteUsersVO.getUserId());
                    upVO.setCustomerAccount(userManageBean.getCurrentSite().getCustomerAccount());
                    upVO.setLocaleDisplayName(DEFAULT_LOCALE_DISPLAY_NAME);
                    upVO.setLanguage(DEFAULT_LOCALE_DISPLAY_NAME);
                    upVO.setTimezone(DEFAULT_TIME_ZONE);
                    if (UserClient.getInstance().createUserPreferences(ServiceRegistrarClient.getInstance().getServiceHostURL(USER_PREFERENCE_SERVICE_NAME), upVO)) {
                        userPreference = 0;
                    } else {
                        userPreference = 1;
                    }
                }
                
                // If user does not exist in Ldap then create.
                if (addToLDAP && ldapUserVO == null) {
                    newUser = new LdapUserVO();
                    newUser.setUserPassword(Instant.now().toString()); // timestamp used as a placeholder unitl the user changes his password
                    newUser.setFirstName(siteUsersVO.getFirstName());
                    newUser.setLastName(siteUsersVO.getLastName());
                    newUser.setUserId(siteUsersVO.getUserId());
                    newUser.setMail(siteUsersVO.getUserId());
                    newUser.setCustomerAccount(userManageBean.getCurrentSite().getCustomerAccount());
                    newUser.setCompany(userManageBean.getSelectedCustomersVO().getCustomerName());

                    // Create LDAP Account
                    Logger.getLogger(CreateUsersBean.class.getName()).log(Level.INFO, "createUser in LDAP - {0}", newUser);
                    if (LdapClient.getInstance().createUsers(ServiceRegistrarClient.getInstance().getServiceHostURL(LDAP_SERVICE_NAME), newUser)) {
                        ldap = 0;
                    } else {
                        ldap = 1;
                    }

                    // Sending password reset email
                    if (createAndSendPasswordResetEmail(newUser, userManageBean.getSelectedCustomersVO().getCustomerAccount(), userManageBean.getSelectedCustomersVO().getFqdn(), getPropertyValue("UPCAST_URL_PROTOCOL"))) {
                        email = 0;
                    } else {
                        email = 1;
                    }
                }
                
                //**********************TO DO*************************
                    // There needed to be a case if a user need 
                    // to be added to SAML for the first time.
                //****************************************************

                usersBean = (UsersBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("usersBean");
                usersBean.getUsersList().add(siteUsersVO);
                usersBean.cloneUsersList();
                PrimeFaces.current().ajax().update("users-form");
                
                // Creating compounded growl message
                growlMsg = new StringBuilder();
                growlMsg.append(userManageBean.getResourceBundleString("growl_user_created"));
                if (userPreference == 0) {
                    growlMsg.append(" ").append(userManageBean.getResourceBundleString("label_created_a_user_preference_entry"));
                } else if (userPreference == 1) {
                    growlMsg.append(" ").append(userManageBean.getResourceBundleString("label_failed_to_add_user_preference_entry"));
                }
                if (ldap == 0) {
                    growlMsg.append(" ").append(userManageBean.getResourceBundleString("label_created_an_account_in_ldap"));
                } else if (ldap == 1) {
                    growlMsg.append(" ").append(userManageBean.getResourceBundleString("label_failed_to_add_account_to_ldap"));
                }
                if (email == 0) {
                    growlMsg.append(" ").append(userManageBean.getResourceBundleString("label_sent_email_successfully"));
                } else if (email == 1) {
                    growlMsg.append(" ").append(userManageBean.getResourceBundleString("label_failed_to_send_email"));
                }
                
                FacesContext.getCurrentInstance().addMessage("nonAutoGrowlMessages", new FacesMessage(growlMsg.toString()));
            } else {
                FacesContext.getCurrentInstance().addMessage("nonAutoGrowlMessages", new FacesMessage(userManageBean.getResourceBundleString("label_failed_to_add_a_site_user")));
                Logger.getLogger(CreateUsersBean.class.getName()).log(Level.INFO, "Error in User creation:{0}", siteUsersVO);
            }
            PrimeFaces.current().executeScript("addDisableFunctionality()");
        }
    }

    public void updateBean() {
        Logger.getLogger(CreateUsersBean.class.getName()).log(Level.INFO, "CreateUsersBean.updateBean() called. ***********");
    }

    public SiteUsersVO getSiteUsersVO() {
        return siteUsersVO;
    }

    public void setSiteUsersVO(SiteUsersVO siteUsersVO) {
        this.siteUsersVO = siteUsersVO;
    }

    public List<SelectItem> getUserRoles() {
        return userRoles;
    }

    public LdapUserVO getLdapUserVO() {
        return ldapUserVO;
    }

    public void setLdapUserVO(LdapUserVO ldapUserVO) {
        this.ldapUserVO = ldapUserVO;
    }

    public boolean isAddToLDAP() {
        return addToLDAP;
    }

    public void setAddToLDAP(boolean addToLDAP) {
        this.addToLDAP = addToLDAP;
    }
}
