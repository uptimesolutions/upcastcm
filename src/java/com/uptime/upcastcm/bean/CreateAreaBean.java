/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.AreaClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import static com.uptime.upcastcm.utils.enums.HourEnum.getHourItemList;
import static com.uptime.upcastcm.utils.enums.WeekDayEnum.getDayItemList;
import com.uptime.upcastcm.utils.helperclass.Filters;
import com.uptime.upcastcm.utils.vo.AreaVO;
import com.uptime.upcastcm.utils.vo.DayVO;
import com.uptime.upcastcm.utils.vo.AreaConfigScheduleVO;
import java.io.Serializable;
import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import com.uptime.upcastcm.utils.interfaces.PresetDialogFields;
import com.uptime.upcastcm.utils.vo.UserSitesVO;
import java.util.UUID;
import javax.faces.model.SelectItem;
import org.primefaces.PrimeFaces;

/**
 *
 * @author gsingh
 */
public class CreateAreaBean implements Serializable, PresetDialogFields {
    private UserManageBean userManageBean;
    private AreaVO areaVO;
    private boolean indoor, outdoor, scheduleBtnClick, scheduleContiFlag;
    private List<String> hrsList, areaNameList;
    private List<AreaVO> areaVOListDb;
    private List<UserSitesVO> siteList;
    private List<AreaConfigScheduleVO> scheduleVOList;
    private UserSitesVO selectedSite;
    private boolean siteSelectDisabled;

    /**
     * Constructor
     */
    public CreateAreaBean() {
        siteSelectDisabled = false;
        selectedSite = null;
        
        try {
            userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
            hrsList = getHourItemList();
        } catch (Exception e) {
            Logger.getLogger(CreateAssetBean.class.getName()).log(Level.SEVERE, "CreateAssetBean Constructor Exception {0}", e.toString());
            userManageBean = null;
        }
    }

    /**
     * Post Constructor
     */
    @PostConstruct
    public void init() {
        resetPage();
    }

    @Override
    public void resetPage() {
        indoor = false;
        outdoor = false;
        siteSelectDisabled = false;
        
        siteList = new ArrayList();
        areaVO = new AreaVO();
        if (userManageBean != null && userManageBean.getCurrentSite() != null) {
            areaVO.setCustomerAccount(userManageBean.getCurrentSite().getCustomerAccount());
            areaVO.setSiteName(userManageBean.getCurrentSite().getSiteName());
            areaVO.setSiteId(userManageBean.getCurrentSite().getSiteId());
            siteList.add(userManageBean.getCurrentSite());
        }
        
        selectedSite = null;
        scheduleVOList = new ArrayList();
        areaVO.setScheduleVOList(scheduleVOList);
        scheduleBtnClick = false;
        scheduleContiFlag = true;
    }

    @Override
    public void presetFields(Filters presets, Object object) {
        UserSitesVO userSitesVO;
        
        if (presets != null && presets.getSite() != null) {
            siteList = new ArrayList();
            
            userSitesVO = new UserSitesVO();
            userSitesVO.setSiteId((UUID) presets.getSite().getValue());
            userSitesVO.setSiteName(presets.getSite().getLabel());
            
            siteList.add(userSitesVO);
            
            areaVO.setSiteName(userSitesVO.getSiteName());
            areaVO.setSiteId(userSitesVO.getSiteId());
            
            siteSelectDisabled = true;
        }
    }

    @Override
    public void presetFields(int operationType, Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void presetFields(Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public void onSiteSelect() {
        if (selectedSite != null && selectedSite.getSiteId() != null && selectedSite.getSiteName() != null && !selectedSite.getSiteName().isEmpty()) {
            areaVOListDb = AreaClient.getInstance().getAreaSiteByCustomerSite(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), areaVO.getCustomerAccount(), selectedSite.getSiteId());
            areaVO.setSiteName(selectedSite.getSiteName());
            areaVO.setSiteId(selectedSite.getSiteId());
            areaNameList = new ArrayList();
            if (areaVOListDb != null & !areaVOListDb.isEmpty()) {
                areaNameList = areaVOListDb.stream().map(AreaVO::getAreaName).collect(Collectors.toList());
            }
        }
    }

    public void onHrsChange(AreaConfigScheduleVO asvo) {
        Logger.getLogger(CreateAreaBean.class.getName()).log(Level.INFO, "com.uptime.upcastcm.bean.AreaBean.onHrsChange() called ******");
        scheduleContiFlag = asvo.getDayVOList().stream().allMatch(dvo -> dvo.getSelectedHrsList() == null || dvo.getSelectedHrsList().isEmpty());
    }

    public void isContinousChange(AreaConfigScheduleVO svo) {
        scheduleContiFlag = !svo.isContinuous();
        svo.setDayVOList(prepareDayVOList());
    }

    public void createArea() {
        try {
            if (!processAreaVo(areaVO)) {
                return;
            }
            areaVO.setEnvironment(indoor + "," + outdoor);
            Logger.getLogger(CreateAreaBean.class.getName()).log(Level.INFO, "CreateAreaBean.createArea(). areaVO: {0}", areaVO);
            if (AreaClient.getInstance().createArea(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), areaVO)) {
                Logger.getLogger(CreateAreaBean.class.getName()).log(Level.INFO, "Area created successfully. {0}", new Object[]{areaVO});
                areaVO = new AreaVO();
                if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("configurationBean") == null) {
                    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("configurationBean", new ConfigurationBean());
                    ((ConfigurationBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("configurationBean")).init();
                }
                ((ConfigurationBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("configurationBean")).onSiteChange(true);
                PrimeFaces.current().executeScript("PF('defaultDlg').hide()");
                PrimeFaces.current().executeScript("updateTreeNodes()");
            } else {
                Logger.getLogger(CreateAreaBean.class.getName()).log(Level.INFO, "Error in creating area. areaVO.getAreaName()*********** {0}", new Object[]{areaVO.getAreaName()});
            }
            scheduleBtnClick = true;
        } catch (Exception ex) {
        }
    }

    public void validateAreaName() {
        Logger.getLogger(CreateAreaBean.class.getName()).log(Level.INFO, "CreateAreaBean.validateAreaName() called. ***********");
    }

    public void updateBean() {
        Logger.getLogger(CreateAreaBean.class.getName()).log(Level.INFO, "CreateAreaBean.updateBean() called. ***********");
        if(areaVO != null && areaVO.getAreaName() != null && !areaVO.getAreaName().isEmpty()){
            scheduleBtnClick = false;
        }
    }

    public void updateClimateControlled() {
        if (!indoor) {
            areaVO.setClimateControlled(false);
        }
    }
    
    public void updateScheduleName() {
        areaVO.getScheduleVOList().forEach(sVO -> {
            scheduleBtnClick = sVO.getScheduleName() == null || sVO.getScheduleName().isEmpty();
        });
        Logger.getLogger(CreateAreaBean.class.getName()).log(Level.INFO, "CreateAreaBean.updateScheduleName() called. ***********{0}", scheduleBtnClick);
    }

    public void addSchedule() {
        AreaConfigScheduleVO svo;
        
        svo = new AreaConfigScheduleVO();
        svo.setDayVOList(prepareDayVOList());
        scheduleVOList.add(svo);
        scheduleBtnClick = true;
    }

    public void deleteSchedule(AreaConfigScheduleVO svo) {
        scheduleVOList.remove(svo);
        scheduleBtnClick = false;
    }

    private List<DayVO> prepareDayVOList() {
        List<DayVO> dayVOList;
        DayVO day;
        
        dayVOList = new ArrayList();
        for (SelectItem item : getDayItemList()) {
            day = new DayVO();
            day.setDay(((DayOfWeek)item.getValue()).toString());
            day.setLabel(item.getLabel());
            dayVOList.add(day);
        }

        return dayVOList;
    }

    private boolean processAreaVo(AreaVO areaVO) {
        List<AreaConfigScheduleVO> scheduleVOListTmp = new ArrayList();
        AreaConfigScheduleVO svoTmp;
        
        areaVO.setAreaId(UUID.randomUUID());
        if (areaVO.getScheduleVOList() != null && !areaVO.getScheduleVOList().isEmpty()) {
            for (AreaConfigScheduleVO svo : areaVO.getScheduleVOList()) {
                scheduleContiFlag = true;
                
                if (svo.isContinuous()) {
                    scheduleContiFlag = false;
                    svoTmp = new AreaConfigScheduleVO();
                    svoTmp.setCustomerAccount(areaVO.getCustomerAccount());
                    svoTmp.setSiteId(areaVO.getSiteId());
                    svoTmp.setAreaId(areaVO.getAreaId());
                    svoTmp.setScheduleId(UUID.randomUUID());
                    svoTmp.setSiteName(areaVO.getSiteName());
                    svoTmp.setAreaName(areaVO.getAreaName());
                    svoTmp.setScheduleName(svo.getScheduleName());
                    svoTmp.setSchDescription(svo.getSchDescription());
                    svoTmp.setDayOfWeek((byte) 8);
                    svoTmp.setHoursOfDay(null);
                    svoTmp.setContinuous(svo.isContinuous());
                    svoTmp.setDayVOList(null);
                    scheduleVOListTmp.add(svoTmp);
                } else {
                    for (DayVO dvo : svo.getDayVOList()) {
                        if (dvo.getSelectedHrsList() != null && !dvo.getSelectedHrsList().isEmpty()) {
                            scheduleContiFlag = false;
                            
                            svoTmp = new AreaConfigScheduleVO();
                            svoTmp.setCustomerAccount(areaVO.getCustomerAccount());
                            svoTmp.setSiteId(areaVO.getSiteId());
                            svoTmp.setAreaId(areaVO.getAreaId());
                            svoTmp.setScheduleId(UUID.randomUUID());
                            svoTmp.setSiteName(areaVO.getSiteName());
                            svoTmp.setAreaName(areaVO.getAreaName());
                            svoTmp.setScheduleName(svo.getScheduleName());
                            svoTmp.setSchDescription(svo.getSchDescription());
                            svoTmp.setDayOfWeek((byte) DayOfWeek.valueOf(dvo.getDay()).getValue());
                            svoTmp.setHoursOfDay(String.join(",", dvo.getSelectedHrsList()));
                            svoTmp.setContinuous(svo.isContinuous());
                            svoTmp.setDayVOList(null);
                            scheduleVOListTmp.add(svoTmp);
                        }
                    }
                    if (scheduleContiFlag) {
                        if (userManageBean != null) {
                            FacesContext.getCurrentInstance().addMessage("nonAutoGrowlMessages", new FacesMessage(svo.getScheduleName() + ": " + userManageBean.getResourceBundleString("growl_create_area_1")));
                        }
                        return false;
                    }
                }
            }
            areaVO.setScheduleVOList(scheduleVOListTmp);

        }
        return true;
    }

    public AreaVO getAreaVO() {
        return areaVO;
    }

    public void setAreaVO(AreaVO areaVO) {
        this.areaVO = areaVO;
    }

    public List<String> getHrsList() {
        return hrsList;
    }

    public boolean isIndoor() {
        return indoor;
    }

    public void setIndoor(boolean indoor) {
        this.indoor = indoor;
    }

    public boolean isOutdoor() {
        return outdoor;
    }

    public void setOutdoor(boolean outdoor) {
        this.outdoor = outdoor;
    }

    public List<String> getAreaNameList() {
        return areaNameList;
    }

    public void setAreaNameList(List<String> areaNameList) {
        this.areaNameList = areaNameList;
    }

    public List<UserSitesVO> getSiteList() {
        return siteList;
    }

    public boolean isSiteSelectDisabled() {
        return siteSelectDisabled;
    }

    public UserSitesVO getSelectedSite() {
        return selectedSite;
    }

    public void setSelectedSite(UserSitesVO selectedSite) {
        this.selectedSite = selectedSite;
    }

    public List<AreaConfigScheduleVO> getScheduleVOList() {
        return scheduleVOList;
    }

    public void setScheduleVOList(List<AreaConfigScheduleVO> scheduleVOList) {
        this.scheduleVOList = scheduleVOList;
    }

    public boolean isScheduleBtnClick() {
        return scheduleBtnClick;
    }

    public boolean isScheduleContiFlag() {
        return scheduleContiFlag;
    }

    
}
