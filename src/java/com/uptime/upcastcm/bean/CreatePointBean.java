/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.APALSetClient;
import com.uptime.upcastcm.http.client.DevicePresetClient;
import com.uptime.upcastcm.http.client.PointClient;
import com.uptime.upcastcm.http.client.PointLocationClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import static com.uptime.upcastcm.utils.enums.ACSensorTypeEnum.getACSensorTypeItemList;
import static com.uptime.upcastcm.utils.enums.DCSensorTypeEnum.getDCSensorTypeItemList;
import static com.uptime.upcastcm.utils.enums.DwellEnum.getDwellItemList;
import static com.uptime.upcastcm.utils.enums.FmaxEnum.getFmaxItemList;
import static com.uptime.upcastcm.utils.enums.ResolutionEnum.getResolutionItemList;
import com.uptime.upcastcm.utils.helperclass.Filters;
import com.uptime.upcastcm.utils.helperclass.Utils;
import static com.uptime.upcastcm.utils.helperclass.Utils.distinctByKeys;
import com.uptime.upcastcm.utils.vo.PointVO;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import com.uptime.upcastcm.utils.interfaces.PresetDialogFields;
import com.uptime.upcastcm.utils.singletons.CommonUtilSingleton;
import com.uptime.upcastcm.utils.vo.ApAlSetVO;
import com.uptime.upcastcm.utils.vo.DevicePresetVO;
import com.uptime.upcastcm.utils.vo.PointLocationVO;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import javax.faces.model.SelectItem;
import javax.faces.model.SelectItemGroup;
import org.primefaces.PrimeFaces;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author madhavi
 */
public class CreatePointBean implements Serializable, PresetDialogFields {

    private UserManageBean userManageBean;
    private NavigationBean navigationBean;

    private final List<ApAlSetVO> siteApAlSetsVOList, selectedSiteApAlSetsVOList, pointApAlsets, displayApAlsets;
    private final Map<UUID, List<ApAlSetVO>> customLimitVOMap;
    private final List<SelectItem> apSetList, alSetList;

    private List<SelectItem> fmaxList, dwellList, resolutionList, dcSensorTypeList, acSensorTypeList;

    private boolean renderAlertMsg, addApalSetToPoint, tachValidationFailed, templateTachValidationFailed, fromTemplate;
    private PointVO pointVO;
    private ApAlSetVO selectedApAlSetVO;
    private PointLocationVO selectedPointLocation,pointLocationVO;
    private UUID previusId;
    private UUID selectedPresetId;
    private final List<DevicePresetVO> devicePresetsList, selectedSiteDevicePresetList;
    private final List<SelectItem> devicePresetsSelectItemList;
    private DevicePresetVO modifiedDevicePresetVO;

    /**
     * Constructor
     */
    public CreatePointBean() {
        apSetList = new ArrayList();
        alSetList = new ArrayList();
        selectedSiteApAlSetsVOList = new ArrayList();
        pointApAlsets = new ArrayList();
        displayApAlsets = new ArrayList();
        siteApAlSetsVOList = new ArrayList();
        customLimitVOMap = new HashMap();
        devicePresetsList = new ArrayList();
        selectedSiteDevicePresetList = new ArrayList();
        devicePresetsSelectItemList = new ArrayList();

        try {
            userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
            navigationBean = (NavigationBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("navigationBean");

            dcSensorTypeList = getDCSensorTypeItemList();
            acSensorTypeList = getACSensorTypeItemList();
            fmaxList = getFmaxItemList();
            dwellList = getDwellItemList();
            resolutionList = getResolutionItemList();
        } catch (Exception e) {
            Logger.getLogger(CreatePointBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            userManageBean = null;
            navigationBean = null;
        }
    }

    /**
     * Post Constructor
     */
    @PostConstruct
    public void init() {
        resetPage();
        if (userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null) {
            Logger.getLogger(CreatePointBean.class.getName()).log(Level.INFO, "**customerAccount to be set : {0}", userManageBean.getCurrentSite().getCustomerAccount());
        }
    }

    @Override
    public void resetPage() {
        List<DevicePresetVO> devicePresetVOList, totalDevicePresetVOList;

        apSetList.clear();
        alSetList.clear();
        selectedSiteApAlSetsVOList.clear();
        pointApAlsets.clear();
        displayApAlsets.clear();
        siteApAlSetsVOList.clear();
        customLimitVOMap.clear();
        selectedSiteDevicePresetList.clear();
        devicePresetsList.clear();
        devicePresetsSelectItemList.clear();
        templateTachValidationFailed = false;
        tachValidationFailed = false;
        addApalSetToPoint = false;
        renderAlertMsg = false;
        fromTemplate = true;
        selectedPresetId = null;
        selectedPointLocation = null;
        pointLocationVO = null;
        selectedApAlSetVO = null;
        previusId = null;
        modifiedDevicePresetVO = null;
        pointVO = new PointVO();
        pointVO.setPointType("AC");

        if (userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null && userManageBean.getCurrentSite().getSiteId() != null) {
            pointVO.setCustomerAccount(userManageBean.getCurrentSite().getCustomerAccount());

            totalDevicePresetVOList = null;

            // Get Uptime Device Presets
            if (!userManageBean.getCurrentSite().getCustomerAccount().equals(DEFAULT_UPTIME_ACCOUNT)) {
                devicePresetVOList = DevicePresetClient.getInstance().getGlobalDevicePresetsByCustomer(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), DEFAULT_UPTIME_ACCOUNT);
                if (!devicePresetVOList.isEmpty()) {
                    totalDevicePresetVOList = new ArrayList();
                    totalDevicePresetVOList.addAll(devicePresetVOList);
                }
            }

            // Get Global Device Presets
            devicePresetVOList = DevicePresetClient.getInstance().getGlobalDevicePresetsByCustomer(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount());
            if (!devicePresetVOList.isEmpty()) {
                if (totalDevicePresetVOList == null) {
                    totalDevicePresetVOList = new ArrayList();
                }
                totalDevicePresetVOList.addAll(devicePresetVOList);
            }

            if (totalDevicePresetVOList != null && !totalDevicePresetVOList.isEmpty()) {
                devicePresetsList.addAll(totalDevicePresetVOList);
            }

            // Get Site Device Presets
            if ((devicePresetVOList = DevicePresetClient.getInstance().getDevicePresetVOs(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), userManageBean.getCurrentSite().getSiteId())) != null && !devicePresetVOList.isEmpty()) {
                devicePresetsList.addAll(devicePresetVOList);
            }

            if (!devicePresetsList.isEmpty()) {
                devicePresetsList.stream().collect(Collectors.groupingBy(DevicePresetVO::getDeviceType)).forEach((key, value) -> groupAndAddToDevicePresetsSelectItemList(value, key));
            }
        }
    }

    @Override
    public void presetFields(Filters presets, Object object) {
        resetPage();

        try {
            if (presets != null) {
                this.pointLocationVO = (PointLocationVO) object;
                this.selectedPointLocation =PointLocationClient.getInstance().getPointLocationsByPK(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), this.pointLocationVO.getSiteId(), this.pointLocationVO.getAreaId(), this.pointLocationVO.getAssetId(), this.pointLocationVO.getPointLocationId()).get(0);
                pointVO.setAlarmEnabled(this.selectedPointLocation.isAlarmEnabled());
                if (presets.getSite() != null) {
                    pointVO.setSiteName(presets.getSite().getLabel());
                    pointVO.setSiteId((UUID) presets.getSite().getValue());
                }

                if (presets.getArea() != null) {
                    pointVO.setAreaName(presets.getArea().getLabel());
                    pointVO.setAreaId((UUID) presets.getArea().getValue());
                }

                if (presets.getAsset() != null) {
                    pointVO.setAssetName(presets.getAsset().getLabel());
                    pointVO.setAssetId((UUID) presets.getAsset().getValue());
                }

                if (presets.getPointLocation() != null) {
                    pointVO.setPointLocationName(presets.getPointLocation().getLabel());
                    pointVO.setPointLocationId((UUID) presets.getPointLocation().getValue());
                    pointVO.setAlarmEnabled(presets.isPointLocAlarmEnabled());
                    Logger.getLogger(CreatePointBean.class.getName()).log(Level.INFO, "presets**************: {0}", presets.isPointLocAlarmEnabled());
                }
            }
        } catch (Exception e) {
            Logger.getLogger(CreatePointBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            pointVO = new PointVO();
        }
    }

    @Override
    public void presetFields(int operationType, Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void presetFields(Object object) {
        DevicePresetVO originalDevicePresetVO;
        int itemIndex;
        
        try {
            modifiedDevicePresetVO = null;
            if ((object instanceof DevicePresetVO)) {
                modifiedDevicePresetVO = (DevicePresetVO) object;
            }

            // Update selectedSiteDevicePresetList feild
            if (modifiedDevicePresetVO != null) {
                //find the Original Object that has been modified.
                originalDevicePresetVO = selectedSiteDevicePresetList.stream().filter(dp -> dp.getPresetId().compareTo(modifiedDevicePresetVO.getPresetId()) == 0 && dp.getChannelNumber() == modifiedDevicePresetVO.getChannelNumber() && dp.getDeviceType().equals(modifiedDevicePresetVO.getDeviceType()) && dp.getChannelType().equals(modifiedDevicePresetVO.getChannelType())).findFirst().orElse(null);

                //find the index of the Object that has been modified.
                if ((itemIndex = selectedSiteDevicePresetList.indexOf(originalDevicePresetVO)) != -1) {
                    //replace the Object
                    selectedSiteDevicePresetList.set(itemIndex, modifiedDevicePresetVO);
                }
            }


            // set templateTachValidationFailed field
            // Check if AP Sets require a tach and if a tach is provided
            templateTachValidationFailed = false;
            outerloop:
            for (DevicePresetVO vo: selectedSiteDevicePresetList) {
                for (ApAlSetVO al: vo.getAlSetVOList()) {
                    if ((al.getFrequencyUnits() != null && al.getFrequencyUnits().equalsIgnoreCase("Orders")) || Utils.validateParamList().contains(al.getParamType())) {
                        templateTachValidationFailed = selectedPointLocation.getTachId() == null;

                        if (templateTachValidationFailed) {
                            break outerloop;
                        }
                    }
                }
            }
            
        } catch (Exception e) {
            Logger.getLogger(CreatePointBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }

        //update UI
        PrimeFaces.current().ajax().update("createPointFormId");
    }

    public void updateBean() {
        Logger.getLogger(CreatePointBean.class.getName()).log(Level.INFO, "***********updateBean***********");
    }

    public void updateFromTemplate() {
        apSetList.clear();
        alSetList.clear();
        selectedSiteApAlSetsVOList.clear();
        pointApAlsets.clear();
        displayApAlsets.clear();
        siteApAlSetsVOList.clear();
        customLimitVOMap.clear();
        selectedSiteDevicePresetList.clear();
        tachValidationFailed = false;
        templateTachValidationFailed=false;
        addApalSetToPoint = false;
        renderAlertMsg = false;
        selectedPresetId = null;
        selectedApAlSetVO = null;
        previusId = null;
        modifiedDevicePresetVO = null;
        pointVO.setPointName(null);
        pointVO.setSensorType(null);
        pointVO.setCustomized(false);
        Logger.getLogger(CreatePointBean.class.getName()).log(Level.INFO, "***********updateFromTemplate***********");
    }

    /**
     * This is a helper method for the method validateSerialNum. Group and Sort
     * the given list, add the group to the field devicePresetsSelectItemList
     *
     * @param devicePresetVOs
     * @param dpType
     * @return
     */
    private void groupAndAddToDevicePresetsSelectItemList(List<DevicePresetVO> devicePresetVOs, String dpType) {
        SelectItem[] dpSetArr;
        SelectItemGroup apSetGroup;
        List<SelectItem> dpSetList;

        if (devicePresetVOs != null && !devicePresetVOs.isEmpty()) {

            // Verfy the items in the list are distinced and sort be the Device Name
            devicePresetVOs = devicePresetVOs.stream().filter(distinctByKeys(DevicePresetVO::getName, DevicePresetVO::getDeviceType, DevicePresetVO::getPresetId)).collect(Collectors.toList());
            devicePresetVOs.sort(Comparator.comparing(DevicePresetVO::getName));

            // Create Group List of SelectItem Objects
            dpSetArr = new SelectItem[devicePresetVOs.size()];
            for (int i = 0; i < devicePresetVOs.size(); i++) {
                dpSetArr[i] = new SelectItem(devicePresetVOs.get(i).getPresetId(), devicePresetVOs.get(i).getName());
            }
            apSetGroup = new SelectItemGroup(dpType); // Set Group Name
            apSetGroup.setSelectItems(dpSetArr); // Add Items to group
            dpSetList = new ArrayList();
            dpSetList.add(apSetGroup); // Add Group to List

            devicePresetsSelectItemList.addAll(dpSetList);
        }
    }

    /**
     * Edit the given Point
     *
     * @param devicePresetVO, DevicePresetVO Object
     */
    public void edit(DevicePresetVO devicePresetVO) {
        if (devicePresetVO != null) {
            if ((EditInstallDevicePresetsBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("editInstallDevicePresetsBean") == null) {
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("editInstallDevicePresetsBean", new EditInstallDevicePresetsBean());
                ((EditInstallDevicePresetsBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("editInstallDevicePresetsBean")).init();
            }
            ((EditInstallDevicePresetsBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("editInstallDevicePresetsBean")).presetFields(devicePresetVO, selectedPointLocation, false);
            navigationBean.updateSecondDialog("edit-point");
            PrimeFaces.current().executeScript("PF('defaultSecondDlg').show()");
        }
    }

    /**
     * Create the new point
     */
    public void createPoint() {
        Map<UUID, PointVO> tempPointVOMap;
        String sensorUnit;
        List<PointVO> pointVOs;

        try {
            if (fromTemplate) {
                pointVOs = new ArrayList();
                selectedSiteDevicePresetList.stream().forEachOrdered(dpVO -> {
                    PointVO pvo = new PointVO();
                    
                    pvo.setCustomerAccount(pointVO.getCustomerAccount());
                    pvo.setSiteId(pointVO.getSiteId());
                    pvo.setAlSetId(dpVO.getAlSetId());
                    pvo.setAlSetName(dpVO.getAlSetName());
                    pvo.setAlarmEnabled(dpVO.isAlarmEnabled());
                    pvo.setApSetId(dpVO.getApSetId());
                    pvo.setApSetName(dpVO.getApSetName());
                    pvo.setAreaId(pointVO.getAreaId());
                    pvo.setAssetId(pointVO.getAssetId());
                    pvo.setPointLocationId(pointVO.getPointLocationId());
                    pvo.setDisabled(dpVO.isDisabled());
                    pvo.setPointName(dpVO.getPointName());
                    pvo.setPointType(dpVO.getChannelType());
                    pvo.setSensorChannelNum(dpVO.getChannelNumber());
                    pvo.setSensorOffset(dpVO.getSensorOffset());
                    pvo.setSensorSensitivity(dpVO.getSensorSensitivity());
                    pvo.setSensorType(dpVO.getSensorType());
                    pvo.setSensorUnits(dpVO.getSensorUnits());
                    pvo.setApAlSetVOs(dpVO.getAlSetVOList());
                    pvo.setSensorSubType(dpVO.getSensorSubType());
                    pvo.setSensorLocalOrientation(dpVO.getSensorLocalOrientation());
                    
                    pointVOs.add(pvo);
                });
            } else {
                tempPointVOMap = new HashMap();
                if (!pointApAlsets.isEmpty()) {
                    pointApAlsets.forEach(action -> action.setCustomized(false));

                    //customized list to be set to the pointApAlsets from customLimitVOMap
                    pointApAlsets.stream().filter(ap -> (customLimitVOMap.containsKey(ap.getApSetId()))).forEachOrdered(ap -> {
                        customLimitVOMap.get(ap.getApSetId()).stream().filter(apAl -> (apAl.getAlSetId().compareTo(ap.getAlSetId()) == 0 && apAl.getParamName().equals(ap.getParamName()))).forEachOrdered(apAl -> {
                            int index;

                            if ((index = pointApAlsets.indexOf(ap)) >= 0) {
                                pointApAlsets.set(index, apAl);
                            }
                        });
                    });

                    //setting SensorUnits getSensorUnitBySensorType
                    previusId = null;
                    if (pointApAlsets != null) {
                        sensorUnit = CommonUtilSingleton.getInstance().getSensorUnitBySensorType(pointVO.getSensorType());

                        pointApAlsets.forEach(action -> {
                            List<ApAlSetVO> tempApALIdVOs;
                            ApAlSetVO tempAlSetVO;
                            PointVO tempPointVO;

                            tempAlSetVO = new ApAlSetVO(action);
                            if (!tempAlSetVO.getApSetId().equals(previusId)) {
                                tempApALIdVOs = new ArrayList();
                                tempApALIdVOs.add(tempAlSetVO);

                                tempPointVO = new PointVO(pointVO);
                                tempPointVO.setDisabled(selectedApAlSetVO.getApSetId().compareTo(action.getApSetId()) != 0);
                                tempPointVO.setCustomized(tempAlSetVO.isCustomized());
                                tempPointVO.setApSetId(tempAlSetVO.getApSetId());
                                tempPointVO.setApSetName(tempAlSetVO.getApSetName());
                                tempPointVO.setAlSetId(tempAlSetVO.getAlSetId());
                                tempPointVO.setAlSetName(tempAlSetVO.getAlSetName());
                                tempPointVO.setApAlSetVOs(tempApALIdVOs);
                                tempPointVO.setSensorUnits(sensorUnit);
                                tempPointVO.setSampleRate(tempAlSetVO.getSampleRate());

                                tempPointVOMap.put(tempAlSetVO.getApSetId(), tempPointVO);
                            } else {
                                tempPointVO = tempPointVOMap.get(tempAlSetVO.getApSetId());
                                tempPointVO.getApAlSetVOs().add(tempAlSetVO);
                            }
                            previusId = tempAlSetVO.getApSetId();
                        });

                    }

                }
                pointVOs = new ArrayList(tempPointVOMap.values());
            }

            if (PointClient.getInstance().createPoint(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), pointVOs)) {
                //Logger.getLogger(CreatePointBean.class.getName()).log(Level.INFO, "Created Points {0}", pointVOs);

                pointVO = new PointVO();
                if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("configurationBean") == null) {
                    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("configurationBean", new ConfigurationBean());
                    ((ConfigurationBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("configurationBean")).init();
                }
                ((ConfigurationBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("configurationBean")).onPointLocationChange(true);
                PrimeFaces.current().executeScript("PF('defaultDlg').hide()");
                PrimeFaces.current().executeScript("updateTreeNodes()");
            } else {
                Logger.getLogger(CreatePointBean.class.getName()).log(Level.INFO, "pointVOs*********** {0}", pointVOs);
            }
        } catch (Exception e) {
            Logger.getLogger(CreatePointBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Add a new apAlSet to the point
     */
    public void addApAlset() {
        List<ApAlSetVO> tempSelectedSiteApAlSetsVOList;
        List<SelectItem> selectItem;

        try {
            if (siteApAlSetsVOList != null && !siteApAlSetsVOList.isEmpty() && pointVO != null && pointVO.getApSetId() != null) {
                if (!(tempSelectedSiteApAlSetsVOList = siteApAlSetsVOList.stream().filter(a -> a.getApSetId().compareTo(pointVO.getApSetId()) == 0 && a.getAlSetId().compareTo(pointVO.getAlSetId()) == 0).collect(Collectors.toList())).isEmpty()) {
                    customLimitVOMap.put(pointVO.getApSetId(), new ArrayList());
                    tempSelectedSiteApAlSetsVOList.forEach(apAlSetVo -> {
                        CommonUtilSingleton.getInstance().onFmaxChange(apAlSetVo);
                        pointApAlsets.add(new ApAlSetVO(apAlSetVo));
                        customLimitVOMap.get(pointVO.getApSetId()).add(new ApAlSetVO(apAlSetVo));
                    });
                    displayApAlsets.clear();
                    displayApAlsets.addAll(pointApAlsets.stream().filter(CommonUtilSingleton.distinctByKey(p -> p.getApSetId())).collect(Collectors.toList()));
                    pointVO.setApSetId(null);
                    pointVO.setAlSetId(null);

                    // Clear and reset apSetList field
                    apSetList.clear();
                    userManageBean.getPresetUtil().clearGlobalApAlSetSensorList();
                    userManageBean.getPresetUtil().clearSiteApAlSetSensorList();
                    if ((selectItem = userManageBean.getPresetUtil().populateApSetSensorTypeList(userManageBean, userManageBean.getCurrentSite().getCustomerAccount(), pointVO.getSiteId(), pointVO.getSensorType(), displayApAlsets)) != null && !selectItem.isEmpty()) {
                        apSetList.addAll(selectItem);
                    }
                }

                if (displayApAlsets != null && displayApAlsets.size() == 1) {
                    selectedApAlSetVO = pointApAlsets.get(0);
                }
            }
            addApalSetToPoint = false;
        } catch (Exception e) {
            Logger.getLogger(CreatePointBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Remove the given apAlSet from the point
     *
     * @param apAlSetVO, ApAlSetVO Object
     */
    public void removeApAlset(ApAlSetVO apAlSetVO) {
        List<SelectItem> selectItem;

        try {
            userManageBean.getPresetUtil().clearGlobalApAlSetSensorList();
            userManageBean.getPresetUtil().clearSiteApAlSetSensorList();
            apSetList.clear();
            pointVO.setApSetId(null);
            pointVO.setAlSetId(null);
            pointApAlsets.removeIf(alset -> alset.getApSetId().compareTo(apAlSetVO.getApSetId()) == 0);
            displayApAlsets.removeIf(alset -> alset.getApSetId().compareTo(apAlSetVO.getApSetId()) == 0);

            // Remove limits from the customLimitVOMap field
            if (customLimitVOMap.containsKey(apAlSetVO.getApSetId())) {
                customLimitVOMap.remove(apAlSetVO.getApSetId());
            }

            if (!displayApAlsets.isEmpty()) {

                //check if selected Object which is selectedApAlSetVO to be removed.
                if (apAlSetVO.getApSetId().compareTo(selectedApAlSetVO.getApSetId()) == 0) {
                    renderAlertMsg = false;
                    selectedApAlSetVO = displayApAlsets.get(0);
                    pointVO.setCustomized(selectedApAlSetVO.isCustomized());
                    onCustomLimitsAlSetNameSelect();
                }

                // set apSetList list
                if ((selectItem = userManageBean.getPresetUtil().populateApSetSensorTypeList(userManageBean, userManageBean.getCurrentSite().getCustomerAccount(), pointVO.getSiteId(), pointVO.getSensorType(), displayApAlsets)) != null && !selectItem.isEmpty()) {
                    apSetList.addAll(selectItem);
                }

            } else {
                renderAlertMsg = false;
                selectedApAlSetVO = null;
                pointVO.setCustomized(false);
                selectedSiteApAlSetsVOList.clear();

                // set apSetList list
                if ((selectItem = userManageBean.getPresetUtil().populateApSetSensorTypeList(userManageBean, userManageBean.getCurrentSite().getCustomerAccount(), pointVO.getSiteId(), pointVO.getSensorType())) != null && !selectItem.isEmpty()) {
                    apSetList.addAll(selectItem);
                }
            }
        } catch (Exception e) {
            Logger.getLogger(CreatePointBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Action called when a new apAlSet is selected to be active
     *
     * @param event, SelectEvent Object
     */
    public void activateApAlSetVO(SelectEvent<ApAlSetVO> event) {
        List<ApAlSetVO> selectedApAlSetVOs;

        try {
            selectedApAlSetVO = event.getObject();

            //find the selected apAlSetVOs list
            if ((selectedApAlSetVOs = pointApAlsets.stream().filter(vo -> vo.getApSetId().compareTo(selectedApAlSetVO.getApSetId()) == 0).collect(Collectors.toList())) != null && !selectedApAlSetVOs.isEmpty()) {
                pointVO.setCustomized(selectedApAlSetVOs.get(0).isCustomized());
            }

            onCustomLimitsAlSetNameSelect();
        } catch (Exception e) {
            Logger.getLogger(CreatePointBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Action taken when the point type is switched
     */
    public void onSwitchChange() {
        pointVO.setSensorType(null);
        pointVO.setApSetId(null);
        pointVO.setAlSetId(null);
        pointVO.setCustomized(false);
        apSetList.clear();
        siteApAlSetsVOList.clear();
        alSetList.clear();
        selectedSiteApAlSetsVOList.clear();
        pointApAlsets.clear();
        displayApAlsets.clear();
        customLimitVOMap.clear();
    }

    /**
     * Action taken when the sensor type is selected
     */
    public void onSensorSelect() {
        List<SelectItem> selectItem;

        try {
            if (userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null && pointVO.getSensorType() != null && !pointVO.getSensorType().isEmpty()) {
                pointVO.setApSetId(null);
                pointVO.setAlSetId(null);
                pointVO.setCustomized(false);
                apSetList.clear();
                siteApAlSetsVOList.clear();
                alSetList.clear();
                pointApAlsets.clear();
                addApalSetToPoint = false;
                selectedApAlSetVO = null;
                displayApAlsets.clear();
                customLimitVOMap.clear();
                userManageBean.getPresetUtil().clearGlobalApAlSetSensorList();
                userManageBean.getPresetUtil().clearSiteApAlSetSensorList();

                if ((selectItem = userManageBean.getPresetUtil().populateApSetSensorTypeList(userManageBean, userManageBean.getCurrentSite().getCustomerAccount(), pointVO.getSiteId(), pointVO.getSensorType())) != null && !selectItem.isEmpty()) {
                    apSetList.addAll(selectItem);
                }

            }
        } catch (Exception e) {
            Logger.getLogger(CreatePointBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Action taken when the ApSet name is selected
     */
    public void onApSetNameSelect() {
        List<SelectItem> selectItem;

        try {
            alSetList.clear();
            siteApAlSetsVOList.clear();
            addApalSetToPoint = false;
            tachValidationFailed = false;
            pointVO.setAlSetId(null);

            if (userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null) {
                siteApAlSetsVOList.addAll(APALSetClient.getInstance().getSiteApAlSetsByCustomerSiteIdApSetId(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), pointVO.getSiteId(), pointVO.getApSetId()));
                if (siteApAlSetsVOList == null || siteApAlSetsVOList.isEmpty()) {
                    siteApAlSetsVOList.addAll(APALSetClient.getInstance().getGlobalApAlSetsByCustomerApSetId(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), pointVO.getApSetId()));
                    if (siteApAlSetsVOList == null || siteApAlSetsVOList.isEmpty()) {
                        siteApAlSetsVOList.addAll(APALSetClient.getInstance().getGlobalApAlSetsByCustomerApSetId(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), DEFAULT_UPTIME_ACCOUNT, pointVO.getApSetId()));
                    }
                }

                if (siteApAlSetsVOList == null || siteApAlSetsVOList.isEmpty()) {
                    FacesContext.getCurrentInstance().addMessage("growlMessages", new FacesMessage(userManageBean.getResourceBundleString("growl_create_point_3")));
                } else {

                    // Validate tach
                    siteApAlSetsVOList.stream().forEach(al -> {
                        if ((al.getFrequencyUnits() != null && al.getFrequencyUnits().equalsIgnoreCase("Orders")) || Utils.validateParamList().contains(al.getParamType())) {
                            if (selectedPointLocation == null) {
                                selectedPointLocation = PointLocationClient.getInstance().getPointLocationsByPK(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), pointVO.getSiteId(), pointVO.getAreaId(), pointVO.getAssetId(), pointVO.getPointLocationId()).get(0);
                            }
                            if (selectedPointLocation.getTachId() == null) {
                                tachValidationFailed = true;
                            }
                        }
                    });

                    //Set Al set list
                    if (!tachValidationFailed && (selectItem = userManageBean.getPresetUtil().populateAlSetByApAlsetList(siteApAlSetsVOList)) != null && !selectItem.isEmpty()) {
                        alSetList.addAll(selectItem);
                    }
                }
            }
        } catch (Exception e) {
            Logger.getLogger(CreatePointBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            alSetList.clear();
            siteApAlSetsVOList.clear();
            addApalSetToPoint = false;
            tachValidationFailed = false;
            if (pointVO != null) {
                pointVO.setAlSetId(null);
            }
        }
    }

    /**
     * Action taken when the AlSet name is selected
     */
    public void onAlSetNameSelect() {
        try {
            addApalSetToPoint = false;
            if (siteApAlSetsVOList != null && !siteApAlSetsVOList.isEmpty() && pointVO != null && pointVO.getApSetId() != null) {
                if (pointApAlsets.isEmpty() || pointApAlsets.stream().filter(p -> pointVO.getApSetId().compareTo(p.getApSetId()) == 0).findAny().orElse(null) == null) {
                    addApalSetToPoint = true;
                }
            }
        } catch (Exception e) {
            Logger.getLogger(CreatePointBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Action taken when the custom limit is toggled
     */
    public void onCustomLimitsAlSetNameSelect() {
        List<ApAlSetVO> tempSelectedSiteApAlSetsVOList;
        List<ApAlSetVO> customApAlSets;
        List<ApAlSetVO> activeApAlSets;
        renderAlertMsg = false;
        try {
            if (selectedApAlSetVO != null && (customApAlSets = customLimitVOMap.get(selectedApAlSetVO.getApSetId())) != null && !customApAlSets.isEmpty() && pointVO != null) {
                selectedSiteApAlSetsVOList.clear();
                if ((activeApAlSets = pointApAlsets.stream().filter(p -> p.getApSetId().compareTo(selectedApAlSetVO.getApSetId()) == 0).collect(Collectors.toList())) != null && !activeApAlSets.isEmpty()) {

                    //set the customized value for the selected/activeApAlSets 
                    activeApAlSets.forEach(ap -> ap.setCustomized(pointVO.isCustomized()));

                    if (pointVO.isCustomized()) {
                        if (!(tempSelectedSiteApAlSetsVOList = customApAlSets.stream().filter(a -> a.getAlSetId().compareTo(activeApAlSets.get(0).getAlSetId()) == 0).collect(Collectors.toList())).isEmpty()) {
                            tempSelectedSiteApAlSetsVOList.forEach(apAlSetVo -> {
                                CommonUtilSingleton.getInstance().onFmaxChange(apAlSetVo);
                                apAlSetVo.setCustomized(true);
                                selectedSiteApAlSetsVOList.add(new ApAlSetVO(apAlSetVo));
                            });
                        }
                        customApAlSets.clear();
                        customApAlSets.addAll(selectedSiteApAlSetsVOList);
                    } else {
                        customApAlSets.clear();
                        pointApAlsets.stream().filter(p -> p.getApSetId().compareTo(selectedApAlSetVO.getApSetId()) == 0).forEachOrdered(p -> customApAlSets.add(p));
                    }
                }
            }

        } catch (Exception e) {
            Logger.getLogger(CreatePointBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Validate Alarm Limits
     *
     * @param apAlByPoint, ApAlSetVO object
     * @param rowId, int
     */
    public void alarmLimitValidation(ApAlSetVO apAlByPoint, int rowId) {
        boolean lfValidationFailed, laValidationFailed, haValidationFailed, hfValidationFailed;

        // Validate Alarm Limits
        if (apAlByPoint != null) {

            // Validates the lowFault field of the given Object
            if (apAlByPoint.isLowFaultActive()) {
                if (apAlByPoint.getLowFault() <= 0) {
                    lfValidationFailed = true;
                } else if (apAlByPoint.isLowAlertActive()) {
                    lfValidationFailed = apAlByPoint.getLowFault() >= apAlByPoint.getLowAlert();
                } else if (apAlByPoint.isHighAlertActive()) {
                    lfValidationFailed = apAlByPoint.getLowFault() >= apAlByPoint.getHighAlert();
                } else if (apAlByPoint.isHighFaultActive()) {
                    lfValidationFailed = apAlByPoint.getLowFault() >= apAlByPoint.getHighFault();
                } else {
                    lfValidationFailed = false;
                }
            } else {
                lfValidationFailed = false;
            }

            // Validates the lowAlert field of the given Object
            if (apAlByPoint.isLowAlertActive()) {
                if (apAlByPoint.getLowAlert() <= 0) {
                    laValidationFailed = true;
                } else if (apAlByPoint.isLowFaultActive() && apAlByPoint.isHighAlertActive()) {
                    laValidationFailed = !(apAlByPoint.getLowAlert() > apAlByPoint.getLowFault() && apAlByPoint.getLowAlert() < apAlByPoint.getHighAlert());
                } else if (apAlByPoint.isLowFaultActive()) {
                    if (apAlByPoint.isLowFaultActive() && apAlByPoint.isHighFaultActive()) {
                        laValidationFailed = !(apAlByPoint.getLowAlert() > apAlByPoint.getLowFault() && apAlByPoint.getLowAlert() < apAlByPoint.getHighFault());
                    } else {
                        laValidationFailed = apAlByPoint.getLowAlert() <= apAlByPoint.getLowFault();
                    }
                } else if (apAlByPoint.isHighAlertActive()) {
                    laValidationFailed = apAlByPoint.getLowAlert() >= apAlByPoint.getHighAlert();
                } else if (apAlByPoint.isHighFaultActive()) {
                    laValidationFailed = apAlByPoint.getLowAlert() >= apAlByPoint.getHighFault();
                } else {
                    laValidationFailed = false;
                }
            } else {
                laValidationFailed = false;
            }

            // Validates the highAlert field of the given Object
            if (apAlByPoint.isHighAlertActive()) {
                if (apAlByPoint.getHighAlert() <= 0) {
                    haValidationFailed = true;
                } else if (apAlByPoint.isLowAlertActive() && apAlByPoint.isHighFaultActive()) {
                    haValidationFailed = !(apAlByPoint.getHighAlert() > apAlByPoint.getLowAlert() && apAlByPoint.getHighAlert() < apAlByPoint.getHighFault());
                } else if (apAlByPoint.isHighFaultActive()) {
                    if (apAlByPoint.isLowFaultActive() && apAlByPoint.isHighFaultActive()) {
                        haValidationFailed = !(apAlByPoint.getHighAlert() > apAlByPoint.getLowFault() && apAlByPoint.getHighAlert() < apAlByPoint.getHighFault());
                    } else {
                        haValidationFailed = apAlByPoint.getHighAlert() >= apAlByPoint.getHighFault();
                    }
                } else if (apAlByPoint.isLowAlertActive()) {
                    haValidationFailed = apAlByPoint.getHighAlert() <= apAlByPoint.getLowAlert();
                } else if (apAlByPoint.isLowFaultActive()) {
                    haValidationFailed = apAlByPoint.getHighAlert() <= apAlByPoint.getLowFault();
                } else {
                    haValidationFailed = false;
                }
            } else {
                haValidationFailed = false;
            }

            // Validates the highFault field of the given Object
            if (apAlByPoint.isHighFaultActive()) {
                if (apAlByPoint.getHighFault() <= 0) {
                    hfValidationFailed = true;
                } else if (apAlByPoint.isHighAlertActive()) {
                    hfValidationFailed = apAlByPoint.getHighFault() <= apAlByPoint.getHighAlert();
                } else if (apAlByPoint.isLowAlertActive()) {
                    hfValidationFailed = apAlByPoint.getHighFault() <= apAlByPoint.getLowAlert();
                } else if (apAlByPoint.isLowFaultActive()) {
                    hfValidationFailed = apAlByPoint.getHighFault() <= apAlByPoint.getLowFault();
                } else {
                    hfValidationFailed = false;
                }
            } else {
                hfValidationFailed = false;
            }

            apAlByPoint.setLowFaultFailure(lfValidationFailed);
            apAlByPoint.setLowAlertFailure(laValidationFailed);
            apAlByPoint.setHighAlertFailure(haValidationFailed);
            apAlByPoint.setHighFaultFailure(hfValidationFailed);

            renderErrorMessage();
        }
    }

    /**
     * perform the given action when a Device Preset is selected
     */
    public void onDevicePresetSelect() {
        selectedSiteDevicePresetList.clear();

        try {
            if (selectedPresetId != null) {
                tachValidationFailed = false;
                templateTachValidationFailed = false;
                selectedSiteDevicePresetList.addAll(devicePresetsList.stream().filter(dp -> dp.getPresetId() != null && dp.getPresetId().equals(selectedPresetId)).collect(Collectors.toList()));
                if (!selectedSiteDevicePresetList.isEmpty() && !selectedSiteDevicePresetList.get(0).getSensorType().equals("TS1X")) {
                    selectedSiteDevicePresetList.stream().forEachOrdered(vo -> {
                        List<ApAlSetVO> apAlSetVOList;
                        if (vo.getDeviceType().equals("StormX") && vo.getChannelType().equals("AC")) {
                            vo.setPointName(pointVO.getPointLocationName() + " "+vo.getSensorLocalOrientation()+ " " + CommonUtilSingleton.getInstance().getPointNameBySensorType(vo.getSensorType()));
                        } else {
                            vo.setPointName(pointVO.getPointLocationName() + " " + CommonUtilSingleton.getInstance().getPointNameBySensorType(vo.getSensorType()));
                        }
                        
                        vo.setSensorUnits(CommonUtilSingleton.getInstance().getSensorUnitBySensorType(vo.getSensorType()));
                        vo.setApAlSetVOList(new ArrayList());
                        vo.setApAlSetSelectItemList(new ArrayList());
                        if ((apAlSetVOList = APALSetClient.getInstance().getGlobalApAlSetsByCustomerApSetIdAlSetId(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), vo.getCustomerAccount(), vo.getApSetId(), vo.getAlSetId())) != null && !apAlSetVOList.isEmpty()) {
                            vo.setApSetName(apAlSetVOList.get(0).getApSetName());
                            vo.setAlSetName(apAlSetVOList.get(0).getAlSetName());
                        } else if ((apAlSetVOList = APALSetClient.getInstance().getGlobalApAlSetsByCustomerApSetIdAlSetId(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), DEFAULT_UPTIME_ACCOUNT, vo.getApSetId(), vo.getAlSetId())) != null && !apAlSetVOList.isEmpty()) {
                            vo.setApSetName(apAlSetVOList.get(0).getApSetName());
                            vo.setAlSetName(apAlSetVOList.get(0).getAlSetName());
                        } else if (pointVO.getSiteId() != null && (apAlSetVOList = APALSetClient.getInstance().getSiteApAlSetsByCustomerSiteIdApSetIdAlSetId(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), vo.getCustomerAccount(), pointVO.getSiteId(), vo.getApSetId(), vo.getAlSetId())) != null && !apAlSetVOList.isEmpty()) {
                            vo.setApSetName(apAlSetVOList.get(0).getApSetName());
                            vo.setAlSetName(apAlSetVOList.get(0).getAlSetName());
                        }
                        
                        vo.setAlSetVOList(apAlSetVOList.stream().filter(ap -> ap.getApSetId() != null && ap.getApSetId().equals(vo.getApSetId())).filter(ap -> ap.getAlSetId() != null && ap.getAlSetId().equals(vo.getAlSetId())).collect(Collectors.toList()));
                        
                        //Check if AP Set requires a tach and if a tach is provided
                        if (!templateTachValidationFailed) {
                            for (ApAlSetVO al: vo.getAlSetVOList()) {
                                if ((al.getFrequencyUnits() != null && al.getFrequencyUnits().equalsIgnoreCase("Orders")) || Utils.validateParamList().contains(al.getParamType())) {
                                    templateTachValidationFailed = selectedPointLocation.getTachId() == null;

                                    if (templateTachValidationFailed) {
                                        break;
                                    }
                                }
                            }
                        }
                        
                    });
                } else {
                    throw new Exception("Invalid Sensor Type or no items found");
                }
            }

        } catch (Exception e) {
            Logger.getLogger(InstallDeviceBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            selectedSiteDevicePresetList.clear();
        }
    }

    /**
     * Render Error message for limits, if there are issue with the values
     *
     * Set renderAlertMsg to true, if limit failure occured
     */
    public void renderErrorMessage() {
        renderAlertMsg = false;
        try {
            for (ApAlSetVO row : selectedSiteApAlSetsVOList) {
                if (row.isLowFaultFailure()
                        || row.isLowAlertFailure()
                        || row.isHighAlertFailure()
                        || row.isHighFaultFailure()) {
                    renderAlertMsg = true;
                    break;
                }
            }
        } catch (Exception e) {
            Logger.getLogger(CreatePointBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    public ApAlSetVO getSelectedApAlSetVO() {
        return selectedApAlSetVO;
    }

    public void setSelectedApAlSetVO(ApAlSetVO selectedApAlSetVO) {
        try {
            if (selectedApAlSetVO != null) {
                this.selectedApAlSetVO = selectedApAlSetVO;
                pointApAlsets.forEach(action -> action.setActive(selectedApAlSetVO.getApSetId().equals(action.getApSetId())));
                pointVO.setCustomized(selectedApAlSetVO.isCustomized());
                onCustomLimitsAlSetNameSelect();
            }
        } catch (Exception e) {
            Logger.getLogger(CreatePointBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    public List<ApAlSetVO> getSelectedSiteApAlSetsVOList() {
        return selectedSiteApAlSetsVOList;
    }

    public List<ApAlSetVO> getDisplayApAlsets() {
        return displayApAlsets;
    }

    public List<SelectItem> getApSetList() {
        return apSetList;
    }

    public List<SelectItem> getAlSetList() {
        return alSetList;
    }

    public List<SelectItem> getDcSensorTypeList() {
        return dcSensorTypeList;
    }

    public List<SelectItem> getAcSensorTypeList() {
        return acSensorTypeList;
    }

    public List<SelectItem> getFmaxList() {
        return fmaxList;
    }

    public List<SelectItem> getDwellList() {
        return dwellList;
    }

    public List<SelectItem> getResolutionList() {
        return resolutionList;
    }

    public boolean isTachValidationFailed() {
        return tachValidationFailed;
    }

    public boolean isTemplateTachValidationFailed() {
        return templateTachValidationFailed;
    }

    public boolean isRenderAlertMsg() {
        return renderAlertMsg;
    }

    public boolean isAddApalSetToPoint() {
        return addApalSetToPoint;
    }

    public PointVO getPointVO() {
        return pointVO;
    }

    public boolean isFromTemplate() {
        return fromTemplate;
    }

    public void setFromTemplate(boolean fromTemplate) {
        this.fromTemplate = fromTemplate;
    }

    public List<DevicePresetVO> getSelectedSiteDevicePresetList() {
        return selectedSiteDevicePresetList;
    }

    public List<DevicePresetVO> getDevicePresetsList() {
        return devicePresetsList;
    }

    public UUID getSelectedPresetId() {
        return selectedPresetId;
    }

    public void setSelectedPresetId(UUID selectedPresetId) {
        this.selectedPresetId = selectedPresetId;
    }

    public List<SelectItem> getDevicePresetsSelectItemList() {
        return devicePresetsSelectItemList;
    }
}
