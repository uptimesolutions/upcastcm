/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.APALSetClient;
import com.uptime.upcastcm.http.client.AreaClient;
import com.uptime.upcastcm.http.client.AssetClient;
import com.uptime.upcastcm.http.client.FaultFrequenciesClient;
import com.uptime.upcastcm.http.client.PointClient;
import com.uptime.upcastcm.http.client.PointLocationClient;
import com.uptime.upcastcm.http.client.SiteClient;
import com.uptime.upcastcm.http.client.TachometerClient;
import com.uptime.upcastcm.http.utils.json.dom.PointLocationDomJsonParser;
import com.uptime.upcastcm.utils.ApplicationConstants;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import static com.uptime.upcastcm.utils.enums.ACSensorTypeEnum.getACSensorTypeItemList;
import static com.uptime.upcastcm.utils.enums.DCSensorTypeEnum.getDCSensorTypeItemList;
import static com.uptime.upcastcm.utils.enums.RollDiameterUnitEnum.getRollDiameterUnitItemList;
import com.uptime.upcastcm.utils.helperclass.AbstractNavigation;
import static com.uptime.upcastcm.utils.helperclass.GenericMessage.showGrowlMsg;
import com.uptime.upcastcm.utils.helperclass.Utils;
import com.uptime.upcastcm.utils.singletons.CommonUtilSingleton;
import com.uptime.upcastcm.utils.vo.ApAlSetVO;
import com.uptime.upcastcm.utils.vo.AreaVO;
import com.uptime.upcastcm.utils.vo.AssetTypesVO;
import com.uptime.upcastcm.utils.vo.AssetVO;
import com.uptime.upcastcm.utils.vo.PointLocationVO;
import com.uptime.upcastcm.utils.vo.PointVO;
import com.uptime.upcastcm.utils.vo.FaultFrequenciesVO;
import com.uptime.upcastcm.utils.vo.SiteVO;
import com.uptime.upcastcm.utils.vo.TachometerVO;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItemGroup;
import org.primefaces.PrimeFaces;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.event.NodeCollapseEvent;
import org.primefaces.event.NodeExpandEvent;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;

/**
 *
 * @author twilcox
 */
public class ConfigurationBean extends AbstractNavigation implements Serializable {

    private UserManageBean userManageBean;

    private final List<TachometerVO> siteTachometersList, globalTachometersList;

    private Map<String, ApAlSetVO> apAlSetVOMap;
    private List<SiteVO> siteVOList;
    private List<AreaVO> areaVOList;
    private List<AssetVO> assetVOList;
    private List<PointLocationVO> pointLocationVOList;
    private List<PointVO> pointVOList;
    private List<ApAlSetVO> siteApAlSetsVOList;
    private List<SelectItem> rollDiameterUnitsList, tachometersList, faultFrequenciesList;
    private List<FaultFrequenciesVO> faultFrequencyList, siteFFSetFavorites, siteFaultFrequenciesVOs;
    private List<TachometerVO> tachometerList;

    private AssetVO selectedAsset;
    private PointLocationVO selectedPointLocation;
    private String createBtnValue, createBtn2Value, createBtn3Value, createBtn4Value, uploadAccPnlDisplay;
    private boolean renderCreateBtn, renderCreateBtn2, renderCreateBtn3, tachValidationFailed, alarmEnabled;
    private PointVO selectedPointVO;
    private UUID tachId;
    private List<PointLocationVO> pointLocationVOlist;
    private List<SelectItem> sensorTypeList;
    private Set<String> sensorTypes, sensorTypeSet;
    private String sensorTypeDefaultLabel, description;
    private List<String> assetTypeList;

    /**
     * Constructor
     */
    public ConfigurationBean() {
        super(POINT);
        siteVOList = new ArrayList();
        areaVOList = new ArrayList();
        assetVOList = new ArrayList();
        pointLocationVOList = new ArrayList();
        pointVOList = new ArrayList();
        siteTachometersList = new ArrayList();
        siteFaultFrequenciesVOs = new ArrayList();
        globalTachometersList = new ArrayList();
        tachometersList = new ArrayList();
        faultFrequenciesList = new ArrayList();
        uploadAccPnlDisplay = "block";
        apAlSetVOMap = new HashMap();
        faultFrequencyList = new ArrayList();
        tachometerList = new ArrayList();
        tachValidationFailed = false;
        siteApAlSetsVOList = new ArrayList();
        pointLocationVOlist = new ArrayList();
        sensorTypeList = new ArrayList();
        sensorTypes = new HashSet();
        try {
            userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
            rollDiameterUnitsList = getRollDiameterUnitItemList();
            assetTypeList = new ArrayList();

            List<AssetTypesVO> assetTypesVOList = new ArrayList();
            assetTypesVOList.addAll(AssetClient.getInstance().getGlobalAssetTypesByCustomer(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount()));
            if (!userManageBean.getCurrentSite().getCustomerAccount().equals(DEFAULT_UPTIME_ACCOUNT)) {
                assetTypesVOList.addAll(AssetClient.getInstance().getGlobalAssetTypesByCustomer(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), DEFAULT_UPTIME_ACCOUNT));
            }
            assetTypesVOList.stream().forEach(a -> assetTypeList.add(a.getAssetTypeName()));

            List<SelectItem> sensorItems;
            sensorItems = getACSensorTypeItemList();
            Collections.sort(sensorItems, Comparator.comparing(SelectItem::getLabel, Comparator.nullsFirst(String::compareToIgnoreCase)));
            sensorTypeList.addAll(Utils.groupSensorTypeList(sensorItems, "Waveform"));
            sensorItems = getDCSensorTypeItemList();
            Collections.sort(sensorItems, Comparator.comparing(SelectItem::getLabel, Comparator.nullsFirst(String::compareToIgnoreCase)));
            sensorTypeList.addAll(Utils.groupSensorTypeList(sensorItems, "Single Measurement"));
        } catch (Exception e) {
            Logger.getLogger(ConfigurationBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Post Constructor
     */
    @PostConstruct
    public void init() {
        resetPage();

        // Set the selectFilters
        try {
            if (userManageBean != null && userManageBean.getCurrentSite() != null) {
                selectedFilters.setRegion(userManageBean.getSelectedFilters().getRegion());
                selectedFilters.setCountry(userManageBean.getSelectedFilters().getCountry());
                selectedFilters.setSite(userManageBean.getSelectedFilters().getSite());
                onSiteChange(true);
            }
        } catch (Exception e) {
            Logger.getLogger(ConfigurationBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * reset Configuration Page values
     */
    @Override
    public void resetPage() {
        setRegionCountrySiteMap();
        setSelectedFilters();
        areaList.clear();
        assetList.clear();
        pointLocationList.clear();
        pointList.clear();
        siteVOList.clear();
        areaVOList.clear();
        assetVOList.clear();
        pointLocationVOList.clear();
        pointVOList.clear();
        siteFaultFrequenciesVOs.clear();
        siteTachometersList.clear();
        selectedAsset = null;
        selectedPointLocation = null;
        selectedPointVO = null;
        tachometersList.clear();
        faultFrequenciesList.clear();
        uploadAccPnlDisplay = "block";
        tachValidationFailed = false;
        pointLocationVOlist.clear();
        sensorTypes.clear();
        try {
            globalTachometersList.clear();
            tachometersList.clear();
            faultFrequenciesList.clear();
            userManageBean.getPresetUtil().clearGlobalTachList();
            userManageBean.getPresetUtil().getGlobalFaultFrequenciesByCustomer(userManageBean.getCurrentSite().getCustomerAccount());
            globalTachometersList.addAll(userManageBean.getPresetUtil().getGlobalTachList(userManageBean.getCurrentSite().getCustomerAccount()));
        } catch (Exception e) {
            faultFrequenciesList.clear();
            globalTachometersList.clear();
            tachometersList.clear();
            uploadAccPnlDisplay = "block";
        }
        updateBreadcrumbModel();
        if (userManageBean != null) {
            treeContentMenuBuilder(ROOT, new SelectItem(null, userManageBean.getResourceBundleString("breadcrumb_4")), null, null);
        }
        updateUI("content");
    }

    @Override
    public void onYearChange(boolean reloadUI) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * called when the region changed
     *
     * @param reloadUI
     */
    @Override
    public void onRegionChange(boolean reloadUI) {
        if (selectedFilters == null) {
            setSelectedFilters();
        }
        if (selectedFilters.getRegion() != null && !selectedFilters.getRegion().isEmpty()) {
            selectedFilters.clear(REGION);
            areaList.clear();
            assetList.clear();
            pointLocationList.clear();
            pointList.clear();
            siteVOList.clear();
            areaVOList.clear();
            assetVOList.clear();
            pointLocationVOList.clear();
            pointVOList.clear();
            siteFaultFrequenciesVOs.clear();
            siteTachometersList.clear();
            selectedAsset = null;
            selectedPointLocation = null;
            selectedPointVO = null;
            tachometersList.clear();
            tachValidationFailed = false;

            if (reloadUI) {
                updateTreeNodeRoot(REGION);
                updateBreadcrumbModel();
                updateUI("content");
            }
        } else {
            resetPage();
        }
    }

    /**
     * called when the country changed
     *
     * @param reloadUI
     */
    @Override
    public void onCountryChange(boolean reloadUI) {
        if (selectedFilters == null) {
            setSelectedFilters();
        }
        if (selectedFilters.getCountry() != null && !selectedFilters.getCountry().isEmpty()) {
            selectedFilters.clear(COUNTRY);
            areaList.clear();
            assetList.clear();
            pointLocationList.clear();
            pointList.clear();
            areaVOList.clear();
            assetVOList.clear();
            pointLocationVOList.clear();
            pointVOList.clear();
            siteFaultFrequenciesVOs.clear();
            siteTachometersList.clear();
            selectedAsset = null;
            selectedPointLocation = null;
            selectedPointVO = null;
            tachometersList.clear();
            faultFrequenciesList.clear();
            tachValidationFailed = false;

            try {
                siteVOList.clear();
                siteVOList.add(SiteClient.getInstance().getSiteRegionCountryByCustomerRegionCountry(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), selectedFilters.getRegion(), selectedFilters.getCountry()).stream().filter(vo -> vo.getSiteId().equals(userManageBean.getCurrentSite().getSiteId())).findFirst().get());
            } catch (Exception ex) {
                siteVOList.clear();
            }

            if (reloadUI) {
                updateTreeNodeRoot(COUNTRY);
                updateBreadcrumbModel();
                updateUI("content");
            }
        } else {
            onRegionChange(reloadUI);
        }
    }

    /**
     * called when the site changed
     *
     * @param reloadUI
     */
    @Override
    public void onSiteChange(boolean reloadUI) {
        if (selectedFilters == null) {
            setSelectedFilters();
        }
        if (selectedFilters.getSite() != null && selectedFilters.getSite().getLabel() != null && !selectedFilters.getSite().getLabel().isEmpty() && selectedFilters.getSite().getValue() != null) {
            selectedFilters.clear(SITE);
            assetList.clear();
            pointLocationList.clear();
            pointList.clear();
            siteVOList.clear();
            assetVOList.clear();
            pointLocationVOList.clear();
            pointVOList.clear();
            selectedAsset = null;
            selectedPointLocation = null;
            selectedPointVO = null;
            tachometersList.clear();
            faultFrequenciesList.clear();
            tachValidationFailed = false;

            try {
                siteFaultFrequenciesVOs.clear();
                siteTachometersList.clear();
                areaVOList.clear();
                areaList.clear();
                userManageBean.getPresetUtil().clearSiteTachList();
                userManageBean.getPresetUtil().clearSiteFaultFrequenciesList();
                areaVOList.addAll(AreaClient.getInstance().getAreaSiteByCustomerSite(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue()));
                areaVOList.stream().sorted(Comparator.comparing(AreaVO::getAreaName)).forEachOrdered(area -> areaList.add(new SelectItem(area.getAreaId(), area.getAreaName())));
                sortAreaVOList();
                siteTachometersList.addAll(userManageBean.getPresetUtil().getSiteTachList(userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue()));
            } catch (Exception ex) {
                areaVOList.clear();
                areaList.clear();
                faultFrequenciesList.clear();
                siteFaultFrequenciesVOs.clear();
                siteTachometersList.clear();
                if (userManageBean != null) {
                    userManageBean.getPresetUtil().clearSiteTachList();
                    userManageBean.getPresetUtil().clearSiteFaultFrequenciesList();
                }
            }

            if (reloadUI) {
                updateTreeNodeRoot(SITE);
                updateBreadcrumbModel();
                updateUI("content");
            }
        } else {
            onCountryChange(reloadUI);
        }
    }

    /**
     * called when the area changed
     *
     * @param reloadUI
     */
    @Override
    public void onAreaChange(boolean reloadUI) {
        if (selectedFilters == null) {
            setSelectedFilters();
        }
        if (selectedFilters.getArea() != null && selectedFilters.getArea().getLabel() != null && !selectedFilters.getArea().getLabel().isEmpty() && selectedFilters.getArea().getValue() != null) {
            selectedFilters.clear(AREA);
            pointLocationList.clear();
            pointList.clear();
            siteVOList.clear();
            areaVOList.clear();
            pointLocationVOList.clear();
            pointVOList.clear();
            selectedAsset = null;
            selectedPointLocation = null;
            selectedPointVO = null;
            tachometersList.clear();
            tachValidationFailed = false;

            try {
                assetVOList.clear();
                assetList.clear();
                assetVOList.addAll(AssetClient.getInstance().getAssetSiteAreaByCustomerSiteArea(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue()));
                assetVOList.stream().forEach(a -> {
                    if (a.isAlarmEnabled() == true) {
                        a.setAlarmEnabledValue(userManageBean.getResourceBundleString("label_true"));
                    } else {
                        a.setAlarmEnabledValue(userManageBean.getResourceBundleString("label_false"));
                    }
                });
                assetVOList.stream().sorted(Comparator.comparing(AssetVO::getAssetName)).forEachOrdered(asset -> assetList.add(new SelectItem(asset.getAssetId(), asset.getAssetName())));
                sortAssetVOList();
            } catch (Exception ex) {
                assetVOList.clear();
                assetList.clear();
            }

            if (reloadUI) {
                updateTreeNodeRoot(AREA);
                updateBreadcrumbModel();
                updateUI("content");
            }
        } else {
            onSiteChange(reloadUI);
        }
    }

    /**
     * called when the asset changed
     *
     * @param reloadUI
     */
    @Override
    public void onAssetChange(boolean reloadUI) {
        if (selectedFilters == null) {
            setSelectedFilters();
        }
        if (selectedFilters.getAsset() != null && selectedFilters.getAsset().getLabel() != null && !selectedFilters.getAsset().getLabel().isEmpty() && selectedFilters.getAsset().getValue() != null) {
            selectedFilters.clear(ASSET);
            pointList.clear();
            siteVOList.clear();
            areaVOList.clear();
            assetVOList.clear();
            pointVOList.clear();
            selectedPointLocation = null;
            selectedPointVO = null;
            tachometersList.clear();
            tachValidationFailed = false;

            try {
                pointLocationVOList.clear();
                pointLocationList.clear();
                selectedAsset = AssetClient.getInstance().getAssetSiteAreaByCustomerSiteArea(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue()).stream().filter(asset -> asset.getAssetId().equals(selectedFilters.getAsset().getValue())).findFirst().get();
                pointLocationVOList.addAll(PointLocationClient.getInstance().getPointLocationsByCustomerSiteAreaAsset(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue(), (UUID) selectedFilters.getAsset().getValue()));
                if (!pointLocationVOList.isEmpty()) {
                    pointLocationVOList.stream().sorted(Comparator.comparing(PointLocationVO::getPointLocationName)).forEachOrdered(pointLocation -> pointLocationList.add(new SelectItem(pointLocation.getPointLocationId(), pointLocation.getPointLocationName())));
                    pointLocationVOList.stream().forEach(pl -> {
                        List<TachometerVO> tachList;

                        if (pl.getTachId() != null) {
                            if ((tachList = TachometerClient.getInstance().getGlobalTachometersByPK(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), pl.getCustomerAccount(), pl.getTachId())) == null || tachList.isEmpty()) {
                                tachList = TachometerClient.getInstance().getSiteTachometersByPK(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), pl.getCustomerAccount(), pl.getSiteId(), pl.getTachId());
                            }
                            if (tachList == null || tachList.isEmpty()) {
                                tachList = TachometerClient.getInstance().getGlobalTachometersByPK(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), ApplicationConstants.DEFAULT_UPTIME_ACCOUNT, pl.getTachId());
                            }
                            pl.setTachReferenceUnits((tachList != null && !tachList.isEmpty() && tachList.get(0).getTachReferenceUnits() != null) ? tachList.get(0).getTachReferenceUnits() : null);
                        }
                    });
                }
                sortPointLocationVOList();
            } catch (Exception ex) {
                Logger.getLogger(ConfigurationBean.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
                selectedAsset = null;
                pointLocationVOList.clear();
                pointLocationList.clear();
            }
            if (reloadUI) {
                updateTreeNodeRoot(ASSET);
                updateBreadcrumbModel();
                updateUI("content");
            }
        } else {
            onAreaChange(reloadUI);
        }
    }

    /**
     * called when the point location changed
     *
     * @param reloadUI
     */
    @Override
    public void onPointLocationChange(boolean reloadUI) {
        List<TachometerVO> tachList;
        Set<String> ffsetnames;

        if (selectedFilters == null) {
            setSelectedFilters();
        }
        if (selectedFilters.getPointLocation() != null && selectedFilters.getPointLocation().getLabel() != null && !selectedFilters.getPointLocation().getLabel().isEmpty() && selectedFilters.getPointLocation().getValue() != null) {
            selectedFilters.clear(POINT_LOCATION);
            siteVOList.clear();
            areaVOList.clear();
            assetVOList.clear();
            pointLocationVOList.clear();
            selectedAsset = null;
            selectedPointVO = null;
            tachValidationFailed = false;

            try {
                pointVOList.clear();
                pointList.clear();
                if (tachometersList != null) {
                    tachometersList.clear();
                }
                if (faultFrequenciesList != null) {
                    faultFrequenciesList.clear();
                }
                selectedPointLocation = PointLocationClient.getInstance().getPointLocationsByPK(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue(), (UUID) selectedFilters.getAsset().getValue(), (UUID) selectedFilters.getPointLocation().getValue()).get(0);
                if (selectedPointLocation != null) {

                    // get tachometer presets
                    userManageBean.getPresetUtil().clearSiteTachList();
                    userManageBean.getPresetUtil().clearGlobalTachList();
                    tachometersList.addAll(userManageBean.getPresetUtil().populateTachometerList(userManageBean, userManageBean.getCurrentSite().getCustomerAccount(), selectedPointLocation.getSiteId()));
                    if ((tachId = selectedPointLocation.getTachId()) != null) {
                        if ((tachList = TachometerClient.getInstance().getGlobalTachometersByPK(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), DEFAULT_UPTIME_ACCOUNT, selectedPointLocation.getTachId())) == null || tachList.isEmpty()) {
                            if ((tachList = TachometerClient.getInstance().getGlobalTachometersByPK(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), selectedPointLocation.getCustomerAccount(), selectedPointLocation.getTachId())) == null || tachList.isEmpty()) {
                                tachList = TachometerClient.getInstance().getSiteTachometersByPK(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), selectedPointLocation.getCustomerAccount(), selectedPointLocation.getSiteId(), selectedPointLocation.getTachId());
                            }
                        }
                        
                        if (tachList != null && !tachList.isEmpty()) {
                            selectedPointLocation.setTachReferenceUnits(tachList.get(0).getTachReferenceUnits());
                            selectedPointLocation.setTachName(tachList.get(0).getTachName());
                        }
                    }

                    // get fault frequencies presets
                    userManageBean.getPresetUtil().clearSiteFaultFrequenciesList();
                    userManageBean.getPresetUtil().clearGlobalFaultFrequenciesList();
                    faultFrequenciesList = populateFFSetList();
                    ffsetnames = new HashSet();
                    if (selectedPointLocation.getFfSetIds() != null) {
                        ffsetnames = PointLocationDomJsonParser.getInstance().populateFFSetFromJson(selectedPointLocation.getFfSetIds(), siteFFSetFavorites);
                    }
                    selectedPointLocation.setFfSetName(ffsetnames);
                    
                    selectedFilters.setPointLocAlarmEnabled(selectedPointLocation.isAlarmEnabled());
                }
                pointVOList.addAll(PointClient.getInstance().getPointsByCustomerSiteAreaAssetPointLocation(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue(), (UUID) selectedFilters.getAsset().getValue(), (UUID) selectedFilters.getPointLocation().getValue(), true));
                pointVOList.stream().forEach(a -> {
                    if (a.isAlarmEnabled()) {
                        a.setAlarmEnabledValue(userManageBean.getResourceBundleString("label_true"));
                    } else {
                        a.setAlarmEnabledValue(userManageBean.getResourceBundleString("label_false"));
                    }
                });
                setupApAlSetVOs(pointVOList);
                pointVOList.stream().sorted(Comparator.comparing(PointVO::getPointName)).forEachOrdered(point -> pointList.add(new SelectItem(point.getPointId(), point.getPointName())));
                CommonUtilSingleton.getInstance().sortPointsByPointType(pointVOList);
            } catch (Exception ex) {
                Logger.getLogger(ConfigurationBean.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
                selectedPointLocation = null;
                tachometersList.clear();
                pointVOList.clear();
                pointList.clear();
            }

            if (reloadUI) {
                updateTreeNodeRoot(POINT_LOCATION);
                updateBreadcrumbModel();
                updateUI("content");
            }
        } else {
            onAssetChange(reloadUI);
        }
    }

    /**
     * called when the point changed
     *
     * @param reloadUI
     */
    @Override
    public void onPointChange(boolean reloadUI) {
        List<PointVO> pointVOs;

        if (selectedFilters == null) {
            setSelectedFilters();
        }
        if (selectedFilters.getPoint() != null && selectedFilters.getPoint().getLabel() != null && !selectedFilters.getPoint().getLabel().isEmpty() && selectedFilters.getPoint().getValue() != null) {
            siteVOList.clear();
            areaVOList.clear();
            assetVOList.clear();
            pointLocationVOList.clear();
            selectedAsset = null;
            selectedPointLocation = null;
            tachometersList.clear();
            tachValidationFailed = false;
            try {
                pointVOList.clear();
                pointVOs = PointClient.getInstance().getPointsByCustomerSiteAreaAssetPointLocationPoint(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue(), (UUID) selectedFilters.getAsset().getValue(), (UUID) selectedFilters.getPointLocation().getValue(), (UUID) selectedFilters.getPoint().getValue(), true);
                selectedPointVO = pointVOs.get(0);
                selectedFilters.setPoint(new SelectItem(selectedPointVO.getPointId(), selectedPointVO.getPointName()));
                editPoint(selectedPointVO);
                setupApAlSetVOs(pointVOs);
            } catch (Exception ex) {
                Logger.getLogger(ConfigurationBean.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
                pointVOList.clear();
            }

            if (reloadUI) {
                updateTreeNodeRoot(POINT);
                updateBreadcrumbModel();
                updateUI("content");
            }
        } else {
            onPointLocationChange(reloadUI);
        }
    }

    @Override
    public void onApSetChange(boolean reloadUI) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void onAlSetParamChange(boolean reloadUI) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public List<SelectItem> populateFFSetList() {
        List<SelectItem> ffSetList;

        ffSetList = new ArrayList();
        siteFFSetFavorites = new ArrayList();
        try {
            siteFFSetFavorites = FaultFrequenciesClient.getInstance().getSiteFFSetFavoritesVOList(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), userManageBean.getCurrentSite().getSiteId());
            ffSetList = userManageBean.getPresetUtil().populateFFSetNameList(userManageBean, siteFFSetFavorites);
        } catch (Exception e) {
            Logger.getLogger(CreatePointLocationBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }

        return ffSetList;
    }

    private void setupApAlSetVOs(List<PointVO> pointVOList) {
        List<ApAlSetVO> apAlSetsVOList;

        if (userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null) {
            for (PointVO pointVO : pointVOList) {
                if (apAlSetVOMap.containsKey(pointVO.getApSetId().toString() + "," + pointVO.getAlSetId().toString())) {
                    pointVO.setApSetName(apAlSetVOMap.get(pointVO.getApSetId().toString() + "," + pointVO.getAlSetId().toString()).getApSetName());
                    pointVO.setAlSetName(apAlSetVOMap.get(pointVO.getApSetId().toString() + "," + pointVO.getAlSetId().toString()).getAlSetName());
                } else {
                    if ((apAlSetsVOList = APALSetClient.getInstance().getSiteApAlSetsByCustomerSiteIdApSetIdAlSetId(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), pointVO.getSiteId(), pointVO.getApSetId(), pointVO.getAlSetId())) == null || apAlSetsVOList.isEmpty()) {
                        if ((apAlSetsVOList = APALSetClient.getInstance().getGlobalApAlSetsByCustomerApSetIdAlSetId(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), pointVO.getApSetId(), pointVO.getAlSetId())) == null || apAlSetsVOList.isEmpty()) {
                            apAlSetsVOList = APALSetClient.getInstance().getGlobalApAlSetsByCustomerApSetIdAlSetId(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), DEFAULT_UPTIME_ACCOUNT, pointVO.getApSetId(), pointVO.getAlSetId());
                        }
                    }
                    if (apAlSetsVOList != null && !apAlSetsVOList.isEmpty()) {
                        pointVO.setApSetName(apAlSetsVOList.get(0).getApSetName());
                        pointVO.setAlSetName(apAlSetsVOList.get(0).getAlSetName());
                        apAlSetVOMap.put(pointVO.getApSetId().toString() + "," + pointVO.getAlSetId().toString(), apAlSetsVOList.get(0));
                    }
                }
            }
        }
    }

    /**
     * Update the UI based on the given value
     *
     * @param value
     */
    @Override
    public void updateUI(String value) {
        try {
            switch (value) {
                case "content":
                    if (selectedFilters != null) {
                        if (selectedFilters.getPoint() != null && selectedFilters.getPoint().getLabel() != null && !selectedFilters.getPoint().getLabel().isEmpty()) {
                            renderCreateBtn = false;
                            renderCreateBtn2 = false;
                            renderCreateBtn3 = false;
                            createBtnValue = "";
                            createBtn2Value = "";
                            createBtn3Value = "";
                            createBtn4Value = "";
                            uploadAccPnlDisplay = "none";
                        } else if (selectedFilters.getPointLocation() != null && selectedFilters.getPointLocation().getLabel() != null && !selectedFilters.getPointLocation().getLabel().isEmpty()) {
                            renderCreateBtn = true;
                            renderCreateBtn2 = true;
                            renderCreateBtn3 = false;
                            createBtn4Value = "";
                            uploadAccPnlDisplay = "block";
                            createBtnValue = userManageBean.getResourceBundleString("dialog_createPoint_header");
                            createBtn2Value = userManageBean.getResourceBundleString("dialog_install_device_header");
                            createBtn3Value = userManageBean.getResourceBundleString("label_replace_device");
                        } else if (selectedFilters.getAsset() != null && selectedFilters.getAsset().getLabel() != null && !selectedFilters.getAsset().getLabel().isEmpty()) {
                            renderCreateBtn = true;
                            renderCreateBtn2 = false;
                            renderCreateBtn3 = true;
                            uploadAccPnlDisplay = "block";
                            createBtnValue = userManageBean.getResourceBundleString("dialog_createPointLocation_header");
                            createBtn2Value = "";
                            createBtn3Value = "";
                            createBtn4Value = userManageBean.getResourceBundleString("dialog_createPointLocation_header");
                        } else if (selectedFilters.getArea() != null && selectedFilters.getArea().getLabel() != null && !selectedFilters.getArea().getLabel().isEmpty()) {
                            renderCreateBtn = true;
                            renderCreateBtn2 = false;
                            renderCreateBtn3 = false;
                            uploadAccPnlDisplay = "block";
                            createBtnValue = userManageBean.getResourceBundleString("dialog_createAsset_header");
                            createBtn2Value = "";
                            createBtn3Value = "";
                            createBtn4Value = "";
                        } else if (selectedFilters.getSite() != null && selectedFilters.getSite().getLabel() != null && !selectedFilters.getSite().getLabel().isEmpty()) {
                            renderCreateBtn = true;
                            renderCreateBtn2 = false;
                            renderCreateBtn3 = false;
                            uploadAccPnlDisplay = "block";
                            createBtnValue = userManageBean.getResourceBundleString("dialog_createArea_header");
                            createBtn2Value = "";
                            createBtn3Value = "";
                            createBtn4Value = "";
                        } else if (selectedFilters.getCountry() != null && !selectedFilters.getCountry().isEmpty()) {
                            renderCreateBtn = false;
                            renderCreateBtn2 = false;
                            renderCreateBtn3 = false;
                            uploadAccPnlDisplay = "block";
                            createBtnValue = "";
                            createBtn2Value = "";
                            createBtn3Value = "";
                            createBtn4Value = "";
                        } else {
                            renderCreateBtn = false;
                            renderCreateBtn2 = false;
                            renderCreateBtn3 = false;
                            uploadAccPnlDisplay = "block";
                            createBtnValue = "";
                            createBtn2Value = "";
                            createBtn3Value = "";
                            createBtn4Value = "";
                        }
                    }
                    PrimeFaces.current().executeScript("if(document.getElementById('uploadAccordionPanelId')){document.getElementById('uploadAccordionPanelId').style.display = '" + uploadAccPnlDisplay + "';}");
                    PrimeFaces.current().ajax().update("configurationFilterFormId");
                    PrimeFaces.current().ajax().update("configurationResultFormId");
                    break;
                case "tree":
                    PrimeFaces.current().ajax().update("leftTreeFormId");
                    break;
                case "breadcrumb":
                    PrimeFaces.current().ajax().update("breadcrumbFormId");
                    break;
                case "pointLocationTable":
                    PrimeFaces.current().ajax().update("pointLocationDataTableId");
                    break;
            }
        } catch (Exception e) {
            Logger.getLogger(ConfigurationBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    public void updateBean() {
        Logger.getLogger(ConfigurationBean.class.getName()).log(Level.INFO, "**Updated ConfigurationBean {0}", "");
    }

    public void updateSensorTypes(String sensorType) {
        Logger.getLogger(ConfigurationBean.class.getName()).log(Level.INFO, "**Update Sensor Types", "");
        sensorTypeDefaultLabel = sensorType;
    }

    public void updateAlarmStatus() {
        if (sensorTypeDefaultLabel.equalsIgnoreCase(userManageBean.getResourceBundleString("label_sensor_type_default"))) {
            sensorTypes = sensorTypeSet;
        }
        if (selectedFilters.getAsset() != null && selectedAsset != null) {
            selectedAsset.setSensorTypes(String.join(",", sensorTypes));
            selectedAsset.setAlarmEnabled(alarmEnabled);
            AssetClient.getInstance().updateAsset(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), selectedAsset);
            selectedAsset.setSensorTypes(null);
            selectedAsset.setAlarmEnabled(false);
        } else if (selectedFilters.getPointLocation() != null && selectedPointLocation != null) {
            selectedPointLocation.setSensorTypes(String.join(",", sensorTypes));
            selectedPointLocation.setAlarmEnabled(alarmEnabled);
            PointLocationClient.getInstance().updatePointLocation(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), selectedPointLocation);
            pointVOList.stream().filter(point -> sensorTypes.contains(point.getSensorType())).forEach(a -> {
                a.setAlarmEnabled(alarmEnabled);
                if (a.isAlarmEnabled()) {
                    a.setAlarmEnabledValue(userManageBean.getResourceBundleString("label_true"));
                } else {
                    a.setAlarmEnabledValue(userManageBean.getResourceBundleString("label_false"));
                }
            });
            updateUI("content");
            selectedPointLocation.setSensorTypes(null);
            selectedPointLocation.setAlarmEnabled(false);
        }
        alarmEnabled = false;
        Logger.getLogger(ConfigurationBean.class.getName()).log(Level.INFO, "**Updated Alarm Status {0}", "");
    }

    /**
     * Updates the create dialog boxes based on the Selection filters
     */
    public void createItem() {
        List<PointLocationVO> pvoList;

        try {
            if (selectedFilters.getPointLocation() != null && selectedPointLocation != null) {
                if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("createPointBean") == null) {
                    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("createPointBean", new CreatePointBean());
                    ((CreatePointBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("createPointBean")).init();
                }
                ((CreatePointBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("createPointBean")).presetFields(selectedFilters, selectedPointLocation);
                navigationBean.updateDialog("create-point");
                PrimeFaces.current().executeScript("PF('defaultDlg').show()");
            } else if (selectedFilters.getAsset() != null && selectedAsset != null) {
                if ((pvoList = PointLocationClient.getInstance().getPointLocationsByCustomerSiteAreaAsset(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue(), (UUID) selectedFilters.getAsset().getValue())) != null && pvoList.size() > 2000) {
                    showGrowlMsg("growl_create_point_location_check_message");
                } else {
                    if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("createPointLocationBean") == null) {
                        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("createPointLocationBean", new CreatePointLocationBean());
                        ((CreatePointLocationBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("createPointLocationBean")).init();
                    }
                    ((CreatePointLocationBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("createPointLocationBean")).presetFields(selectedFilters, selectedAsset);
                    navigationBean.updateDialog("create-point-location");
                    PrimeFaces.current().executeScript("PF('defaultDlg').show()");
                }
            } else if (selectedFilters.getArea() != null) {
                if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("createAssetBean") == null) {
                    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("createAssetBean", new CreateAssetBean());
                    ((CreateAssetBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("createAssetBean")).init();
                }
                ((CreateAssetBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("createAssetBean")).presetFields(selectedFilters, null);
                navigationBean.updateDialog("create-asset");
                PrimeFaces.current().executeScript("PF('defaultDlg').show()");
            } else if (selectedFilters.getSite() != null) {
                if ((CreateAreaBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("createAreaBean") == null) {
                    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("createAreaBean", new CreateAreaBean());
                    ((CreateAreaBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("createAreaBean")).init();
                }
                ((CreateAreaBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("createAreaBean")).presetFields(selectedFilters, null);
                navigationBean.updateDialog("create-area");
                PrimeFaces.current().executeScript("PF('defaultDlg').show()");
            }
        } catch (Exception e) {
            Logger.getLogger(ConfigurationBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Launches the Pair Device overlay
     */
    public void launchInstallDeviceOverlay() {
        try {
            if (selectedFilters.getPointLocation() != null && selectedPointLocation != null) {
                if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("installDeviceBean") == null) {
                    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("installDeviceBean", new InstallDeviceBean());
                }
                ((InstallDeviceBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("installDeviceBean")).init();
                ((InstallDeviceBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("installDeviceBean")).presetFields(selectedFilters, selectedPointLocation);
                navigationBean.updateDialog("install-device");
            }
        } catch (Exception e) {
            Logger.getLogger(ConfigurationBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Launches the Map Existing Points overlay
     */
    public void launchReplaceDeviceOverlay() {
        try {
            if (selectedFilters.getPointLocation() != null && selectedPointLocation != null) {
                if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("replaceDeviceBean") == null) {
                    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("replaceDeviceBean", new ReplaceDeviceBean());
                }
                ((ReplaceDeviceBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("replaceDeviceBean")).init();
                ((ReplaceDeviceBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("replaceDeviceBean")).presetFields(selectedFilters, selectedPointLocation);
                navigationBean.updateDialog("replace-device");
            }
        } catch (Exception e) {
            Logger.getLogger(ConfigurationBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Launches the Pair Device overlay
     */
    public void launchModifyAlarmStatusOverlay() {
        try {
            alarmEnabled = false;
            sensorTypes.clear();
            sensorTypeSet = sensorTypeList.stream()
                    .flatMap(item -> {
                        if (item instanceof SelectItemGroup) {
                            // If it's a group, flatten its items into the stream
                            return Arrays.stream(((SelectItemGroup) item).getSelectItems());
                        } else {
                            // If it's a single item, return it as a stream
                            return Stream.of(item);
                        }
                    })
                    .map(item -> String.valueOf(item.getValue())) // Convert value to String
                    .collect(Collectors.toSet());
            sensorTypes = sensorTypeSet;
            sensorTypeDefaultLabel = userManageBean.getResourceBundleString("label_sensor_type_default");
            if (selectedFilters.getAsset() != null && selectedAsset != null) {
                description = userManageBean.getResourceBundleString("message_modify_alarm_status_at_asset");
            } else if (selectedFilters.getPointLocation() != null && selectedPointLocation != null) {
                description = userManageBean.getResourceBundleString("message_modify_alarm_status_at_point_location");
            }
            navigationBean.updateDialog("modify-alarm-status");
        } catch (Exception e) {
            Logger.getLogger(ConfigurationBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Update the current item
     *
     * @param item, Object
     */
    public void updateItem(Object item) {
        if (item instanceof PointVO) {
            try {
                if (selectedFilters.getPointLocation() != null && selectedPointLocation != null) {
                    if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("editPointBean") == null) {
                        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("editPointBean", new EditPointBean());
                        ((EditPointBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("editPointBean")).init();
                    }
                    ((EditPointBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("editPointBean")).presetFields(selectedFilters, item);
                    navigationBean.updateDialog("edit-point");
                }
            } catch (Exception e) {
                Logger.getLogger(ConfigurationBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            }
        } else if (item instanceof AreaVO) {
            if ((EditAreaBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("editAreaBean") == null) {
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("editAreaBean", new EditAreaBean());
                ((EditAreaBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("editAreaBean")).init();
            }
            ((EditAreaBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("editAreaBean")).presetFields(selectedFilters, item);
            navigationBean.updateDialog("edit-area");
        } else if (item instanceof AssetVO) {
            if ((EditAssetBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("editAssetBean") == null) {
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("editAssetBean", new EditAssetBean());
                ((EditAssetBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("editAssetBean")).init();
            }
            ((EditAssetBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("editAssetBean")).presetFields(selectedFilters, item);
            navigationBean.updateDialog("edit-asset");
        } else if (item instanceof PointLocationVO) {
            if ((EditPointLocationBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("editPointLocationBean") == null) {
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("editPointLocationBean", new EditPointLocationBean());
                ((EditPointLocationBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("editPointLocationBean")).init();
            }
            ((EditPointLocationBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("editPointLocationBean")).presetFields(selectedFilters, item);
            navigationBean.updateDialog("edit-point-location");
        } else if (item instanceof SiteVO) {
            if ((EditSiteBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("editSiteBean") == null) {
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("editSiteBean", new EditSiteBean());
                ((EditSiteBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("editSiteBean")).init();
            }
            ((EditSiteBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("editSiteBean")).presetFields(selectedFilters, item);
            navigationBean.updateDialog("edit-site");
        }
    }

    /**
     * Updates the the given PointLocationVO's field pointList and the UI
     *
     * @param pointLocationVO PointLocationVO Object
     */
    public void pointLookupByRowToggle(PointLocationVO pointLocationVO) {
        List<TachometerVO> tachometeritemsList;
        List<String> ffsetnameList;
        Set<String> ffsetnames;
        UUID id;

        if (pointLocationVO != null) {
            if (pointLocationVO.getPointList() != null) {
                pointLocationVO.setPointList(null);
                updateUI("pointLocationTable");
            } else if (userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null
                    && selectedFilters.getSite() != null && selectedFilters.getSite().getValue() != null
                    && selectedFilters.getArea() != null && selectedFilters.getArea().getValue() != null
                    && selectedFilters.getAsset() != null && selectedFilters.getAsset().getValue() != null
                    && pointLocationVO.getPointLocationName() != null && !pointLocationVO.getPointLocationName().isEmpty()) {

                pointVOList = PointClient.getInstance().getPointsByCustomerSiteAreaAssetPointLocation(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue(), (UUID) selectedFilters.getAsset().getValue(), pointLocationVO.getPointLocationId(), true);
                setupApAlSetVOs(pointVOList);

                pointLocationVO.setPointList(pointVOList);

                // get tachometer presets
                if (tachometersList.isEmpty()) {
                    tachometersList.addAll(userManageBean.getPresetUtil().populateTachometerList(userManageBean, userManageBean.getCurrentSite().getCustomerAccount(), pointLocationVO.getSiteId()));
                }

                // get fault frequencies presets
                if (faultFrequenciesList.isEmpty()) {
                    faultFrequenciesList = populateFFSetList();
                }

                if (pointLocationVO.getTachId() != null) {
                    if ((tachometeritemsList = tachometerList) == null || tachometeritemsList.isEmpty()) {
                        tachometeritemsList = getTachometerList(pointLocationVO.getSiteId());
                    }

                    for (TachometerVO tachometerVO : tachometeritemsList) {
                        id = UUID.fromString(tachometerVO.getTachId().toString());
                        if (id.equals(pointLocationVO.getTachId())) {
                            pointLocationVO.setTachName(tachometerVO.getTachName());
                        }
                    }
                }

                if (pointLocationVO.getFfSetIds() != null) {
                    ffsetnames = PointLocationDomJsonParser.getInstance().populateFFSetFromJson(pointLocationVO.getFfSetIds(), siteFFSetFavorites);
                    pointLocationVO.setFfSetName(ffsetnames);

                    ffsetnameList = PointLocationDomJsonParser.getInstance().populateFFSetFromJson(pointLocationVO.getFfSetIds());
                    pointLocationVO.setFfSetNames(String.join(",", ffsetnameList));
                }
                updateUI("pointLocationTable");
            }
        }
    }

    /**
     * get Tachometers
     *
     * @param siteId
     * @return
     */
    public List<TachometerVO> getTachometerList(UUID siteId) {
        tachometerList = new ArrayList();
        try {
            userManageBean.getPresetUtil().clearSiteTachList();

            if (!userManageBean.getCurrentSite().getCustomerAccount().equals(DEFAULT_UPTIME_ACCOUNT)) {
                tachometerList.addAll(TachometerClient.getInstance().getGlobalTachometersByCustomer(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), DEFAULT_UPTIME_ACCOUNT));
            }
            tachometerList.addAll(userManageBean.getPresetUtil().getGlobalTachList(userManageBean.getCurrentSite().getCustomerAccount()));
            tachometerList.addAll(userManageBean.getPresetUtil().getSiteTachList(userManageBean.getCurrentSite().getCustomerAccount(), siteId));
            tachometerList = tachometerList.stream().filter(Utils.distinctByKeys(TachometerVO::getTachId)).collect(Collectors.toList());
            tachometerList.sort(Comparator.comparing(TachometerVO::getTachName));
        } catch (Exception e) {
            Logger.getLogger(PresetsBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            tachometerList.clear();
        }
        return tachometerList;
    }

    /**
     * Fault Frequencies
     *
     * @param siteId
     * @return
     */
    public List<FaultFrequenciesVO> getFaultFrequencies(UUID siteId) {
        faultFrequencyList = new ArrayList();

        try {
            userManageBean.getPresetUtil().clearSiteFaultFrequenciesList();
            if (!userManageBean.getCurrentSite().getCustomerAccount().equals(DEFAULT_UPTIME_ACCOUNT)) {
                faultFrequencyList.addAll(FaultFrequenciesClient.getInstance().getGlobalFaultFrequenciesByCustomer(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), DEFAULT_UPTIME_ACCOUNT));
            }
            faultFrequencyList.addAll(userManageBean.getPresetUtil().getGlobalFaultFrequenciesByCustomer(userManageBean.getCurrentSite().getCustomerAccount()));
            faultFrequencyList.addAll(userManageBean.getPresetUtil().getSiteFaultFrequenciesList(userManageBean.getCurrentSite().getCustomerAccount(), siteId));
            faultFrequencyList = faultFrequencyList.stream().filter(Utils.distinctByKeys(FaultFrequenciesVO::getFfSetId)).collect(Collectors.toList());
            faultFrequencyList.sort(Comparator.comparing(FaultFrequenciesVO::getFfSetName));
        } catch (Exception e) {
            Logger.getLogger(PresetsBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            faultFrequencyList.clear();
        }
        return faultFrequencyList;
    }

    /**
     * Update the SelectedFilters based on the given info
     *
     * @param type, String Object
     * @param label_1, String Object
     * @param id_1, UUID Object
     * @param label_2, String Object
     * @param id_2, UUID Object
     */
    public void updateSelection(String type, String label_1, UUID id_1, String label_2, UUID id_2) {
        if (type != null && !type.isEmpty() && label_1 != null && !label_1.isEmpty() && id_1 != null) {
            switch (type.toLowerCase()) {
                case SITE:
                    selectedFilters.setSite(new SelectItem(id_1, label_1));
                    onSiteChange(true);
                    break;
                case AREA:
                    selectedFilters.setArea(new SelectItem(id_1, label_1));
                    onAreaChange(true);
                    break;
                case ASSET:
                    selectedFilters.setAsset(new SelectItem(id_1, label_1));
                    onAssetChange(true);
                    break;
                case POINT_LOCATION:
                    selectedFilters.setPointLocation(new SelectItem(id_1, label_1));
                    if (label_2 != null && !label_2.isEmpty()) {
                        selectedFilters.setPoint(new SelectItem(id_2, label_2));
                        onPointChange(true);
                    } else {
                        onPointLocationChange(true);
                    }
                    break;
                case POINT:
                    selectedFilters.setPoint(new SelectItem(id_1, label_1));
                    onPointChange(true);
                    break;
            }
        }
    }

    /**
     * Update the breadcrumbModel field from the NavigationBean
     */
    @Override
    public void updateBreadcrumbModel() {
        DefaultMenuItem index;

        if (navigationBean != null && userManageBean != null) {

            // Set Root
            navigationBean.setBreadcrumbModel(new DefaultMenuModel());
            index = new DefaultMenuItem();
            index.setValue(userManageBean.getResourceBundleString("breadcrumb_4"));
            index.setIcon("fa fa-wrench");
            index.setCommand("#{configurationBean.resetPage()}");
            index.setOncomplete("updateTreeNodes()");
            navigationBean.getBreadcrumbModel().getElements().add(index);

            try {
                // Set Region
                if (selectedFilters.getRegion() != null && !selectedFilters.getRegion().isEmpty()) {
                    updateBreadcrumbModelHelper(selectedFilters.getRegion(), "#{configurationBean.onRegionChange(true)}", navigationBean.getBreadcrumbModel().getElements());
                }

                // Set Country
                if (selectedFilters.getCountry() != null && !selectedFilters.getCountry().isEmpty()) {
                    updateBreadcrumbModelHelper(selectedFilters.getCountry(), "#{configurationBean.onCountryChange(true)}", navigationBean.getBreadcrumbModel().getElements());
                }

                // Set Site
                if (selectedFilters.getSite() != null && selectedFilters.getSite().getLabel() != null && !selectedFilters.getSite().getLabel().isEmpty()) {
                    updateBreadcrumbModelHelper(selectedFilters.getSite().getLabel(), "#{configurationBean.onSiteChange(true)}", navigationBean.getBreadcrumbModel().getElements());
                }

                // Set Area
                if (selectedFilters.getArea() != null && selectedFilters.getArea().getLabel() != null && !selectedFilters.getArea().getLabel().isEmpty()) {
                    updateBreadcrumbModelHelper(selectedFilters.getArea().getLabel(), "#{configurationBean.onAreaChange(true)}", navigationBean.getBreadcrumbModel().getElements());
                }

                // Set Asset
                if (selectedFilters.getAsset() != null && selectedFilters.getAsset().getLabel() != null && !selectedFilters.getAsset().getLabel().isEmpty()) {
                    updateBreadcrumbModelHelper(selectedFilters.getAsset().getLabel(), "#{configurationBean.onAssetChange(true)}", navigationBean.getBreadcrumbModel().getElements());
                }

                // Set Point Location
                if (selectedFilters.getPointLocation() != null && selectedFilters.getPointLocation().getLabel() != null && !selectedFilters.getPointLocation().getLabel().isEmpty()) {
                    updateBreadcrumbModelHelper(selectedFilters.getPointLocation().getLabel(), "#{configurationBean.onPointLocationChange(true)}", navigationBean.getBreadcrumbModel().getElements());
                }

                // Set Point
                if (selectedFilters.getPoint() != null && selectedFilters.getPoint().getLabel() != null && !selectedFilters.getPoint().getLabel().isEmpty()) {
                    updateBreadcrumbModelHelper(selectedFilters.getPoint().getLabel(), "#{configurationBean.onPointChange(true)}", navigationBean.getBreadcrumbModel().getElements());
                }
            } catch (Exception e) {
                Logger.getLogger(ConfigurationBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);

                // Set Root
                navigationBean.setBreadcrumbModel(new DefaultMenuModel());
                index = new DefaultMenuItem();
                index.setValue(userManageBean.getResourceBundleString("breadcrumb_4"));
                index.setIcon("fa fa-wrench");
                index.setCommand("#{configurationBean.resetPage()}");
                index.setOncomplete("updateTreeNodes()");
                navigationBean.getBreadcrumbModel().getElements().add(index);
            }
            updateUI("breadcrumb");
        }
    }

    /**
     * Build the treeNodeRoot field from the NavigationBean
     *
     * @param type, String Object
     * @param data, Object
     * @param parent, TreeNode Object
     * @param list, List Object of Objects
     */
    @Override
    public void treeContentMenuBuilder(String type, Object data, TreeNode parent, List<Object> list) {
        TreeNode newNode;
        boolean useList = false;

        if (navigationBean != null && userManageBean != null) {
            if (type != null) {
                switch (type.toLowerCase()) {
                    case ROOT:
                        newNode = new DefaultTreeNode(ROOT, data, null);
                        navigationBean.setTreeNodeRoot(newNode);
                        if (regionCountrySiteMap.keySet() != null && !regionCountrySiteMap.keySet().isEmpty()) {
                            regionCountrySiteMap.keySet().stream().sorted().collect(Collectors.toList()).forEach(key -> treeContentMenuBuilder(REGION, key, navigationBean.getTreeNodeRoot(), null));
                        }
                        updateUI("tree");
                        break;
                    case REGION:
                        list = (data == null) ? new ArrayList(regionCountrySiteMap.keySet().stream().sorted().collect(Collectors.toList())) : new ArrayList();
                        treeContentMenuBuilderHelper(type, data, parent, list, true);
                        break;
                    case COUNTRY:
                        if (parent != null) {
                            list = (data == null) ? new ArrayList(regionCountrySiteMap.get((String) parent.getData()).keySet().stream().sorted().collect(Collectors.toList())) : new ArrayList();
                            treeContentMenuBuilderHelper(type, data, parent, list, true);
                        }
                        break;
                    case SITE_OVER_100:
                        list = new ArrayList(regionCountrySiteMap.get((String) parent.getParent().getData()).get((String) parent.getData()).stream().sorted(Comparator.comparing(SelectItem::getLabel)).collect(Collectors.toList()));
                        treeContentMenuBuilderHelper(type, data, SITE, parent, list, true);
                        break;
                    case SITE:
                        if (parent != null) {
                            list = (data == null) ? new ArrayList(regionCountrySiteMap.get((String) parent.getParent().getData()).get((String) parent.getData()).stream().sorted(Comparator.comparing(SelectItem::getLabel)).collect(Collectors.toList())) : new ArrayList();
                            treeContentMenuBuilderHelper(type, data, parent, list, true);
                        }
                        break;
                    case AREA_OVER_100:
                        if (list == null || list.isEmpty()) {
                            list = areaList != null ? new ArrayList(areaList) : new ArrayList();
                        } else {
                            useList = true;
                        }
                        treeContentMenuBuilderHelper(type, data, AREA, parent, list, useList);
                        break;
                    case AREA:
                        if (list == null || list.isEmpty()) {
                            list = areaList != null ? new ArrayList(areaList) : new ArrayList();
                        }
                        treeContentMenuBuilderHelper(type, data, parent, list, true);
                        break;
                    case ASSET_OVER_100:
                        if (list == null || list.isEmpty()) {
                            list = assetList != null ? new ArrayList(assetList) : new ArrayList();
                        } else {
                            useList = true;
                        }
                        treeContentMenuBuilderHelper(type, data, ASSET, parent, list, useList);
                        break;
                    case ASSET:
                        if (list == null || list.isEmpty()) {
                            list = assetList != null ? new ArrayList(assetList) : new ArrayList();
                        }
                        treeContentMenuBuilderHelper(type, data, parent, list, true);
                        break;
                    case POINT_LOCATION_OVER_100:
                        if (list == null || list.isEmpty()) {
                            list = pointLocationList != null ? new ArrayList(pointLocationList) : new ArrayList();
                        } else {
                            useList = true;
                        }
                        treeContentMenuBuilderHelper(type, data, POINT_LOCATION, parent, list, useList);
                        break;
                    case POINT_LOCATION:
                        if (list == null || list.isEmpty()) {
                            list = pointLocationList != null ? new ArrayList(pointLocationList) : new ArrayList();
                        }
                        treeContentMenuBuilderHelper(type, data, parent, list, true);
                        break;
                    case POINT_OVER_100:
                        if (list == null || list.isEmpty()) {
                            list = pointList != null ? new ArrayList(pointList) : new ArrayList();
                        } else {
                            useList = true;
                        }
                        treeContentMenuBuilderHelper(type, data, POINT, parent, list, useList);
                        break;
                    case POINT:
                        if (list == null || list.isEmpty()) {
                            list = pointList != null ? new ArrayList(pointList) : new ArrayList();
                        }
                        treeContentMenuBuilderHelper(type, data, parent, list, false);
                        break;
                    case EMPTY:
                        if (parent != null) {
                            newNode = new DefaultTreeNode(type, data, parent);
                            newNode.setSelectable(false);
                        }
                        break;
                }
            } else {
                navigationBean.setTreeNodeRoot(new DefaultTreeNode(ROOT, new SelectItem(null, userManageBean.getResourceBundleString("breadcrumb_4")), null));
                updateUI("tree");
            }
        }
    }

    /**
     * Used for the NodeExpandEvent caused by the Nav Tree component
     *
     * @param event, NodeExpandEvent Object
     */
    @Override
    public void onNodeExpand(NodeExpandEvent event) {
        TreeNode parent;
        List<Object> list = new ArrayList();
        UUID siteId = null;
        UUID areaId = null;
        UUID assetId = null;
        UUID pointLocationId;

        try {
            if (event != null && userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null) {
                switch (event.getTreeNode().getType()) {
                    case REGION:
                        event.getTreeNode().getChildren().clear();
                        treeContentMenuBuilder(COUNTRY, null, event.getTreeNode(), null);
                        break;
                    case COUNTRY:
                        if (regionCountrySiteMap.get((String) event.getTreeNode().getParent().getData()).get((String) event.getTreeNode().getData()).size() > 100) {
                            event.getTreeNode().getChildren().clear();
                            treeContentMenuBuilder(SITE_OVER_100, null, event.getTreeNode(), null);
                        } else if (!regionCountrySiteMap.get((String) event.getTreeNode().getParent().getData()).get((String) event.getTreeNode().getData()).isEmpty()) {
                            event.getTreeNode().getChildren().clear();
                            treeContentMenuBuilder(SITE, null, event.getTreeNode(), null);
                        }
                        break;
                    case SITE:
                        siteId = (UUID) ((SelectItem) event.getTreeNode().getData()).getValue();
                        AreaClient.getInstance().getAreaSiteByCustomerSite(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), siteId).stream().sorted(Comparator.comparing(AreaVO::getAreaName)).forEachOrdered(vo -> list.add(new SelectItem(vo.getAreaId(), vo.getAreaName())));
                        if (list.size() > 100) {
                            event.getTreeNode().getChildren().clear();
                            treeContentMenuBuilder(AREA_OVER_100, null, event.getTreeNode(), list);
                        } else if (!list.isEmpty()) {
                            event.getTreeNode().getChildren().clear();
                            treeContentMenuBuilder(AREA, null, event.getTreeNode(), list);
                        }
                        break;
                    case AREA:
                        siteId = event.getTreeNode().getParent().getType().equals(SITE) ? (UUID) ((SelectItem) event.getTreeNode().getParent().getData()).getValue() : (UUID) ((SelectItem) event.getTreeNode().getParent().getParent().getData()).getValue();
                        areaId = (UUID) ((SelectItem) event.getTreeNode().getData()).getValue();
                        AssetClient.getInstance().getAssetSiteAreaByCustomerSiteArea(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), siteId, areaId).stream().sorted(Comparator.comparing(AssetVO::getAssetName)).forEachOrdered(vo -> list.add(new SelectItem(vo.getAssetId(), vo.getAssetName())));
                        if (list.size() > 100) {
                            event.getTreeNode().getChildren().clear();
                            treeContentMenuBuilder(ASSET_OVER_100, null, event.getTreeNode(), list);
                        } else if (!list.isEmpty()) {
                            event.getTreeNode().getChildren().clear();
                            treeContentMenuBuilder(ASSET, null, event.getTreeNode(), list);
                        }
                        break;
                    case ASSET:
                        parent = event.getTreeNode().getParent();
                        do {
                            if (parent.getType().equals(SITE)) {
                                siteId = (UUID) ((SelectItem) parent.getData()).getValue();
                            } else if (parent.getType().equals(AREA)) {
                                areaId = (UUID) ((SelectItem) parent.getData()).getValue();
                            }
                            parent = parent.getParent();
                        } while (siteId == null || areaId == null);
                        assetId = (UUID) ((SelectItem) event.getTreeNode().getData()).getValue();
                        PointLocationClient.getInstance().getPointLocationsByCustomerSiteAreaAsset(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), siteId, areaId, assetId).stream().sorted(Comparator.comparing(PointLocationVO::getPointLocationName)).forEachOrdered(vo -> list.add(new SelectItem(vo.getPointLocationId(), vo.getPointLocationName())));
                        if (list.size() > 100) {
                            event.getTreeNode().getChildren().clear();
                            treeContentMenuBuilder(POINT_LOCATION_OVER_100, null, event.getTreeNode(), list);
                        } else if (!list.isEmpty()) {
                            event.getTreeNode().getChildren().clear();
                            treeContentMenuBuilder(POINT_LOCATION, null, event.getTreeNode(), list);
                        }
                        break;
                    case POINT_LOCATION:
                        parent = event.getTreeNode().getParent();
                        do {
                            switch (parent.getType()) {
                                case SITE:
                                    siteId = (UUID) ((SelectItem) parent.getData()).getValue();
                                    break;
                                case AREA:
                                    areaId = (UUID) ((SelectItem) parent.getData()).getValue();
                                    break;
                                case ASSET:
                                    assetId = (UUID) ((SelectItem) parent.getData()).getValue();
                                    break;
                                default:
                                    break;
                            }
                            parent = parent.getParent();
                        } while (siteId == null || areaId == null || assetId == null);
                        pointLocationId = (UUID) ((SelectItem) event.getTreeNode().getData()).getValue();
                        PointClient.getInstance().getPointsByCustomerSiteAreaAssetPointLocation(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), siteId, areaId, assetId, pointLocationId, true).stream().sorted(Comparator.comparing(PointVO::getPointName)).forEachOrdered(vo -> list.add(new SelectItem(vo.getPointId(), vo.getPointName())));
                        if (list.size() > 100) {
                            event.getTreeNode().getChildren().clear();
                            treeContentMenuBuilder(POINT_OVER_100, null, event.getTreeNode(), list);
                        } else if (!list.isEmpty()) {
                            event.getTreeNode().getChildren().clear();
                            treeContentMenuBuilder(POINT, null, event.getTreeNode(), list);
                        }
                        break;
                }
            }
        } catch (Exception e) {
            Logger.getLogger(ConfigurationBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Used for the NodeCollapseEvent caused by the Nav Tree component
     *
     * @param event, NodeCollapseEvent Object
     */
    @Override
    public void onNodeCollapse(NodeCollapseEvent event) {
        try {
            if (event != null && (event.getTreeNode().getType().equals(REGION) || event.getTreeNode().getType().equals(COUNTRY) || event.getTreeNode().getType().equals(SITE) || event.getTreeNode().getType().equals(AREA) || event.getTreeNode().getType().equals(ASSET) || event.getTreeNode().getType().equals(POINT_LOCATION))) {
                event.getTreeNode().getChildren().clear();
                treeContentMenuBuilder(EMPTY, null, event.getTreeNode(), null);
            }
        } catch (Exception e) {
            Logger.getLogger(ConfigurationBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Used for the NodeSelectEvent caused by the Nav Tree component
     *
     * @param event, NodeSelectEvent Object
     */
    @Override
    public void onNodeSelect(NodeSelectEvent event) {
        TreeNode parent;
        final List<AssetVO> list;
        List<PointVO> pointVOs;
        Set<String> ffsetnames;
        List<TachometerVO> tachList;

        try {
            if (event != null) {
                parent = event.getTreeNode().getParent();
                do {
                    if (parent != null) {
                        switch (parent.getType()) {
                            case REGION:
                                selectedFilters.setRegion((String) parent.getData());
                                parent = null;
                                break;
                            case COUNTRY:
                                selectedFilters.setCountry((String) parent.getData());
                                break;
                            case SITE_OVER_100:
                                selectedFilters.setSiteOver100((String) parent.getData());
                                break;
                            case SITE:
                                selectedFilters.setSite((SelectItem) parent.getData());
                                break;
                            case AREA_OVER_100:
                                selectedFilters.setAreaOver100((String) parent.getData());
                                resetDataTable();
                                break;
                            case AREA:
                                selectedFilters.setArea((SelectItem) parent.getData());
                                resetDataTable();
                                break;
                            case ASSET_OVER_100:
                                selectedFilters.setAssetOver100((String) parent.getData());
                                break;
                            case ASSET:
                                selectedFilters.setAsset((SelectItem) parent.getData());
                                break;
                            case POINT_LOCATION_OVER_100:
                                selectedFilters.setPointLocationOver100((String) parent.getData());
                                break;
                            case POINT_LOCATION:
                                selectedFilters.setPointLocation((SelectItem) parent.getData());
                                break;
                            case POINT_OVER_100:
                                selectedFilters.setPointOver100((String) parent.getData());
                                break;
                        }
                        if (parent != null) {
                            parent = parent.getParent();
                        }
                    }
                } while (parent != null);

                switch (event.getTreeNode().getType()) {
                    case REGION:
                        selectedFilters.setRegion((String) event.getTreeNode().getData());
                        selectedFilters.clear(REGION);
                        areaList.clear();
                        assetList.clear();
                        pointLocationList.clear();
                        pointList.clear();
                        siteVOList.clear();
                        areaVOList.clear();
                        assetVOList.clear();
                        pointLocationVOList.clear();
                        pointVOList.clear();
                        selectedAsset = null;
                        selectedPointLocation = null;
                        selectedPointVO = null;
                        tachometersList.clear();
                        break;
                    case COUNTRY:
                        selectedFilters.setCountry((String) event.getTreeNode().getData());
                        selectedFilters.clear(COUNTRY);
                        areaList.clear();
                        assetList.clear();
                        pointLocationList.clear();
                        pointList.clear();
                        areaVOList.clear();
                        assetVOList.clear();
                        pointLocationVOList.clear();
                        pointVOList.clear();
                        selectedAsset = null;
                        selectedPointLocation = null;
                        selectedPointVO = null;
                        tachometersList.clear();
                        try {
                            siteVOList.clear();
                            siteVOList.add(SiteClient.getInstance().getSiteRegionCountryByCustomerRegionCountry(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), selectedFilters.getRegion(), selectedFilters.getCountry()).stream().filter(vo -> vo.getSiteId().equals(userManageBean.getCurrentSite().getSiteId())).findFirst().get());
                        } catch (Exception ex) {
                            siteVOList.clear();
                        }
                        break;
                    case SITE:
                        selectedFilters.setSite((SelectItem) event.getTreeNode().getData());
                        selectedFilters.clear(SITE);
                        assetList.clear();
                        pointLocationList.clear();
                        pointList.clear();
                        siteVOList.clear();
                        assetVOList.clear();
                        pointLocationVOList.clear();
                        pointVOList.clear();
                        selectedAsset = null;
                        selectedPointLocation = null;
                        selectedPointVO = null;
                        tachometersList.clear();
                        faultFrequenciesList.clear();
                        try {
                            areaVOList.clear();
                            areaList.clear();
                            siteFaultFrequenciesVOs.clear();
                            siteTachometersList.clear();
                            userManageBean.getPresetUtil().clearSiteTachList();
                            userManageBean.getPresetUtil().clearSiteFaultFrequenciesList();
                            areaVOList.addAll(AreaClient.getInstance().getAreaSiteByCustomerSite(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue()));
                            areaVOList.stream().sorted(Comparator.comparing(AreaVO::getAreaName)).forEachOrdered(area -> areaList.add(new SelectItem(area.getAreaId(), area.getAreaName())));
                            sortAreaVOList();
                            siteTachometersList.addAll(userManageBean.getPresetUtil().getSiteTachList(userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue()));
                        } catch (Exception ex) {
                            areaVOList.clear();
                            areaList.clear();
                            faultFrequenciesList.clear();
                            siteFaultFrequenciesVOs.clear();
                            siteTachometersList.clear();
                            userManageBean.getPresetUtil().clearSiteTachList();
                            userManageBean.getPresetUtil().clearSiteFaultFrequenciesList();
                        }
                        break;
                    case AREA:
                        selectedFilters.setArea((SelectItem) event.getTreeNode().getData());
                        selectedFilters.clear(AREA);
                        pointLocationList.clear();
                        pointList.clear();
                        siteVOList.clear();
                        areaVOList.clear();
                        pointLocationVOList.clear();
                        pointVOList.clear();
                        selectedAsset = null;
                        selectedPointLocation = null;
                        selectedPointVO = null;
                        tachometersList.clear();
                        faultFrequenciesList.clear();
                        try {
                            areaList.clear();
                            siteFaultFrequenciesVOs.clear();
                            siteTachometersList.clear();
                            userManageBean.getPresetUtil().clearSiteTachList();
                            userManageBean.getPresetUtil().clearSiteFaultFrequenciesList();
                            AreaClient.getInstance().getAreaSiteByCustomerSite(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue()).stream().sorted(Comparator.comparing(AreaVO::getAreaName)).forEachOrdered(area -> areaList.add(new SelectItem(area.getAreaId(), area.getAreaName())));
                            siteTachometersList.addAll(userManageBean.getPresetUtil().getSiteTachList(userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue()));
                        } catch (Exception ex) {
                            areaList.clear();
                            faultFrequenciesList.clear();
                            siteFaultFrequenciesVOs.clear();
                            siteTachometersList.clear();
                            userManageBean.getPresetUtil().clearSiteTachList();
                            userManageBean.getPresetUtil().clearSiteFaultFrequenciesList();
                        }
                        try {
                            assetVOList.clear();
                            assetList.clear();
                            assetVOList.addAll(AssetClient.getInstance().getAssetSiteAreaByCustomerSiteArea(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue()));
                            assetVOList.stream().sorted(Comparator.comparing(AssetVO::getAssetName)).forEachOrdered(asset -> assetList.add(new SelectItem(asset.getAssetId(), asset.getAssetName())));
                            sortAssetVOList();
                        } catch (Exception ex) {
                            assetVOList.clear();
                            assetList.clear();
                        }
                        break;
                    case ASSET:
                        selectedFilters.setAsset((SelectItem) event.getTreeNode().getData());
                        selectedFilters.clear(ASSET);
                        pointList.clear();
                        siteVOList.clear();
                        areaVOList.clear();
                        assetVOList.clear();
                        pointVOList.clear();
                        selectedPointLocation = null;
                        selectedPointVO = null;
                        tachometersList.clear();
                        faultFrequenciesList.clear();
                        try {
                            areaList.clear();
                            siteFaultFrequenciesVOs.clear();
                            siteTachometersList.clear();
                            userManageBean.getPresetUtil().clearSiteTachList();
                            userManageBean.getPresetUtil().clearSiteFaultFrequenciesList();
                            AreaClient.getInstance().getAreaSiteByCustomerSite(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue()).stream().sorted(Comparator.comparing(AreaVO::getAreaName)).forEachOrdered(area -> areaList.add(new SelectItem(area.getAreaId(), area.getAreaName())));
                            siteTachometersList.addAll(userManageBean.getPresetUtil().getSiteTachList(userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue()));
                        } catch (Exception ex) {
                            areaList.clear();
                            faultFrequenciesList.clear();
                            siteFaultFrequenciesVOs.clear();
                            siteTachometersList.clear();
                            userManageBean.getPresetUtil().clearSiteTachList();
                            userManageBean.getPresetUtil().clearSiteFaultFrequenciesList();
                        }
                        try {
                            assetList.clear();
                            list = AssetClient.getInstance().getAssetSiteAreaByCustomerSiteArea(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue());
                            selectedAsset = list.stream().filter(asset -> asset.getAssetId().equals((UUID) selectedFilters.getAsset().getValue())).findFirst().get();
                            list.stream().sorted(Comparator.comparing(AssetVO::getAssetName)).forEachOrdered(asset -> assetList.add(new SelectItem(asset.getAssetId(), asset.getAssetName())));
                        } catch (Exception ex) {
                            selectedAsset = null;
                            assetList.clear();
                        }
                        try {
                            pointLocationVOList.clear();
                            pointLocationList.clear();
                            pointLocationVOList.addAll(PointLocationClient.getInstance().getPointLocationsByCustomerSiteAreaAsset(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue(), (UUID) selectedFilters.getAsset().getValue()));
                            pointLocationVOList.stream().sorted(Comparator.comparing(PointLocationVO::getPointLocationName)).forEachOrdered(pointLocation -> pointLocationList.add(new SelectItem(pointLocation.getPointLocationId(), pointLocation.getPointLocationName())));
                            pointLocationVOList.stream().forEach(pl -> {
                                List<TachometerVO> tachometerVOlist;

                                if (pl.getTachId() != null) {
                                    if ((tachometerVOlist = TachometerClient.getInstance().getGlobalTachometersByPK(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), pl.getCustomerAccount(), pl.getTachId())) == null || tachometerVOlist.isEmpty()) {
                                        tachometerVOlist = TachometerClient.getInstance().getSiteTachometersByPK(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), pl.getCustomerAccount(), pl.getSiteId(), pl.getTachId());
                                    }
                                    if (tachometerVOlist == null || tachometerVOlist.isEmpty()) {
                                        tachometerVOlist = TachometerClient.getInstance().getGlobalTachometersByPK(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), ApplicationConstants.DEFAULT_UPTIME_ACCOUNT, pl.getTachId());
                                    }
                                    pl.setTachReferenceUnits((tachometerVOlist != null && !tachometerVOlist.isEmpty() && tachometerVOlist.get(0).getTachReferenceUnits() != null) ? tachometerVOlist.get(0).getTachReferenceUnits() : null);
                                }
                            });
                            sortPointLocationVOList();
                        } catch (Exception ex) {
                            selectedAsset = null;
                            pointLocationVOList.clear();
                            pointLocationList.clear();
                        }
                        break;
                    case POINT_LOCATION:
                        selectedFilters.setPointLocation((SelectItem) event.getTreeNode().getData());
                        selectedFilters.clear(POINT_LOCATION);
                        siteVOList.clear();
                        areaVOList.clear();
                        assetVOList.clear();
                        pointLocationVOList.clear();
                        selectedAsset = null;
                        selectedPointVO = null;
                        try {
                            areaList.clear();
                            siteFaultFrequenciesVOs.clear();
                            siteTachometersList.clear();
                            faultFrequenciesList.clear();
                            userManageBean.getPresetUtil().clearSiteTachList();
                            userManageBean.getPresetUtil().clearSiteFaultFrequenciesList();
                            AreaClient.getInstance().getAreaSiteByCustomerSite(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue()).stream().sorted(Comparator.comparing(AreaVO::getAreaName)).forEachOrdered(area -> areaList.add(new SelectItem(area.getAreaId(), area.getAreaName())));
                            siteTachometersList.addAll(userManageBean.getPresetUtil().getSiteTachList(userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue()));
                        } catch (Exception ex) {
                            areaList.clear();
                            faultFrequenciesList.clear();
                            siteFaultFrequenciesVOs.clear();
                            siteTachometersList.clear();
                            userManageBean.getPresetUtil().clearSiteTachList();
                            userManageBean.getPresetUtil().clearSiteFaultFrequenciesList();
                        }
                        try {
                            assetList.clear();
                            AssetClient.getInstance().getAssetSiteAreaByCustomerSiteArea(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue()).stream().sorted(Comparator.comparing(AssetVO::getAssetName)).forEachOrdered(asset -> assetList.add(new SelectItem(asset.getAssetId(), asset.getAssetName())));
                        } catch (Exception ex) {
                            assetList.clear();
                        }
                        try {
                            pointLocationList.clear();
                            PointLocationClient.getInstance().getPointLocationsByCustomerSiteAreaAsset(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue(), (UUID) selectedFilters.getAsset().getValue()).stream().sorted(Comparator.comparing(PointLocationVO::getPointLocationName)).forEachOrdered(pointLocation -> pointLocationList.add(new SelectItem(pointLocation.getPointLocationId(), pointLocation.getPointLocationName())));
                        } catch (Exception ex) {
                            pointLocationList.clear();
                        }
                        try {
                            pointVOList.clear();
                            pointList.clear();
                            if (tachometersList != null) {
                                tachometersList.clear();
                            }
                            if (faultFrequenciesList != null) {
                                faultFrequenciesList.clear();
                            }
                            selectedPointLocation = PointLocationClient.getInstance().getPointLocationsByPK(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue(), (UUID) selectedFilters.getAsset().getValue(), (UUID) selectedFilters.getPointLocation().getValue()).get(0);
                            if (selectedPointLocation != null) {

                                // get tachometer presets
                                userManageBean.getPresetUtil().clearSiteTachList();
                                userManageBean.getPresetUtil().clearGlobalTachList();
                                tachometersList.addAll(userManageBean.getPresetUtil().populateTachometerList(userManageBean, userManageBean.getCurrentSite().getCustomerAccount(), selectedPointLocation.getSiteId()));
                                if ((tachId = selectedPointLocation.getTachId()) != null) {
                                    if ((tachList = TachometerClient.getInstance().getGlobalTachometersByPK(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), DEFAULT_UPTIME_ACCOUNT, selectedPointLocation.getTachId())) == null || tachList.isEmpty()) {
                                        if ((tachList = TachometerClient.getInstance().getGlobalTachometersByPK(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), selectedPointLocation.getCustomerAccount(), selectedPointLocation.getTachId())) == null || tachList.isEmpty()) {
                                            tachList = TachometerClient.getInstance().getSiteTachometersByPK(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), selectedPointLocation.getCustomerAccount(), selectedPointLocation.getSiteId(), selectedPointLocation.getTachId());
                                        }
                                    }

                                    if (tachList != null && !tachList.isEmpty()) {
                                        selectedPointLocation.setTachReferenceUnits(tachList.get(0).getTachReferenceUnits());
                                        selectedPointLocation.setTachName(tachList.get(0).getTachName());
                                    }
                                }

                                // get fault frequencies presets
                                userManageBean.getPresetUtil().clearSiteFaultFrequenciesList();
                                userManageBean.getPresetUtil().clearGlobalFaultFrequenciesList();
                                faultFrequenciesList = populateFFSetList();
                                ffsetnames = new HashSet();
                                if (selectedPointLocation.getFfSetIds() != null) {
                                    ffsetnames = PointLocationDomJsonParser.getInstance().populateFFSetFromJson(selectedPointLocation.getFfSetIds(), siteFFSetFavorites);
                                }
                                selectedPointLocation.setFfSetName(ffsetnames);

                                selectedFilters.setPointLocAlarmEnabled(selectedPointLocation.isAlarmEnabled());
                            }
                            pointVOList.addAll(PointClient.getInstance().getPointsByCustomerSiteAreaAssetPointLocation(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue(), (UUID) selectedFilters.getAsset().getValue(), (UUID) selectedFilters.getPointLocation().getValue(), true));
                            pointVOList.stream().forEach(a -> {
                                if (a.isAlarmEnabled()) {
                                    a.setAlarmEnabledValue(userManageBean.getResourceBundleString("label_true"));
                                } else {
                                    a.setAlarmEnabledValue(userManageBean.getResourceBundleString("label_false"));
                                }
                            });
                            setupApAlSetVOs(pointVOList);
                            pointVOList.stream().sorted(Comparator.comparing(PointVO::getPointName)).forEachOrdered(point -> pointList.add(new SelectItem(point.getPointId(), point.getPointName())));
                            CommonUtilSingleton.getInstance().sortPointsByPointType(pointVOList);
                        } catch (Exception ex) {
                            selectedPointLocation = null;
                            tachometersList.clear();
                            pointVOList.clear();
                            pointList.clear();
                        }
                        break;
                    case POINT:
                        selectedFilters.setPoint((SelectItem) event.getTreeNode().getData());
                        siteVOList.clear();
                        areaVOList.clear();
                        assetVOList.clear();
                        pointLocationVOList.clear();
                        selectedAsset = null;
                        selectedPointLocation = null;
                        tachometersList.clear();
                        faultFrequenciesList.clear();
                        try {
                            areaList.clear();
                            siteFaultFrequenciesVOs.clear();
                            siteTachometersList.clear();
                            userManageBean.getPresetUtil().clearSiteTachList();
                            userManageBean.getPresetUtil().clearSiteFaultFrequenciesList();
                            AreaClient.getInstance().getAreaSiteByCustomerSite(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue()).stream().sorted(Comparator.comparing(AreaVO::getAreaName)).forEachOrdered(area -> areaList.add(new SelectItem(area.getAreaId(), area.getAreaName())));
                            siteTachometersList.addAll(userManageBean.getPresetUtil().getSiteTachList(userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue()));

                        } catch (Exception ex) {
                            areaList.clear();
                            faultFrequenciesList.clear();
                            siteFaultFrequenciesVOs.clear();
                            siteTachometersList.clear();
                            userManageBean.getPresetUtil().clearSiteTachList();
                            userManageBean.getPresetUtil().clearSiteFaultFrequenciesList();
                        }
                        try {
                            assetList.clear();
                            AssetClient.getInstance().getAssetSiteAreaByCustomerSiteArea(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue()).stream().sorted(Comparator.comparing(AssetVO::getAssetName)).forEachOrdered(asset -> assetList.add(new SelectItem(asset.getAssetId(), asset.getAssetName())));
                        } catch (Exception ex) {
                            assetList.clear();
                        }
                        try {
                            pointLocationList.clear();
                            PointLocationClient.getInstance().getPointLocationsByCustomerSiteAreaAsset(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue(), (UUID) selectedFilters.getAsset().getValue()).stream().sorted(Comparator.comparing(PointLocationVO::getPointLocationName)).forEachOrdered(pointLocation -> pointLocationList.add(new SelectItem(pointLocation.getPointLocationId(), pointLocation.getPointLocationName())));
                        } catch (Exception ex) {
                            pointLocationList.clear();
                        }
                        try {
                            pointList.clear();
                            PointClient.getInstance().getPointsByCustomerSiteAreaAssetPointLocation(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue(), (UUID) selectedFilters.getAsset().getValue(), (UUID) selectedFilters.getPointLocation().getValue(), true).stream().sorted(Comparator.comparing(PointVO::getPointName)).forEachOrdered(point -> pointList.add(new SelectItem(point.getPointId(), point.getPointName())));
                        } catch (Exception ex) {
                            pointList.clear();
                        }
                        try {
                            pointVOList.clear();
                            pointVOs = PointClient.getInstance().getPointsByCustomerSiteAreaAssetPointLocationPoint(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue(), (UUID) selectedFilters.getAsset().getValue(), (UUID) selectedFilters.getPointLocation().getValue(), (UUID) selectedFilters.getPoint().getValue(), true);
                            selectedPointVO = pointVOs.get(0);
                            setupApAlSetVOs(pointVOs);
                            editPoint(selectedPointVO);
                        } catch (Exception ex) {
                            pointVOList.clear();
                        }
                        break;
                }
                updateBreadcrumbModel();
                updateUI("content");
            }
        } catch (Exception e) {
            Logger.getLogger(ConfigurationBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    public void editPoint(PointVO item) {
        if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("editPointBean") == null) {
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("editPointBean", new EditPointBean());
            ((EditPointBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("editPointBean")).init();
        }
        ((EditPointBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("editPointBean")).presetFields(selectedFilters, item);

    }

    public void sortPointLocationVOList() {
        pointLocationVOList = pointLocationVOList.stream().sorted(Comparator.comparing(PointLocationVO::getPointLocationName)).collect(Collectors.toList());
    }

    public void sortAssetVOList() {
        assetVOList = assetVOList.stream().sorted(Comparator.comparing(AssetVO::getAssetName)).collect(Collectors.toList());
    }

    public void sortAreaVOList() {
        areaVOList.stream().forEach(a -> {
            String[] envArray;

            if (a.getEnvironment() != null && !a.getEnvironment().trim().isEmpty()) {
                envArray = a.getEnvironment().split(",");
                if (Boolean.parseBoolean(envArray[0])) {
                    a.setEnvironment(Boolean.parseBoolean(envArray[1]) ? (userManageBean.getResourceBundleString("label_indoor") + "/" + userManageBean.getResourceBundleString("label_outdoor")) : userManageBean.getResourceBundleString("label_indoor"));
                } else if (Boolean.parseBoolean(envArray[1])) {
                    a.setEnvironment(userManageBean.getResourceBundleString("label_outdoor"));
                } else {
                    a.setEnvironment("");
                }
            }
            a.setClimateControlledValue(userManageBean.getResourceBundleString(a.isClimateControlled() == true ? "label_true" : "label_false"));
        });
        areaVOList = areaVOList.stream().sorted(Comparator.comparing(AreaVO::getAreaName)).collect(Collectors.toList());
    }

    /**
     * Sets the given field regionCountrySiteMap
     */
    @Override
    public void setRegionCountrySiteMap() {
        try {
            regionCountrySiteMap.clear();

            if (userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null) {
                SiteClient.getInstance().getSiteRegionCountryByCustomer(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount()).forEach(siteVO -> {
                    if (userManageBean.getCurrentSite().getSiteId().equals(siteVO.getSiteId())) {
                        if (regionCountrySiteMap.containsKey(siteVO.getRegion())) {
                            if (regionCountrySiteMap.get(siteVO.getRegion()).containsKey(siteVO.getCountry())) {
                                if (!regionCountrySiteMap.get(siteVO.getRegion()).get(siteVO.getCountry()).stream().anyMatch(site -> site.getValue().equals(siteVO.getSiteId()))) {
                                    regionCountrySiteMap.get(siteVO.getRegion()).get(siteVO.getCountry()).add(new SelectItem(siteVO.getSiteId(), siteVO.getSiteName()));
                                }
                            } else {
                                regionCountrySiteMap.get(siteVO.getRegion()).put(siteVO.getCountry(), new ArrayList());
                                regionCountrySiteMap.get(siteVO.getRegion()).get(siteVO.getCountry()).add(new SelectItem(siteVO.getSiteId(), siteVO.getSiteName()));
                            }
                        } else {
                            regionCountrySiteMap.put(siteVO.getRegion(), new HashMap());
                            regionCountrySiteMap.get(siteVO.getRegion()).put(siteVO.getCountry(), new ArrayList());
                            regionCountrySiteMap.get(siteVO.getRegion()).get(siteVO.getCountry()).add(new SelectItem(siteVO.getSiteId(), siteVO.getSiteName()));
                        }
                    }
                });
            }
        } catch (Exception ex) {
            regionCountrySiteMap.clear();
        }
    }

    /**
     * Launches the import configuration overlay
     */
    public void launchImportConfigOverlay() {
        try {
            navigationBean.updateDialog("import-config");
        } catch (Exception e) {
            Logger.getLogger(ConfigurationBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    private void resetDataTable() {
        DataTable datatable;

        try {
            if ((datatable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("configurationResultFormId:assetDataTableId")) != null) {
                datatable.reset();
            }
        } catch (Exception e) {
            Logger.getLogger(UserManageBean.class.getName()).log(Level.SEVERE, "***Error occured*** {0} ", e.getMessage());
        }
    }

    public List<SiteVO> getSiteVOList() {
        return siteVOList;
    }

    public List<AreaVO> getAreaVOList() {
        return areaVOList;
    }

    public List<AssetVO> getAssetVOList() {
        return assetVOList;
    }

    public List<PointLocationVO> getPointLocationVOList() {
        return pointLocationVOList;
    }

    public List<PointVO> getPointVOList() {
        return pointVOList;
    }

    public boolean isRenderCreateBtn() {
        return renderCreateBtn;
    }

    public boolean isRenderCreateBtn2() {
        return renderCreateBtn2;
    }

    public String getCreateBtnValue() {
        return createBtnValue;
    }

    public String getCreateBtn2Value() {
        return createBtn2Value;
    }

    public String getCreateBtn3Value() {
        return createBtn3Value;
    }

    public AssetVO getSelectedAsset() {
        return selectedAsset;
    }

    public PointLocationVO getSelectedPointLocation() {
        return selectedPointLocation;
    }

    public PointVO getSelectedPointVO() {
        return selectedPointVO;
    }

    public List<TachometerVO> getGlobalTachometersList() {
        return globalTachometersList;
    }

    public List<TachometerVO> getSiteTachometersList() {
        return siteTachometersList;
    }

    public List<SelectItem> getTachometersList() {
        return tachometersList;
    }

    public List<SelectItem> getFaultFrequenciesList() {
        return faultFrequenciesList;
    }

    public List<SelectItem> getRollDiameterUnitsList() {
        return rollDiameterUnitsList;
    }

    public String getCreateBtn4Value() {
        return createBtn4Value;
    }

    public void setCreateBtn4Value(String createBtn4Value) {
        this.createBtn4Value = createBtn4Value;
    }

    public boolean isRenderCreateBtn3() {
        return renderCreateBtn3;
    }

    public void setRenderCreateBtn3(boolean renderCreateBtn3) {
        this.renderCreateBtn3 = renderCreateBtn3;
    }

    public List<String> getAssetTypeList() {
        return assetTypeList;
    }

    public void setAssetTypeList(List<String> assetTypeList) {
        this.assetTypeList = assetTypeList;
    }

    public List<FaultFrequenciesVO> getSiteFFSetFavorites() {
        return siteFFSetFavorites;
    }

    public UUID getTachId() {
        return tachId;
    }

    public void setTachId(UUID tachId) {
        this.tachId = tachId;
    }

    public boolean isTachValidationFailed() {
        return tachValidationFailed;
    }

    public void setTachValidationFailed(boolean tachValidationFailed) {
        this.tachValidationFailed = tachValidationFailed;
    }

    public List<ApAlSetVO> getSiteApAlSetsVOList() {
        return siteApAlSetsVOList;
    }

    public void setSiteApAlSetsVOList(List<ApAlSetVO> siteApAlSetsVOList) {
        this.siteApAlSetsVOList = siteApAlSetsVOList;
    }

    public List<SelectItem> getSensorTypeList() {
        return sensorTypeList;
    }

    public Set<String> getSensorTypes() {
        return sensorTypes;
    }

    public void setSensorTypes(Set<String> sensorTypes) {
        this.sensorTypes = sensorTypes;
    }

    public boolean isAlarmEnabled() {
        return alarmEnabled;
    }

    public void setAlarmEnabled(boolean alarmEnabled) {
        this.alarmEnabled = alarmEnabled;
    }

    public String getSensorTypeDefaultLabel() {
        return sensorTypeDefaultLabel;
    }

    public void setSensorTypeDefaultLabel(String sensorTypeDefaultLabel) {
        this.sensorTypeDefaultLabel = sensorTypeDefaultLabel;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<String> getSensorTypeSet() {
        return sensorTypeSet;
    }

    public void setSensorTypeSet(Set<String> sensorTypeSet) {
        this.sensorTypeSet = sensorTypeSet;
    }

}
