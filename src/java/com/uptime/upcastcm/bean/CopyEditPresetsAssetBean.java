/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.AssetClient;
import com.uptime.upcastcm.http.client.AssetPresetClient;
import com.uptime.upcastcm.http.client.DevicePresetClient;
import com.uptime.upcastcm.http.utils.json.dom.PointLocationDomJsonParser;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import static com.uptime.upcastcm.utils.enums.ParamUnitEnum.getFrequencyPlusUnits;
import com.uptime.upcastcm.utils.helperclass.Filters;
import static com.uptime.upcastcm.utils.helperclass.GenericMessage.setNonAutoGrowlMsg;
import com.uptime.upcastcm.utils.helperclass.Utils;
import com.uptime.upcastcm.utils.interfaces.PresetDialogFields;
import com.uptime.upcastcm.utils.singletons.CommonUtilSingleton;
import com.uptime.upcastcm.utils.vo.AssetPresetPointLocationVO;
import com.uptime.upcastcm.utils.vo.AssetPresetVO;
import com.uptime.upcastcm.utils.vo.AssetTypesVO;
import com.uptime.upcastcm.utils.vo.DevicePresetVO;
import com.uptime.upcastcm.utils.vo.DeviceVO;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.faces.model.SelectItemGroup;
import org.primefaces.PrimeFaces;

/**
 *
 * @author Joseph
 */
public class CopyEditPresetsAssetBean implements Serializable, PresetDialogFields {

    private UserManageBean userManageBean;
    private String siteOrGlobal;
    private NavigationBean navigationBean;
    private AssetPresetVO assetPresetVO;
    private List<String> assetTypeList, ffsetnameList;
    private int operationType;
    private List<DevicePresetVO> devicePresetList, distinctDevicePresetList;
    //List<AssetPresetPointLocationVO> delPointLocationNamesVOs;
    private List<SelectItem> tachometersList;

    /**
     * Constructor
     */
    public CopyEditPresetsAssetBean() {

        try {
            assetTypeList = new ArrayList();
            distinctDevicePresetList = new ArrayList();
            devicePresetList = new ArrayList();
            // delPointLocationNamesVOs = new ArrayList();
            tachometersList = new ArrayList();
            navigationBean = (NavigationBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("navigationBean");

            userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");

            //***************************For v1.0********************************
            List<AssetTypesVO> assetTypesVOList = new ArrayList();
            assetTypesVOList.addAll(AssetClient.getInstance().getGlobalAssetTypesByCustomer(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount()));
            if (!userManageBean.getCurrentSite().getCustomerAccount().equals(DEFAULT_UPTIME_ACCOUNT)) {
                assetTypesVOList.addAll(AssetClient.getInstance().getGlobalAssetTypesByCustomer(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), DEFAULT_UPTIME_ACCOUNT));
            }
            assetTypesVOList.stream().forEach(a -> assetTypeList.add(a.getAssetTypeName()));
        } catch (Exception e) {
            Logger.getLogger(CopyEditPresetsAssetBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            userManageBean = null;
        }
    }

    /**
     * Post Constructor
     */
    @PostConstruct
    public void init() {
        resetPage();
    }

    @Override
    public void resetPage() {

        operationType = -1;
        siteOrGlobal = null;
        assetPresetVO = new AssetPresetVO();
        assetPresetVO.setAssetPresetPointLocationVOList(new ArrayList());
        distinctDevicePresetList = new ArrayList();
        devicePresetList = new ArrayList();
        // delPointLocationNamesVOs.clear();
        tachometersList.clear();
    }

    @Override
    public void presetFields(Filters presets, Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void presetFields(int operationType, Object object) {
        if (userManageBean != null && userManageBean.getCurrentSite() != null && object != null && object instanceof AssetPresetVO) {
            resetPage();
            this.operationType = operationType;

            List<AssetPresetPointLocationVO> assetPresetPointLocationVOList = new ArrayList();
            assetPresetVO = new AssetPresetVO((AssetPresetVO) object);

            if (this.operationType == 2) {
                assetPresetVO.setAssetPresetName(null);
                siteOrGlobal = userManageBean.getCurrentSite().getSiteName();
            } else {
                siteOrGlobal = assetPresetVO.getType().equalsIgnoreCase("site") ? userManageBean.getCurrentSite().getSiteName() : assetPresetVO.getType();
            }
            List<AssetPresetVO> assetPresetVOs;
            if (assetPresetVO.getSiteId() != null) {
                assetPresetVOs = AssetPresetClient.getInstance().getSiteAssetPresetsByPK(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), assetPresetVO.getCustomerAccount(), assetPresetVO.getSiteId(), assetPresetVO.getAssetTypeName(), assetPresetVO.getAssetPresetId());
                tachometersList.addAll(userManageBean.getPresetUtil().populateTachometerList(userManageBean, userManageBean.getCurrentSite().getCustomerAccount(), userManageBean.getCurrentSite().getSiteId()));
            } else {
                assetPresetVOs = AssetPresetClient.getInstance().getGlobalAssetPresetsByPK(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), assetPresetVO.getCustomerAccount(), assetPresetVO.getAssetTypeName(), assetPresetVO.getAssetPresetId());
                tachometersList.addAll(userManageBean.getPresetUtil().populateTachometerList(userManageBean, userManageBean.getCurrentSite().getCustomerAccount(), null));
            }
            assetPresetVOs.stream().forEach(asset -> {
                AssetPresetPointLocationVO assetPresetPointLocationVO = new AssetPresetPointLocationVO();
                assetPresetPointLocationVO.setCustomerAccount(asset.getCustomerAccount());
                assetPresetPointLocationVO.setSiteId(asset.getSiteId());
                assetPresetPointLocationVO.setPointLocationName(asset.getPointLocationName());
                //assetPresetPointLocationVO.setTachName(asset.getTach);
                assetPresetPointLocationVO.setTachId(asset.getTachId());
                assetPresetPointLocationVO.setSpeedRatio(asset.getSpeedRatio());
                assetPresetPointLocationVO.setRollDiameter(asset.getRollDiameter());
                assetPresetPointLocationVO.setRollDiameterUnits(asset.getRollDiameterUnits());
                ffsetnameList = PointLocationDomJsonParser.getInstance().populateFFSetFromJson(asset.getFfSetIds());
                asset.setFfSetNames(String.join(", ", ffsetnameList));
                assetPresetPointLocationVO.setFfSetNames(asset.getFfSetNames());
                // assetPresetPointLocationVO.setFfSetName(asset.);
                assetPresetPointLocationVO.setDeviceType(asset.getDevicePresetType());
                assetPresetPointLocationVO.setDeviceId(asset.getDevicePresetId());
                // assetPresetPointLocationVO.setDeviceName(asset.getDeviceName());
                // assetPresetPointLocationVO.setTachReferenceUnits(asset.getTachReferenceUnits());
                //  assetPresetPointLocationVO.setDistinctDevicePresetList(asset.getDistinctDevicePresetList());
                assetPresetPointLocationVO.setFfSetIds(asset.getFfSetIds());
                onDeviceTypeChange(assetPresetPointLocationVO);
                onDeviceTemplateChange(assetPresetPointLocationVO);

                tachometersList.stream().forEachOrdered(item -> {

                    SelectItemGroup ig = (SelectItemGroup) item;
                    SelectItem[] itemArray = ig.getSelectItems();
                    for (int i = 0; i < itemArray.length; i++) {
                        SelectItem item1 = itemArray[i];
                        if (item1.getValue().toString().equalsIgnoreCase(assetPresetPointLocationVO.getTachId().toString())) {
                            assetPresetPointLocationVO.setTachName(item1.getLabel());
                        }
                    }

                });
                if (asset.getSpeedRatio() > 0f) {
                    assetPresetPointLocationVO.setRollDiameterText(null);
                    assetPresetPointLocationVO.setRollDiameterUnits(null);
                    assetPresetPointLocationVO.setSpeedRatioText(String.valueOf(asset.getSpeedRatio()));
                } else {
                    assetPresetPointLocationVO.setSpeedRatioText(null);
                    assetPresetPointLocationVO.setRollDiameterText(String.valueOf(asset.getRollDiameter()));
                }
                assetPresetPointLocationVOList.add(assetPresetPointLocationVO);

            });
            assetPresetVO.setAssetPresetPointLocationVOList(assetPresetPointLocationVOList);
//            setName = this.operationType != 2 ? devicePresetVO.getName() : null;
//            disable = devicePresetVO.isDisabled();
//            selectedDeviceType = devicePresetVO.getDeviceType();
            Logger.getLogger(CopyEditPresetsAssetBean.class.getName()).log(Level.INFO, "selectedDeviceType *********** ");

        } else {
            resetPage();
        }
    }

    @Override
    public void presetFields(Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void updateBean() {
        Logger.getLogger(CopyEditPresetsAssetBean.class.getName()).log(Level.INFO, "CopyEditPresetsAssetBean.updateBean() called. ***********");
    }

    public void updateSampleInteval() {
        if (assetPresetVO.getSampleInterval() <= 0 || assetPresetVO.getSampleInterval() > 1440) {
            assetPresetVO.setSampleInterval(720);
            FacesContext.getCurrentInstance().addMessage("growlMessages", new FacesMessage(userManageBean.getResourceBundleString("message_sample_interval_minimum")));
        }
    }

    public void onDeviceTypeChange(AssetPresetPointLocationVO assetPresetPointLocationVO) {
        List<DevicePresetVO> devicesPresetsLists;
        String selectedDeviceType = assetPresetPointLocationVO.getDeviceType();
        devicePresetList.clear();
        distinctDevicePresetList.clear();
        //deviceTypesList.clear();

        try {
            devicesPresetsLists = new ArrayList();
            if (!userManageBean.getCurrentSite().getCustomerAccount().equals(DEFAULT_UPTIME_ACCOUNT)) {
                devicesPresetsLists.addAll(DevicePresetClient.getInstance().getGlobalDevicePresetsByCustomerDeviceType(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), DEFAULT_UPTIME_ACCOUNT, selectedDeviceType));
            }
            devicesPresetsLists.addAll(DevicePresetClient.getInstance().getGlobalDevicePresetsByCustomerDeviceType(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), selectedDeviceType));
            devicePresetList.addAll(devicesPresetsLists.stream().filter(Utils.distinctByKeys(DevicePresetVO::getSensorType, DevicePresetVO::getName, DevicePresetVO::getDeviceType, DevicePresetVO::getPresetId)).collect(Collectors.toList()));

            devicePresetList.addAll(DevicePresetClient.getInstance().getSiteDeviceByCustomerSiteDeviceType(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), userManageBean.getCurrentSite().getSiteId().toString(), selectedDeviceType));

            //distinctDevicePresetList.addAll(devicePresetList.stream().filter(Utils.distinctByKeys(DevicePresetVO::getName, DevicePresetVO::getDeviceType, DevicePresetVO::getPresetId)).collect(Collectors.toList()));
            // deviceTypesList.addAll(userManageBean.getPresetUtil().populateDevicePresetList(userManageBean, devicePresetList));
        } catch (Exception e) {
            Logger.getLogger(PresetsBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            devicePresetList.clear();
        }
    }

    public void onDeviceTemplateChange(AssetPresetPointLocationVO assetPresetPointLocationVO) {
        List<DevicePresetVO> list;
        List<DeviceVO> deviceVOs;

        distinctDevicePresetList.clear();
        if (assetPresetPointLocationVO.getDeviceId() != null) {
            deviceVOs = CommonUtilSingleton.getInstance().populateDeviceTypeList(assetPresetPointLocationVO.getDeviceType());
            list = devicePresetList.stream().filter(device -> device.getDeviceType().equalsIgnoreCase(assetPresetPointLocationVO.getDeviceType()) && device.getPresetId().compareTo(assetPresetPointLocationVO.getDeviceId()) == 0).collect(Collectors.toList());
            assetPresetPointLocationVO.setDeviceType(assetPresetPointLocationVO.getDeviceType());
            assetPresetPointLocationVO.setDeviceName(list.get(0).getName());
            assetPresetPointLocationVO.setDeviceId(assetPresetPointLocationVO.getDeviceId());
            list.forEach(device -> {
                device.setPointName(assetPresetPointLocationVO.getPointLocationName() + " " + CommonUtilSingleton.getInstance().getPointNameBySensorType(device.getSensorType()));
                if (device.isAlarmEnabled()) {
                    device.setAlarmEnabledValue(userManageBean.getResourceBundleString("label_true"));
                } else {
                    device.setAlarmEnabledValue(userManageBean.getResourceBundleString("label_false"));
                }
            });
            distinctDevicePresetList.addAll(list);
            assetPresetPointLocationVO.setDistinctDevicePresetList(new ArrayList<>(distinctDevicePresetList));
        }
    }

    /**
     * Open the Point Location Overlay
     */
    public void openPointLocationOverlay() {
        try {
            //assetPresetVO.setType(siteOrGlobal);
            if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("addPresetsPointLocationBean") == null) {
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("addPresetsPointLocationBean", new AddPresetsPointLocationBean());
                ((AddPresetsPointLocationBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("addPresetsPointLocationBean")).init();
            }
            ((AddPresetsPointLocationBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("addPresetsPointLocationBean")).presetFields(assetPresetVO);
            navigationBean.updateSecondDialog("add-preset-point-location");
        } catch (Exception e) {
            Logger.getLogger(CreateWorkOrderBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     *
     * @param assetPresetPointLocationVO
     * @param operationType, int
     */
    public void setAssetPointLocationDialog(AssetPresetPointLocationVO assetPresetPointLocationVO, int operationType) {
        try {
            // assetPresetVO.setType(siteOrGlobal);
            if (operationType >= 0 && operationType <= 2 && navigationBean != null) {
                if ((operationType == 2 || operationType == 1) && assetPresetPointLocationVO != null) {
                    if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("copyEditPresetsPointLocationBean") == null) {
                        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("copyEditPresetsPointLocationBean", new CopyEditPresetsPointLocationBean());
                        ((CopyEditPresetsPointLocationBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("copyEditPresetsPointLocationBean")).init();
                    }
                    ((CopyEditPresetsPointLocationBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("copyEditPresetsPointLocationBean")).presetFields(operationType, assetPresetPointLocationVO, assetPresetVO);
                    if (operationType == 1) {
                        navigationBean.updateSecondDialog("edit-preset-point-location");
                    } else {
                        navigationBean.updateSecondDialog("copy-preset-point-location");
                    }
                } else if (operationType == 0 && assetPresetVO == null) {
                    if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("createPresetsAssetBean") == null) {
                        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("createPresetsAssetBean", new CreatePresetsAssetBean());
                    }
                    ((CreatePresetsAssetBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("createPresetsAssetBean")).init();

                    navigationBean.updateSecondDialog("create-presets-asset");
                }
            }
        } catch (Exception e) {
            Logger.getLogger(PresetsBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Deletes the given object
     *
     * @param obj, Object
     * @param tab, String Object
     */
    public void delete(Object obj, String tab) {
        AssetPresetPointLocationVO pointLocationNamesVO;
        if (obj != null && tab != null) {
            try {
                switch (tab) {
                    case "pointLocationNames":
                        pointLocationNamesVO = (AssetPresetPointLocationVO) obj;
                        assetPresetVO.getAssetPresetPointLocationVOList().remove(pointLocationNamesVO);
                        PrimeFaces.current().ajax().update("copyEditPresetsAssetFormId:pointLocationDatatableId");
                        PrimeFaces.current().ajax().update("copyEditPresetsAssetFormId:assetPresetSaveId");
//                        PrimeFaces.current().ajax().update("createAssetsFormId:pointLocationDatatableId");
//                        PrimeFaces.current().ajax().update("createAssetsFormId:assetPresetSaveId2");
                        break;

                }
            } catch (Exception e) {
                Logger.getLogger(PresetsBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            }
        }
    }

    public void submit() {
        if (userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null) {
            UUID assetPresetId;
            if (this.operationType == 2) {
                assetPresetId = null;
            } else {
                assetPresetId = assetPresetVO.getAssetPresetId();
            }
            List<AssetPresetVO> list = new ArrayList();
            assetPresetVO.getAssetPresetPointLocationVOList().forEach(pl -> {
                AssetPresetVO assetPreset = new AssetPresetVO();
                assetPreset.setCustomerAccount(userManageBean.getCurrentSite().getCustomerAccount());
                assetPreset.setSiteId(userManageBean.getCurrentSite().getSiteId());
                assetPreset.setAssetTypeName(assetPresetVO.getAssetTypeName());
                assetPreset.setAssetPresetId(assetPresetVO.getAssetPresetId());
                assetPreset.setPointLocationName(pl.getPointLocationName());
                assetPreset.setAssetPresetName(assetPresetVO.getAssetPresetName());
                assetPreset.setDevicePresetId(pl.getDeviceId());
                assetPreset.setSampleInterval(assetPresetVO.getSampleInterval());
                assetPreset.setFfSetIds(pl.getFfSetIds());
                assetPreset.setRollDiameter(pl.getRollDiameter());
                assetPreset.setRollDiameterUnits(pl.getRollDiameterUnits());
                assetPreset.setSpeedRatio(pl.getSpeedRatio());
                assetPreset.setTachId(pl.getTachId());
                assetPreset.setDescription(assetPresetVO.getDescription());
                assetPreset.setAssetPresetId(assetPresetId);
                assetPreset.setDevicePresetType(pl.getDeviceType());
                list.add(assetPreset);
            });

            if (siteOrGlobal.equalsIgnoreCase("Global")) {
                // For Edit
                if (AssetPresetClient.getInstance().updateGlobalAssetPresetVO(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), list)) {
                    setNonAutoGrowlMsg("growl_update_item_items");
                    Logger.getLogger(CopyEditPresetsAssetBean.class.getName()).log(Level.INFO, "Global Device Presets updated successfully.");
                } else {
                    Logger.getLogger(CopyEditPresetsAssetBean.class.getName()).log(Level.INFO, "Error in updating Site Device Presets.");
                }
            } else {
                // For Copy
                if (this.operationType == 2) {
                    if (AssetPresetClient.getInstance().createSiteAssetPresetVO(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), list)) {
                        setNonAutoGrowlMsg("growl_create_item_items");
                        Logger.getLogger(CreatePresetsDeviceBean.class.getName()).log(Level.INFO, "Site Asset preset created successfully.");
                    } else {
                        Logger.getLogger(CreatePresetsDeviceBean.class.getName()).log(Level.INFO, "Error in creating Site Asset preset.");
                    }
                } // For Edit
                else {
                    if (AssetPresetClient.getInstance().updateSiteAssetPresetVO(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), list)) {
                        setNonAutoGrowlMsg("growl_update_item_items");
                        Logger.getLogger(CopyEditPresetsAssetBean.class.getName()).log(Level.INFO, "Site Device Presets updated successfully.");
                    } else {
                        Logger.getLogger(CopyEditPresetsAssetBean.class.getName()).log(Level.INFO, "Error in updating Site Device Presets.");
                    }
                }
            }

            ((PresetsBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("presetsBean")).resetAssetsTab();
            PrimeFaces.current().ajax().update("presetsTabViewId:assetsFormId");
            PrimeFaces.current().executeScript("PF('defaultDlg').hide()");
        }
    }
    
    /**
     * Convert null or empty String to "N/A"
     * @param value, String Object
     * @return String Object
     */
    public String convertNullToNA(String value) {
        if (value == null || value.isEmpty()) {
            return "N/A";
        } else {
            return value;
        }
    }

    public List<SelectItem> getFrequencyUnitsList() {
        return getFrequencyPlusUnits();
    }

    public int getOperationType() {
        return operationType;
    }

    public void setOperationType(int operationType) {
        this.operationType = operationType;
    }

    public AssetPresetVO getAssetPresetVO() {
        return assetPresetVO;
    }

    public List<String> getAssetTypeList() {
        return assetTypeList;
    }

    public String getSiteOrGlobal() {
        return siteOrGlobal;
    }

    public void setSiteOrGlobal(String siteOrGlobal) {
        this.siteOrGlobal = siteOrGlobal;
    }

    public List<DevicePresetVO> getDevicePresetList() {
        return devicePresetList;
    }

    public void setDevicePresetList(List<DevicePresetVO> devicePresetList) {
        this.devicePresetList = devicePresetList;
    }

    public List<DevicePresetVO> getDistinctDevicePresetList() {
        return distinctDevicePresetList;
    }

}
