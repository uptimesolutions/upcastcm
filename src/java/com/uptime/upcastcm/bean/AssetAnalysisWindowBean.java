/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.APALSetClient;
import com.uptime.upcastcm.http.client.ApALByPointClient;
import com.uptime.upcastcm.http.client.PointClient;
import com.uptime.upcastcm.http.client.PointLocationClient;
import com.uptime.upcastcm.http.client.TrendDataClient;
import com.uptime.upcastcm.http.client.WorkOrderClient;
import com.uptime.upcastcm.http.utils.json.dom.FaultFrequenciesDomJsonParser;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import com.uptime.upcastcm.utils.AudioUtil;
import static com.uptime.upcastcm.utils.enums.AmpFactorsEnum.getAmpFactorItemList;
import static com.uptime.upcastcm.utils.enums.AmpFactorsEnum.getAmpFactorLabelByValue;
import static com.uptime.upcastcm.utils.enums.DwellEnum.getDwellItemList;
import static com.uptime.upcastcm.utils.enums.MarkersPatternEnum.getMarkersPatternValueList;
import com.uptime.upcastcm.utils.enums.ParamUnitEnum;
import com.uptime.upcastcm.utils.vo.ApAlSetVO;
import com.uptime.upcastcm.utils.vo.ChartInfoVO;
import com.uptime.upcastcm.utils.vo.PointLocationVO;
import com.uptime.upcastcm.utils.vo.PointVO;
import com.uptime.upcastcm.utils.vo.TrendValuesVO;
import com.uptime.upcastcm.utils.vo.WaveDataVO;
import static com.uptime.upcastcm.utils.enums.ParamUnitEnum.getACParamUnitItemListBySensorTypeParamType;
import static com.uptime.upcastcm.utils.enums.ParamUnitEnum.getParamUnitLabelByValue;
import static com.uptime.upcastcm.utils.enums.WorkOrderActionEnum.getActionItemList;
import static com.uptime.upcastcm.utils.enums.WorkOrderIssueEnum.getIssueItemList;
import static com.uptime.upcastcm.utils.enums.WorkOrderPriorityEnum.getPriorityItemList;
import com.uptime.upcastcm.utils.helperclass.Filters;
import com.uptime.upcastcm.utils.helperclass.GenericMessage;
import static com.uptime.upcastcm.utils.helperclass.GenericMessage.noChangesFound;
import static com.uptime.upcastcm.utils.helperclass.GenericMessage.setNonAutoGrowlMsg;
import java.io.Serializable;
import java.time.Instant;
import com.uptime.upcastcm.utils.vo.FaultFrequenciesVO;
import com.uptime.upcastcm.utils.vo.GraphDataVO;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import org.primefaces.PrimeFaces;
import us.uptime.processing.algorithms.RMS;
import us.uptime.processing.algorithms.Spectrum;
import us.uptime.processing.algorithms.UnitsConversion;
import us.uptime.processing.algorithms.Util;
import us.uptime.processing.algorithms.exceptions.UnitConversionException;
import static com.uptime.upcastcm.utils.helperclass.GenericMessage.trendDataNotAvailable;
import com.uptime.upcastcm.utils.vo.TrendDetailsVO;
import com.uptime.upcastcm.utils.vo.UUIDItemVO;
import com.uptime.upcastcm.utils.vo.WorkOrderVO;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Iterator;
import javax.faces.application.FacesMessage;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import us.uptime.processing.algorithms.TotalSpectralEnergy;
import us.uptime.processing.algorithms.WaveformCorrelation;

/**
 *
 * @author twilcox
 */
public class AssetAnalysisWindowBean implements Serializable {

    private static final String SPEC_DEFAULT_AMP_FACTOR = "RMS";
    private static final String WAVE_DEFAULT_TIME_UNIT = "s";
    private static final String SPEC_DEFAULT_FREQ_UNIT = "Hz";
    private static final String TACHSPEED_DEFAULT_UNIT = "Hz";
    private static final int HIGHCHART_CROPTHRESHOLD = 9999;

    final private DecimalFormat DECIMAL_FORMAT;
    final private List<SelectItem> dwellList, priorities, issueList, actionList, ampFactorList, timeUnitList, frequencyUnitList;
    final private List<Integer> startHarmonicList;
    final private List<String> markersPatternList;

    private NavigationBean navigationBean;
    private UserManageBean userManageBean;

    private Map<String, ChartInfoVO> chartInfoMap;
    private List<String> chartOptionsJsList;
    private float[] unitConvertedSamplesArray;

    private String windowId, timestamp, genericStr, xaxis, yaxis, yaxisTitle, chartOptionsJs, waveFormChartOptionsJs, spectrumChartOptionsJs;
    private boolean generateGraph, generateWaveAndSpecGraph, removeWindowDetails, acWindowOpened, itemDownload;
    private int windowCount;

    /**
     * Constructor
     */
    public AssetAnalysisWindowBean() {
        DECIMAL_FORMAT = new DecimalFormat("#.####");

        chartOptionsJsList = new ArrayList();
        chartInfoMap = new HashMap();
        startHarmonicList = new ArrayList();

        removeWindowDetails = true;
        acWindowOpened = false;
        generateGraph = false;
        generateWaveAndSpecGraph = false;

        windowCount = 0;
        chartOptionsJs = "{}";
        waveFormChartOptionsJs = "{}";
        spectrumChartOptionsJs = "{}";

        dwellList = getDwellItemList();
        actionList = getActionItemList();
        issueList = getIssueItemList();
        priorities = getPriorityItemList();
        ampFactorList = getAmpFactorItemList();
        timeUnitList = ParamUnitEnum.getTimeUnits();
        frequencyUnitList = ParamUnitEnum.getFrequencyPlusUnits();
        markersPatternList = getMarkersPatternValueList();

        for (int i = 1; i <= 49; i++) {
            startHarmonicList.add(i);
        }

        try {
            navigationBean = (NavigationBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("navigationBean");
            userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
        } catch (Exception e) {
            Logger.getLogger(AssetAnalysisWindowBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            userManageBean = null;
            navigationBean = null;
        }
    }

    /**
     * Post Constructor
     */
    @PostConstruct
    public void init() {
        chartOptionsJsList.clear();
        removeWindowDetails = true;
    }

    public void loadTrendGraph(Filters selectedFilters, boolean isLoadedFromAlarms) {
        List<PointLocationVO> pointLocationVOList;
        List<PointVO> pointVOList, apAlByPointList;
        List<TrendValuesVO> trendDataList;
        List<UUIDItemVO> apSetList;
        String paramsWindowId, paramName;
        PointLocationVO selectedPtlo;
        ChartInfoVO ciVo, tmp;
        boolean recordExist;
        long startTime = System.currentTimeMillis();
        try {
            chartOptionsJsList.clear();

            pointLocationVOList = new ArrayList();
            pointLocationVOList.addAll(PointLocationClient.getInstance().getPointLocationsByCustomerSiteAreaAsset(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue(), (UUID) selectedFilters.getAsset().getValue()));

            apAlByPointList = new ArrayList();
            apSetList = new ArrayList();
            pointVOList = new ArrayList();
            pointVOList.addAll(PointClient.getInstance().getPointsByCustomerSiteAreaAssetPointLocationPoint(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue(), (UUID) selectedFilters.getAsset().getValue(), (UUID) selectedFilters.getPointLocation().getValue(), (UUID) selectedFilters.getPoint().getValue(), false));
            pointVOList.stream().forEachOrdered(point -> {
                List<ApAlSetVO> apAlSetsVOList;

                if ((apAlSetsVOList = APALSetClient.getInstance().getSiteApAlSetsByCustomerSiteIdApSetIdAlSetId(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), point.getSiteId(), point.getApSetId(), point.getAlSetId())) == null || apAlSetsVOList.isEmpty()) {
                    apAlSetsVOList = APALSetClient.getInstance().getGlobalApAlSetsByCustomerApSetIdAlSetId(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), point.getApSetId(), point.getAlSetId());
                    if (apAlSetsVOList == null || apAlSetsVOList.isEmpty()) {
                        apAlSetsVOList = APALSetClient.getInstance().getGlobalApAlSetsByCustomerApSetIdAlSetId(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), DEFAULT_UPTIME_ACCOUNT, point.getApSetId(), point.getAlSetId());
                    }
                }

                if (apAlSetsVOList != null && !apAlSetsVOList.isEmpty()) {
                    if (!point.isDisabled()) {
                        apSetList.add(new UUIDItemVO(apAlSetsVOList.get(0).getApSetId(), apAlSetsVOList.get(0).getApSetName(), PRE_NODE_ICON));
                    } else {
                        apSetList.add(new UUIDItemVO(apAlSetsVOList.get(0).getApSetId(), apAlSetsVOList.get(0).getApSetName()));
                    }
                }
                apAlByPointList.addAll(PointClient.getInstance().getApALByPoints_bySiteAreaAssetPointLocationPointApSetAlSet(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue(), (UUID) selectedFilters.getAsset().getValue(), (UUID) selectedFilters.getPointLocation().getValue(), (UUID) selectedFilters.getPoint().getValue(), point.getApSetId(), point.getAlSetId()));
            });

            apAlByPointList.sort((s1, s2) -> {
                if (((PointVO) s1).getApSetId().compareTo(((PointVO) s2).getApSetId()) == 0 && ((PointVO) s1).getAlSetId().compareTo(((PointVO) s2).getAlSetId()) == 0) {
                    return ((PointVO) s1).getParamName().compareTo(((PointVO) s2).getParamName());
                } else if (((PointVO) s1).getApSetId().compareTo(((PointVO) s2).getApSetId()) == 0) {
                    return ((PointVO) s1).getAlSetId().compareTo(((PointVO) s2).getAlSetId());
                } else {
                    return ((PointVO) s1).getApSetId().compareTo(((PointVO) s2).getApSetId());
                }
            });
            Logger.getLogger(AssetAnalysisWindowBean.class.getName()).log(Level.INFO, "Timer1: {0}", System.currentTimeMillis() - startTime);
            if (!pointVOList.isEmpty() && pointVOList.get(0).getPointType().equalsIgnoreCase("AC")) {
                trendDataList = TrendDataClient.getInstance().getTrendsByCustomerSiteAreaAssetPointLocationPointApSetParam(ServiceRegistrarClient.getInstance().getServiceHostURL(WAVEFORM_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue(), (UUID) selectedFilters.getAsset().getValue(), (UUID) selectedFilters.getPointLocation().getValue(), (UUID) selectedFilters.getPoint().getValue(), (UUID) selectedFilters.getApSet().getValue(), selectedFilters.getAlSetParam().getLabel());
            } else {
                trendDataList = TrendDataClient.getInstance().getTrendsByCustomerSiteAreaAssetPointLocationPointApSet(ServiceRegistrarClient.getInstance().getServiceHostURL(WAVEFORM_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), (UUID) selectedFilters.getSite().getValue(), (UUID) selectedFilters.getArea().getValue(), (UUID) selectedFilters.getAsset().getValue(), (UUID) selectedFilters.getPointLocation().getValue(), (UUID) selectedFilters.getPoint().getValue(), (UUID) selectedFilters.getApSet().getValue());
            }

            generateGraph = !(trendDataList == null || trendDataList.isEmpty());
            paramName = selectedFilters.getAlSetParam().getLabel().equalsIgnoreCase("AUTO_TEMP") ? "Temperature" : selectedFilters.getAlSetParam().getLabel();
            Logger.getLogger(AssetAnalysisWindowBean.class.getName()).log(Level.INFO, "Timer2: {0}", System.currentTimeMillis() - startTime);
            paramsWindowId = "";
            if (generateGraph) {
                recordExist = false;

                if (chartInfoMap.isEmpty()) {
                    recordExist = false;
                } else {
                    for (Map.Entry<String, ChartInfoVO> entry : chartInfoMap.entrySet()) {
                        tmp = entry.getValue();
                        if (Objects.equals(tmp.getSiteId(), (UUID) selectedFilters.getSite().getValue())
                                && Objects.equals(tmp.getAreaId(), (UUID) selectedFilters.getArea().getValue())
                                && Objects.equals(tmp.getAssetId(), (UUID) selectedFilters.getAsset().getValue())
                                && Objects.equals(tmp.getPointLocationId(), (UUID) selectedFilters.getPointLocation().getValue())
                                && Objects.equals(tmp.getPointId(), (UUID) selectedFilters.getPoint().getValue())) {
                            recordExist = true;
                            paramsWindowId = entry.getKey();
                            break;
                        }
                    }
                }

                if (!recordExist) {
                    ciVo = new ChartInfoVO();
                    ciVo.setSpectrumGraphData(new GraphDataVO());
                    ciVo.setWaveGraphData(new GraphDataVO());
                    ciVo.setSiteId((UUID) selectedFilters.getSite().getValue());
                    ciVo.setAreaId((UUID) selectedFilters.getArea().getValue());
                    ciVo.setAssetId((UUID) selectedFilters.getAsset().getValue());
                    ciVo.setPointLocationId((UUID) selectedFilters.getPointLocation().getValue());
                    ciVo.setPointId((UUID) selectedFilters.getPoint().getValue());
                    ciVo.setSiteName(selectedFilters.getSite().getLabel());
                    ciVo.setAreaName(selectedFilters.getArea().getLabel());
                    ciVo.setAssetName(selectedFilters.getAsset().getLabel());
                    ciVo.setPointLocationName(selectedFilters.getPointLocation().getLabel());
                    ciVo.setPointName(selectedFilters.getPoint().getLabel());
                    ciVo.setRegion(selectedFilters.getRegion());
                    ciVo.setCountry(selectedFilters.getCountry());

                    for (PointVO pvo : pointVOList) {
                        if (pvo.getPointId().equals(ciVo.getPointId())) {
                            ciVo.setResolution(pvo.getResolution());
                            ciVo.setPointType(pvo.getPointType());
                            break;
                        }
                    }
                    
                    ciVo.setApSetList(apSetList.stream().sorted(Comparator.comparing(UUIDItemVO::getLabel)).collect(Collectors.toList()));
                    paramsWindowId = "window" + ++windowCount;
                    chartInfoMap.put(paramsWindowId, ciVo);
                } else {
                    ciVo = chartInfoMap.get(paramsWindowId);
                    ciVo.getSpectrumGraphData().setEndingHarmonic(null);
                    ciVo.getSpectrumGraphData().setStartingHarmonic(null);
                    ciVo.getSpectrumGraphData().setSelectedFfIds(null);
                    removeWindowDetails = false;
                }
                
                if (!pointVOList.isEmpty()) {
                    apAlByPointList.stream().forEach(pvo -> pvo.setSensorChannelNum(pointVOList.get(0).getSensorChannelNum()));
                }
                apAlByPointList.stream().forEach(pvo -> pvo.setPointType(ciVo.getPointType()));
                ciVo.setApAlByPointList(new ArrayList(apAlByPointList));
                
                ciVo.setParamNameList(apAlByPointList.stream().filter(apAlByPoint -> apAlByPoint.getApSetId().equals((UUID) selectedFilters.getApSet().getValue())).map(PointVO::getParamName).collect(Collectors.toList()));
                ciVo.setWindowDisplayName("UpCastCM - " + paramsWindowId.replace("window", "Window "));
                ciVo.setGraphTitle(selectedFilters.getRegion() + " > " + selectedFilters.getCountry() + " > " + selectedFilters.getSite().getLabel() + " > " + selectedFilters.getArea().getLabel() + " > " + selectedFilters.getAsset().getLabel() + " > " + selectedFilters.getPointLocation().getLabel() + " > " + selectedFilters.getPoint().getLabel());
                ciVo.setApSetItem(apSetList.stream().filter(item -> item.getValue().equals((UUID) selectedFilters.getApSet().getValue())).findFirst().get());
                ciVo.setApSetId(ciVo.getApSetItem().getValue());
                ciVo.setParamName(paramName);
                ciVo.setRefreshWaveAndSpectrum(false);

                for (PointVO pvo : ciVo.getApAlByPointList()) {
                    if (pvo.getApSetId().equals(ciVo.getApSetId())) {
                        if (Objects.equals("Temperature", pvo.getSensorType())) {
                            if (Objects.equals("AUTO_TEMP", ciVo.getParamName()) || Objects.equals("Temperature", ciVo.getParamName())) {
                                ciVo.setSelectedApAlByPoint(pvo);
                                break;
                            }
                        } else {
                            if (Objects.equals(pvo.getParamName(), ciVo.getParamName())) {
                                ciVo.setSelectedApAlByPoint(pvo);
                                break;
                            }
                        }
                    }
                }

                if (isLoadedFromAlarms) {
                    ciVo.setGraphTimestamp(timestamp);
                }
                if (pointLocationVOList.stream().anyMatch(vo -> vo.getPointLocationId().equals(ciVo.getPointLocationId()))) {
                    selectedPtlo = pointLocationVOList.stream().filter(vo -> vo.getPointLocationId().equals(ciVo.getPointLocationId())).findAny().get();
                    ciVo.setSerialNum(selectedPtlo.getDeviceSerialNumber());
                    ciVo.setSampleInterval(selectedPtlo.getSampleInterval());
                    ciVo.setFfSetVOList(FaultFrequenciesDomJsonParser.getInstance().parseFfSetIdsJson(selectedPtlo.getFfSetIds()));
                }
                Logger.getLogger(AssetAnalysisWindowBean.class.getName()).log(Level.INFO, "Timer3: {0}", System.currentTimeMillis() - startTime);
                configureHighChart(trendDataList, paramsWindowId, true);
                Logger.getLogger(AssetAnalysisWindowBean.class.getName()).log(Level.INFO, "Timer4: {0}", System.currentTimeMillis() - startTime);
            } else {
                trendDataNotAvailable();
            }
        } catch (Exception e) {
            Logger.getLogger(AssetAnalysisWindowBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    public void updateBean() {
        Logger.getLogger(AssetAnalysisWindowBean.class.getName()).log(Level.INFO, "updateBean() called.");
    }

    /**
     * Configure the Highchart options
     *
     * @param trendDataList, list of trend data for a given param
     * @param paramsWindowId, a unique id for a graph window
     * @param updateWaveAndSpecChart
     */
    public void configureHighChart(List<TrendValuesVO> trendDataList, String paramsWindowId, boolean updateWaveAndSpecChart) {
        int selectedDataPointIndex = 0, count, port;
        ExternalContext context;
        ChartInfoVO ciVo;
        ArrayList al;
        ArrayList tmp;
        String yAxisUnit, trendGraphTitle, alPlotLines, protocal, server, path;
        long startTime = System.currentTimeMillis();

        try {
            ciVo = chartInfoMap.get(paramsWindowId);
            ciVo.setyAxisUpperBound(Float.MIN_VALUE);

            al = new ArrayList();
            if (ciVo.getGraphTimestamp() != null) {
                count = 0;

                for (TrendValuesVO tvo : trendDataList) {
                    tmp = new ArrayList();
                    tmp.add(tvo.getSampleTime().toEpochMilli());
                    tmp.add(tvo.getValue());
                    al.add(tmp);
                    if (tvo.getValue() > ciVo.getyAxisUpperBound()) {
                        ciVo.setyAxisUpperBound(tvo.getValue());
                    }
                    if (String.valueOf(tvo.getSampleTime().toEpochMilli()).equals(ciVo.getGraphTimestamp())) {
                        selectedDataPointIndex = count;
                    }
                    count++;
                }
                timestamp = ciVo.getGraphTimestamp();
            } else {
                for (TrendValuesVO tvo : trendDataList) {
                    tmp = new ArrayList();
                    tmp.add(tvo.getSampleTime().toEpochMilli());
                    tmp.add(tvo.getValue());
                    al.add(tmp);

                    if (tvo.getValue() > ciVo.getyAxisUpperBound()) {
                        ciVo.setyAxisUpperBound(tvo.getValue());
                    }
                }
                timestamp = String.valueOf(trendDataList.get(0).getSampleTime().toEpochMilli());
                ciVo.setGraphTimestamp(timestamp);
            }

            yAxisUnit = "";
            Logger.getLogger(AssetAnalysisWindowBean.class.getName()).log(Level.INFO, "Timer1: {0}", System.currentTimeMillis() - startTime);
            if (!trendDataList.isEmpty()) {
                yAxisUnit = trendDataList.get(0).getUnits();
                windowId = paramsWindowId;
                if (updateWaveAndSpecChart) {
                    displayAssociatedWaveAndSpectrum(false);
                }
            }
            Logger.getLogger(AssetAnalysisWindowBean.class.getName()).log(Level.INFO, "Timer2: {0}", System.currentTimeMillis() - startTime);
            if (ciVo.getSelectedApAlByPoint().isHighAlertActive() && !ciVo.getSelectedApAlByPoint().isHighFaultActive()) {
                ciVo.setyAxisUpperBound(ciVo.getSelectedApAlByPoint().getHighAlert() > ciVo.getyAxisUpperBound() ? ciVo.getSelectedApAlByPoint().getHighAlert() : ciVo.getyAxisUpperBound());
            } else if (ciVo.getSelectedApAlByPoint().isHighFaultActive()) {
                ciVo.setyAxisUpperBound(ciVo.getSelectedApAlByPoint().getHighFault() > ciVo.getyAxisUpperBound() ? ciVo.getSelectedApAlByPoint().getHighFault() : ciVo.getyAxisUpperBound());
            }

            ciVo.setyAxisUpperBound((float) (ciVo.getyAxisUpperBound() * 1.1));
            trendGraphTitle = ciVo.getSelectedApAlByPoint().getParamName();
            alPlotLines = getAlarmLimitPlotLines(ciVo.getSelectedApAlByPoint());
            chartOptionsJs = getTrendChartJson(ciVo.getPointType(), al.toString(), yAxisUnit, trendGraphTitle, alPlotLines, selectedDataPointIndex, ciVo.getyAxisUpperBound(), getAmpFactorLabelByValue(ciVo.getSelectedApAlByPoint().getParamAmpFactor()));
            Logger.getLogger(AssetAnalysisWindowBean.class.getName()).log(Level.INFO, "Timer3: {0}", System.currentTimeMillis() - startTime);
            if (updateWaveAndSpecChart) {
                PrimeFaces.current().ajax().update("javascriptID");

                context = FacesContext.getCurrentInstance().getExternalContext();
                protocal = context.getRequestScheme();
                server = context.getRequestServerName();
                port = context.getRequestServerPort();

                if (!(port == 8080 || port == 8443)) {
                    path = protocal + "://" + server + "" + FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() + "/user/asset-analysis-window.xhtml?winid=" + windowId;
                } else {
                    path = protocal + "://" + server + ":" + port + "" + FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() + "/user/asset-analysis-window.xhtml?winid=" + windowId;
                }
                PrimeFaces.current().executeScript("openChartInNewBrowser('" + path + "','" + paramsWindowId + "')");
            }
            Logger.getLogger(AssetAnalysisWindowBean.class.getName()).log(Level.INFO, "Timer4: {0}", System.currentTimeMillis() - startTime);
        } catch (Exception e) {
            Logger.getLogger(AssetAnalysisWindowBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Configure the Waveform & spectrum chart options
     *
     * @param isCalledFromUI, boolean
     */
    public void displayAssociatedWaveAndSpectrum(boolean isCalledFromUI) {
        long startTime;
        ChartInfoVO ciVo;
        Instant sampleTime;
        String timeDisplayedInHeader, yAxisUnit, samplesStr, graphHeader, toUnit, id;
        GraphDataVO waveGraphData, spectrumGraphData;
        OffsetDateTime offsetDateTime;
        WaveDataVO waveDataVO;
        List<Float> samples;
        float[] samplesArray, spectrumArray;
        float timeDelta, frequencyDelta;

        try {
            if ((id = getCurrentWindowId()) != null) {
                startTime = System.currentTimeMillis();
                Logger.getLogger(AssetAnalysisWindowBean.class.getName()).log(Level.INFO, "timestamp:{0} , windowId:{1}", new Object[]{timestamp, windowId = id});

                ciVo = chartInfoMap.get(windowId);
                ciVo.setCount(0);
                if (Objects.equals("AC", ciVo.getPointType())) {
                    generateWaveAndSpecGraph = true;
                    if (isCalledFromUI) {
                        ciVo.setGraphTimestamp(timestamp);
                    }
                    sampleTime = Instant.ofEpochMilli(Long.valueOf(ciVo.getGraphTimestamp()));
                    timeDisplayedInHeader = userManageBean.getTzOffsetString(sampleTime, 3);
                    offsetDateTime = sampleTime.atOffset(ZoneOffset.UTC);
                    Logger.getLogger(AssetAnalysisWindowBean.class.getName()).log(Level.INFO, "displayAssociatedWaveAndSpectrum method execution time1 {0}", System.currentTimeMillis() - startTime);
                    waveDataVO = TrendDataClient.getInstance().getByCustomerSiteAreaAssetPointLocationPointSampleYearSampleMonthSampleTime(ServiceRegistrarClient.getInstance().getServiceHostURL(WAVEFORM_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), ciVo.getSiteId(), ciVo.getAreaId(), ciVo.getAssetId(), ciVo.getPointLocationId(), ciVo.getPointId(), (short) offsetDateTime.getYear(), (byte) offsetDateTime.getMonthValue(), sampleTime);
                    Logger.getLogger(AssetAnalysisWindowBean.class.getName()).log(Level.INFO, "displayAssociatedWaveAndSpectrum method execution time2 {0}", System.currentTimeMillis() - startTime);
                    ciVo.setWaveDataVO(waveDataVO);
                    if (waveDataVO != null && waveDataVO.getWave() != null) {
                        ciVo.getWaveGraphData().setTachSpeed(waveDataVO.getWave().getTachSpeed());
                        ciVo.getWaveGraphData().setOriginalTachSpeed(waveDataVO.getWave().getTachSpeed());
                        ciVo.getWaveGraphData().setTachSpeedUnit(TACHSPEED_DEFAULT_UNIT);
                        ciVo.getSpectrumGraphData().setTachSpeed(waveDataVO.getWave().getTachSpeed());
                        ciVo.getSpectrumGraphData().setOriginalTachSpeed(waveDataVO.getWave().getTachSpeed());
                        ciVo.getSpectrumGraphData().setTachSpeedUnit(TACHSPEED_DEFAULT_UNIT);
                        samples = waveDataVO.getWave().getSamples();
                        if (waveDataVO.getWave().getSensitivityUnits() != null && waveDataVO.getWave().getSensitivityUnits().equalsIgnoreCase("gs")) {
                            waveDataVO.getWave().setSensitivityUnits("Gs");
                        }
                        yAxisUnit = waveDataVO.getWave().getSensitivityUnits();
                        graphHeader = timeDisplayedInHeader;
                        if (samples != null) {

                            samplesArray = getFloatArrayFromSampleList(samples);

                            waveGraphData = ciVo.getWaveGraphData();
                            waveGraphData.setSelectedTimeUnit(WAVE_DEFAULT_TIME_UNIT);
                            waveGraphData.setSelectedAmplitudeUnit(yAxisUnit);

                            spectrumGraphData = ciVo.getSpectrumGraphData();
                            spectrumGraphData.setSelectedFrequencyUnit(SPEC_DEFAULT_FREQ_UNIT);
                            spectrumGraphData.setSelectedAmplitudeUnit(yAxisUnit);
                            spectrumGraphData.setSelectedAmpFactor(SPEC_DEFAULT_AMP_FACTOR);

                            if (isUnitsConversionRequired(ciVo, "wave", "x")) {
                                waveGraphData.setSelectedTimeUnit(getUnitforSensorType("time", "wave"));
                            }

                            timeDelta = calculateDeltaTime(waveGraphData.getSelectedTimeUnit(), samplesArray.length, ciVo.getSelectedApAlByPoint().getPeriod());
                            if (isUnitsConversionRequired(ciVo, "wave", "y")) {
                                toUnit = getUnitforSensorType(ciVo.getSelectedApAlByPoint().getSensorType(), "wave");
                                waveGraphData.setSelectedAmplitudeUnit(toUnit);
                                samplesStr = getUnitConvertedSamplesAsString(samplesArray, yAxisUnit, timeDelta, waveGraphData);
                                waveGraphData.setRmsWave(Float.valueOf(DECIMAL_FORMAT.format(RMS.calculate(unitConvertedSamplesArray))));

                                yAxisUnit = toUnit;
                            } else {
                                samplesStr = AssetAnalysisWindowBean.this.getSamplesAsString(samplesArray, timeDelta, waveGraphData);
                                waveGraphData.setRmsWave(Float.valueOf(DECIMAL_FORMAT.format(RMS.calculate(samplesArray))));
                            }

                            waveFormChartOptionsJs = getWaveChartJson(samplesStr, yAxisUnit, waveGraphData.getSelectedTimeUnit(), "none", graphHeader);

                            spectrumArray = Spectrum.getSpectrumAsFloat(samplesArray, waveDataVO.getWave().getSampleRateHz(), spectrumGraphData.getTachSpeed());
                            if (isAmpFactorConversionRequired(ciVo)) {
                                spectrumArray = Util.applyAmpFactor(spectrumArray, SPEC_DEFAULT_AMP_FACTOR, spectrumGraphData.getSelectedAmpFactor());
                            }
                            if (isUnitsConversionRequired(ciVo, "spectrum", "x")) {
                                spectrumGraphData.setSelectedFrequencyUnit(getUnitforSensorType("frequency", "spectrum"));
                                if (Objects.equals("Orders", spectrumGraphData.getSelectedFrequencyUnit()) && Float.compare(spectrumGraphData.getTachSpeed(), 0.0f) == 0) {
                                    spectrumGraphData.setSelectedFrequencyUnit(SPEC_DEFAULT_FREQ_UNIT);
                                }
                            }
                            frequencyDelta = calculateDeltaFrequency(spectrumGraphData, ciVo.getSelectedApAlByPoint().getResolution(), ciVo.getSelectedApAlByPoint().getfMax());
                            yAxisUnit = spectrumGraphData.getSelectedAmplitudeUnit();
                            if (isUnitsConversionRequired(ciVo, "spectrum", "y")) {
                                toUnit = getUnitforSensorType(ciVo.getSelectedApAlByPoint().getSensorType(), "spectrum");
                                spectrumGraphData.setSelectedAmplitudeUnit(toUnit);
                                if (ciVo.getSelectedApAlByPoint().getSensorType().equalsIgnoreCase("acceleration")) {
                                    samplesStr = getSpectralUnitConvertedSamplesAsString(spectrumArray, yAxisUnit, frequencyDelta, spectrumGraphData, ciVo);
                                } else {
                                    samplesStr = getUnitConvertedSamplesAsString(spectrumArray, yAxisUnit, frequencyDelta, spectrumGraphData);
                                }
                                spectrumGraphData.setTotalAmplSpec(Float.valueOf(DECIMAL_FORMAT.format(TotalSpectralEnergy.calculate(unitConvertedSamplesArray))));
                                yAxisUnit = toUnit;
                            } else {
                                samplesStr = AssetAnalysisWindowBean.this.getSamplesAsString(spectrumArray, frequencyDelta, spectrumGraphData);
                                spectrumGraphData.setTotalAmplSpec(Float.valueOf(DECIMAL_FORMAT.format(TotalSpectralEnergy.calculate(spectrumArray))));
                            }
                            setSpectrumLogYaxis(ciVo);
                            spectrumChartOptionsJs = getSpectrumChartJson(samplesStr, yAxisUnit, getParamUnitLabelByValue(spectrumGraphData.getSelectedFrequencyUnit()), getAmpFactorLabelByValue(spectrumGraphData.getSelectedAmpFactor()), graphHeader, ciVo.isSpectrumLogYAxis());

                            // Redraw plot lines
                            if (isCalledFromUI) {
                                onFFSelect();
                            }

                        } else {
                            waveFormChartOptionsJs = "{}";
                            spectrumChartOptionsJs = "{}";
                        }
                    } else {
                        waveFormChartOptionsJs = "{}";
                        spectrumChartOptionsJs = "{}";
                    }
                } else {
                    generateWaveAndSpecGraph = false;
                }
                Logger.getLogger(AssetAnalysisWindowBean.class.getName()).log(Level.INFO, "displayAssociatedWaveAndSpectrum method execution time3 {0}", System.currentTimeMillis() - startTime);
                PrimeFaces.current().ajax().update("javaScriptPoint1");
                PrimeFaces.current().ajax().update("javaScriptPoint2");
                PrimeFaces.current().ajax().update("waveFormPanel");
                PrimeFaces.current().ajax().update("spectrumPanel");

                Logger.getLogger(AssetAnalysisWindowBean.class.getName()).log(Level.INFO, "displayAssociatedWaveAndSpectrum method execution time4 {0}", System.currentTimeMillis() - startTime);
            }
        } catch (Exception e) {
            Logger.getLogger(AssetAnalysisWindowBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    public void updateRemoveWindowDetails() {
        ChartInfoVO ciVo;
        String id;

        try {
            if ((id = getCurrentWindowId()) != null) {
                windowId = id;

                if ((ciVo = chartInfoMap.get(windowId)) != null) {
                    ciVo.setCount(ciVo.getCount() + 1);
                    if (ciVo.getPointType() != null && ciVo.getPointType().equalsIgnoreCase("AC")) {
                        acWindowOpened = true;
                        for (Map.Entry<String, ChartInfoVO> entry : chartInfoMap.entrySet()) {
                            if (entry.getValue().getPointType().equalsIgnoreCase("DC")) {
                                entry.getValue().setCount(2);
                            }
                        }
                    }
                }
                removeWindowDetails = false;
            }
        } catch (Exception e) {
            Logger.getLogger(AssetAnalysisWindowBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Configure the trend chart options
     *
     * @param pointType, String Object
     * @param values, the chart values
     * @param yAxisTitle, yAxis title
     * @param trendGraphTitle, trend graph title
     * @param plotLines, alarm limit lines
     * @param selectedDataPointIndex
     * @param yAxisUpperBound
     * @param paramAmpFactor
     * @return trend chart options
     */
    public String getTrendChartJson(String pointType, String values, String yAxisTitle, String trendGraphTitle, String plotLines, int selectedDataPointIndex, float yAxisUpperBound, String paramAmpFactor) {
        StringBuilder sb;
        int minutes;

        minutes = userManageBean.getZoneId().getRules().getOffset(Instant.ofEpochMilli(Long.valueOf(timestamp))).getTotalSeconds() / 60;
        minutes = -1 * minutes;

        sb = new StringBuilder();
        sb
                .append("{ \"chart\" : { marginTop:70, \"zoomType\" : \"x\" ")
                //.append(", \"height\": 292.5")
                //.append(", \"height\": 330.5")
                .append(", \"height\": 280")
                //.append(", \"height\": (1080 / 1920 * 100) + '%'")
                //.append(", \"height\": (3 / 16 * 100) + '%'")
                .append(",events: { load() { updateRemoveWindowDetails(), this.series[0].points[").append(selectedDataPointIndex).append("].select() } }")
                .append(" }, time: { timezoneOffset: ").append(minutes).append(" }, \"series\" : [ { lineWidth: 1, states: { hover: { lineWidth: 1 }  }, \"data\" :")
                //.append("{ \"chart\" : { \"zoomType\" : 'x'  }, events: { load: function() {  let categoryHeight = 35; this.update({ chart: { height: categoryHeight * this.pointCount + (this.chartHeight - this.plotHeight)  }  }) }  }\"series\" : [ { \"data\" :")
                .append(values)
                .append(", color: \"").append("#003399").append("\"")
                .append(", \"name\" : \" \", cropThreshold: ").append(HIGHCHART_CROPTHRESHOLD).append(" } ], ")
                .append("plotOptions: { series: {  ")
                .append("point: { events: { select: function () {var date = new Date(this.x).toLocaleString(\"en-US\", {timeZone: \"").append(userManageBean.getZoneId().toString().equals("Z") ? "GMT" : userManageBean.getZoneId().toString()).append("\", weekday:\"short\", year:\"numeric\", month:\"short\", day:\"numeric\", hour:\"2-digit\", minute:\"2-digit\", second:\"2-digit\", hourCycle: 'h23'}); var text = date + '</br>' + Highcharts.numberFormat(this.y, 4); var chart = this.series.chart; if (!chart.lbl) { chart.lbl = chart.renderer.label(text, 75, 7).attr({ padding: 10,r: 5,zIndex: 0 }).css({color: 'red'}).add();} else {chart.lbl.attr({text: text}); } } , click: function (event) { this.select(!this.selected, false); } } },")
                .append(" marker: { enabled: true, radius: 0.1,  states: { select: { fillColor: 'red', lineWidth: 0, radius: 4  }, hover: { fillColor: '#4da6ff', lineWidth: 1, radius: 6 } } } } }")
                .append(", \"title\" : { \"text\" : \" ").append(trendGraphTitle).append(" \" },")
                .append("exporting: appendExportButtonsToTrenGraph(), ")
                .append(" \"legend\" : { \"enabled\" : false },")
                .append("\"yAxis\" : [ { min:0, max: ").append(yAxisUpperBound).append(",  \"title\" : { \"text\" : \" ").append(yAxisTitle)
                .append(Objects.equals(pointType, "AC") ? " - " : "").append(Objects.equals(pointType, "AC") ? paramAmpFactor : "")
                .append(" \" } ")
                .append(plotLines)
                .append(" } ],")
                .append("\"xAxis\": [{ \"title\": { \"text\": \" ").append(userManageBean.getResourceBundleString("label_sensorType_time")).append(" \" }, \"type\": \"").append("datetime").append("\" } ]")
                .append(",tooltip: { positioner: function() { return { x: this.chart.plotSizeX-135, y: this.chart.plotTop-45 }; }, formatter() { return new Date(this.x).toLocaleString(\"en-US\", { timeZone: \"").append(userManageBean.getZoneId().toString().equals("Z") ? "GMT" : userManageBean.getZoneId().toString()).append("\", weekday:\"short\", year:\"numeric\", month:\"short\", day:\"numeric\", hour:\"2-digit\", minute:\"2-digit\", second:\"2-digit\", hourCycle: 'h23'}) +'</br>' + Highcharts.numberFormat(this.y, 4)},shared: true, shadow: false, borderWidth: 0, backgroundColor: \"rgba(255,255,255,0.8)\" } ")
                //.append(" ,navigator: { top: 35, margin: 10, enabled: true, series: {  dataGrouping: { enabled: false } }")
            //    .append(" ,navigator: { margin: 10, enabled: true, series: {  dataGrouping: { enabled: false } }")
            //    .append(" ,xAxis: { type: 'linear' } }, scrollbar: { enabled: true }")
                .append("}");
                
        return sb.toString();
    }

    /**
     * Configure the Waveform chart options
     *
     * @param values, the chart values
     * @param yAxisTitle, yAxis title
     * @param xAxisTitle, xAxis title
     * @param xAxisType, xAxis Unit type
     * @param graphHeader, waveform graph title
     * @return waveform chart options
     */
    public String getWaveChartJson(String values, String yAxisTitle, String xAxisTitle, String xAxisType, String graphHeader) {
        StringBuilder sb;
        
        sb = new StringBuilder();
        sb
                .append("{ \"chart\" : { ") 
                .append("events: { load() { updateRemoveWindowDetails() }, render() { let chart = this; if (chart.customImg) { chart.customImg.destroy(); } chart.customImg =  chart.renderer.image(\"../javax.faces.resource/images/volume.png.xhtml?ln=roma-layout\", chart.plotWidth + chart.plotLeft-54, 12, 25, 20).on(\"click\", function() { generateWaveAudio(); }).attr({ zIndex: 100 }).attr({'class': ''}).attr({'title' : '").append(userManageBean.getResourceBundleString("label_volume_waveform")).append("'}).css({ cursor: \"pointer\" }).add(); chart.customImg.element.onmouseenter = function () { this.style.opacity = '0.7'; }; chart.customImg.element.onmouseleave = function () { this.style.opacity = '1'; };     } },")
                .append(" \"zoomType\" : \"x\" ")
                //.append(", \"height\": 292.5")
                .append(", \"height\": 242")
                //.append(", \"height\": (1080 / 1920 * 100) + '%'")
                //.append(", \"height\": (3 / 16 * 100) + '%'")
                .append("},")           
                .append("  \"boost\" : { useGPUTranslations: true }, \"series\" : [ { showInNavigator: true, boostThreshold: 5000, lineWidth: 1, states: { hover: { lineWidth: 1 }  }, \"data\" :")
                .append(values)
                .append(", color: \"").append("#003399").append("\"")
                .append(", \"name\" : \" \" } ], \"title\" : { \"text\" : \"").append(graphHeader).append("\" },")
                .append("plotOptions: { series: { showInNavigator: true, ")
                .append("point: { events: { select: function () { var text = 'Ampl: ' + Highcharts.numberFormat(this.y, 4) + '</br>' + 'Time: ' + Highcharts.numberFormat(this.x, 4); var chart = this.series.chart; if (!chart.lbl) { chart.lbl = chart.renderer.label(text, 75, 7).attr({ padding: 10,r: 5,zIndex: 0 }).css({color: 'red'}).add();} else {chart.lbl.attr({text: text}); } } , click: function (event) { this.select(!this.selected, false); } } },")
                .append(" marker: { enabled: true, radius: 0.1, states: { select: { fillColor: 'red', lineWidth: 0, radius: 4  }, hover: { fillColor: '#4da6ff', lineWidth: 1, radius: 6 } } } } },")
                .append("exporting: appendExportButtonsToWaveGraph(),")
                .append(" \"legend\" : { \"enabled\" : false },")
                .append("\"yAxis\" : [ { \"title\" : { \"text\" : \" ")
                .append(yAxisTitle)
                .append(" \" } ")
                .append(" } ],")
                .append("\"xAxis\": [{ min:0, \"title\": { \"text\": \" ").append(xAxisTitle).append(" \" }, \"type\": \"").append(xAxisType).append("\"  ")
                .append(" } ]")
                .append(",tooltip: { formatter: function(){ return 'Ampl: ' + Highcharts.numberFormat(this.y, 4) + '</br>' + 'Time: ' + Highcharts.numberFormat(this.x, 4) }, positioner: function() { return { x: this.chart.plotSizeX-120, y: this.chart.plotTop-45 }; }, shared: true, shadow: false, borderWidth: 0, backgroundColor: \"rgba(255,255,255,0.8)\" } ") 
            //    .append(" ,navigator: { margin:10, enabled: true, series: {  dataGrouping: { enabled: false } } ,xAxis: { type: 'linear', labels: { formatter: function() { return Highcharts.numberFormat(this.value, 2); } } } }")
            //    .append(" ,scrollbar: { enabled: true }")
                .append("}");
        
        return sb.toString();
    }

    /**
     * Configure the spectrum chart options
     *
     * @param values, the chart values
     * @param yAxisTitle, yAxis title
     * @param xAxisTitle, xAxis title
     * @param ampFactor, applies on chart data
     * @param graphHeader, waveform graph title
     * @param logYaxis, boolean
     * @return spectrum chart options
     */
    public String getSpectrumChartJson(String values, String yAxisTitle, String xAxisTitle, String ampFactor, String graphHeader, boolean logYaxis) {
        StringBuilder sb;

        sb = new StringBuilder();
        sb
                .append("{ \"chart\" : { \"zoomType\" : \"x\" ")
                //.append(", \"height\": 292.5")
                .append(", \"height\": 242")
                //.append(", \"height\": (1080 / 1920 * 100) + '%'")
                //.append(", \"height\": (3 / 16 * 100) + '%'")
                .append(", events: { load() { updateRemoveWindowDetails() } }")
                .append(" }, \"series\" : [ { lineWidth: 1, states: { hover: { lineWidth: 1 }  }, \"data\" :")
                .append(values)
                .append(", color: \"").append("#003399").append("\"")
                .append(", \"name\" : \" \" } ], \"title\" : { \"text\" : \"").append(graphHeader).append("\" },")
                .append("plotOptions: { series: { ")
                .append("point: { events: { select: function () { var text = 'Ampl: ' + Highcharts.numberFormat(this.y, 4) + '</br>' + 'Freq: ' + Highcharts.numberFormat(this.x, 4); var chart = this.series.chart; if (!chart.lbl) { chart.lbl = chart.renderer.label(text, 75, 7).attr({ padding: 10,r: 5,zIndex: 0 }).css({color: 'red'}).add();} else {chart.lbl.attr({text: text}); } }  , click: function (event) { this.select(!this.selected, false); } } },")
                .append(" marker: { enabled: true, radius: 0.1, states: { select: { fillColor: 'red', lineWidth: 0, radius: 4  }, hover: { fillColor: '#4da6ff', lineWidth: 1, radius: 6 } } } } },")
                .append("exporting: appendExportButtonsToSpectrumGraph(),")
                .append(" \"legend\" : { \"enabled\" : false },")
                .append(logYaxis ? "\"yAxis\" : [ { type: \"logarithmic\" , minorTickInterval: 0.1 , \"title\" : { \"text\" : \" " : "\"yAxis\" : [ { min:0, \"title\" : { \"text\" : \" ")
                .append(yAxisTitle)
                .append(" - ").append(ampFactor).append(" \" } ")
                .append(" } ],")
                .append("\"xAxis\": [{ min:0, \"title\": { \"text\": \" ").append(xAxisTitle).append(" \" }, \"type\": \"none\"  ")
                .append("  } ]")
                .append(",tooltip: { formatter: function(){ return 'Ampl: ' + Highcharts.numberFormat(this.y, 4) + '</br>' + 'Freq: ' + Highcharts.numberFormat(this.x, 4) }, positioner: function() { return { x: this.chart.plotSizeX-120, y: this.chart.plotTop-45 }; }, shared: true, shadow: false, borderWidth: 0, backgroundColor: \"rgba(255,255,255,0.8)\" } ")
            //    .append(" ,navigator: { ").append(logYaxis ? "yAxis: {type: 'logarithmic'}," : "").append(" margin:10, enabled: true, series: {  dataGrouping: { enabled: false } } ,xAxis: { type: 'linear', labels: { formatter: function() { return Highcharts.numberFormat(this.value, 2); } } } }")
            //    .append(" ,scrollbar: { enabled: true }")
                .append("}");

        return sb.toString();
    }

    /**
     * Get the plotlines for alarm limits
     *
     * @param selectedApAlByPoint, PointVO Object
     */
    public String getAlarmLimitPlotLines(PointVO selectedApAlByPoint) {
        StringBuilder builder = new StringBuilder();

        builder.append(",\"plotLines\": [");
        if (userManageBean != null) {
            if (selectedApAlByPoint.isLowFaultActive()) {
                builder.append("{ \"color\": \"red\", \"width\": 2, \"dashStyle\": \"dash\", \"value\": ").append(selectedApAlByPoint.getLowFault()).append(", \"label\": { \"text\": \"").append(userManageBean.getResourceBundleString("label_lowFault")).append("\", \"align\": \"right\"} },");
            }
            if (selectedApAlByPoint.isLowAlertActive()) {
                builder.append("{ \"color\": \"orange\", \"width\": 2, \"dashStyle\": \"dash\", \"value\": ").append(selectedApAlByPoint.getLowAlert()).append(", \"label\": { \"text\": \"").append(userManageBean.getResourceBundleString("label_lowAlert")).append("\", \"align\": \"right\"} },");
            }
            if (selectedApAlByPoint.isHighAlertActive()) {
                builder.append("{ \"color\": \"orange\", \"width\": 2, \"dashStyle\": \"dash\", \"value\": ").append(selectedApAlByPoint.getHighAlert()).append(", \"label\": { \"text\": \"").append(userManageBean.getResourceBundleString("label_highAlert")).append("\", \"align\": \"right\"} },");
            }
            if (selectedApAlByPoint.isHighFaultActive()) {
                builder.append("{ \"color\": \"red\", \"width\": 2, \"dashStyle\": \"dash\", \"value\": ").append(selectedApAlByPoint.getHighFault()).append(", \"label\": { \"text\": \"").append(userManageBean.getResourceBundleString("label_highFault")).append("\", \"align\": \"right\"} }");
            }
        }
        builder.append("]");

        return builder.toString();
    }

    /**
     * Update the alarm limits in db.
     */
    public void updateAlarmLimits() {
        List<ApAlSetVO> apAlSetVOList;
        ApAlSetVO apAlSensorTypeVO;
        PointVO newAlarmLimits, vo;
        String id;

        try {
            if ((id = getCurrentWindowId()) != null && !id.isEmpty() && chartInfoMap.get(id) != null
                    && chartInfoMap.get(id).getSelectedApAlByPoint() != null && chartInfoMap.get(id).getSelectedAlarmLimitPoint() != null) {
                if (chartInfoMap.get(id).getSelectedAlarmLimitPoint().getLowFault() == chartInfoMap.get(id).getSelectedApAlByPoint().getLowFault()
                        && chartInfoMap.get(id).getSelectedAlarmLimitPoint().getLowAlert() == chartInfoMap.get(id).getSelectedApAlByPoint().getLowAlert()
                        && chartInfoMap.get(id).getSelectedAlarmLimitPoint().getHighAlert() == chartInfoMap.get(id).getSelectedApAlByPoint().getHighAlert()
                        && chartInfoMap.get(id).getSelectedAlarmLimitPoint().getHighFault() == chartInfoMap.get(id).getSelectedApAlByPoint().getHighFault()
                        && chartInfoMap.get(id).getSelectedAlarmLimitPoint().isLowFaultActive() == chartInfoMap.get(id).getSelectedApAlByPoint().isLowFaultActive()
                        && chartInfoMap.get(id).getSelectedAlarmLimitPoint().isLowAlertActive() == chartInfoMap.get(id).getSelectedApAlByPoint().isLowAlertActive()
                        && chartInfoMap.get(id).getSelectedAlarmLimitPoint().isHighAlertActive() == chartInfoMap.get(id).getSelectedApAlByPoint().isHighAlertActive()
                        && chartInfoMap.get(id).getSelectedAlarmLimitPoint().isHighFaultActive() == chartInfoMap.get(id).getSelectedApAlByPoint().isHighFaultActive()
                        && chartInfoMap.get(id).getSelectedAlarmLimitPoint().getDwell() == chartInfoMap.get(id).getSelectedApAlByPoint().getDwell()) {
                    noChangesFound();
                } else {
                    newAlarmLimits = new PointVO(chartInfoMap.get(id).getSelectedAlarmLimitPoint());
                    for (Iterator<PointVO> iterator = chartInfoMap.get(id).getApAlByPointList().iterator(); iterator.hasNext();) {
                        vo = iterator.next();
                        if (vo.getParamName().equals(newAlarmLimits.getParamName()) && vo.getAlSetId().equals(newAlarmLimits.getAlSetId()) && vo.getApSetId().equals(newAlarmLimits.getApSetId())) {
                            iterator.remove();
                            break;
                        }
                    }
                    chartInfoMap.get(id).getApAlByPointList().add(new PointVO(newAlarmLimits));
                    chartInfoMap.get(id).setSelectedApAlByPoint(new PointVO(newAlarmLimits));

                    apAlSetVOList = new ArrayList();
                    for (PointVO newAlarmLimit : chartInfoMap.get(id).getApAlByPointList().stream().filter(apAlByPoint -> apAlByPoint.getApSetId().equals(chartInfoMap.get(id).getSelectedApAlByPoint().getApSetId())).collect(Collectors.toList())) {
                        apAlSensorTypeVO = new ApAlSetVO();
                        apAlSensorTypeVO.setCustomerAccount(newAlarmLimit.getCustomerAccount());
                        apAlSensorTypeVO.setSensorType(newAlarmLimit.getSensorType());
                        apAlSensorTypeVO.setApSetName(newAlarmLimit.getApSetName());
                        apAlSensorTypeVO.setApSetId(newAlarmLimit.getApSetId());
                        apAlSensorTypeVO.setAlSetName(newAlarmLimit.getAlSetName());
                        apAlSensorTypeVO.setAlSetId(newAlarmLimit.getAlSetId());
                        apAlSensorTypeVO.setParamName(newAlarmLimit.getParamName());
                        apAlSensorTypeVO.setSiteName(newAlarmLimit.getSiteName());
                        apAlSensorTypeVO.setSiteId(newAlarmLimit.getSiteId());
                        apAlSensorTypeVO.setParamType(newAlarmLimit.getParamType());
                        apAlSensorTypeVO.setFrequencyUnits(newAlarmLimit.getFreqUnits());
                        apAlSensorTypeVO.setParamUnits(newAlarmLimit.getParamUnits());
                        apAlSensorTypeVO.setParamAmpFactor(newAlarmLimit.getParamAmpFactor());
                        apAlSensorTypeVO.setFmax(newAlarmLimit.getfMax());
                        apAlSensorTypeVO.setResolution(newAlarmLimit.getResolution());
                        apAlSensorTypeVO.setPeriod(newAlarmLimit.getPeriod());
                        apAlSensorTypeVO.setSampleRate(newAlarmLimit.getSampleRate());
                        apAlSensorTypeVO.setDemodHighPass(newAlarmLimit.getDemodHighPass());
                        apAlSensorTypeVO.setDemodLowPass(newAlarmLimit.getDemodLowPass());
                        apAlSensorTypeVO.setDemod(newAlarmLimit.isDemodEnabled());
                        apAlSensorTypeVO.setMinFrequency(newAlarmLimit.getMinFreq());
                        apAlSensorTypeVO.setMaxFrequency(newAlarmLimit.getMaxFreq());
                        apAlSensorTypeVO.setDwell(newAlarmLimit.getDwell());
                        apAlSensorTypeVO.setLowFault(newAlarmLimit.getLowFault());
                        apAlSensorTypeVO.setLowAlert(newAlarmLimit.getLowAlert());
                        apAlSensorTypeVO.setHighAlert(newAlarmLimit.getHighAlert());
                        apAlSensorTypeVO.setHighFault(newAlarmLimit.getHighFault());
                        apAlSensorTypeVO.setLowFaultActive(newAlarmLimit.isLowFaultActive());
                        apAlSensorTypeVO.setLowAlertActive(newAlarmLimit.isLowAlertActive());
                        apAlSensorTypeVO.setHighAlertActive(newAlarmLimit.isHighAlertActive());
                        apAlSensorTypeVO.setHighFaultActive(newAlarmLimit.isHighFaultActive());
                        apAlSetVOList.add(apAlSensorTypeVO);
                    }
                    newAlarmLimits.setCustomized(true);
                    newAlarmLimits.setApAlSetVOs(apAlSetVOList);

                    if (ApALByPointClient.getInstance().updateApALByPoint(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), newAlarmLimits, "growlMessages")) {
                        //Logger.getLogger(AssetAnalysisWindowBean.class.getName()).log(Level.INFO, "Updated ApAlByPoint {0}", new Object[]{newAlarmLimits.toString()});
                        PrimeFaces.current().executeScript("PF('defaultDlg').hide()");
                    } else {
                        Logger.getLogger(AssetAnalysisWindowBean.class.getName()).log(Level.INFO, "Error in updating ApAlByPoint*********** {0}", new Object[]{newAlarmLimits.toString()});
                    }
                }
            }
        } catch (Exception e) {
            Logger.getLogger(AssetAnalysisWindowBean.class.getName()).log(Level.INFO, e.getMessage(), e);
        }
    }

    /**
     * Validate Alarm Limits
     *
     * @param reset, boolean, true if the selectedAlarmLimitPoint need to be reset, else false
     */
    public void alarmLimitValidation(boolean reset) {
        PointVO apAlByPoint;
        String id;

        if ((id = getCurrentWindowId()) != null && !id.isEmpty() && chartInfoMap.get(id) != null) {

            // Reset the selectedAlarmLimitPoint for the given field
            if (reset) {
                chartInfoMap.get(id).setSelectedAlarmLimitPoint(new PointVO(chartInfoMap.get(id).getSelectedApAlByPoint()));
            }

            // Validate Alarm Limits
            if ((apAlByPoint = chartInfoMap.get(id).getSelectedAlarmLimitPoint()) != null) {

                // Validates the lowFault field of the given Object
                if (apAlByPoint.isLowFaultActive()) {
                    if (apAlByPoint.getLowFault() <= 0) {
                        chartInfoMap.get(id).setLowFaultValidationFailed(true);
                    } else if (apAlByPoint.isLowAlertActive()) {
                        if (apAlByPoint.getLowFault() < apAlByPoint.getLowAlert()) {
                            chartInfoMap.get(id).setLowFaultValidationFailed(false);
                        } else {
                            chartInfoMap.get(id).setLowFaultValidationFailed(true);
                        }
                    } else if (apAlByPoint.isHighAlertActive()) {
                        if (apAlByPoint.getLowFault() < apAlByPoint.getHighAlert()) {
                            chartInfoMap.get(id).setLowFaultValidationFailed(false);
                        } else {
                            chartInfoMap.get(id).setLowFaultValidationFailed(true);
                        }
                    } else if (apAlByPoint.isHighFaultActive()) {
                        if (apAlByPoint.getLowFault() < apAlByPoint.getHighFault()) {
                            chartInfoMap.get(id).setLowFaultValidationFailed(false);
                        } else {
                            chartInfoMap.get(id).setLowFaultValidationFailed(true);
                        }
                    } else {
                        chartInfoMap.get(id).setLowFaultValidationFailed(false);
                    }
                } else {
                    chartInfoMap.get(id).setLowFaultValidationFailed(false);
                }

                // Validates the lowAlert field of the given Object
                if (apAlByPoint.isLowAlertActive()) {
                    if (apAlByPoint.getLowAlert() <= 0) {
                        chartInfoMap.get(id).setLowAlertValidationFailed(true);
                    } else if (apAlByPoint.isLowFaultActive() && apAlByPoint.isHighAlertActive()) {
                        if (apAlByPoint.getLowAlert() > apAlByPoint.getLowFault() && apAlByPoint.getLowAlert() < apAlByPoint.getHighAlert()) {
                            chartInfoMap.get(id).setLowAlertValidationFailed(false);
                        } else {
                            chartInfoMap.get(id).setLowAlertValidationFailed(true);
                        }
                    } else if (apAlByPoint.isLowFaultActive()) {
                        if (apAlByPoint.isLowFaultActive() && apAlByPoint.isHighFaultActive()) {
                            if (apAlByPoint.getLowAlert() > apAlByPoint.getLowFault() && apAlByPoint.getLowAlert() < apAlByPoint.getHighFault()) {
                                chartInfoMap.get(id).setLowAlertValidationFailed(false);
                            } else {
                                chartInfoMap.get(id).setLowAlertValidationFailed(true);
                            }
                        } else {
                            if (apAlByPoint.getLowAlert() > apAlByPoint.getLowFault()) {
                                chartInfoMap.get(id).setLowAlertValidationFailed(false);
                            } else {
                                chartInfoMap.get(id).setLowAlertValidationFailed(true);
                            }
                        }
                    } else if (apAlByPoint.isHighAlertActive()) {
                        if (apAlByPoint.getLowAlert() < apAlByPoint.getHighAlert()) {
                            chartInfoMap.get(id).setLowAlertValidationFailed(false);
                        } else {
                            chartInfoMap.get(id).setLowAlertValidationFailed(true);
                        }
                    } else if (apAlByPoint.isHighFaultActive()) {
                        if (apAlByPoint.getLowAlert() < apAlByPoint.getHighFault()) {
                            chartInfoMap.get(id).setLowAlertValidationFailed(false);
                        } else {
                            chartInfoMap.get(id).setLowAlertValidationFailed(true);
                        }
                    } else {
                        chartInfoMap.get(id).setLowAlertValidationFailed(false);
                    }
                } else {
                    chartInfoMap.get(id).setLowAlertValidationFailed(false);
                }

                // Validates the highAlert field of the given Object
                if (apAlByPoint.isHighAlertActive()) {
                    if (apAlByPoint.getHighAlert() <= 0) {
                        chartInfoMap.get(id).setHighAlertValidationFailed(true);
                    } else if (apAlByPoint.isLowAlertActive() && apAlByPoint.isHighFaultActive()) {
                        if (apAlByPoint.getHighAlert() > apAlByPoint.getLowAlert() && apAlByPoint.getHighAlert() < apAlByPoint.getHighFault()) {
                            chartInfoMap.get(id).setHighAlertValidationFailed(false);
                        } else {
                            chartInfoMap.get(id).setHighAlertValidationFailed(true);
                        }
                    } else if (apAlByPoint.isHighFaultActive()) {
                        if (apAlByPoint.isLowFaultActive() && apAlByPoint.isHighFaultActive()) {
                            if (apAlByPoint.getHighAlert() > apAlByPoint.getLowFault() && apAlByPoint.getHighAlert() < apAlByPoint.getHighFault()) {
                                chartInfoMap.get(id).setHighAlertValidationFailed(false);
                            } else {
                                chartInfoMap.get(id).setHighAlertValidationFailed(true);
                            }
                        } else {
                            if (apAlByPoint.getHighAlert() < apAlByPoint.getHighFault()) {
                                chartInfoMap.get(id).setHighAlertValidationFailed(false);
                            } else {
                                chartInfoMap.get(id).setHighAlertValidationFailed(true);
                            }
                        }
                    } else if (apAlByPoint.isLowAlertActive()) {
                        if (apAlByPoint.getHighAlert() > apAlByPoint.getLowAlert()) {
                            chartInfoMap.get(id).setHighAlertValidationFailed(false);
                        } else {
                            chartInfoMap.get(id).setHighAlertValidationFailed(true);
                        }
                    } else if (apAlByPoint.isLowFaultActive()) {
                        if (apAlByPoint.getHighAlert() > apAlByPoint.getLowFault()) {
                            chartInfoMap.get(id).setHighAlertValidationFailed(false);
                        } else {
                            chartInfoMap.get(id).setHighAlertValidationFailed(true);
                        }
                    } else {
                        chartInfoMap.get(id).setHighAlertValidationFailed(false);
                    }
                } else {
                    chartInfoMap.get(id).setHighAlertValidationFailed(false);
                }

                // Validates the highFault field of the given Object
                if (apAlByPoint.isHighFaultActive()) {
                    if (apAlByPoint.getHighFault() <= 0) {
                        chartInfoMap.get(id).setHighFaultValidationFailed(true);
                    } else if (apAlByPoint.isHighAlertActive()) {
                        if (apAlByPoint.getHighFault() > apAlByPoint.getHighAlert()) {
                            chartInfoMap.get(id).setHighFaultValidationFailed(false);
                        } else {
                            chartInfoMap.get(id).setHighFaultValidationFailed(true);
                        }
                    } else if (apAlByPoint.isLowAlertActive()) {
                        if (apAlByPoint.getHighFault() > apAlByPoint.getLowAlert()) {
                            chartInfoMap.get(id).setHighFaultValidationFailed(false);
                        } else {
                            chartInfoMap.get(id).setHighFaultValidationFailed(true);
                        }
                    } else if (apAlByPoint.isLowFaultActive()) {
                        if (apAlByPoint.getHighFault() > apAlByPoint.getLowFault()) {
                            chartInfoMap.get(id).setHighFaultValidationFailed(false);
                        } else {
                            chartInfoMap.get(id).setHighFaultValidationFailed(true);
                        }
                    } else {
                        chartInfoMap.get(id).setHighFaultValidationFailed(false);
                    }
                } else {
                    chartInfoMap.get(id).setHighFaultValidationFailed(false);
                }
            }
        }
    }

    /**
     * Calls when selecting the different params from param dropdown.
     */
    public void onParamChange() {
        List<TrendValuesVO> trendDataList;
        ChartInfoVO ciVo;
        String id;

        try {
            chartOptionsJsList.clear();
            long start = System.currentTimeMillis();
            if ((id = getCurrentWindowId()) != null) {
                Logger.getLogger(AssetAnalysisWindowBean.class.getName()).log(Level.INFO, " windowId:{0}", new Object[]{(windowId = id)});

                ciVo = chartInfoMap.get(windowId);
                Logger.getLogger(AssetAnalysisWindowBean.class.getName()).log(Level.INFO, "paramName: {0}", ciVo.getParamName());
                if (ciVo.getParamName().equals("All")) {
                    ciVo.setCount(ciVo.getCount() - 3);
                    for (String selectedParam : ciVo.getParamNameList()) {
                        if (ciVo.getPointType().equalsIgnoreCase("AC")) {
                            trendDataList = TrendDataClient.getInstance().getTrendsByCustomerSiteAreaAssetPointLocationPointApSetParam(ServiceRegistrarClient.getInstance().getServiceHostURL(WAVEFORM_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), ciVo.getSiteId(), ciVo.getAreaId(), ciVo.getAssetId(), ciVo.getPointLocationId(), ciVo.getPointId(), ciVo.getSelectedApAlByPoint().getApSetId(), selectedParam);
                        } else {
                            trendDataList = TrendDataClient.getInstance().getTrendsByCustomerSiteAreaAssetPointLocationPointApSet(ServiceRegistrarClient.getInstance().getServiceHostURL(WAVEFORM_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), ciVo.getSiteId(), ciVo.getAreaId(), ciVo.getAssetId(), ciVo.getPointLocationId(), ciVo.getPointId(), ciVo.getSelectedApAlByPoint().getApSetId());
                        }
                        System.out.println(selectedParam + ", DB Query Time: " + (System.currentTimeMillis() - start));
                        if (generateGraph = !(trendDataList == null || trendDataList.isEmpty())) {
                            ciVo.setSelectedApAlByPoint(ciVo.getApAlByPointList().stream().filter(p -> Objects.equals(selectedParam, p.getParamName())).findAny().get());
                            configureHighChart(trendDataList, windowId, false);
                            chartOptionsJsList.add(chartOptionsJs);
                        }
                        System.out.println(selectedParam + ", UI processing : " + (System.currentTimeMillis() - start));
                    }
                    System.out.println("DB query time for All params :" + (System.currentTimeMillis() - start));
                    PrimeFaces.current().ajax().update("javaScriptArea");
                    System.out.println("UI update time:" + (System.currentTimeMillis() - start));
                } else {
                    generateGraph = true;

                    ciVo.setCount(ciVo.getCount() - 1);
                    if (ciVo.getPointType().equalsIgnoreCase("AC")) {
                        trendDataList = TrendDataClient.getInstance().getTrendsByCustomerSiteAreaAssetPointLocationPointApSetParam(ServiceRegistrarClient.getInstance().getServiceHostURL(WAVEFORM_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), ciVo.getSiteId(), ciVo.getAreaId(), ciVo.getAssetId(), ciVo.getPointLocationId(), ciVo.getPointId(), ciVo.getSelectedApAlByPoint().getApSetId(), ciVo.getParamName());
                    } else {
                        trendDataList = TrendDataClient.getInstance().getTrendsByCustomerSiteAreaAssetPointLocationPointApSet(ServiceRegistrarClient.getInstance().getServiceHostURL(WAVEFORM_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), ciVo.getSiteId(), ciVo.getAreaId(), ciVo.getAssetId(), ciVo.getPointLocationId(), ciVo.getPointId(), ciVo.getSelectedApAlByPoint().getApSetId());
                    }
                    System.out.println(ciVo.getParamName() + ", db query time: " + (System.currentTimeMillis() - start));                    
                    if (trendDataList != null && !trendDataList.isEmpty()) {
                        ciVo.setSelectedApAlByPoint(ciVo.getApAlByPointList().stream().filter(p -> Objects.equals(ciVo.getParamName(), p.getParamName())).findAny().get());
                        configureHighChart(trendDataList, windowId, false);
                        chartOptionsJsList.add(chartOptionsJs);
                        System.out.println(ciVo.getParamName() + ", UI processing 1 : " + (System.currentTimeMillis() - start));

                        if (ciVo.isRefreshWaveAndSpectrum()) {
                            displayAssociatedWaveAndSpectrum(false);
                            System.out.println(ciVo.getParamName() + ", UI processing 2 : " + (System.currentTimeMillis() - start));
                            PrimeFaces.current().executeScript("showTrendGraphActionBarPanel()");
                            System.out.println(ciVo.getParamName() + ", UI processing 3 : " + (System.currentTimeMillis() - start));
                            PrimeFaces.current().ajax().update("graphDataWaveForm");
                            System.out.println(ciVo.getParamName() + ", UI processing 4 : " + (System.currentTimeMillis() - start));
                            PrimeFaces.current().ajax().update("graphDataSpecForm");
                        }
                        ciVo.setRefreshWaveAndSpectrum(false);
                        System.out.println(ciVo.getParamName() + ", UI processing 5 : " + (System.currentTimeMillis() - start));
                        PrimeFaces.current().ajax().update("javaScriptArea");
                        System.out.println("UI update time:" + (System.currentTimeMillis() - start));
                    } else {
                        ciVo.setSelectedApAlByPoint(ciVo.getApAlByPointList().stream().filter(p -> Objects.equals(ciVo.getParamName(), p.getParamName())).findAny().get());
                        ciVo.setRefreshWaveAndSpectrum(true);

                        chartOptionsJs = "{}";
                        waveFormChartOptionsJs = "{}";
                        spectrumChartOptionsJs = "{}";
                        chartOptionsJsList.add(chartOptionsJs);

                        PrimeFaces.current().executeScript("hideTrendGraphActionBarPanel()");
                        PrimeFaces.current().ajax().update("javaScriptPoint1");
                        PrimeFaces.current().ajax().update("javaScriptPoint2");
                        PrimeFaces.current().ajax().update("javaScriptArea");
                        PrimeFaces.current().ajax().update("graphDataWaveForm");
                        PrimeFaces.current().ajax().update("graphDataSpecForm");

                        setNonAutoGrowlMsg("label_no_graph_data_for_ap");
                        PrimeFaces.current().ajax().update("nonAutoGrowlId");
                    }

                }
            }
        } catch (Exception ex) {
            Logger.getLogger(AssetAnalysisWindowBean.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

    /**
     * Calls when selecting the different apSet from apSet dropdown.
     */
    public void onApSetChange() {
        ChartInfoVO ciVo;
        String id;

        try {
            if ((id = getCurrentWindowId()) != null) {
                Logger.getLogger(AssetAnalysisWindowBean.class.getName()).log(Level.INFO, " windowId:{0}", new Object[]{(windowId = id)});

                ciVo = chartInfoMap.get(windowId);
                ciVo.setApSetId(ciVo.getApSetItem().getValue());
                ciVo.setParamNameList(ciVo.getApAlByPointList().stream().filter(apAlByPoint -> apAlByPoint.getApSetId().equals(ciVo.getApSetId())).map(PointVO::getParamName).collect(Collectors.toList()));
                ciVo.setParamName(ciVo.getParamNameList().get(0));
                ciVo.setGraphTimestamp(null);
                if (ciVo.getPointType().equalsIgnoreCase("AC")) {
                    ciVo.setRefreshWaveAndSpectrum(true);
                }
                for (PointVO pvo : ciVo.getApAlByPointList()) {
                    if (pvo.getApSetId().equals(ciVo.getApSetId())) {
                        if (Objects.equals("Temperature", pvo.getSensorType())) {
                            if (Objects.equals("AUTO_TEMP", ciVo.getParamName()) || Objects.equals("Temperature", ciVo.getParamName())) {
                                ciVo.setSelectedApAlByPoint(pvo);
                                break;
                            }
                        } else {
                            if (Objects.equals(pvo.getParamName(), ciVo.getParamName())) {
                                ciVo.setSelectedApAlByPoint(pvo);
                                break;
                            }
                        }
                    }
                }
                onParamChange();
            }
        } catch (Exception ex) {
            Logger.getLogger(AssetAnalysisWindowBean.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

    /**
     * Calls when select 'Plot Display Options' from context menu of graph.
     */
    public void loadPlotDataOptions() {
        ChartInfoVO ciVo;
        String id;

        try {
            if ((id = getCurrentWindowId()) != null) {
                Logger.getLogger(AssetAnalysisWindowBean.class.getName()).log(Level.INFO, " windowId:{0}", new Object[]{(windowId = id)});

                ciVo = chartInfoMap.get(windowId);
                ciVo.setPlotOptionsModified(false);
                if (Objects.equals(ciVo.getGraphType(), "wave")) {
                    ciVo.setTimeUnit(ciVo.getWaveGraphData().getSelectedTimeUnit());
                    ciVo.setAmplitudeUnit(ciVo.getWaveGraphData().getSelectedAmplitudeUnit());
                } else if (Objects.equals(ciVo.getGraphType(), "spectrum")) {
                    ciVo.setFrequencyUnit(ciVo.getSpectrumGraphData().getSelectedFrequencyUnit());
                    ciVo.setAmplitudeUnit(ciVo.getSpectrumGraphData().getSelectedAmplitudeUnit());
                    ciVo.setAmpFactor(ciVo.getSpectrumGraphData().getSelectedAmpFactor());
                }
                navigationBean.updateDialog("plot-display-options");
            }
        } catch (Exception e) {
            Logger.getLogger(AssetAnalysisWindowBean.class.getName()).log(Level.INFO, e.getMessage(), e);
        }
    }

    /**
     * Calls when saving the plot options
     */
    public void savePlotDataOptions(boolean resetZoom) {
        List<FaultFrequenciesVO> selectedFrequencies;
        GraphDataVO graphDataVO;
        WaveDataVO waveDataVO;
        float[] samplesArray, spectrumArray;
        List<Float> samples;
        ChartInfoVO ciVo;
        String id, yAxisUnit, samplesStr, oldTime, plotLines, oldFreq;
        float timeDelta, refMarkerMultiple, frequencyDelta, tachSpeed;

        try {
            if ((id = getCurrentWindowId()) != null) {
                Logger.getLogger(AssetAnalysisWindowBean.class.getName()).log(Level.INFO, " windowId:{0}", new Object[]{(windowId = id)});

                ciVo = chartInfoMap.get(windowId);
                if ((waveDataVO = ciVo.getWaveDataVO()) != null && waveDataVO.getWave() != null) {
                    yAxisUnit = waveDataVO.getWave().getSensitivityUnits();
                    if ((samples = waveDataVO.getWave().getSamples()) != null) {
                        samplesArray = getFloatArrayFromSampleList(samples);

                        // If the graph type is WaveForm
                        if (Objects.equals(ciVo.getGraphType(), "wave")) {
                            graphDataVO = ciVo.getWaveGraphData();
                            oldTime = graphDataVO.getSelectedTimeUnit();
                            graphDataVO.setSelectedTimeUnit(ciVo.getTimeUnit());
                            graphDataVO.setSelectedAmplitudeUnit(ciVo.getAmplitudeUnit());
                            timeDelta = calculateDeltaTime(graphDataVO.getSelectedTimeUnit(), samplesArray.length, ciVo.getSelectedApAlByPoint().getPeriod());
                            samplesStr = getUnitConvertedSamplesAsString(samplesArray, yAxisUnit, timeDelta, graphDataVO);
                            if (unitConvertedSamplesArray != null && unitConvertedSamplesArray.length > 0) {
                                graphDataVO.setRmsWave(Float.valueOf(DECIMAL_FORMAT.format(RMS.calculate(unitConvertedSamplesArray))));
                            }
                            plotLines = "[]";
                            refMarkerMultiple = convertTime(oldTime, ciVo.getTimeUnit());
                            
                            PrimeFaces.current().executeScript("updateChartAxis('wave', " + samplesStr + " , '" + graphDataVO.getSelectedTimeUnit() + "' , '" + graphDataVO.getSelectedAmplitudeUnit() + "' , " + plotLines + ", " + graphDataVO.getRmsWave() + " , " + refMarkerMultiple + " , '' , " + resetZoom + " )");
                            PrimeFaces.current().executeScript("PF('defaultDlg').hide()");
                            PrimeFaces.current().executeScript("removeAutocorrelateTick()");
                        } 

                        // If the Graph type is Spectrum
                        else if (Objects.equals(ciVo.getGraphType(), "spectrum")) {
                            graphDataVO = ciVo.getSpectrumGraphData();
                            oldFreq = graphDataVO.getSelectedFrequencyUnit();
                            graphDataVO.setSelectedFrequencyUnit(ciVo.getFrequencyUnit());
                            graphDataVO.setSelectedAmplitudeUnit(ciVo.getAmplitudeUnit());
                            graphDataVO.setSelectedAmpFactor(ciVo.getAmpFactor());
                            
                            tachSpeed = graphDataVO.getTachSpeed();
                            if (graphDataVO.getTachSpeedUnit().equals("RPM")) {
                                tachSpeed = tachSpeed / 60f;
                            }
                            
                            spectrumArray = Spectrum.getSpectrumAsFloat(samplesArray, waveDataVO.getWave().getSampleRateHz(), tachSpeed);
                            spectrumArray = Util.applyAmpFactor(spectrumArray, SPEC_DEFAULT_AMP_FACTOR, graphDataVO.getSelectedAmpFactor());
                            frequencyDelta = calculateDeltaFrequency(graphDataVO, ciVo.getSelectedApAlByPoint().getResolution(), ciVo.getSelectedApAlByPoint().getfMax());
                            
                            if (ciVo.getSelectedApAlByPoint().getSensorType().equalsIgnoreCase("acceleration")) {
                                samplesStr = getSpectralUnitConvertedSamplesAsString(spectrumArray, yAxisUnit, frequencyDelta, graphDataVO, ciVo);
                            } else {
                                samplesStr = getUnitConvertedSamplesAsString(spectrumArray, yAxisUnit, frequencyDelta, graphDataVO);
                            }
                            selectedFrequencies = new ArrayList();
                            if (graphDataVO.getSelectedFfIds() != null) {
                                graphDataVO.getSelectedFfIds().stream().forEachOrdered(ffId -> selectedFrequencies.add(graphDataVO.getFrequencyList().stream().filter(f -> Objects.equals(f.getFfId(), ffId)).findAny().get()));
                            }
                            if (unitConvertedSamplesArray != null && unitConvertedSamplesArray.length > 0) {
                                graphDataVO.setTotalAmplSpec(Float.valueOf(DECIMAL_FORMAT.format(TotalSpectralEnergy.calculate(unitConvertedSamplesArray))));
                            }
                            plotLines = getFrequecyPlotLines(selectedFrequencies, graphDataVO, waveDataVO.getWave().getSampleRateHz() / 2.56f);
                            refMarkerMultiple = convertFrequency(oldFreq, graphDataVO.getSelectedFrequencyUnit(), tachSpeed);

                            PrimeFaces.current().executeScript("updateChartAxis('spectrum', " + samplesStr + " , '" + getParamUnitLabelByValue(graphDataVO.getSelectedFrequencyUnit()) + "' , '" + graphDataVO.getSelectedAmplitudeUnit() + " - " + getAmpFactorLabelByValue(graphDataVO.getSelectedAmpFactor()) + "' , " + plotLines + ", " + graphDataVO.getTotalAmplSpec() + " , " + refMarkerMultiple + " , " + ciVo.isSpectrumLogYAxis() + ", " + resetZoom + " )");
                            PrimeFaces.current().executeScript("PF('defaultDlg').hide()");
                        }

                    }
                }
            }
        } catch (Exception e) {
            Logger.getLogger(AssetAnalysisWindowBean.class.getName()).log(Level.INFO, e.getMessage(), e);
        }
    }

    /**
     * Converts time from one unit to another.
     *
     * @param oldTime
     * @param newTime
     * @return float
     */
    public float convertTime(String oldTime, String newTime) {
        float timeFactor = 1.00f;

        if (Objects.equals(oldTime, newTime)) {
            return timeFactor;
        }

        switch (oldTime) {
            case "s":
                switch (newTime) {
                    case "ms":
                        timeFactor = timeFactor * 1000;
                        break;
                    case "min":
                        timeFactor = timeFactor / 60;
                        break;
                    default:
                        throw new AssertionError();
                }
                break;
            case "ms":
                switch (newTime) {
                    case "s":
                        timeFactor = timeFactor / 1000;
                        break;
                    case "min":
                        timeFactor = (timeFactor / 1000) / 60;
                        break;
                    default:
                        throw new AssertionError();
                }
                break;
            case "min":
                switch (newTime) {
                    case "s":
                        timeFactor = timeFactor * 60;
                        break;
                    case "ms":
                        timeFactor = timeFactor * 60 * 1000;
                        break;
                    default:
                        throw new AssertionError();
                }
                break;
            default:
                throw new AssertionError();
        }

        return timeFactor;
    }

    /**
     * Converts frequency from one unit to another.
     *
     * @param oldFreq
     * @param newFreq
     * @param tachSpeed
     * @return float
     */
    public float convertFrequency(String oldFreq, String newFreq, float tachSpeed) {
        float freqFactor = 1.00f;

        if (Objects.equals(oldFreq, newFreq)) {
            return freqFactor;
        }

        switch (oldFreq) {
            case "Hz":
                switch (newFreq) {
                    case "RPM":
                        freqFactor = freqFactor * 60;
                        break;
                    case "Orders":
                        freqFactor = freqFactor / tachSpeed;
                        break;
                    default:
                        throw new AssertionError();
                }
                break;
            case "RPM":
                switch (newFreq) {
                    case "Hz":
                        freqFactor = freqFactor / 60;
                        break;
                    case "Orders":
                        freqFactor = (freqFactor / 60) / tachSpeed;
                        break;
                    default:
                        throw new AssertionError();
                }
                break;
            case "Orders":
                switch (newFreq) {
                    case "Hz":
                        freqFactor = freqFactor * tachSpeed;
                        break;
                    case "RPM":
                        freqFactor = freqFactor * tachSpeed * 60;
                        break;
                    default:
                        throw new AssertionError();
                }
                break;
            default:
                throw new AssertionError();
        }

        return freqFactor;
    }

    /**
     * @param timeUnit, xAxis unit
     * @param noOfRecords, total data points
     * @param xAxisLength, length of xAxis in time.
     * @return
     */
    public float calculateDeltaTime(String timeUnit, int noOfRecords, float xAxisLength) {
        float deltaTime = 0;

        switch (timeUnit) {
            case "s":
                deltaTime = xAxisLength / noOfRecords;
                break;
            case "ms":
                deltaTime = (xAxisLength / noOfRecords) * 1000;
                break;
            case "min":
                deltaTime = (xAxisLength / noOfRecords) / 60;
                break;
            default:
                throw new AssertionError();
        }

        return deltaTime;
    }

    /**
     * @param specGdVo, GraphDataVO object
     * @param noOfRecords, total data points
     * @param xAxisLength, length of xAxis in frequency.
     * @return
     */
    public float calculateDeltaFrequency(GraphDataVO specGdVo, int noOfRecords, float xAxisLength) {
        float deltaFreq = 0;
        float tachSpeed = specGdVo.getTachSpeed();
        if (specGdVo.getTachSpeedUnit().equals("RPM")) {
            tachSpeed = tachSpeed / 60f;
        }
        switch (specGdVo.getSelectedFrequencyUnit()) {
            case "Hz":
                deltaFreq = xAxisLength / noOfRecords;
                break;
            case "RPM":
                deltaFreq = (xAxisLength / noOfRecords) * 60;
                break;
            case "Orders":
                deltaFreq = (xAxisLength / noOfRecords) / tachSpeed;
                break;
            default:
                throw new AssertionError();
        }

        return deltaFreq;
    }

    /**
     * @param sampleArray, input samples
     * @param yAxisUnit, yAxis unit
     * @param timeDelta, interval
     * @param gdVo
     * @return
     * @throws us.uptime.processing.algorithms.exceptions.UnitConversionException
     */
    public String getUnitConvertedSamplesAsString(float[] sampleArray, String yAxisUnit, float timeDelta, GraphDataVO gdVo) throws UnitConversionException {
        ArrayList al, tmp;
        int i;

        i = 0;
        al = new ArrayList();
        unitConvertedSamplesArray = new float[sampleArray.length];
        for (float sample : sampleArray) {
            sample = UnitsConversion.convert(sample, yAxisUnit, gdVo.getSelectedAmplitudeUnit());
            unitConvertedSamplesArray[i] = sample;
            tmp = new ArrayList();
            tmp.add(new BigDecimal(timeDelta * i++).setScale(8, RoundingMode.HALF_UP).stripTrailingZeros());
            tmp.add(sample);
            al.add(tmp);
        }

        return al.toString();
    }

    /**
     * @param samplesArray, input samples
     * @param yAxisUnit, yAxis unit
     * @param frequencyDelta, interval
     * @param gdVo
     * @param ciVo
     * @return
     * @throws us.uptime.processing.algorithms.exceptions.UnitConversionException
     */
    public String getSpectralUnitConvertedSamplesAsString(float[] samplesArray, String yAxisUnit, float frequencyDelta, GraphDataVO gdVo, ChartInfoVO ciVo) throws UnitConversionException {
        float[] frequenciesHz, result;
        float deltaFreqHz;

        deltaFreqHz = (float) ciVo.getSelectedApAlByPoint().getfMax() / (float) ciVo.getSelectedApAlByPoint().getResolution();
        frequenciesHz = new float[samplesArray.length];
        for (int i = 0; i < frequenciesHz.length; i++) {
            frequenciesHz[i] = deltaFreqHz * i;
        }
        result = UnitsConversion.convertSpectral(samplesArray, frequenciesHz, yAxisUnit, gdVo.getSelectedAmplitudeUnit());
        unitConvertedSamplesArray = result;
        return AssetAnalysisWindowBean.this.getSamplesAsString(result, frequencyDelta, gdVo);
    }

    /**
     * @param sampleArray, input samples
     * @param delta, interval
     * @param gdVo
     * @return
     * @throws us.uptime.processing.algorithms.exceptions.UnitConversionException
     */
    public String getSamplesAsString(float[] sampleArray, float delta, GraphDataVO gdVo) throws UnitConversionException {
        ArrayList al, tmp;
        int i;

        i = 0;
        al = new ArrayList();
        for (float sample : sampleArray) {
            tmp = new ArrayList();
            tmp.add(new BigDecimal(delta * i++).setScale(8, RoundingMode.HALF_UP).stripTrailingZeros());
            tmp.add(sample);
            al.add(tmp);
        }

        return al.toString();
    }

    /**
     * @param sampleArray, input samples
     * @param delta, interval
     * @param gdVo
     * @return
     * @throws us.uptime.processing.algorithms.exceptions.UnitConversionException
     */
    public String getSamplesAsString(double[] sampleArray, float delta, GraphDataVO gdVo) throws UnitConversionException {
        ArrayList al, tmp;
        int i;

        i = 0;
        al = new ArrayList();
        unitConvertedSamplesArray = new float[sampleArray.length];
        for (double sample : sampleArray) {
            unitConvertedSamplesArray[i] = (float) sample;
            tmp = new ArrayList();
            tmp.add(new BigDecimal(delta * i++).setScale(8, RoundingMode.HALF_UP).stripTrailingZeros());
            tmp.add(sample);
            al.add(tmp);
        }

        return al.toString();
    }

    /**
     * Converts list of samples to float array of samples
     *
     * @param samples, input samples
     * @return
     */
    public float[] getFloatArrayFromSampleList(List<Float> samples) {
        float[] samplesArray;
        int i;

        i = 0;
        samplesArray = new float[samples.size()];
        for (Float f : samples) {
            samplesArray[i++] = f;
        }

        return samplesArray;
    }

    /**
     * Converts list of samples to float array of samples
     *
     * @param samples, input samples
     * @return
     */
    public double[] getDoubleArrayFromSampleList(List<Float> samples) {
        int i = 0;
        double[] samplesArray = new double[samples.size()];
        for (double f : samples) {
            samplesArray[i++] = f;
        }

        return samplesArray;
    }

    /**
     * Calls when select the FF set from graph UI.
     *
     */
    public void onFFSetSelect() {
        Map<String, String> ffMarkerMap;
        List<FaultFrequenciesVO> ffItemList;
        GraphDataVO spectrumGraphData;
        ChartInfoVO ciVo;
        String id;
        int count;

        if ((id = getCurrentWindowId()) != null) {
            windowId = id;

            if ((ciVo = chartInfoMap.get(windowId)) != null) {
                spectrumGraphData = ciVo.getSpectrumGraphData();
                spectrumGraphData.setSelectedFfIds(null);
                spectrumGraphData.setEndingHarmonic(null);
                spectrumGraphData.setStartingHarmonic(null);

                getFfVoList();

                ffItemList = spectrumGraphData.getFrequencyList();
                if (!ffItemList.isEmpty()) {
                    ffMarkerMap = new HashMap();
                    count = 0;

                    for (FaultFrequenciesVO f : ffItemList) {
                        if (count < 12) {
                            ffMarkerMap.put(f.getFfName(), markersPatternList.get(count++));
                        } else {
                            ffMarkerMap.put(f.getFfName(), markersPatternList.get(12));
                        }
                    }
                    spectrumGraphData.setFfMarkerMap(ffMarkerMap);
                }

                PrimeFaces.current().ajax().update("graphDataSpecForm:ffIdCheckboxSpec");
                PrimeFaces.current().executeScript("addLabelForFF('spectrum')");
            }
        }
    }

    private String getCurrentWindowId() {
        String id;

        try {
            /*if ((id = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("hiddenForm1:windowUrlId")) != null) {
                Logger.getLogger(AssetAnalysisWindowBean.class.getName()).log(Level.INFO, "******************************************hiddenForm1:windowUrlId :{0}", id);
                return id;
            } else if ((id = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("paramFormId:windowUrlId")) != null) {
                Logger.getLogger(AssetAnalysisWindowBean.class.getName()).log(Level.INFO, "******************************************paramFormId:windowUrlId :{0}", id);
                return id;
            } else if ((id = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("actionTopBarFormId:windowUrlId")) != null) {
                Logger.getLogger(AssetAnalysisWindowBean.class.getName()).log(Level.INFO, "******************************************actionTopBarFormId:windowUrlId :{0}", id);
                return id;
            } else if ((id = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("trendGraphActionBarFormId:windowUrlId")) != null) {
                Logger.getLogger(AssetAnalysisWindowBean.class.getName()).log(Level.INFO, "******************************************trendGraphActionBarFormId:windowUrlId :{0}", id);
                return id;
            } else if ((id = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("editAlarmLimitFormId:windowUrlId")) != null) {
                Logger.getLogger(AssetAnalysisWindowBean.class.getName()).log(Level.INFO, "******************************************editAlarmLimitFormId:windowUrlId :{0}", id);
                return id;
            } else if ((id = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("createWorkOrderAnalysisFormId:windowUrlId")) != null) {
                Logger.getLogger(AssetAnalysisWindowBean.class.getName()).log(Level.INFO, "******************************************createWorkOrderAnalysisFormId:windowUrlId :{0}", id);
                return id;
            } else if ((id = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("graphDataSpecForm:windowUrlId")) != null) {
                Logger.getLogger(AssetAnalysisWindowBean.class.getName()).log(Level.INFO, "******************************************graphDataSpecForm:windowUrlId :{0}", id);
                return id;
            } else if ((id = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("graphDataWaveForm:windowUrlId")) != null) {
                Logger.getLogger(AssetAnalysisWindowBean.class.getName()).log(Level.INFO, "******************************************graphDataWaveForm:windowUrlId :{0}", id);
                return id;
            } else if ((id = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("plotDataOptionsFormId:windowUrlId")) != null) {
                Logger.getLogger(AssetAnalysisWindowBean.class.getName()).log(Level.INFO, "******************************************plotDataOptionsFormId:windowUrlId :{0}", id);
                return id;
            } else if ((id = windowId) != null) {
                Logger.getLogger(AssetAnalysisWindowBean.class.getName()).log(Level.INFO, "******************************************windowId :{0}", id);
                return id;
            } else {
                Logger.getLogger(AssetAnalysisWindowBean.class.getName()).log(Level.INFO, "******************************************id:null");
            }*/

            if (((id = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("hiddenForm1:windowUrlId")) != null
                    || (id = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("paramFormId:windowUrlId")) != null
                    || (id = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("actionTopBarFormId:windowUrlId")) != null
                    || (id = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("trendGraphActionBarFormId:windowUrlId")) != null
                    || (id = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("editAlarmLimitFormId:windowUrlId")) != null
                    || (id = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("createWorkOrderAnalysisFormId:windowUrlId")) != null
                    || (id = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("graphDataSpecForm:windowUrlId")) != null
                    || (id = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("graphDataWaveForm:windowUrlId")) != null
                    || (id = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("plotDataOptionsFormId:windowUrlId")) != null
                    || (id = windowId) != null) && !id.isEmpty()) {
                return id;
            }
        } catch (Exception e) {
            Logger.getLogger(AssetAnalysisWindowBean.class.getName()).log(Level.INFO, e.getMessage(), e);
        }
        return null;
    }

    public void toggleCreateWorkOrder() {
        ChartInfoVO chartInfoVO;
        String id;

        try {
            if ((id = getCurrentWindowId()) != null) {
                chartInfoVO = chartInfoMap.get(id);
                if (userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null) {
                    chartInfoVO.setWorkOrderVO(new WorkOrderVO());
                    chartInfoVO.getWorkOrderVO().setCustomerAccount(userManageBean.getCurrentSite().getCustomerAccount());
                    chartInfoVO.getWorkOrderVO().setUser(userManageBean.getUserName());
                    chartInfoVO.getWorkOrderVO().setSiteId(chartInfoVO.getSiteId());
                    chartInfoVO.getWorkOrderVO().setSiteName(chartInfoVO.getSiteName());
                    chartInfoVO.getWorkOrderVO().setAreaName(chartInfoVO.getAreaName());
                    chartInfoVO.getWorkOrderVO().setAreaId(chartInfoVO.getAreaId());
                    chartInfoVO.getWorkOrderVO().setAssetName(chartInfoVO.getAssetName());
                    chartInfoVO.getWorkOrderVO().setPointLocationName(chartInfoVO.getPointLocationName());
                    chartInfoVO.getWorkOrderVO().setPointName(chartInfoVO.getPointName());
                    chartInfoVO.getWorkOrderVO().setAssetId(chartInfoVO.getAssetId());
                    chartInfoVO.getWorkOrderVO().setChannelType(chartInfoVO.getPointType());
                    chartInfoVO.getWorkOrderVO().setPointId(chartInfoVO.getPointId());
                    chartInfoVO.getWorkOrderVO().setPointLocationId(chartInfoVO.getPointLocationId());
                    chartInfoVO.getWorkOrderVO().setApSetId(chartInfoVO.getSelectedApAlByPoint().getApSetId());
                    chartInfoVO.getWorkOrderVO().setTrendDetails(new ArrayList());
                    chartInfoVO.setWorkOrderSelectedParamList(new ArrayList());
                } else {
                    chartInfoVO.setWorkOrderVO(new WorkOrderVO());
                    chartInfoVO.setWorkOrderSelectedParamList(new ArrayList());
                }
            }
        } catch (Exception e) {
            Logger.getLogger(AssetAnalysisWindowBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Save the given work order
     */
    public void submitWorkOrder() {
        ChartInfoVO chartInfoVO;
        WorkOrderVO workOrderVO;
        String id;

        try {
            if ((id = getCurrentWindowId()) != null) {
                chartInfoVO = chartInfoMap.get(id);
                if ((workOrderVO = chartInfoVO.getWorkOrderVO()) != null && workOrderVO.getDescription() != null && !workOrderVO.getDescription().isEmpty()
                        && workOrderVO.getPriority() > 0 && workOrderVO.getIssue() != null && !workOrderVO.getIssue().isEmpty()
                        && workOrderVO.getAction() != null && !workOrderVO.getAction().isEmpty()) {

                    // Set WorkOrder Values
                    if (chartInfoMap.get(id).getWorkOrderSelectedParamList() != null && chartInfoMap.get(id).getWorkOrderSelectedParamList().size() > 0) {
                        chartInfoMap.get(id).getWorkOrderSelectedParamList().stream().forEachOrdered(param -> {
                            TrendDetailsVO trendDetailsVO = new TrendDetailsVO();
                            trendDetailsVO.setApSetId(workOrderVO.getApSetId());
                            trendDetailsVO.setChannelType(workOrderVO.getChannelType());
                            trendDetailsVO.setParamName(param);
                            trendDetailsVO.setPointId(workOrderVO.getPointId());
                            trendDetailsVO.setPointName(workOrderVO.getPointName());
                            trendDetailsVO.setPointLocationId(workOrderVO.getPointLocationId());
                            trendDetailsVO.setPointLocationName(workOrderVO.getPointLocationName());
                            workOrderVO.getTrendDetails().add(trendDetailsVO);
                        });
                    }

                    if (WorkOrderClient.getInstance().createOpenWorkOrder(ServiceRegistrarClient.getInstance().getServiceHostURL(WORK_ORDER_SERVICE_NAME), workOrderVO)) {
                        try {
                            GenericMessage.setNonAutoGrowlMsg("growl_create_work_order");
                            chartInfoVO.setWorkOrderVO(new WorkOrderVO());
                            chartInfoVO.getWorkOrderVO().setCustomerAccount(userManageBean.getCurrentSite().getCustomerAccount());
                            chartInfoVO.getWorkOrderVO().setUser(userManageBean.getUserName());
                            chartInfoVO.getWorkOrderVO().setSiteId(chartInfoVO.getSiteId());
                            chartInfoVO.getWorkOrderVO().setSiteName(chartInfoVO.getSiteName());
                            chartInfoVO.getWorkOrderVO().setAreaName(chartInfoVO.getAreaName());
                            chartInfoVO.getWorkOrderVO().setAreaId(chartInfoVO.getAreaId());
                            chartInfoVO.getWorkOrderVO().setAssetName(chartInfoVO.getAssetName());
                            chartInfoVO.getWorkOrderVO().setPointLocationName(chartInfoVO.getPointLocationName());
                            chartInfoVO.getWorkOrderVO().setPointName(chartInfoVO.getPointName());
                            chartInfoVO.getWorkOrderVO().setAssetId(chartInfoVO.getAssetId());
                            chartInfoVO.getWorkOrderVO().setChannelType(chartInfoVO.getPointType());
                            chartInfoVO.getWorkOrderVO().setPointId(chartInfoVO.getPointId());
                            chartInfoVO.getWorkOrderVO().setPointLocationId(chartInfoVO.getPointLocationId());
                            chartInfoVO.getWorkOrderVO().setApSetId(chartInfoVO.getSelectedApAlByPoint().getApSetId());
                            chartInfoVO.getWorkOrderVO().setTrendDetails(new ArrayList());
                            chartInfoVO.setWorkOrderSelectedParamList(new ArrayList());
                        } catch (Exception e) {
                            Logger.getLogger(CreateWorkOrderBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
                        }
                    }
                } else {
                    GenericMessage.setNonAutoGrowlMsg("label_missing_required_fields");
                }
            }
        } catch (Exception e) {
            Logger.getLogger(AssetAnalysisWindowBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Calls when select the FF from graph UI.
     */
    public void onFFSelect() {
        List<FaultFrequenciesVO> selectedFrequencies;
        GraphDataVO graphDataVO;
        WaveDataVO waveDataVO;
        List<Float> samples = null;
        ChartInfoVO ciVo;
        String plotLines, id;

        try {
            if ((id = getCurrentWindowId()) != null) {
                windowId = id;
                if ((ciVo = chartInfoMap.get(windowId)) != null) {
                    if ((waveDataVO = ciVo.getWaveDataVO()) != null && waveDataVO.getWave() != null) {
                        samples = waveDataVO.getWave().getSamples();
                    }
                    if (waveDataVO != null && waveDataVO.getWave() != null && samples != null) {
                        selectedFrequencies = new ArrayList();
                        graphDataVO = ciVo.getSpectrumGraphData();

                        if (graphDataVO.getSelectedFfIds() != null) {
                            graphDataVO.getSelectedFfIds().stream().forEachOrdered(ffId -> selectedFrequencies.add(graphDataVO.getFrequencyList().stream().filter(f -> Objects.equals(f.getFfId(), ffId)).findAny().get()));
                        }
                        if (graphDataVO.getSelectedFfIds() == null || graphDataVO.getSelectedFfIds().isEmpty()) {
                            graphDataVO.setStartingHarmonic(null);
                            graphDataVO.setEndingHarmonic(null);
                        }

                        plotLines = getFrequecyPlotLines(selectedFrequencies, graphDataVO, waveDataVO.getWave().getSampleRateHz() / 2.56f);
                        PrimeFaces.current().executeScript("setFFMarkers(" + plotLines + ", 1.00)");
                    }
                }
            }
        } catch (Exception e) {
            Logger.getLogger(AssetAnalysisWindowBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Generates the FF marker lines
     *
     * @param gdVo, GraphDataVO object
     * @param xAxisLength, length of xAxis in frequency.
     * @param frequencyList, list of FF.
     * @return
     */
    public String getFrequecyPlotLines(List<FaultFrequenciesVO> frequencyList, GraphDataVO gdVo, float xAxisLength) {
        String ff_unit, pattern, color;
        StringBuilder builder;
        String[] lineStyle;
        int SH, EH;
        float VAL, xAxisLengthTmp;

        float tachSpeed = gdVo.getTachSpeed();
        if (gdVo.getTachSpeedUnit().equals("RPM")) {
            tachSpeed = tachSpeed / 60f;
        }
        builder = new StringBuilder();
        builder.append("[");

        for (FaultFrequenciesVO frequency : frequencyList) {
            VAL = frequency.getFfValue();
            ff_unit = frequency.getFfUnit();
            xAxisLengthTmp = xAxisLength;

            if (Objects.equals("Orders", ff_unit) && Float.compare(tachSpeed, 0.0f) == 0) {
                FacesContext.getCurrentInstance().addMessage("growlMessages", new FacesMessage(userManageBean.getResourceBundleString("growl_ff_can_not_displayed")));
            } else {
                lineStyle = gdVo.getFfMarkerMap().get(frequency.getFfName()).split("-");
                pattern = lineStyle[0];
                color = lineStyle[1];

                if (!Objects.equals(frequency.getFfUnit(), gdVo.getSelectedFrequencyUnit())) {
                    switch (gdVo.getSelectedFrequencyUnit()) {
                        case "Hz":
                            switch (ff_unit) {
                                case "Orders":
                                    VAL = VAL * tachSpeed;
                                    break;
                                case "RPM":
                                    VAL = VAL / 60;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case "Orders":
                            switch (ff_unit) {
                                case "Hz":
                                    VAL = VAL / tachSpeed;
                                    break;
                                case "RPM":
                                    VAL = (VAL / 60) / tachSpeed;
                                    break;
                                default:
                                    break;
                            }
                            xAxisLengthTmp = xAxisLengthTmp / tachSpeed;
                            break;
                        case "RPM":
                            switch (ff_unit) {
                                case "Hz":
                                    VAL = VAL * 60;
                                    break;
                                case "Orders":
                                    VAL = VAL * tachSpeed * 60;
                                    break;
                                default:
                                    break;
                            }
                            xAxisLengthTmp = xAxisLengthTmp * 60;
                            break;
                        default:
                            throw new AssertionError();
                    }
                } else {
                    if (gdVo.getSelectedFrequencyUnit().equals("RPM")) {
                        xAxisLengthTmp = xAxisLengthTmp * 60;
                    } else if (gdVo.getSelectedFrequencyUnit().equals("Orders")) {
                        xAxisLengthTmp = xAxisLengthTmp / tachSpeed;
                    }
                }

                if (gdVo.getStartingHarmonic() != null && gdVo.getEndingHarmonic() != null && gdVo.getStartingHarmonic() < gdVo.getEndingHarmonic()) {
                    SH = gdVo.getStartingHarmonic();
                    EH = gdVo.getEndingHarmonic();
                } else {
                    SH = 1;
                    EH = Math.round(xAxisLengthTmp / VAL) + 1;
                    if (EH > 30) {
                        EH = 30;
                    }
                }

                for (int i = SH; i <= EH; i++) {
                    builder.append("{ \"color\": \"").append(color).append("\", \"width\": 2, \"dashStyle\": \"").append(pattern).append("\", \"value\": ").append(VAL * i).append("},");
                }
            }
        }

        builder.append("]");
        return builder.toString();
    }

    /**
     * Calls when selecting 'Show All Data Points' from trend chart, context menu
     */
    public void showAllTrendPoints() {
        List<TrendValuesVO> trendDataList;
        ArrayList al, tmp;
        int count, selectedDataPointIndex;
        ChartInfoVO ciVo;
        String id;

        try {
            if ((id = getCurrentWindowId()) != null) {
                windowId = id;

                ciVo = chartInfoMap.get(windowId);
                if (Objects.equals("DC", ciVo.getPointType())) {
                    ciVo.setGraphTimestamp(timestamp);
                }

                if (userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null) {
                    if (ciVo.getPointType().equalsIgnoreCase("AC")) {
                        trendDataList = TrendDataClient.getInstance().getTrendsByCustomerSiteAreaAssetPointLocationPointApSetParamDateRange(ServiceRegistrarClient.getInstance().getServiceHostURL(WAVEFORM_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), ciVo.getSiteId(), ciVo.getAreaId(), ciVo.getAssetId(), ciVo.getPointLocationId(), ciVo.getPointId(), ciVo.getSelectedApAlByPoint().getApSetId(), ciVo.getGraphType(), Instant.EPOCH.toString(), Instant.now().toString());
                    } else {
                        trendDataList = TrendDataClient.getInstance().getTrendsByCustomerSiteAreaAssetPointLocationPointApSetDateRange(ServiceRegistrarClient.getInstance().getServiceHostURL(WAVEFORM_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), ciVo.getSiteId(), ciVo.getAreaId(), ciVo.getAssetId(), ciVo.getPointLocationId(), ciVo.getPointId(), ciVo.getSelectedApAlByPoint().getApSetId(), Instant.EPOCH.toString(), Instant.now().toString());
                    }
                } else {
                    trendDataList = new ArrayList();
                }

                al = new ArrayList();
                count = 0;
                selectedDataPointIndex = 0;
                for (TrendValuesVO tvo : trendDataList) {
                    tmp = new ArrayList();
                    tmp.add(tvo.getSampleTime().toEpochMilli());
                    tmp.add(tvo.getValue());
                    al.add(tmp);
                    if (String.valueOf(tvo.getSampleTime().toEpochMilli()).equals(ciVo.getGraphTimestamp())) {
                        selectedDataPointIndex = count;
                    }
                    count++;
                }
                PrimeFaces.current().executeScript("updateTrendChartData( " + al + "," + selectedDataPointIndex + " )");
            }
        } catch (Exception e) {
            Logger.getLogger(AssetAnalysisWindowBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Performs Auto correlation of wave data
     */
    public void autoCorrelate() {
        double[] doubleSamplesArray;
        float[] floatSamplesArray;
        List<Float> samples;
        String yAxisUnit, samplesStr, plotLines;
        GraphDataVO waveGraphData;
        ChartInfoVO ciVo;
        WaveDataVO waveDataVO;
        float timeDelta, refMarkerMultiple;
        String id;
        
        try {
            if ((id = getCurrentWindowId()) != null) {
                windowId = id;
                ciVo = chartInfoMap.get(windowId);
                if ((waveDataVO = ciVo.getWaveDataVO()) != null && waveDataVO.getWave() != null) {
                    if ((samples = waveDataVO.getWave().getSamples()) != null) {
                        yAxisUnit = waveDataVO.getWave().getSensitivityUnits();
                        waveGraphData = ciVo.getWaveGraphData();
                        samplesStr = null;

                        if (genericStr.equalsIgnoreCase("apply-correlation")) {
                            doubleSamplesArray = getDoubleArrayFromSampleList(samples);
                            doubleSamplesArray = WaveformCorrelation.calculateCrossCorrelation(doubleSamplesArray);
                            timeDelta = calculateDeltaTime(waveGraphData.getSelectedTimeUnit(), doubleSamplesArray.length, ciVo.getSelectedApAlByPoint().getPeriod());
                            samplesStr = getSamplesAsString(doubleSamplesArray, timeDelta, waveGraphData);
                        } else if (genericStr.equalsIgnoreCase("remove-correlation")) {
                            floatSamplesArray = getFloatArrayFromSampleList(samples);
                            timeDelta = calculateDeltaTime(waveGraphData.getSelectedTimeUnit(), floatSamplesArray.length, ciVo.getSelectedApAlByPoint().getPeriod());
                            samplesStr = getUnitConvertedSamplesAsString(floatSamplesArray, yAxisUnit, timeDelta, waveGraphData);
                        }
                        if (unitConvertedSamplesArray != null && unitConvertedSamplesArray.length > 0) {
                            waveGraphData.setRmsWave(Float.valueOf(DECIMAL_FORMAT.format(RMS.calculate(unitConvertedSamplesArray))));
                        }
                        plotLines = "[]";
                        refMarkerMultiple = 1.0f;
                        PrimeFaces.current().executeScript("updateChartAxis('wave', " + samplesStr + " , '" + waveGraphData.getSelectedTimeUnit() + "' , '" + (genericStr.equalsIgnoreCase("apply-correlation") ? "" : waveGraphData.getSelectedAmplitudeUnit()) + "' , " + plotLines + ", " + waveGraphData.getRmsWave() + " , " + refMarkerMultiple + " , '', '' )");
                    }
                }
            }
        } catch (Exception e) {
            Logger.getLogger(AssetAnalysisWindowBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    public void setChartExportOptions() {
        String id;
        GraphDataVO graphDataVO;
        if ((id = getCurrentWindowId()) != null) {
            ChartInfoVO ciVo = chartInfoMap.get(id);
            if (Objects.equals(ciVo.getGraphType(), "wave")) {
                graphDataVO = ciVo.getWaveGraphData();
                PrimeFaces.current().executeScript("setChartExportOptions('wave', '" + graphDataVO.getSelectedTimeUnit() + "' , '" + graphDataVO.getSelectedAmplitudeUnit() + "')");
            } else if (Objects.equals(ciVo.getGraphType(), "spectrum")) {
                graphDataVO = ciVo.getSpectrumGraphData();
                PrimeFaces.current().executeScript("setChartExportOptions('spectrum', '" + getParamUnitLabelByValue(graphDataVO.getSelectedFrequencyUnit()) + "' , '" + graphDataVO.getSelectedAmplitudeUnit() + " - " + getAmpFactorLabelByValue(graphDataVO.getSelectedAmpFactor()) + "')");
            }
        }
    }

    /**
     * Validate the unit conversion
     *
     * @param graphType, wave or spectrum
     * @param axisType, x or y
     * @param ciVo, ChartInfoVO object
     * @return boolean
     */
    public boolean isUnitsConversionRequired(ChartInfoVO ciVo, String graphType, String axisType) {
        boolean isConversionRequired = false;
        String yAxisUnit, xAxisUnit;

        if (userManageBean != null && userManageBean.getAccountDefaultUnits() != null) {
            switch (axisType) {
                case "x":
                    if (Objects.equals(graphType, "wave")) {
                        xAxisUnit = ciVo.getWaveGraphData().getSelectedTimeUnit();
                        if (userManageBean.getAccountDefaultUnits().getTimeUnit() != null && !userManageBean.getAccountDefaultUnits().getTimeUnit().isEmpty() && !xAxisUnit.equalsIgnoreCase(userManageBean.getAccountDefaultUnits().getTimeUnit())) {
                            isConversionRequired = true;
                        }
                    } else if (Objects.equals(graphType, "spectrum")) {
                        xAxisUnit = ciVo.getSpectrumGraphData().getSelectedFrequencyUnit();
                        if (userManageBean.getAccountDefaultUnits().getFrequencyUnit() != null && !userManageBean.getAccountDefaultUnits().getFrequencyUnit().isEmpty() && !xAxisUnit.equalsIgnoreCase(userManageBean.getAccountDefaultUnits().getFrequencyUnit())) {
                            isConversionRequired = true;
                        }
                    }
                    break;
                case "y":
                    yAxisUnit = ciVo.getWaveDataVO().getWave().getSensitivityUnits();
                    if (Objects.equals(graphType, "wave")) {
                        switch (ciVo.getSelectedApAlByPoint().getSensorType().toLowerCase()) {
                            case "acceleration":
                                if (userManageBean.getAccountDefaultUnits().getAccelerationWaveform() != null && !userManageBean.getAccountDefaultUnits().getAccelerationWaveform().isEmpty() && !yAxisUnit.equalsIgnoreCase(userManageBean.getAccountDefaultUnits().getAccelerationWaveform())) {
                                    isConversionRequired = true;
                                }
                                break;
                            case "ultrasonic":
                                if (userManageBean.getAccountDefaultUnits().getUltrasonicWaveform() != null && !userManageBean.getAccountDefaultUnits().getUltrasonicWaveform().isEmpty() && !yAxisUnit.equalsIgnoreCase(userManageBean.getAccountDefaultUnits().getUltrasonicWaveform())) {
                                    isConversionRequired = true;
                                }
                                break;
                            case "velocity":
                                if (userManageBean.getAccountDefaultUnits().getVelocityWaveform() != null && !userManageBean.getAccountDefaultUnits().getVelocityWaveform().isEmpty() && !yAxisUnit.equalsIgnoreCase(userManageBean.getAccountDefaultUnits().getVelocityWaveform())) {
                                    isConversionRequired = true;
                                }
                                break;
                            case "displacement":
                                if (userManageBean.getAccountDefaultUnits().getDisplacementWaveform() != null && !userManageBean.getAccountDefaultUnits().getDisplacementWaveform().isEmpty() && !yAxisUnit.equalsIgnoreCase(userManageBean.getAccountDefaultUnits().getDisplacementWaveform())) {
                                    isConversionRequired = true;
                                }
                                break;
                            default:
                                throw new AssertionError();
                        }
                    } else if (Objects.equals(graphType, "spectrum")) {
                        switch (ciVo.getSelectedApAlByPoint().getSensorType().toLowerCase()) {
                            case "acceleration":
                                if (userManageBean.getAccountDefaultUnits().getAccelerationSpectrum() != null && !userManageBean.getAccountDefaultUnits().getAccelerationSpectrum().isEmpty() && !yAxisUnit.equalsIgnoreCase(userManageBean.getAccountDefaultUnits().getAccelerationSpectrum())) {
                                    isConversionRequired = true;
                                }
                                break;
                            case "ultrasonic":
                                if (userManageBean.getAccountDefaultUnits().getUltrasonicSpectrum() != null && !userManageBean.getAccountDefaultUnits().getUltrasonicSpectrum().isEmpty() && !yAxisUnit.equalsIgnoreCase(userManageBean.getAccountDefaultUnits().getUltrasonicSpectrum())) {
                                    isConversionRequired = true;
                                }
                                break;
                            case "velocity":
                                if (userManageBean.getAccountDefaultUnits().getVelocitySpectrum() != null && !userManageBean.getAccountDefaultUnits().getVelocitySpectrum().isEmpty() && !yAxisUnit.equalsIgnoreCase(userManageBean.getAccountDefaultUnits().getVelocitySpectrum())) {
                                    isConversionRequired = true;
                                }
                                break;
                            case "displacement":
                                if (userManageBean.getAccountDefaultUnits().getDisplacementSpectrum() != null && !userManageBean.getAccountDefaultUnits().getDisplacementSpectrum().isEmpty() && !yAxisUnit.equalsIgnoreCase(userManageBean.getAccountDefaultUnits().getDisplacementSpectrum())) {
                                    isConversionRequired = true;
                                }
                                break;
                            default:
                                throw new AssertionError();
                        }
                    }
                    break;
                default:
                    return false;
            }
        }
        return isConversionRequired;
    }

    /**
     * Validate the unit conversion
     *
     * @param ciVo, ChartInfoVO object
     * @return boolean
     */
    public boolean isAmpFactorConversionRequired(ChartInfoVO ciVo) {
        boolean isConversionRequired = false;

        if (userManageBean != null && userManageBean.getAccountDefaultUnits() != null) {
            switch (ciVo.getSelectedApAlByPoint().getSensorType().toLowerCase()) {
                case "acceleration":
                    if (userManageBean.getAccountDefaultUnits().getAccelerationAmpFactor() != null && !userManageBean.getAccountDefaultUnits().getAccelerationAmpFactor().isEmpty() && !SPEC_DEFAULT_AMP_FACTOR.equalsIgnoreCase(userManageBean.getAccountDefaultUnits().getAccelerationAmpFactor())) {
                        ciVo.getSpectrumGraphData().setSelectedAmpFactor(userManageBean.getAccountDefaultUnits().getAccelerationAmpFactor());
                        isConversionRequired = true;
                    }
                    break;
                case "ultrasonic":
                    if (userManageBean.getAccountDefaultUnits().getUltrasonicAmpFactor() != null && !userManageBean.getAccountDefaultUnits().getUltrasonicAmpFactor().isEmpty() && !SPEC_DEFAULT_AMP_FACTOR.equalsIgnoreCase(userManageBean.getAccountDefaultUnits().getUltrasonicAmpFactor())) {
                        ciVo.getSpectrumGraphData().setSelectedAmpFactor(userManageBean.getAccountDefaultUnits().getUltrasonicAmpFactor());
                        isConversionRequired = true;
                    }
                    break;
                case "velocity":
                    if (userManageBean.getAccountDefaultUnits().getVelocityAmpFactor() != null && !userManageBean.getAccountDefaultUnits().getVelocityAmpFactor().isEmpty() && !SPEC_DEFAULT_AMP_FACTOR.equalsIgnoreCase(userManageBean.getAccountDefaultUnits().getVelocityAmpFactor())) {
                        ciVo.getSpectrumGraphData().setSelectedAmpFactor(userManageBean.getAccountDefaultUnits().getVelocityAmpFactor());
                        isConversionRequired = true;
                    }
                    break;
                case "displacement":
                    if (userManageBean.getAccountDefaultUnits().getDisplacementAmpFactor() != null && !userManageBean.getAccountDefaultUnits().getDisplacementAmpFactor().isEmpty() && !SPEC_DEFAULT_AMP_FACTOR.equalsIgnoreCase(userManageBean.getAccountDefaultUnits().getDisplacementAmpFactor())) {
                        ciVo.getSpectrumGraphData().setSelectedAmpFactor(userManageBean.getAccountDefaultUnits().getDisplacementAmpFactor());
                        isConversionRequired = true;
                    }
                    break;
                default:
                    throw new AssertionError();
            }
        }

        return isConversionRequired;
    }

    /**
     * Set the spectrum Log Y axis
     *
     * @param ciVo, ChartInfoVO object
     */
    public void setSpectrumLogYaxis(ChartInfoVO ciVo) {
        if (userManageBean != null && userManageBean.getAccountDefaultUnits() != null) {
            switch (ciVo.getSelectedApAlByPoint().getSensorType().toLowerCase()) {
                case "acceleration":
                    ciVo.setSpectrumLogYAxis(userManageBean.getAccountDefaultUnits().isAccelerationlogYaxis());
                    break;
                case "ultrasonic":
                    ciVo.setSpectrumLogYAxis(userManageBean.getAccountDefaultUnits().isUltrasoniclogYaxis());
                    break;
                case "velocity":
                    ciVo.setSpectrumLogYAxis(userManageBean.getAccountDefaultUnits().isVelocitylogYaxis());
                    break;
                case "displacement":
                    ciVo.setSpectrumLogYAxis(userManageBean.getAccountDefaultUnits().isDisplacementlogYaxis());
                    break;
                default:
                    throw new AssertionError();
            }
        }
    }

    /**
     * Returns the default unit of given sensor
     *
     * @param graphType, wave or spectrum
     * @param sensorType
     * @return String Object
     */
    public String getUnitforSensorType(String sensorType, String graphType) {
        String unit = "";

        if (userManageBean != null && userManageBean.getAccountDefaultUnits() != null) {
            if (Objects.equals(graphType, "wave")) {
                switch (sensorType.toLowerCase()) {
                    case "acceleration":
                        unit = userManageBean.getAccountDefaultUnits().getAccelerationWaveform();
                        break;
                    case "ultrasonic":
                        unit = userManageBean.getAccountDefaultUnits().getUltrasonicWaveform();
                        break;
                    case "velocity":
                        unit = userManageBean.getAccountDefaultUnits().getVelocityWaveform();
                        break;
                    case "displacement":
                        unit = userManageBean.getAccountDefaultUnits().getDisplacementWaveform();
                        break;
                    case "time":
                        unit = userManageBean.getAccountDefaultUnits().getTimeUnit();
                        break;
                    default:
                        throw new AssertionError();
                }
            } else if (Objects.equals(graphType, "spectrum")) {
                switch (sensorType.toLowerCase()) {
                    case "acceleration":
                        unit = userManageBean.getAccountDefaultUnits().getAccelerationSpectrum();
                        break;
                    case "ultrasonic":
                        unit = userManageBean.getAccountDefaultUnits().getUltrasonicSpectrum();
                        break;
                    case "velocity":
                        unit = userManageBean.getAccountDefaultUnits().getVelocitySpectrum();
                        break;
                    case "displacement":
                        unit = userManageBean.getAccountDefaultUnits().getDisplacementSpectrum();
                        break;
                    case "frequency":
                        unit = userManageBean.getAccountDefaultUnits().getFrequencyUnit();
                        break;
                    default:
                        throw new AssertionError();
                }
            }
        }
        return unit;
    }

    /**
     * Download the trend graph
     *
     * @return StreamedContent Object
     */
    public StreamedContent downloadTrendGraph() {
        InputStream targetStream;
        StringBuilder csvData;
        String[] xvals, yvals;
        StreamedContent file;
        ChartInfoVO civo;
        String id;

        try {
            if ((id = getCurrentWindowId()) != null) {
                windowId = id;

                csvData = new StringBuilder();
                if ((civo = chartInfoMap.get(windowId)) != null) {
                    csvData.append("Area").append(",");
                    csvData.append(civo.getAreaName()).append("\n");
                    csvData.append("Asset").append(",");
                    csvData.append(civo.getAssetName()).append("\n");
                    csvData.append("Point Location").append(",");
                    csvData.append(civo.getPointLocationName()).append("\n");
                    csvData.append("Point").append(",");
                    csvData.append(civo.getPointName()).append("\n");
                    csvData.append("Param").append(",");
                    csvData.append(civo.getParamName()).append("\n");
                    csvData.append("Time").append(",");
                    if (civo.getPointType().equalsIgnoreCase("AC")) {
                        if (civo.getSelectedApAlByPoint().getParamType().equalsIgnoreCase("Waveform Crest Factor")) {
                            csvData.append("Amplitude (").append("Crest").append(")").append("\n");
                        } else {
                            csvData.append("Amplitude (").append(yaxisTitle).append(")").append("\n");
                        }
                    } else {
                        csvData.append("Amplitude (").append(yaxisTitle).append(")").append("\n");
                    }

                    xvals = xaxis.split(",");
                    yvals = yaxis.split(",");
                    for (int i = 1; i < xvals.length; i++) {
                        csvData.append(dateFormatter(new Date(Long.parseLong(xvals[i])).toInstant()).replace("$", " ")).append(",").append(yvals[i]).append("\n");
                    }
                }
                targetStream = new ByteArrayInputStream(csvData.toString().getBytes());
                file = DefaultStreamedContent.builder().name("TrendData_" + civo.getParamName() + ".csv").contentType("text/plain").stream(() -> targetStream).build();
                return file;
            }

        } catch (Exception e) {
            Logger.getLogger(AssetAnalysisWindowBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
        return null;
    }

    /**
     * Download the waveform graph
     *
     * @return StreamedContent Object
     */
    public StreamedContent downloadWaveformGraph() {
        InputStream targetStream;
        StringBuilder csvData;
        String[] xvals, yvals;
        StreamedContent file;
        ChartInfoVO civo;
        String id;

        try {
            if ((id = getCurrentWindowId()) != null) {
                windowId = id;

                csvData = new StringBuilder();
                if ((civo = chartInfoMap.get(windowId)) != null) {
                    csvData.append("Area").append(",");
                    csvData.append(civo.getAreaName()).append("\n");
                    csvData.append("Asset").append(",");
                    csvData.append(civo.getAssetName()).append("\n");
                    csvData.append("Point Location").append(",");
                    csvData.append(civo.getPointLocationName()).append("\n");
                    csvData.append("Point").append(",");
                    csvData.append(civo.getPointName()).append("\n");
                    csvData.append("Sample Timestamp").append(",");
                    csvData.append(dateFormatter(civo.getWaveDataVO().getSampleTime()).replace("$", " ")).append("\n");
                    csvData.append("Time (").append(civo.getWaveGraphData().getSelectedTimeUnit()).append(")").append(",");
                    csvData.append("Amplitude (").append(yaxisTitle).append(")").append("\n");

                    xvals = xaxis.split(",");
                    yvals = yaxis.split(",");
                    for (int i = 1; i < xvals.length; i++) {
                        csvData.append(xvals[i]).append(",").append(yvals[i]).append("\n");
                    }
                }
                targetStream = new ByteArrayInputStream(csvData.toString().getBytes());
                file = DefaultStreamedContent.builder().name("WaveForm" + System.currentTimeMillis() + ".csv").contentType("text/plain").stream(() -> targetStream).build();
                return file;
            }
        } catch (Exception e) {
            Logger.getLogger(AssetAnalysisWindowBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
        return null;
    }

    /**
     * Download the Spectrum graph
     *
     * @return StreamedContent Object
     */
    public StreamedContent downloadSpectrumGraph() {
        InputStream targetStream;
        StringBuilder csvData;
        String[] xvals, yvals;
        StreamedContent file;
        ChartInfoVO civo;
        String id;

        try {
            if ((id = getCurrentWindowId()) != null) {
                windowId = id;

                csvData = new StringBuilder();
                if ((civo = chartInfoMap.get(windowId)) != null) {
                    csvData.append("Area").append(",");
                    csvData.append(civo.getAreaName()).append("\n");
                    csvData.append("Asset").append(",");
                    csvData.append(civo.getAssetName()).append("\n");
                    csvData.append("Point Location").append(",");
                    csvData.append(civo.getPointLocationName()).append("\n");
                    csvData.append("Point").append(",");
                    csvData.append(civo.getPointName()).append("\n");
                    csvData.append("Sample Timestamp").append(",");
                    csvData.append(dateFormatter(civo.getWaveDataVO().getSampleTime()).replace("$", " ")).append("\n");
                    csvData.append("Frequency (").append(civo.getSpectrumGraphData().getSelectedFrequencyUnit()).append(")").append(",");
                    csvData.append("Amplitude (").append(yaxisTitle).append(")").append("\n");

                    xvals = xaxis.split(",");
                    yvals = yaxis.split(",");
                    for (int i = 1; i < xvals.length; i++) {
                        csvData.append(xvals[i]).append(",").append(yvals[i]).append("\n");
                    }
                }
                targetStream = new ByteArrayInputStream(csvData.toString().getBytes());
                file = DefaultStreamedContent.builder().name("Spectrum" + System.currentTimeMillis() + ".csv").contentType("text/plain").stream(() -> targetStream).build();
                return file;
            }
        } catch (Exception e) {
            Logger.getLogger(AssetAnalysisWindowBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
        return null;
    }

    private String dateFormatter(Instant instant) {
        try {
            return LocalDateTime.ofInstant(instant, (userManageBean.getZoneId() != null) ? userManageBean.getZoneId() : ZoneOffset.UTC).format(DateTimeFormatter.ofPattern("yyyy-MM-dd$hh:mm:ss a"));
        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * Clears the details associated with the window upon its closure.
     *
     * This method is typically invoked when a window is closed to ensure that any resources or data related to the window are properly cleaned up.
     *
     */
    public void clearWindowDetailsOnWindowClose() {
        ChartInfoVO ciVo;
        String id;

        try {
            if ((id = getCurrentWindowId()) != null) {
                windowId = id;

                if (!itemDownload) {
                    ciVo = chartInfoMap.get(windowId);
                    if (removeWindowDetails || (acWindowOpened ? (ciVo != null && ciVo.getPointType().equalsIgnoreCase("DC") && ciVo.getCount() == 2) : (ciVo != null && ciVo.getPointType().equalsIgnoreCase("DC") && ciVo.getCount() == 1)) || (ciVo != null && ciVo.getPointType().equalsIgnoreCase("AC") && ciVo.getCount() == 3)) {
                        ciVo.setCount(0);
                        chartInfoMap.remove(windowId);
                        PrimeFaces.current().executeScript("removeWindowId('" + windowId + "')");
                        Logger.getLogger(AssetAnalysisWindowBean.class.getName()).log(Level.INFO, "**************ChartInfoMap after remove:{0}", chartInfoMap.size());
                    } else {
                        Logger.getLogger(AssetAnalysisWindowBean.class.getName()).log(Level.INFO, "**************Failed to remove id:{0}", windowId);
                    }
                    removeWindowDetails = true;
                } else {
                    Logger.getLogger(AssetAnalysisWindowBean.class.getName()).log(Level.INFO, "**************Download occured. Ignore window detail clear for:{0}", windowId);
                    itemDownload = false;
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(AssetAnalysisWindowBean.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

    public void updateCursorHarmonical() {
        PrimeFaces.current().executeScript("cursorHarmonical()");
    }

    public void updateCursorSideband() {
        PrimeFaces.current().executeScript("cursorSidebandFloatPoints()");
    }

    public void onHarmonicChange(String harmonicType) {
        String id;
        try {
            if (harmonicType != null && (harmonicType.equalsIgnoreCase("startHarmonic") || harmonicType.equalsIgnoreCase("endHarmonic")) && (id = getCurrentWindowId()) != null) {
                windowId = id;
                if (chartInfoMap.get(id) != null && chartInfoMap.get(id).getSpectrumGraphData() != null) {
                    if (harmonicType.equalsIgnoreCase("startHarmonic") && chartInfoMap.get(id).getSpectrumGraphData().getEndingHarmonic() != null) {
                        chartInfoMap.get(id).getSpectrumGraphData().setEndingHarmonic(null);
                        onFFSelect();
                    } else if (chartInfoMap.get(id).getSpectrumGraphData().getStartingHarmonic() != null && chartInfoMap.get(id).getSpectrumGraphData().getEndingHarmonic() != null && chartInfoMap.get(id).getSpectrumGraphData().getStartingHarmonic() < chartInfoMap.get(id).getSpectrumGraphData().getEndingHarmonic()) {
                        onFFSelect();
                    }
                }
            }
        } catch (Exception e) {
            Logger.getLogger(AssetAnalysisWindowBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    public List<SelectItem> getFfSetItemsList() {
        List<SelectItem> list;
        Set<UUID> ffSetIds;
        String id;
        list = new ArrayList();
        try {
            if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id) && chartInfoMap.get(id).getFfSetVOList() != null) {
                windowId = id;
                ffSetIds = new HashSet();
                chartInfoMap.get(id).getFfSetVOList().stream().filter(vo -> vo.getFfSetId() != null).forEach(vo -> ffSetIds.add(vo.getFfSetId()));
                ffSetIds.stream().forEachOrdered(ffSetId -> {
                    FaultFrequenciesVO tmpFreq;
                    tmpFreq = chartInfoMap.get(id).getFfSetVOList().stream().filter(vo -> Objects.equals(ffSetId, vo.getFfSetId())).findAny().get();
                    list.add(new SelectItem(tmpFreq.getFfSetId(), tmpFreq.getFfSetName()));
                });
            }
        } catch (Exception e) {
            Logger.getLogger(AssetAnalysisWindowBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
        return list;
    }

    public List<FaultFrequenciesVO> getFfVoList() {
        String id;
        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id) && chartInfoMap.get(id).getFfSetVOList() != null && chartInfoMap.get(id).getSpectrumGraphData() != null && chartInfoMap.get(id).getSpectrumGraphData().getSelectedFfSetId() != null) {
            windowId = id;
            try {
                chartInfoMap.get(id).getSpectrumGraphData().setFrequencyList(new ArrayList());
                chartInfoMap.get(id).getFfSetVOList().stream().filter(ffset -> Objects.equals(ffset.getFfSetId(), chartInfoMap.get(id).getSpectrumGraphData().getSelectedFfSetId())).sorted(Comparator.comparing(FaultFrequenciesVO::getFfName)).forEachOrdered(ff -> chartInfoMap.get(id).getSpectrumGraphData().getFrequencyList().add(new FaultFrequenciesVO(ff)));
                return chartInfoMap.get(id).getSpectrumGraphData().getFrequencyList();
            } catch (Exception e) {
                Logger.getLogger(AssetAnalysisWindowBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            }
        }
        return new ArrayList();
    }

    /**
     * Called when there is a change in tachometer speed, associated with a specific graph type.
     *
     * @param graphType The type of graph associated with the tachometer speed change.
     */
    public void onTachSpeedChange(String graphType) {
        String id;
        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id) && chartInfoMap.get(id).getSpectrumGraphData() != null) {
            windowId = id;
            try {
                if (Objects.equals(graphType, "spectrum")) {
                    GraphDataVO specGraphData = chartInfoMap.get(id).getSpectrumGraphData();
                    if (Objects.equals(specGraphData.getTachSpeed(), 0.0f) || specGraphData.getTachSpeed() < 0.1f) {
                        specGraphData.setTachSpeed(0.1f);
                    }
                    specGraphData.setTachSpeed(Math.round(specGraphData.getTachSpeed() * 10000f) / 10000f);
                    if (specGraphData.getSelectedFrequencyUnit() != null && specGraphData.getSelectedFrequencyUnit().equals("Orders")) {
                        chartInfoMap.get(id).setGraphType("spectrum");
                        savePlotDataOptions(false);
                    } else if (specGraphData.getSelectedFfIds() != null && !specGraphData.getSelectedFfIds().isEmpty()) {
                        onFFSelect();
                    }
                } else if (Objects.equals(graphType, "wave")) {
                    // TO-DO
                }
            } catch (Exception e) {
                Logger.getLogger(AssetAnalysisWindowBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            }
        }
    }

    /**
     * Resets the tachometer speed with one which is stored with the wave record.
     *
     * @param graphType The type of graph associated with the tachometer speed reset.
     *
     */
    public void resetTachSpeed(String graphType) {
        String id;
        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id) && chartInfoMap.get(id).getSpectrumGraphData() != null) {
            windowId = id;
            try {
                if (Objects.equals(graphType, "spectrum")) {
                    GraphDataVO specGraphData = chartInfoMap.get(id).getSpectrumGraphData();
                    specGraphData.setTachSpeed(specGraphData.getOriginalTachSpeed());
                    specGraphData.setTachSpeedUnit(TACHSPEED_DEFAULT_UNIT);
                    if (specGraphData.getSelectedFrequencyUnit() != null && specGraphData.getSelectedFrequencyUnit().equals("Orders")) {
                        chartInfoMap.get(id).setGraphType("spectrum");
                        savePlotDataOptions(false);
                    } else if (specGraphData.getSelectedFfIds() != null && !specGraphData.getSelectedFfIds().isEmpty()) {
                        onFFSelect();
                    }
                } else if (Objects.equals(graphType, "wave")) {
                    GraphDataVO waveGraphData = chartInfoMap.get(id).getWaveGraphData();
                    waveGraphData.setTachSpeed(waveGraphData.getOriginalTachSpeed());
                    waveGraphData.setTachSpeedUnit(TACHSPEED_DEFAULT_UNIT);
                    // TO-DO
                }
            } catch (Exception e) {
                Logger.getLogger(AssetAnalysisWindowBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            }
        }
    }

    /**
     * Called when changing unit of measurement for tachometer speed.
     *
     * @param graphType The type of graph associated with the tachometer speed change.
     */
    public void onTachSpeedUnitChange(String graphType) {
        String id;
        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id) && chartInfoMap.get(id).getSpectrumGraphData() != null) {
            windowId = id;
            try {
                if (Objects.equals(graphType, "spectrum")) {
                    GraphDataVO specGraphData = chartInfoMap.get(id).getSpectrumGraphData();
                    if (specGraphData.getTachSpeedUnit().equals("Hz")) {
                        specGraphData.setTachSpeed(specGraphData.getTachSpeed() / 60f);
                    } else if (specGraphData.getTachSpeedUnit().equals("RPM")) {
                        specGraphData.setTachSpeed(specGraphData.getTachSpeed() * 60f);
                    }
                } else if (Objects.equals(graphType, "wave")) {
                    GraphDataVO waveGraphData = chartInfoMap.get(id).getWaveGraphData();
                    if (waveGraphData.getTachSpeedUnit().equals("Hz")) {
                        waveGraphData.setTachSpeed(waveGraphData.getTachSpeed() / 60f);
                    } else if (waveGraphData.getTachSpeedUnit().equals("RPM")) {
                        waveGraphData.setTachSpeed(waveGraphData.getTachSpeed() * 60f);
                    }
                }
            } catch (Exception e) {
                Logger.getLogger(AssetAnalysisWindowBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            }
        }
    }

    /**
     * Composes and plays waveform audio based on the current window's wave data.
     * 
     * This method retrieves the current window ID and uses it to fetch the corresponding
     * {@link ChartInfoVO} object from a map. If the {@link ChartInfoVO} object contains valid
     * wave data, it extracts the samples and sample rate, converts the samples into a 
     * base64-encoded WAV audio string, and triggers the playback of this audio in the browser
     * using a JavaScript function.
     * 
     * In case of any exception during this process, it logs the error at the SEVERE level.
     * 
     * @throws Exception if an error occurs during the process of composing and playing the waveform audio.
     */
    public void composeWaveformAudio() {
        WaveDataVO waveDataVO;
        List<Float> samples = null;
        ChartInfoVO ciVo;
        String id;
        try {
            if ((id = getCurrentWindowId()) != null) {
                windowId = id;
                if ((ciVo = chartInfoMap.get(windowId)) != null) {
                    if ((waveDataVO = ciVo.getWaveDataVO()) != null && waveDataVO.getWave() != null) {
                        samples = waveDataVO.getWave().getSamples();
                    }
                    if (samples != null) {
                        float sampleRate = waveDataVO.getWave().getSampleRateHz();
                        AudioUtil audioUtil = new AudioUtil();
                        String base64Audio = audioUtil.convertWavAudio(getDoubleArrayFromSampleList(samples), sampleRate);
                        PrimeFaces.current().executeScript("playWaveAudio('" + base64Audio + "')");
                    }
                }
            }
        } catch (Exception e) {
            Logger.getLogger(AssetAnalysisWindowBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }
    
    public void setEditAlarmLimitLowFaultActive(boolean value) {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id) && chartInfoMap.get(id).getSelectedAlarmLimitPoint() != null) {
            windowId = id;
            chartInfoMap.get(id).getSelectedAlarmLimitPoint().setLowFaultActive(value);
        }
    }

    public boolean isEditAlarmLimitLowFaultActive() {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id) && chartInfoMap.get(id).getSelectedAlarmLimitPoint() != null) {
            windowId = id;
            return chartInfoMap.get(id).getSelectedAlarmLimitPoint().isLowFaultActive();
        }
        return false;
    }

    public void setEditAlarmLimitLowAlertActive(boolean value) {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id) && chartInfoMap.get(id).getSelectedAlarmLimitPoint() != null) {
            windowId = id;
            chartInfoMap.get(id).getSelectedAlarmLimitPoint().setLowAlertActive(value);
        }
    }

    public boolean isEditAlarmLimitLowAlertActive() {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id) && chartInfoMap.get(id).getSelectedAlarmLimitPoint() != null) {
            windowId = id;
            return chartInfoMap.get(id).getSelectedAlarmLimitPoint().isLowAlertActive();
        }
        return false;
    }

    public void setEditAlarmLimitHighAlertActive(boolean value) {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id) && chartInfoMap.get(id).getSelectedAlarmLimitPoint() != null) {
            windowId = id;
            chartInfoMap.get(id).getSelectedAlarmLimitPoint().setHighAlertActive(value);
        }
    }

    public boolean isEditAlarmLimitHighAlertActive() {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id) && chartInfoMap.get(id).getSelectedAlarmLimitPoint() != null) {
            windowId = id;
            return chartInfoMap.get(id).getSelectedAlarmLimitPoint().isHighAlertActive();
        }
        return false;
    }

    public void setEditAlarmLimitHighFaultActive(boolean value) {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id) && chartInfoMap.get(id).getSelectedAlarmLimitPoint() != null) {
            windowId = id;
            chartInfoMap.get(id).getSelectedAlarmLimitPoint().setHighFaultActive(value);
        }
    }

    public boolean isEditAlarmLimitHighFaultActive() {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id) && chartInfoMap.get(id).getSelectedAlarmLimitPoint() != null) {
            windowId = id;
            return chartInfoMap.get(id).getSelectedAlarmLimitPoint().isHighFaultActive();
        }
        return false;
    }

    public void setEditAlarmLimitLowFault(float value) {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id) && chartInfoMap.get(id).getSelectedAlarmLimitPoint() != null) {
            windowId = id;
            chartInfoMap.get(id).getSelectedAlarmLimitPoint().setLowFault(value);
        }
    }

    public float getEditAlarmLimitLowFault() {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id) && chartInfoMap.get(id).getSelectedAlarmLimitPoint() != null) {
            windowId = id;
            return chartInfoMap.get(id).getSelectedAlarmLimitPoint().getLowFault();
        }
        return (float) 0;
    }

    public void setEditAlarmLimitLowAlert(float value) {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id) && chartInfoMap.get(id).getSelectedAlarmLimitPoint() != null) {
            windowId = id;
            chartInfoMap.get(id).getSelectedAlarmLimitPoint().setLowAlert(value);
        }
    }

    public float getEditAlarmLimitLowAlert() {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id) && chartInfoMap.get(id).getSelectedAlarmLimitPoint() != null) {
            windowId = id;
            return chartInfoMap.get(id).getSelectedAlarmLimitPoint().getLowAlert();
        }
        return (float) 0;
    }

    public void setEditAlarmLimitHighAlert(float value) {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id) && chartInfoMap.get(id).getSelectedAlarmLimitPoint() != null) {
            windowId = id;
            chartInfoMap.get(id).getSelectedAlarmLimitPoint().setHighAlert(value);
        }
    }

    public float getEditAlarmLimitHighAlert() {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id) && chartInfoMap.get(id).getSelectedAlarmLimitPoint() != null) {
            windowId = id;
            return chartInfoMap.get(id).getSelectedAlarmLimitPoint().getHighAlert();
        }
        return (float) 0;
    }

    public void setEditAlarmLimitHighFault(float value) {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id) && chartInfoMap.get(id).getSelectedAlarmLimitPoint() != null) {
            windowId = id;
            chartInfoMap.get(id).getSelectedAlarmLimitPoint().setHighFault(value);
        }
    }

    public float getEditAlarmLimitHighFault() {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id) && chartInfoMap.get(id).getSelectedAlarmLimitPoint() != null) {
            windowId = id;
            return chartInfoMap.get(id).getSelectedAlarmLimitPoint().getHighFault();
        }
        return (float) 0;
    }

    public void setEditAlarmLimitDwell(int value) {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id) && chartInfoMap.get(id).getSelectedAlarmLimitPoint() != null) {
            windowId = id;
            chartInfoMap.get(id).getSelectedAlarmLimitPoint().setDwell(value);
        }
    }

    public int getEditAlarmLimitDwell() {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id) && chartInfoMap.get(id).getSelectedAlarmLimitPoint() != null) {
            windowId = id;
            return chartInfoMap.get(id).getSelectedAlarmLimitPoint().getDwell();
        }
        return 0;
    }

    public void setWorkOrderSiteName(String value) {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id) && chartInfoMap.get(id).getWorkOrderVO() != null) {
            windowId = id;
            chartInfoMap.get(id).getWorkOrderVO().setSiteName(value);
        }
    }

    public String getWorkOrderSiteName() {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id) && chartInfoMap.get(id).getWorkOrderVO() != null) {
            windowId = id;
            return chartInfoMap.get(id).getWorkOrderVO().getSiteName();
        }
        return null;
    }

    public void setWorkOrderAreaName(String value) {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id) && chartInfoMap.get(id).getWorkOrderVO() != null) {
            windowId = id;
            chartInfoMap.get(id).getWorkOrderVO().setAreaName(value);
        }
    }

    public String getWorkOrderAreaName() {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id) && chartInfoMap.get(id).getWorkOrderVO() != null) {
            windowId = id;
            return chartInfoMap.get(id).getWorkOrderVO().getAreaName();
        }
        return null;
    }

    public void setWorkOrderAssetName(String value) {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id) && chartInfoMap.get(id).getWorkOrderVO() != null) {
            windowId = id;
            chartInfoMap.get(id).getWorkOrderVO().setAssetName(value);
        }
    }

    public String getWorkOrderAssetName() {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id) && chartInfoMap.get(id).getWorkOrderVO() != null) {
            windowId = id;
            return chartInfoMap.get(id).getWorkOrderVO().getAssetName();
        }
        return null;
    }

    public void setWorkOrderPointLocationName(String value) {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id) && chartInfoMap.get(id).getWorkOrderVO() != null) {
            windowId = id;
            chartInfoMap.get(id).getWorkOrderVO().setPointLocationName(value);
        }
    }

    public String getWorkOrderPointLocationName() {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id) && chartInfoMap.get(id).getWorkOrderVO() != null) {
            windowId = id;
            return chartInfoMap.get(id).getWorkOrderVO().getPointLocationName();
        }
        return null;
    }

    public void setWorkOrderPointName(String value) {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id) && chartInfoMap.get(id).getWorkOrderVO() != null) {
            windowId = id;
            chartInfoMap.get(id).getWorkOrderVO().setPointName(value);
        }
    }

    public String getWorkOrderPointName() {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id) && chartInfoMap.get(id).getWorkOrderVO() != null) {
            windowId = id;
            return chartInfoMap.get(id).getWorkOrderVO().getPointName();
        }
        return null;
    }

    public void setWorkOrderAssetComponent(String value) {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id) && chartInfoMap.get(id).getWorkOrderVO() != null) {
            windowId = id;
            chartInfoMap.get(id).getWorkOrderVO().setAssetComponent(value);
        }
    }

    public String getWorkOrderAssetComponent() {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id) && chartInfoMap.get(id).getWorkOrderVO() != null) {
            windowId = id;
            return chartInfoMap.get(id).getWorkOrderVO().getAssetComponent();
        }
        return null;
    }

    public void setWorkOrderIssue(String value) {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id) && chartInfoMap.get(id).getWorkOrderVO() != null) {
            windowId = id;
            chartInfoMap.get(id).getWorkOrderVO().setIssue(value);
        }
    }

    public String getWorkOrderIssue() {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id) && chartInfoMap.get(id).getWorkOrderVO() != null) {
            windowId = id;
            return chartInfoMap.get(id).getWorkOrderVO().getIssue();
        }
        return null;
    }

    public void setWorkOrderAction(String value) {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id) && chartInfoMap.get(id).getWorkOrderVO() != null) {
            windowId = id;
            chartInfoMap.get(id).getWorkOrderVO().setAction(value);
        }
    }

    public String getWorkOrderAction() {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id) && chartInfoMap.get(id).getWorkOrderVO() != null) {
            windowId = id;
            return chartInfoMap.get(id).getWorkOrderVO().getAction();
        }
        return null;
    }

    public void setWorkOrderPriority(byte value) {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id) && chartInfoMap.get(id).getWorkOrderVO() != null) {
            windowId = id;
            chartInfoMap.get(id).getWorkOrderVO().setPriority(value);
        }
    }

    public byte getWorkOrderPriority() {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id) && chartInfoMap.get(id).getWorkOrderVO() != null) {
            windowId = id;
            return chartInfoMap.get(id).getWorkOrderVO().getPriority();
        }
        return (byte) 0;
    }

    public void setWorkOrderDescription(String value) {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id) && chartInfoMap.get(id).getWorkOrderVO() != null) {
            windowId = id;
            chartInfoMap.get(id).getWorkOrderVO().setDescription(value);
        }
    }

    public String getWorkOrderDescription() {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id) && chartInfoMap.get(id).getWorkOrderVO() != null) {
            windowId = id;
            return chartInfoMap.get(id).getWorkOrderVO().getDescription();
        }
        return null;
    }

    public void setWorkOrderSelectedParamList(List<String> values) {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id)) {
            windowId = id;
            chartInfoMap.get(id).setWorkOrderSelectedParamList(values);
        }
    }

    public List<String> getWorkOrderSelectedParamList() {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id)) {
            windowId = id;
            return chartInfoMap.get(id).getWorkOrderSelectedParamList();
        }
        return null;
    }

    public void setWorkOrderNotes(String value) {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id) && chartInfoMap.get(id).getWorkOrderVO() != null) {
            windowId = id;
            chartInfoMap.get(id).getWorkOrderVO().setNotes(value);
        }
    }

    public String getWorkOrderNotes() {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id) && chartInfoMap.get(id).getWorkOrderVO() != null) {
            windowId = id;
            return chartInfoMap.get(id).getWorkOrderVO().getNotes();
        }
        return null;
    }

    public List<String> getWorkOrderParamNameList() {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id)) {
            windowId = id;
            return chartInfoMap.get(id).getParamNameList();
        }
        return null;
    }

    public boolean isEditAlarmLimitLowFaultValidation() {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id)) {
            windowId = id;
            return chartInfoMap.get(id).isLowFaultValidationFailed();
        }
        return false;
    }

    public boolean isEditAlarmLimitLowAlertValidation() {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id)) {
            windowId = id;
            return chartInfoMap.get(id).isLowAlertValidationFailed();
        }
        return false;
    }

    public boolean isEditAlarmLimitHighAlertValidation() {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id)) {
            windowId = id;
            return chartInfoMap.get(id).isHighAlertValidationFailed();
        }
        return false;
    }

    public boolean isEditAlarmLimitHighFaultValidation() {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id)) {
            windowId = id;
            return chartInfoMap.get(id).isHighFaultValidationFailed();
        }
        return false;
    }

    public List<UUID> getSelectedFfIds() {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id) && chartInfoMap.get(id).getSpectrumGraphData() != null) {
            windowId = id;
            return chartInfoMap.get(id).getSpectrumGraphData().getSelectedFfIds();
        }
        return null;
    }

    public void setSelectedFfIds(List<UUID> selectedFfIds) {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id) && chartInfoMap.get(id).getSpectrumGraphData() != null) {
            windowId = id;
            chartInfoMap.get(id).getSpectrumGraphData().setSelectedFfIds(selectedFfIds);
        }
    }

    public UUID getSelectedFfSetId() {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id) && chartInfoMap.get(id).getSpectrumGraphData() != null) {
            windowId = id;
            return chartInfoMap.get(id).getSpectrumGraphData().getSelectedFfSetId();
        }
        return null;
    }

    public void setSelectedFfSetId(UUID selectedFfSetId) {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id) && chartInfoMap.get(id).getSpectrumGraphData() != null) {
            windowId = id;
            chartInfoMap.get(id).getSpectrumGraphData().setSelectedFfSetId(selectedFfSetId);
        }
    }

    public Integer getStartHarmonic() {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id) && chartInfoMap.get(id).getSpectrumGraphData() != null) {
            windowId = id;
            return chartInfoMap.get(id).getSpectrumGraphData().getStartingHarmonic();
        }
        return null;
    }

    public void setStartHarmonic(Integer startHarmonic) {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id) && chartInfoMap.get(id).getSpectrumGraphData() != null) {
            windowId = id;
            chartInfoMap.get(id).getSpectrumGraphData().setStartingHarmonic(startHarmonic);
        }
    }

    public Integer getEndHarmonic() {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id) && chartInfoMap.get(id).getSpectrumGraphData() != null) {
            windowId = id;
            return chartInfoMap.get(id).getSpectrumGraphData().getEndingHarmonic();
        }
        return null;
    }

    public void setEndHarmonic(Integer endHarmonic) {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id) && chartInfoMap.get(id).getSpectrumGraphData() != null) {
            windowId = id;
            chartInfoMap.get(id).getSpectrumGraphData().setEndingHarmonic(endHarmonic);
        }
    }

    public float getSpectrumTotalAmplSpec() {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id) && chartInfoMap.get(id).getSpectrumGraphData() != null) {
            windowId = id;
            return chartInfoMap.get(id).getSpectrumGraphData().getTotalAmplSpec();
        }
        return (float) 0;
    }

    public void setSpectrumTotalAmplSpec(float totalAmplSpec) {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id) && chartInfoMap.get(id).getSpectrumGraphData() != null) {
            windowId = id;
            chartInfoMap.get(id).getSpectrumGraphData().setTotalAmplSpec(totalAmplSpec);
        }
    }

    public float getSpectrumTachSpeed() {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id) && chartInfoMap.get(id).getSpectrumGraphData() != null) {
            windowId = id;
            return chartInfoMap.get(id).getSpectrumGraphData().getTachSpeed();
        }
        return (float) 0;
    }

    public void setSpectrumTachSpeed(float tachSpeed) {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id) && chartInfoMap.get(id).getSpectrumGraphData() != null) {
            windowId = id;
            chartInfoMap.get(id).getSpectrumGraphData().setTachSpeed(tachSpeed);
        }
    }

    public String getSpectrumTachSpeedUnit() {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id) && chartInfoMap.get(id).getSpectrumGraphData() != null) {
            windowId = id;
            return chartInfoMap.get(id).getSpectrumGraphData().getTachSpeedUnit();
        }
        return null;
    }

    public void setSpectrumTachSpeedUnit(String tachSpeedUnit) {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id) && chartInfoMap.get(id).getSpectrumGraphData() != null) {
            windowId = id;
            chartInfoMap.get(id).getSpectrumGraphData().setTachSpeedUnit(tachSpeedUnit);
        }
    }

    public String getWaveTachSpeedUnit() {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id) && chartInfoMap.get(id).getSpectrumGraphData() != null) {
            windowId = id;
            return chartInfoMap.get(id).getWaveGraphData().getTachSpeedUnit();
        }
        return null;
    }

    public void setWaveTachSpeedUnit(String tachSpeedUnit) {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id) && chartInfoMap.get(id).getSpectrumGraphData() != null) {
            windowId = id;
            chartInfoMap.get(id).getWaveGraphData().setTachSpeedUnit(tachSpeedUnit);
        }
    }

    public float getWaveRmsWave() {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id) && chartInfoMap.get(id).getWaveGraphData() != null) {
            windowId = id;
            return chartInfoMap.get(id).getWaveGraphData().getRmsWave();
        }
        return (float) 0;
    }

    public void setWaveRmsWave(float rmsWave) {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id) && chartInfoMap.get(id).getWaveGraphData() != null) {
            windowId = id;
            chartInfoMap.get(id).getWaveGraphData().setRmsWave(rmsWave);
        }
    }

    public float getWaveTachSpeed() {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id) && chartInfoMap.get(id).getWaveGraphData() != null) {
            windowId = id;
            return chartInfoMap.get(id).getWaveGraphData().getTachSpeed();
        }
        return (float) 0;
    }

    public void setWaveTachSpeed(float tachSpeed) {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id) && chartInfoMap.get(id).getWaveGraphData() != null) {
            windowId = id;
            chartInfoMap.get(id).getWaveGraphData().setTachSpeed(tachSpeed);
        }
    }

    public String getAreaName() {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id)) {
            windowId = id;
            return chartInfoMap.get(id).getAreaName();
        }
        return null;
    }

    public String getAssetName() {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id)) {
            windowId = id;
            return chartInfoMap.get(id).getAssetName();
        }
        return null;
    }

    public String getPointLocationName() {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id)) {
            windowId = id;
            return chartInfoMap.get(id).getPointLocationName();
        }
        return null;
    }

    public String getSerialNum() {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id)) {
            windowId = id;
            return chartInfoMap.get(id).getSerialNum();
        }
        return null;
    }

    public String getPointName() {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id)) {
            windowId = id;
            return chartInfoMap.get(id).getPointName();
        }
        return null;
    }

    public int getSampleInterval() {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id)) {
            windowId = id;
            return chartInfoMap.get(id).getSampleInterval();
        }
        return 0;
    }

    public int getSelectedApAlByPointResolution() {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id) && chartInfoMap.get(id).getSelectedApAlByPoint() != null) {
            windowId = id;
            return chartInfoMap.get(id).getSelectedApAlByPoint().getResolution();
        }
        return 0;
    }

    public List<Integer> getEndHarmonicList() {
        List<Integer> list = new ArrayList();
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id) && chartInfoMap.get(id).getSpectrumGraphData() != null && chartInfoMap.get(id).getSpectrumGraphData().getStartingHarmonic() != null) {
            windowId = id;

            for (Integer i = chartInfoMap.get(id).getSpectrumGraphData().getStartingHarmonic() + 1; i <= 50; i++) {
                list.add(i);
            }
        }
        return list;

    }

    public List<String> getWindowParams() {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id)) {
            windowId = id;
            return chartInfoMap.get(id).getParamNameList();
        }

        return null;
    }

    public String getParamName() {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id)) {
            windowId = id;
            return chartInfoMap.get(id).getParamName();
        }
        return null;
    }

    public void setParamName(String paramName) {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id)) {
            windowId = id;
            chartInfoMap.get(id).setParamName(paramName);
        }
    }

    public List<UUIDItemVO> getWindowApSets() {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id)) {
            windowId = id;
            return chartInfoMap.get(id).getApSetList();
        }

        return null;
    }

    public UUIDItemVO getApSetItem() {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id)) {
            windowId = id;
            return chartInfoMap.get(id).getApSetItem();
        }
        return null;
    }

    public void setApSetItem(UUIDItemVO apSetItem) {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id)) {
            windowId = id;
            chartInfoMap.get(id).setApSetItem(apSetItem);
        }
    }

    public String getGraphType() {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id)) {
            windowId = id;
            return chartInfoMap.get(id).getGraphType();
        }
        return null;
    }

    public void setGraphType(String graphType) {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id)) {
            windowId = id;
            chartInfoMap.get(id).setGraphType(graphType);
        }
    }

    public List<SelectItem> getAmplitudeUnitList() {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id) && chartInfoMap.get(id).getGraphType() != null && chartInfoMap.get(id).getSelectedApAlByPoint() != null && chartInfoMap.get(id).getSelectedApAlByPoint().getSensorType() != null) {
            if (chartInfoMap.get(id).getGraphType().equalsIgnoreCase("wave")) {
                return getACParamUnitItemListBySensorTypeParamType(chartInfoMap.get(id).getSelectedApAlByPoint().getSensorType(), "waveform");
            } else if (chartInfoMap.get(id).getGraphType().equalsIgnoreCase("spectrum")) {
                return getACParamUnitItemListBySensorTypeParamType(chartInfoMap.get(id).getSelectedApAlByPoint().getSensorType(), "spectrum");
            }
        }
        return null;
    }

    public String getPointType() {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id)) {
            windowId = id;
            return chartInfoMap.get(id).getPointType();
        }
        return null;
    }

    public boolean isPlotOptionsModified() {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id)) {
            windowId = id;
            return chartInfoMap.get(id).isPlotOptionsModified();
        }
        return false;
    }

    public void setPlotOptionsModified(boolean plotOptionsModified) {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id)) {
            windowId = id;
            chartInfoMap.get(id).setPlotOptionsModified(plotOptionsModified);
        }
    }

    public String getAmplitudeUnit() {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id)) {
            windowId = id;
            return chartInfoMap.get(id).getAmplitudeUnit();
        }
        return null;
    }

    public void setAmplitudeUnit(String amplitudeUnit) {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id)) {
            windowId = id;
            chartInfoMap.get(id).setAmplitudeUnit(amplitudeUnit);
        }
    }

    public String getTimeUnit() {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id)) {
            windowId = id;
            return chartInfoMap.get(id).getTimeUnit();
        }
        return null;
    }

    public void setTimeUnit(String timeUnit) {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id)) {
            windowId = id;
            chartInfoMap.get(id).setTimeUnit(timeUnit);
        }
    }

    public String getFrequencyUnit() {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id)) {
            windowId = id;
            return chartInfoMap.get(id).getFrequencyUnit();
        }
        return null;
    }

    public void setFrequencyUnit(String frequencyUnit) {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id)) {
            windowId = id;
            chartInfoMap.get(id).setFrequencyUnit(frequencyUnit);
        }
    }

    public boolean isLogYaxis() {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id)) {
            windowId = id;
            return chartInfoMap.get(id).isSpectrumLogYAxis();
        }
        return false;
    }

    public void setLogYaxis(boolean logYaxis) {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id)) {
            windowId = id;
            chartInfoMap.get(id).setSpectrumLogYAxis(logYaxis);
        }
    }

    public boolean isDisableParamAllSelection() {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id)) {
            windowId = id;
            return chartInfoMap.get(id).isDisableParamAllSelection();
        }
        return false;
    }

    public void setDisableParamAllSelection(boolean disableParamAllSelection) {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id)) {
            windowId = id;
            chartInfoMap.get(id).setDisableParamAllSelection(disableParamAllSelection);
        }
    }

    public List<PointVO> getApAlByPointList() {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id)) {
            windowId = id;
            return chartInfoMap.get(id).getApAlByPointList();
        }
        return new ArrayList();
    }

    public String getGraphTitle() {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id)) {
            windowId = id;
            return chartInfoMap.get(id).getGraphTitle();
        }
        return null;
    }

    public String getWindowDisplayName() {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id)) {
            windowId = id;
            return chartInfoMap.get(id).getWindowDisplayName();
        }
        return null;
    }

    public String getAmpFactor() {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id)) {
            windowId = id;
            return chartInfoMap.get(id).getAmpFactor();
        }
        return null;
    }

    public void setAmpFactor(String ampFactor) {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id)) {
            windowId = id;
            chartInfoMap.get(id).setAmpFactor(ampFactor);
        }
    }

    public float getyAxisUpperBound() {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id)) {
            windowId = id;
            return chartInfoMap.get(id).getyAxisUpperBound();
        }
        return (float) 0;
    }

    public boolean isRefreshWaveAndSpectrum() {
        String id;

        if ((id = getCurrentWindowId()) != null && chartInfoMap.containsKey(id)) {
            windowId = id;
            return chartInfoMap.get(id).isRefreshWaveAndSpectrum();
        }
        return false;
    }

    public List<Integer> getStartHarmonicList() { // Decoupling not needed
        return startHarmonicList;
    }

    public List<SelectItem> getDwellList() { // Decoupling not needed
        return dwellList;
    }

    public List<SelectItem> getTimeUnitList() { // Decoupling not needed
        return timeUnitList;
    }

    public List<SelectItem> getAmpFactorList() { // Decoupling not needed
        return ampFactorList;
    }

    public List<SelectItem> getFrequencyUnitList() { // Decoupling not needed
        for (SelectItem si : frequencyUnitList) {
            if (si.getValue().equals("Orders")) {
                if (Objects.equals(getSpectrumTachSpeed(), 0.0f)) {
                    si.setLabel("Orders - Speed Unavailable");
                    si.setDisabled(true);
                } else {
                    si.setLabel("Orders");
                    si.setDisabled(false);
                }
            }
        }
        return frequencyUnitList;
    }

    public List<SelectItem> getPriorities() { // Decoupling not needed
        return priorities;
    }

    public List<SelectItem> getIssueList() { // Decoupling not needed
        return issueList;
    }

    public List<SelectItem> getActionList() { // Decoupling not needed
        return actionList;
    }

    public Map<String, ChartInfoVO> getChartInfoMap() { // Decoupling not needed
        return chartInfoMap;
    }

    public void setChartInfoMap(Map<String, ChartInfoVO> chartInfoMap) { // Decoupling not needed
        this.chartInfoMap = chartInfoMap;
    }

    public String getTimestamp() { // Decoupling not possible.
        return timestamp;
    }

    public void setTimestamp(String timestamp) { // Decoupling not possible.
        this.timestamp = timestamp;
    }

    public List<String> getChartOptionsJsList() { // Decoupling not needed
        return chartOptionsJsList;
    }

    public String getChartOptionsJs() { // Decoupling not needed
        return chartOptionsJs;
    }

    public String getWaveFormChartOptionsJs() { // Decoupling not needed
        return waveFormChartOptionsJs;
    }

    public String getSpectrumChartOptionsJs() { // Decoupling not needed
        return spectrumChartOptionsJs;
    }

    public boolean isGenerateGraph() { // Decoupling not needed
        return generateGraph;
    }

    public String getWindowId() { // Decoupling not needed
        return windowId;
    }

    public void setWindowId(String windowId) { // Decoupling not needed
        this.windowId = windowId;
    }

    public boolean isGenerateWaveAndSpecGraph() { // Decoupling not needed
        return generateWaveAndSpecGraph;
    }

    public String getGenericStr() { // Decoupling not needed
        return genericStr;
    }

    public void setGenericStr(String genericStr) { // Decoupling not needed
        this.genericStr = genericStr;
    }

    public String getXaxis() { // Decoupling not needed
        return xaxis;
    }

    public void setXaxis(String xaxis) { // Decoupling not needed
        this.xaxis = xaxis;
    }

    public String getYaxis() { // Decoupling not needed
        return yaxis;
    }

    public void setYaxis(String yaxis) { // Decoupling not needed
        this.yaxis = yaxis;
    }

    public String getYaxisTitle() { // Decoupling not needed
        return yaxisTitle;
    }

    public void setYaxisTitle(String yaxisTitle) { // Decoupling not needed
        this.yaxisTitle = yaxisTitle;
    }

    public void setItemDownload(boolean itemDownload) {
        this.itemDownload = itemDownload;
    }
}
