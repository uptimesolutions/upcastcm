/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.APALSetClient;
import com.uptime.upcastcm.http.client.AssetPresetClient;
import com.uptime.upcastcm.http.client.DevicePresetClient;
import com.uptime.upcastcm.http.client.FaultFrequenciesClient;
import com.uptime.upcastcm.http.client.PointLocationNamesClient;
import com.uptime.upcastcm.http.client.TachometerClient;
import com.uptime.upcastcm.http.utils.json.dom.PointLocationDomJsonParser;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import com.uptime.upcastcm.utils.enums.ACSensorTypeEnum;
import static com.uptime.upcastcm.utils.enums.ACSensorTypeEnum.getACSensorTypeItemList;
import com.uptime.upcastcm.utils.enums.DCSensorTypeEnum;
import static com.uptime.upcastcm.utils.enums.DCSensorTypeEnum.getDCSensorTypeItemList;
import com.uptime.upcastcm.utils.enums.ParamUnitEnum;
import com.uptime.upcastcm.utils.enums.TachReferenceUnitEnum;
import static com.uptime.upcastcm.utils.helperclass.GenericMessage.failedSuccessUploads;
import com.uptime.upcastcm.utils.vo.ApAlSetVO;
import com.uptime.upcastcm.utils.vo.FaultFrequenciesVO;
import com.uptime.upcastcm.utils.vo.TachometerVO;
import static com.uptime.upcastcm.utils.helperclass.GenericMessage.setNonAutoGrowlMsg;
import com.uptime.upcastcm.utils.helperclass.Utils;
import static com.uptime.upcastcm.utils.helperclass.Utils.distinctByKeys;
import com.uptime.upcastcm.utils.singletons.CommonUtilSingleton;
import com.uptime.upcastcm.utils.vo.AssetPresetVO;
import com.uptime.upcastcm.utils.vo.BearingCatalogVO;
import com.uptime.upcastcm.utils.vo.DevicePresetVO;
import com.uptime.upcastcm.utils.vo.DeviceVO;
import com.uptime.upcastcm.utils.vo.PointLocationNamesVO;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import org.primefaces.PrimeFaces;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.TabChangeEvent;

/**
 *
 * @author twilcox
 */
public class PresetsBean implements Serializable {
    private UserManageBean userManageBean;
    private String uploadType;
    private final List<TachometerVO> tachometerList;
    private final List<ApAlSetVO> acApSetList, dcApSetList;
    private final List<PointLocationNamesVO> pointLocationNameList;
    private final Map<UUID, List<ApAlSetVO>> apAlSetMap;
    private List<ApAlSetVO> filteredAcApSetList, filteredDcApSetList;
    private int operationType;
    private NavigationBean navigationBean;
    private final List<DevicePresetVO> devicePresetList, filteredDevicePresetList, distinctDevicePresetList;
    private List<FaultFrequenciesVO> siteFFSetFavorites, distinctsiteFFSetFavorites, filteredSiteFaultFrequencyList;
    private final List<AssetPresetVO> assetPresetsList,distinctAssetPresetsList,filteredAssetPresetsList;
    private List<String> ffsetnameList;
    /**
     * Constructor
     */
    public PresetsBean() {
        tachometerList = new ArrayList();
        siteFFSetFavorites= new ArrayList();
        acApSetList = new ArrayList();
        dcApSetList = new ArrayList();
        apAlSetMap = new HashMap();
        filteredSiteFaultFrequencyList = new ArrayList();
        distinctsiteFFSetFavorites = new ArrayList();
        devicePresetList = new ArrayList();
        filteredDevicePresetList = new ArrayList();
        distinctDevicePresetList = new ArrayList();
        pointLocationNameList = new ArrayList();
        assetPresetsList = new ArrayList();
        distinctAssetPresetsList = new ArrayList();
        filteredAssetPresetsList = new ArrayList();
        try {
            navigationBean = (NavigationBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("navigationBean");
            userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
        } catch (Exception e) {
            Logger.getLogger(PresetsBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            navigationBean = null;
            userManageBean = null;
        }
    }

    /**
     * Post Constructor
     */
    @PostConstruct
    public void init() {
        resetPage();
    }

    /**
     * Reset full page
     */
    public void resetPage() {
        resetFaultFrequenciesTab();
        apAlSetMap.clear();
        acApSetList.clear();
        dcApSetList.clear();
        tachometerList.clear();
        devicePresetList.clear();    
        assetPresetsList.clear();
        updateUI("content");
    }

     /**  
     * Reset Point Locations tab
     */
    public void resetAssetsTab() {
        assetPresetsList.clear();   
        distinctAssetPresetsList.clear();
        try {
            userManageBean.getPresetUtil().clearSiteAssetPresetList();
            userManageBean.getPresetUtil().clearGlobalAssetPresetList();
            assetPresetsList.addAll(AssetPresetClient.getInstance().getGlobalAssetPresetsByCustomer(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount()));
            if (!userManageBean.getCurrentSite().getCustomerAccount().equals(DEFAULT_UPTIME_ACCOUNT)) {
                assetPresetsList.addAll(AssetPresetClient.getInstance().getGlobalAssetPresetsByCustomer(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), DEFAULT_UPTIME_ACCOUNT));
            }
            assetPresetsList.addAll(userManageBean.getPresetUtil().getSiteAssetPresetList(userManageBean.getCurrentSite().getCustomerAccount(), userManageBean.getCurrentSite().getSiteId()));
            distinctAssetPresetsList.addAll(assetPresetsList.stream().filter(Utils.distinctByKeys(AssetPresetVO::getCustomerAccount, AssetPresetVO::getAssetPresetId)).collect(Collectors.toList()));
        } catch (Exception ex) {
            Logger.getLogger(PresetsBean.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
            assetPresetsList.clear ();
        }
    }
    public void getAssetPresetsByRowToggle(AssetPresetVO assetPresetVO){
        filteredAssetPresetsList.clear();
        
        if (assetPresetVO != null) {
            if (assetPresetVO.getAssetPresetId()!= null) {
                filteredAssetPresetsList.addAll(assetPresetsList.stream().filter(ff -> ff.getAssetPresetId() != null && ff.getAssetPresetId().toString().equals(assetPresetVO.getAssetPresetId().toString())).collect(Collectors.toList()));
          
                filteredAssetPresetsList.forEach(asset -> {
                    if(asset.getSpeedRatio()>0f){
                        asset.setRollDiameterText(null);
                        asset.setRollDiameterUnits(null);
                        asset.setSpeedRatioText(String.valueOf(asset.getSpeedRatio()));
                    }else{
                        asset.setSpeedRatioText(null);
                        asset.setRollDiameterText(String.valueOf(asset.getRollDiameter()));
                    }
                    ffsetnameList = PointLocationDomJsonParser.getInstance().populateFFSetFromJson(asset.getFfSetIds());
                    asset.setFfSetNames(String.join(", ", ffsetnameList));
                  });
            } 
        }
    }
    /**  
     * Reset Point Locations tab
     */
    public void resetPointLocationNamesTab() {
        pointLocationNameList.clear();        
        List<PointLocationNamesVO> pointLocationNamesVOs = new ArrayList();
        try {
            userManageBean.getPresetUtil().clearSitePointLocationNameList();
            userManageBean.getPresetUtil().clearGlobalPtLoNamePresetList();
            
            if (!userManageBean.getCurrentSite().getCustomerAccount().equals(DEFAULT_UPTIME_ACCOUNT)) {
                pointLocationNamesVOs.addAll(userManageBean.getPresetUtil().getGlobalptLoNameList(DEFAULT_UPTIME_ACCOUNT));
            }
            pointLocationNamesVOs.addAll(userManageBean.getPresetUtil().getGlobalptLoNameList(userManageBean.getCurrentSite().getCustomerAccount()));
            pointLocationNameList.addAll(pointLocationNamesVOs.stream().filter(Utils.distinctByKeys(PointLocationNamesVO::getCustomerAccount, PointLocationNamesVO::getPointLocationName)).collect(Collectors.toList()));
            pointLocationNameList.addAll(userManageBean.getPresetUtil().getSitePtLoNameList(userManageBean.getCurrentSite().getCustomerAccount(), userManageBean.getCurrentSite().getSiteId()));
        } catch (Exception ex) {
            Logger.getLogger(PresetsBean.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
            pointLocationNameList.clear ();
        }
    }

    /** 
     * Reset Device Presets tab
     */
    public void resetDevicePresetsTab() {
        List<DevicePresetVO> devicesPresetsLists;
        
        devicePresetList.clear();
        distinctDevicePresetList.clear();

        try {
            devicesPresetsLists = new ArrayList();
            if (!userManageBean.getCurrentSite().getCustomerAccount().equals(DEFAULT_UPTIME_ACCOUNT)) {
                devicesPresetsLists.addAll(DevicePresetClient.getInstance().getGlobalDevicePresetsByCustomer(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), DEFAULT_UPTIME_ACCOUNT));
            }
            devicesPresetsLists.addAll(DevicePresetClient.getInstance().getGlobalDevicePresetsByCustomer(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount()));
            devicePresetList.addAll(devicesPresetsLists.stream().filter(Utils.distinctByKeys(DevicePresetVO::getChannelNumber, DevicePresetVO::getSensorType, DevicePresetVO::getName, DevicePresetVO::getDeviceType, DevicePresetVO::getPresetId)).collect(Collectors.toList()));
            devicePresetList.addAll(DevicePresetClient.getInstance().getDevicePresetVOs(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), userManageBean.getCurrentSite().getSiteId()));
            distinctDevicePresetList.addAll(devicePresetList.stream().filter(Utils.distinctByKeys(DevicePresetVO::getName, DevicePresetVO::getDeviceType, DevicePresetVO::getPresetId)).collect(Collectors.toList()));

        } catch (Exception e) {
            Logger.getLogger(PresetsBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            devicePresetList.clear();
        }
    }

    /**
     * Reset Fault Frequencies tab
     */
    public void resetFaultFrequenciesTab() {
        List<FaultFrequenciesVO> customFaultFrequencies, siteFaultFrequencies;
        
        try {
            uploadType = null;
            distinctsiteFFSetFavorites.clear();
            userManageBean.getPresetUtil().clearSiteFaultFrequenciesList();
            
            siteFFSetFavorites = new ArrayList();
            siteFFSetFavorites = FaultFrequenciesClient.getInstance().getSiteFFSetFavoritesVOList(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME),userManageBean.getCurrentSite().getCustomerAccount(), userManageBean.getCurrentSite().getSiteId());
       
            customFaultFrequencies = siteFFSetFavorites.stream().filter(ff -> ff.getFfSetId() != null).filter(Utils.distinctByKeys(FaultFrequenciesVO::getCustomerAccount, FaultFrequenciesVO::getSiteId,FaultFrequenciesVO::getFfSetId)).collect(Collectors.toList());
            siteFaultFrequencies = siteFFSetFavorites.stream().filter(ff -> ff.getFfSetId() == null).collect(Collectors.toList());
          
            distinctsiteFFSetFavorites.addAll(siteFaultFrequencies.stream().filter(distinctByKeys(FaultFrequenciesVO::getFfSetName,FaultFrequenciesVO::getFfSetDesc)).collect(Collectors.toList()));
            distinctsiteFFSetFavorites.addAll(Utils.getUniqueFFList(customFaultFrequencies));
        } catch (Exception e) {
            Logger.getLogger(PresetsBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }
    
    /**
     * Reset Analysis Parameters tab
     */
    public void resetAnalysisParametersTab() {
        List<ApAlSetVO> list, vos;
        
        uploadType = null;
        apAlSetMap.clear();
        acApSetList.clear();
        dcApSetList.clear();
        
        try {
            list = new ArrayList();
            list.addAll(APALSetClient.getInstance().getCustomerApSetByCustomer(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount()));
            if (!userManageBean.getCurrentSite().getCustomerAccount().equals(DEFAULT_UPTIME_ACCOUNT)) {
                list.addAll(APALSetClient.getInstance().getCustomerApSetByCustomer(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), DEFAULT_UPTIME_ACCOUNT));
            }
            
            if (!list.isEmpty()) {
                vos = new ArrayList();
                vos.addAll(list.stream().filter(apSet -> apSet.getSiteId() == null || apSet.getSiteId().equals(userManageBean.getCurrentSite().getSiteId())).sorted(Comparator.comparing(ApAlSetVO::getApSetName)).collect(Collectors.toList()));
                vos.stream().forEachOrdered(apSet -> apAlSetMap.put(apSet.getApSetId(), null));
                
                acApSetList.addAll(vos.stream().filter(apSet -> apSet.getFmax() > 0 && apSet.getResolution() > 0 && apSet.getPeriod() > 0 && apSet.getSampleRate() > 0).collect(Collectors.toList()));
                dcApSetList.addAll(vos.stream().filter(apSet -> apSet.getFmax() == 0 && apSet.getResolution() == 0 && apSet.getPeriod() == 0 && apSet.getSampleRate() == 0).collect(Collectors.toList()));
            }
        } catch (Exception e) {
            Logger.getLogger(PresetsBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            apAlSetMap.clear();
            acApSetList.clear();
            dcApSetList.clear();
        }
    }

    /**
     * Reset Tachometers tab
     */
    public void resetTachometersTab() {
        uploadType = null;
        tachometerList.clear();
        
        try {
            userManageBean.getPresetUtil().clearSiteTachList();
            
            if (!userManageBean.getCurrentSite().getCustomerAccount().equals(DEFAULT_UPTIME_ACCOUNT)) {
                tachometerList.addAll(TachometerClient.getInstance().getGlobalTachometersByCustomer(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), DEFAULT_UPTIME_ACCOUNT));
            }
            tachometerList.addAll(userManageBean.getPresetUtil().getGlobalTachList(userManageBean.getCurrentSite().getCustomerAccount()));
            tachometerList.addAll(userManageBean.getPresetUtil().getSiteTachList(userManageBean.getCurrentSite().getCustomerAccount(), userManageBean.getCurrentSite().getSiteId()));
            tachometerList.stream().forEach(a -> {
                if("Constant Speed".equalsIgnoreCase(a.getTachType())){
                    a.setTachType(userManageBean.getResourceBundleString("label_tachType_constantSpeed"));
                }
            });
            tachometerList.sort(Comparator.comparing(TachometerVO::getTachName));
        } catch (Exception e) {
            Logger.getLogger(PresetsBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            tachometerList.clear();
        }
    }

    /**
     * Reset apAlSetMap values to null
     */
    public void resetApAlSetMapForAc() {
        apAlSetMap.keySet().forEach(id -> {
            if (acApSetList.stream().anyMatch(apSet -> apSet.getApSetId().equals(id))) {
                apAlSetMap.put(id, null);
            }
        });
    }

    /**
     * Reset apAlSetMap values to null
     */
    public void resetApAlSetMapForDc() {
        apAlSetMap.keySet().forEach(id -> {
            if (dcApSetList.stream().anyMatch(apSet -> apSet.getApSetId().equals(id))) {
                apAlSetMap.put(id, null);
            }
        });
    }

    /**
     * Deletes the given object
     *
     * @param obj, Object
     * @param tab, String Object
     */
    public void delete(Object obj, String tab) {
        List<PointLocationNamesVO> pointLocationNamesVOs;
        List<FaultFrequenciesVO> faultFrequenciesVOs;        
        List<DevicePresetVO> devicePresetVOs;
        List<AssetPresetVO> assetPresetVOs;
        
        PointLocationNamesVO pointLocationNamesVO;
        FaultFrequenciesVO faultFrequenciesVO;
        BearingCatalogVO bearingCatalogVO;
        DevicePresetVO devicePresetVO;
        AssetPresetVO assetPresetsVO;
        
        if (obj != null && tab != null) {
            try {
                switch (tab) {
                    case "sitePreferences":
                        faultFrequenciesVO = (FaultFrequenciesVO) obj;
                        
                        if(faultFrequenciesVO.getFfSetId() != null){
                            faultFrequenciesVOs = siteFFSetFavorites.stream().filter(ff -> ff.getFfSetId() != null && ff.getFfSetId().equals(faultFrequenciesVO.getFfSetId())).collect(Collectors.toList());
                            FaultFrequenciesClient.getInstance().deleteCustomFaultFrequencies(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), faultFrequenciesVOs);
                        } else {                                 
                            bearingCatalogVO = new BearingCatalogVO();                                   
                            bearingCatalogVO.setBearingId(faultFrequenciesVO.getFfSetName().split(" - ")[1].trim());
                            bearingCatalogVO.setCustomerAcct(faultFrequenciesVO.getCustomerAccount());
                            bearingCatalogVO.setSiteId(faultFrequenciesVO.getSiteId());
                            bearingCatalogVO.setVendorId(faultFrequenciesVO.getFfSetName().split(" - ")[0].trim());                           
                            FaultFrequenciesClient.getInstance().deleteSiteBearingFavorites(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), bearingCatalogVO);
                        }
                        
                        setNonAutoGrowlMsg("growl_remove_ffset_item_items");
                        resetFaultFrequenciesTab();
                        updateUI("faultFrequencies");
                        break;
                    case "analysisParameters":
                        break;
                    case "tachometers":
                        TachometerClient.getInstance().deleteSiteTachometers(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), (TachometerVO) obj);
                        resetTachometersTab();
                        updateUI("tachometers");
                        break;
                    case "devices":
                        devicePresetVO = (DevicePresetVO) obj;
                        
                        devicePresetVOs = devicePresetList.stream().filter(d -> d.getPresetId().compareTo(devicePresetVO.getPresetId()) == 0).collect(Collectors.toList());
                        DevicePresetClient.getInstance().deletePresetDevices(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), devicePresetVOs);
                        
                        setNonAutoGrowlMsg("growl_delete_item_items");
                        resetDevicePresetsTab();
                        updateUI("devices");
                        break;
                    case "pointLocationNames":
                        pointLocationNamesVO = (PointLocationNamesVO) obj;
                        
                        pointLocationNamesVOs = pointLocationNameList.stream().filter(d ->d.getCustomerAccount().equalsIgnoreCase(pointLocationNamesVO.getCustomerAccount()) && d.getSiteId() != null && d.getSiteId().compareTo(pointLocationNamesVO.getSiteId()) == 0 && d.getPointLocationName().equalsIgnoreCase(pointLocationNamesVO.getPointLocationName())).collect(Collectors.toList());
                        PointLocationNamesClient.getInstance().deleteSitePointLocationNames(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), pointLocationNamesVOs.get(0));
                        
                        setNonAutoGrowlMsg("growl_delete_item_items");
                        resetPointLocationNamesTab();
                        updateUI("pointLocationNames");
                        break;
                    case "assets":
                        assetPresetsVO = (AssetPresetVO) obj;  
                         assetPresetVOs = assetPresetsList.stream().filter(ff -> ff.getAssetPresetId() != null && ff.getAssetPresetId().toString().equals(assetPresetsVO.getAssetPresetId().toString())).collect(Collectors.toList());
                        if(assetPresetsVO.getSiteId() != null){
                          AssetPresetClient.getInstance().deleteSiteAssetPresetVO(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), assetPresetVOs);
                        }else{
                          AssetPresetClient.getInstance().deleteGlobalAssetPresetVO(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), assetPresetVOs);
                        }
                        setNonAutoGrowlMsg("growl_delete_item_items");
                        resetAssetsTab();
                        updateUI("assets");                       
                        break;
                }
            } catch (Exception e) {
                Logger.getLogger(PresetsBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            }
        }
    }

    /**
     * Update the UI based on the given value
     *
     * @param value
     */
    private void updateUI(String value) {
        try {
            switch (value) {
                case "content":
                    PrimeFaces.current().ajax().update("presetsTabViewId");
                    break;
                case "faultFrequencies":
                    PrimeFaces.current().ajax().update("presetsTabViewId:faultFrequenciesFormId");
                    break;
                case "analysisParameters":
                    PrimeFaces.current().executeScript("PF('acApSetTable').clearFilters()");
                    PrimeFaces.current().executeScript("PF('dcApSetTable').clearFilters()");
                    PrimeFaces.current().ajax().update("presetsTabViewId:analysisParametersFormId");
                    break;
                case "tachometers":
                    PrimeFaces.current().ajax().update("presetsTabViewId:tachometersFormId");
                    break;
                case "devices":
                    PrimeFaces.current().ajax().update("presetsTabViewId:devicesFormId");
                    break;
                case "pointLocationNames":
                    PrimeFaces.current().ajax().update("presetsTabViewId:pointLocationNamesFormId");
                    break;
                 case "assets":
                    PrimeFaces.current().ajax().update("presetsTabViewId:assetsFormId");
                    break;
            }
        } catch (Exception e) {
            Logger.getLogger(PresetsBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Set the selectedTachometer, operationType, and open the dialog box
     *
     * @param tachometerVO, TachometerVO object
     * @param operationType, int
     */
    public void setTachometerDialog(TachometerVO tachometerVO, int operationType) {
        try {
            if (tachometerVO != null && operationType > 0 && operationType <= 2 && navigationBean != null) {
                if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("copyEditTachometersBean") == null) {
                    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("copyEditTachometersBean", new CopyEditTachometersBean());
                    ((CopyEditTachometersBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("copyEditTachometersBean")).init();
                }
                ((CopyEditTachometersBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("copyEditTachometersBean")).presetFields(operationType, tachometerVO);
                if (operationType == 1) {
                    navigationBean.updateDialog("edit-tachometers");
                } else {
                    navigationBean.updateDialog("copy-tachometers");
                }
            } else {
                if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("createTachometerBean") == null) {
                    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("createTachometerBean", new CreateTachometerBean());
                    ((CreateTachometerBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("createTachometerBean")).init();
                }
                navigationBean.updateDialog("create-tachometer");
            }
        } catch (Exception e) {
            Logger.getLogger(PresetsBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Set the selectedApAlSet, operationType, and open the dialog box
     *
     * @param apAlSetVO, ApAlSetVO object
     * @param operationType, int
     */
    public void setApAlSetDialog(ApAlSetVO apAlSetVO, int operationType) {
        try {
            if (operationType >= 0 && operationType <= 2 && navigationBean != null) {
                if ((operationType == 2 || operationType == 1) && apAlSetVO != null) {
                    if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("copyEditApAlSetBean") == null) {
                        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("copyEditApAlSetBean", new CopyEditApAlSetBean());
                    }
                    if (apAlSetVO.getSiteId() == null) {
                        ((CopyEditApAlSetBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("copyEditApAlSetBean")).presetFields(operationType, apAlSetVO);
                        ((CopyEditApAlSetBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("copyEditApAlSetBean")).presetFields(userManageBean.getCurrentSite());
                    } else {
                        ((CopyEditApAlSetBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("copyEditApAlSetBean")).presetFields(operationType, apAlSetVO);
                    }
                    navigationBean.updateDialog("copy-edit-ap-al-set");
                } else if (operationType == 0 && apAlSetVO == null) {
                    if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("createApAlSetBean") == null) {
                        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("createApAlSetBean", new CreateApAlSetBean());
                        ((CreateApAlSetBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("createApAlSetBean")).init();
                    }
                    navigationBean.updateDialog("create-ap-al-set");
                }
            }
        } catch (Exception e) {
            Logger.getLogger(PresetsBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }
    
    /**
     * Open add AlSet dialog box
     * 
     * @param apAlSetVO, ApAlSetVO object
     */
    public void addAlSet(ApAlSetVO apAlSetVO) {
        try {
            if (apAlSetVO != null) {
                if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("addAlSetBean") == null) {
                    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("addAlSetBean", new AddAlSetBean());
                    ((AddAlSetBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("addAlSetBean")).init();
                }
                ((AddAlSetBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("addAlSetBean")).presetFields(apAlSetVO);
                navigationBean.updateDialog("add-al-set"); 
            }
        } catch (Exception e) {
            Logger.getLogger(PresetsBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }
    
    /**
     *  open Bearing Catalog  dialog box
     *
     */
    public void bearingCatalogDialog() {
        try {
            if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("createBearingCatalogBean") == null) {
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("createBearingCatalogBean", new CreateBearingCatalogBean());
                ((CreateBearingCatalogBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("createBearingCatalogBean")).init();
            }
            navigationBean.updateDialog("create-bearing-catalog"); 
        } catch (Exception e) {
            Logger.getLogger(PresetsBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }
    
    /**
     *  open Bearing Catalog  dialog box
     *
     */
    public void customSetsDialog() {
        try { 
            if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("createCustomFaultFrequecySetsBean") == null) {
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("createCustomFaultFrequecySetsBean", new CreateCustomFaultFrequecySetsBean());
            }  
            ((CreateCustomFaultFrequecySetsBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("createCustomFaultFrequecySetsBean")).init();
            navigationBean.updateDialog("create-custom-fault-frequecy-sets"); 
        } catch (Exception e) {
            Logger.getLogger(PresetsBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }
    
    /**
     *
     * @param devicePresetVO, DevicePresetVOs object
     * @param operationType, int
     */
    public void setDevicesDialog(DevicePresetVO devicePresetVO, int operationType) {
        try {
            if (operationType >= 0 && operationType <= 2 && navigationBean != null) {
                if ((operationType == 2 || operationType == 1) && devicePresetVO != null) {
                    if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("copyEditPresetsDeviceBean") == null) {
                        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("copyEditPresetsDeviceBean", new CopyEditPresetsDeviceBean());
                        ((CopyEditPresetsDeviceBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("copyEditPresetsDeviceBean")).init();
                    }
                    ((CopyEditPresetsDeviceBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("copyEditPresetsDeviceBean")).presetFields(operationType, devicePresetVO);
                    if (operationType == 1) {
                        navigationBean.updateDialog("edit-presets-device");
                    } else {
                        navigationBean.updateDialog("copy-presets-device");
                    }
                } else if (operationType == 0 && devicePresetVO == null) {
                    if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("createPresetsDeviceBean") == null) {
                        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("createPresetsDeviceBean", new CreatePresetsDeviceBean());
                        ((CreatePresetsDeviceBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("createPresetsDeviceBean")).init();
                    }
                    navigationBean.updateDialog("create-presets-device");
                }
            }
        } catch (Exception e) {
            Logger.getLogger(PresetsBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     *
     * @param pointLocationNameVO, PointLocationNamesVO object
     * @param operationType, int
     */
    public void setPointLocationNameDialog(PointLocationNamesVO pointLocationNameVO, int operationType) {
        try {
            if (operationType >= 0 && operationType <= 2 && navigationBean != null) {
                if ((operationType == 2 || operationType == 1) && pointLocationNameVO != null) {
                    if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("copyEditPointLocationNameBean") == null) {
                        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("copyEditPointLocationNameBean", new CopyEditPointLocationNameBean());
                        ((CopyEditPointLocationNameBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("copyEditPointLocationNameBean")).init();
                    }            
                    ((CopyEditPointLocationNameBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("copyEditPointLocationNameBean")).presetFields(operationType, pointLocationNameVO);
                    if (operationType == 1) {
                        navigationBean.updateDialog("edit-point-location-name");
                    } else {
                        navigationBean.updateDialog("copy-point-location-name");
                    }
                } else if (operationType == 0 && pointLocationNameVO == null) {
                    if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("createPointLocationNameBean") == null) {
                        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("createPointLocationNameBean", new CreatePointLocationNameBean());
                        ((CreatePointLocationNameBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("createPointLocationNameBean")).init();
                    }
                    navigationBean.updateDialog("create-point-location-name");
                }
            }
        } catch (Exception e) {
            Logger.getLogger(PresetsBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

     /**
     *
     * @param assetPresetVO, AssetPresetVO object
     * @param operationType, int
     */
    public void setAssetsDialog(AssetPresetVO assetPresetVO, int operationType) {
        try {
            if (operationType >= 0 && operationType <= 2 && navigationBean != null) {
                if ((operationType == 2 || operationType == 1) && assetPresetVO != null) {
                   // assetPresetVO.setType("site");
                    if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("copyEditPresetsAssetBean") == null) {
                        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("copyEditPresetsAssetBean", new CopyEditPresetsAssetBean());
                        ((CopyEditPresetsAssetBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("copyEditPresetsAssetBean")).init();
                    }
                    ((CopyEditPresetsAssetBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("copyEditPresetsAssetBean")).presetFields(operationType, assetPresetVO);
                    if (operationType == 1) {
                        navigationBean.updateDialog("edit-presets-asset");
                    } else {
                        navigationBean.updateDialog("copy-presets-asset");
                    }
                } else if (operationType == 0 && assetPresetVO == null) {
                    if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("createPresetsAssetBean") == null) {
                        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("createPresetsAssetBean", new CreatePresetsAssetBean());
                      }
                     ((CreatePresetsAssetBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("createPresetsAssetBean")).init();
                   
                    navigationBean.updateDialog("create-presets-asset");
                }
            }
        } catch (Exception e) {
            Logger.getLogger(PresetsBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Do a lookup of the site_ap_al_sets and global_ap_al_sets tables based on the
     * given info
     *
     * @param apAlSetVO, ApAlSetVO Object
     * @param dcRow, boolean, true if a DC row, else it is an AC row
     */
    public void apAlLookupByRowToggle(ApAlSetVO apAlSetVO, boolean dcRow) {
        List<ApAlSetVO> list;
        
        if (userManageBean != null && apAlSetVO != null && apAlSetVO.getApSetId() != null) {
            if (apAlSetMap.get(apAlSetVO.getApSetId()) == null) {
                try {
                    apAlSetMap.put(apAlSetVO.getApSetId(), new ArrayList());
                    if (apAlSetVO.getSiteId() == null) {
                        if ((list = APALSetClient.getInstance().getGlobalApAlSetsByCustomerApSetId(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), apAlSetVO.getApSetId())) == null || list.isEmpty()) {
                            list = APALSetClient.getInstance().getGlobalApAlSetsByCustomerApSetId(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), DEFAULT_UPTIME_ACCOUNT, apAlSetVO.getApSetId());
                        }
                        apAlSetMap.get(apAlSetVO.getApSetId()).addAll(list);
                    } else {
                        apAlSetMap.get(apAlSetVO.getApSetId()).addAll(APALSetClient.getInstance().getSiteApAlSetsByCustomerSiteIdApSetId(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), apAlSetVO.getSiteId(), apAlSetVO.getApSetId()));
                    }
                    apAlSetMap.get(apAlSetVO.getApSetId()).sort(Comparator.comparing(ApAlSetVO::getAlSetName).thenComparing(ApAlSetVO::getParamName));
                } catch (Exception e) {
                    Logger.getLogger(PresetsBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
                    apAlSetMap.put(apAlSetVO.getApSetId() ,null);
                }
            } else {
                apAlSetMap.put(apAlSetVO.getApSetId(), null);
            }
            
            if (dcRow) {
                apAlSetVO.setSelectedAlSet(new SelectItem(UUID.fromString("00000000-0000-0000-0000-000000000000"), userManageBean.getResourceBundleString("label_all")));
                apAlSetVO.setParamUnits((apAlSetMap.get(apAlSetVO.getApSetId()) != null && !apAlSetMap.get(apAlSetVO.getApSetId()).isEmpty()) ? apAlSetMap.get(apAlSetVO.getApSetId()).get(0).getParamUnits() : null);
            } else {
                apAlSetVO.setSelectedAlSet((apAlSetMap.get(apAlSetVO.getApSetId()) != null && !apAlSetMap.get(apAlSetVO.getApSetId()).isEmpty()) ? new SelectItem (apAlSetMap.get(apAlSetVO.getApSetId()).get(0).getAlSetId(), apAlSetMap.get(apAlSetVO.getApSetId()).get(0).getAlSetName()) : null);
            } 
        }
    }
    
    /**
     * Validate given account with the logged in account
     * 
     * @param customerAccount, String Object
     * @return boolean, If accounts equal each other return true, else false.
     */
    public boolean renderAddAlSet(String customerAccount) {
        if (userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null && !userManageBean.getCurrentSite().getCustomerAccount().isEmpty()) {
            return customerAccount != null && customerAccount.equals(userManageBean.getCurrentSite().getCustomerAccount());
        }
        return false;
    }

    public void getSiteFaultFrequenciesByRowToggle(FaultFrequenciesVO faultFrequenciesVO) {
        filteredSiteFaultFrequencyList.clear();
        if (faultFrequenciesVO != null) {
            if (faultFrequenciesVO.getFfSetId() != null) {
                filteredSiteFaultFrequencyList.addAll(siteFFSetFavorites.stream().filter(ff -> ff.getFfSetId() != null && ff.getFfSetId().equals(faultFrequenciesVO.getFfSetId())).collect(Collectors.toList()));
            } else {
                filteredSiteFaultFrequencyList.addAll(siteFFSetFavorites.stream().filter(ff -> ff.getFfSetId() == null && ff.getFfSetName().equals(faultFrequenciesVO.getFfSetName())).collect(Collectors.toList()));
            }
        }
    }

    public void getDevicePresetsByRowToggle(DevicePresetVO devicePresetVO) {
        List<DevicePresetVO> list;
        List<DeviceVO> deviceVOs;
        
        filteredDevicePresetList.clear();        
        if (devicePresetVO != null) {
            deviceVOs = CommonUtilSingleton.getInstance().populateDeviceTypeList(devicePresetVO.getDeviceType());
            list = devicePresetList.stream().filter(device -> device.getName() != null && devicePresetVO.getName() != null && device.getName().equals(devicePresetVO.getName()) && device.getDeviceType().equalsIgnoreCase(devicePresetVO.getDeviceType()) && device.getPresetId().compareTo(devicePresetVO.getPresetId())==0).collect(Collectors.toList());
            list.forEach(device -> {
                DeviceVO  deviceVO; 
                
                if (devicePresetVO.getDeviceType().toUpperCase().equalsIgnoreCase("TS1X")) {
                    deviceVO = deviceVOs.stream().filter(d -> (byte)d.getChannelNum() == device.getChannelNumber() && d.getChannelType().equalsIgnoreCase(device.getChannelType())).findAny().orElse(null);
                } else {
                     deviceVO = deviceVOs.stream().filter(d -> (byte)d.getChannelNum() == device.getChannelNumber() && d.getRestrictions().equalsIgnoreCase(device.getSensorType())).findAny().orElse(null);
                }
                 if (device.isAlarmEnabled()) {
                        device.setAlarmEnabledValue(userManageBean.getResourceBundleString("label_true"));
                    } else {
                        device.setAlarmEnabledValue(userManageBean.getResourceBundleString("label_false"));
                    }
                device.setChannelNumberLabel(deviceVO.getChannelNumLabel());
                if (device.getChannelType().equalsIgnoreCase("AC")) {
                    device.setSensorType(ACSensorTypeEnum.getACLabelByValue(device.getSensorType()));
                } else {
                    device.setSensorType(DCSensorTypeEnum.getDCLabelByValue(device.getSensorType()));
                }
            });          
            
            filteredDevicePresetList.addAll(list);
        }
    }
    
    /**
     * on tab change event handler
     *
     * @param event, TabChangeEvent Object
     */
    public void onTabChange(TabChangeEvent event) {
        if (event != null) {
            switch (event.getTab().getId()) {
                case "faultFrequenciesTabId":
                    resetFaultFrequenciesTab();
                    updateUI("faultFrequencies");
                    break;
                case "analysisParametersTabId":
                    resetAnalysisParametersTab();
                    updateUI("analysisParameters");
                    break;
                case "tachometersTabId":
                    resetTachometersTab();
                    updateUI("tachometers");
                    break;
                case "devicesTabId":
                    resetDevicePresetsTab();
                    updateUI("devices");
                    break;
                case "pointLocationNamesTabId":
                    resetPointLocationNamesTab();
                    updateUI("pointLocationNames");
                    break;
                 case "assetsTabId":
                    resetAssetsTab();
                    updateUI("assets");
                    break;
            }
        }
    }

    /**
     * Upload the tackometers from the given file
     *
     * @param event, FileUploadEvent Object
     */
    public void tachometerUpload(FileUploadEvent event) {
        String line;
        TachometerVO tachometerVO;
        List<TachometerVO> list = new ArrayList();
        String[] elements;
        int errorCount, alreadyExist, validationNum;

        if (userManageBean != null && userManageBean.getCurrentSite() != null && event != null && event.getFile() != null && uploadType != null) {
            try {
                errorCount = 0;
                alreadyExist = 0;

                try ( BufferedReader br = new BufferedReader(new InputStreamReader(event.getFile().getInputStream()))) {
                    br.readLine(); // Skip Header

                    switch (uploadType) {
                        case "Constant Speed":
                            while ((line = br.readLine()) != null) {
                                try {
                                    if ((validationNum = tachValidation(elements = line.split(","))) == 0) {
                                        tachometerVO = new TachometerVO();
                                        tachometerVO.setCustomerAccount(userManageBean.getCurrentSite().getCustomerAccount());
                                        tachometerVO.setSiteId(userManageBean.getCurrentSite().getSiteId());
                                        tachometerVO.setTachName(elements[0]);
                                        tachometerVO.setTachId(UUID.randomUUID());
                                        tachometerVO.setTachReferenceSpeed(Float.parseFloat(elements[1]));
                                        tachometerVO.setTachType(uploadType);
                                        if (tachometerVO.getTachName() != null && !tachometerVO.getTachName().isEmpty()) {
                                            list.add(tachometerVO);
                                        }
                                    } else if (validationNum == 1) {
                                        alreadyExist++;
                                    } else {
                                        errorCount++;
                                    }
                                } catch (Exception e) {
                                    errorCount++;
                                }
                            }
                            break;
                    }
                }
                if (!list.isEmpty()) {
                    TachometerClient.getInstance().createSiteTachometers(list, errorCount, alreadyExist);
                    resetTachometersTab();
                    updateUI("tachometers");
                } else {
                    failedSuccessUploads(0, errorCount, alreadyExist);
                }
            } catch (Exception e) {
                Logger.getLogger(PresetsBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            }
        }
    }
    
    private int tachValidation(String[] elements) {
        if (uploadType != null) {
            switch (uploadType) {
                case "Constant Speed":
                    if (elements != null && elements.length == 3 && 
                            elements[0] != null && !elements[0].isEmpty() &&
                            elements[1] != null && !elements[1].isEmpty() && elements[2] != null && !elements[2].isEmpty()) {
                        try {
                            Float.parseFloat(elements[1]);
                            Float.parseFloat(elements[2]);
                            if (!tachometerList.stream().anyMatch(tach -> tach.getTachName().equals(elements[0]))) {
                                return 0;
                            } else {
                                return 1;
                            }
                        } catch (Exception ex) {}
                    }
                    
            }
        }
        return 2;
    }
    
    /**
     * Return the tachReferenceRpm value if relevant to the tach type else return "-" 
     * @param tachometerVO, TachometerVO Object
     * @return String Object
     */
    public String tachReferenceRpmTableDisplay (TachometerVO tachometerVO) {
        if (tachometerVO != null && tachometerVO.getTachType() != null) {
            switch(tachometerVO.getTachType()) {
                case "Constant Speed":
                    return String.valueOf(tachometerVO.getTachReferenceSpeed());
                default:
                    return String.valueOf(tachometerVO.getTachReferenceSpeed());
            }
        }
        return "-";
    }
    
    /**
     * Convert null or empty String to "N/A"
     * @param value, String Object
     * @return String Object
     */
    public String convertNullToNA(String value) {
        if (value == null || value.isEmpty()) {
            return "N/A";
        } else {
            return value;
        }
    }

    /**
     * Query the acApSetList for all items that contain the given value for the
     * field apSetName
     *
     * @param query, String Object
     * @return List Object of String Objects
     */
    public List<String> queryAcApSetNameList(String query) {
        try {
            return acApSetList.stream().filter(apSet -> apSet.getApSetName().toLowerCase().contains(query.toLowerCase())).map(ApAlSetVO::getApSetName).sorted().collect(Collectors.toList());
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Query the dcApSetList for all items that contain the given value for the
     * field apSetName
     *
     * @param query, String Object
     * @return List Object of String Objects
     */
    public List<String> queryDcApSetNameList(String query) {
        try {
            return dcApSetList.stream().filter(apSet -> apSet.getApSetName().toLowerCase().contains(query.toLowerCase())).map(ApAlSetVO::getApSetName).sorted().collect(Collectors.toList());
        } catch (Exception e) {
            return null;
        }
    }
    
    /**
     * Query AlSetItemList based on the given query
     * @param query, String Object
     * @return, List Object of SelectItem Objects
     */
    public List<SelectItem> queryAcAlSetItemList(String query) {
        FacesContext context;
        UUID apSetId;
        List<SelectItem> list;
        
        try {
            context = FacesContext.getCurrentInstance();
            apSetId = (UUID) UIComponent.getCurrentComponent(context).getAttributes().get("apSetId");
            if (apAlSetMap.get(apSetId) != null && !apAlSetMap.get(apSetId).isEmpty()) {
                list = new ArrayList();
                apAlSetMap.get(apSetId).stream().filter(vo -> !list.stream().anyMatch(selectItem -> ((UUID)selectItem.getValue()).equals(vo.getAlSetId()))).forEachOrdered(vo -> list.add(new SelectItem(vo.getAlSetId(), vo.getAlSetName())));
                if (query == null || query.isEmpty()) {
                    return list.stream().sorted(Comparator.comparing(SelectItem::getLabel)).collect(Collectors.toList());
                } else {
                    return list.stream().filter(item -> item.getLabel().toLowerCase().contains(query.toLowerCase())).sorted(Comparator.comparing(SelectItem::getLabel)).collect(Collectors.toList());
                }
            }
        } catch (Exception e) {
            Logger.getLogger(PresetsBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
        return null;
    }
    
    /**
     * Query AlSetItemList based on the given query
     * @param query, String Object
     * @return, List Object of SelectItem Objects
     */
    public List<SelectItem> queryDcAlSetItemList(String query) {
        FacesContext context;
        UUID apSetId;
        List<SelectItem> list, tmpList;
        
        try {
            context = FacesContext.getCurrentInstance();
            apSetId = (UUID) UIComponent.getCurrentComponent(context).getAttributes().get("apSetId");
            if (userManageBean != null && apAlSetMap.get(apSetId) != null && !apAlSetMap.get(apSetId).isEmpty()) {
                list = new ArrayList();
                tmpList = new ArrayList();
                
                apAlSetMap.get(apSetId).stream().filter(vo -> !tmpList.stream().anyMatch(selectItem -> ((UUID)selectItem.getValue()).equals(vo.getAlSetId()))).forEachOrdered(vo -> tmpList.add(new SelectItem(vo.getAlSetId(), vo.getAlSetName())));
                
                list.add(new SelectItem(UUID.fromString("00000000-0000-0000-0000-000000000000"), userManageBean.getResourceBundleString("label_all"))); // Add All option for DC
                if (query == null || query.isEmpty()) {
                    list.addAll(tmpList.stream().sorted(Comparator.comparing(SelectItem::getLabel)).collect(Collectors.toList()));
                } else {
                    list.addAll(tmpList.stream().filter(item -> item.getLabel().toLowerCase().contains(query.toLowerCase())).sorted(Comparator.comparing(SelectItem::getLabel)).collect(Collectors.toList()));
                }
                
                return list;
            }
        } catch (Exception e) {
            Logger.getLogger(PresetsBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
        return null;
    }

    /**
     * Return a List of ApAlSetVO based on a given apSetId
     *
     * @param apSetId, UUID Object
     * @return Object List of ApAlSetVO Objects
     */
    public List<ApAlSetVO> getApAlSetList(UUID apSetId) {
        return apAlSetMap.get(apSetId);
    }

    /**
     * Return a List of ApAlSetVO based with the field paramName being distinct
     *
     * @param apSetId, UUID Object
     * @return Object List of ApAlSetVO Objects
     */
    public List<ApAlSetVO> getParamList(UUID apSetId) {
        List<ApAlSetVO> list = new ArrayList();
        
        try {
            if (apAlSetMap.get(apSetId) != null && !apAlSetMap.get(apSetId).isEmpty()) {
                apAlSetMap.get(apSetId).stream().filter(object -> !list.stream().anyMatch(vo -> vo.getParamName().equals(object.getParamName()))).forEachOrdered(object -> list.add(object));
            }
        } catch (Exception e) {
            Logger.getLogger(PresetsBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list.clear();
        }
        
        return list;
    }
    
    public List<ApAlSetVO> getAlSetList(ApAlSetVO apAlSetVO) {
        List<ApAlSetVO> list = new ArrayList();
        
        try {
            if (apAlSetVO != null && apAlSetVO.getApSetId() != null && apAlSetVO.getSelectedAlSet() != null && apAlSetVO.getSelectedAlSet().getValue() != null && apAlSetMap.get(apAlSetVO.getApSetId()) != null && !apAlSetMap.get(apAlSetVO.getApSetId()).isEmpty()) {
                if (apAlSetVO.getSelectedAlSet().getValue().toString().equals("00000000-0000-0000-0000-000000000000")) {
                    list = new ArrayList(apAlSetMap.get(apAlSetVO.getApSetId()));
                } else {
                    list = new ArrayList(apAlSetMap.get(apAlSetVO.getApSetId()).stream().filter(vo -> vo.getAlSetId().equals((UUID)apAlSetVO.getSelectedAlSet().getValue())).collect(Collectors.toList()));
                }
                
                 list.stream().forEach(a -> {
                    if (a.isLowFaultActive()) {
                        a.setLowFaultActiveValue(userManageBean.getResourceBundleString("label_true"));
                    } else {
                        a.setLowFaultActiveValue(userManageBean.getResourceBundleString("label_false"));
                    }
                    if (a.isLowAlertActive()) {
                        a.setLowAlertActiveValue(userManageBean.getResourceBundleString("label_true"));
                    } else {
                        a.setLowAlertActiveValue(userManageBean.getResourceBundleString("label_false"));
                    }
                    
                    if (a.isHighAlertActive()) {
                        a.setHighAlertActiveValue(userManageBean.getResourceBundleString("label_true"));
                    } else {
                        a.setHighAlertActiveValue(userManageBean.getResourceBundleString("label_false"));
                    }
                    if (a.isHighFaultActive()) {
                        a.setHighFaultActiveValue(userManageBean.getResourceBundleString("label_true"));
                    } else {
                        a.setHighFaultActiveValue(userManageBean.getResourceBundleString("label_false"));
                    }
                });
                
            }
        } catch (Exception e) {
            Logger.getLogger(PresetsBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            list = new ArrayList();
        }
        
        return list;
    }
    
    /**
     * Return a list of the sensorType from the table acApSetList
     *
     * @return List Object of String Objects
     */
   public List<SelectItem> getAcSensorTypeList() {
        List<SelectItem> fullList, list; 
        
        try {
            list = new ArrayList();
            fullList = getACSensorTypeItemList();
            
            for (String type : acApSetList.stream().filter(apSet -> apSet.getSensorType() != null && !apSet.getSensorType().isEmpty()).map(ApAlSetVO::getSensorType).distinct().sorted().collect(Collectors.toList())) {
                try {
                    list.add(fullList.stream().filter(item -> ((String)item.getValue()).equalsIgnoreCase(type)).findFirst().get());
                } catch (Exception e) {}
            }
            return list.stream().sorted(Comparator.comparing(SelectItem::getLabel)).collect(Collectors.toList());
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Return a list of the sensorType from the table dcApSetList
     *
     * @return List Object of String Objects
     */
    public List<SelectItem> getDcSensorTypeList() {
        List<SelectItem> fullDcList, dclist; 
        
        try {
            dclist = new ArrayList();
            fullDcList = getDCSensorTypeItemList();
            
            for (String type : dcApSetList.stream().filter(apSet -> apSet.getSensorType() != null && !apSet.getSensorType().isEmpty()).map(ApAlSetVO::getSensorType).distinct().sorted().collect(Collectors.toList())) {
                try {
                    dclist.add(fullDcList.stream().filter(item -> ((String)item.getValue()).equalsIgnoreCase(type)).findFirst().get());
                } catch (Exception e) {}
            }
            return dclist.stream().sorted(Comparator.comparing(SelectItem::getLabel)).collect(Collectors.toList());
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Return a list of the fmax from the table acApSetList
     *
     * @return List Object of Integer Objects
     */
    public List<Integer> getFmaxList() {
        try {
            return acApSetList.stream().map(ApAlSetVO::getFmax).distinct().sorted().collect(Collectors.toList());
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Return a list of the resolution from the table acApSetList
     *
     * @return List Object of Integer Objects
     */
    public List<Integer> getResolutionList() {
        try {
            return acApSetList.stream().map(ApAlSetVO::getResolution).distinct().sorted().collect(Collectors.toList());
        } catch (Exception e) {
            return null;
        }
    }
    
    
    public String getDCLabelByValue(String value) {
        return DCSensorTypeEnum.getDCLabelByValue(value);
    }

    public String getACLabelByValue(String value) {
        return ACSensorTypeEnum.getACLabelByValue(value);
    }
    
    public String getTachReferenceUnitLabelByValue(String value) {
        return TachReferenceUnitEnum.getTachReferenceUnitLabelByValue(value);
    }
    
    public String getParamUnitLabelByValue(String value) {
        return ParamUnitEnum.getParamUnitLabelByValue(value);
    }
    
    public List<TachometerVO> getTachometerList() {
        return tachometerList;
    }

    public int getOperationType() {
        return operationType;
    }

    public String getUploadType() {
        return uploadType;
    }

    public void setUploadType(String uploadType) {
        this.uploadType = uploadType;
    }

    public List<ApAlSetVO> getAcApSetList() {
        return acApSetList;
    }

    public List<ApAlSetVO> getDcApSetList() {
        return dcApSetList;
    }

    public List<ApAlSetVO> getFilteredAcApSetList() {
        return filteredAcApSetList;
    }

    public void setFilteredAcApSetList(List<ApAlSetVO> filteredAcApSetList) {
        this.filteredAcApSetList = filteredAcApSetList;
    }

    public List<ApAlSetVO> getFilteredDcApSetList() {
        return filteredDcApSetList;
    }

    public void setFilteredDcApSetList(List<ApAlSetVO> filteredDcApSetList) {
        this.filteredDcApSetList = filteredDcApSetList;
    }

    public List<FaultFrequenciesVO> getFilteredSiteFaultFrequencyList() {
        return filteredSiteFaultFrequencyList;
    }

    public List<FaultFrequenciesVO> getDistinctsiteFFSetFavorites() {
        return distinctsiteFFSetFavorites;
    }
    
     public List<DevicePresetVO> getDevicePresetList() {
        return devicePresetList;
    }

    public List<DevicePresetVO> getFilteredDevicePresetList() {
        return filteredDevicePresetList;
    }

    public List<DevicePresetVO> getDistinctDevicePresetList() {
        return distinctDevicePresetList;
    }

    public List<PointLocationNamesVO> getPointLocationNameList() {
        return pointLocationNameList;
    }

    public List<FaultFrequenciesVO> getSiteFFSetFavorites() {
        return siteFFSetFavorites;
    }

    public List<AssetPresetVO> getAssetPresetsList() {
        return assetPresetsList;
    }

    public List<AssetPresetVO> getDistinctAssetPresetsList() {
        return distinctAssetPresetsList;
    }

    public List<AssetPresetVO> getFilteredAssetPresetsList() {
        return filteredAssetPresetsList;
    }
        
}
