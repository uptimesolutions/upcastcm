/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.UserClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import static com.uptime.upcastcm.utils.enums.UserGroupEnum.getListOfSelectItemsFromSiteItems;
import com.uptime.upcastcm.utils.vo.SiteUsersVO;
import com.uptime.upcastcm.utils.vo.UserPreferencesVO;
import com.uptime.upcastcm.utils.vo.UserSitesVO;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import java.util.HashMap;
import java.util.Map;
import javax.faces.model.SelectItem;
import org.primefaces.PrimeFaces;
import org.primefaces.event.RowEditEvent;

/**
 *
 * @author gsingh
 */
public class UsersBean implements Serializable {
    private UserManageBean userManageBean;
    private NavigationBean navigationBean;
    private List<SiteUsersVO> usersList, clonedUsersList;
    private final List<SelectItem> userRoles;
    private Map<String, String> rolesMap;
    private SiteUsersVO selectedSiteUsersVO;

    /**
     * Constructor
     */
    public UsersBean() {
        rolesMap = new HashMap();
        userRoles = new ArrayList();
        selectedSiteUsersVO = null;

        try {
            navigationBean = (NavigationBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("navigationBean");
            userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
            userRoles.addAll(getListOfSelectItemsFromSiteItems());
            userRoles.forEach(item -> rolesMap.put((String) item.getValue(), item.getLabel()));
        } catch (Exception e) {
            Logger.getLogger(UsersBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            navigationBean = null;
            userManageBean = null;
        }
    }

    /**
     * Post Constructor
     */
    @PostConstruct
    public void init() {
        try {
            if (userManageBean != null && userManageBean.getCurrentSite() != null) {
                usersList = UserClient.getInstance().getSiteUsersListBySiteIdCustomerAccount(ServiceRegistrarClient.getInstance().getServiceHostURL(USER_PREFERENCE_SERVICE_NAME), userManageBean.getCurrentSite().getSiteId().toString(), userManageBean.getCurrentSite().getCustomerAccount());
                cloneUsersList();
            } else {
                Logger.getLogger(UsersBean.class.getName()).log(Level.WARNING, "userManageBean is null");
            }
        } catch (Exception e) {
            Logger.getLogger(UsersBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    public void onCreateUser() {
        if (navigationBean != null) {
            navigationBean.updateDialog("create-users");
        }
    }

    public void viewRoleInformation() {
        if (navigationBean != null) {
            navigationBean.updateDialog("role-information");
        }
    }

    /**
     * Edit the user row Check whether any field has been changed only then
     * perform update. Compare uvo with same record from clonedUsersList.
     *
     * @param uvo, SiteUsersVO Object
     */
    public void onUserRowEdit(SiteUsersVO uvo) {
        SiteUsersVO tmp = null;

        if (userManageBean != null && userManageBean.getSelectedCustomersVO() != null && userManageBean.getSelectedCustomersVO().getCustomerName() != null && userManageBean.getSelectedCustomersVO().getCustomerAccount() != null) {
            for (SiteUsersVO suvo : clonedUsersList) {
                if (suvo.getSiteId().equals(uvo.getSiteId()) && suvo.getUserId().equals(uvo.getUserId())) {
                    tmp = suvo;
                    break;
                }
            }

            if (tmp != null && !Objects.equals(uvo, tmp)) {
                if (UserClient.getInstance().updateUsers(ServiceRegistrarClient.getInstance().getServiceHostURL(USER_PREFERENCE_SERVICE_NAME), uvo)) {
                    Logger.getLogger(UsersBean.class.getName()).log(Level.INFO, "User updated successfully.");
                } else {
                    Logger.getLogger(UsersBean.class.getName()).log(Level.INFO, "Error in User updation:{0}", uvo);
                }
                cloneUsersList();
            }
            PrimeFaces.current().executeScript("enableEdit()");
        }
    }
    
    public void onCancel(RowEditEvent event){
        PrimeFaces.current().executeScript("enableEdit()");
    }

    /**
     * Delete the given user
     */
    public void deleteUser() {
        List<UserSitesVO> userList;
        UserPreferencesVO upVO;
        
        try {
            if (selectedSiteUsersVO != null && userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null) {
                UserClient.getInstance().deleteUsers(ServiceRegistrarClient.getInstance().getServiceHostURL(USER_PREFERENCE_SERVICE_NAME), selectedSiteUsersVO);
                if ((userList = UserClient.getInstance().getUserSiteListByUserId(ServiceRegistrarClient.getInstance().getServiceHostURL(USER_PREFERENCE_SERVICE_NAME), selectedSiteUsersVO.getUserId())) == null || userList.isEmpty()) {

                    // User do not exist for other sites. 
                    // Delete entry from user-preference table, if item found
                    if (UserClient.getInstance().getUserPreferencesByPK(ServiceRegistrarClient.getInstance().getServiceHostURL(USER_PREFERENCE_SERVICE_NAME), selectedSiteUsersVO.getUserId()) != null) {
                        upVO = new UserPreferencesVO();
                        upVO.setUserId(selectedSiteUsersVO.getUserId());
                        upVO.setCustomerAccount(selectedSiteUsersVO.getCustomerAccount());
                        UserClient.getInstance().deleteUserPreferences(ServiceRegistrarClient.getInstance().getServiceHostURL(USER_PREFERENCE_SERVICE_NAME), upVO);
                    }
                    
                } else {
                    Logger.getLogger(UsersBean.class.getName()).log(Level.INFO, "User {0} exist for other sites. Hence does not deleted in ldap", selectedSiteUsersVO.getUserId());
                }
                usersList.remove(selectedSiteUsersVO);
                cloneUsersList();
                PrimeFaces.current().ajax().update("users-form");
            }
        } catch (Exception e) {
            Logger.getLogger(UsersBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
        PrimeFaces.current().executeScript("addDisableFunctionality()");
        
    }

    public void cloneUsersList() {
        clonedUsersList = new ArrayList<>();
        usersList.forEach(tmp -> clonedUsersList.add(new SiteUsersVO(tmp)));
    }

    public List<SiteUsersVO> getUsersList() {
        return usersList;
    }

    public void setUsersList(List<SiteUsersVO> usersList) {
        this.usersList = usersList;
    }

    public List<SelectItem> getUserRoles() {
        return userRoles;
    }

    public Map<String, String> getRolesMap() {
        return rolesMap;
    }

    public void setRolesMap(Map<String, String> rolesMap) {
        this.rolesMap = rolesMap;
    }

    public List<SiteUsersVO> getClonedUsersList() {
        return clonedUsersList;
    }

    public void setClonedUsersList(List<SiteUsersVO> clonedUsersList) {
        this.clonedUsersList = clonedUsersList;
    }

    public void setSelectedSiteUsersVO(SiteUsersVO selectedSiteUsersVO) {
        this.selectedSiteUsersVO = selectedSiteUsersVO;
    }
}
