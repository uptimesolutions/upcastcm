/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.bean;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.Serializable;
import java.util.Enumeration;

/**
 *
 * @author ksimmons
 */
public class LogoutBean implements Serializable {
 
    /**
     * Close Session and Redirect to login page for Ldap Users
     */
    public void doLdapLogout() {
        Enumeration<String> enumeration;
        HttpServletRequest request;
        HttpSession session;
        String attrName;
        
        try{
            request = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
            
            // Get Redirect URL, if saml authentication is used, logout by redirecting the the slo_url from customers table for the current customer account
            Logger.getLogger(LogoutBean.class.getName()).log(Level.INFO,"****Redirecting to login page*****");
            FacesContext.getCurrentInstance().getExternalContext().redirect(request.getContextPath());
            Logger.getLogger(LogoutBean.class.getName()).log(Level.INFO,"****Redirected*****");
            
            // Close session
            if(request.getSession(false) != null){
                Logger.getLogger(LogoutBean.class.getName()).log(Level.INFO, "Old Session id {0}" , request.getSession().getId());
                request.getSession(false).removeAttribute("nameId");
                request.getSession(false).removeAttribute("displayName");
                request.getSession(false).removeAttribute("company");
                request.getSession(false).removeAttribute("accountNumber");
                
                session = request.getSession(false);
                enumeration = session.getAttributeNames();
                while (enumeration.hasMoreElements()) {
                    Logger.getLogger(LogoutBean.class.getName()).log(Level.INFO, "Session attr removed {0}", (attrName = enumeration.nextElement()));
                    session.removeAttribute(attrName);
                }
                
                session.invalidate();
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().clear();
                request.getSession(true);
                Logger.getLogger(LogoutBean.class.getName()).log(Level.INFO, "New Session id {0}" , request.getSession().getId());
                Logger.getLogger(LogoutBean.class.getName()).log(Level.INFO,"*****Invalidated Tomcat session*****");
            }
        } catch(Exception e){
            Logger.getLogger(LogoutBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }
    
    /**
     * Redirect for Saml Users
     */
    public void doSamlRedirect() {
        UserManageBean userManageBean;
        
        try{
            // Get Redirect URL, if saml authentication is used, logout by redirecting the the slo_url from customers table for the current customer account
            if ((userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean")) != null && userManageBean.getLoginCustomerVO() != null && userManageBean.getLoginCustomerVO().getSloUrl() != null && !userManageBean.getLoginCustomerVO().getSloUrl().isEmpty()) {
                Logger.getLogger(LogoutBean.class.getName()).log(Level.INFO,"****Redirecting to {0}", userManageBean.getLoginCustomerVO().getSloUrl());
                FacesContext.getCurrentInstance().getExternalContext().redirect(userManageBean.getLoginCustomerVO().getSloUrl());
            } else {
                Logger.getLogger(LogoutBean.class.getName()).log(Level.INFO,"****Redirect URL Not Found!!!");
            }
        } catch(Exception e){
            Logger.getLogger(LogoutBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }
    
    /**
     * Keep the Session alive and active
     */
    public void keepSessionAlive(){
        Logger.getLogger(LogoutBean.class.getName()).log(Level.INFO, "Session Extended");
        HttpServletRequest request;
        
        request = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
        request.getSession();
    }
}
