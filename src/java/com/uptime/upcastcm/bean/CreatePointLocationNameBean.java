/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.PointLocationNamesClient;
import com.uptime.upcastcm.utils.ApplicationConstants;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import static com.uptime.upcastcm.utils.helperclass.GenericMessage.setNonAutoGrowlMsg;
import com.uptime.upcastcm.utils.vo.PointLocationNamesVO;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import org.primefaces.PrimeFaces;

/**
 *
 * @author Joseph
 */
public class CreatePointLocationNameBean implements Serializable {
    private UserManageBean userManageBean;
    private PointLocationNamesVO pointLocationNamesVO;
    private String siteOrGlobal;

    /**
     * Constructor
     */
    public CreatePointLocationNameBean() {
        try {
            userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
        } catch (Exception e) {
            Logger.getLogger(CreatePointLocationNameBean.class.getName()).log(Level.SEVERE, "CreatePointLocationNameBean Constructor Exception {0}", e.toString());
            userManageBean = null;
        }
    }

    /**
     * Post Constructor
     */
    @PostConstruct
    public void init() {
        resetPage();
        if (userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null) {
            Logger.getLogger(CreatePointLocationNameBean.class.getName()).log(Level.INFO, "**customerAccount to be set : {0}", userManageBean.getCurrentSite().getCustomerAccount());
        }
    }

    public void resetPage() {
        siteOrGlobal = null;
        pointLocationNamesVO = new PointLocationNamesVO();
        if (userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null) {
            pointLocationNamesVO.setCustomerAccount(userManageBean.getCurrentSite().getCustomerAccount());
        }
    }

    public void onSiteChange() {
        pointLocationNamesVO.setPointLocationName(null);
    }
    
    public void onPointLocationNameChange() {
        Logger.getLogger(CreatePointLocationNameBean.class.getName()).log(Level.INFO, "CreatePointLocationNameBean.updateBean() called. ***********");
        
        if (userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null) {
            if (pointLocationNamesVO.getPointLocationName() != null && !pointLocationNamesVO.getPointLocationName().trim().isEmpty() && siteOrGlobal.equalsIgnoreCase("site")) {
                if (PointLocationNamesClient.getInstance().getSitePointLocationNamesByPK(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), userManageBean.getCurrentSite().getSiteId(), pointLocationNamesVO.getPointLocationName()).size() > 0) {
                    pointLocationNamesVO.setPointLocationName(null);
                    setNonAutoGrowlMsg("growl_validate_point_location_name");
                }
            } else if (pointLocationNamesVO.getPointLocationName() != null && !pointLocationNamesVO.getPointLocationName().trim().isEmpty() && siteOrGlobal.equalsIgnoreCase("global")) {
                if (PointLocationNamesClient.getInstance().getGlobalPointLocationNamesByPK(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), ApplicationConstants.DEFAULT_UPTIME_ACCOUNT, pointLocationNamesVO.getPointLocationName()).size() > 0) {
                    pointLocationNamesVO.setPointLocationName(null);
                    setNonAutoGrowlMsg("growl_validate_point_location_name");
                } else {
                    if (PointLocationNamesClient.getInstance().getGlobalPointLocationNamesByPK(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), pointLocationNamesVO.getPointLocationName()).size() > 0) {
                        pointLocationNamesVO.setPointLocationName(null);
                        setNonAutoGrowlMsg("growl_validate_point_location_name");
                    }
                }
            } else {
                pointLocationNamesVO.setPointLocationName(null);
            }
        }
    }

    public void submit() {
        if (userManageBean != null && userManageBean.getCurrentSite() != null) {
            if (siteOrGlobal.equalsIgnoreCase("site")) {
                pointLocationNamesVO.setSiteId(userManageBean.getCurrentSite().getSiteId());
                pointLocationNamesVO.setSiteName(userManageBean.getCurrentSite().getSiteName());
                if (PointLocationNamesClient.getInstance().createSitePointLocationNames(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), pointLocationNamesVO)) {
                    setNonAutoGrowlMsg("growl_create_item_items");
                    Logger.getLogger(CreateFaultFrequenciesBean.class.getName()).log(Level.INFO, "Site Point Location Names created successfully. {0}", new Object[]{pointLocationNamesVO});
                } else {
                    Logger.getLogger(CreateFaultFrequenciesBean.class.getName()).log(Level.INFO, "Error in creating Site Point Location Names. frequenciesVO.getSiteName()*********** {0}", new Object[]{pointLocationNamesVO.getSiteName()});
                }
            } else {
                pointLocationNamesVO.setSiteId(null);
                pointLocationNamesVO.setSiteName(null);
                if (PointLocationNamesClient.getInstance().createGlobalPointLocationNames(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), pointLocationNamesVO)) {
                    setNonAutoGrowlMsg("growl_create_item_items");
                    userManageBean.getPresetUtil().clearGlobalPtLoNamePresetList();
                    Logger.getLogger(CreateFaultFrequenciesBean.class.getName()).log(Level.INFO, "Global Point Location Names created successfully. {0}", new Object[]{pointLocationNamesVO});
                } else {
                    Logger.getLogger(CreateFaultFrequenciesBean.class.getName()).log(Level.INFO, "Error in creating Global Point Location Names. siteFaultFrequenciesVO.getSiteName()*********** {0}", new Object[]{pointLocationNamesVO.getSiteName()});
                }
            }
            ((PresetsBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("presetsBean")).resetPointLocationNamesTab();
            PrimeFaces.current().ajax().update("presetsTabViewId:pointLocationNamesFormId");
            PrimeFaces.current().executeScript("PF('defaultDlg').hide()");
        }
    }

    public PointLocationNamesVO getPointLocationNamesVO() {
        return pointLocationNamesVO;
    }

    public void setPointLocationNamesVO(PointLocationNamesVO pointLocationNamesVO) {
        this.pointLocationNamesVO = pointLocationNamesVO;
    }

    public String getSiteOrGlobal() {
        return siteOrGlobal;
    }

    public void setSiteOrGlobal(String siteOrGlobal) {
        this.siteOrGlobal = siteOrGlobal;
    }

}
