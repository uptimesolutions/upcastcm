/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author ksimmons
 */
public class TestBean implements Serializable{
    private String username;
    private ArrayList<String> headers;
    private ArrayList<String> sessionAttributs;

    /**
     * Post Constructor
     */
    public TestBean() {
        username = null;
        headers = null;
        sessionAttributs = null;
    }
    
    @PostConstruct 
    public void init(){
        HttpServletRequest request;
        Iterator<String> iterator;
        Enumeration headerNames;
        
        request = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
        iterator = FacesContext.getCurrentInstance().getExternalContext().getSessionMap().keySet().iterator();
        
        headerNames = request.getHeaderNames();
        String key = null;
        StringBuilder value = null;
        headers = new ArrayList();
        sessionAttributs = new ArrayList();
        
        while (headerNames.hasMoreElements()) {
            value = new StringBuilder();
            key = (String) headerNames.nextElement();
            //System.out.print("*********KEY: "+key);
            value.append(key);
            value.append(": ");
            value.append(request.getHeader(key));
            //System.out.print("*********VALUE: "+request.getHeader(key));
            headers.add(value.toString());
        }
        while (iterator.hasNext()) {
            value = new StringBuilder();
            key = (String) iterator.next();
            //System.out.print("*********KEY: "+key);
            value.append(key);
            value.append(": ");
            value.append(FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(key));
            if(key.compareTo("nameId") == 0){
                username = FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(key).toString();
            }
            //System.out.print("*********VALUE: "+request.getHeader(key));
            sessionAttributs.add(value.toString());
        }
    }
    
    public String getUsername() {
        return username;
    }

    public ArrayList getHeaders() {
        return headers;
    }

    public ArrayList<String> getSessionAttributs() {
        return sessionAttributs;
    }
    
}
