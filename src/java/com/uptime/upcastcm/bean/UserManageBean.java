package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import static com.uptime.client.http.servlet.AbstractServiceManagerServlet.getPropertyValue;
import static com.uptime.client.utils.JsonConverterUtil.DATE_MONTH_FORMATTER;
import static com.uptime.client.utils.JsonConverterUtil.DATE_FORMATTER;
import static com.uptime.client.utils.JsonConverterUtil.DATE_TIME_FORMATTER;
import static com.uptime.client.utils.JsonConverterUtil.DATE_TIME_OFFSET_FORMATTER;
import com.uptime.upcastcm.http.client.AreaClient;
import com.uptime.upcastcm.http.client.AssetClient;
import com.uptime.upcastcm.http.client.CustomerClient;
import com.uptime.upcastcm.http.client.PointClient;
import com.uptime.upcastcm.http.client.PointLocationClient;
import com.uptime.upcastcm.http.client.SiteClient;
import com.uptime.upcastcm.http.client.UserClient;
import com.uptime.upcastcm.utils.ApplicationConstants;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import com.uptime.upcastcm.utils.enums.AmpFactorsEnum;
import static com.uptime.upcastcm.utils.enums.LocaleEnum.getLocaleItemList;
import com.uptime.upcastcm.utils.enums.ParamUnitEnum;
import static com.uptime.upcastcm.utils.enums.ParamUnitEnum.getACParamUnitItemListBySensorTypeParamType;
import static com.uptime.upcastcm.utils.enums.ParamUnitEnum.getDCParamUnitItemListBySensorType;
import static com.uptime.upcastcm.utils.enums.UserGroupEnum.getValueByGroupInfo;
import com.uptime.upcastcm.utils.helperclass.DefaultUnits;
import com.uptime.upcastcm.utils.helperclass.Filters;
import com.uptime.upcastcm.utils.helperclass.PresetUtil;
import com.uptime.upcastcm.utils.vo.AreaVO;
import com.uptime.upcastcm.utils.vo.AssetVO;
import com.uptime.upcastcm.utils.vo.CustomersVO;
import com.uptime.upcastcm.utils.vo.PointLocationVO;
import com.uptime.upcastcm.utils.vo.PointVO;
import com.uptime.upcastcm.utils.vo.SiteVO;
import com.uptime.upcastcm.utils.vo.UserPreferencesVO;
import com.uptime.upcastcm.utils.vo.UserSitesVO;
import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;
import org.primefaces.PrimeFaces;

public class UserManageBean implements Serializable {
    private List<UserSitesVO> userSiteList;
    private List<String> localeDisplayNameList;
    private NavigationBean navigationBean;

    private final Map<String, Map<String, List<SelectItem>>> regionCountrySiteMap;
    private final List<Locale> LOCALE_LIST;
    private final List<CustomersVO> customerList;
    private final List<Integer> groupIndexList;
    private final Map<String, String> timezoneMap = new HashMap();
    private final List<String> timezoneList = new ArrayList();

    private String userName, userId, page, selectedTimezone, displayedTimeZone;
    private ResourceBundle activeResourceBundle;
    private Locale activeLocale;
    private UserSitesVO currentSite;
    private UserPreferencesVO userPreferencesVO, accountUserPreferencesVO;
    private DefaultUnits defaultUnits, accountDefaultUnits;
    private ZoneId zoneId;
    private boolean globalGroups, providedCustomerRole;
    private Filters selectedFilters, initialNavFilter;
    private CustomersVO selectedCustomersVO, loginCustomerVO;
    
    private final String PATH;
    private final PresetUtil presetUtil;
   
    /**
     * Constructor
     */
    public UserManageBean() {
        PATH = "com/uptime/upcastcm/utils/properties/lang/resourceBundle";
        LOCALE_LIST = getLocaleItemList();
        presetUtil = new PresetUtil();
        groupIndexList = new ArrayList();
        regionCountrySiteMap = new HashMap();
        customerList = new ArrayList();
        page = null;
    }

    /**
     * Post Constructor
     */
    @PostConstruct
    public void init() {
        Logger.getLogger(UserManageBean.class.getName()).log(Level.INFO, "***init***");
        HttpServletRequest request;
        ExternalContext externalContext;
        String customerRole;
        boolean found;
        List<SiteVO> siteVOList;
        List<AreaVO> areaVOList;
        List<AssetVO> assetVOList;
        List<PointLocationVO> pointLocationVOList;
        List<PointVO> pointVOList;
        int index;
        
        // Add this to session if not already
        if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean") == null) {
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("userManageBean", this);
        }
        
        if (page == null) {
            groupIndexList.clear();
            regionCountrySiteMap.clear();
            customerList.clear();
            globalGroups = false;
            providedCustomerRole = false;
            selectedFilters = new Filters();
            initialNavFilter = null;
            selectedCustomersVO = null;
            loginCustomerVO = null;

            try {
                externalContext = FacesContext.getCurrentInstance().getExternalContext();
                request = (HttpServletRequest) externalContext.getRequest();

                // Set loginCustomerVO
                if (request.getSession().getAttribute("accountNumber") != null) {
                    loginCustomerVO = CustomerClient.getInstance().getCustomersByPK(ServiceRegistrarClient.getInstance().getServiceHostURL(USER_PREFERENCE_SERVICE_NAME), request.getSession().getAttribute("accountNumber").toString());
                    Logger.getLogger(UserManageBean.class.getName()).log(Level.INFO, "User Customer account from request : {0}", loginCustomerVO.getCustomerAccount());
                    Logger.getLogger(UserManageBean.class.getName()).log(Level.INFO, "User Customer name from request : {0}", loginCustomerVO.getCustomerName());
                    Logger.getLogger(UserManageBean.class.getName()).log(Level.INFO, "User Customer fqdn from request : {0}", loginCustomerVO.getFqdn());
                    Logger.getLogger(UserManageBean.class.getName()).log(Level.INFO, "User Customer Logo from request : {0}", loginCustomerVO.getLogo());
                    Logger.getLogger(UserManageBean.class.getName()).log(Level.INFO, "User Customer Skin from request : {0}", loginCustomerVO.getSkin());
                } else {
                    loginCustomerVO = null;
                    Logger.getLogger(UserManageBean.class.getName()).log(Level.INFO, "User Customer account from request : null");
                }

                // Set userName
                if (request.getSession().getAttribute("displayName") != null) {
                    userName = request.getSession().getAttribute("displayName").toString();
                    Logger.getLogger(UserManageBean.class.getName()).log(Level.INFO, "Display name from request : {0}", userName);
                } else {
                    userName = null;
                    Logger.getLogger(UserManageBean.class.getName()).log(Level.INFO, "Display name from request : null");
                }

                // Set groupIndexList and globalGroups
                if (request.getSession().getAttribute("globalGroupList") != null) {
                    for (String group : (List<String>) request.getSession().getAttribute("globalGroupList")) {   
                        if ((index = getValueByGroupInfo(group, true)) > -1) {
                            groupIndexList.add(index);
                        }
                    }
                    globalGroups = !groupIndexList.isEmpty();
                }

                // Get userId
                if (request.getSession().getAttribute("nameId") != null) {
                    userId = request.getSession().getAttribute("nameId").toString();
                    Logger.getLogger(UserManageBean.class.getName()).log(Level.INFO, "User Id from session : {0}", userId);
                } else {
                    userId = null;
                    Logger.getLogger(UserManageBean.class.getName()).log(Level.INFO, "User Id from session : null");
                }

                // Set accountUserPreferencesVO
                if (request.getSession().getAttribute("userVO") == null) {
                    if (loginCustomerVO == null || loginCustomerVO.getCustomerAccount() == null || userId == null || 
                            (accountUserPreferencesVO = UserClient.getInstance().getUserPreferencesByPK(ServiceRegistrarClient.getInstance().getServiceHostURL(USER_PREFERENCE_SERVICE_NAME), userId)) == null || 
                            !accountUserPreferencesVO.getCustomerAccount().equals(loginCustomerVO.getCustomerAccount())) {

                        accountUserPreferencesVO = new UserPreferencesVO();
                        accountUserPreferencesVO.setUserId(userId);
                        accountUserPreferencesVO.setCustomerAccount(loginCustomerVO.getCustomerAccount());
                        accountUserPreferencesVO.setLocaleDisplayName(DEFAULT_LOCALE_DISPLAY_NAME);
                        accountUserPreferencesVO.setLanguage(DEFAULT_LOCALE_DISPLAY_NAME);
                        accountUserPreferencesVO.setTimezone(DEFAULT_TIME_ZONE);
                    } else {
                        accountUserPreferencesVO.setLocaleDisplayName(accountUserPreferencesVO.getLanguage());

                        // Set Language and LocaleDisplayName to default if null or empty
                        if(accountUserPreferencesVO.getLocaleDisplayName() == null || accountUserPreferencesVO.getLocaleDisplayName().isEmpty()) {
                            accountUserPreferencesVO.setLocaleDisplayName(DEFAULT_LOCALE_DISPLAY_NAME);
                            accountUserPreferencesVO.setLanguage(DEFAULT_LOCALE_DISPLAY_NAME);
                        }

                        // Set Timezone to default if null or empty
                        if(accountUserPreferencesVO.getTimezone() == null || accountUserPreferencesVO.getTimezone().isEmpty()) {
                            accountUserPreferencesVO.setTimezone(DEFAULT_TIME_ZONE);
                        }
                    }

                    Logger.getLogger(UserManageBean.class.getName()).log(Level.INFO, "***************Set userVO attribute");
                    request.getSession().setAttribute("userVO", accountUserPreferencesVO);
                } else {
                    accountUserPreferencesVO = (UserPreferencesVO) request.getSession().getAttribute("userVO");
                }

                // Set accountDefaultUnits
                if (accountUserPreferencesVO.getDefaultUnits() != null) {
                    accountDefaultUnits = new DefaultUnits(accountUserPreferencesVO.getDefaultUnits());
                } else {
                    accountDefaultUnits = new DefaultUnits();
                }

                // Set page and initialNavFilter if provided
                if (!globalGroups) {
                    if (pageCheck(request) && loginCustomerVO != null) {
                        initialNavFilter = new Filters();

                        try {
                            // Set Initial Site 
                            if (request.getSession().getAttribute("siteId") != null) {
                                initialNavFilter.setSite(new SelectItem(UUID.fromString(request.getSession().getAttribute("siteId").toString())));
                                request.getSession().removeAttribute("siteId");

                                if ((siteVOList = SiteClient.getInstance().getSiteByCustomerSite(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), loginCustomerVO.getCustomerAccount(), initialNavFilter.getSite().getValue().toString())) != null && !siteVOList.isEmpty()) {
                                    initialNavFilter.getSite().setLabel(siteVOList.get(0).getSiteName());
                                    siteVOList.clear();
                                } else {
                                    throw new Exception("Initial Site: " + initialNavFilter.getSite().getValue().toString() + " not found for the Customer:" + loginCustomerVO.getCustomerName());
                                }

                                // Set Initial Area 
                                if (request.getSession().getAttribute("areaId") != null) {
                                    initialNavFilter.setArea(new SelectItem(UUID.fromString(request.getSession().getAttribute("areaId").toString())));
                                    request.getSession().removeAttribute("areaId");
                                    if ((areaVOList = AreaClient.getInstance().getAreaSiteByPK(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), loginCustomerVO.getCustomerAccount(), (UUID)initialNavFilter.getSite().getValue(), (UUID)initialNavFilter.getArea().getValue())) != null && !areaVOList.isEmpty()) {
                                        initialNavFilter.getArea().setLabel(areaVOList.get(0).getAreaName());
                                        areaVOList.clear();
                                    } else {
                                        throw new Exception("Initial Area: " + initialNavFilter.getArea().getValue().toString() + " not found for the Customer:" + loginCustomerVO.getCustomerName());
                                    }

                                    // Set Initial Asset 
                                    if (request.getSession().getAttribute("assetId") != null) {
                                        initialNavFilter.setAsset(new SelectItem(UUID.fromString(request.getSession().getAttribute("assetId").toString())));
                                        request.getSession().removeAttribute("assetId");
                                        if ((assetVOList = AssetClient.getInstance().getAssetSiteAreaByPK(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), loginCustomerVO.getCustomerAccount(), initialNavFilter.getSite().getValue().toString(), initialNavFilter.getArea().getValue().toString(), initialNavFilter.getAsset().getValue().toString())) != null && !assetVOList.isEmpty()) {
                                            initialNavFilter.getAsset().setLabel(assetVOList.get(0).getAssetName());
                                            assetVOList.clear();
                                        } else {
                                            throw new Exception("Initial Asset: " + initialNavFilter.getAsset().getValue().toString() + " not found for the Customer:" + loginCustomerVO.getCustomerName());
                                        }

                                        // Set Initial PointLocation 
                                        if (request.getSession().getAttribute("pointLocationId") != null) {
                                            initialNavFilter.setPointLocation(new SelectItem(UUID.fromString(request.getSession().getAttribute("pointLocationId").toString())));
                                            request.getSession().removeAttribute("pointLocationId");
                                            if ((pointLocationVOList = PointLocationClient.getInstance().getPointLocationsByPK(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), loginCustomerVO.getCustomerAccount(), (UUID)initialNavFilter.getSite().getValue(), (UUID)initialNavFilter.getArea().getValue(), (UUID)initialNavFilter.getAsset().getValue(), (UUID)initialNavFilter.getPointLocation().getValue())) != null && !pointLocationVOList.isEmpty()) {
                                                initialNavFilter.getPointLocation().setLabel(pointLocationVOList.get(0).getPointLocationName());
                                                pointLocationVOList.clear();
                                            } else {
                                                throw new Exception("Initial PointLocation: " + initialNavFilter.getPointLocation().getValue().toString() + " not found for the Customer:" + loginCustomerVO.getCustomerName());
                                            }

                                            // Set Initial PointLocation 
                                            if (request.getSession().getAttribute("pointId") != null) {
                                                initialNavFilter.setPoint(new SelectItem(UUID.fromString(request.getSession().getAttribute("pointId").toString())));
                                                request.getSession().removeAttribute("pointId");
                                                if ((pointVOList = PointClient.getInstance().getPointsByCustomerSiteAreaAssetPointLocationPoint(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), loginCustomerVO.getCustomerAccount(), (UUID)initialNavFilter.getSite().getValue(), (UUID)initialNavFilter.getArea().getValue(), (UUID)initialNavFilter.getAsset().getValue(), (UUID)initialNavFilter.getPointLocation().getValue(), (UUID)initialNavFilter.getPoint().getValue(), true)) != null && !pointVOList.isEmpty()) {
                                                    initialNavFilter.getPoint().setLabel(pointVOList.get(0).getPointName());
                                                    pointVOList.clear();
                                                } else {
                                                    throw new Exception("Initial Point: " + initialNavFilter.getPoint().getValue().toString() + " not found for the Customer:" + loginCustomerVO.getCustomerName());
                                                }
                                            } else {
                                                Logger.getLogger(UserManageBean.class.getName()).log(Level.INFO, "pointId : null");
                                            }
                                        } else {
                                            Logger.getLogger(UserManageBean.class.getName()).log(Level.INFO, "pointLocationId : null");
                                            request.getSession().removeAttribute("pointId");
                                        }
                                    } else {
                                        Logger.getLogger(UserManageBean.class.getName()).log(Level.INFO, "assetId : null");
                                        request.getSession().removeAttribute("pointLocationId");
                                        request.getSession().removeAttribute("pointId");
                                    }
                                } else {
                                    Logger.getLogger(UserManageBean.class.getName()).log(Level.INFO, "areaId : null");
                                    request.getSession().removeAttribute("assetId");
                                    request.getSession().removeAttribute("pointLocationId");
                                    request.getSession().removeAttribute("pointId");
                                }
                            } else {
                                Logger.getLogger(UserManageBean.class.getName()).log(Level.INFO, "siteId : null");
                                initialNavFilter = null;
                                request.getSession().removeAttribute("areaId");
                                request.getSession().removeAttribute("assetId");
                                request.getSession().removeAttribute("pointLocationId");
                                request.getSession().removeAttribute("pointId");
                                
                                if (accountUserPreferencesVO.getDefaultSite() == null)  {
                                    page = "home";
                                }
                            }   
                        } catch (Exception ex) {
                            Logger.getLogger(UserManageBean.class.getName()).log(Level.WARNING, ex.getMessage(), ex);
                            request.getSession().removeAttribute("siteId");
                            request.getSession().removeAttribute("areaId");
                            request.getSession().removeAttribute("assetId");
                            request.getSession().removeAttribute("pointLocationId");
                            request.getSession().removeAttribute("pointId");
                            initialNavFilter = null;
                            page = "home";
                        }
                    } 

                    // Page is provided, but no Ids
                    else {
                        request.getSession().removeAttribute("siteId");
                        request.getSession().removeAttribute("areaId");
                        request.getSession().removeAttribute("assetId");
                        request.getSession().removeAttribute("pointLocationId");
                        request.getSession().removeAttribute("pointId");
                    }
                } 

                // Notifications aren't compatible with Uptime Groups. Setting page to Home
                else if (request.getSession().getAttribute("page") != null) {
                    request.getSession().removeAttribute("page");
                    request.getSession().removeAttribute("siteId");
                    request.getSession().removeAttribute("areaId");
                    request.getSession().removeAttribute("assetId");
                    request.getSession().removeAttribute("pointLocationId");
                    request.getSession().removeAttribute("pointId");
                    page = "home";
                }

                // Set other values if the user isn't an uptime user
                if (!globalGroups) {

                    // Check if this 'customerRole' attribute is null, 
                    // if not use the provided role as a global row for all sites for the given customer
                    if (request.getSession().getAttribute("customerRole") != null) {
                        customerRole = request.getSession().getAttribute("customerRole").toString();
                        Logger.getLogger(UserManageBean.class.getName()).log(Level.WARNING, "User Customer global role : {0}", customerRole);

                        providedCustomerRole = true;

                        // Set selectedCustomersVO based off loginCustomerVO
                        selectedCustomersVO = new CustomersVO(loginCustomerVO);

                        // Check if the provided customer role is is supported, if not set the role to 'viewer'
                        if ((index = getValueByGroupInfo(customerRole, false)) > -1) {
                            groupIndexList.add(index);
                        } else {
                            Logger.getLogger(UserManageBean.class.getName()).log(Level.WARNING, "CustomerRole: {0}, is not supported.", customerRole);
                            Logger.getLogger(UserManageBean.class.getName()).log(Level.WARNING, "Permissions set to \"Viewer\".");
                            groupIndexList.add(getValueByGroupInfo("viewer", false));
                        }

                        // Check if the initial Nav and page are compatible with the role
                        if (page != null && !page.equalsIgnoreCase("home")) {
                            switch(page.toLowerCase()) {
                                case "alarms":
                                case "work-order":
                                    if (!(groupIndexList.contains(0) || groupIndexList.contains(2) || groupIndexList.contains(4) || groupIndexList.contains(10))) {
                                        initialNavFilter = null;
                                        page = "home";
                                    }
                                    break;
                                case "configuration":
                                    if (!(groupIndexList.contains(0) || groupIndexList.contains(2) || groupIndexList.contains(3) || groupIndexList.contains(4) || groupIndexList.contains(10))) {
                                        initialNavFilter = null;
                                        page = "home";
                                    }
                                    break;
                                default:
                                    initialNavFilter = null;
                                    page = "home";
                                    break;
                            }
                        }

                        // Set currentSite
                        if (initialNavFilter != null) {
                            currentSite = new UserSitesVO();
                            currentSite.setCustomerAccount(selectedCustomersVO.getCustomerAccount());
                            currentSite.setSiteId((UUID)initialNavFilter.getSite().getValue());
                            currentSite.setSiteName(initialNavFilter.getSite().getLabel());
                            currentSite.setUserId(userId);
                        } else if (accountUserPreferencesVO != null && accountUserPreferencesVO.getDefaultSite() != null) {
                            currentSite = new UserSitesVO();
                            currentSite.setCustomerAccount(selectedCustomersVO.getCustomerAccount());
                            currentSite.setSiteId(accountUserPreferencesVO.getDefaultSite());
                            currentSite.setSiteName(accountUserPreferencesVO.getDefaultSiteName());
                            currentSite.setUserId(userId);
                        } else {
                            currentSite = null;
                        }

                        // Set regionCountrySiteMap
                        setRegionCountrySiteMap();

                        if (currentSite != null) {

                            // Set selectedFilters based on currentSite and regionCountrySiteMap
                            found = false;
                            setCurrent:
                            for (String region : regionCountrySiteMap.keySet()) {
                                for (String country : regionCountrySiteMap.get(region).keySet()) {
                                    for (SelectItem site : regionCountrySiteMap.get(region).get(country)) {
                                        if (currentSite.getSiteId().equals((UUID) site.getValue())) {
                                            selectedFilters.setRegion(region);
                                            selectedFilters.setCountry(country);
                                            selectedFilters.setSite(site);
                                            found = true;
                                            break setCurrent;
                                        }
                                    }
                                }
                            }

                            // Though Error if the Default Site isn't found within the regionCountrySite Map
                            if (!found) {
                                throw new Exception("The Default Site isn't found with in the RegionCountrySite Map.");
                            }
                        }

                    // If this 'customerRole' attribute is null, assume the user is a default user 
                    } else if (loginCustomerVO != null && loginCustomerVO.getCustomerAccount() != null && !loginCustomerVO.getCustomerAccount().isEmpty()) {

                        // Set selectedCustomersVO based off loginCustomerVO
                        selectedCustomersVO = new CustomersVO(loginCustomerVO);

                        // Set userSiteList
                        if (userId == null || (userSiteList = UserClient.getInstance().getUserSiteListByUserIdCustomerAccount(ServiceRegistrarClient.getInstance().getServiceHostURL(USER_PREFERENCE_SERVICE_NAME), userId, loginCustomerVO.getCustomerAccount())) == null) {
                            userSiteList = new ArrayList();
                        }
                        Logger.getLogger(UserManageBean.class.getName()).log(Level.INFO, "***************Set siteList attribute");
                        request.getSession().setAttribute("siteList", userSiteList);

                        // Set currentSite
                        if (page != null && !page.equalsIgnoreCase("home") && initialNavFilter != null && userSiteList != null && !userSiteList.isEmpty()) {
                            try {
                                currentSite = userSiteList.stream().filter(userSite -> userSite != null && userSite.getSiteId().equals((UUID)initialNavFilter.getSite().getValue())).findFirst().get();

                                // check the permissions for the initial info if the page isn't permitted then throw an exception
                                if (currentSite.getSiteRole() != null && (index = getValueByGroupInfo(currentSite.getSiteRole(), false)) > -1) {
                                    switch(page.toLowerCase()) {
                                        case "alarms":
                                        case "work-order":
                                            if (!(index == 0 || index == 2 || index == 4 || index == 10)) {
                                                throw new Exception();
                                            }
                                            break;
                                        case "configuration":
                                            if (!(index == 0 || index == 2 || index == 3 || index == 4 || index == 10)) {
                                                throw new Exception();
                                            }
                                            break;
                                        default:
                                            throw new Exception();
                                    }
                                }
                            } catch (Exception e) {
                                initialNavFilter = null;
                                page = "home";

                                if (accountUserPreferencesVO != null && accountUserPreferencesVO.getDefaultSite() != null) {
                                    try {
                                        currentSite = userSiteList.stream().filter(userSite -> userSite != null && userSite.getSiteId().equals(accountUserPreferencesVO.getDefaultSite())).findFirst().get();
                                    } catch (Exception ex) {
                                        currentSite = new UserSitesVO();
                                    }
                                } else {
                                    currentSite = null;
                                }
                            }
                        } else if (accountUserPreferencesVO != null && accountUserPreferencesVO.getDefaultSite() != null && userSiteList != null && !userSiteList.isEmpty()) {
                            try {
                                currentSite = userSiteList.stream().filter(userSite -> userSite != null && userSite.getSiteId().equals(accountUserPreferencesVO.getDefaultSite())).findFirst().get();

                                // check the permissions for the initial page if the page isn't permitted then throw an exception
                                if (currentSite.getSiteRole() != null && (index = getValueByGroupInfo(currentSite.getSiteRole(), false)) > -1) {

                                    if (page != null && !page.equalsIgnoreCase("home")) {
                                        switch(page.toLowerCase()) {
                                            case "alarms":
                                            case "work-order":
                                                if (!(index == 0 || index == 2 || index == 4 || index == 10)) {
                                                    page = "home";
                                                }
                                                break;
                                            case "configuration":
                                                if (!(index == 0 || index == 2 || index == 3 || index == 4 || index == 10)) {
                                                    page = "home";
                                                }
                                                break;
                                            default:
                                                throw new Exception();
                                        }
                                    }
                                }
                            } catch (Exception e) {
                                currentSite = new UserSitesVO();
                                if (page != null) {
                                    page = "home";
                                }
                            }
                        } else {
                            currentSite = null;
                            if (page != null) {
                                page = "home";
                            }
                        }

                        // Set regionCountrySiteMap
                        setRegionCountrySiteMap();

                        if (currentSite != null) {
                            Logger.getLogger(UserManageBean.class.getName()).log(Level.WARNING, "User Customer initial site role : {0}", currentSite.getSiteRole());

                            // Set selectedFilters based on currentSite and regionCountrySiteMap
                            found = false;
                            setCurrent:
                            for (String region : regionCountrySiteMap.keySet()) {
                                for (String country : regionCountrySiteMap.get(region).keySet()) {
                                    for (SelectItem site : regionCountrySiteMap.get(region).get(country)) {
                                        if (currentSite.getSiteId().equals((UUID) site.getValue())) {
                                            selectedFilters.setRegion(region);
                                            selectedFilters.setCountry(country);
                                            selectedFilters.setSite(site);
                                            found = true;
                                            break setCurrent;
                                        }
                                    }
                                }
                            }

                            // Though Error if the Default Site isn't found within the regionCountrySite Map
                            if (!found) {
                                throw new Exception("The Default Site isn't found with in the RegionCountrySite Map.");
                            }

                            // Set group index based on the site role (Note this is only for customers)
                            if (currentSite.getSiteRole() != null) {
                                if ((index = getValueByGroupInfo(currentSite.getSiteRole(), false)) > -1) {
                                    groupIndexList.add(index);
                                } else {
                                    groupIndexList.add(getValueByGroupInfo("viewer", false));
                                }
                            } else {
                                groupIndexList.add(getValueByGroupInfo("viewer", false));
                            }

                        }
                    } else {
                        userSiteList = new ArrayList();
                        currentSite = null;
                    }
                } else {
                    userSiteList = new ArrayList();
                    currentSite = null;
                    customerList.addAll(CustomerClient.getInstance().getAllCustomers(ServiceRegistrarClient.getInstance().getServiceHostURL(USER_PREFERENCE_SERVICE_NAME)));
                }
            } catch (Exception e) {
                Logger.getLogger(UserManageBean.class.getName()).log(Level.SEVERE, "userManageBean Constructor Exception {0}", e.toString());
                Logger.getLogger(UserManageBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);

                regionCountrySiteMap.clear();
                groupIndexList.clear();
                customerList.clear();
                selectedFilters = new Filters();
                currentSite = null;
                globalGroups = false;
                providedCustomerRole = false;
                userName = null;
                userId = null;
                selectedCustomersVO = null;
                loginCustomerVO = null;
                userSiteList = new ArrayList();
                accountDefaultUnits = new DefaultUnits();
                userPreferencesVO = new UserPreferencesVO();
                defaultUnits = new DefaultUnits();
                initialNavFilter = null;
                page = null;
                request = null;
                externalContext = null;

                accountUserPreferencesVO = new UserPreferencesVO();
                accountUserPreferencesVO.setLocaleDisplayName(DEFAULT_LOCALE_DISPLAY_NAME);
                accountUserPreferencesVO.setLanguage(DEFAULT_LOCALE_DISPLAY_NAME);
                accountUserPreferencesVO.setTimezone(DEFAULT_TIME_ZONE);
            }

            setTimezoneItems();
            setLanguageFields();
            setLocaleDisplayNameList();
            resetUserPreferenceTabs();
            pageRedirect(request, externalContext);
        }
    }

    /**
     * Method used to update the UserManageBean
     */
    public void updateBean() {
        Logger.getLogger(UserManageBean.class.getName()).log(Level.INFO, "*************update UserManageBean.");
    }
    
    /**
     * Check the the page attribute if given
     * @param request, HttpServletRequest Object
     * @return boolean, true if valid page, else false
     */
    private boolean pageCheck(HttpServletRequest request) {
        if (request.getSession().getAttribute("page") != null) {
            page = request.getSession().getAttribute("page").toString();
            request.getSession().removeAttribute("page");
            
            if (page != null) {
                switch (page.toLowerCase()) {
                    case"alarms":
                    case"configuration":
                    case"work-order":
                        return true;
                }
            }
            page = "home";
            return false;
        }
        page = null;
        return false;
    }
    
    /**
     * Page redirect
     * @param request, HttpServletRequest Object
     * @param externalContext, ExternalContext Object
     */
    private void pageRedirect(HttpServletRequest request, ExternalContext externalContext) {
        String url;
        Enumeration<String> names;
        String name;
        
        if (page != null && request != null && externalContext != null) {
            try {
                names = request.getSession().getAttributeNames();
                while (names.hasMoreElements()) {
                    if ((name = names.nextElement()) != null && name.endsWith("Bean") &&
                            !name.equalsIgnoreCase("navigationBean") &&
                            !name.equalsIgnoreCase("logoutBean") &&
                            !name.equalsIgnoreCase("loginBean") &&
                            !name.equalsIgnoreCase("userManageBean") &&
                            !name.equalsIgnoreCase("alarmNotificationBean")) {
                        request.getSession().removeAttribute(name);
                        Logger.getLogger(UserManageBean.class.getName()).log(Level.INFO, "***Removing {0} from the session", name);
                    }
                }
                
                if (navigationBean == null) {
                    if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("navigationBean") == null) {
                        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("navigationBean", new NavigationBean());
                    }
                    navigationBean = (NavigationBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("navigationBean");
                }
                
                if (loginCustomerVO.getFqdn() != null) {
                    switch(page.toLowerCase()) {
                        case "alarms":
                            url = getPropertyValue("UPCAST_URL_PROTOCOL")+ "://" + loginCustomerVO.getFqdn() + "/UpCastCM/user/alarms.xhtml";
                            Logger.getLogger(UserManageBean.class.getName()).log(Level.INFO, "***Load Alarms Page***");
                            navigationBean.updatePage("alarms");
                            Logger.getLogger(UserManageBean.class.getName()).log(Level.INFO, "***Redirect*** {0}", url);
                            externalContext.redirect(url);
                            break;
                        case "configuration":
                            url = getPropertyValue("UPCAST_URL_PROTOCOL")+ "://" + loginCustomerVO.getFqdn() + "/UpCastCM/user/configuration.xhtml";
                            Logger.getLogger(UserManageBean.class.getName()).log(Level.INFO, "***Load Configuration Page***");
                            navigationBean.updatePage("configuration");
                            Logger.getLogger(UserManageBean.class.getName()).log(Level.INFO, "***Redirect*** {0}", url);
                            externalContext.redirect(url);
                            break;
                        case "work-order":
                            url = getPropertyValue("UPCAST_URL_PROTOCOL")+ "://" + loginCustomerVO.getFqdn() + "/UpCastCM/user/work-order.xhtml";
                            Logger.getLogger(UserManageBean.class.getName()).log(Level.INFO, "***Load Work Order Page***");
                            navigationBean.updatePage("work-order");
                            Logger.getLogger(UserManageBean.class.getName()).log(Level.INFO, "***Redirect*** {0}", url);
                            externalContext.redirect(url);
                            break;
                        case"home":
                        default:
                            if (!request.getRequestURI().equals(url = getPropertyValue("UPCAST_URL_PROTOCOL")+ "://" + loginCustomerVO.getFqdn() + "/UpCastCM/user/home.xhtml")) {
                                Logger.getLogger(UserManageBean.class.getName()).log(Level.INFO, "***Load Home Page***");
                                navigationBean.updatePage("home");
                                Logger.getLogger(UserManageBean.class.getName()).log(Level.INFO, "***Redirect*** {0}", url);
                                externalContext.redirect(url);
                            } else {
                                page = null;
                                initialNavFilter = null;
                            }
                            break;
                    }
                    
                    navigationRoutines();
                }
            } catch (Exception e) {
                Logger.getLogger(UserManageBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
                page = null;
                initialNavFilter = null;
            }
        } else {
            page = null;
            initialNavFilter = null;
        }
    }
    
    /**
     * The navigation routine based on the given attributes
     */
    private void navigationRoutines() {
        Logger.getLogger(UserManageBean.class.getName()).log(Level.INFO, "***navigationRoutines*** ");
        
        AlarmsBean alarmsBean;
        ConfigurationBean configurationBean;
        WorkOrderBean workOrderBean;
        
        if (page != null && !page.isEmpty() && !page.equalsIgnoreCase("home")) {
            Logger.getLogger(UserManageBean.class.getName()).log(Level.INFO, "***Page : {0}", page);
            
            try {
                switch(page.toLowerCase()) {
                    case "alarms":
                        if (initialNavFilter != null) {
                            alarmsBean = (AlarmsBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("alarmsBean");
                            if (initialNavFilter.getArea() != null) {
                                Logger.getLogger(UserManageBean.class.getName()).log(Level.INFO, "Initial Navigating Area: {0}", initialNavFilter.getArea().getLabel());
                                alarmsBean.getSelectedFilters().setArea(new SelectItem((UUID)initialNavFilter.getArea().getValue(), initialNavFilter.getArea().getLabel()));
                                alarmsBean.onAreaChange(initialNavFilter.getAsset() == null);
                            }
                            if (initialNavFilter.getAsset() != null) {
                                Logger.getLogger(UserManageBean.class.getName()).log(Level.INFO, "Initial Navigating Asset: {0}", initialNavFilter.getAsset().getLabel());
                                alarmsBean.getSelectedFilters().setAsset(new SelectItem((UUID)initialNavFilter.getAsset().getValue(), initialNavFilter.getAsset().getLabel()));
                                alarmsBean.onAssetChange(initialNavFilter.getPointLocation() == null);
                            }
                            if (initialNavFilter.getPointLocation() != null) {
                                Logger.getLogger(UserManageBean.class.getName()).log(Level.INFO, "Initial Navigating PointLocation: {0}", initialNavFilter.getPointLocation().getLabel());
                                alarmsBean.getSelectedFilters().setPointLocation(new SelectItem((UUID)initialNavFilter.getPointLocation().getValue(), initialNavFilter.getPointLocation().getLabel()));
                                alarmsBean.onPointLocationChange(initialNavFilter.getPoint() == null);
                            }
                            if (initialNavFilter.getPoint() != null) {
                                Logger.getLogger(UserManageBean.class.getName()).log(Level.INFO, "Initial Navigating Point: {0}", initialNavFilter.getPoint().getLabel());
                                alarmsBean.getSelectedFilters().setPoint(new SelectItem((UUID)initialNavFilter.getPoint().getValue(), initialNavFilter.getPoint().getLabel()));
                                alarmsBean.onPointChange(true);
                            }
                        }
                        break;
                    case "configuration":
                        if (initialNavFilter != null) {
                            configurationBean = (ConfigurationBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("configurationBean");
                            if (initialNavFilter.getArea() != null) {
                                Logger.getLogger(UserManageBean.class.getName()).log(Level.INFO, "Initial Navigating Area: {0}", initialNavFilter.getArea().getLabel());
                                configurationBean.getSelectedFilters().setArea(new SelectItem((UUID)initialNavFilter.getArea().getValue(), initialNavFilter.getArea().getLabel()));
                                configurationBean.onAreaChange(initialNavFilter.getAsset() == null);
                            }
                            if (initialNavFilter.getAsset() != null) {
                                Logger.getLogger(UserManageBean.class.getName()).log(Level.INFO, "Initial Navigating Asset: {0}", initialNavFilter.getAsset().getLabel());
                                configurationBean.getSelectedFilters().setAsset(new SelectItem((UUID)initialNavFilter.getAsset().getValue(), initialNavFilter.getAsset().getLabel()));
                                configurationBean.onAssetChange(initialNavFilter.getPointLocation() == null);
                            }
                            if (initialNavFilter.getPointLocation() != null) {
                                Logger.getLogger(UserManageBean.class.getName()).log(Level.INFO, "Initial Navigating PointLocation: {0}", initialNavFilter.getPointLocation().getLabel());
                                configurationBean.getSelectedFilters().setPointLocation(new SelectItem((UUID)initialNavFilter.getPointLocation().getValue(), initialNavFilter.getPointLocation().getLabel()));
                                configurationBean.onPointLocationChange(initialNavFilter.getPoint() == null);
                            }
                            if (initialNavFilter.getPoint() != null) {
                                Logger.getLogger(UserManageBean.class.getName()).log(Level.INFO, "Initial Navigating Point: {0}", initialNavFilter.getPoint().getLabel());
                                configurationBean.getSelectedFilters().setPoint(new SelectItem((UUID)initialNavFilter.getPoint().getValue(), initialNavFilter.getPoint().getLabel()));
                                configurationBean.onPointChange(true);
                            }
                        }
                        break;
                    case "work-order":
                        if (initialNavFilter != null) {
                            workOrderBean = (WorkOrderBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("workOrderBean");
                            if (initialNavFilter.getArea() != null) {
                                Logger.getLogger(UserManageBean.class.getName()).log(Level.INFO, "Initial Navigating Area: {0}", initialNavFilter.getArea().getLabel());
                                workOrderBean.getSelectedFilters().setArea(new SelectItem((UUID)initialNavFilter.getArea().getValue(), initialNavFilter.getArea().getLabel()));
                                workOrderBean.onAreaChange(initialNavFilter.getAsset() == null);
                            }
                            if (initialNavFilter.getAsset() != null) {
                                Logger.getLogger(UserManageBean.class.getName()).log(Level.INFO, "Initial Navigating Asset: {0}", initialNavFilter.getAsset().getLabel());
                                workOrderBean.getSelectedFilters().setAsset(new SelectItem((UUID)initialNavFilter.getAsset().getValue(), initialNavFilter.getAsset().getLabel()));
                                workOrderBean.onAssetChange(true);
                            }
                        }
                        break;
                }
            } catch (Exception e) {
                Logger.getLogger(UserManageBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            }
        }
        
        page = null;
        initialNavFilter = null;
    }
    
    /**
     * Redirect to the home page or reload the current page
     * @param home boolean
     */
    private void homeRedirectOrReloadPage(boolean home) {
        HttpServletRequest httpRequest;
        ExternalContext externalContext;
        String url;
        
        if (navigationBean == null) {
            if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("navigationBean") == null) {
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("navigationBean", new NavigationBean());
            }
            navigationBean = (NavigationBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("navigationBean");
        }
        
        try {
            externalContext = FacesContext.getCurrentInstance().getExternalContext();
            httpRequest = (HttpServletRequest) externalContext.getRequest();
            
            if (home) {
                if (!httpRequest.getRequestURI().equals(url = getPropertyValue("UPCAST_URL_PROTOCOL")+ "://" + loginCustomerVO.getFqdn() + "/UpCastCM/user/home.xhtml")) {
                    navigationBean.updatePage("home");
                    externalContext.redirect(url);
                }
            } else {
                navigationBean.updatePage(navigationBean.getPage());
                externalContext.redirect(httpRequest.getRequestURI());
            }
        } catch (Exception e) {
            Logger.getLogger(UserManageBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }
    
    /**
     * Check if the given value is found in the list checkGroupIndexList
     * @param value, int
     * @return boolean, true if found, else false
     */
    public boolean checkGroupIndexList(int value) {
        try {
            return groupIndexList.stream().anyMatch(index -> index != null && index == value);
        } catch (Exception e) {
            Logger.getLogger(UserManageBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
        return false;
    }
    
    /**
     * Clear unrequired beans from Session
     */
    public void clearSession() {
        HttpServletRequest request;
        Enumeration<String> names;
        String name;
        
        // Close Asset Analysis Windows
        PrimeFaces.current().executeScript("closeAllWindows()");
            
        // Clear Beans from session
        request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        names = request.getSession().getAttributeNames();
        while (names.hasMoreElements()) {
            if ((name = names.nextElement()) != null && name.endsWith("Bean") &&
                    !name.equalsIgnoreCase("navigationBean") &&
                    !name.equalsIgnoreCase("logoutBean") &&
                    !name.equalsIgnoreCase("loginBean") &&
                    !name.equalsIgnoreCase("userManageBean") &&
                    !name.equalsIgnoreCase("alarmNotificationBean")) {
                request.getSession().removeAttribute(name);
            }
        }
    }

    /**
     * Submit the new values from the user
     */
    public void submit() {
        int startIndex, endIndex;
        String subString, modified;
        
        try {
            userPreferencesVO.setDefaultUnits(defaultUnits.getAsJsonString());
            userPreferencesVO.setUserId(userId);
            userPreferencesVO.setCustomerAccount(loginCustomerVO.getCustomerAccount());
            if (selectedTimezone != null && !selectedTimezone.isEmpty()) {
                if (selectedTimezone.contains("(") && selectedTimezone.contains(")")) {
                    startIndex = selectedTimezone.indexOf("(");
                    endIndex = selectedTimezone.indexOf(")");
                    subString = selectedTimezone.substring(startIndex, endIndex + 1);
                    modified = selectedTimezone.replace(subString, "").trim();
                    
                    userPreferencesVO.setTimezone(modified);
                } else {
                    userPreferencesVO.setTimezone(selectedTimezone);
                }
            } else {
                userPreferencesVO.setTimezone(DEFAULT_TIME_ZONE);
            }
            
            updateUserPreferences(new UserPreferencesVO(userPreferencesVO));
            clearSession();
            
            // Reset UserManageBean
            init();
            
            // Re-Navigate to original page
            homeRedirectOrReloadPage(globalGroups);
            
            FacesContext.getCurrentInstance().addMessage("nonAutoGrowlMessages", new FacesMessage(getResourceBundleString("dialog_preferences_msg_1")));
            PrimeFaces.current().ajax().update("nonAutoGrowlMessages");
        } catch (Exception e) {
            Logger.getLogger(UserManageBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Update the current user's preferences with the given info
     * @param userPreferencesVO, UserPreferencesVO Object 
     */
    private void updateUserPreferences(UserPreferencesVO userPreferencesVO) {
        HttpServletRequest request;
        
        try {        
            userPreferencesVO.setLanguage(userPreferencesVO.getLocaleDisplayName());
            
            if (UserClient.getInstance().createUserPreferences(ServiceRegistrarClient.getInstance().getServiceHostURL(USER_PREFERENCE_SERVICE_NAME), userPreferencesVO)) {
                request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
                request.getSession().setAttribute("userVO", userPreferencesVO);
                Logger.getLogger(UserManageBean.class.getName()).log(Level.INFO, "Saved user preferences {0}", new Object[]{userPreferencesVO.toString()});
            } else {
                Logger.getLogger(UserManageBean.class.getName()).log(Level.INFO, "userPreferencesVO.toString()*********** {0}", new Object[]{userPreferencesVO.toString()});
            }
        } catch (Exception e) {
            Logger.getLogger(UserManageBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * called to reset user preference tabs in the user preference overlay
     */
    public void resetUserPreferenceTabs() {
        int startIndex, endIndex;
        String subString, modified, key;
        
        userPreferencesVO = new UserPreferencesVO(accountUserPreferencesVO);
        defaultUnits = new DefaultUnits(accountDefaultUnits);
        
        try {
            if (accountUserPreferencesVO.getTimezone() != null && !accountUserPreferencesVO.getTimezone().equals("")) {
                if (accountUserPreferencesVO.getTimezone().contains("(") && accountUserPreferencesVO.getTimezone().contains(")")) {
                    startIndex = accountUserPreferencesVO.getTimezone().indexOf("(");
                    endIndex = accountUserPreferencesVO.getTimezone().indexOf(")");
                    subString = accountUserPreferencesVO.getTimezone().substring(startIndex, endIndex + 1);
                    key = accountUserPreferencesVO.getTimezone().replace(subString, "").trim();
                } else {
                    key = accountUserPreferencesVO.getTimezone().trim();
                }

                if (!TIME_ZONE_SITE_DEFAULT.equalsIgnoreCase(key)) {
                    if (timezoneMap.containsKey(key)) {
                        selectedTimezone = key;
                    } else {
                        for (String item : timezoneMap.keySet()) {
                            if (item != null && item.contains("(") && item.contains(")")) {
                                startIndex = item.indexOf("(");
                                endIndex = item.indexOf(")");
                                subString = item.substring(startIndex, endIndex + 1);
                                modified = item.replace(subString, "").trim();

                                if (modified.equalsIgnoreCase(key)) {
                                    selectedTimezone = item;
                                    break;
                                }
                            }
                        }
                    }

                    if (zoneId == null) {
                        selectedTimezone = "UTC (UTC +00:00)";
                    }
                } else {
                    selectedTimezone = TIME_ZONE_SITE_DEFAULT;
                }
            } else {
                selectedTimezone = "UTC (UTC +00:00)";
            }
        } catch (Exception e) {
            Logger.getLogger(UserManageBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            selectedTimezone = "UTC (UTC +00:00)";
        }
        
        if (!globalGroups) {
            try { 
                if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("alarmNotificationBean") == null) {
                    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("alarmNotificationBean", new AlarmNotificationBean());
                    ((AlarmNotificationBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("alarmNotificationBean")).setUserPreferencesVO(accountUserPreferencesVO);
                }
                ((AlarmNotificationBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("alarmNotificationBean")).reset();
            } catch (Exception e) {
                Logger.getLogger(UserManageBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            }
        }
    }
    
    /**
     * called when the Customer changed
     */
    public void onCustomerChange() {
        setRegionCountrySiteMap();
        
        // Set displayedTimeZone and zoneId to UTC if account timezone equals TIME_ZONE_SITE_DEFAULT
        if (accountUserPreferencesVO != null && accountUserPreferencesVO.getTimezone() != null && accountUserPreferencesVO.getTimezone().trim().equalsIgnoreCase(TIME_ZONE_SITE_DEFAULT)) {
            displayedTimeZone = DEFAULT_TIME_ZONE;
            zoneId = ZoneOffset.UTC;
        }
        
        selectedFilters = new Filters();
        currentSite = null;
        clearSession();
        homeRedirectOrReloadPage(true);
    }

    /**
     * called when the region changed
     */
    public void onRegionChange() {
        if (selectedFilters != null) {
            selectedFilters.clear(REGION);
        }
        
        // Set displayedTimeZone and zoneId to UTC if account timezone equals TIME_ZONE_SITE_DEFAULT
        if (accountUserPreferencesVO != null && accountUserPreferencesVO.getTimezone() != null && accountUserPreferencesVO.getTimezone().trim().equalsIgnoreCase(TIME_ZONE_SITE_DEFAULT)) {
            displayedTimeZone = DEFAULT_TIME_ZONE;
            zoneId = ZoneOffset.UTC;
        }
        
        currentSite = null;
        clearSession();
        homeRedirectOrReloadPage(true);
    }

    /**
     * called when the country changed
     */
    public void onCountryChange() {
        if (selectedFilters != null) {
            selectedFilters.clear(COUNTRY);
        }
        
        // Set displayedTimeZone and zoneId to UTC if account timezone equals TIME_ZONE_SITE_DEFAULT
        if (accountUserPreferencesVO != null && accountUserPreferencesVO.getTimezone() != null && accountUserPreferencesVO.getTimezone().trim().equalsIgnoreCase(TIME_ZONE_SITE_DEFAULT)) {
            displayedTimeZone = DEFAULT_TIME_ZONE;
            zoneId = ZoneOffset.UTC;
        }
        
        currentSite = null;
        clearSession();
        homeRedirectOrReloadPage(true);
    }

    /**
     * called when the site changed
     */
    public void onSiteChange() {
        int startIndex, endIndex, index;
        String subString, modified, key;
        
        if (selectedFilters != null && selectedFilters.getSite() != null) {
            try {
                
                // Set currentSite and groupIndexList if no global Groups or no provided customer role
                if (!globalGroups && !providedCustomerRole) {
                    currentSite = userSiteList.stream().filter(userSite -> userSite != null && userSite.getSiteId().equals((UUID)selectedFilters.getSite().getValue())).findFirst().get();

                    groupIndexList.clear();
                    if (currentSite.getSiteRole() != null) {
                        if ((index = getValueByGroupInfo(currentSite.getSiteRole(), false)) > -1) {
                            groupIndexList.add(index);
                        } else {
                            groupIndexList.add(getValueByGroupInfo("viewer", false));
                        }
                    } else {
                        groupIndexList.add(getValueByGroupInfo("viewer", false));
                    }
                } 
                
                // Set currentSite for Uptime user
                else {
                    currentSite = new UserSitesVO();
                    currentSite.setCustomerAccount(selectedCustomersVO.getCustomerAccount());
                    currentSite.setSiteId((UUID)selectedFilters.getSite().getValue());
                    currentSite.setSiteName(selectedFilters.getSite().getLabel());
                    currentSite.setUserId(userId);
                }
                
                // Set displayedTimeZone and zoneId to site default timezone if account timezone equals TIME_ZONE_SITE_DEFAULT
                if (accountUserPreferencesVO != null && accountUserPreferencesVO.getTimezone() != null && accountUserPreferencesVO.getTimezone().trim().equalsIgnoreCase(TIME_ZONE_SITE_DEFAULT)) {
                    if (selectedFilters != null && selectedFilters.getSite() != null && selectedFilters.getSite().getDescription() != null && !selectedFilters.getSite().getDescription().isEmpty()) {
                        if (selectedFilters.getSite().getDescription().contains("(") && selectedFilters.getSite().getDescription().contains(")")) {
                            startIndex = selectedFilters.getSite().getDescription().indexOf("(");
                            endIndex = selectedFilters.getSite().getDescription().indexOf(")");
                            subString = selectedFilters.getSite().getDescription().substring(startIndex, endIndex + 1);
                            key = selectedFilters.getSite().getDescription().replace(subString, "").trim();
                        } else {
                            key = selectedFilters.getSite().getDescription().trim();
                        }
                
                        if (!TIME_ZONE_SITE_DEFAULT.equalsIgnoreCase(key)) {
                            if (timezoneMap.containsKey(key)) {
                                zoneId = ZoneId.of(timezoneMap.get(key));
                                displayedTimeZone = key; 
                            } else {
                                for (String item : timezoneMap.keySet()) {
                                    if (item != null && item.contains("(") && item.contains(")")) {
                                        startIndex = item.indexOf("(");
                                        endIndex = item.indexOf(")");
                                        subString = item.substring(startIndex, endIndex + 1);
                                        modified = item.replace(subString, "").trim();

                                        if (modified.equalsIgnoreCase(key)) {
                                            zoneId = ZoneId.of(timezoneMap.get(item));
                                            displayedTimeZone = modified; 
                                            break;
                                        }
                                    }
                                }
                            }

                            if (zoneId == null) {
                                zoneId = ZoneOffset.UTC;
                                displayedTimeZone = DEFAULT_TIME_ZONE;
                            }
                        } else {
                            zoneId = ZoneOffset.UTC;
                            displayedTimeZone = DEFAULT_TIME_ZONE;
                        }
                    } else {
                        zoneId = ZoneOffset.UTC;
                        displayedTimeZone = DEFAULT_TIME_ZONE;
                    }
                }
                
                presetUtil.clearAllPresets();
                clearSession();
                homeRedirectOrReloadPage(true);
            } catch (Exception e) {
                Logger.getLogger(UserManageBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
                if (!globalGroups && !providedCustomerRole) {
                    groupIndexList.clear();
                }
        
                // Set displayedTimeZone and zoneId to UTC if account timezone equals TIME_ZONE_SITE_DEFAULT
                if (accountUserPreferencesVO != null && accountUserPreferencesVO.getTimezone() != null && accountUserPreferencesVO.getTimezone().trim().equalsIgnoreCase(TIME_ZONE_SITE_DEFAULT)) {
                    displayedTimeZone = DEFAULT_TIME_ZONE;
                    zoneId = ZoneOffset.UTC;
                }
                
                currentSite = null;
                presetUtil.clearAllPresets();
                clearSession();
                homeRedirectOrReloadPage(true);
            }
        } else {
            if (!globalGroups && !providedCustomerRole) {
                groupIndexList.clear();
            }
        
            // Set displayedTimeZone and zoneId to UTC if account timezone equals TIME_ZONE_SITE_DEFAULT
            if (accountUserPreferencesVO != null && accountUserPreferencesVO.getTimezone() != null && accountUserPreferencesVO.getTimezone().trim().equalsIgnoreCase(TIME_ZONE_SITE_DEFAULT)) {
                displayedTimeZone = DEFAULT_TIME_ZONE;
                zoneId = ZoneOffset.UTC;
            }
            
            currentSite = null;
            presetUtil.clearAllPresets();
            clearSession();
            homeRedirectOrReloadPage(true);
        }
    }

    /**
     * Query the customersVO list for all items that contain the given value
     * @param query, String Object
     * @return List Object of CustomersVO Objects
     */
    public List<CustomersVO> queryCustomerList(String query) {
        try {
            if (query == null || query.isEmpty()) {
                return customerList.stream().sorted(Comparator.comparing(CustomersVO::getCustomerName)).collect(Collectors.toList());
            } else {
                return customerList.stream().filter(item -> item.getCustomerName().toLowerCase().contains(query.toLowerCase())).sorted(Comparator.comparing(CustomersVO::getCustomerName)).collect(Collectors.toList());
            }
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Query the region list for all items that contain the given value
     * @param query, String Object
     * @return List Object of String Objects
     */
    public List<String> queryRegionList(String query) {
        List<String> list;
        
        try {
            list = new ArrayList(regionCountrySiteMap.keySet());
            if (query == null || query.isEmpty()) {
                return list.stream().sorted().collect(Collectors.toList());
            } else {
                return list.stream().filter(region -> region.toLowerCase().contains(query.toLowerCase())).sorted().collect(Collectors.toList());
            }
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Query the country list for all items that contain the given value
     * @param query, String Object
     * @return List Object of String Objects
     */
    public List<String> queryCountryList(String query) {
        List<String> list;
        
        try {
            list = new ArrayList(regionCountrySiteMap.get(selectedFilters.getRegion()).keySet());
            if (query == null || query.isEmpty()) {
                return list.stream().sorted().collect(Collectors.toList());
            } else {
                return list.stream().filter(country -> country.toLowerCase().contains(query.toLowerCase())).sorted().collect(Collectors.toList());
            }
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Query the site list for all items that contain the given value
     * @param query, String Object
     * @return List Object of SelectItem Objects
     */
    public List<SelectItem> querySiteNameList(String query) {
        try {
            if (query == null || query.isEmpty()) {
                return regionCountrySiteMap.get(selectedFilters.getRegion()).get(selectedFilters.getCountry()).stream().sorted(Comparator.comparing(SelectItem::getLabel)).collect(Collectors.toList());
            } else {
                return regionCountrySiteMap.get(selectedFilters.getRegion()).get(selectedFilters.getCountry()).stream().filter(site -> site.getLabel().toLowerCase().contains(query.toLowerCase())).sorted(Comparator.comparing(SelectItem::getLabel)).collect(Collectors.toList());
            }
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Sets the given field regionCountrySiteMap
     */
    public void setRegionCountrySiteMap() {
        try {
            regionCountrySiteMap.clear();
                
            if (selectedCustomersVO != null && selectedCustomersVO.getCustomerAccount() != null && !selectedCustomersVO.getCustomerAccount().isEmpty()) {
                if (globalGroups || providedCustomerRole) {
                    SiteClient.getInstance().getSiteRegionCountryByCustomer(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), selectedCustomersVO.getCustomerAccount()).forEach(siteVO -> {
                        if (regionCountrySiteMap.containsKey(siteVO.getRegion())) {
                            if (regionCountrySiteMap.get(siteVO.getRegion()).containsKey(siteVO.getCountry())) {
                                if (!regionCountrySiteMap.get(siteVO.getRegion()).get(siteVO.getCountry()).stream().anyMatch(site -> site.getValue().equals(siteVO.getSiteId()))) {
                                    regionCountrySiteMap.get(siteVO.getRegion()).get(siteVO.getCountry()).add(new SelectItem(siteVO.getSiteId(), siteVO.getSiteName(), siteVO.getTimezone()));
                                }
                            } else {
                                regionCountrySiteMap.get(siteVO.getRegion()).put(siteVO.getCountry(), new ArrayList());
                                regionCountrySiteMap.get(siteVO.getRegion()).get(siteVO.getCountry()).add(new SelectItem(siteVO.getSiteId(), siteVO.getSiteName(), siteVO.getTimezone()));
                            }
                        } else {
                            regionCountrySiteMap.put(siteVO.getRegion(), new HashMap());
                            regionCountrySiteMap.get(siteVO.getRegion()).put(siteVO.getCountry(), new ArrayList());
                            regionCountrySiteMap.get(siteVO.getRegion()).get(siteVO.getCountry()).add(new SelectItem(siteVO.getSiteId(), siteVO.getSiteName(), siteVO.getTimezone()));
                        }
                    });
                } else if (userSiteList != null && !userSiteList.isEmpty()) {
                    SiteClient.getInstance().getSiteRegionCountryByCustomer(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), selectedCustomersVO.getCustomerAccount()).forEach(siteVO -> {
                        if (userSiteList.stream().anyMatch(userSite -> userSite.getSiteId().equals(siteVO.getSiteId()))) {
                            if (regionCountrySiteMap.containsKey(siteVO.getRegion())) {
                                if (regionCountrySiteMap.get(siteVO.getRegion()).containsKey(siteVO.getCountry())) {
                                    if (!regionCountrySiteMap.get(siteVO.getRegion()).get(siteVO.getCountry()).stream().anyMatch(site -> site.getValue().equals(siteVO.getSiteId()))) {
                                        regionCountrySiteMap.get(siteVO.getRegion()).get(siteVO.getCountry()).add(new SelectItem(siteVO.getSiteId(), siteVO.getSiteName(), siteVO.getTimezone()));
                                    }
                                } else {
                                    regionCountrySiteMap.get(siteVO.getRegion()).put(siteVO.getCountry(), new ArrayList());
                                    regionCountrySiteMap.get(siteVO.getRegion()).get(siteVO.getCountry()).add(new SelectItem(siteVO.getSiteId(), siteVO.getSiteName(), siteVO.getTimezone()));
                                }
                            } else {
                                regionCountrySiteMap.put(siteVO.getRegion(), new HashMap());
                                regionCountrySiteMap.get(siteVO.getRegion()).put(siteVO.getCountry(), new ArrayList());
                                regionCountrySiteMap.get(siteVO.getRegion()).get(siteVO.getCountry()).add(new SelectItem(siteVO.getSiteId(), siteVO.getSiteName(), siteVO.getTimezone()));
                            }
                        }
                    });
                }
            }
            
        } catch (Exception ex) {
            regionCountrySiteMap.clear();
        }
    }

    /**
     * Set the active language fields based on the given displayName
     */
    private void setLanguageFields() {
        activeLocale = null;

        // Set activeLocale based on the displayName
        if (accountUserPreferencesVO != null && accountUserPreferencesVO.getLocaleDisplayName() != null && !accountUserPreferencesVO.getLocaleDisplayName().isEmpty()) {
            LOCALE_LIST.stream().filter(locale -> locale.getDisplayName().equals(accountUserPreferencesVO.getLocaleDisplayName())).forEach(locale -> activeLocale = new Locale(locale.getLanguage(), locale.getCountry()));
        }

        // Set English and USA as the default for the locale if activeLocale is null
        if (activeLocale == null) {
            activeLocale = new Locale("en", "US");
        }

        // Set the activeResourceBundle
        try {
            activeResourceBundle = ResourceBundle.getBundle(PATH, activeLocale);
        } catch (Exception e) {
            Logger.getLogger(UserManageBean.class.getName()).log(Level.WARNING, e.getMessage(), e);
            activeResourceBundle = null;
        }
    }
    
    /**
     * Convert an Instant Object to String Object with the users zoneId offset.
     * Note: The Instant is assumed to be in UTC
     * 
     * The format is int representing a certain format the String should be in.
     * 
     * @param instant, Instant Object
     * @param format, int
     * @return String Object
     */
    public String getTzOffsetString(Instant instant, int format) {
        ZonedDateTime zonedDateTime;
        
        if (instant != null && zoneId != null) {
            try {
                zonedDateTime = ZonedDateTime.ofInstant(instant, ZoneOffset.UTC).withZoneSameInstant(zoneId);
                switch (format) {
                    case 0: // yyyy-MM-dd
                        return zonedDateTime.format(DATE_FORMATTER);
                    case 1: // yyyy-MM-dd HH:mm:ss
                        return zonedDateTime.format(DATE_TIME_FORMATTER);
                    case 2: // yyyy-MM-dd'T'HH:mm:ssZ
                        return zonedDateTime.format(DATE_TIME_OFFSET_FORMATTER);
                    case 3: // E, MMM dd, YYYY HH:mm:ss
                        return zonedDateTime.format(DATE_MONTH_FORMATTER);
                    default: // yyyy-MM-dd HH:mm:ss
                        return zonedDateTime.format(DATE_TIME_FORMATTER);
                }
            } catch (Exception e) {
                Logger.getLogger(UserManageBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            }
        }
        return "";
    }

    /**
     * Set the localeDisplayNameList field by the LOCALE_LIST field
     */
    private void setLocaleDisplayNameList() {
        localeDisplayNameList = new ArrayList();
        
        LOCALE_LIST.stream().forEachOrdered(locale -> localeDisplayNameList.add(locale.getDisplayName()));
        Collections.sort(localeDisplayNameList);
    }

    public List<String> getLocaleDisplayNameList() {
        return localeDisplayNameList;
    }
    
    /**
     * Set timezoneList and timezoneMap fields
     */
    private void setTimezoneItems() {
        LocalDateTime localDateTime = LocalDateTime.now();
        Map<String, String> tmpMap = new HashMap();
        int startIndex, endIndex;
        String subString, modified, key;
        
        // Set timezoneList and timezoneMap fields
        try {
            timezoneList.clear();
            timezoneMap.clear();
            timezoneList.add(TIME_ZONE_SITE_DEFAULT);
            timezoneMap.put(TIME_ZONE_SITE_DEFAULT, TIME_ZONE_SITE_DEFAULT);
            ZoneId.getAvailableZoneIds().forEach(item -> tmpMap.put(item, localDateTime.atZone(ZoneId.of(item)).getOffset().getId().replace("Z", "+00:00")));
            tmpMap.entrySet().stream().sorted(Map.Entry.<String, String>comparingByValue().reversed()).forEachOrdered(entry -> {
                if(entry.getValue().contains("-")) {
                    String timezone = String.format("%s (UTC %s)", entry.getKey(), entry.getValue());
                    timezoneList.add(timezone);
                    timezoneMap.put(timezone, entry.getKey());
                }
            });

            tmpMap.entrySet().stream().sorted(Map.Entry.<String, String>comparingByValue()).forEachOrdered(entry -> {
                if(entry.getValue().contains("+")) {
                    String timezone = String.format("%s (UTC %s)", entry.getKey(), entry.getValue());
                    timezoneList.add(timezone);
                    timezoneMap.put(timezone, entry.getKey());
                }
            });
        } catch (Exception e) {
            Logger.getLogger(UserManageBean.class.getName()).log(Level.WARNING, e.getMessage(), e);
            timezoneList.clear();
            timezoneMap.clear();
        }
        
        // Set displayedTimeZone, zoneId, and selectedTimezone
        if (accountUserPreferencesVO.getTimezone() != null && !accountUserPreferencesVO.getTimezone().equals("")) {
            try {
                zoneId = null;
                
                if (accountUserPreferencesVO.getTimezone().contains("(") && accountUserPreferencesVO.getTimezone().contains(")")) {
                    startIndex = accountUserPreferencesVO.getTimezone().indexOf("(");
                    endIndex = accountUserPreferencesVO.getTimezone().indexOf(")");
                    subString = accountUserPreferencesVO.getTimezone().substring(startIndex, endIndex + 1);
                    key = accountUserPreferencesVO.getTimezone().replace(subString, "").trim();
                } else {
                    key = accountUserPreferencesVO.getTimezone().trim();
                }
                
                if (!TIME_ZONE_SITE_DEFAULT.equalsIgnoreCase(key)) {
                    if (timezoneMap.containsKey(key)) {
                        zoneId = ZoneId.of(timezoneMap.get(key));
                        selectedTimezone = key;
                        displayedTimeZone = key; 
                    } else {
                        for (String item : timezoneMap.keySet()) {
                            if (item != null && item.contains("(") && item.contains(")")) {
                                startIndex = item.indexOf("(");
                                endIndex = item.indexOf(")");
                                subString = item.substring(startIndex, endIndex + 1);
                                modified = item.replace(subString, "").trim();

                                if (modified.equalsIgnoreCase(key)) {
                                    zoneId = ZoneId.of(timezoneMap.get(item));
                                    selectedTimezone = item;
                                    displayedTimeZone = modified; 
                                    break;
                                }
                            }
                        }
                    }
                
                    if (zoneId == null) {
                        selectedTimezone = "UTC (UTC +00:00)";
                        zoneId = ZoneOffset.UTC;
                        displayedTimeZone = DEFAULT_TIME_ZONE;
                    }
                } else {
                    selectedTimezone = TIME_ZONE_SITE_DEFAULT;
                    
                    if (selectedFilters != null && selectedFilters.getSite() != null && selectedFilters.getSite().getDescription() != null && !selectedFilters.getSite().getDescription().isEmpty()) {
                        if (selectedFilters.getSite().getDescription().contains("(") && selectedFilters.getSite().getDescription().contains(")")) {
                            startIndex = selectedFilters.getSite().getDescription().indexOf("(");
                            endIndex = selectedFilters.getSite().getDescription().indexOf(")");
                            subString = selectedFilters.getSite().getDescription().substring(startIndex, endIndex + 1);
                            key = selectedFilters.getSite().getDescription().replace(subString, "").trim();
                        } else {
                            key = selectedFilters.getSite().getDescription().trim();
                        }
                
                        if (!TIME_ZONE_SITE_DEFAULT.equalsIgnoreCase(key)) {
                            if (timezoneMap.containsKey(key)) {
                                zoneId = ZoneId.of(timezoneMap.get(key));
                                displayedTimeZone = key; 
                            } else {
                                for (String item : timezoneMap.keySet()) {
                                    if (item != null && item.contains("(") && item.contains(")")) {
                                        startIndex = item.indexOf("(");
                                        endIndex = item.indexOf(")");
                                        subString = item.substring(startIndex, endIndex + 1);
                                        modified = item.replace(subString, "").trim();

                                        if (modified.equalsIgnoreCase(key)) {
                                            zoneId = ZoneId.of(timezoneMap.get(item));
                                            displayedTimeZone = modified; 
                                            break;
                                        }
                                    }
                                }
                            }

                            if (zoneId == null) {
                                zoneId = ZoneOffset.UTC;
                                displayedTimeZone = DEFAULT_TIME_ZONE;
                            }
                        } else {
                            zoneId = ZoneOffset.UTC;
                            displayedTimeZone = DEFAULT_TIME_ZONE;
                        }
                    } else {
                        zoneId = ZoneOffset.UTC;
                        displayedTimeZone = DEFAULT_TIME_ZONE;
                    }
                }
            } catch (Exception e) {
                Logger.getLogger(UserManageBean.class.getName()).log(Level.WARNING, e.getMessage(), e);
                accountUserPreferencesVO.setTimezone(null);
                selectedTimezone = "UTC (UTC +00:00)";
                zoneId = ZoneOffset.UTC;
                displayedTimeZone = DEFAULT_TIME_ZONE;
            }
        } else {
            accountUserPreferencesVO.setTimezone(null);
            selectedTimezone = "UTC (UTC +00:00)";
            zoneId = ZoneOffset.UTC;
            displayedTimeZone = DEFAULT_TIME_ZONE;
        }
    }

    public List<UserSitesVO> getUserSiteListForNotifications() {
        List <UserSitesVO> list;
        
        try {
            if (!providedCustomerRole) {
                return userSiteList;
            } else {
                list = new ArrayList();
                regionCountrySiteMap.keySet().stream().forEachOrdered(region -> regionCountrySiteMap.get(region).keySet().stream().forEachOrdered(country -> regionCountrySiteMap.get(region).get(country).stream().forEachOrdered(site -> {
                    UserSitesVO userSitesVO;

                    userSitesVO = new UserSitesVO();
                    userSitesVO.setSiteName(site.getLabel());
                    userSitesVO.setSiteId((UUID)site.getValue());

                    list.add(userSitesVO);
                })));
                return list.stream().sorted(Comparator.comparing(UserSitesVO::getSiteName)).collect(Collectors.toList());
            }
        } catch (Exception e) {
            Logger.getLogger(UserManageBean.class.getName()).log(Level.WARNING, e.getMessage(), e);
        }
        return null;
    }
    
    public ZoneId getZoneId() {
        return zoneId;
    }
    
    public String getSkin() {
        return (loginCustomerVO != null && loginCustomerVO.getSkin() != null && !loginCustomerVO.getSkin().isEmpty()) ? loginCustomerVO.getSkin() : DEFAULT_SKIN;
    }
    
    public String getLogo() {
        return (loginCustomerVO != null && loginCustomerVO.getLogo() != null && !loginCustomerVO.getLogo().isEmpty()) ? loginCustomerVO.getLogo() : DEFAULT_LOGO;
    }

    public String getResourceBundleString(String property) {
        return activeResourceBundle != null ? activeResourceBundle.getString(property) : "NULL";
    }

    public List<String> getTimezoneList() {
        return timezoneList;
    }

    public List<SelectItem> getACUnitList(String sensorType, String paramType) {
        return getACParamUnitItemListBySensorTypeParamType(sensorType, paramType);
    }

    public List<SelectItem> getDCUnitList(String sensorType) {
        return getDCParamUnitItemListBySensorType(sensorType);
    }
    
    public List<SelectItem> getWaveTime() {
       return ParamUnitEnum.getTimeUnits();
    }
    
    public List<SelectItem> getWaveFrequency() {
       return ParamUnitEnum.getFrequencyPlusUnits();
  
    }
    
    public List<SelectItem> getAmpFactorItemList(String ampFactorType) {
       return AmpFactorsEnum.getAmpFactorItemListByParamType(ampFactorType);
    }

    public UserPreferencesVO getUserPreferencesVO() {
        return userPreferencesVO;
    }

    public DefaultUnits getDefaultUnits() {
        return defaultUnits;
    }

    public String getUserName() {
        return userName;
    }

    public List<UserSitesVO> getUserSiteList() {
        return userSiteList;
    }
    
    public List<SelectItem> getCompleteSiteList() {
        List <SelectItem> list = new ArrayList();
        
        try {
            regionCountrySiteMap.keySet().forEach(region -> regionCountrySiteMap.get(region).keySet().forEach(country -> list.addAll(regionCountrySiteMap.get(region).get(country))));
            return list.stream().sorted(Comparator.comparing(SelectItem::getLabel)).collect(Collectors.toList());
        } catch (Exception e) {
            return new ArrayList();
        }
    }

    public UserSitesVO getCurrentSite() {
        return currentSite;
    }

    public UserPreferencesVO getAccountUserPreferencesVO() {
        return accountUserPreferencesVO;
    }

    public DefaultUnits getAccountDefaultUnits() {
        return accountDefaultUnits;
    }

    public PresetUtil getPresetUtil() {
        return presetUtil;
    }

    public Filters getSelectedFilters() {
        return selectedFilters;
    }

    public boolean isGlobalGroups() {
        return globalGroups;
    }

    public boolean isProvidedCustomerRole() {
        return providedCustomerRole;
    }

    public CustomersVO getSelectedCustomersVO() {
        return selectedCustomersVO;
    }

    public void setSelectedCustomersVO(CustomersVO selectedCustomersVO) {
        this.selectedCustomersVO = selectedCustomersVO;
    }

    public CustomersVO getLoginCustomerVO() {
        return loginCustomerVO;
    }

    public String getSelectedTimezone() {
        return selectedTimezone;
    }

    public void setSelectedTimezone(String selectedTimezone) {
        this.selectedTimezone = selectedTimezone;
    }

    public Map<String, String> getTimezoneMap() {
        return timezoneMap;
    }

    public String getDisplayedTimeZone() {
        return displayedTimeZone;
    }

    public Map<String, Map<String, List<SelectItem>>> getRegionCountrySiteMap() {
        return regionCountrySiteMap;
    }
}
