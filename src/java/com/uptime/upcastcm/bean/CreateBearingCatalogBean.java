/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.FaultFrequenciesClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import com.uptime.upcastcm.utils.vo.BearingCatalogVO;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import org.primefaces.PrimeFaces;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.ToggleSelectEvent;
import org.primefaces.event.UnselectEvent;

/**
 *
 * @author joseph
 */
public class CreateBearingCatalogBean implements Serializable {
    private List<String> vendorList;
    private String selectedVendor;
    private List<BearingCatalogVO> bearingCatalogVOs, selectedBearingCatalogs, existingBearingCatalogs, vendorBearingCatalogs;
    private final UserManageBean userManageBean;
    private String rowsPerPage = "10";
    private boolean edited;

    public CreateBearingCatalogBean() {
        vendorList = new ArrayList();
        bearingCatalogVOs = new ArrayList();
        selectedBearingCatalogs = new ArrayList();
        existingBearingCatalogs = new ArrayList();
        vendorBearingCatalogs = new ArrayList();
        vendorList = FaultFrequenciesClient.getInstance().getVendors(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME));
        Collections.sort(vendorList);
        userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
            
    }

    /**
     * Post Constructor
     */
    @PostConstruct
    public void init() {
        resetPage();
    }

    public void resetPage() {
        selectedVendor = null;
        bearingCatalogVOs = new ArrayList();
        selectedBearingCatalogs = new ArrayList();
        clearDataTables();
    }

    public void updateBean() {
        Logger.getLogger(CreateBearingCatalogBean.class.getName()).log(Level.INFO, "CreateBearingCatalogBean.updateBean() called. ***********");
    }

    public void onVendorChange() {
        edited = false;
        selectedBearingCatalogs = new ArrayList();
        vendorBearingCatalogs.clear();
        if (userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null) {
            existingBearingCatalogs = FaultFrequenciesClient.getInstance().getSiteBearingFavoritesByVendorId(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), userManageBean.getCurrentSite().getSiteId(), selectedVendor);
        }
        bearingCatalogVOs = FaultFrequenciesClient.getInstance().getBearingCatalog(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), selectedVendor);

        rowsPerPage = "10";
        if (!existingBearingCatalogs.isEmpty()) {
            existingBearingCatalogs.stream().forEach(bc1 -> {
                bearingCatalogVOs.stream().forEach(bc -> {
                    if (bc1.getVendorId().equals(bc.getVendorId()) && bc1.getBearingId().equals(bc.getBearingId())) {
                        selectedBearingCatalogs.add(bc);
                        vendorBearingCatalogs.add(bc);
                    }
                });
            });
        }
        clearDataTables();
    }

    /**
     * Clear the DataTables
     */
    public void clearDataTables() {
        DataTable datatable;
        
        try {
            if ((datatable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("createBearingCatalogFormId:dt-bearing-catalog")) != null) {
                datatable.reset();
            }
        } catch (Exception e) {
            Logger.getLogger(AlarmsBean.class.getName()).log(Level.WARNING, e.getMessage(), e);
        }

    }

    /**
     * Action to occur on row select event
     *
     * @param event
     */
    public void onRowSelect(SelectEvent<BearingCatalogVO> event) {
        edited = true;
        if (selectedBearingCatalogs == null) {
            selectedBearingCatalogs = new ArrayList();
        }

        if (event != null && event.getObject() != null && !selectedBearingCatalogs.contains(event.getObject())) {
            selectedBearingCatalogs.add(event.getObject());
        }
    }

    /**
     * Action to occur on row unselect event
     *
     * @param event
     */
    public void onRowUnselect(UnselectEvent<BearingCatalogVO> event) {
        edited = true;
        if (selectedBearingCatalogs != null && event != null && event.getObject() != null) {
            selectedBearingCatalogs.remove(event.getObject());
        }
    }

    /**
     * Action to occur on toggle select all event
     *
     * @param event
     */
    public void dataTableSelectAllRows(ToggleSelectEvent event) {
        edited = true;
        if (event.isSelected()) {
            selectedBearingCatalogs = new ArrayList();
            selectedBearingCatalogs = bearingCatalogVOs;
        } else {
            selectedBearingCatalogs = new ArrayList();
        }
    }

    /**
     * bearing catalog button message.
     *
     * @return String Object
     */
    public String bearingCatalogButtonMessage() {
        if (userManageBean != null) {
            return (selectedBearingCatalogs != null && !selectedBearingCatalogs.isEmpty()) ? userManageBean.getResourceBundleString("btn_save_preferred") + " (" + selectedBearingCatalogs.size() + ")" : userManageBean.getResourceBundleString("btn_save_preferred");
        }
        return null;
    }

    public void submit() {
        List<BearingCatalogVO> deleteBearingCatalogVOs = null;
        
        if (userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null) {
            if (selectedBearingCatalogs != null) {
                selectedBearingCatalogs.stream().forEachOrdered(vo -> {
                    vo.setCustomerAcct(userManageBean.getCurrentSite().getCustomerAccount());
                    vo.setSiteId(userManageBean.getCurrentSite().getSiteId());
                });
            }

            if (vendorBearingCatalogs != null) {
                deleteBearingCatalogVOs = new ArrayList();
                for (BearingCatalogVO existingBearingCatalog : vendorBearingCatalogs) {
                    if (selectedBearingCatalogs == null || !selectedBearingCatalogs.contains(existingBearingCatalog)) {
                        existingBearingCatalog.setCustomerAcct(userManageBean.getCurrentSite().getCustomerAccount());
                        existingBearingCatalog.setSiteId(userManageBean.getCurrentSite().getSiteId());
                        deleteBearingCatalogVOs.add(existingBearingCatalog);
                    }
                }
            }

            FaultFrequenciesClient.getInstance().updateSiteBearingFavorites(selectedBearingCatalogs, deleteBearingCatalogVOs);
            ((PresetsBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("presetsBean")).resetFaultFrequenciesTab();
            edited = false;
            clearDataTables();
            PrimeFaces.current().ajax().update("presetsTabViewId:faultFrequenciesFormId");
            PrimeFaces.current().executeScript("PF('defaultDlg').hide()");
        }
    }

    public List<String> getVendorList() {
        return vendorList;
    }

    public String getSelectedVendor() {
        return selectedVendor;
    }

    public void setSelectedVendor(String selectedVendor) {
        this.selectedVendor = selectedVendor;
    }

    public List<BearingCatalogVO> getBearingCatalogVOs() {
        return bearingCatalogVOs;
    }

    public List<BearingCatalogVO> getSelectedBearingCatalogs() {
        return selectedBearingCatalogs;
    }

    public void setSelectedBearingCatalogs(List<BearingCatalogVO> selectedBearingCatalogs) {
        this.selectedBearingCatalogs = selectedBearingCatalogs;
    }

    public List<BearingCatalogVO> getVendorBearingCatalogs() {
        return vendorBearingCatalogs;
    }

    public void setVendorBearingCatalogs(List<BearingCatalogVO> vendorBearingCatalogs) {
        this.vendorBearingCatalogs = vendorBearingCatalogs;
    }

    public String getRowsPerPage() {
        return rowsPerPage;
    }

    public boolean isEdited() {
        return edited;
    }

    public void setEdited(boolean edited) {
        this.edited = edited;
    }

}
