/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.FaultFrequenciesClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import com.uptime.upcastcm.utils.enums.ParamUnitEnum;
import static com.uptime.upcastcm.utils.enums.ParamUnitEnum.getFrequencyPlusUnits;
import static com.uptime.upcastcm.utils.helperclass.GenericMessage.setNonAutoGrowlMsg;
import com.uptime.upcastcm.utils.helperclass.Utils;
import com.uptime.upcastcm.utils.vo.FaultFrequenciesVO;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import org.primefaces.PrimeFaces;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.ToggleSelectEvent;
import org.primefaces.event.UnselectEvent;

/**
 *
 * @author twilcox
 */
public class CreateCustomFaultFrequecySetsBean implements Serializable {
    private UserManageBean userManageBean;
    private NavigationBean navigationBean;
    private final List<FaultFrequenciesVO> faultFrequencyList, filteredFaultFrequencyList, distinctFaultFrequencies;
    private List<FaultFrequenciesVO> siteFFSetFavorites, selectedFaultFrequencyList;
    private boolean edited;

    /**
     * Creates a new instance of CreateCustomFaultFrequecySets
     */
    public CreateCustomFaultFrequecySetsBean() {
        filteredFaultFrequencyList = new ArrayList();
        distinctFaultFrequencies = new ArrayList();
        faultFrequencyList = new ArrayList();

        try {
            navigationBean = (NavigationBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("navigationBean");
            userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
        } catch (Exception e) {
            Logger.getLogger(CreateCustomFaultFrequecySetsBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            navigationBean = null;
            userManageBean = null;
        }
    }

    /**
     * Post Constructor
     */
    @PostConstruct
    public void init() {
        resetPage();
    }

    public void resetPage() {
        DataTable datatable;

        try {
            //Clear the DataTables
            try {
                if ((datatable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("customFaultFrequencieySetsFormId:dt-setNameFaultFrequenciesId")) != null) {
                    datatable.reset();
                }
            } catch (Exception e) {
                Logger.getLogger(CreateCustomFaultFrequecySetsBean.class.getName()).log(Level.WARNING, e.getMessage(), e);
            }

            edited = false;
            faultFrequencyList.clear();
            distinctFaultFrequencies.clear();
            filteredFaultFrequencyList.clear();
            siteFFSetFavorites = new ArrayList();
            selectedFaultFrequencyList = new ArrayList();

            if (!userManageBean.getCurrentSite().getCustomerAccount().equalsIgnoreCase(DEFAULT_UPTIME_ACCOUNT)) {
                faultFrequencyList.addAll(FaultFrequenciesClient.getInstance().getGlobalFaultFrequenciesByCustomer(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), DEFAULT_UPTIME_ACCOUNT));
            }
            faultFrequencyList.addAll(FaultFrequenciesClient.getInstance().getGlobalFaultFrequenciesByCustomer(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount()));
            faultFrequencyList.addAll(FaultFrequenciesClient.getInstance().getSiteFaultFrequenciesByCustomerAndSiteId(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), userManageBean.getCurrentSite().getSiteId()));
            distinctFaultFrequencies.addAll(Utils.getUniqueFFList(faultFrequencyList));
            siteFFSetFavorites = FaultFrequenciesClient.getInstance().getSiteFFSetFavoritesVOList(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), userManageBean.getCurrentSite().getSiteId());

            siteFFSetFavorites.stream().filter(ff -> ff.getFfSetId() != null).filter(Utils.distinctByKeys(FaultFrequenciesVO::getCustomerAccount, FaultFrequenciesVO::getSiteId, FaultFrequenciesVO::getFfSetId)).forEachOrdered(cff -> {
                distinctFaultFrequencies.stream().forEachOrdered(ff -> {
                    if (ff.getFfSetId().equals(cff.getFfSetId())) {
                        selectedFaultFrequencyList.add(ff);
                    }
                });
            });
        } catch (Exception e) {
            Logger.getLogger(CreateCustomFaultFrequecySetsBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);

            edited = false;
            faultFrequencyList.clear();
            distinctFaultFrequencies.clear();
            filteredFaultFrequencyList.clear();
            siteFFSetFavorites = new ArrayList();
            selectedFaultFrequencyList = new ArrayList();
        }
    }

    /**
     * Action to occur on row select event
     *
     * @param event
     */
    public void onRowSelect(SelectEvent<FaultFrequenciesVO> event) {
        edited = true;

        if (selectedFaultFrequencyList == null) {
            selectedFaultFrequencyList = new ArrayList();
        }

        if (event != null && event.getObject() != null && !selectedFaultFrequencyList.contains(event.getObject())) {
            selectedFaultFrequencyList.add(event.getObject());
        }
    }

    /**
     * Action to occur on row unselect event
     *
     * @param event
     */
    public void onRowUnselect(UnselectEvent<FaultFrequenciesVO> event) {
        edited = true;

        if (selectedFaultFrequencyList != null && event != null && event.getObject() != null) {
            selectedFaultFrequencyList.remove(event.getObject());
        }
    }

    /**
     * Action to occur on toggle select all event
     *
     * @param event
     */
    public void dataTableSelectAllRows(ToggleSelectEvent event) {
        edited = true;

        if (event != null) {
            if (event.isSelected()) {
                selectedFaultFrequencyList = new ArrayList();
                selectedFaultFrequencyList = distinctFaultFrequencies;
            } else {
                selectedFaultFrequencyList = new ArrayList();
            }
        }
    }

    /**
     *
     * @param faultFrequenciesVO
     */
    public void getFaultFrequenciesByRowToggle(FaultFrequenciesVO faultFrequenciesVO) {
        filteredFaultFrequencyList.clear();
        if (faultFrequenciesVO != null) {
            filteredFaultFrequencyList.addAll(faultFrequencyList.stream().filter(ff -> ff.getFfSetId().equals(faultFrequenciesVO.getFfSetId())).collect(Collectors.toList()));
        }
    }

    /**
     * Submit
     */
    public void submit() {
        List<FaultFrequenciesVO> addFaultFrequenciesVOList, deleteFaultFrequenciesVOList = null, faultFrequencySet;
        List<UUID> ffSetIds;

        try {
            if (userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null) {
                addFaultFrequenciesVOList = new ArrayList();
                faultFrequencySet = new ArrayList();

                if (selectedFaultFrequencyList != null) {
                    selectedFaultFrequencyList.stream().forEachOrdered(vo -> faultFrequencySet.addAll(faultFrequencyList.stream().filter(ff -> ff.getFfSetId().equals(vo.getFfSetId())).collect(Collectors.toList())));

                    faultFrequencySet.stream().forEachOrdered(vo -> {
                        FaultFrequenciesVO faultFrequenciesVO;

                        faultFrequenciesVO = new FaultFrequenciesVO();
                        faultFrequenciesVO.setCustomerAccount(userManageBean.getCurrentSite().getCustomerAccount());
                        faultFrequenciesVO.setFfId(vo.getFfId());
                        faultFrequenciesVO.setFfName(vo.getFfName());
                        faultFrequenciesVO.setFfSetDesc(vo.getFfSetDesc());
                        faultFrequenciesVO.setFfSetId(vo.getFfSetId());
                        faultFrequenciesVO.setFfSetName(vo.getFfSetName());
                        faultFrequenciesVO.setFfUnit(vo.getFfUnit());
                        faultFrequenciesVO.setFfValue(vo.getFfValue());
                        faultFrequenciesVO.setGlobal(vo.getSiteId() == null);
                        faultFrequenciesVO.setSiteId(userManageBean.getCurrentSite().getSiteId());

                        addFaultFrequenciesVOList.add(faultFrequenciesVO);
                    });

                    if (siteFFSetFavorites != null && !siteFFSetFavorites.isEmpty()) {
                        ffSetIds = selectedFaultFrequencyList.stream().map(FaultFrequenciesVO::getFfSetId).sorted().collect(Collectors.toList());
                        deleteFaultFrequenciesVOList = siteFFSetFavorites.stream().filter(ff -> ff.getFfSetId() != null && !ffSetIds.contains(ff.getFfSetId())).collect(Collectors.toList());
                    }
                } else if (siteFFSetFavorites != null && !siteFFSetFavorites.isEmpty()) {
                    deleteFaultFrequenciesVOList = siteFFSetFavorites.stream().filter(ff -> ff.getFfSetId() != null).collect(Collectors.toList());
                }

                if (FaultFrequenciesClient.getInstance().updateSiteFFSetFavoritesVOList(addFaultFrequenciesVOList, deleteFaultFrequenciesVOList)) {
                    PrimeFaces.current().executeScript("PF('defaultDlg').hide()");
                    ((PresetsBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("presetsBean")).resetPage();
                    edited = false;
                }
            }
        } catch (Exception e) {
            Logger.getLogger(CreateCustomFaultFrequecySetsBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Deletes the given object
     *
     * @param faultFrequenciesVO, FaultFrequenciesVO Object
     */
    public void delete(FaultFrequenciesVO faultFrequenciesVO) {
        List<FaultFrequenciesVO> faultFrequenciesVOs;

        try {
            if (faultFrequenciesVO != null) {
                faultFrequenciesVOs = faultFrequencyList.stream().filter(ff -> ff.getFfSetId().equals(faultFrequenciesVO.getFfSetId()) && ff.getSiteId() != null && ff.getSiteId().equals(faultFrequenciesVO.getSiteId())).collect(Collectors.toList());
                if (FaultFrequenciesClient.getInstance().deleteSiteFaultFrequencies(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), faultFrequenciesVOs)) {
                    setNonAutoGrowlMsg("growl_delete_item_items");
                    resetPage();
                    PrimeFaces.current().ajax().update("customFaultFrequencieySetsFormId");
                }
            }
        } catch (Exception e) {
            Logger.getLogger(CreateCustomFaultFrequecySetsBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Upload the faultFrequencies from the given file
     *
     * @param event, FileUploadEvent Object
     */
    public void faultFrequencieUpload(FileUploadEvent event) {
        List<FaultFrequenciesVO> list = new ArrayList();
        String[] elements;
        int errorCount, validationFailedCount;
        FaultFrequenciesVO faultFrequenciesVO;
        UUID ffSetId;
        String line;

        if (userManageBean != null && userManageBean.getCurrentSite() != null && event != null && event.getFile() != null) {
            Logger.getLogger(CreateCustomFaultFrequecySetsBean.class.getName()).log(Level.INFO, "upload started....");
            System.out.println("upload started....");

            errorCount = 0;
            validationFailedCount = 0;
            try ( BufferedReader br = new BufferedReader(new InputStreamReader(event.getFile().getInputStream()))) {
                br.readLine(); // Skip Header

                ffSetId = UUID.randomUUID();
                while ((line = br.readLine()) != null) {
                    try {
                        if ((elements = line.split(",")).length == 8) {
                            faultFrequenciesVO = new FaultFrequenciesVO();
                            faultFrequenciesVO.setCustomerAccount(elements[0]);
                            faultFrequenciesVO.setSiteId(userManageBean.getCurrentSite().getSiteId());
                            faultFrequenciesVO.setFfSetId(ffSetId);
                            faultFrequenciesVO.setFfId(UUID.randomUUID());
                            faultFrequenciesVO.setFfSetName(elements[1]);
                            faultFrequenciesVO.setFfName(elements[2]);
                            faultFrequenciesVO.setFfSetDesc(elements[3]);
                            faultFrequenciesVO.setFfValue(Float.parseFloat(elements[4]));
                            faultFrequenciesVO.setFfUnit(elements[5]);

                            if (faultFrequenciesVO.getCustomerAccount() != null && faultFrequenciesVO.getCustomerAccount().equals(userManageBean.getCurrentSite().getCustomerAccount())
                                    && faultFrequenciesVO.getSiteId() != null
                                    && faultFrequenciesVO.getFfSetName() != null && !faultFrequenciesVO.getFfSetName().isEmpty()
                                    && faultFrequenciesVO.getFfName() != null && !faultFrequenciesVO.getFfName().isEmpty()
                                    && faultFrequenciesVO.getFfUnit() != null && !faultFrequenciesVO.getFfUnit().isEmpty()
                                    && getFrequencyPlusUnits().contains(faultFrequenciesVO.getFfUnit())) {
                                list.add(faultFrequenciesVO);
                            } else {
                                validationFailedCount++;
                            }
                        }
                    } catch (Exception e) {
                        errorCount++;
                    }
                }

                FaultFrequenciesClient.getInstance().createSiteFaultFrequencies(list, errorCount, validationFailedCount);
                resetPage();
                PrimeFaces.current().ajax().update("customFaultFrequencieySetsFormId");
            } catch (Exception e) {
                Logger.getLogger(CreateCustomFaultFrequecySetsBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            }
        }
    }

    /**
     * Set the selectedFaultFrequency, operationType, and open the dialog box
     *
     * @param faultFrequenciesVO, FaultFrequenciesVO Object
     * @param operationType, int
     */
    public void setFaultFrequenciesDialog(FaultFrequenciesVO faultFrequenciesVO, int operationType) {
        try {
            if (faultFrequenciesVO != null && operationType > 0 && operationType <= 2 && navigationBean != null) {
                if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("copyEditFaultFrequenciesBean") == null) {
                    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("copyEditFaultFrequenciesBean", new CopyEditFaultFrequenciesBean());
                }
                ((CopyEditFaultFrequenciesBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("copyEditFaultFrequenciesBean")).presetFields(operationType, faultFrequenciesVO);
                if (operationType == 1) {
                    navigationBean.updateSecondDialog("edit-fault-frequencies");
                } else {
                    navigationBean.updateSecondDialog("copy-fault-frequencies");
                }

                PrimeFaces.current().executeScript("PF('defaultSecondDlg').show()");
            } else {
                if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("createFaultFrequenciesBean") == null) {
                    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("createFaultFrequenciesBean", new CreateFaultFrequenciesBean());
                }
                ((CreateFaultFrequenciesBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("createFaultFrequenciesBean")).init();

                navigationBean.updateSecondDialog("create-fault-frequencies");
                PrimeFaces.current().executeScript("PF('defaultSecondDlg').show()");
            }
        } catch (Exception e) {
            Logger.getLogger(CreateCustomFaultFrequecySetsBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * custom faultFrequencies set button message.
     *
     * @return String Object
     */
    public String customFaultFrequenciesButtonMessage() {
        if (userManageBean != null) {
            return (selectedFaultFrequencyList != null && !selectedFaultFrequencyList.isEmpty()) ? userManageBean.getResourceBundleString("btn_save_preferred") + " (" + selectedFaultFrequencyList.size() + ")" : userManageBean.getResourceBundleString("btn_save_preferred");
        }
        return null;
    }

    public String getParamUnitLabelByValue(String value) {
        return ParamUnitEnum.getParamUnitLabelByValue(value);
    }

    public List<FaultFrequenciesVO> getFaultFrequencyList() {
        return faultFrequencyList;
    }

    public List<FaultFrequenciesVO> getFilteredFaultFrequencyList() {
        return filteredFaultFrequencyList;
    }

    public List<FaultFrequenciesVO> getDistinctFaultFrequencies() {
        return distinctFaultFrequencies;
    }

    public List<FaultFrequenciesVO> getSelectedFaultFrequencyList() {
        return selectedFaultFrequencyList;
    }

    public void setSelectedFaultFrequencyList(List<FaultFrequenciesVO> selectedFaultFrequencyList) {
        this.selectedFaultFrequencyList = selectedFaultFrequencyList;
    }

    public boolean isEdited() {
        return edited;
    }

    public void setEdited(boolean edited) {
        this.edited = edited;
    }
}
