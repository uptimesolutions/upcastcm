/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import static com.uptime.client.http.servlet.AbstractServiceManagerServlet.getPropertyValue;
import com.uptime.upcastcm.http.client.LdapClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import static com.uptime.upcastcm.utils.EmailUtil.createAndSendPasswordResetEmail;
import com.uptime.upcastcm.utils.vo.LdapUserVO;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author madhavi
 */
public class ForgotPasswordBean implements Serializable {
    private final String host, customerAccount;
    
    private boolean emailSent;
    private String email;
    private LdapUserVO ldapUserVO;

    /**
     * Constructor
     */
    public ForgotPasswordBean() {
        HttpServletRequest request;
        
        request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();     
        customerAccount = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("account_number");
        host = request.getHeader("host");
        emailSent = false;
    }

    /**
     * Create password token
     */
    public void createPasswordToken() {
        try {
            if ((ldapUserVO = LdapClient.getInstance().getUserAccountByUserId(ServiceRegistrarClient.getInstance().getServiceHostURL(LDAP_SERVICE_NAME), email.toLowerCase(), customerAccount)) != null) {
                createAndSendPasswordResetEmail(ldapUserVO, customerAccount, host, getPropertyValue("UPCAST_URL_PROTOCOL"));
                emailSent = true;
            } else {
                Logger.getLogger(ForgotPasswordBean.class.getName()).log(Level.INFO, "Invalid user: {0}", ldapUserVO.getUserId());
                Logger.getLogger(ForgotPasswordBean.class.getName()).log(Level.INFO, "customerAccount : {0}", customerAccount);
                emailSent = false;
            }
        } catch (Exception ex) {
            Logger.getLogger(ForgotPasswordBean.class.getName()).log(Level.INFO, "Exception in Token creation : {0}", ex.toString());
            emailSent = false;
        }
    }

    public boolean isEmailSent() {
        return emailSent;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
