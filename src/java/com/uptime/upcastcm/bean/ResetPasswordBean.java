/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import static com.uptime.client.http.servlet.AbstractServiceManagerServlet.getPropertyValue;
import com.uptime.upcastcm.http.client.LdapClient;
import com.uptime.upcastcm.http.client.UserClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import com.uptime.upcastcm.utils.vo.LdapUserVO;
import com.uptime.upcastcm.utils.vo.PasswordTokenVO;
import java.io.Serializable;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author madhavi
 */
public class ResetPasswordBean implements Serializable {
    private boolean tokenVerified, tokenExpired, errorOccured, passwordUpdated;
    private final String email;
    private final String uuid;
    private final String link;
    private LdapUserVO ldapUserVO;
    private final String customerAccount;
    
    /**
     * Constructor
     */
    public ResetPasswordBean() {
        HttpServletRequest request;
        
        request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();     
        customerAccount = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("account_number");
        email = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("email");
        uuid = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("uuid");
        link = getPropertyValue("UPCAST_URL_PROTOCOL") + "://" + request.getHeader("host") + "/UpCastCM";
    }

    /**
     * Post Constructor
     */
    @PostConstruct
    public void init() {
        try {
            Logger.getLogger(ResetPasswordBean.class.getName()).log(Level.INFO, "Updating password - CustomerAccount : {0}", customerAccount);
            Logger.getLogger(ResetPasswordBean.class.getName()).log(Level.INFO, "Updating password - User: {0}", email);
            Logger.getLogger(ResetPasswordBean.class.getName()).log(Level.INFO, "Updating password - Token: {0}", uuid);
            
            if (email != null && !email.isEmpty() && uuid != null && !uuid.isEmpty()) {
                ldapUserVO = new LdapUserVO();
                ldapUserVO.setUserId(email);
                ldapUserVO.setCustomerAccount(customerAccount);
                
                if (UserClient.getInstance().checkForPasswordTokenByPK(ServiceRegistrarClient.getInstance().getServiceHostURL(USER_PREFERENCE_SERVICE_NAME), new PasswordTokenVO(email, UUID.fromString(uuid)))) {
                    Logger.getLogger(ResetPasswordBean.class.getName()).log(Level.INFO, "Password Token present.");
                    if ((ldapUserVO = LdapClient.getInstance().getUserAccountByUserId(ServiceRegistrarClient.getInstance().getServiceHostURL(LDAP_SERVICE_NAME), email.toLowerCase(),customerAccount)) != null) {
                        tokenVerified = true;
                    } else {
                        Logger.getLogger(ResetPasswordBean.class.getName()).log(Level.INFO, "Invalid user: {0}", email);
                    }
                } else {
                    Logger.getLogger(ResetPasswordBean.class.getName()).log(Level.INFO, "Password update link expired. User: {0}", email);
                    tokenExpired = true;
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(ResetPasswordBean.class.getName()).log(Level.INFO, "Exception in init: {0}", ex.toString());
            errorOccured = true;
        }
    }

    /**
     * Save user password into Ldap server
     */
    public void savePassword() {
        tokenVerified = false;
        
        ldapUserVO.setCustomerAccount(customerAccount);
        Logger.getLogger(ResetPasswordBean.class.getName()).log(Level.INFO, "Password updatiom for user: {0}", ldapUserVO.toString());
        if (LdapClient.getInstance().updateUsers(ServiceRegistrarClient.getInstance().getServiceHostURL(LDAP_SERVICE_NAME), ldapUserVO, "updateUserPassword")) {
            passwordUpdated = true;
        } else {
            errorOccured = true;
        }
    }

    public boolean isTokenVerified() {
        return tokenVerified;
    }

    public boolean isTokenExpired() {
        return tokenExpired;
    }

    public boolean isErrorOccured() {
        return errorOccured;
    }

    public boolean isPasswordUpdated() {
        return passwordUpdated;
    }

    public LdapUserVO getLdapUserVO() {
        return ldapUserVO;
    }

    public void setLdapUserVO(LdapUserVO ldapUserVO) {
        this.ldapUserVO = ldapUserVO;
    }

    public String getLink() {
        return link;
    }
}
