/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.PointLocationNamesClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import com.uptime.upcastcm.utils.helperclass.Filters;
import static com.uptime.upcastcm.utils.helperclass.GenericMessage.setNonAutoGrowlMsg;
import com.uptime.upcastcm.utils.interfaces.PresetDialogFields;
import com.uptime.upcastcm.utils.vo.PointLocationNamesVO;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import org.primefaces.PrimeFaces;

/**
 *
 * @author Joseph
 */
public class CopyEditPointLocationNameBean implements Serializable, PresetDialogFields {
    private UserManageBean userManageBean;
    private PointLocationNamesVO pointLocationNamesVO;
    private int operationType;

    /**
     * Constructor
     */
    public CopyEditPointLocationNameBean() {
        try {
            userManageBean = ((UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean"));
        } catch (Exception e) {
            Logger.getLogger(CopyEditPointLocationNameBean.class.getName()).log(Level.SEVERE, "CopyEditPointLocationNameBean Constructor Exception {0}", e.toString());
            userManageBean = null;
        }
    }

    /**
     * Post Constructor
     */
    @PostConstruct
    public void init() {
        resetPage();
        if (userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null) {
            Logger.getLogger(CopyEditPointLocationNameBean.class.getName()).log(Level.INFO, "**customerAccount to be set : {0}", userManageBean.getCurrentSite().getCustomerAccount());
        }
    }

    /**
     * Reset Overlay
     */
    @Override
    public void resetPage() {
        pointLocationNamesVO = new PointLocationNamesVO();
        operationType = -1;
        if (userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null) {
            pointLocationNamesVO.setCustomerAccount(userManageBean.getCurrentSite().getCustomerAccount());
        }
    }

    @Override
    public void presetFields(Filters presets, Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void presetFields(int operationType, Object object) {
        if (userManageBean != null && userManageBean.getCurrentSite() != null && object != null && object instanceof PointLocationNamesVO) {
            pointLocationNamesVO = new PointLocationNamesVO((PointLocationNamesVO)object);
            pointLocationNamesVO.setSiteId(userManageBean.getCurrentSite().getSiteId());
            pointLocationNamesVO.setSiteName(userManageBean.getCurrentSite().getSiteName());
            if ((this.operationType = operationType) == 2) {
                pointLocationNamesVO.setPointLocationName(null);
            }
        }
    }

    @Override
    public void presetFields(Object object) {
    }

    /**
     * Perform action on pointLocationName change
     */
    public void onPointLocationNameChange() {
        if (userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null) {
            if (pointLocationNamesVO.getPointLocationName() != null && !pointLocationNamesVO.getPointLocationName().trim().isEmpty()) {
                pointLocationNamesVO.setPointLocationName(pointLocationNamesVO.getPointLocationName().trim());
                if (PointLocationNamesClient.getInstance().getSitePointLocationNamesByPK(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), pointLocationNamesVO.getSiteId(), pointLocationNamesVO.getPointLocationName()).size() > 0) {
                    pointLocationNamesVO.setPointLocationName(null);
                    setNonAutoGrowlMsg("growl_validate_point_location_name");
                }
            } else {
                pointLocationNamesVO.setPointLocationName(null);
            }
        }
    }

    /**
     * Submit copied or edited pointLocationName item
     */
    public void submit() {
        // For Copy
        if (this.operationType == 2) { 
            if (PointLocationNamesClient.getInstance().createSitePointLocationNames(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), pointLocationNamesVO)) {
                setNonAutoGrowlMsg("growl_create_item_items");
                Logger.getLogger(CreateFaultFrequenciesBean.class.getName()).log(Level.INFO, "Site Point Location Names created successfully. {0}", new Object[]{pointLocationNamesVO});
            } else {
                Logger.getLogger(CreateFaultFrequenciesBean.class.getName()).log(Level.INFO, "Error in creating Site Point Location Names. frequenciesVO.getSiteName()*********** {0}", new Object[]{pointLocationNamesVO.getSiteName()});
            }
        }

        // Edit
        else { 
            if (PointLocationNamesClient.getInstance().updateSitePointLocationNames(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), pointLocationNamesVO)) {
                setNonAutoGrowlMsg("growl_update_item_items");
                Logger.getLogger(CreateFaultFrequenciesBean.class.getName()).log(Level.INFO, "Site Point Location Names updated successfully. {0}", new Object[]{pointLocationNamesVO});
            } else {
                Logger.getLogger(CreateFaultFrequenciesBean.class.getName()).log(Level.INFO, "Error in updating Site Point Location Names. frequenciesVO.getSiteName()*********** {0}", new Object[]{pointLocationNamesVO.getSiteName()});
            }
        }

        ((PresetsBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("presetsBean")).resetPointLocationNamesTab();
        PrimeFaces.current().ajax().update("presetsTabViewId:pointLocationNamesFormId");
        PrimeFaces.current().executeScript("PF('defaultDlg').hide()");
    }

    public PointLocationNamesVO getPointLocationNamesVO() {
        return pointLocationNamesVO;
    }

    public void setPointLocationNamesVO(PointLocationNamesVO pointLocationNamesVO) {
        this.pointLocationNamesVO = pointLocationNamesVO;
    }

    public int getOperationType() {
        return operationType;
    }

    public void setOperationType(int operationType) {
        this.operationType = operationType;
    }
}
