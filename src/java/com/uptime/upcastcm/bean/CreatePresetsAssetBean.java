/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.AssetClient;
import com.uptime.upcastcm.http.client.AssetPresetClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.DEFAULT_UPTIME_ACCOUNT;
import static com.uptime.upcastcm.utils.ApplicationConstants.PRESET_SERVICE_NAME;
import static com.uptime.upcastcm.utils.helperclass.GenericMessage.setNonAutoGrowlMsg;
import com.uptime.upcastcm.utils.vo.AssetPresetPointLocationVO;
import com.uptime.upcastcm.utils.vo.AssetPresetVO;
import com.uptime.upcastcm.utils.vo.AssetTypesVO;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.primefaces.PrimeFaces;

/**
 *
 * @author joseph
 */
public class CreatePresetsAssetBean implements Serializable {

    private UserManageBean userManageBean;
    private String siteOrGlobal;
    private NavigationBean navigationBean;
    private AssetPresetVO assetPresetVO;
    private List<String> assetTypeList;

    /**
     * Constructor
     */
    public CreatePresetsAssetBean() {

        try {
            assetTypeList = new ArrayList();
            navigationBean = (NavigationBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("navigationBean");
            userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");

            List<AssetTypesVO> assetTypesVOList = new ArrayList();
            assetTypesVOList.addAll(AssetClient.getInstance().getGlobalAssetTypesByCustomer(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount()));
            if (!userManageBean.getCurrentSite().getCustomerAccount().equals(DEFAULT_UPTIME_ACCOUNT)) {
                assetTypesVOList.addAll(AssetClient.getInstance().getGlobalAssetTypesByCustomer(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), DEFAULT_UPTIME_ACCOUNT));
            }
            assetTypesVOList.stream().forEach(a -> assetTypeList.add(a.getAssetTypeName()));

        } catch (Exception e) {
            Logger.getLogger(CreatePresetsAssetBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            userManageBean = null;
            navigationBean = null;
        }
    }

    /**
     * Post Constructor
     */
    @PostConstruct
    public void init() {
        resetPage();
    }

    public void resetPage() {
        siteOrGlobal = null;
        assetPresetVO = new AssetPresetVO();
        assetPresetVO.setSampleInterval(720);
        assetPresetVO.setAssetPresetPointLocationVOList(new ArrayList());
    }

    public void updateBean() {
    }

    public void updateSampleInteval() {
        if (assetPresetVO.getSampleInterval() <= 0 || assetPresetVO.getSampleInterval() > 1440) {
            assetPresetVO.setSampleInterval(720);
            FacesContext.getCurrentInstance().addMessage("growlMessages", new FacesMessage(userManageBean.getResourceBundleString("message_sample_interval_minimum")));
        }
    }

    public void onSiteChange() {
        assetPresetVO.getAssetPresetPointLocationVOList().clear();
    }

    /**
     * Open the Point Location Overlay
     */
    public void openPointLocationOverlay() {
        try {
            assetPresetVO.setType(siteOrGlobal);
            if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("addPresetsPointLocationBean") == null) {
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("addPresetsPointLocationBean", new AddPresetsPointLocationBean());
                ((AddPresetsPointLocationBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("addPresetsPointLocationBean")).init();
            }
            ((AddPresetsPointLocationBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("addPresetsPointLocationBean")).presetFields(assetPresetVO);
            navigationBean.updateSecondDialog("add-preset-point-location");
        } catch (Exception e) {
            Logger.getLogger(CreateWorkOrderBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     *
     * @param assetPresetPointLocationVO
     * @param operationType, int
     */
    public void setPointLocationDialog(AssetPresetPointLocationVO assetPresetPointLocationVO, int operationType) {
        try {
            if (operationType >= 0 && operationType <= 2 && navigationBean != null) {
                if ((operationType == 2 || operationType == 1) && assetPresetVO != null && assetPresetPointLocationVO != null) {
                    if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("copyEditPresetsPointLocationBean") == null) {
                        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("copyEditPresetsPointLocationBean", new CopyEditPresetsPointLocationBean());
                        ((CopyEditPresetsPointLocationBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("copyEditPresetsPointLocationBean")).init();
                    }
                    ((CopyEditPresetsPointLocationBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("copyEditPresetsPointLocationBean")).presetFields(operationType, assetPresetPointLocationVO, assetPresetVO);
                    if (operationType == 1) {
                        navigationBean.updateSecondDialog("edit-preset-point-location");
                    } else {
                        navigationBean.updateSecondDialog("copy-preset-point-location");
                    }
                }
            }
        } catch (Exception e) {
            Logger.getLogger(CreatePresetsAssetBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Deletes the given object
     *
     * @param obj, Object
     * @param tab, String Object
     */
    public void delete(Object obj, String tab) {
        AssetPresetPointLocationVO pointLocationNamesVO;
        if (obj != null && tab != null) {
            try {
                switch (tab) {
                    case "pointLocationNames":
                        pointLocationNamesVO = (AssetPresetPointLocationVO) obj;
                        assetPresetVO.getAssetPresetPointLocationVOList().remove(pointLocationNamesVO);
//                        PrimeFaces.current().ajax().update("copyEditPresetsAssetFormId:pointLocationDatatableId");
//                        PrimeFaces.current().ajax().update("copyEditPresetsAssetFormId:assetPresetSaveId");
                        PrimeFaces.current().ajax().update("createAssetsFormId:pointLocationDatatableId");
                        PrimeFaces.current().ajax().update("createAssetsFormId:assetPresetSaveId2");
                        break;

                }
            } catch (Exception e) {
                Logger.getLogger(PresetsBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            }
        }
    }

    public void submit() {
        List<AssetPresetVO> list = new ArrayList();

        if (siteOrGlobal.equalsIgnoreCase("global")) {
            assetPresetVO.getAssetPresetPointLocationVOList().forEach(pl -> {
                AssetPresetVO assetPreset = new AssetPresetVO();
                assetPreset.setCustomerAccount(userManageBean.getCurrentSite().getCustomerAccount());
                assetPreset.setSiteId(null);
                assetPreset.setAssetTypeName(assetPresetVO.getAssetTypeName());
                assetPreset.setAssetPresetId(assetPresetVO.getAssetPresetId());
                assetPreset.setPointLocationName(pl.getPointLocationName());
                assetPreset.setAssetPresetName(assetPresetVO.getAssetPresetName());
                assetPreset.setDevicePresetId(pl.getDeviceId());
                assetPreset.setSampleInterval(assetPresetVO.getSampleInterval());
                assetPreset.setFfSetIds(pl.getFfSetIds());
                assetPreset.setRollDiameter(pl.getRollDiameter());
                assetPreset.setRollDiameterUnits(pl.getRollDiameterUnits());
                assetPreset.setSpeedRatio(pl.getSpeedRatio());
                assetPreset.setTachId(pl.getTachId());
                assetPreset.setDescription(assetPresetVO.getDescription());
                assetPreset.setDevicePresetType(pl.getDeviceType());
                list.add(assetPreset);
            });
            if (AssetPresetClient.getInstance().createGlobalAssetPresetVO(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), list)) {
                setNonAutoGrowlMsg("growl_create_item_items");
                Logger.getLogger(CreatePresetsDeviceBean.class.getName()).log(Level.INFO, "Global Asset Preset created successfully.");
            } else {
                Logger.getLogger(CreatePresetsDeviceBean.class.getName()).log(Level.INFO, "Error in creating Global Asset Preset.");
            }
        } else {
            assetPresetVO.getAssetPresetPointLocationVOList().forEach(pl -> {
                AssetPresetVO assetPreset = new AssetPresetVO();
                assetPreset.setCustomerAccount(userManageBean.getCurrentSite().getCustomerAccount());
                assetPreset.setSiteId(userManageBean.getCurrentSite().getSiteId());
                assetPreset.setAssetTypeName(assetPresetVO.getAssetTypeName());
                assetPreset.setAssetPresetId(assetPresetVO.getAssetPresetId());
                assetPreset.setPointLocationName(pl.getPointLocationName());
                assetPreset.setAssetPresetName(assetPresetVO.getAssetPresetName());
                assetPreset.setDevicePresetId(pl.getDeviceId());
                assetPreset.setSampleInterval(assetPresetVO.getSampleInterval());
                assetPreset.setFfSetIds(pl.getFfSetIds());
                assetPreset.setRollDiameter(pl.getRollDiameter());
                assetPreset.setRollDiameterUnits(pl.getRollDiameterUnits());
                assetPreset.setSpeedRatio(pl.getSpeedRatio());
                assetPreset.setTachId(pl.getTachId());
                assetPreset.setDescription(assetPresetVO.getDescription());
                assetPreset.setDevicePresetType(pl.getDeviceType());
                list.add(assetPreset);
            });
            if (AssetPresetClient.getInstance().createSiteAssetPresetVO(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), list)) {
                setNonAutoGrowlMsg("growl_create_item_items");
                Logger.getLogger(CreatePresetsDeviceBean.class.getName()).log(Level.INFO, "Site Asset preset created successfully.");
            } else {
                Logger.getLogger(CreatePresetsDeviceBean.class.getName()).log(Level.INFO, "Error in creating Site Asset preset.");
            }
        }

        ((PresetsBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("presetsBean")).resetAssetsTab();
        PrimeFaces.current().ajax().update("presetsTabViewId:assetsFormId");
        PrimeFaces.current().executeScript("PF('defaultDlg').hide()");
    }
    
    /**
     * Convert null or empty String to "N/A"
     * @param value, String Object
     * @return String Object
     */
    public String convertNullToNA(String value) {
        if (value == null || value.isEmpty()) {
            return "N/A";
        } else {
            return value;
        }
    }

    public String getSiteOrGlobal() {
        return siteOrGlobal;
    }

    public void setSiteOrGlobal(String siteOrGlobal) {
        this.siteOrGlobal = siteOrGlobal;
    }

    public AssetPresetVO getAssetPresetVO() {
        return assetPresetVO;
    }

    public List<String> getAssetTypeList() {
        return assetTypeList;
    }

}
