/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.APALSetClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import static com.uptime.upcastcm.utils.enums.ACSensorTypeEnum.getACLabelByValue;
import static com.uptime.upcastcm.utils.enums.DCSensorTypeEnum.getDCLabelByValue;
import static com.uptime.upcastcm.utils.enums.DwellEnum.getDwellItemList;
import com.uptime.upcastcm.utils.helperclass.Filters;
import static com.uptime.upcastcm.utils.helperclass.GenericMessage.missingFields;
import static com.uptime.upcastcm.utils.helperclass.GenericMessage.setNonAutoGrowlMsg;
import com.uptime.upcastcm.utils.interfaces.PresetDialogFields;
import com.uptime.upcastcm.utils.vo.ApAlSetVO;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import org.primefaces.PrimeFaces;

/**
 *
 * @author twilcox
 */
public class AddAlSetBean implements Serializable, PresetDialogFields {
    private UserManageBean userManageBean;
    private final List<ApAlSetVO> alSetList;
    private List<SelectItem> dwellList;
    private boolean renderAlertMsg, global;
    private ApAlSetVO apAlSetVO;
    private Boolean acSensor;

    /**
     * Creates a new instance of addAlSetBean
     */
    public AddAlSetBean() {
        alSetList = new ArrayList();
        
        try {
            dwellList = getDwellItemList();
            userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
        } catch (Exception e) {
            Logger.getLogger(AddAlSetBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            userManageBean = null;
        }
    }

    /**
     * Post Constructor
     */
    @PostConstruct
    public void init() {
        resetPage();
    }

    @Override
    public void resetPage() {
        alSetList.clear();
        renderAlertMsg = false;
        global = false;
        apAlSetVO = new ApAlSetVO();
        acSensor = null;
    }

    @Override
    public void presetFields(Filters presets, Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void presetFields(int operationType, Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void presetFields(Object object) {
        List<ApAlSetVO> tmpList;
        ApAlSetVO newApAl;
        String sensorTypeLabel;
        UUID alSetId;
        
        resetPage();
        try {
            if (userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null && !userManageBean.getCurrentSite().getCustomerAccount().isEmpty() && object != null && object instanceof ApAlSetVO) {
                apAlSetVO = new ApAlSetVO((ApAlSetVO)object);
                
                if (apAlSetVO != null && apAlSetVO.getSensorType() != null) {
                    if ((sensorTypeLabel = getACLabelByValue(apAlSetVO.getSensorType())) != null) {
                        apAlSetVO.setSensorType(sensorTypeLabel);
                        acSensor = true;
                    } else if ((sensorTypeLabel = getDCLabelByValue(apAlSetVO.getSensorType())) != null) {
                        apAlSetVO.setSensorType(sensorTypeLabel);
                        acSensor = false;
                    }
                }

                if (apAlSetVO.getSiteId() == null) {
                    global = true;

                    tmpList = APALSetClient.getInstance().getGlobalApAlSetsByCustomerApSetId(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), apAlSetVO.getApSetId());
                } else {
                    tmpList = APALSetClient.getInstance().getSiteApAlSetsByCustomerSiteIdApSetId(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), apAlSetVO.getSiteId(), apAlSetVO.getApSetId());
                }

                if (!tmpList.isEmpty()) {
                    alSetId = tmpList.get(0).getAlSetId();

                    for (ApAlSetVO apAl : tmpList) {
                        if (apAl.getAlSetId().equals(alSetId)) {
                            newApAl = new ApAlSetVO(apAl);
                            newApAl.setAlSetId(null);
                            newApAl.setAlSetName(null);
                            newApAl.setDwell(0);
                            newApAl.setLowFaultActive(false);
                            newApAl.setLowFault(0);
                            newApAl.setLowAlertActive(false);
                            newApAl.setLowAlert(0);
                            newApAl.setHighAlertActive(false);
                            newApAl.setHighAlert(0);
                            newApAl.setHighFaultActive(false);
                            newApAl.setHighFault(0);
                            alSetList.add(newApAl);
                        }
                    }
                }

                alSetList.sort(Comparator.comparing(ApAlSetVO::getParamName));
                
                if (acSensor != null && !acSensor && !alSetList.isEmpty()) {
                    apAlSetVO.setParamUnits(alSetList.get(0).getParamUnits());
                }
            }
        } catch (Exception e) {
            Logger.getLogger(AddAlSetBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            resetPage();
        }
    }

    /**
     * Submit the given info to Cassandra
     */
    public void submit() {
        UUID alSetId;
        
        try {
            if (userManageBean != null && userManageBean.getCurrentSite() != null) {
                if (apAlSetVO != null && apAlSetVO.getAlSetName() != null && !apAlSetVO.getAlSetName().isEmpty()) {
                    alSetId = UUID.randomUUID();

                    alSetList.stream().forEachOrdered(alRow -> {
                        alRow.setAlSetId(alSetId);
                        alRow.setAlSetName(apAlSetVO.getAlSetName());
                    });

                    if (global) {
                        alSetList.stream().forEachOrdered(apAlSet -> {
                            APALSetClient.getInstance().createGlobalApAlSets(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), apAlSet);
                        });
                    } else {
                        alSetList.stream().forEachOrdered(apAlSet -> {
                            APALSetClient.getInstance().createSiteApAlSets(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), apAlSet);
                        });
                    }
                    userManageBean.getPresetUtil().clearGlobalApAlSetSensorList();
                    ((PresetsBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("presetsBean")).resetAnalysisParametersTab();
                    PrimeFaces.current().ajax().update("presetsTabViewId:analysisParametersFormId");
                    PrimeFaces.current().executeScript("PF('defaultDlg').hide()");
                    setNonAutoGrowlMsg("growl_create_item_items");
                } else {
                    missingFields();
                }
            }
        } catch (Exception e) {
            Logger.getLogger(AddAlSetBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Validate Alarm Limits
     *
     * @param apAlByPoint, ApAlSetVO object
     */
    public void alarmLimitValidation(ApAlSetVO apAlByPoint) {
        boolean lfValidationFailed, laValidationFailed, haValidationFailed, hfValidationFailed;
        
        // Validate Alarm Limits
        if (apAlByPoint != null) {

            // Validates the lowFault field of the given Object
            if (apAlByPoint.isLowFaultActive()) {
                if (apAlByPoint.getLowFault() <= 0) {
                    lfValidationFailed = true;
                } else if (apAlByPoint.isLowAlertActive()) {
                    lfValidationFailed = apAlByPoint.getLowFault() >= apAlByPoint.getLowAlert();
                } else if (apAlByPoint.isHighAlertActive()) {
                    lfValidationFailed = apAlByPoint.getLowFault() >= apAlByPoint.getHighAlert();
                } else if (apAlByPoint.isHighFaultActive()) {
                    lfValidationFailed = apAlByPoint.getLowFault() >= apAlByPoint.getHighFault();
                } else {
                    lfValidationFailed = false;
                }
            } else {
                lfValidationFailed = false;
            }

            // Validates the lowAlert field of the given Object
            if (apAlByPoint.isLowAlertActive()) {
                if (apAlByPoint.getLowAlert() <= 0) {
                    laValidationFailed = true;
                } else if (apAlByPoint.isLowFaultActive() && apAlByPoint.isHighAlertActive()) {
                    laValidationFailed = !(apAlByPoint.getLowAlert() > apAlByPoint.getLowFault() && apAlByPoint.getLowAlert() < apAlByPoint.getHighAlert());
                } else if (apAlByPoint.isLowFaultActive()) {
                    if (apAlByPoint.isLowFaultActive() && apAlByPoint.isHighFaultActive()) {
                        laValidationFailed = !(apAlByPoint.getLowAlert() > apAlByPoint.getLowFault() && apAlByPoint.getLowAlert() < apAlByPoint.getHighFault());
                    } else {
                        laValidationFailed = apAlByPoint.getLowAlert() <= apAlByPoint.getLowFault();
                    }
                } else if (apAlByPoint.isHighAlertActive()) {
                    laValidationFailed = apAlByPoint.getLowAlert() >= apAlByPoint.getHighAlert();
                } else if (apAlByPoint.isHighFaultActive()) {
                    laValidationFailed = apAlByPoint.getLowAlert() >= apAlByPoint.getHighFault();
                } else {
                    laValidationFailed = false;
                }
            } else {
                laValidationFailed = false;
            }

            // Validates the highAlert field of the given Object
            if (apAlByPoint.isHighAlertActive()) {
                if (apAlByPoint.getHighAlert() <= 0) {
                    haValidationFailed = true;
                } else if (apAlByPoint.isLowAlertActive() && apAlByPoint.isHighFaultActive()) {
                    haValidationFailed = !(apAlByPoint.getHighAlert() > apAlByPoint.getLowAlert() && apAlByPoint.getHighAlert() < apAlByPoint.getHighFault());
                } else if (apAlByPoint.isHighFaultActive()) {
                    if (apAlByPoint.isLowFaultActive() && apAlByPoint.isHighFaultActive()) {
                        haValidationFailed = !(apAlByPoint.getHighAlert() > apAlByPoint.getLowFault() && apAlByPoint.getHighAlert() < apAlByPoint.getHighFault());
                    } else {
                        haValidationFailed = apAlByPoint.getHighAlert() >= apAlByPoint.getHighFault();
                    }
                } else if (apAlByPoint.isLowAlertActive()) {
                    haValidationFailed = apAlByPoint.getHighAlert() <= apAlByPoint.getLowAlert();
                } else if (apAlByPoint.isLowFaultActive()) {
                    haValidationFailed = apAlByPoint.getHighAlert() <= apAlByPoint.getLowFault();
                } else {
                    haValidationFailed = false;
                }
            } else {
                haValidationFailed = false;
            }

            // Validates the highFault field of the given Object
            if (apAlByPoint.isHighFaultActive()) {
                if (apAlByPoint.getHighFault() <= 0) {
                    hfValidationFailed = true;
                } else if (apAlByPoint.isHighAlertActive()) {
                    hfValidationFailed = apAlByPoint.getHighFault() <= apAlByPoint.getHighAlert();
                } else if (apAlByPoint.isLowAlertActive()) {
                    hfValidationFailed = apAlByPoint.getHighFault() <= apAlByPoint.getLowAlert();
                } else if (apAlByPoint.isLowFaultActive()) {
                    hfValidationFailed = apAlByPoint.getHighFault() <= apAlByPoint.getLowFault();
                } else {
                    hfValidationFailed = false;
                }
            } else {
                hfValidationFailed = false;
            }

            apAlByPoint.setLowFaultFailure(lfValidationFailed);
            apAlByPoint.setLowAlertFailure(laValidationFailed);
            apAlByPoint.setHighAlertFailure(haValidationFailed);
            apAlByPoint.setHighFaultFailure(hfValidationFailed);

            renderErrorMessage();
        }
    }

    /**
     * Render Error message for limits, if there are issue with the values
     * 
     * Set renderAlertMsg to true, if limit failure occured 
     */
    public void renderErrorMessage() {
        renderAlertMsg = false;
        try {
            for (ApAlSetVO row : alSetList) {
                if (row.isLowFaultFailure()
                        || row.isLowAlertFailure()
                        || row.isHighAlertFailure()
                        || row.isHighFaultFailure()) {
                    renderAlertMsg = true;
                    break;
                }
            }
        } catch (Exception e) {
            Logger.getLogger(AddAlSetBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    public List<ApAlSetVO> getAlSetList() {
        return alSetList;
    }

    public List<SelectItem> getDwellList() {
        return dwellList;
    }

    public boolean isRenderAlertMsg() {
        return renderAlertMsg;
    }

    public ApAlSetVO getApAlSetVO() {
        return apAlSetVO;
    }

    public Boolean getAcSensor() {
        return acSensor;
    }
}
