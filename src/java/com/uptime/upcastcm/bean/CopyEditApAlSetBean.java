/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.APALSetClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import com.uptime.upcastcm.utils.helperclass.Filters;
import com.uptime.upcastcm.utils.interfaces.PresetDialogFields;
import com.uptime.upcastcm.utils.vo.ApAlSetVO;
import com.uptime.upcastcm.utils.vo.UserSitesVO;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;

/**
 *
 * @author twilcox
 */
public class CopyEditApAlSetBean implements Serializable, PresetDialogFields {
    private UserSitesVO selectedSite;
    private final List<UserSitesVO> userSiteList;
    private ApAlSetVO apAlSetVO;
    private final List<ApAlSetVO> apAlSetVOList;
    private int operationType;
    private UserManageBean userManageBean;

    /**
     * Constructor
     */
    public CopyEditApAlSetBean() {
        userSiteList = new ArrayList();
        apAlSetVOList = new ArrayList();
        
        try {
            if ((userManageBean = ((UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean"))) != null && userManageBean.getCurrentSite() != null) {
                userSiteList.add(userManageBean.getCurrentSite());
            }
        } catch (Exception e) {
            Logger.getLogger(CopyEditApAlSetBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            userManageBean = null;
            userSiteList.clear();
        }
    }

    /**
     * Resets the values of this overlay page
     */
    @Override
    public void resetPage() {
        apAlSetVOList.clear();
        operationType = -1;
        apAlSetVO = null;
        selectedSite = null;
    }

    /**
     * Presets fields based on the given info
     * @param presets, Filters Object
     * @param object, Object
     */
    @Override
    public void presetFields(Filters presets, Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Presets fields based on the given info
     * @param operationType, int
     * @param object, Object
     */
    @Override
    public void presetFields(int operationType, Object object) {
        if (userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null) {
            if (object != null && object instanceof ApAlSetVO) {
                apAlSetVO = (ApAlSetVO)object;
                if (apAlSetVO.getSiteId() == null) {
                    apAlSetVOList.addAll(APALSetClient.getInstance().getGlobalApAlSetsByCustomerApSetId(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), apAlSetVO.getApSetId()));
                } else {
                    selectedSite = new UserSitesVO();
                    selectedSite.setSiteId(apAlSetVO.getSiteId());
                    selectedSite.setSiteName(apAlSetVO.getSiteName());
                    apAlSetVOList.addAll(APALSetClient.getInstance().getSiteApAlSetsByCustomerSiteIdApSetId(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), apAlSetVO.getSiteId(), apAlSetVO.getApSetId()));
                }
                this.operationType = operationType;
            } else {
                resetPage();
            }
        }
    }

    /**
     * Presets fields based on the given info
     * @param object, Object
     */
    @Override
    public void presetFields(Object object) {
        if (object != null && object instanceof UserSitesVO) {
            selectedSite = (UserSitesVO)object;
            apAlSetVOList.stream().forEachOrdered(apAlSet -> {
                apAlSet.setSiteId(selectedSite.getSiteId());
                apAlSet.setSiteName(selectedSite.getSiteName());
            });
            apAlSetVO.setSiteId(selectedSite.getSiteId());
            apAlSetVO.setSiteName(selectedSite.getSiteName());
        }
    }
    
    /**
     * Submit the given info to Cassandra
     */
    public void submit() {
    }
}
