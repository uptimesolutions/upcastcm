/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.bean;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.APALSetClient;
import com.uptime.upcastcm.http.client.PointClient;
import com.uptime.upcastcm.http.client.PointLocationClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import static com.uptime.upcastcm.utils.enums.DwellEnum.getDwellItemList;
import static com.uptime.upcastcm.utils.enums.FmaxEnum.getFmaxItemList;
import static com.uptime.upcastcm.utils.enums.ResolutionEnum.getResolutionItemList;
import com.uptime.upcastcm.utils.helperclass.Filters;
import static com.uptime.upcastcm.utils.helperclass.GenericMessage.noChangesFound;
import com.uptime.upcastcm.utils.helperclass.Utils;
import com.uptime.upcastcm.utils.vo.PointVO;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import com.uptime.upcastcm.utils.interfaces.PresetDialogFields;
import com.uptime.upcastcm.utils.singletons.CommonUtilSingleton;
import com.uptime.upcastcm.utils.vo.ApAlSetVO;
import com.uptime.upcastcm.utils.vo.PointLocationVO;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import javax.faces.application.FacesMessage;
import javax.faces.model.SelectItem;
import org.primefaces.PrimeFaces;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author Mohd Juned Alam
 */
public class EditPointBean implements Serializable, PresetDialogFields {

    private UserManageBean userManageBean;

    private final List<ApAlSetVO> siteApAlSetsVOList, displayApAlsets, selectedSiteApAlSetsVOList, pointApAlsets;
    private final Map<UUID, List<ApAlSetVO>> customLimitVOMap, originalCustomLimitVOMap;
    private final List<SelectItem> apSetList, alSetList;
    private final List<PointVO> originalPointVOs;

    private List<SelectItem> fmaxList, dwellList, resolutionList;

    private boolean renderAlertMsg, addApalSetToPoint, tachValidationFailed;
    private ApAlSetVO selectedApAlSetVO, originalSelectedApAlSetVO;
    private UUID previusId;
    private PointVO pointVO;
    PointLocationVO selectedPointLocation;

    /**
     * Constructor
     */
    public EditPointBean() {
        pointApAlsets = new ArrayList();
        siteApAlSetsVOList = new ArrayList();
        selectedSiteApAlSetsVOList = new ArrayList();
        originalPointVOs = new ArrayList();
        displayApAlsets = new ArrayList();
        apSetList = new ArrayList();
        alSetList = new ArrayList();
        customLimitVOMap = new HashMap();
        originalCustomLimitVOMap = new HashMap();

        try {
            fmaxList = getFmaxItemList();
            dwellList = getDwellItemList();
            resolutionList = getResolutionItemList();
            userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean");
        } catch (Exception e) {
            Logger.getLogger(EditPointBean.class.getName()).log(Level.SEVERE, "EditPointBean Constructor Exception {0}", e.toString());
            userManageBean = null;
        }
    }

    /**
     * Post Constructor
     */
    @PostConstruct
    public void init() {
        resetPage();

        if (userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null) {
            Logger.getLogger(EditPointBean.class.getName()).log(Level.INFO, "**customerAccount to be set : {0}", userManageBean.getCurrentSite().getCustomerAccount());
        }
    }

    @Override
    public void resetPage() {
        displayApAlsets.clear();
        pointApAlsets.clear();
        siteApAlSetsVOList.clear();
        selectedSiteApAlSetsVOList.clear();
        apSetList.clear();
        originalPointVOs.clear();
        customLimitVOMap.clear();
        originalCustomLimitVOMap.clear();
        alSetList.clear();

        renderAlertMsg = false;
        tachValidationFailed = false;
        addApalSetToPoint = false;

        selectedApAlSetVO = null;
        originalSelectedApAlSetVO = null;
        previusId = null;
        selectedPointLocation = null;

        pointVO = new PointVO();
    }

    @Override
    public void presetFields(Filters presets, Object object) {
        List<SelectItem> selectItem;
        String sensorUnit;

        resetPage();
        try {
            if (presets != null && object != null && object instanceof PointVO) {
                pointVO = new PointVO((PointVO) object);
                userManageBean.getPresetUtil().clearGlobalApAlSetSensorList();
                userManageBean.getPresetUtil().clearSiteApAlSetSensorList();

                if (presets.getSite() != null) {
                    pointVO.setSiteName(presets.getSite().getLabel());
                    pointVO.setSiteId((UUID) presets.getSite().getValue());
                }

                if (presets.getArea() != null) {
                    pointVO.setAreaName(presets.getArea().getLabel());
                    pointVO.setAreaId((UUID) presets.getArea().getValue());
                }

                if (presets.getAsset() != null) {
                    pointVO.setAssetName(presets.getAsset().getLabel());
                    pointVO.setAssetId((UUID) presets.getAsset().getValue());
                }

                if (presets.getPointLocation() != null) {
                    pointVO.setPointLocationName(presets.getPointLocation().getLabel());
                    pointVO.setPointLocationId((UUID) presets.getPointLocation().getValue());
                }

                siteApAlSetsVOList.addAll(APALSetClient.getInstance().getGlobalApAlSetsAllByCustomerSensorType(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), pointVO.getSensorType()));
                if (!userManageBean.getCurrentSite().getCustomerAccount().equalsIgnoreCase(DEFAULT_UPTIME_ACCOUNT)) {
                    siteApAlSetsVOList.addAll(APALSetClient.getInstance().getGlobalApAlSetsAllByCustomerSensorType(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), DEFAULT_UPTIME_ACCOUNT, pointVO.getSensorType()));
                }
                siteApAlSetsVOList.addAll(APALSetClient.getInstance().getSiteApAlSetsByCustomerSiteIdSensorType(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), pointVO.getSiteId(), pointVO.getSensorType()));

                sensorUnit = CommonUtilSingleton.getInstance().getSensorUnitBySensorType(pointVO.getSensorType());
                PointClient.getInstance().getPointsByCustomerSiteAreaAssetPointLocationPointWithApsets(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), pointVO.getSiteId(), pointVO.getAreaId(), pointVO.getAssetId(), pointVO.getPointLocationId(), pointVO.getPointId()).forEach(point -> {
                    pointApAlsets.addAll(point.getApAlSetVOs());
                    customLimitVOMap.put(point.getApSetId(), point.getApAlSetVOs());

                    originalCustomLimitVOMap.put(point.getApSetId(), new ArrayList());
                    point.getApAlSetVOs().stream().forEachOrdered(vo -> originalCustomLimitVOMap.get(point.getApSetId()).add(new ApAlSetVO(vo)));

                    if (!point.isDisabled()) {
                        originalSelectedApAlSetVO = new ApAlSetVO(point.getApAlSetVOs().get(0));
                        selectedApAlSetVO = new ApAlSetVO(originalSelectedApAlSetVO);

                    }

                    point.getApAlSetVOs().forEach(tempAlSetVO -> {
                        List<ApAlSetVO> tempApALIdVOs;
                        PointVO tempPointVO;
                        ApAlSetVO tempApAlSetVO;

                        if (!tempAlSetVO.getApSetId().equals(previusId)) {
                            tempApAlSetVO = siteApAlSetsVOList.stream().filter(a -> a.getAlSetId().compareTo(tempAlSetVO.getAlSetId()) == 0).findFirst().get();

                            tempAlSetVO.setActive(!point.isDisabled());
                            tempAlSetVO.setApSetName(tempApAlSetVO.getApSetName());
                            tempAlSetVO.setAlSetName(tempApAlSetVO.getAlSetName());

                            tempApALIdVOs = new ArrayList();
                            tempApALIdVOs.add(tempAlSetVO);

                            tempPointVO = new PointVO(point);
                            tempPointVO.setDisabled(!pointVO.getApSetId().equals(tempAlSetVO.getApSetId()));
                            tempPointVO.setCustomized(tempAlSetVO.isCustomized());
                            tempPointVO.setApAlSetVOs(tempApALIdVOs);
                            tempPointVO.setSensorUnits(sensorUnit);
                            tempPointVO.setSensorSensitivity(pointVO.getSensorSensitivity());

                            originalPointVOs.add(tempPointVO);
                            previusId = tempAlSetVO.getApSetId();

                            if (!point.isDisabled()) {
                                pointVO.setCustomized(tempAlSetVO.isCustomized());
                                pointVO.setApSetId(tempApAlSetVO.getApSetId());
                                pointVO.setAlSetId(tempApAlSetVO.getAlSetId());
                            }
                        } else {
                            tempApAlSetVO = siteApAlSetsVOList.stream().filter(a -> a.getAlSetId().compareTo(tempAlSetVO.getAlSetId()) == 0).findFirst().get();
                            tempAlSetVO.setApSetName(tempApAlSetVO.getApSetName());
                            tempAlSetVO.setAlSetName(tempApAlSetVO.getAlSetName());

                            tempPointVO = originalPointVOs.get(originalPointVOs.size() - 1);
                            tempPointVO.getApAlSetVOs().add(tempAlSetVO);
                        }
                    });
                });

                displayApAlsets.addAll(pointApAlsets.stream().filter(CommonUtilSingleton.distinctByKey(p -> p.getApSetId())).collect(Collectors.toList()));

                if ((selectItem = userManageBean.getPresetUtil().populateApSetSensorTypeList(userManageBean, userManageBean.getCurrentSite().getCustomerAccount(), pointVO.getSiteId(), pointVO.getSensorType(), displayApAlsets)) != null && !selectItem.isEmpty()) {
                    apSetList.addAll(selectItem);
                }

                if (pointVO.isCustomized()) {
                    onCustomLimitsAlSetNameSelect();
                }

                pointVO.setApSetId(null);
                pointVO.setAlSetId(null);
            }
        } catch (Exception e) {
            Logger.getLogger(EditPointBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            resetPage();
        }
    }

    @Override
    public void presetFields(int operationType, Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void presetFields(Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void updateBean() {
        isEditPoint();
        Logger.getLogger(EditPointBean.class.getName()).log(Level.INFO, "***********updateBean***********");
    }

    /**
     * Update the point
     *
     * @param updateTree, boolean
     */
    public void updatePoint(boolean updateTree) {
        List<PointVO> updateList;
        PointVO enabledPointVO;

        try {
            if (isEditPoint()) {
                if (pointApAlsets.size() == 1) {
                    selectedApAlSetVO = pointApAlsets.get(0);
                }

                originalPointVOs.stream().forEach(action -> action.setDisabled(true));
                if ((originalPointVOs.stream().filter(point -> point.getApSetId().compareTo(selectedApAlSetVO.getApSetId()) == 0).findAny().orElse(null)) == null) {
                    enabledPointVO = new PointVO(pointVO);
                    enabledPointVO.setApSetId(selectedApAlSetVO.getApSetId());
                    enabledPointVO.setAlSetId(selectedApAlSetVO.getAlSetId());
                    originalPointVOs.add(enabledPointVO);
                }

                pointApAlsets.forEach(action -> action.setCustomized(false));

                //customized list to be set to the pointApAlsets from customLimitVOMap
                pointApAlsets.stream().filter(ap -> (customLimitVOMap.containsKey(ap.getApSetId()))).forEachOrdered(ap -> {
                    customLimitVOMap.get(ap.getApSetId()).stream().filter(apAl -> (apAl.getAlSetId().compareTo(ap.getAlSetId()) == 0 && apAl.getParamName().equals(ap.getParamName()))).forEachOrdered(apAl -> {
                        int index;

                        if ((index = pointApAlsets.indexOf(ap)) >= 0) {
                            pointApAlsets.set(index, apAl);
                        }
                    });
                });

                updateList = new ArrayList();
                displayApAlsets.forEach(displayVo -> {
                    PointVO pointvo;
                    List<ApAlSetVO> apAlSetVOs;

                    //find the ApAlSEtVOs for this particular Point.
                    apAlSetVOs = pointApAlsets.stream().filter(apset -> apset.getApSetId().compareTo(displayVo.getApSetId()) == 0).collect(Collectors.toList());

                    pointvo = originalPointVOs.stream().filter(point -> point.getApSetId().compareTo(displayVo.getApSetId()) == 0).findAny().orElse(new PointVO(pointVO));
                    pointvo.setPointName(pointVO.getPointName());
                    pointvo.setAlarmEnabled(pointVO.isAlarmEnabled());
                    pointvo.setApSetId(displayVo.getApSetId());
                    pointvo.setAlSetId(displayVo.getAlSetId());
                    pointvo.setApAlSetVOs(apAlSetVOs);
                    pointvo.setDisabled(selectedApAlSetVO.getApSetId().compareTo(displayVo.getApSetId()) != 0);
                    pointvo.setCustomized(!apAlSetVOs.isEmpty() ? apAlSetVOs.get(0).isCustomized() : false);
                    pointvo.setApSetId(displayVo.getApSetId());
                    pointvo.setApSetName(displayVo.getApSetName());
                    pointvo.setAlSetId(displayVo.getAlSetId());
                    pointvo.setAlSetName(displayVo.getAlSetName());
                    pointvo.setSampleRate(displayVo.getSampleRate());
                    updateList.add(pointvo);
                });

                if (PointClient.getInstance().updatePoint(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), updateList)) {
                    if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("configurationBean") == null) {
                        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("configurationBean", new ConfigurationBean());
                        ((ConfigurationBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("configurationBean")).init();
                    }

                    if (updateTree) {
                        Logger.getLogger(EditPointBean.class.getName()).log(Level.INFO, "Update Point {0}", new Object[]{pointVO.toString()});
                        pointVO = new PointVO();
                        ((ConfigurationBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("configurationBean")).onPointLocationChange(true);
                        PrimeFaces.current().executeScript("PF('defaultDlg').hide()");
                    } else {
                        ((ConfigurationBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("configurationBean")).onPointLocationChange(false);
                        ((ConfigurationBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("configurationBean")).getSelectedFilters().setArea(new SelectItem(pointVO.getAreaId(), pointVO.getAreaName()));
                        ((ConfigurationBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("configurationBean")).getSelectedFilters().setAsset(new SelectItem(pointVO.getAssetId(), pointVO.getAssetName()));
                        ((ConfigurationBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("configurationBean")).getSelectedFilters().setPointLocation(new SelectItem(pointVO.getPointLocationId(), pointVO.getPointLocationName()));
                        ((ConfigurationBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("configurationBean")).getSelectedFilters().setPoint(new SelectItem(pointVO.getPointId(), pointVO.getPointName()));
                        ((ConfigurationBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("configurationBean")).onPointChange(true);
                    }

                    PrimeFaces.current().executeScript("updateTreeNodes()");
                } else {
                    Logger.getLogger(EditPointBean.class.getName()).log(Level.INFO, "pointVO*********** {0}", new Object[]{pointVO.toString()});
                }
            } else {
                noChangesFound();
            }
        } catch (Exception e) {
            Logger.getLogger(EditPointBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Add a new apAlSet to the point
     */
    public void addApAlset() {
        List<ApAlSetVO> tempSelectedSiteApAlSetsVOList;
        List<SelectItem> selectItem;

        try {
            if (siteApAlSetsVOList != null && !siteApAlSetsVOList.isEmpty() && pointVO != null && pointVO.getApSetId() != null) {
                if (!(tempSelectedSiteApAlSetsVOList = siteApAlSetsVOList.stream().filter(a -> a.getApSetId().compareTo(pointVO.getApSetId()) == 0 && a.getAlSetId().compareTo(pointVO.getAlSetId()) == 0).collect(Collectors.toList())).isEmpty()) {
                    customLimitVOMap.put(pointVO.getApSetId(), new ArrayList());
                    tempSelectedSiteApAlSetsVOList.forEach(apAlSetVo -> {
                        CommonUtilSingleton.getInstance().onFmaxChange(apAlSetVo);
                        pointApAlsets.add(new ApAlSetVO(apAlSetVo));
                        customLimitVOMap.get(pointVO.getApSetId()).add(new ApAlSetVO(apAlSetVo));
                    });
                    displayApAlsets.addAll(tempSelectedSiteApAlSetsVOList.stream().filter(CommonUtilSingleton.distinctByKey(p -> p.getApSetId())).collect(Collectors.toList()));
                    pointVO.setApSetId(null);
                    pointVO.setAlSetId(null);

                    // Clear and reset apSetList field
                    apSetList.clear();
                    userManageBean.getPresetUtil().clearGlobalApAlSetSensorList();
                    userManageBean.getPresetUtil().clearSiteApAlSetSensorList();
                    if ((selectItem = userManageBean.getPresetUtil().populateApSetSensorTypeList(userManageBean, userManageBean.getCurrentSite().getCustomerAccount(), pointVO.getSiteId(), pointVO.getSensorType(), displayApAlsets)) != null && !selectItem.isEmpty()) {
                        apSetList.addAll(selectItem);
                    }
                }
            }

            addApalSetToPoint = false;
        } catch (Exception e) {
            Logger.getLogger(EditPointBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Remove the given apAlSet from the point
     *
     * @param apAlSetVO, ApAlSetVO Object
     */
    public void removeApAlset(ApAlSetVO apAlSetVO) {
        List<SelectItem> selectItem;

        try {
            userManageBean.getPresetUtil().clearGlobalApAlSetSensorList();
            userManageBean.getPresetUtil().clearSiteApAlSetSensorList();
            apSetList.clear();
            pointVO.setApSetId(null);
            pointVO.setAlSetId(null);
            pointApAlsets.removeIf(alset -> alset.getApSetId().compareTo(apAlSetVO.getApSetId()) == 0);
            displayApAlsets.removeIf(alset -> alset.getApSetId().compareTo(apAlSetVO.getApSetId()) == 0);

            // Remove limits from the customLimitVOMap field
            if (customLimitVOMap.containsKey(apAlSetVO.getApSetId())) {
                customLimitVOMap.remove(apAlSetVO.getApSetId());
            }

            //check if selected Object which is selectedApAlSetVO to be removed.
            if (apAlSetVO.getApSetId().compareTo(selectedApAlSetVO.getApSetId()) == 0) {
                renderAlertMsg = false;
                selectedApAlSetVO = displayApAlsets.get(0);
                pointVO.setCustomized(selectedApAlSetVO.isCustomized());
                onCustomLimitsAlSetNameSelect();
            }

            // set apSetList list
            if ((selectItem = userManageBean.getPresetUtil().populateApSetSensorTypeList(userManageBean, userManageBean.getCurrentSite().getCustomerAccount(), pointVO.getSiteId(), pointVO.getSensorType(), displayApAlsets)) != null && !selectItem.isEmpty()) {
                apSetList.addAll(selectItem);
            }

        } catch (Exception e) {
            Logger.getLogger(EditPointBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Action called when a new apAlSet is selected to be active
     *
     * @param event, SelectEvent Object
     */
    public void activateApAlSetVO(SelectEvent<ApAlSetVO> event) {
        List<ApAlSetVO> selectedApAlSetVOs;
        try {
            selectedApAlSetVO = event.getObject();

            //find the selected apAlSetVOs list
            if ((selectedApAlSetVOs = pointApAlsets.stream().filter(apAlSet -> apAlSet.getApSetId().compareTo(selectedApAlSetVO.getApSetId()) == 0).collect(Collectors.toList())) != null && !selectedApAlSetVOs.isEmpty()) {
                pointVO.setCustomized(selectedApAlSetVOs.get(0).isCustomized());
            }
            onCustomLimitsAlSetNameSelect();
        } catch (Exception e) {
            Logger.getLogger(EditPointBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Action taken when the ApSet name is selected
     */
    public void onApSetNameSelect() {
        List<SelectItem> selectItem;

        try {
            siteApAlSetsVOList.clear();
            alSetList.clear();
            addApalSetToPoint = false;
            tachValidationFailed = false;
            pointVO.setAlSetId(null);

            if (userManageBean != null && userManageBean.getCurrentSite() != null && userManageBean.getCurrentSite().getCustomerAccount() != null) {
                siteApAlSetsVOList.addAll(APALSetClient.getInstance().getSiteApAlSetsByCustomerSiteIdApSetId(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), pointVO.getSiteId(), pointVO.getApSetId()));
                if (siteApAlSetsVOList == null || siteApAlSetsVOList.isEmpty()) {
                    siteApAlSetsVOList.addAll(APALSetClient.getInstance().getGlobalApAlSetsByCustomerApSetId(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), pointVO.getApSetId()));
                    if (siteApAlSetsVOList == null || siteApAlSetsVOList.isEmpty()) {
                        siteApAlSetsVOList.addAll(APALSetClient.getInstance().getGlobalApAlSetsByCustomerApSetId(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), DEFAULT_UPTIME_ACCOUNT, pointVO.getApSetId()));
                    }
                }

                if (siteApAlSetsVOList == null || siteApAlSetsVOList.isEmpty()) {
                    FacesContext.getCurrentInstance().addMessage("growlMessages", new FacesMessage(userManageBean.getResourceBundleString("growl_create_point_3")));
                } else {

                    // Validate tach
                    siteApAlSetsVOList.stream().forEach(al -> {
                        if ((al.getFrequencyUnits() != null && al.getFrequencyUnits().equalsIgnoreCase("Orders")) || Utils.validateParamList().contains(al.getParamType())) {
                            if (selectedPointLocation == null) {
                                selectedPointLocation = PointLocationClient.getInstance().getPointLocationsByPK(ServiceRegistrarClient.getInstance().getServiceHostURL(CONFIGURATION_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), pointVO.getSiteId(), pointVO.getAreaId(), pointVO.getAssetId(), pointVO.getPointLocationId()).get(0);
                            }
                            if (selectedPointLocation.getTachId() == null) {
                                tachValidationFailed = true;
                            }
                        }
                    });

                    //Set Al set list
                    if (!tachValidationFailed && (selectItem = userManageBean.getPresetUtil().populateAlSetByApAlsetList(siteApAlSetsVOList)) != null && !selectItem.isEmpty()) {
                        alSetList.addAll(selectItem);
                    }
                }
            }
        } catch (Exception e) {
            Logger.getLogger(EditPointBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            alSetList.clear();
            siteApAlSetsVOList.clear();
            addApalSetToPoint = false;
            tachValidationFailed = false;
            if (pointVO != null) {
                pointVO.setAlSetId(null);
            }
        }
    }

    /**
     * Action taken when the AlSet name is selected
     */
    public void onAlSetNameSelect() {
        try {
            addApalSetToPoint = false;
            if (siteApAlSetsVOList != null && !siteApAlSetsVOList.isEmpty() && pointVO != null && pointVO.getApSetId() != null) {
                if (pointApAlsets.isEmpty() || pointApAlsets.stream().filter(p -> pointVO.getApSetId().equals(p.getApSetId())).findAny().orElse(null) == null) {
                    addApalSetToPoint = true;
                }
            }
        } catch (Exception e) {
            Logger.getLogger(EditPointBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    public void onCustomLimitsAlSetNameSelect() {
        List<ApAlSetVO> customApAlSets, tempSelectedSiteApAlSetsVOList, activeApAlSets, presetApAlsets, apAlSetVOList;

        renderAlertMsg = false;
        try {
            if (selectedApAlSetVO != null && (customApAlSets = customLimitVOMap.get(selectedApAlSetVO.getApSetId())) != null && !customApAlSets.isEmpty() && pointVO != null) {
                selectedSiteApAlSetsVOList.clear();
                if ((activeApAlSets = pointApAlsets.stream().filter(p -> p.getApSetId().compareTo(selectedApAlSetVO.getApSetId()) == 0).collect(Collectors.toList())) != null && !activeApAlSets.isEmpty()) {

                    //set the customized value for the selected/activeApAlSets 
                    activeApAlSets.forEach(ap -> ap.setCustomized(pointVO.isCustomized()));

                    if (pointVO.isCustomized()) {
                        if (!(tempSelectedSiteApAlSetsVOList = customApAlSets.stream().filter(a -> a.getAlSetId().compareTo(activeApAlSets.get(0).getAlSetId()) == 0).collect(Collectors.toList())).isEmpty()) {
                            tempSelectedSiteApAlSetsVOList.forEach(apAlSetVo -> {
                                apAlSetVo.setCustomized(true);
                                selectedSiteApAlSetsVOList.add(new ApAlSetVO(apAlSetVo));
                            });
                        }
                        customApAlSets.clear();
                        customApAlSets.addAll(selectedSiteApAlSetsVOList);
                    } else {
                        customApAlSets.clear();

                        if ((apAlSetVOList = APALSetClient.getInstance().getSiteApAlSetsByCustomerSiteIdApSetId(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), userManageBean.getCurrentSite().getSiteId(), selectedApAlSetVO.getApSetId())) == null || apAlSetVOList.isEmpty()) {
                            if ((apAlSetVOList = APALSetClient.getInstance().getGlobalApAlSetsByCustomerApSetId(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), userManageBean.getCurrentSite().getCustomerAccount(), selectedApAlSetVO.getApSetId())) == null || apAlSetVOList.isEmpty()) {
                                apAlSetVOList = APALSetClient.getInstance().getGlobalApAlSetsByCustomerApSetId(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), DEFAULT_UPTIME_ACCOUNT, selectedApAlSetVO.getApSetId());
                            }
                        }

                        if (!apAlSetVOList.isEmpty()) {
                            presetApAlsets = new ArrayList();

                            if (!(tempSelectedSiteApAlSetsVOList = apAlSetVOList.stream().filter(a -> a.getApSetId().compareTo(selectedApAlSetVO.getApSetId()) == 0 && a.getAlSetId().compareTo(selectedApAlSetVO.getAlSetId()) == 0).collect(Collectors.toList())).isEmpty()) {
                                tempSelectedSiteApAlSetsVOList.forEach(apAlSetVo -> {
                                    if (apAlSetVo.getParamName() != null) {
                                        CommonUtilSingleton.getInstance().onFmaxChange(apAlSetVo);
                                        presetApAlsets.add(new ApAlSetVO(apAlSetVo));
                                    }
                                });
                            }

                            if (!presetApAlsets.isEmpty()) {
                                pointApAlsets.removeIf(alset -> alset.getApSetId().compareTo(selectedApAlSetVO.getApSetId()) == 0);
                                pointApAlsets.addAll(presetApAlsets);
                                customApAlSets.addAll(presetApAlsets);
                            }
                        }

                    }
                }

            }
        } catch (Exception e) {
            Logger.getLogger(EditPointBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Validate Alarm Limits
     *
     * @param apAlByPoint, ApAlSetVO object
     * @param rowId, int
     */
    public void alarmLimitValidation(ApAlSetVO apAlByPoint, int rowId) {
        boolean lfValidationFailed, laValidationFailed, haValidationFailed, hfValidationFailed;

        // Validate Alarm Limits
        if (apAlByPoint != null) {

            // Validates the lowFault field of the given Object
            if (apAlByPoint.isLowFaultActive()) {
                if (apAlByPoint.getLowFault() <= 0) {
                    lfValidationFailed = true;
                } else if (apAlByPoint.isLowAlertActive()) {
                    lfValidationFailed = apAlByPoint.getLowFault() >= apAlByPoint.getLowAlert();
                } else if (apAlByPoint.isHighAlertActive()) {
                    lfValidationFailed = apAlByPoint.getLowFault() >= apAlByPoint.getHighAlert();
                } else if (apAlByPoint.isHighFaultActive()) {
                    lfValidationFailed = apAlByPoint.getLowFault() >= apAlByPoint.getHighFault();
                } else {
                    lfValidationFailed = false;
                }
            } else {
                lfValidationFailed = false;
            }

            // Validates the lowAlert field of the given Object
            if (apAlByPoint.isLowAlertActive()) {
                if (apAlByPoint.getLowAlert() <= 0) {
                    laValidationFailed = true;
                } else if (apAlByPoint.isLowFaultActive() && apAlByPoint.isHighAlertActive()) {
                    laValidationFailed = !(apAlByPoint.getLowAlert() > apAlByPoint.getLowFault() && apAlByPoint.getLowAlert() < apAlByPoint.getHighAlert());
                } else if (apAlByPoint.isLowFaultActive()) {
                    if (apAlByPoint.isLowFaultActive() && apAlByPoint.isHighFaultActive()) {
                        laValidationFailed = !(apAlByPoint.getLowAlert() > apAlByPoint.getLowFault() && apAlByPoint.getLowAlert() < apAlByPoint.getHighFault());
                    } else {
                        laValidationFailed = apAlByPoint.getLowAlert() <= apAlByPoint.getLowFault();
                    }
                } else if (apAlByPoint.isHighAlertActive()) {
                    laValidationFailed = apAlByPoint.getLowAlert() >= apAlByPoint.getHighAlert();
                } else if (apAlByPoint.isHighFaultActive()) {
                    laValidationFailed = apAlByPoint.getLowAlert() >= apAlByPoint.getHighFault();
                } else {
                    laValidationFailed = false;
                }
            } else {
                laValidationFailed = false;
            }

            // Validates the highAlert field of the given Object
            if (apAlByPoint.isHighAlertActive()) {
                if (apAlByPoint.getHighAlert() <= 0) {
                    haValidationFailed = true;
                } else if (apAlByPoint.isLowAlertActive() && apAlByPoint.isHighFaultActive()) {
                    haValidationFailed = !(apAlByPoint.getHighAlert() > apAlByPoint.getLowAlert() && apAlByPoint.getHighAlert() < apAlByPoint.getHighFault());
                } else if (apAlByPoint.isHighFaultActive()) {
                    if (apAlByPoint.isLowFaultActive() && apAlByPoint.isHighFaultActive()) {
                        haValidationFailed = !(apAlByPoint.getHighAlert() > apAlByPoint.getLowFault() && apAlByPoint.getHighAlert() < apAlByPoint.getHighFault());
                    } else {
                        haValidationFailed = apAlByPoint.getHighAlert() >= apAlByPoint.getHighFault();
                    }
                } else if (apAlByPoint.isLowAlertActive()) {
                    haValidationFailed = apAlByPoint.getHighAlert() <= apAlByPoint.getLowAlert();
                } else if (apAlByPoint.isLowFaultActive()) {
                    haValidationFailed = apAlByPoint.getHighAlert() <= apAlByPoint.getLowFault();
                } else {
                    haValidationFailed = false;
                }
            } else {
                haValidationFailed = false;
            }

            // Validates the highFault field of the given Object
            if (apAlByPoint.isHighFaultActive()) {
                if (apAlByPoint.getHighFault() <= 0) {
                    hfValidationFailed = true;
                } else if (apAlByPoint.isHighAlertActive()) {
                    hfValidationFailed = apAlByPoint.getHighFault() <= apAlByPoint.getHighAlert();
                } else if (apAlByPoint.isLowAlertActive()) {
                    hfValidationFailed = apAlByPoint.getHighFault() <= apAlByPoint.getLowAlert();
                } else if (apAlByPoint.isLowFaultActive()) {
                    hfValidationFailed = apAlByPoint.getHighFault() <= apAlByPoint.getLowFault();
                } else {
                    hfValidationFailed = false;
                }
            } else {
                hfValidationFailed = false;
            }

            apAlByPoint.setLowFaultFailure(lfValidationFailed);
            apAlByPoint.setLowAlertFailure(laValidationFailed);
            apAlByPoint.setHighAlertFailure(haValidationFailed);
            apAlByPoint.setHighFaultFailure(hfValidationFailed);

            renderErrorMessage();

        }
    }

    public void renderErrorMessage() {
        renderAlertMsg = false;

        try {
            for (ApAlSetVO row : selectedSiteApAlSetsVOList) {
                if (row.isLowFaultFailure()
                        || row.isLowAlertFailure()
                        || row.isHighAlertFailure()
                        || row.isHighFaultFailure()) {
                    renderAlertMsg = true;
                    break;
                }
            }
        } catch (Exception e) {
            Logger.getLogger(EditPointBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    public boolean isEditPoint() {
        List<ApAlSetVO> values, originalValues;

        if (!pointVO.getPointName().equals(originalPointVOs.get(0).getPointName())) {
            return true;
        }
        if (pointVO.isAlarmEnabled() != originalPointVOs.get(0).isAlarmEnabled()) {
            return true;
        }
        if (selectedApAlSetVO != null && originalSelectedApAlSetVO != null) {
            if (!selectedApAlSetVO.getApSetId().equals(originalSelectedApAlSetVO.getApSetId()) || !selectedApAlSetVO.getAlSetId().equals(originalSelectedApAlSetVO.getAlSetId())) {
                return true;
            }
        }
       

        // Check if the custom limits have been changed
        if (customLimitVOMap.size() != originalCustomLimitVOMap.size()) {
            return true;
        }
        if (!customLimitVOMap.keySet().stream().allMatch(key -> originalCustomLimitVOMap.containsKey(key))) {
            return true;
        }
        for (UUID key : customLimitVOMap.keySet()) {
            values = customLimitVOMap.get(key);
            originalValues = originalCustomLimitVOMap.get(key);

            if (values.size() != originalValues.size()) {
                return true;
            }

            for (ApAlSetVO limits : values) {
                for (ApAlSetVO originalLimits : originalValues) {
                    if (limits.getParamName().equalsIgnoreCase(originalLimits.getParamName())) {
                        if (limits.getHighFault() != originalLimits.getHighFault()) {
                            return true;
                        }
                        if (limits.getHighAlert() != originalLimits.getHighAlert()) {
                            return true;
                        }
                        if (limits.getLowAlert() != originalLimits.getLowAlert()) {
                            return true;
                        }
                        if (limits.getLowFault() != originalLimits.getLowFault()) {
                            return true;
                        }
                        if (limits.getDwell() != originalLimits.getDwell()) {
                            return true;
                        }
                        if (limits.isHighFaultActive() != originalLimits.isHighFaultActive()) {
                            return true;
                        }
                        if (limits.isHighAlertActive() != originalLimits.isHighAlertActive()) {
                            return true;
                        }
                        if (limits.isLowAlertActive() != originalLimits.isLowAlertActive()) {
                            return true;
                        }
                        if (limits.isLowFaultActive() != originalLimits.isLowFaultActive()) {
                            return true;
                        }
                        if (limits.isCustomized() != originalLimits.isCustomized()) {
                            return true;
                        }
                        break;
                    }
                }
            }
        }

        return false;
    }

    public PointVO getPointVO() {
        return pointVO;
    }

    public List<ApAlSetVO> getSelectedSiteApAlSetsVOList() {
        return selectedSiteApAlSetsVOList;
    }

    public List<SelectItem> getFmaxList() {
        return fmaxList;
    }

    public List<SelectItem> getDwellList() {
        return dwellList;
    }

    public List<SelectItem> getResolutionList() {
        return resolutionList;
    }

    public boolean isRenderAlertMsg() {
        return renderAlertMsg;
    }

    public void setRenderAlertMsg(boolean renderAlertMsg) {
        this.renderAlertMsg = renderAlertMsg;
    }

    public boolean isAddApalSetToPoint() {
        return addApalSetToPoint;
    }

    public void setAddApalSetToPoint(boolean addApalSetToPoint) {
        this.addApalSetToPoint = addApalSetToPoint;
    }

    public List<ApAlSetVO> getDisplayApAlsets() {
        return displayApAlsets;
    }

    public List<SelectItem> getApSetList() {
        return apSetList;
    }

    public List<SelectItem> getAlSetList() {
        return alSetList;
    }

    public ApAlSetVO getSelectedApAlSetVO() {
        return selectedApAlSetVO;
    }

    public void setSelectedApAlSetVO(ApAlSetVO selectedApAlSetVO) {
        this.selectedApAlSetVO = selectedApAlSetVO;
    }

    public boolean isTachValidationFailed() {
        return tachValidationFailed;
    }

    public void setTachValidationFailed(boolean tachValidationFailed) {
        this.tachValidationFailed = tachValidationFailed;
    }

}
