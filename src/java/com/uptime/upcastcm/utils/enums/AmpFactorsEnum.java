/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.utils.enums;

import com.uptime.upcastcm.bean.UserManageBean;
import java.util.ArrayList;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

/**
 *
 * @author twilcox
 */
public class AmpFactorsEnum {
    
    /**
     * The Analysis Parameter Amp Factors
     */
    private enum AmpFactor {
        PEAK("Peak", "label_ampFactors_peak"),
        PEAK_TO_PEAK("Peak-to-Peak", "label_ampFactors_PeakToPeak"),
        RMS("RMS", "label_ampFactors_rms");

        private final String value;
        private final String langVariable;

        private AmpFactor(String value, String langVariable) {
            this.value = value;
            this.langVariable = langVariable;
        }

        public String getValue() {
            return value;
        }

        public String getLangVariable() {
            return langVariable;
        }
        
        static List<AmpFactor> findByParamType(String paramType) {
            List<AmpFactor> list = new ArrayList();
            
            if(paramType != null) {
                switch(paramType.toLowerCase()) {
                    case"waveform true peak":
                        list.add(PEAK);
                        break;
                    case"waveform peak-to-peak":
                        list.add(PEAK_TO_PEAK);
                        break;
                    case"waveform rms":
                        list.add(RMS);
                        break;
                    case"total spectral energy":
                    case"total non sync energy":
                    case"total sub sync energy":
                    case"demodulation":
                    case"spectral band total amplitude":
                    case"spectral band peak amplitude":
                    case"peak amplitude":
                        list.add(PEAK);
                        list.add(PEAK_TO_PEAK);
                        list.add(RMS);
                        break;
                    case"waveform crest factor":
                    case"tach speed":
                    default:
                        break;
                }
            }
            
            return list;
        }
    }
    
    /**
     * Return a List of SelectItem from the AmpFactor enum
     * @return List Object of SelectItem Objects
     */
    public static List<SelectItem> getAmpFactorItemList() {
        List<SelectItem> list = new ArrayList();
        UserManageBean userManageBean;
        
        if ((userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean")) != null) { 
            for (AmpFactor item : AmpFactor.values()) {
                list.add(new SelectItem(item.getValue(), userManageBean.getResourceBundleString(item.getLangVariable())));
            }
        }
        return list;
    }
    
    /**
     * Return a List of Strings of just the value from the AmpFactor enum
     * @return List Object of String Objects 
     */
    public static List<String> getAmpFactorValueList() {
        List<String> list = new ArrayList();
        
        for (AmpFactor item : AmpFactor.values()) {
            list.add(item.getValue());
        }
        return list;
    }
    
    /**
     * Return a List of Strings of just the labels based on the langVariable from the AmpFactor enum
     * @return List Object of String Objects 
     */
    public static List<String> getAmpFactorLabelList() {
        List<String> list = new ArrayList();
        UserManageBean userManageBean;
        
        if ((userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean")) != null) { 
            for (AmpFactor item : AmpFactor.values()) {
                list.add(userManageBean.getResourceBundleString(item.getLangVariable()));
            }
        }
        return list;
    }
    
    /**
     * Return the label based on the given value and langVariable from the AmpFactor enum 
     * @param value, String Object
     * @return String Object
     */
    public static String getAmpFactorLabelByValue(String value) {
        UserManageBean userManageBean;
        
        if (value != null && (userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean")) != null) {
            for (AmpFactor item : AmpFactor.values()) {
                if (item.getValue().equals(value)) {
                    return userManageBean.getResourceBundleString(item.getLangVariable());
                }
            }
        }
        return null;
    }
    
    
     public static String getAmpFactorValueByLabel(String value) {
        UserManageBean userManageBean;
        
        if (value != null && (userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean")) != null) {
            for (AmpFactor item : AmpFactor.values()) {
                if (userManageBean.getResourceBundleString(item.getLangVariable()).equals(value)) {
                    return item.getValue();
                }
            }
        }
        return null;
    }
    
    /**
     * Return a List of SelectItem based on the given langVariable from the AmpFactor enum and paramType
     * @param paramType, String Object
     * @return List Object of SelectItem Objects
     */
    public static List<SelectItem> getAmpFactorItemListByParamType(String paramType) {
        List<SelectItem> list = new ArrayList();
        UserManageBean userManageBean;
        
        if(paramType != null && (userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean")) != null) {
            AmpFactor.findByParamType(paramType).forEach(item -> list.add(new SelectItem(item.getValue(), userManageBean.getResourceBundleString(item.getLangVariable()))));
        }
        
        return list;
    }
}
