/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.utils.enums;

import com.uptime.upcastcm.bean.UserManageBean;
import java.util.ArrayList;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

/**
 *
 * @author twilcox
 */
public class DCSensorTypeEnum {

    private enum DCSensorType {
        TEMPERATURE("Temperature", "label_sensorType_temperature"),
        //FREQUENCY("Frequency", "label_sensorType_frequency"),
        //PRESSURE("Pressure", "label_sensorType_pressure"),
        VOLTAGE("Voltage", "label_sensorType_voltage"),
        //CURRENT("Current", "label_sensorType_current"),
        //PHASE("Phase", "label_sensorType_phase"),
        
        // USER_DEFINED is being hidden for version 1.0 release and until further notice
        //USER_DEFINED("User-Defined", "label_sensorType_user_defined"),
        
        //COUNT("Count", "label_sensorType_count"),
        RELATIVE_HUMIDITY("Relative Humidity", "label_sensorType_relative_humidity"),
        TEMPERATURE_RTD("Temperature RTD", "label_sensorType_temperature_rtd");

        private final String value;
        private final String langVariable;

        private DCSensorType(String value, String langVariable) {
            this.value = value;
            this.langVariable = langVariable;
        }

        public String getValue() {
            return value;
        }

        public String getLangVariable() {
            return langVariable;
        }
    }
    
    /**
     * Return a List of SelectItem from the DCSensorType enum
     * @return List Object of SelectItem Objects
     */
    public static List<SelectItem> getDCSensorTypeItemList() {
        List<SelectItem> list = new ArrayList();
        UserManageBean userManageBean;
        
        if ((userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean")) != null) { 
            for (DCSensorType item : DCSensorType.values()) {
                list.add(new SelectItem(item.getValue(), userManageBean.getResourceBundleString(item.getLangVariable())));
            }
        }
        return list;
    }
    
    /**
     * Return a List of Strings of just the value from the DCSensorType enum
     * @return List Object of String Objects 
     */
    public static List<String> getDCSensorTypeValueList() {
        List<String> list = new ArrayList();
        
        for (DCSensorType item : DCSensorType.values()) {
            list.add(item.getValue());
        }
        return list;
    }
    
    /**
     * Return a List of Strings of just the labels based on the langVariable from the DCSensorType enum
     * @return List Object of String Objects 
     */
    public static List<String> getDCSensorTypeLabelList() {
        List<String> list = new ArrayList();
        UserManageBean userManageBean;
        
        if ((userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean")) != null) { 
            for (DCSensorType item : DCSensorType.values()) {
                list.add(userManageBean.getResourceBundleString(item.getLangVariable()));
            }
        }
        return list;
    }
    
    /**
     * Return the label based on the given value and langVariable from the DCSensorType enum 
     * @param value, String Object
     * @return String Object
     */
    public static String getDCLabelByValue(String value) {
        UserManageBean userManageBean;
        
        if (value != null && (userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean")) != null) {
            for (DCSensorType item : DCSensorType.values()) {
                if (item.getValue().equals(value)) {
                    return userManageBean.getResourceBundleString(item.getLangVariable());
                }
            }
        }
        return null;
    }
}
