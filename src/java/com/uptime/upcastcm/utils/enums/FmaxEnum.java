/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.utils.enums;

import java.util.ArrayList;
import java.util.List;
import javax.faces.model.SelectItem;

/**
 *
 * @author twilcox
 */
public class FmaxEnum {
    
    private enum Fmax {
        FMAX_61(61, "61 Hz"),
        FMAX_122(122, "122 Hz"),
        FMAX_244(244, "244 Hz"),
        FMAX_488(488, "488 Hz"),
        FMAX_977(977, "977 Hz"),
        FMAX_1953(1953, "1953 Hz"),
        FMAX_3906(3906, "3906 Hz"),
        FMAX_7813(7813, "7813 Hz");

        private final int value;
        private final String label;

        private Fmax(int value, String label) {
            this.value = value;
            this.label = label;
        }

        public int getValue() {
            return value;
        }

        public String getLabel() {
            return label;
        }
    }
    
    /**
     * Return a List of SelectItem from the Fmax enum
     * @return List Object of SelectItem Objects
     */
    public static List<SelectItem> getFmaxItemList() {
        List<SelectItem> list = new ArrayList();
        
        for (Fmax item : Fmax.values()) {
            list.add(new SelectItem(item.getValue(), item.getLabel()));
        }
            
        return list;
    }
}
