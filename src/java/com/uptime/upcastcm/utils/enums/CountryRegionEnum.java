/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.utils.enums;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author twilcox
 */
public class CountryRegionEnum {
    
    private enum CountryRegion {
        ALGERIA("Algeria", "Africa"), ANGOLA("Angola", "Africa"), BENIN("Benin", "Africa"), BOTSWANA("Botswana", "Africa"),
        BURKINA_FASO("Burkina Faso", "Africa"), BURUNDI("Burundi", "Africa"), CAMEROON("Cameroon", "Africa"),
        CAPE_VERDE("Cape Verde", "Africa"), CENTRAL_AFRICAN_REPUBLIC("Central African Republic", "Africa"),
        CHAD("Chad", "Africa"), COMOROS("Comoros", "Africa"), COTE_IVOIRE("Cote d'Ivoire", "Africa"),
        DEMOCRATIC_REPUBLIC_OF_THE_CONGO("Democratic Republic of the Congo", "Africa"), DJIBOUTI("Djibouti", "Africa"),
        EGYPT("Egypt", "Africa"), EQUATORIAL_GUINEA("Equatorial Guinea", "Africa"), ERITREA("Eritrea", "Africa"),
        ETHIOPIA("Ethiopia", "Africa"), GABON("Gabon", "Africa"), GAMBIA("Gambia", "Africa"), GHANA("Ghana", "Africa"), GUINEA("Guinea", "Africa"),
        GUINEA_BISSAU("Guinea Bissau", "Africa"), KENYA("Kenya", "Africa"), LESOTHO("Lesotho", "Africa"),
        LIBERIA("Liberia", "Africa"), LIBYA("Libya", "Africa"), MADAGASCAR("Madagascar", "Africa"),
        MALAWI("Malawi", "Africa"), MALI("Mali", "Africa"), MAURITANIA("Mauritania", "Africa"), MAURITIUS("Mauritius", "Africa"),
        MOROCCO("Morocco", "Africa"), MOZAMBIQUE("Mozambique", "Africa"), NAMIBIA("Namibia", "Africa"),
        NIGER("Niger", "Africa"), NIGERIA("Nigeria", "Africa"),
        REPUBLIC_OF_THE_CONGO("Republic of the Congo", "Africa"), REUNION("Reunion", "Africa"),
        RWANDA("Rwanda", "Africa"), SAINT_HELENA("Saint Helena", "Africa"),
        SAO_TOME_AND_PRINCIPE("Sao Tome and Principe", "Africa"), SENEGAL("Senegal", "Africa"),
        SEYCHELLES("Seychelles", "Africa"), SIERRA_LEONE("Sierra Leone", "Africa"), SOMALIA("Somalia", "Africa"),
        SOUTH_AFRICA("South Africa", "Africa"), SOUTH_SUDAN("South Sudan", "Africa"), SUDAN("Sudan", "Africa"),
        SWAZILAND("Swaziland", "Africa"), TANZANIA("Tanzania", "Africa"), TOGO("Togo", "Africa"),
        TUNISIA("Tunisia", "Africa"), UGANDA("Uganda", "Africa"), WESTERN_SAHARA("Western Sahara", "Africa"),
        ZAMBIA("Zambia", "Africa"), ZIMBABWE("Zimbabwe", "Africa"),
        AFGHANISTAN("Afghanistan", "Asia"), ARMENIA("Armenia", "Asia"), AZERBAIJAN("Azerbaijan", "Asia"),
        BAHRAIN("Bahrain", "Asia"), BANGLADESH("Bangladesh", "Asia"), BHUTAN("Bhutan", "Asia"),
        BRUNEI("Brunei", "Asia"), BURMA("Burma", "Asia"), CAMBODIA("Cambodia", "Asia"),
        CHINA("China", "Asia"), CYPRUS("Cyprus", "Asia"), EAST_TIMOR("East Timor", "Asia"),
        GEORGIA("Georgia", "Asia"), HONG_KONG("Hong Kong", "Asia"), INDIA("India", "Asia"),
        INDONESIA("Indonesia", "Asia"), IRAN("Iran", "Asia"), IRAQ("Iraq", "Asia"),
        ISRAEL("Israel", "Asia"), JAPAN("Japan", "Asia"), JORDAN("Jordan", "Asia"),
        KAZAKHSTAN("Kazakhstan", "Asia"), KUWAIT("Kuwait", "Asia"), KYRGYZSTAN("Kyrgyzstan", "Asia"),
        LAOS("Laos", "Asia"), LEBANON("Lebanon", "Asia"), MACAU("Macau", "Asia"),
        MALAYSIA("Malaysia", "Asia"), MALDIVES("Maldives", "Asia"), MONGOLIA("Mongolia", "Asia"),
        NEPAL("Nepal", "Asia"), NORTH_KOREA("North Korea", "Asia"), OMAN("Oman", "Asia"),
        PAKISTAN("Pakistan", "Asia"), PHILIPPINES("Philippines", "Asia"), QATAR("Qatar", "Asia"),
        SAUDI_ARABIA("Saudi Arabia", "Asia"), SINGAPORE("Singapore", "Asia"), SOUTH_KOREA("South Korea", "Asia"),
        SRI_LANKA("Sri Lanka", "Asia"), SYRIA("Syria", "Asia"), TAIWAN("Taiwan", "Asia"),
        TAJIKISTAN("Tajikistan", "Asia"), THAILAND("Thailand", "Asia"), TURKEY("Turkey", "Asia"),
        TURKMENISTAN("Turkmenistan", "Asia"), UNITED_ARAB_EMIRATES("United Arab Emirates", "Asia"),
        UZBEKISTAN("Uzbekistan", "Asia"), VIETNAM("Vietnam", "Asia"), YEMEN("Yemen", "Asia"),
        ANGUILLA("Anguilla", "Caribbean"), ANTIGUA_AND_BARBUDA("Antigua and Barbuda", "Caribbean"), ARUBA("Aruba", "Caribbean"),
        THE_BAHAMAS("The Bahamas", "Caribbean"), BARBADOS("Barbados", "Caribbean"), BERMUDA("Bermuda", "Caribbean"),
        BRITISH_VIRGIN_ISLANDS("British Virgin Islands", "Caribbean"),
        CAYMAN_ISLANDS("Cayman Islands", "Caribbean"), CUBA("Cuba", "Caribbean"),
        DOMINICA("Dominica", "Caribbean"), DOMINICAN_REPUBLIC("Dominican Republic", "Caribbean"), GRENADA("Grenada", "Caribbean"),
        GUADELOUPE("Guadeloupe", "Caribbean"), HAITI("Haiti", "Caribbean"), JAMAICA("Jamaica", "Caribbean"),
        MARTINIQUE("Martinique", "Caribbean"), MONTSERRAT("Montserrat", "Caribbean"), NETHERLANDS_ANTILLES("Netherlands Antilles", "Caribbean"),
        PUERTO_RICO("Puerto Rico", "Caribbean"), SAINT_KITTS_AND_NEVIS("Saint Kitts and Nevis", "Caribbean"),
        SAINT_LUCIA("Saint Lucia", "Caribbean"), SAINT_VINCENT_AND_THE_GRENADINES("Saint Vincent and the Grenadines", "Caribbean"),
        TRINIDAD_AND_TOBAGO("Trinidad and Tobago", "Caribbean"), TURKS_AND_CAICOS_ISLANDS("Turks and Caicos Islands", "Caribbean"),
        U_S_VIRGIN_ISLANDS("U.S.Virgin Islands", "Caribbean"),
        BELIZE("Belize", "Central America"), COSTA_RICA("Costa Rica", "Central America"), EL_SALVADOR("El Salvador", "Central America"),
        GUATEMALA("Guatemala", "Central America"), HONDURAS("Honduras", "Central America"), NICARAGUA("Nicaragua", "Central America"),
        PANAMA("Panama", "Central America"),
        ALBANIA("Albania", "Europe"), ANDORRA("Andorra", "Europe"), AUSTRIA("Austria", "Europe"), BELARUS("Belarus", "Europe"),
        BELGIUM("Belgium", "Europe"), BOSNIA_AND_HERZEGOVINA("Bosnia and Herzegovina", "Europe"), BULGARIA("Bulgaria", "Europe"),
        CROATIA("Croatia", "Europe"),
        CZECH_REPUBLIC("Czech Republic", "Europe"), DENMARK("Denmark", "Europe"), ESTONIA("Estonia", "Europe"),
        FINLAND("Finland", "Europe"), FRANCE("France", "Europe"),
        GERMANY("Germany", "Europe"), GIBRALTAR("Gibraltar", "Europe"), GREECE("Greece", "Europe"),
        HOLY_SEE("Holy See", "Europe"), HUNGARY("Hungary", "Europe"),
        ICELAND("Iceland", "Europe"), IRELAND("Ireland", "Europe"), ITALY("Italy", "Europe"),
        KOSOVO("Kosovo", "Europe"), LATVIA("Latvia", "Europe"), LIECHTENSTEIN("Liechtenstein", "Europe"),
        LITHUANIA("Lithuania", "Europe"), LUXEMBOURG("Luxembourg", "Europe"), MACEDONIA("Macedonia", "Europe"),
        MALTA("Malta", "Europe"), MOLDOVA("Moldova", "Europe"),
        MONACO("Monaco", "Europe"), MONTENEGRO("Montenegro", "Europe"), NETHERLANDS("Netherlands", "Europe"),
        NORWAY("Norway", "Europe"), POLAND("Poland", "Europe"),
        PORTUGAL("Portugal", "Europe"), ROMANIA("Romania", "Europe"), RUSSIA("Russia", "Europe"),
        SAN_MARINO("San Marino", "Europe"), SLOVAK_REPUBLIC("Slovak Republic", "Europe"),
        SLOVENIA("Slovenia", "Europe"), SPAIN("Spain", "Europe"), SERBIA("Serbia", "Europe"), SERBIA_AND_MONTENEGRO("Serbia and Montenegro", "Europe"),
        SWEDEN("Sweden", "Europe"), SWITZERLAND("Switzerland", "Europe"), UKRAINE("Ukraine", "Europe"), UNITED_KINGDOM("United Kingdom", "Europe"),
        CANADA("Canada", "North America"), GREENLAND("Greenland", "North America"), MEXICO("Mexico", "North America"),
        SAINT_PIERRE_AND_MIQUELON("Saint Pierre and Miquelon", "North America"), UNITED_STATES("United States", "North America"),
        AMERICAN_SAMOA("American Samoa", "Oceania"), AUSTRALIA("Australia", "Oceania"), CHRISTMAS_ISLAND("Christmas Island", "Oceania"),
        COCOS_KEELING_ISLANDS("Cocos (Keeling) Islands", "Oceania"),
        COOK_ISLANDS("Cook Islands", "Oceania"), FEDERATED_STATES_OF_MICRONESIA("Federated States of Micronesia", "Oceania"),
        FIJI("Fiji", "Oceania"), FRENCH_POLYNESIA("French Polynesia", "Oceania"),
        GUAM("Guam", "Oceania"), KIRIBATI("Kiribati", "Oceania"), MARSHALL_ISLANDS("Marshall Islands", "Oceania"),
        NAURU("Nauru", "Oceania"), NEW_CALEDONIA("New Caledonia", "Oceania"),
        NEW_ZEALAND("New Zealand", "Oceania"), NIUE("Niue", "Oceania"),
        NORTHERN_MARIANA_ISLANDS("Northern Mariana Islands", "Oceania"), PALAU("Palau", "Oceania"),
        PAPUA_NEW_GUINEA("Papua New Guinea", "Oceania"),
        PITCAIRN_ISLANDS("Pitcairn Islands", "Oceania"), SAMOA("Samoa", "Oceania"),
        SOLOMON_ISLANDS("Solomon Islands", "Oceania"), TOKELAU("Tokelau", "Oceania"), TONGA("Tonga", "Oceania"),
        TUVALU("Tuvalu", "Oceania"), VANUATU("Vanuatu", "Oceania"), WALLIS_AND_FUTUNA_ISLANDS("Wallis and Futuna Islands", "Oceania"),
        ARGENTINA("Argentina", "South America"), BOLIVIA("Bolivia", "South America"),
        BRAZIL("Brazil", "South America"), CHILE("Chile", "South America"),
        COLOMBIA("Colombia", "South America"), ECUADOR("Ecuador", "South America"),
        FALKLAND_ISLANDS("Falkland Islands", "South America"), FRENCH_GUIANA("French Guiana", "South America"),
        GUYANA("Guyana", "South America"),
        PARAGUAY("Paraguay", "South America"), PERU("Peru", "South America"),
        SURINAME("Suriname", "South America"), URUGUAY("Uruguay", "South America"), VENEZUELA("Venezuela", "South America");

        private final String country;
        private final String region;

        private CountryRegion(String country, String region) {
            this.country = country;
            this.region = region;
        }

        public String getCountry() {
            return country;
        }

        public String getRegion() {
            return region;
        }
    }

    /**
     * Return a Map of Regions and Countries
     *
     * @param upperCased, boolean
     * @return Map Object, Key: String Object, Value: List Object of String Objects
     */
    public static Map<String, List<String>> getCountryRegionMap(boolean upperCased) {
        Map<String, List<String>> map = new HashMap();
        List<String> list;

        if (upperCased) {
            for (CountryRegion item : CountryRegion.values()) {
                if (!map.containsKey(item.getRegion().toUpperCase())) {
                    list = new ArrayList();
                    list.add(item.getCountry().toUpperCase());
                    map.put(item.getRegion().toUpperCase(), list);
                } else {
                    map.get(item.getRegion().toUpperCase()).add(item.getRegion().toUpperCase());
                }
            }
        } else {
            for (CountryRegion item : CountryRegion.values()) {
                if (!map.containsKey(item.getRegion())) {
                    list = new ArrayList();
                    list.add(item.getCountry());
                    map.put(item.getRegion(), list);
                } else {
                    map.get(item.getRegion()).add(item.getRegion());
                }
            }
        }

        return map;
    }

    /**
     * Return a list of Regions
     *
     * @param upperCased, boolean
     * @return List Object of String Objects
     */
    public static List<String> getRegionList(boolean upperCased) {
        List<String> list = new ArrayList();

        if (upperCased) {
            for (CountryRegion item : CountryRegion.values()) {
                if (!list.contains(item.getRegion().toUpperCase())) {
                    list.add(item.getRegion().toUpperCase());
                }
            }
        } else {
            for (CountryRegion item : CountryRegion.values()) {
                if (!list.contains(item.getRegion())) {
                    list.add(item.getRegion());
                }
            }
        }

        Collections.sort(list);
        return list;
    }

    /**
     * Return a list of all Countries
     *
     * @param upperCased, boolean
     * @return List Object of String Objects
     */
    public static List<String> getCountryList(boolean upperCased) {
        List<String> list = new ArrayList();

        if (upperCased) {
            for (CountryRegion item : CountryRegion.values()) {
                list.add(item.getCountry().toUpperCase());
            }
        } else {
            for (CountryRegion item : CountryRegion.values()) {
                list.add(item.getCountry());
            }
        }

        Collections.sort(list);
        return list;
    }

    /**
     * Return a Region based on the given country
     * 
     * @param country, String Object
     * @param upperCased, boolean
     * @return 
     */
    public static String getRegionByCountry(String country, boolean upperCased) {
        if (country != null) {
            for (CountryRegion item : CountryRegion.values()) {
                if (item.getCountry().equalsIgnoreCase(country)) {
                    return upperCased ? item.getRegion().toUpperCase() : item.getRegion();
                }
            }
        }
        return null;
    }

    /**
     * Return a list of Countries based on the given region
     * 
     * @param region, String Object
     * @param upperCased, boolean
     * @return 
     */
    public static List<String> getCountriesByRegion(String region, boolean upperCased) {
        List<String> list = new ArrayList();
        
        if (region != null) {
            if (upperCased) {
                for (CountryRegion item : CountryRegion.values()) {
                    if (item.getRegion().equalsIgnoreCase(region)) {
                        list.add(item.getCountry().toUpperCase());
                    }
                }
            } else {
                for (CountryRegion item : CountryRegion.values()) {
                    if (item.getRegion().equalsIgnoreCase(region)) {
                        list.add(item.getCountry());
                    }
                }
            }
        }

        Collections.sort(list);
        return list;
    }
}
