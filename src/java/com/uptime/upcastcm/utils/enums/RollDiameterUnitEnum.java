/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.utils.enums;

import com.uptime.upcastcm.bean.UserManageBean;
import java.util.ArrayList;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

/**
 *
 * @author kpati
 */
public class RollDiameterUnitEnum {
    private enum RollDiameterUnit {
        INCHES("Inches", "label_roll_diameter_units_inches"),
        MILLIMETERS("Millimeters", "label_roll_diameter_units_millimeters");
        
        private final String value;
        private final String langVariable;

        private RollDiameterUnit(String value, String langVariable) {
            this.value = value;
            this.langVariable = langVariable;
        }

        public String getValue() {
            return value;
        }
        
        public String getLangVariable() {
            return langVariable;
        }
    }
    
    /**
     * Return a List of SelectItem from the RollDiameterUnit enum
     * @return List Object of SelectItem Objects
     */
    public static List<SelectItem> getRollDiameterUnitItemList() {
        List<SelectItem> list = new ArrayList();
        UserManageBean userManageBean;
        
        if ((userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean")) != null) { 
            for (RollDiameterUnit item : RollDiameterUnit.values()) {
                list.add(new SelectItem(item.getValue(), userManageBean.getResourceBundleString(item.getLangVariable())));
            }
        }
        return list;
    }
    
    /**
     * Return a List of Strings of just the value from the RollDiameterUnit enum
     * @return List Object of String Objects 
     */
    public static List<String> getRollDiameterUnitValueList() {
        List<String> list = new ArrayList();
        
        for (RollDiameterUnit item : RollDiameterUnit.values()) {
            list.add(item.getValue());
        }
        return list;
    }
    
    /**
     * Return a List of Strings of just the labels based on the langVariable from the RollDiameterUnit enum
     * @return List Object of String Objects 
     */
    public static List<String> getRollDiameterUnitLabelList() {
        List<String> list = new ArrayList();
        UserManageBean userManageBean;
        
        if ((userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean")) != null) { 
            for (RollDiameterUnit item : RollDiameterUnit.values()) {
                list.add(userManageBean.getResourceBundleString(item.getLangVariable()));
            }
        }
        return list;
    }
    
    /**
     * Return the label based on the given value and langVariable from the RollDiameterUnit enum 
     * @param value, String Object
     * @return String Object
     */
    public static String getRollDiameterUnitLabelByValue(String value) {
        UserManageBean userManageBean;
        
        if (value != null && (userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean")) != null) {
            for (RollDiameterUnit item : RollDiameterUnit.values()) {
                if (item.getValue().equals(value)) {
                    return userManageBean.getResourceBundleString(item.getLangVariable());
                }
            }
        }
        return null;
    }
    
}
