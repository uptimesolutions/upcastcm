/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.utils.enums;

import com.uptime.upcastcm.bean.UserManageBean;
import java.util.ArrayList;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

/**
 *
 * @author twilcox
 */
public class ParamTypeEnum {

    private enum ParamType {
        HORIZONTAL_MOTOR("Waveform True Peak", "label_paramType_waveformTruePeak", "label_paramType_waveformTruePeak_description"),
        VERTICAL_MOTOR("Waveform Peak-to-Peak", "label_paramType_waveformPeakToPeak", "label_paramType_waveformPeakToPeak_description"),
        HORIZONTAL_PUMP("Waveform RMS", "label_paramType_waveformRMS", "label_paramType_waveformRMS_description"),
        VERTICAL_PUMP("Waveform Crest Factor", "label_paramType_waveformCrestFactor", "label_paramType_waveformCrestFactor_description"),
        GENERAL_FAN("Tach Speed", "label_paramType_tachSpeed", "label_paramType_tachSpeed_description"),
        FD_FAN("Total Spectral Energy", "label_paramType_totalSpectralEnergy", "label_paramType_totalSpectralEnergy_description"),
        ID_FAN("Total Non Sync Energy", "label_paramType_totalNonSyncEnergy", "label_paramType_totalNonSyncEnergy_description"),
        BLOWER("Total Sub Sync Energy", "label_paramType_totalSubSyncEnergy", "label_paramType_totalSubSyncEnergy_description"),
        
        //Demodulation is being remove for v1.0
        //GEARBOX("Demodulation", "label_paramType_demodulation", "label_paramType_demodulation_description"),
        
        
        GENERIC_HORIZONTAL_MACHINE("Spectral Band Total Amplitude", "label_paramType_spectralBandTotalAmplitude", "label_paramType_spectralBandTotalAmplitude_description"),
        GENERIC_VERTICAL_MACHINE("Spectral Band Peak Amplitude", "label_paramType_spectralBandPeakAmplitude", "label_paramType_spectralBandPeakAmplitude_description"),
        COMPRESSOR("Peak Amplitude", "label_paramType_peakAmplitude", "label_paramType_peakAmplitude_description");

        private final String value;
        private final String langVariable;
        private final String langDescriptionVariable;

        private ParamType(String value, String langVariable, String langDescriptionVariable) {
            this.value = value;
            this.langVariable = langVariable;
            this.langDescriptionVariable = langDescriptionVariable;
        }

        public String getValue() {
            return value;
        }

        public String getLangVariable() {
            return langVariable;
        }

        public String getLangDescriptionVariable() {
            return langDescriptionVariable;
        }
    }
    
    /**
     * Return a List of SelectItem from the ParamType enum
     * @return List Object of SelectItem Objects
     */
    public static List<SelectItem> getParamTypeItemList() {
        List<SelectItem> list = new ArrayList();
        UserManageBean userManageBean;
        
        if ((userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean")) != null) { 
            for (ParamType item : ParamType.values()) {
                list.add(new SelectItem(item.getValue(), userManageBean.getResourceBundleString(item.getLangVariable()), userManageBean.getResourceBundleString(item.getLangDescriptionVariable())));
            }
        }
        return list;
    }
    
    /**
     * Return a List of Strings of just the value from the ParamType enum
     * @return List Object of String Objects 
     */
    public static List<String> getParamTypeValueList() {
        List<String> list = new ArrayList();
        
        for (ParamType item : ParamType.values()) {
            list.add(item.getValue());
        }
        return list;
    }
    
    /**
     * Return a List of Strings of just the labels based on the langVariable from the ParamType enum
     * @return List Object of String Objects 
     */
    public static List<String> getParamTypeLabelList() {
        List<String> list = new ArrayList();
        UserManageBean userManageBean;
        
        if ((userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean")) != null) { 
            for (ParamType item : ParamType.values()) {
                list.add(userManageBean.getResourceBundleString(item.getLangVariable()));
            }
        }
        return list;
    }
    
    /**
     * Return the label based on the given value and langVariable from the ParamType enum 
     * @param value, String Object
     * @return String Object
     */
    public static String getParamTypeLabelByValue(String value) {
        UserManageBean userManageBean;
        
        if (value != null && (userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean")) != null) {
            for (ParamType item : ParamType.values()) {
                if (item.getValue().equals(value)) {
                    return userManageBean.getResourceBundleString(item.getLangVariable());
                }
            }
        }
        return null;
    }
}
