/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.utils.enums;

import com.uptime.upcastcm.bean.UserManageBean;
import java.util.ArrayList;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

/**
 *
 * @author twilcox
 */
public class MonthEnum {

    private enum Month {
        JANUARY("january", "label_january"),
        FEBRUARY("february", "label_february"),
        MARCH("march", "label_march"),
        APRIL("april", "label_april"),
        MAY("may", "label_may"),
        JUNE("june", "label_june"),
        JULY("july", "label_july"),
        AUGUST("august", "label_august"),
        SEPTEMBER("september", "label_september"),
        OCTOBER("october", "label_october"),
        NOVEMBER("november", "label_november"),
        DECEMBER("december", "label_december");

        private final String value;
        private final String langVariable;

        private Month(String value, String langVariable) {
            this.value = value;
            this.langVariable = langVariable;
        }

        public String getValue() {
            return value;
        }

        public String getLangVariable() {
            return langVariable;
        }
    }
    
    /**
     * Return a List of SelectItem from the Month enum
     * @return List Object of SelectItem Objects
     */
    public static List<SelectItem> getMonthItemList() {
        List<SelectItem> list = new ArrayList();
        UserManageBean userManageBean;
        
        if ((userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean")) != null) { 
            for (Month item : Month.values()) {
                list.add(new SelectItem(item.getValue(), userManageBean.getResourceBundleString(item.getLangVariable())));
            }
        }
        return list;
    }
    
    /**
     * Return a List of Strings of just the value from the Month enum
     * @return List Object of String Objects 
     */
    public static List<String> getMonthValueList() {
        List<String> list = new ArrayList();
        
        for (Month item : Month.values()) {
            list.add(item.getValue());
        }
        return list;
    }
    
    /**
     * Return a List of Strings of just the labels based on the langVariable from the Month enum
     * @return List Object of String Objects 
     */
    public static List<String> getMonthLabelList() {
        List<String> list = new ArrayList();
        UserManageBean userManageBean;
        
        if ((userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean")) != null) { 
            for (Month item : Month.values()) {
                list.add(userManageBean.getResourceBundleString(item.getLangVariable()));
            }
        }
        return list;
    }
    
    /**
     * Return the label based on the given value and langVariable from the Month enum 
     * @param value, String Object
     * @return String Object
     */
    public static String getMonthLabelByValue(String value) {
        UserManageBean userManageBean;
        
        if (value != null && (userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean")) != null) {
            for (Month item : Month.values()) {
                if (item.getValue().equals(value)) {
                    return userManageBean.getResourceBundleString(item.getLangVariable());
                }
            }
        }
        return null;
    }
}
