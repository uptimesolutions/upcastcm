/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.utils.enums;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 *
 * @author twilcox
 */
public class LocaleEnum {

    private enum Locales {
        EN_US("en", "US"),
        ES_MX("es", "MX");

        private final String language;
        private final String country;

        private Locales(String language, String country) {
            this.language = language;
            this.country = country;
        }

        public String getLanguage() {
            return language;
        }

        public String getCountry() {
            return country;
        }
    }
    
    /**
     * Return a List of Locale Objects from the Locales enum
     * @return List Object of Locale Objects
     */
    public static List<Locale> getLocaleItemList() {
        List<Locale> list = new ArrayList();

        for (Locales item : Locales.values()) {
            list.add(new Locale(item.getLanguage(), item.getCountry()));
        }

        return list;
    }
}
