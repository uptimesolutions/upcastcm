/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.utils.enums;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author twilcox
 */
public class MarkersPatternEnum {
    private enum MarkersPattern {
        DASHDOT_RED("DashDot-Red"), 
        SHORTDASH_BROWN("ShortDashDot-Brown"), 
        LONGDASHDOT_GREEN("LongDashDot-#40ff00"), 
        SHORTDASHDOT_FUCHSIA("ShortDashDot-Fuchsia"), 
        SHORTDASHDOT_GREEN("ShortDashDot-Green"), 
        LONGDASHDOT_RED("LongDashDot-Red"), 
        SHORTDASHDOT_BLUE("ShortDashDot-Blue"), 
        DASH_RED("Dash-Red"), 
        LONGDASHDOT_FUCHSIA("LongDashDot-Fuchsia"), 
        DASH_BLUE("Dash-Blue"), 
        DASH_GREEN("Dash-Green"), 
        DASHDOT_BROWN("DashDot-Brown"), 
        SHORTDASHDOT_BLACK("ShortDashDot-Black");

        private final String value;

        private MarkersPattern(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }
    
    /**
     * Return a List of Strings of just the value from the MarkersPattern enum
     * @return List Object of String Objects 
     */
    public static List<String> getMarkersPatternValueList() {
        List<String> list = new ArrayList();
        
        for (MarkersPattern item : MarkersPattern.values()) {
            list.add(item.getValue());
        }
        return list;
    }
}
