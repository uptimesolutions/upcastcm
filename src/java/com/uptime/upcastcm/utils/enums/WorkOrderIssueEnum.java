/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.utils.enums;

import com.uptime.upcastcm.bean.UserManageBean;
import java.util.ArrayList;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

/**
 *
 * @author twilcox
 */
public class WorkOrderIssueEnum {
    
    private enum Issue {
        BEARING_FAULT("bearing_fault", "label_work_order_issue1"),
        BPFO_DEFECT("bpfo_defect", "label_work_order_issue2"),
        BPFI_DEFECT("bpfi_defect", "label_work_order_issue3"),
        BSF_DEFECT("bsf_defect", "label_work_order_issue4"),
        FTF_DEFECT("ftf_defect", "label_work_order_issue5"),
        MISALIGNMENT("misalignment", "label_work_order_issue6"),
        STRUCTURAL_LOOSENESS("structural_looseness", "label_work_order_issue7"),
        MECHANICAL_LOOSENESS("mechanical_looseness", "label_work_order_issue8"),
        UNBALANCE("unbalance", "label_work_order_issue9"),
        BENT_SHAFT("bent_shaft", "label_work_order_issue10"),
        STRUCTURAL_RESONANCE("structural_resonance", "label_work_order_issue11"),
        COUPLING_ISSUE("coupling_issue", "label_work_order_issue12"),
        GEAR_DEFECT("gear_defect", "label_work_order_issue13"),
        MOTOR_ELECTRICAL_DEFECT("motor_electrical_defect", "label_work_order_issue14"),
        BELT_OR_SHEAVE_WEAR("belt_or_sheave_wear", "label_work_order_issue15"),
        POTENTIAL_FOD_OR_LOOSENESS("potential_fod_or_looseness", "label_work_order_issue16"),
        REDUCER_WEAR("reducer_wear", "label_work_order_issue17"),
        LUBRICATION_ISSUE("lubrication_issue", "label_work_order_issue18"),
        ECCENTRICITY("eccentricity", "label_work_order_issue19"),
        SENSOR_FAULT("sensor_fault", "label_work_order_issue20"),
        OTHER_OR_UNKNOWN("other_or_unknown", "label_work_order_issue21");
        
        private final String value;    
        private final String langVariable;

        private Issue(String value, String langVariable) {
            this.value = value;
            this.langVariable = langVariable;
        }

        public String getValue() {
            return value;
        }

        public String getLangVariable() {
            return langVariable;
        }
    }
    
    /**
     * Return a List of SelectItem from the Issue enum
     * @return List Object of SelectItem Objects
     */
    public static List<SelectItem> getIssueItemList() {
        List<SelectItem> list = new ArrayList();
        UserManageBean userManageBean;
        
        if ((userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean")) != null) { 
            for (Issue item : Issue.values()) {
                list.add(new SelectItem(item.getValue(), userManageBean.getResourceBundleString(item.getLangVariable())));
            }
        }
        return list;
    }
    
    /**
     * Return a List of Strings of just the value from the Issue enum
     * @return List Object of String Objects 
     */
    public static List<String> getIssueValueList() {
        List<String> list = new ArrayList();
        
        for (Issue item : Issue.values()) {
            list.add(item.getValue());
        }
        return list;
    }
    
    /**
     * Return a List of Strings of just the labels based on the langVariable from the Issue enum
     * @return List Object of String Objects 
     */
    public static List<String> getIssueLabelList() {
        List<String> list = new ArrayList();
        UserManageBean userManageBean;
        
        if ((userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean")) != null) { 
            for (Issue item : Issue.values()) {
                list.add(userManageBean.getResourceBundleString(item.getLangVariable()));
            }
        }
        return list;
    }
    
    /**
     * Return the label based on the given value and langVariable from the Issue enum 
     * @param value, String Object
     * @return String Object
     */
    public static String getIssueLabelByValue(String value) {
        UserManageBean userManageBean;
        
        if (value != null && (userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean")) != null) {
            for (Issue item : Issue.values()) {
                if (item.getValue().equals(value)) {
                    return userManageBean.getResourceBundleString(item.getLangVariable());
                }
            }
        }
        return null;
    }
}
