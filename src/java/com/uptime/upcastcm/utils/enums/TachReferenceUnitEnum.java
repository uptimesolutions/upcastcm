/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.utils.enums;

import com.uptime.upcastcm.bean.UserManageBean;
import java.util.ArrayList;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

/**
 *
 * @author twilcox
 */
public class TachReferenceUnitEnum {
    private enum TachReferenceUnit {
        RPM("RPM", "CPM", null),
        FPM("FPM", "FPM", null);
        
        private final String value;
        private final String label;
        private final String langVariable;

        private TachReferenceUnit(String value, String label, String langVariable) {
            this.value = value;
            this.label = label;
            this.langVariable = langVariable;
        }

        public String getValue() {
            return value;
        }

        public String getLabel() {
            return label;
        }

        public String getLangVariable() {
            return langVariable;
        }
    }
    
    /**
     * Return the label based on the given value from the TachReferenceUnit enums
     * @param value, String Object
     * @return String Object
     */
    public static String getTachReferenceUnitLabelByValue(String value) {
        UserManageBean userManageBean;
        
        if ((userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean")) != null && value != null) {
            for (TachReferenceUnit item : TachReferenceUnit.values()) {
                if (item.getValue().equals(value)) {
                    if (item.getLabel() != null && !item.getLabel().isEmpty()) {
                        return item.getLabel();
                    } else if (item.getLangVariable() != null && !item.getLangVariable().isEmpty()) {
                        return userManageBean.getResourceBundleString(item.getLangVariable());
                    }
                }
            }
        }
        
        return null;
    }
    
    /**
     * Return the List of Tach Reference Units
     * @return List Object of SelectItem Objects
     */
    public static List<SelectItem> getTachReferenceUnitItemList() {
        List<SelectItem> list = new ArrayList();
        UserManageBean userManageBean;
        
        if((userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean")) != null) {
            for (TachReferenceUnit item : TachReferenceUnit.values()) {
                if (item.getLabel() != null && !item.getLabel().isEmpty()) {
                    list.add(new SelectItem(item.getValue(), item.getLabel()));
                } else if (item.getLangVariable() != null && !item.getLangVariable().isEmpty()) {
                    list.add(new SelectItem(item.getValue(), userManageBean.getResourceBundleString(item.getLangVariable())));
                }
            }
        }
                    
        return list;
    }
}
