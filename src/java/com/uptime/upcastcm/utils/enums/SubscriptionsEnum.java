package com.uptime.upcastcm.utils.enums;

import java.util.ArrayList;
import java.util.List;
import javax.faces.model.SelectItem;

/**
 *
 * @author aswan
 */
public class SubscriptionsEnum {

    private enum SubscriptionsType {
        ALARMS("ALARMS", "ALARMS"),
        /*(WORKORDERS (Hide option in dropdown),*/
        AC_TREND("AC TREND", "AC_TREND"),
        DC_TREND("DC TREND", "DC_TREND"),
        WAVE("WAVE", "WAVE"),
        DEVICE("DEVICE", "DEVICE");

        private final String label;
        private final String value;

        private SubscriptionsType(String label, String value) {
            this.label = label;
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        public String getLabel() {
            return label;
        }

    }

    /**
     * Return a List of SelectItem Objects from the SubscriptionsType
     * enum
     *
     * @return List Object of SelectItem Objects
     */
    public static List<SelectItem> getSubscriptionsTypes() {
        List<SelectItem> list = new ArrayList<>();

        for (SubscriptionsType item : SubscriptionsType.values()) {
            list.add(new SelectItem(item.getValue(), item.getLabel()));
        }
        return list;
    }

    private enum MQProtocol {
        MQTT, ARTEMIS;
    }

    /**
     *
     * @return List Object of SelectItem Objects
     */
    public static List<SelectItem> getMQProtocols() {
        List<SelectItem> list = new ArrayList<>();

        for (MQProtocol item : MQProtocol.values()) {
            list.add(new SelectItem(item));
        }
        return list;
    }

}
