/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.utils.enums;

import java.util.ArrayList;
import java.util.List;
import javax.faces.model.SelectItem;

/**
 *
 * @author madhavi
 */
public class TS1DCChannelNumberEnum {
    
    private enum TS1DCChannelNumber {
        
        CHANNEL_0(0, "PI1"),
        CHANNEL_1(1, "PI2"),
        CHANNEL_2(2, "PI3"),
        CHANNEL_3(3, "PI4"),
        CHANNEL_4(4, "PI5"),
        CHANNEL_5(5, "PI6"),
        CHANNEL_6(6, "PI7");

        private final int value;
        private final String label;

        private TS1DCChannelNumber(int value, String label) {
            this.value = value;
            this.label = label;
        }

        public int getValue() {
            return value;
        }
        
        public String getLabel() {
            return label;
        }
    }

    /**
     * Return a List of SelectItem from the TS1DCChannelNumber Enum
     * @return List Object of SelectItem Objects
     */
    public static List<SelectItem> getTS1DCChannelNumberList() {
        List<SelectItem> list = new ArrayList();
        
        for (TS1DCChannelNumber item : TS1DCChannelNumber.values()) {
            list.add(new SelectItem(item.getValue(), item.getLabel()));
        }
            
        return list;
    }
    
    /**
     * Return the label based on the given value from the TS1DCChannelNumber enum 
     * @param value, String Object
     * @return String Object
     */
    public static String getTS1DCChannelNumberLabelByValue(int value) {
        
        for (TS1DCChannelNumber item : TS1DCChannelNumber.values()) {
            if (item.getValue()== value) {
                return item.getLabel();
            }
        }
        return null;
    }
}
