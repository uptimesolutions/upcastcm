/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.utils.enums;

import com.uptime.upcastcm.bean.UserManageBean;
import java.util.ArrayList;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

/**
 *
 * @author twilcox
 */
public class WorkOrderPriorityEnum {
    
    private enum Priority {
        PRIORITY_1((byte)1, "label_work_order_priority_1"),
        PRIORITY_2((byte)2, "label_work_order_priority_2"),
        PRIORITY_3((byte)3, "label_work_order_priority_3"),
        PRIORITY_4((byte)4, "label_work_order_priority_4");

        private final byte value;
        private final String langVariable;

        private Priority(byte value, String langVariable) {
            this.value = value;
            this.langVariable = langVariable;
        }

        public byte getValue() {
            return value;
        }
        
        public String getLangVariable() {
            return langVariable;
        }
    }
    
    /**
     * Return a List of SelectItem from the Priority enum
     * @return List Object of SelectItem Objects
     */
    public static List<SelectItem> getPriorityItemList() {
        List<SelectItem> list = new ArrayList();
        UserManageBean userManageBean;
        
        if ((userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean")) != null) { 
            for (Priority item : Priority.values()) {
                list.add(new SelectItem(item.getValue(), userManageBean.getResourceBundleString(item.getLangVariable())));
            }
        }
        return list;
    }
    
    /**
     * Return a List of Bytes of just the value from the Priority enum
     * @return List Object of Integer Objects 
     */
    public static List<Byte> getPriorityValueList() {
        List<Byte> list = new ArrayList();
        
        for (Priority item : Priority.values()) {
            list.add(item.getValue());
        }
        return list;
    }
    
    /**
     * Return a List of Strings of just the labels based on the langVariable from the Priority enum
     * @return List Object of String Objects 
     */
    public static List<String> getPriorityLabelList() {
        List<String> list = new ArrayList();
        UserManageBean userManageBean;
        
        if ((userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean")) != null) { 
            for (Priority item : Priority.values()) {
                list.add(userManageBean.getResourceBundleString(item.getLangVariable()));
            }
        }
        return list;
    }
    
    /**
     * Return the label based on the given value and langVariable from the Priority enum 
     * @param value, int
     * @return String Object
     */
    public static String getPriorityLabelByValue(byte value) {
        UserManageBean userManageBean;
        
        if ((userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean")) != null) {
            for (Priority item : Priority.values()) {
                if (item.getValue() == value) {
                    return userManageBean.getResourceBundleString(item.getLangVariable());
                }
            }
        }
        return null;
    }
}
