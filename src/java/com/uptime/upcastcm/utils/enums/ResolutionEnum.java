/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.utils.enums;

import java.util.ArrayList;
import java.util.List;
import javax.faces.model.SelectItem;

/**
 *
 * @author twilcox
 */
public class ResolutionEnum {
    
    private enum Resolution {
        RESOLUTION_100(100, "100"),
        RESOLUTION_200(200, "200"),
        RESOLUTION_400(400, "400"),
        RESOLUTION_800(800, "800"),
        RESOLUTION_1600(1600, "1600"),
        RESOLUTION_3200(3200, "3200"),
        RESOLUTION_6400(6400, "6400"),
        RESOLUTION_12800(12800, "12800"),
        RESOLUTION_25600(25600, "25600"),
        RESOLUTION_51200(51200, "51200");

        private final int value;
        private final String label;

        private Resolution(int value, String label) {
            this.value = value;
            this.label = label;
        }

        public int getValue() {
            return value;
        }

        public String getLabel() {
            return label;
        }
    }
    
    /**
     * Return a List of SelectItem from the Resolution enum
     * @return List Object of SelectItem Objects
     */
    public static List<SelectItem> getResolutionItemList() {
        List<SelectItem> list = new ArrayList();
        
        for (Resolution item : Resolution.values()) {
            list.add(new SelectItem(item.getValue(), item.getLabel()));
        }
        return list;
    }
}
