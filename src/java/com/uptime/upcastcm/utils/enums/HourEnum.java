/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.utils.enums;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author twilcox
 */
public class HourEnum {
    
    private enum Hour{
        HOUR_01("00:00-01:00"),
        HOUR_02("01:00-02:00"),
        HOUR_03("02:00-03:00"),
        HOUR_04("03:00-04:00"),
        HOUR_05("04:00-05:00"),
        HOUR_06("05:00-06:00"),
        HOUR_07("06:00-07:00"),
        HOUR_08("07:00-08:00"),
        HOUR_09("08:00-09:00"),
        HOUR_10("09:00-10:00"),
        HOUR_11("10:00-11:00"),
        HOUR_12("11:00-12:00"),
        HOUR_13("12:00-13:00"),
        HOUR_14("13:00-14:00"),
        HOUR_15("14:00-15:00"),
        HOUR_16("15:00-16:00"),
        HOUR_17("16:00-17:00"),
        HOUR_18("17:00-18:00"),
        HOUR_19("18:00-19:00"),
        HOUR_20("19:00-20:00"),
        HOUR_21("20:00-21:00"),
        HOUR_22("21:00-22:00"),
        HOUR_23("22:00-23:00"),
        HOUR_24("23:00-24:00");

        private final String value;

        private Hour(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    
    /**
     * Return a List of Strings from the Hour enum
     * @return List  Object of String Objects
     */
    public static List<String> getHourItemList() {
        List<String> list = new ArrayList();
        
        for (Hour item : Hour.values()) {
           list.add(item.getValue());
        }
            
        return list;
    }
}
