/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.utils.enums;

import com.uptime.upcastcm.bean.UserManageBean;
import java.util.ArrayList;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

/**
 *
 * @author twilcox
 */
public class WorkOrderActionEnum {
    
    private enum Action {
        CHANGE_BEARING("change_bearing", "label_work_order_action0"),
        TRIM_BALANCE("trim_balance", "label_work_order_action1"),
        ALIGN_COMPONENT("align_component", "label_work_order_action2"),
        INSPECT_COMPONENT("inspect_component", "label_work_order_action3"),
        CLEAN_COMPONENT("clean_component", "label_work_order_action4"),
        REPAIR_COMPONENT("repair_component", "label_work_order_action5"),
        REPLACE_COMPONENT("replace_component", "label_work_order_action6"),
        ELECTRICALLY_TEST_MOTOR("electrically_test_motor", "label_work_order_action7"),
        LUBRICATE_BEARING("lubricate_bearing", "label_work_order_action8"),
        MONITOR_COMPONENT("monitor_component", "label_work_order_action9");
        
        private final String value;
        private final String langVariable;

        private Action(String value, String langVariable) {
            this.value = value;
            this.langVariable = langVariable;
        }

        public String getValue() {
            return value;
        }

        public String getLangVariable() {
            return langVariable;
        }
    }
    
    /**
     * Return a List of SelectItem from the Action enum
     * @return List Object of SelectItem Objects
     */
    public static List<SelectItem> getActionItemList() {
        List<SelectItem> list = new ArrayList();
        UserManageBean userManageBean;
        
        if ((userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean")) != null) { 
            for (Action item : Action.values()) {
                list.add(new SelectItem(item.getValue(), userManageBean.getResourceBundleString(item.getLangVariable())));
            }
        }
        return list;
    }
    
    /**
     * Return a List of Strings of just the value from the Action enum
     * @return List Object of String Objects 
     */
    public static List<String> getActionValueList() {
        List<String> list = new ArrayList();
        
        for (Action item : Action.values()) {
            list.add(item.getValue());
        }
        return list;
    }
    
    /**
     * Return a List of Strings of just the labels based on the langVariable from the Action enum
     * @return List Object of String Objects 
     */
    public static List<String> getActionLabelList() {
        List<String> list = new ArrayList();
        UserManageBean userManageBean;
        
        if ((userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean")) != null) { 
            for (Action item : Action.values()) {
                list.add(userManageBean.getResourceBundleString(item.getLangVariable()));
            }
        }
        return list;
    }
    
    /**
     * Return the label based on the given value and langVariable from the Action enum 
     * @param value, String Object
     * @return String Object
     */
    public static String getActionLabelByValue(String value) {
        UserManageBean userManageBean;
        
        if (value != null && (userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean")) != null) {
            for (Action item : Action.values()) {
                if (item.getValue().equals(value)) {
                    return userManageBean.getResourceBundleString(item.getLangVariable());
                }
            }
        }
        return null;
    }
}
