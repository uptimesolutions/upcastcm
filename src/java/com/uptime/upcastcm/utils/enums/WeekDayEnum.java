/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.utils.enums;

import com.uptime.upcastcm.bean.UserManageBean;
import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

/**
 *
 * @author twilcox
 */
public class WeekDayEnum {

    private enum Day {
        MONDAY(DayOfWeek.MONDAY, "label_monday"),
        TUESDAY(DayOfWeek.TUESDAY, "label_tuesday"),
        WEDNESDAY(DayOfWeek.WEDNESDAY, "label_wednesday"),
        THURSDAY(DayOfWeek.THURSDAY, "label_thursday"),
        FRIDAY(DayOfWeek.FRIDAY, "label_friday"),
        SATURDAY(DayOfWeek.SATURDAY, "label_saturday"),
        SUNDAY(DayOfWeek.SUNDAY, "label_sunday");

        private final DayOfWeek value;
        private final String langVariable;

        private Day(DayOfWeek value, String langVariable) {
            this.value = value;
            this.langVariable = langVariable;
        }

        public DayOfWeek getValue() {
            return value;
        }

        public String getLangVariable() {
            return langVariable;
        }
    }
    
    /**
     * Return a List of SelectItem from the Day enum
     * @return List Object of SelectItem Objects
     */
    public static List<SelectItem> getDayItemList() {
        List<SelectItem> list = new ArrayList();
        UserManageBean userManageBean;
        
        if ((userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean")) != null) { 
            for (Day item : Day.values()) {
                list.add(new SelectItem(item.getValue(), userManageBean.getResourceBundleString(item.getLangVariable())));
            }
        }
        return list;
    }
    
    /**
     * Return a List of Strings of just the labels based on the langVariable from the Day enum
     * @return List Object of String Objects 
     */
    public static List<String> getDayLabelList() {
        List<String> list = new ArrayList();
        UserManageBean userManageBean;
        
        if ((userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean")) != null) { 
            for (Day item : Day.values()) {
                list.add(userManageBean.getResourceBundleString(item.getLangVariable()));
            }
        }
        return list;
    }
    
    /**
     * Return the label based on the given value and langVariable from the Day enum 
     * @param value, DayOfWeek Object
     * @return String Object
     */
    public static String getDayLabelByValue(DayOfWeek value) {
        UserManageBean userManageBean;
        
        if (value != null && (userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean")) != null) {
            for (Day item : Day.values()) {
                if (item.getValue().equals(value)) {
                    return userManageBean.getResourceBundleString(item.getLangVariable());
                }
            }
        }
        return null;
    }
}
