/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.utils.enums;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author twilcox
 */
public class DonutChartBGColorEnum {
    
    private enum DonutChartBGColor {
        GREEN("rgb(41, 171, 135)"),
        YELLOW("rgb(255, 205, 86)"),
        RED("rgb(255, 99, 132)");

        private final String value;

        private DonutChartBGColor(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    
    /**
     * Return a List of Strings from the DonutChartBGColor enum
     * @return List  Object of String Objects
     */
    public static List<String> getDonutChartBGColorItemList() {
        List<String> list = new ArrayList();
        
        for (DonutChartBGColor item : DonutChartBGColor.values()) {
           list.add(item.getValue());
        }
            
        return list;
    }
}
