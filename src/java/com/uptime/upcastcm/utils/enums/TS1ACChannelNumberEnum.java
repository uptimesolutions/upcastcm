/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.utils.enums;

import java.util.ArrayList;
import java.util.List;
import javax.faces.model.SelectItem;

/**
 *
 * @author madhavi
 */
public class TS1ACChannelNumberEnum {
    
    private enum TS1ACChannelNumber {
        
        CHANNEL_0(0, "CH1"),
        CHANNEL_1(1, "CH2"),
        CHANNEL_2(2, "CH3"),
        CHANNEL_4(4, "CH4"),
        CHANNEL_5(5, "CH5"),
        CHANNEL_6(6, "CH6"),
        CHANNEL_7(7, "CH7"),
        CHANNEL_3(3, "TACH");

        private final int value;
        private final String label;

        private TS1ACChannelNumber(int value, String label) {
            this.value = value;
            this.label = label;
        }

        public int getValue() {
            return value;
        }
        
        public String getLabel() {
            return label;
        }
    }

    /**
     * Return a List of SelectItem from the TS1ACChannelNumber Enum
     * @return List Object of SelectItem Objects
     */
    public static List<SelectItem> getTS1ACChannelNumberList() {
        List<SelectItem> list = new ArrayList();
        
        for (TS1ACChannelNumber item : TS1ACChannelNumber.values()) {
            list.add(new SelectItem(item.getValue(), item.getLabel()));
        }
            
        return list;
    }
    
    /**
     * Return the label based on the given value from the TS1ACChannelNumber enum 
     * @param value, String Object
     * @return String Object
     */
    public static String getTS1ACChannelNumberLabelByValue(int value) {
        
        for (TS1ACChannelNumber item : TS1ACChannelNumber.values()) {
            if (item.getValue()== value) {
                return item.getLabel();
            }
        }
        return null;
    }
}
