/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.utils.enums;

import com.uptime.upcastcm.bean.UserManageBean;
import java.util.ArrayList;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

/**
 *
 * @author twilcox
 */
public class DwellEnum {
    
    private enum Dwell {
        DWELL_0(0),
        DWELL_1(1),
        DWELL_2(2),
        DWELL_3(3),
        DWELL_4(4),
        DWELL_5(5);

        private final int value;

        private Dwell(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }
    
    /**
     * Return a List of SelectItem from the Dwell enum
     * @return List Object of SelectItem Objects
     */
    public static List<SelectItem> getDwellItemList() {
        List<SelectItem> list = new ArrayList();
        UserManageBean userManageBean;
        String measurementLanguage;
        
        if ((userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean")) != null) {
            measurementLanguage = " " + userManageBean.getResourceBundleString("label_measurements");
            for (Dwell item : Dwell.values()) {
                list.add(new SelectItem(item.getValue(), item.getValue() + measurementLanguage));
            }
        }
        return list;
    }
}
