/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.utils.enums;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author twilcox
 */
public class DeviceTypeEnum {
    
    private enum DeviceType{
        TS1X("TS1X"),
        STORMX("StormX"),
        MISTLX("MistLX"),
        STORMXT("StormXT"),
        STORMXRTD("StormXRTD");

        private final String value;

        private DeviceType(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    
    /**
     * Return a List of Strings from the DeviceType enum
     * @return List  Object of String Objects
     */
    public static List<String> getDeviceTypeItemList() {
        List<String> list = new ArrayList();
        
        for (DeviceType item : DeviceType.values()) {
           list.add(item.getValue());
        }
            
        return list;
    }
}
