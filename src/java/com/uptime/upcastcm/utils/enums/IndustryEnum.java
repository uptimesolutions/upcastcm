/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.utils.enums;

import com.uptime.upcastcm.bean.UserManageBean;
import java.util.ArrayList;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

/**
 *
 * @author twilcox
 */
public class IndustryEnum {

    private enum Industry {
        AUTOMOTIVE("Automotive", "label_industry_automotive"),
        PRODUCTION("Production", "label_industry_production"),
        MECHANICAL("Mechanical", "label_industry_mechanical"),
        CHEMICAL("Chemical", "label_industry_chemical"),
        TRANSPORTATION("Transportation", "label_industry_transportation");

        private final String value;
        private final String langVariable;

        private Industry(String value, String langVariable) {
            this.value = value;
            this.langVariable = langVariable;
        }

        public String getValue() {
            return value;
        }

        public String getLangVariable() {
            return langVariable;
        }
    }
    
    /**
     * Return a List of SelectItem from the Industry enum
     * @return List Object of SelectItem Objects
     */
    public static List<SelectItem> getIndustryItemList() {
        List<SelectItem> list = new ArrayList();
        UserManageBean userManageBean;
        
        if ((userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean")) != null) { 
            for (Industry item : Industry.values()) {
                list.add(new SelectItem(item.getValue(), userManageBean.getResourceBundleString(item.getLangVariable())));
            }
        }
        return list;
    }
    
    /**
     * Return a List of Strings of just the value from the Industry enum
     * @return List Object of String Objects 
     */
    public static List<String> getIndustryValueList() {
        List<String> list = new ArrayList();
        
        for (Industry item : Industry.values()) {
            list.add(item.getValue());
        }
        return list;
    }
    
    /**
     * Return a List of Strings of just the labels based on the langVariable from the Industry enum
     * @return List Object of String Objects 
     */
    public static List<String> getIndustryLabelList() {
        List<String> list = new ArrayList();
        UserManageBean userManageBean;
        
        if ((userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean")) != null) { 
            for (Industry item : Industry.values()) {
                list.add(userManageBean.getResourceBundleString(item.getLangVariable()));
            }
        }
        return list;
    }
    
    /**
     * Return the label based on the given value and langVariable from the Industry enum 
     * @param value, String Object
     * @return String Object
     */
    public static String getIndustryLabelByValue(String value) {
        UserManageBean userManageBean;
        
        if (value != null && (userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean")) != null) {
            for (Industry item : Industry.values()) {
                if (item.getValue().equals(value)) {
                    return userManageBean.getResourceBundleString(item.getLangVariable());
                }
            }
        }
        return null;
    }
}
