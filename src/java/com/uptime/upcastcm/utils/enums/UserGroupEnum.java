/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.utils.enums;

import com.uptime.upcastcm.bean.UserManageBean;
import java.util.ArrayList;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

/**
 *
 * @author twilcox
 */
public class UserGroupEnum {
    
    private enum UserGroup {
        
        //Site level Roles
        SITE_ADMIN(0, "admin", "label_admin", false),
        SITE_USER_MANAGEMENT(1, "user-management", "label_user_management", false),
        SITE_USER(2, "user", "label_user", false),
        SITE_CONFIGURATOR(3, "configurator", "label_configurator", false),
        SITE_VIEWER(4, "viewer", "label_viewer", false),
        SITE_USER_PLUS(10, "user+", "label_user_plus", false),
        
        //Uptime Group Roles
        UPTIME_SUPER_ADMINS(5, "super-admins", "", true),
        UPTIME_ACCOUNT_ADMINS(6, "account-admins", "", true),
        UPTIME_ANALYSTS(7, "analysts", "", true),
        UPTIME_SITE_BUILDERS(8, "site-builders", "", true),
        UPTIME_VIEWERS(9, "viewers", "", true);

        private final int value;
        private final String groupName;
        private final String langVariable;
        private final boolean uptimeGroup;

        private UserGroup(int value, String groupName, String langVariable, boolean uptimeGroup) {
            this.value = value;
            this.groupName = groupName;
            this.uptimeGroup = uptimeGroup;
            this.langVariable = langVariable;
        }

        public int getValue() {
            return value;
        }

        public String getGroupName() {
            return groupName;
        }

        public String getLangVariable() {
            return langVariable;
        }

        public boolean isUptimeGroup() {
            return uptimeGroup;
        }
    }
    
    /**
     * Return an int based on the group name and if the group name is
     * an Uptime user group
     * 
     * @param groupName, String Object
     * @param uptimeGroup, boolean
     * @return int
     */
    public static int getValueByGroupInfo(String groupName, boolean uptimeGroup) {
        if (groupName != null) {
            for (UserGroup item : UserGroup.values()) {
                if (item.getGroupName().equals(groupName.toLowerCase()) && item.isUptimeGroup() == uptimeGroup) {
                    return item.getValue();
                }
            }
        }
        return -1;
    }
    
    /**
     * Return a List of SelectItem items that aren't apart of the Uptime employee groups from the UserGroup enum
     * @return List Object of SelectItem Objects
     */
    public static List<SelectItem> getListOfSelectItemsFromSiteItems() {
        List<SelectItem> list =  new ArrayList();
        UserManageBean userManageBean;
        
        if ((userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean")) != null) { 
            for (UserGroup item : UserGroup.values()) {
                if (!item.isUptimeGroup()) {
                    list.add(new SelectItem(item.getGroupName(), userManageBean.getResourceBundleString(item.getLangVariable())));
                }
            }
        }
        return list;
    }
}
