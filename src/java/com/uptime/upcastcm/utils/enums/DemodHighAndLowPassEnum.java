/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.utils.enums;

import java.util.ArrayList;
import java.util.List;
import javax.faces.model.SelectItem;

/**
 *
 * @author twilcox
 */
public class DemodHighAndLowPassEnum {
    
    private enum Demod {
        DEMOD_500(500, "500 Hz"),
        DEMOD_1000(1000, "1000 Hz"),
        DEMOD_2000(2000, "2000 Hz"),
        DEMOD_5000(5000, "5000 Hz"),
        DEMOD_7500(7500, "7500 Hz");

        private final int value;
        private final String label;

        private Demod(int value, String label) {
            this.value = value;
            this.label = label;
        }

        public int getValue() {
            return value;
        }

        public String getLabel() {
            return label;
        }
    }
    
    /**
     * Return a List of SelectItem from the Demod enum
     * @return List Object of SelectItem Objects
     */
    public static List<SelectItem> getDemodItemList() {
        List<SelectItem> list = new ArrayList();
        
        for (Demod item : Demod.values()) {
            list.add(new SelectItem(item.getValue(), item.getLabel()));
        }
            
        return list;
    }
    
    /**
     * Return a List of SelectItem from the Demod enum based on the given info
     * @param selectedHighPass, int
     * @return List Object of SelectItem Objects
     */
    public static List<SelectItem> getLowPassOptionsGreaterThanSelectedHighPass(int selectedHighPass) {
        List<SelectItem> list = new ArrayList();
        
        for (Demod item : Demod.values()) {
            if (item.getValue() > selectedHighPass) {
                list.add(new SelectItem(item.getValue(), item.getLabel()));
            }
        }
            
        return list;
    }
}
