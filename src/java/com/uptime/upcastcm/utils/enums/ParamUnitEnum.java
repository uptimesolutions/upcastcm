/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.utils.enums;

import com.uptime.upcastcm.bean.UserManageBean;
import java.util.ArrayList;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

/**
 *
 * @author twilcox
 */
public class ParamUnitEnum {
    
    /**
     * Analysis Parameter Units
     */
    private enum ParamUnit {
        // Temperature Units
        // Temperature RTD
        F("F", "F", null),
        C("C", "C", null),
        
        // Voltage Units
        V("V", "V", null),
        MV("mV", "mV", null),
        UV("uV", "uV", null),
        
        // Current Units
        A("A", "A", null),
        MA("mA", "mA", null),
        UA("uA", "uA", null),
        
        // Phase Units
        DEGREES("Degrees", "Degrees", null),
        RADIANS("Radians", "Radians", null),
        
        // User-Defined Units
        USER_DEFINED_UNIT("userDefinedUnit", "userDefinedUnit", null),
        
        // Count Units
        COUNT("Count", "Count", null),
        
        // Relative Humidity Units
        RH("%RH", "%RH", null),
        
        // Pressure Units
        // Ultrasonic Units
        DB_SPL("dB SPL", "dB SPL", null),
        PASCALS("Pascals", "Pascals", null),
        PSI("PSI", "PSI", null),
        
        // Acceleration Units
        G_S("Gs", "Gs", null),
        M_S_S("m/s/s", "m/s/s", null),
        IN_S_S("in/s/s", "in/s/s", null),
        
        // Velocity Units
        IN_S("in/s", "in/s", null),
        MM_S("mm/s", "mm/s", null),
        
        // Displacement Units
        MILS("mils", "mils", null),
        MICRONS("microns", "microns", null),
        
        // Frequency Units
        RPM("RPM", "CPM", null),
        HZ("Hz", "Hz", null),
        
        // Others
        ORDERS("Orders", "Orders", null),
        
        // Time Units
        MINUTES("min", "min", null),
        MILLISECONDS("ms", "ms", null),
        SECONDS("s", "s", null);
        
        private final String value;
        private final String label;
        private final String langVariable;

        private ParamUnit(String value, String label, String langVariable) {
            this.value = value;
            this.label = label;
            this.langVariable = langVariable;
        }

        public String getValue() {
            return value;
        }

        public String getLabel() {
            return label;
        }

        public String getLangVariable() {
            return langVariable;
        }
        
        /**
         * Return the List of Analysis Parameter Units based on the given values
         * @param sensorType, String Object
         * @param paramType, String Object
         * @return List Object of ParamUnit enums
         */
        static List<ParamUnit> findBySensorTypeParamType(String sensorType, String paramType) {
            List<ParamUnit> list = new ArrayList();

            if(sensorType != null && paramType != null) {
                switch(paramType.toLowerCase()) {
                    case"waveform":
                    case"waveform true peak":
                    case"waveform peak-to-peak":
                    case"waveform rms":
                        switch(sensorType.toLowerCase()) {
                            case"acceleration":
                                list.add(G_S);
                                list.add(M_S_S);
                                list.add(IN_S_S);
                                break;
                            case"velocity":
                                list.add(IN_S);
                                list.add(MM_S);
                                break;
                            case"displacement":
                                list.add(MILS);
                                list.add(MICRONS);
                                break;
                            case"ultrasonic":
                                list.add(DB_SPL);
                                list.add(PASCALS);
                                list.add(PSI);
                                break;
                        }
                        break;
                    case"waveform crest factor":
                        break;
                    case"tach speed":
                        list.add(RPM);
                        list.add(HZ);
                        break;
                    case"spectrum":
                    case"total spectral energy":
                    case"total non sync energy":
                    case"total sub sync energy":
                    case"demodulation":
                    case"spectral band total amplitude":
                    case"spectral band peak amplitude":
                    case"peak amplitude":
                        switch(sensorType.toLowerCase()) {
                            case"acceleration":
                            case"velocity":
                            case"displacement":
                                list.add(G_S);
                                list.add(M_S_S);
                                list.add(IN_S_S);
                                list.add(IN_S);
                                list.add(MM_S);
                                list.add(MILS);
                                list.add(MICRONS);
                                break;
                            case"ultrasonic":
                                list.add(DB_SPL);
                                list.add(PASCALS);
                                list.add(PSI);
                                break;
                        }
                        break;
                }
            }

            return list;
        }
        
        /**
         * Return the List of Analysis Parameter Units based on the given values
         * @param sensorType, String Object
         * @param paramType, String Object
         * @return List Object of ParamUnit enums
         */
        static List<ParamUnit> findByDCSensorType(String sensorType) {
            List<ParamUnit> list = new ArrayList();

            if(sensorType != null) {
                switch(sensorType.toLowerCase()) {
                            case"temperature":
                            case"temperature rtd":
                                list.add(F);
                                list.add(C);
                                break;
                            case"frequency":
                                list.add(RPM);
                                list.add(HZ);
                                break;
                            case"pressure":
                                list.add(DB_SPL);
                                list.add(PASCALS);
                                list.add(PSI);
                                break;
                            case"voltage":
                                list.add(V);
                                list.add(MV);
                                list.add(UV);
                                break;
                            case"current":
                                list.add(A);
                                list.add(MA);
                                list.add(UA);
                                break;
                            case"phase":
                                list.add(DEGREES);
                                list.add(RADIANS);
                                break;
                            case"user-defined":
                                list.add(USER_DEFINED_UNIT);
                                break;
                            case"count":
                                list.add(COUNT);
                                break;
                            case"relative humidity":
                                list.add(RH);
                                break;
                }
            }

            return list;
        }
        
        /**
         * Return List of Temperature Units
         * @return List Object of ParamUnit enums
         */
        static List<ParamUnit> getTemperatureUnits() {
            List<ParamUnit> list;
            
            list = new ArrayList();
            list.add(F);
            list.add(C);
            
            return list;
        }
        
        /**
         * Return List of Voltage Units
         * @return List Object of ParamUnit enums
         */
        static List<ParamUnit> getVoltageUnits() {
            List<ParamUnit> list;
            
            list = new ArrayList();
            list.add(V);
            list.add(MV);
            list.add(UV);
            
            return list;
        }
        
        /**
         * Return List of Current Units
         * @return List Object of ParamUnit enums
         */
        static List<ParamUnit> getCurrentUnits() {
            List<ParamUnit> list;
            
            list = new ArrayList();
            list.add(A);
            list.add(MA);
            list.add(UA);
            
            return list;
        }
        
        /**
         * Return List of Phase Units
         * @return List Object of ParamUnit enums
         */
        static List<ParamUnit> getPhaseUnits() {
            List<ParamUnit> list;
            
            list = new ArrayList();
            list.add(DEGREES);
            list.add(RADIANS);
            
            return list;
        }
        
        /**
         * Return List of User-Defined Units
         * @return List Object of ParamUnit enums
         */
        static List<ParamUnit> getUserDefinedUnits() {
            List<ParamUnit> list;
            
            list = new ArrayList();
            list.add(USER_DEFINED_UNIT);
            
            return list;
        }
        
        /**
         * Return List of Count Units
         * @return List Object of ParamUnit enums
         */
        static List<ParamUnit> getCountUnits() {
            List<ParamUnit> list;
            
            list = new ArrayList();
            list.add(COUNT);
            
            return list;
        }
        
        /**
         * Return List of Relative Humidity Units
         * @return List Object of ParamUnit enums
         */
        static List<ParamUnit> getRelativeHumidityUnits() {
            List<ParamUnit> list;
            
            list = new ArrayList();
            list.add(RH);
            
            return list;
        }
        
        /**
         * Return List of Pressure Units
         * @return List Object of ParamUnit enums
         */
        static List<ParamUnit> getPressureUnits() {
            List<ParamUnit> list;
            
            list = new ArrayList();
            list.add(DB_SPL);
            list.add(PASCALS);
            list.add(PSI);
            
            return list;
        }
        
        /**
         * Return List of Acceleration Units
         * @return List Object of ParamUnit enums
         */
        static List<ParamUnit> getAccelerationUnits() {
            List<ParamUnit> list;
            
            list = new ArrayList();
            list.add(G_S);
            list.add(M_S_S);
            list.add(IN_S_S);
            
            return list;
        }
        
        /**
         * Return List of Velocity Units
         * @return List Object of ParamUnit enums
         */
        static List<ParamUnit> getVelocityUnits() {
            List<ParamUnit> list;
            
            list = new ArrayList();
            list.add(IN_S);
            list.add(MM_S);
            
            return list;
        }
        
        /**
         * Return List of Displacement Units
         * @return List Object of ParamUnit enums
         */
        static List<ParamUnit> getDisplacementUnits() {
            List<ParamUnit> list;
            
            list = new ArrayList();
            list.add(MILS);
            list.add(MICRONS);
            
            return list;
        }
        
        /**
         * Return List of Frequency Units
         * @return List Object of ParamUnit enums
         */
        static List<ParamUnit> getFrequencyUnits() {
            List<ParamUnit> list;
            
            list = new ArrayList();
            list.add(RPM);
            list.add(HZ);
            
            return list;
        }
        
        /**
         * Return List of Time Units
         * @return List Object of ParamUnit enums
         */
        static List<ParamUnit> getTimeUnits() {
            List<ParamUnit> list;
            
            list = new ArrayList();
            list.add(MINUTES);
            list.add(MILLISECONDS);
            list.add(SECONDS);
            
            return list;
        }

        /**
         * Return List of Frequency Plus Units
         * @return List Object of ParamUnit enums
         */
        static List<ParamUnit> getFrequencyPlusUnits() {
            List<ParamUnit> list;
            
            list = new ArrayList();
            list.add(RPM);
            list.add(HZ);
            list.add(ORDERS);
            
            return list;
        }
    }
    
    /**
     * Return the label based on the given value from the ParamUnit enums
     * @param value, String Object
     * @return String Object
     */
    public static String getParamUnitLabelByValue(String value) {
        UserManageBean userManageBean;
        
        if ((userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean")) != null && value != null) {
            for (ParamUnit item : ParamUnit.values()) {
                if (item.getValue().equals(value)) {
                    if (item.getLabel() != null && !item.getLabel().isEmpty()) {
                        return item.getLabel();
                    } else if (item.getLangVariable() != null && !item.getLangVariable().isEmpty()) {
                        return userManageBean.getResourceBundleString(item.getLangVariable());
                    }
                }
            }
        }
        
        return null;
    }
    
    /**
     * Return the List of AC Analysis Parameter Units based on the given values
     * @param sensorType, String Object
     * @param paramType, String Object
     * @return List Object of SelectItem Objects
     */
    public static List<SelectItem> getACParamUnitItemListBySensorTypeParamType(String sensorType, String paramType) {
        List<SelectItem> list = new ArrayList();
        UserManageBean userManageBean;
        
        if((userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean")) != null && sensorType != null && paramType != null) {
            ParamUnit.findBySensorTypeParamType(sensorType, paramType).stream().forEachOrdered(item -> {
                if (item.getLabel() != null && !item.getLabel().isEmpty()) {
                    list.add(new SelectItem(item.getValue(), item.getLabel()));
                } else if (item.getLangVariable() != null && !item.getLangVariable().isEmpty()) {
                    list.add(new SelectItem(item.getValue(), userManageBean.getResourceBundleString(item.getLangVariable())));
                }
            });
        }
        
        return list;
    }
    
    /**
     * Return the List of DC Analysis Parameter Units based on the given values
     * @param sensorType, String Object
     * @return List Object of SelectItem Objects
     */
    public static List<SelectItem> getDCParamUnitItemListBySensorType(String sensorType) {
        List<SelectItem> list = new ArrayList();
        UserManageBean userManageBean;
        
        if((userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean")) != null && sensorType != null) {
            ParamUnit.findByDCSensorType(sensorType).stream().forEachOrdered(item -> {
                if (item.getLabel() != null && !item.getLabel().isEmpty()) {
                    list.add(new SelectItem(item.getValue(), item.getLabel()));
                } else if (item.getLangVariable() != null && !item.getLangVariable().isEmpty()) {
                    list.add(new SelectItem(item.getValue(), userManageBean.getResourceBundleString(item.getLangVariable())));
                }
            });
        }
        
        return list;
    }
        
    /**
     * Return List of Temperature Units
     * @return List Object of SelectItem Objects
     */
    public static List<SelectItem> getTemperatureUnits() {
        List<SelectItem> list = new ArrayList();
        UserManageBean userManageBean;
        
        if((userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean")) != null) {
            ParamUnit.getTemperatureUnits().stream().forEachOrdered(item -> {
                if (item.getLabel() != null && !item.getLabel().isEmpty()) {
                    list.add(new SelectItem(item.getValue(), item.getLabel()));
                } else if (item.getLangVariable() != null && !item.getLangVariable().isEmpty()) {
                    list.add(new SelectItem(item.getValue(), userManageBean.getResourceBundleString(item.getLangVariable())));
                }
            });
        }
                    
        return list;
    }

    /**
     * Return List of Voltage Units
     * @return List Object of SelectItem Objects
     */
    public static List<SelectItem> getVoltageUnits() {
        List<SelectItem> list = new ArrayList();
        UserManageBean userManageBean;
        
        if((userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean")) != null) {
            ParamUnit.getVoltageUnits().stream().forEachOrdered(item -> {
                if (item.getLabel() != null && !item.getLabel().isEmpty()) {
                    list.add(new SelectItem(item.getValue(), item.getLabel()));
                } else if (item.getLangVariable() != null && !item.getLangVariable().isEmpty()) {
                    list.add(new SelectItem(item.getValue(), userManageBean.getResourceBundleString(item.getLangVariable())));
                }
            });
        }
                    
        return list;
    }

    /**
     * Return List of Current Units
     * @return List Object of SelectItem Objects
     */
    public static List<SelectItem> getCurrentUnits() {
        List<SelectItem> list = new ArrayList();
        UserManageBean userManageBean;
        
        if((userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean")) != null) {
            ParamUnit.getCurrentUnits().stream().forEachOrdered(item -> {
                if (item.getLabel() != null && !item.getLabel().isEmpty()) {
                    list.add(new SelectItem(item.getValue(), item.getLabel()));
                } else if (item.getLangVariable() != null && !item.getLangVariable().isEmpty()) {
                    list.add(new SelectItem(item.getValue(), userManageBean.getResourceBundleString(item.getLangVariable())));
                }
            });
        }
                    
        return list;
    }

    /**
     * Return List of Phase Units
     * @return List Object of SelectItem Objects
     */
    public static List<SelectItem> getPhaseUnits() {
        List<SelectItem> list = new ArrayList();
        UserManageBean userManageBean;
        
        if((userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean")) != null) {
            ParamUnit.getPhaseUnits().stream().forEachOrdered(item -> {
                if (item.getLabel() != null && !item.getLabel().isEmpty()) {
                    list.add(new SelectItem(item.getValue(), item.getLabel()));
                } else if (item.getLangVariable() != null && !item.getLangVariable().isEmpty()) {
                    list.add(new SelectItem(item.getValue(), userManageBean.getResourceBundleString(item.getLangVariable())));
                }
            });
        }
                    
        return list;
    }

    /**
     * Return List of User-Defined Units
     * @return List Object of SelectItem Objects
     */
    public static List<SelectItem> getUserDefinedUnits() {
        List<SelectItem> list = new ArrayList();
        UserManageBean userManageBean;
        
        if((userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean")) != null) {
            ParamUnit.getUserDefinedUnits().stream().forEachOrdered(item -> {
                if (item.getLabel() != null && !item.getLabel().isEmpty()) {
                    list.add(new SelectItem(item.getValue(), item.getLabel()));
                } else if (item.getLangVariable() != null && !item.getLangVariable().isEmpty()) {
                    list.add(new SelectItem(item.getValue(), userManageBean.getResourceBundleString(item.getLangVariable())));
                }
            });
        }
                    
        return list;
    }

    /**
     * Return List of Count Units
     * @return List Object of SelectItem Objects
     */
    public static List<SelectItem> getCountUnits() {
        List<SelectItem> list = new ArrayList();
        UserManageBean userManageBean;
        
        if((userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean")) != null) {
            ParamUnit.getCountUnits().stream().forEachOrdered(item -> {
                if (item.getLabel() != null && !item.getLabel().isEmpty()) {
                    list.add(new SelectItem(item.getValue(), item.getLabel()));
                } else if (item.getLangVariable() != null && !item.getLangVariable().isEmpty()) {
                    list.add(new SelectItem(item.getValue(), userManageBean.getResourceBundleString(item.getLangVariable())));
                }
            });
        }
                    
        return list;
    }

    /**
     * Return List of Relative Humidity Units
     * @return List Object of SelectItem Objects
     */
    public static List<SelectItem> getRelativeHumidityUnits() {
        List<SelectItem> list = new ArrayList();
        UserManageBean userManageBean;
        
        if((userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean")) != null) {
            ParamUnit.getRelativeHumidityUnits().stream().forEachOrdered(item -> {
                if (item.getLabel() != null && !item.getLabel().isEmpty()) {
                    list.add(new SelectItem(item.getValue(), item.getLabel()));
                } else if (item.getLangVariable() != null && !item.getLangVariable().isEmpty()) {
                    list.add(new SelectItem(item.getValue(), userManageBean.getResourceBundleString(item.getLangVariable())));
                }
            });
        }
                    
        return list;
    }
        
    /**
     * Return List of Temperature RTD Units
     * Note: Temperature RTD uses the same units as Temperature
     * @return List Object of SelectItem Objects
     */
    public static List<SelectItem> getTemperatureRTDUnits() {
        List<SelectItem> list = new ArrayList();
        UserManageBean userManageBean;
        
        if((userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean")) != null) {
            ParamUnit.getTemperatureUnits().stream().forEachOrdered(item -> {
                if (item.getLabel() != null && !item.getLabel().isEmpty()) {
                    list.add(new SelectItem(item.getValue(), item.getLabel()));
                } else if (item.getLangVariable() != null && !item.getLangVariable().isEmpty()) {
                    list.add(new SelectItem(item.getValue(), userManageBean.getResourceBundleString(item.getLangVariable())));
                }
            });
        }
                    
        return list;
    }
    
    /**
     * Return List of Pressure Units
     * @return List Object of SelectItem Objects
     */
    public static List<SelectItem> getPressureUnits() {
        List<SelectItem> list = new ArrayList();
        UserManageBean userManageBean;
        
        if((userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean")) != null) {
            ParamUnit.getPressureUnits().stream().forEachOrdered(item -> {
                if (item.getLabel() != null && !item.getLabel().isEmpty()) {
                    list.add(new SelectItem(item.getValue(), item.getLabel()));
                } else if (item.getLangVariable() != null && !item.getLangVariable().isEmpty()) {
                    list.add(new SelectItem(item.getValue(), userManageBean.getResourceBundleString(item.getLangVariable())));
                }
            });
        }
                    
        return list;
    }
    
    /**
     * Return List of Acceleration Units
     * @return List Object of SelectItem Objects
     */
    public static List<SelectItem> getAccelerationUnits() {
        List<SelectItem> list = new ArrayList();
        UserManageBean userManageBean;
        
        if((userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean")) != null) {
            ParamUnit.getAccelerationUnits().stream().forEachOrdered(item -> {
                if (item.getLabel() != null && !item.getLabel().isEmpty()) {
                    list.add(new SelectItem(item.getValue(), item.getLabel()));
                } else if (item.getLangVariable() != null && !item.getLangVariable().isEmpty()) {
                    list.add(new SelectItem(item.getValue(), userManageBean.getResourceBundleString(item.getLangVariable())));
                }
            });
        }
                    
        return list;
    }
    
    /**
     * Return List of Velocity Units
     * @return List Object of SelectItem Objects
     */
    public static List<SelectItem> getVelocityUnits() {
        List<SelectItem> list = new ArrayList();
        UserManageBean userManageBean;
        
        if((userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean")) != null) {
            ParamUnit.getVelocityUnits().stream().forEachOrdered(item -> {
                if (item.getLabel() != null && !item.getLabel().isEmpty()) {
                    list.add(new SelectItem(item.getValue(), item.getLabel()));
                } else if (item.getLangVariable() != null && !item.getLangVariable().isEmpty()) {
                    list.add(new SelectItem(item.getValue(), userManageBean.getResourceBundleString(item.getLangVariable())));
                }
            });
        }
                    
        return list;
    }
    
    /**
     * Return List of Displacement Units
     * @return List Object of SelectItem Objects
     */
    public static List<SelectItem> getDisplacementUnits() {
        List<SelectItem> list = new ArrayList();
        UserManageBean userManageBean;
        
        if((userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean")) != null) {
            ParamUnit.getDisplacementUnits().stream().forEachOrdered(item -> {
                if (item.getLabel() != null && !item.getLabel().isEmpty()) {
                    list.add(new SelectItem(item.getValue(), item.getLabel()));
                } else if (item.getLangVariable() != null && !item.getLangVariable().isEmpty()) {
                    list.add(new SelectItem(item.getValue(), userManageBean.getResourceBundleString(item.getLangVariable())));
                }
            });
        }
                    
        return list;
    }
    
    /**
     * Return List of Ultrasonic Units
     * Note: Ultrasonic uses the same units as Pressure
     * @return List Object of SelectItem Objects
     */
    public static List<SelectItem> getUltrasonicUnits() {
        List<SelectItem> list = new ArrayList();
        UserManageBean userManageBean;
        
        if((userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean")) != null) {
            ParamUnit.getPressureUnits().stream().forEachOrdered(item -> {
                if (item.getLabel() != null && !item.getLabel().isEmpty()) {
                    list.add(new SelectItem(item.getValue(), item.getLabel()));
                } else if (item.getLangVariable() != null && !item.getLangVariable().isEmpty()) {
                    list.add(new SelectItem(item.getValue(), userManageBean.getResourceBundleString(item.getLangVariable())));
                }
            });
        }
                    
        return list;
    }
    
    /**
     * Return List of Frequency Units
     * @return List Object of SelectItem Objects
     */
    public static List<SelectItem> getFrequencyUnits() {
        List<SelectItem> list = new ArrayList();
        UserManageBean userManageBean;
        
        if((userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean")) != null) {
            ParamUnit.getFrequencyUnits().stream().forEachOrdered(item -> {
                if (item.getLabel() != null && !item.getLabel().isEmpty()) {
                    list.add(new SelectItem(item.getValue(), item.getLabel()));
                } else if (item.getLangVariable() != null && !item.getLangVariable().isEmpty()) {
                    list.add(new SelectItem(item.getValue(), userManageBean.getResourceBundleString(item.getLangVariable())));
                }
            });
        }
                    
        return list;
    }
    
    /**
     * Return List of Frequency Plus Units
     * @return List Object of SelectItem Objects
     */
    public static List<SelectItem> getFrequencyPlusUnits() {
        List<SelectItem> list = new ArrayList();
        UserManageBean userManageBean;
        
        if((userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean")) != null) {
            ParamUnit.getFrequencyPlusUnits().stream().forEachOrdered(item -> {
                if (item.getLabel() != null && !item.getLabel().isEmpty()) {
                    list.add(new SelectItem(item.getValue(), item.getLabel()));
                } else if (item.getLangVariable() != null && !item.getLangVariable().isEmpty()) {
                    list.add(new SelectItem(item.getValue(), userManageBean.getResourceBundleString(item.getLangVariable())));
                }
            });
        }
                    
        return list;
    }
    
    /**
     * Return List of Time Units
     * @return List Object of SelectItem Objects
     */
    public static List<SelectItem> getTimeUnits() {
        List<SelectItem> list = new ArrayList();
        UserManageBean userManageBean;
        
        if((userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean")) != null) {
            ParamUnit.getTimeUnits().stream().forEachOrdered(item -> {
                if (item.getLabel() != null && !item.getLabel().isEmpty()) {
                    list.add(new SelectItem(item.getValue(), item.getLabel()));
                } else if (item.getLangVariable() != null && !item.getLangVariable().isEmpty()) {
                    list.add(new SelectItem(item.getValue(), userManageBean.getResourceBundleString(item.getLangVariable())));
                }
            });
        }
                    
        return list;
    }
}
