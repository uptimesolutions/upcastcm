/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.utils.interfaces;

import com.uptime.upcastcm.utils.helperclass.Filters;

/**
 *
 * @author twilcox
 */
public interface PresetDialogFields {
    public void resetPage();
    public void presetFields (Filters presets, Object object);
    public void presetFields (int operationType, Object object);
    public void presetFields (Object object);
}
