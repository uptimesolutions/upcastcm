/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.utils.helperclass;

import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;
import javax.faces.model.SelectItem;

/**
 *
 * @author twilcox
 */
public class Filters implements Serializable {
    private String region;
    private String country;
    private String siteOver100;
    private SelectItem site;
    private String areaOver100;
    private SelectItem area;
    private String assetOver100;
    private SelectItem asset;
    private String pointLocationOver100;
    private SelectItem pointLocation;
    private String pointOver100;
    private SelectItem point;
    private String apSetOver100;
    private SelectItem apSet;
    private String alSetParamOver100;
    private SelectItem alSetParam;
    private short createdYear;
    private boolean pointLocAlarmEnabled;

    /**
     * Constructor
     */
    public Filters() {
        createdYear = (short)LocalDate.now().getYear();
    }

    /**
     * Parameterized Constructor
     * @param filters 
     */
    public Filters(Filters filters) {
        createdYear = filters.getCreatedYear();
        region = filters.getRegion();
        country = filters.getCountry();
        siteOver100 = filters.getSiteOver100();
        site = filters.getSite();
        areaOver100 = filters.getAreaOver100();
        area = filters.getArea();
        assetOver100 = filters.getAssetOver100();
        asset = filters.getAsset();
        pointLocationOver100 = filters.getPointLocationOver100();
        pointLocation = filters.getPointLocation();
        pointOver100 = filters.getPointOver100();
        point = filters.getPoint();
        apSetOver100 = filters.getApSetOver100();
        apSet = filters.getApSet();
        alSetParamOver100 = filters.getAlSetParamOver100();
        alSetParam = filters.getAlSetParam();
        pointLocAlarmEnabled = filters.isPointLocAlarmEnabled();
    }

    /**
     * clear up to the given value
     * @param value, String Object 
     */
    public void clear(String value) {
        if (value != null) {
            switch(value) {
                case YEAR:
                    region = null;
                    country = null;
                    siteOver100 = null;
                    site = null;
                    areaOver100 = null;
                    area = null;
                    assetOver100 = null;
                    asset = null;
                    pointLocationOver100 = null;
                    pointLocation = null;
                    pointOver100 = null;
                    point = null;
                    apSetOver100 = null;
                    apSet = null;
                    alSetParamOver100 = null;
                    alSetParam = null;
                    break;
                case REGION:
                    country = null;
                    siteOver100 = null;
                    site = null;
                    areaOver100 = null;
                    area = null;
                    assetOver100 = null;
                    asset = null;
                    pointLocationOver100 = null;
                    pointLocation = null;
                    pointOver100 = null;
                    point = null;
                    apSetOver100 = null;
                    apSet = null;
                    alSetParamOver100 = null;
                    alSetParam = null;
                    break;
                case COUNTRY:
                    siteOver100 = null;
                    site = null;
                    areaOver100 = null;
                    area = null;
                    assetOver100 = null;
                    asset = null;
                    pointLocationOver100 = null;
                    pointLocation = null;
                    pointOver100 = null;
                    point = null;
                    apSetOver100 = null;
                    apSet = null;
                    alSetParamOver100 = null;
                    alSetParam = null;
                    break;
                case SITE_OVER_100:
                    site = null;
                    areaOver100 = null;
                    area = null;
                    assetOver100 = null;
                    asset = null;
                    pointLocationOver100 = null;
                    pointLocation = null;
                    pointOver100 = null;
                    point = null;
                    apSetOver100 = null;
                    apSet = null;
                    alSetParamOver100 = null;
                    alSetParam = null;
                    break;
                case SITE:
                    areaOver100 = null;
                    area = null;
                    assetOver100 = null;
                    asset = null;
                    pointLocationOver100 = null;
                    pointLocation = null;
                    pointOver100 = null;
                    point = null;
                    apSetOver100 = null;
                    apSet = null;
                    alSetParamOver100 = null;
                    alSetParam = null;
                    break;
                case AREA_OVER_100:
                    area = null;
                    assetOver100 = null;
                    asset = null;
                    pointLocationOver100 = null;
                    pointLocation = null;
                    pointOver100 = null;
                    point = null;
                    apSetOver100 = null;
                    apSet = null;
                    alSetParamOver100 = null;
                    alSetParam = null;
                    break;
                case AREA:
                    assetOver100 = null;
                    asset = null;
                    pointLocationOver100 = null;
                    pointLocation = null;
                    pointOver100 = null;
                    point = null;
                    apSetOver100 = null;
                    apSet = null;
                    alSetParamOver100 = null;
                    alSetParam = null;
                    break;
                case ASSET_OVER_100:
                    asset = null;
                    pointLocationOver100 = null;
                    pointLocation = null;
                    pointOver100 = null;
                    point = null;
                    apSetOver100 = null;
                    apSet = null;
                    alSetParamOver100 = null;
                    alSetParam = null;
                    break;
                case ASSET:
                    pointLocationOver100 = null;
                    pointLocation = null;
                    pointOver100 = null;
                    point = null;
                    apSetOver100 = null;
                    apSet = null;
                    alSetParamOver100 = null;
                    alSetParam = null;
                    break;
                case POINT_LOCATION_OVER_100:
                    pointLocation = null;
                    pointOver100 = null;
                    point = null;
                    apSetOver100 = null;
                    apSet = null;
                    alSetParamOver100 = null;
                    alSetParam = null;
                    break;
                case POINT_LOCATION:
                    pointOver100 = null;
                    point = null;
                    apSetOver100 = null;
                    apSet = null;
                    alSetParamOver100 = null;
                    alSetParam = null;
                    break;
                case POINT_OVER_100:
                    point = null;
                    apSetOver100 = null;
                    apSet = null;
                    alSetParamOver100 = null;
                    alSetParam = null;
                    break;
                case POINT:
                    apSetOver100 = null;
                    apSet = null;
                    alSetParamOver100 = null;
                    alSetParam = null;
                    break;
                case AP_SET_OVER_100:
                    apSet = null;
                    alSetParamOver100 = null;
                    alSetParam = null;
                    break;
                case AP_SET:
                    alSetParamOver100 = null;
                    alSetParam = null;
                    break;
                case AL_SET_PARAM_OVER_100:
                    alSetParam = null;
                    break;
            }
        }
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getSiteOver100() {
        return siteOver100;
    }

    public void setSiteOver100(String siteOver100) {
        this.siteOver100 = siteOver100;
    }

    public SelectItem getSite() {
        return site;
    }

    public void setSite(SelectItem site) {
        this.site = site;
    }

    public String getAreaOver100() {
        return areaOver100;
    }

    public void setAreaOver100(String areaOver100) {
        this.areaOver100 = areaOver100;
    }

    public SelectItem getArea() {
        return area;
    }

    public void setArea(SelectItem area) {
        this.area = area;
    }

    public String getAssetOver100() {
        return assetOver100;
    }

    public void setAssetOver100(String assetOver100) {
        this.assetOver100 = assetOver100;
    }

    public SelectItem getAsset() {
        return asset;
    }

    public void setAsset(SelectItem asset) {
        this.asset = asset;
    }

    public String getPointLocationOver100() {
        return pointLocationOver100;
    }

    public void setPointLocationOver100(String pointLocationOver100) {
        this.pointLocationOver100 = pointLocationOver100;
    }

    public SelectItem getPointLocation() {
        return pointLocation;
    }

    public void setPointLocation(SelectItem pointLocation) {
        this.pointLocation = pointLocation;
    }

    public String getPointOver100() {
        return pointOver100;
    }

    public void setPointOver100(String pointOver100) {
        this.pointOver100 = pointOver100;
    }

    public SelectItem getPoint() {
        return point;
    }

    public void setPoint(SelectItem point) {
        this.point = point;
    }

    public String getApSetOver100() {
        return apSetOver100;
    }

    public void setApSetOver100(String apSetOver100) {
        this.apSetOver100 = apSetOver100;
    }

    public SelectItem getApSet() {
        return apSet;
    }

    public void setApSet(SelectItem apSet) {
        this.apSet = apSet;
    }

    public String getAlSetParamOver100() {
        return alSetParamOver100;
    }

    public void setAlSetParamOver100(String alSetParamOver100) {
        this.alSetParamOver100 = alSetParamOver100;
    }

    public SelectItem getAlSetParam() {
        return alSetParam;
    }

    public void setAlSetParam(SelectItem alSetParam) {
        this.alSetParam = alSetParam;
    }

    public short getCreatedYear() {
        return createdYear;
    }

    public void setCreatedYear(short createdYear) {
        this.createdYear = createdYear;
    }

    public boolean isPointLocAlarmEnabled() {
        return pointLocAlarmEnabled;
    }

    public void setPointLocAlarmEnabled(boolean pointLocAlarmEnabled) {
        this.pointLocAlarmEnabled = pointLocAlarmEnabled;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this.region);
        hash = 37 * hash + Objects.hashCode(this.country);
        hash = 37 * hash + Objects.hashCode(this.siteOver100);
        hash = 37 * hash + Objects.hashCode(this.site);
        hash = 37 * hash + Objects.hashCode(this.areaOver100);
        hash = 37 * hash + Objects.hashCode(this.area);
        hash = 37 * hash + Objects.hashCode(this.assetOver100);
        hash = 37 * hash + Objects.hashCode(this.asset);
        hash = 37 * hash + Objects.hashCode(this.pointLocationOver100);
        hash = 37 * hash + Objects.hashCode(this.pointLocation);
        hash = 37 * hash + Objects.hashCode(this.pointOver100);
        hash = 37 * hash + Objects.hashCode(this.point);
        hash = 37 * hash + Objects.hashCode(this.apSetOver100);
        hash = 37 * hash + Objects.hashCode(this.apSet);
        hash = 37 * hash + Objects.hashCode(this.alSetParamOver100);
        hash = 37 * hash + Objects.hashCode(this.alSetParam);
        hash = 37 * hash + this.createdYear;
        hash = 37 * hash + (this.pointLocAlarmEnabled ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Filters other = (Filters) obj;
        if (this.createdYear != other.createdYear) {
            return false;
        }
        if (this.pointLocAlarmEnabled != other.pointLocAlarmEnabled) {
            return false;
        }
        if (!Objects.equals(this.region, other.region)) {
            return false;
        }
        if (!Objects.equals(this.country, other.country)) {
            return false;
        }
        if (!Objects.equals(this.siteOver100, other.siteOver100)) {
            return false;
        }
        if (!Objects.equals(this.areaOver100, other.areaOver100)) {
            return false;
        }
        if (!Objects.equals(this.assetOver100, other.assetOver100)) {
            return false;
        }
        if (!Objects.equals(this.pointLocationOver100, other.pointLocationOver100)) {
            return false;
        }
        if (!Objects.equals(this.pointOver100, other.pointOver100)) {
            return false;
        }
        if (!Objects.equals(this.apSetOver100, other.apSetOver100)) {
            return false;
        }
        if (!Objects.equals(this.alSetParamOver100, other.alSetParamOver100)) {
            return false;
        }
        if (!Objects.equals(this.site, other.site)) {
            return false;
        }
        if (!Objects.equals(this.area, other.area)) {
            return false;
        }
        if (!Objects.equals(this.asset, other.asset)) {
            return false;
        }
        if (!Objects.equals(this.pointLocation, other.pointLocation)) {
            return false;
        }
        if (!Objects.equals(this.point, other.point)) {
            return false;
        }
        if (!Objects.equals(this.apSet, other.apSet)) {
            return false;
        }
        if (!Objects.equals(this.alSetParam, other.alSetParam)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Filters{" + "region=" + region + ", country=" + country + ", siteOver100=" + siteOver100 + ", site=" + site + ", areaOver100=" + areaOver100 + ", area=" + area + ", assetOver100=" + assetOver100 + ", asset=" + asset + ", pointLocationOver100=" + pointLocationOver100 + ", pointLocation=" + pointLocation + ", pointOver100=" + pointOver100 + ", point=" + point + ", apSetOver100=" + apSetOver100 + ", apSet=" + apSet + ", alSetParamOver100=" + alSetParamOver100 + ", alSetParam=" + alSetParam + ", createdYear=" + createdYear + ", pointLocAlarmEnabled=" + pointLocAlarmEnabled + '}';
    }
}
