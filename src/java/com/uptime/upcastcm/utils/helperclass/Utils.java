/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.utils.helperclass;

import com.uptime.upcastcm.bean.AssetAnalysisBean;
import com.uptime.upcastcm.bean.WorkOrderBean;
import static com.uptime.upcastcm.utils.enums.ACSensorTypeEnum.getACSensorTypeItemList;
import static com.uptime.upcastcm.utils.enums.DCSensorTypeEnum.getDCSensorTypeItemList;
import com.uptime.upcastcm.utils.vo.ApAlSetVO;
import com.uptime.upcastcm.utils.vo.AssetTypesVO;
import com.uptime.upcastcm.utils.vo.DevicePresetVO;
import com.uptime.upcastcm.utils.vo.FaultFrequenciesVO;
import com.uptime.upcastcm.utils.vo.PointLocationNamesVO;
import com.uptime.upcastcm.utils.vo.TachometerVO;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.faces.model.SelectItemGroup;

/**
 *
 * @author gsingh
 */
public class Utils {

    public static List<SelectItem> groupTachometerList(List<TachometerVO> tachometerVOs, String tachType) {
        if (tachometerVOs != null && !tachometerVOs.isEmpty()) {
            List<SelectItem> tachometersList = new ArrayList();
            SelectItem[] tachSIArr;
            SelectItemGroup tachGroup;
            tachometerVOs.sort(Comparator.comparing(TachometerVO::getTachName));
            tachSIArr = new SelectItem[tachometerVOs.size()];
            int i = 0;
            for (TachometerVO tvo : tachometerVOs) {
                tachSIArr[i++] = new SelectItem(tvo.getTachId(), tvo.getTachName());
            }
            tachGroup = new SelectItemGroup(tachType);
            tachGroup.setSelectItems(tachSIArr);
            tachometersList.add(tachGroup);
            return tachometersList;
        }
        return null;
    }

    public static List<SelectItem> groupPointLocationList(List<PointLocationNamesVO> pointLocationNamesVOList, String ptLoType) {
        if (pointLocationNamesVOList != null && !pointLocationNamesVOList.isEmpty()) {
            List<SelectItem> pointLocationNamesList = new ArrayList();
            SelectItem[] ptLoSIArr;
            SelectItemGroup ptLoGroup;
            pointLocationNamesVOList.sort(Comparator.comparing(PointLocationNamesVO::getPointLocationName));
            ptLoSIArr = new SelectItem[pointLocationNamesVOList.size()];
            int i = 0;
            for (PointLocationNamesVO tvo : pointLocationNamesVOList) {
                ptLoSIArr[i++] = new SelectItem(tvo.getPointLocationName(), tvo.getPointLocationName());
            }
            ptLoGroup = new SelectItemGroup(ptLoType);
            ptLoGroup.setSelectItems(ptLoSIArr);
            pointLocationNamesList.add(ptLoGroup);
            return pointLocationNamesList;
        }
        return null;
    }

    public static List<SelectItem> groupFaultFrequencyList(List<FaultFrequenciesVO> ffVOs, String ffType) {
        if (ffVOs != null && !ffVOs.isEmpty()) {
            List<SelectItem> ffList = new ArrayList();
            SelectItem[] ffSIArr;
            SelectItemGroup ffGroup;
            ffVOs.sort(Comparator.comparing(FaultFrequenciesVO::getFfSetName));
            ffSIArr = new SelectItem[ffVOs.size()];
            int i = 0;
            for (FaultFrequenciesVO ffvo : ffVOs) {
                ffSIArr[i++] = new SelectItem(ffvo.getFfSetId().toString(), ffvo.getFfSetName());
            }
            ffGroup = new SelectItemGroup(ffType);
            ffGroup.setSelectItems(ffSIArr);
            ffList.add(ffGroup);
            return ffList;
        }
        return null;
    }

    public static List<SelectItem> groupFaultFrequencySetNameList(List<FaultFrequenciesVO> ffVOs, String ffType) {
        if (ffVOs != null && !ffVOs.isEmpty()) {
            List<SelectItem> ffList = new ArrayList();
            SelectItem[] ffSIArr;
            SelectItemGroup ffGroup;
            ffVOs.sort(Comparator.comparing(FaultFrequenciesVO::getFfSetName));
            ffSIArr = new SelectItem[ffVOs.size()];
            int i = 0;
            for (FaultFrequenciesVO ffvo : ffVOs) {
                ffSIArr[i++] = new SelectItem(ffvo.getFfSetName(), ffvo.getFfSetName());
            }
            ffGroup = new SelectItemGroup(ffType);
            ffGroup.setSelectItems(ffSIArr);
            ffList.add(ffGroup);
            return ffList;
        }
        return null;
    }

    public static List<SelectItem> groupApSetList(List<ApAlSetVO> apAlSetVOs, String apSetType) {
        if (apAlSetVOs != null && !apAlSetVOs.isEmpty()) {
            List<SelectItem> apSetList = new ArrayList();
            SelectItem[] apSetArr;
            SelectItemGroup apSetGroup;
            apAlSetVOs.sort(Comparator.comparing(ApAlSetVO::getApSetName));
            apSetArr = new SelectItem[apAlSetVOs.size()];
            int i = 0;
            for (ApAlSetVO ffvo : apAlSetVOs) {
                apSetArr[i++] = new SelectItem(ffvo.getApSetId(), ffvo.getApSetName());
            }
            apSetGroup = new SelectItemGroup(apSetType);
            apSetGroup.setSelectItems(apSetArr);
            apSetList.add(apSetGroup);
            return apSetList;
        }
        return null;
    }

    public static List<SelectItem> groupAlSetList(List<ApAlSetVO> apAlSetVOs, String apSetType) {
        if (apAlSetVOs != null && !apAlSetVOs.isEmpty()) {
            List<SelectItem> apSetList = new ArrayList();
            SelectItem[] apSetArr;
            SelectItemGroup apSetGroup;
            apAlSetVOs.sort(Comparator.comparing(ApAlSetVO::getApSetName));
            apSetArr = new SelectItem[apAlSetVOs.size()];
            int i = 0;
            for (ApAlSetVO ffvo : apAlSetVOs) {
                apSetArr[i++] = new SelectItem(ffvo.getAlSetId(), ffvo.getAlSetName());
            }
            apSetGroup = new SelectItemGroup(apSetType);
            apSetGroup.setSelectItems(apSetArr);
            apSetList.add(apSetGroup);
            return apSetList;
        }
        return null;
    }

    public static List<SelectItem> groupDeviceTemplateList(List<DevicePresetVO> devicePresetVOs, String type) {
        if (devicePresetVOs != null && !devicePresetVOs.isEmpty()) {
            List<SelectItem> apSetList = new ArrayList();
            SelectItem[] deviceTemplateSetArr;
            SelectItemGroup dtSetGroup;
            devicePresetVOs.sort(Comparator.comparing(DevicePresetVO::getName));
            deviceTemplateSetArr = new SelectItem[devicePresetVOs.size()];
            int i = 0;
            for (DevicePresetVO ffvo : devicePresetVOs) {
                deviceTemplateSetArr[i++] = new SelectItem(ffvo.getPresetId(), ffvo.getName());
            }
            dtSetGroup = new SelectItemGroup(type);
            dtSetGroup.setSelectItems(deviceTemplateSetArr);
            apSetList.add(dtSetGroup);
            return apSetList;
        }
        return null;
    }

    public static <T> Predicate<T> distinctByKeys(final Function<? super T, ?>... keyExtractors) {
        final Map<List<?>, Boolean> seen;

        seen = new ConcurrentHashMap<>();
        return t -> {
            final List<?> keys;

            keys = Arrays.stream(keyExtractors).map(ke -> ke.apply(t)).collect(Collectors.toList());
            return seen.putIfAbsent(keys, Boolean.TRUE) == null;
        };
    }

    public static List<TachometerVO> getUniqueTachometerList(List<TachometerVO> tachometerVOs) {
        if (tachometerVOs != null && !tachometerVOs.isEmpty()) {
            tachometerVOs = tachometerVOs.stream()
                    .filter(distinctByKeys(TachometerVO::getCustomerAccount, TachometerVO::getTachId))
                    .collect(Collectors.toList());
        }
        return tachometerVOs;
    }

    public static List<PointLocationNamesVO> getUniquePointLocationNameList(List<PointLocationNamesVO> pointLocationNamesVOs) {
        if (pointLocationNamesVOs != null && !pointLocationNamesVOs.isEmpty()) {
            pointLocationNamesVOs = pointLocationNamesVOs.stream()
                    .filter(distinctByKeys(PointLocationNamesVO::getCustomerAccount, PointLocationNamesVO::getPointLocationName))
                    .collect(Collectors.toList());
        }
        return pointLocationNamesVOs;
    }

    public static List<FaultFrequenciesVO> getUniqueFFList(List<FaultFrequenciesVO> ffList) {
        if (ffList != null && !ffList.isEmpty()) {
            ffList = ffList.stream()
                    .filter(distinctByKeys(FaultFrequenciesVO::getCustomerAccount, FaultFrequenciesVO::getFfSetId))
                    .collect(Collectors.toList());
        }
        return ffList;
    }

    public static List<ApAlSetVO> getUniqueApAlSetList(List<ApAlSetVO> apAlSetList) {
        if (apAlSetList != null && !apAlSetList.isEmpty()) {
            apAlSetList = apAlSetList.stream()
                    .filter(distinctByKeys(ApAlSetVO::getCustomerAccount, ApAlSetVO::getSiteId, ApAlSetVO::getSensorType, ApAlSetVO::getApSetId))
                    .collect(Collectors.toList());
        }
        return apAlSetList;
    }

    public static List<SelectItem> groupAlSetList(List<ApAlSetVO> apAlSetVOs) {
        if (apAlSetVOs != null && !apAlSetVOs.isEmpty()) {
            List<SelectItem> apSetList = new ArrayList();
            apAlSetVOs.sort(Comparator.comparing(ApAlSetVO::getAlSetName));
            for (ApAlSetVO ffvo : apAlSetVOs) {
                apSetList.add(new SelectItem(ffvo.getAlSetId(), ffvo.getAlSetName()));
            }
            return apSetList;
        }
        return new ArrayList<>();
    }

    public static String getPointTypeBySensorType(String sensorType) {
        String pointType = null;

        if (sensorType != null && !sensorType.isEmpty()) {
            if (getACSensorTypeItemList().stream().filter(acSensorType -> acSensorType.getValue().toString().equalsIgnoreCase(sensorType)).findAny().isPresent()) {
                pointType = "AC";
            } else if (getDCSensorTypeItemList().stream().filter(acSensorType -> acSensorType.getValue().toString().equalsIgnoreCase(sensorType)).findAny().isPresent()) {
                pointType = "DC";
            }
        }
        return pointType;
    }

    public static List<String> validateParamList() {
        List<String> paramList = new ArrayList();
        paramList.add("Tach Speed");
        paramList.add("Total Non Sync Energy");
        paramList.add("Total Sub Sync Energy");
        return paramList;
    }
    
    public static List<SelectItem> groupSensorTypeList(List<SelectItem> SelectItems, String type) {
        if (SelectItems != null && !SelectItems.isEmpty()) {
            List<SelectItem> selectItemList = new ArrayList();
            SelectItem[] itemsArr;
            SelectItemGroup itemsGroup;            
            itemsArr = new SelectItem[SelectItems.size()];
            int i = 0;
            for (SelectItem item : SelectItems) {
                itemsArr[i++] = new SelectItem(item.getValue(),item.getLabel());
            }
            itemsGroup = new SelectItemGroup(type);
            itemsGroup.setSelectItems(itemsArr);
            selectItemList.add(itemsGroup);
            return selectItemList;
        }
        return null;
    }
}
