/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.utils.helperclass;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.Serializable;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author twilcox
 */
public class DefaultUnits implements Serializable {
    private String accelerationWaveform;
    private String accelerationSpectrum;
    private String accelerationAmpFactor;
    private boolean accelerationlogYaxis;
    private String velocityWaveform;
    private String velocitySpectrum;
    private String velocityAmpFactor;
    private boolean velocitylogYaxis;
    private String displacementWaveform;
    private String displacementSpectrum;
    private String displacementAmpFactor;
    private boolean displacementlogYaxis;
    private String ultrasonicWaveform;
    private String ultrasonicSpectrum;
    private String ultrasonicAmpFactor;
    private boolean ultrasoniclogYaxis=true;
    private String timeUnit;
    private String frequencyUnit;
    private String temperature;
    private String frequency;
    private String pressure;
    private String voltage;
    private String current;
    private String phase;
    private String count;
    private String relativeHumidity;
    private String temperatureRTD;
    

    /**
     * Constructor
     */
    public DefaultUnits() {
        
    }
    
    /**
     * Parameterized Constructor
     * @param defaultUnits, DefaultUnits Object
     */
    public DefaultUnits(DefaultUnits defaultUnits) {
        accelerationWaveform = defaultUnits.getAccelerationWaveform();
        accelerationSpectrum = defaultUnits.getAccelerationSpectrum();
        accelerationAmpFactor =defaultUnits.getAccelerationAmpFactor();
        accelerationlogYaxis =defaultUnits.isAccelerationlogYaxis();
        velocityWaveform = defaultUnits.getVelocityWaveform();
        velocitySpectrum = defaultUnits.getVelocitySpectrum();
        velocityAmpFactor =defaultUnits.getVelocityAmpFactor();
        velocitylogYaxis =defaultUnits.isVelocitylogYaxis();
        displacementWaveform = defaultUnits.getDisplacementWaveform();
        displacementSpectrum = defaultUnits.getDisplacementSpectrum();
        displacementAmpFactor = defaultUnits.getDisplacementAmpFactor();
        displacementlogYaxis =defaultUnits.isDisplacementlogYaxis();
        ultrasonicWaveform = defaultUnits.getUltrasonicWaveform();
        ultrasonicSpectrum = defaultUnits.getUltrasonicSpectrum();
        ultrasonicAmpFactor = defaultUnits.getUltrasonicAmpFactor();
        ultrasoniclogYaxis =defaultUnits.isUltrasoniclogYaxis();
        timeUnit = defaultUnits.getTimeUnit();
        frequencyUnit =defaultUnits.getFrequencyUnit();
        temperature = defaultUnits.getTemperature();
        frequency = defaultUnits.getFrequency();
        pressure = defaultUnits.getPressure();
        voltage = defaultUnits.getVoltage();
        current = defaultUnits.getCurrent();
        phase = defaultUnits.getPhase();
        count = defaultUnits.getCount();
        relativeHumidity = defaultUnits.getRelativeHumidity();
        temperatureRTD = defaultUnits.getTemperatureRTD();
    }
    
    /**
     * Parameterized Constructor
     * @param json, String Object
     */
    public DefaultUnits(String json) {
        JsonObject jsonObject, innerJsonObject;
        JsonElement element;
        
        try {
            if (json != null && !json.isEmpty() && (element = JsonParser.parseString(json)) != null && (jsonObject = element.getAsJsonObject()) != null) {
                if (jsonObject.has("AC Units") && (innerJsonObject = jsonObject.getAsJsonObject("AC Units")) != null) {
                    if (innerJsonObject.has("acceleration_waveform")) {
                        try {
                            accelerationWaveform = innerJsonObject.get("acceleration_waveform").getAsString();
                        } catch (Exception ex) {
                            accelerationWaveform = null;
                        }
                    }
                    if (innerJsonObject.has("acceleration_spectrum")) {
                        try {
                            accelerationSpectrum = innerJsonObject.get("acceleration_spectrum").getAsString();
                        } catch (Exception ex) {
                            accelerationSpectrum = null;
                        }
                    }
                     if (innerJsonObject.has("acceleration_amp_factor")) {
                        try {
                            accelerationAmpFactor = innerJsonObject.get("acceleration_amp_factor").getAsString();
                        } catch (Exception ex) {
                            accelerationAmpFactor = null;
                        }
                    }
                      if (innerJsonObject.has("acceleration_log_y_axis")) {
                        try {
                            accelerationlogYaxis = innerJsonObject.get("acceleration_log_y_axis").getAsBoolean(); 
                        } catch (Exception ex) {
                            accelerationlogYaxis = false;
                        }
                    }
                    if (innerJsonObject.has("velocity_waveform")) {
                        try {
                            velocityWaveform = innerJsonObject.get("velocity_waveform").getAsString();
                        } catch (Exception ex) {
                            velocityWaveform = null;
                        }
                    }
                    if (innerJsonObject.has("velocity_spectrum")) {
                        try {
                            velocitySpectrum = innerJsonObject.get("velocity_spectrum").getAsString();
                        } catch (Exception ex) {
                            velocitySpectrum = null;
                        }
                    }
                    if (innerJsonObject.has("velocity_amp_factor")) {
                        try {
                            velocityAmpFactor = innerJsonObject.get("velocity_amp_factor").getAsString();
                        } catch (Exception ex) {
                            velocityAmpFactor = null;
                        }
                    }
                    if (innerJsonObject.has("velocity_log_y_axis")) {
                        try {
                            velocitylogYaxis = innerJsonObject.get("velocity_log_y_axis").getAsBoolean();
                        } catch (Exception ex) {
                            velocitylogYaxis = false;
                        }
                    }
                    if (innerJsonObject.has("displacement_waveform")) {
                        try {
                            displacementWaveform = innerJsonObject.get("displacement_waveform").getAsString();
                        } catch (Exception ex) {
                            displacementWaveform = null;
                        }
                    }
                    if (innerJsonObject.has("displacement_spectrum")) {
                        try {
                            displacementSpectrum = innerJsonObject.get("displacement_spectrum").getAsString();
                        } catch (Exception ex) {
                            displacementSpectrum = null;
                        }
                    }
                    if (innerJsonObject.has("displacement_amp_factor")) {
                        try {
                            displacementAmpFactor = innerJsonObject.get("displacement_amp_factor").getAsString();
                        } catch (Exception ex) {
                            displacementAmpFactor = null;
                        }
                    }
                    if (innerJsonObject.has("displacement_log_y_axis")) {
                        try {
                            displacementlogYaxis = innerJsonObject.get("displacement_log_y_axis").getAsBoolean();
                        } catch (Exception ex) {
                            displacementlogYaxis = false;
                        }
                    }
                    if (innerJsonObject.has("ultrasonic_waveform")) {
                        try {
                            ultrasonicWaveform = innerJsonObject.get("ultrasonic_waveform").getAsString();
                        } catch (Exception ex) {
                            ultrasonicWaveform = null;
                        }
                    }
                    if (innerJsonObject.has("ultrasonic_spectrum")) {
                        try {
                            ultrasonicSpectrum = innerJsonObject.get("ultrasonic_spectrum").getAsString();
                        } catch (Exception ex) {
                            ultrasonicSpectrum = null;
                        }
                    }
                    if (innerJsonObject.has("ultrasonic_amp_factor")) {
                        try {
                            ultrasonicAmpFactor = innerJsonObject.get("ultrasonic_amp_factor").getAsString();
                        } catch (Exception ex) {
                            ultrasonicAmpFactor = null;
                        }
                    }
                    if (innerJsonObject.has("ultrasonic_log_y_axis")) {
                        try {
                            ultrasoniclogYaxis = innerJsonObject.get("ultrasonic_log_y_axis").getAsBoolean();
                        } catch (Exception ex) {
                            ultrasoniclogYaxis = false;
                        }
                    }
                    if (innerJsonObject.has("time_unit")) {
                        try {
                            timeUnit = innerJsonObject.get("time_unit").getAsString();
                        } catch (Exception ex) {
                            timeUnit = null;
                        }
                    }
                    if (innerJsonObject.has("frequency_unit")) {
                        try {
                            frequencyUnit = innerJsonObject.get("frequency_unit").getAsString();
                        } catch (Exception ex) {
                            frequencyUnit = null;
                        }
                    }
                }
                
                if (jsonObject.has("DC Units") && (innerJsonObject = jsonObject.getAsJsonObject("DC Units")) != null) {
                    if (innerJsonObject.has("temperature")) {
                        try {
                            temperature = innerJsonObject.get("temperature").getAsString();
                        } catch (Exception ex) {
                            temperature = null;
                        }
                    }
                    if (innerJsonObject.has("frequency")) {
                        try {
                            frequency = innerJsonObject.get("frequency").getAsString();
                        } catch (Exception ex) {
                            frequency = null;
                        }
                    }
                    if (innerJsonObject.has("pressure")) {
                        try {
                            pressure = innerJsonObject.get("pressure").getAsString();
                        } catch (Exception ex) {
                            pressure = null;
                        }
                    }
                    if (innerJsonObject.has("voltage")) {
                        try {
                            voltage = innerJsonObject.get("voltage").getAsString();
                        } catch (Exception ex) {
                            voltage = null;
                        }
                    }
                    if (innerJsonObject.has("current")) {
                        try {
                            current = innerJsonObject.get("current").getAsString();
                        } catch (Exception ex) {
                            current = null;
                        }
                    }
                    if (innerJsonObject.has("phase")) {
                        try {
                            phase = innerJsonObject.get("phase").getAsString();
                        } catch (Exception ex) {
                            phase = null;
                        }
                    }
                    if (innerJsonObject.has("count")) {
                        try {
                            count = innerJsonObject.get("count").getAsString();
                        } catch (Exception ex) {
                            count = null;
                        }
                    }
                    if (innerJsonObject.has("relative_humidity")) {
                        try {
                            relativeHumidity = innerJsonObject.get("relative_humidity").getAsString();
                        } catch (Exception ex) {
                            relativeHumidity = null;
                        }
                    }
                    if (innerJsonObject.has("temperature_rtd")) {
                        try {
                            temperatureRTD = innerJsonObject.get("temperature_rtd").getAsString();
                        } catch (Exception ex) {
                            temperatureRTD = null;
                        }
                    }
                }
            }
        } catch (Exception e) {
            Logger.getLogger(DefaultUnits.class.getName()).log(Level.WARNING, e.getMessage(), e);
        }
    }
    
    /**
     * Return a String json based on the fields of this class
     * @return String
     */
    public String getAsJsonString() {
        StringBuilder json;
        
        json = new StringBuilder();
        json.append("{\"AC Units\":{");
        if (accelerationWaveform != null && !accelerationWaveform.isEmpty()) {
            json.append("\"acceleration_waveform\":\"").append(accelerationWaveform).append("\",");
        }
        if (accelerationSpectrum != null && !accelerationSpectrum.isEmpty()) {
            json.append("\"acceleration_spectrum\":\"").append(accelerationSpectrum).append("\",");
        }
        if (accelerationAmpFactor != null && !accelerationAmpFactor.isEmpty()) {
            json.append("\"acceleration_amp_factor\":\"").append(accelerationAmpFactor).append("\",");
        }
        
        json.append("\"acceleration_log_y_axis\":\"").append(accelerationlogYaxis).append("\",");
        
        if (velocityWaveform != null && !velocityWaveform.isEmpty()) {
            json.append("\"velocity_waveform\":\"").append(velocityWaveform).append("\",");
        }
        if (velocitySpectrum != null && !velocitySpectrum.isEmpty()) {
            json.append("\"velocity_spectrum\":\"").append(velocitySpectrum).append("\",");
        }
        if (velocityAmpFactor != null && !velocityAmpFactor.isEmpty()) {
            json.append("\"velocity_amp_factor\":\"").append(velocityAmpFactor).append("\",");
        }
            json.append("\"velocity_log_y_axis\":\"").append(velocitylogYaxis).append("\",");

        if (displacementWaveform != null && !displacementWaveform.isEmpty()) {
            json.append("\"displacement_waveform\":\"").append(displacementWaveform).append("\",");
        }
        if (displacementSpectrum != null && !displacementSpectrum.isEmpty()) {
            json.append("\"displacement_spectrum\":\"").append(displacementSpectrum).append("\",");
        }
        if (displacementAmpFactor != null && !displacementAmpFactor.isEmpty()) {
            json.append("\"displacement_amp_factor\":\"").append(displacementAmpFactor).append("\",");
        }
            json.append("\"displacement_log_y_axis\":\"").append(displacementlogYaxis).append("\",");

        if (ultrasonicWaveform != null && !ultrasonicWaveform.isEmpty()) {
            json.append("\"ultrasonic_waveform\":\"").append(ultrasonicWaveform).append("\",");
        }
        if (ultrasonicSpectrum != null && !ultrasonicSpectrum.isEmpty()) {
            json.append("\"ultrasonic_spectrum\":\"").append(ultrasonicSpectrum).append("\",");
        }
        if (ultrasonicAmpFactor != null && !ultrasonicAmpFactor.isEmpty()) {
            json.append("\"ultrasonic_amp_factor\":\"").append(ultrasonicAmpFactor).append("\",");
        }
            json.append("\"ultrasonic_log_y_axis\":\"").append(ultrasoniclogYaxis).append("\",");

        if (timeUnit != null && !frequencyUnit.isEmpty()) {
            json.append("\"time_unit\":\"").append(timeUnit).append("\",");
        }
        if (frequencyUnit != null && !frequencyUnit.isEmpty()) {
            json.append("\"frequency_unit\":\"").append(frequencyUnit).append("\",");
        }
        if (json.toString().endsWith(",")) {
            json.deleteCharAt(json.length() - 1);
        }
        json.append("},\"DC Units\":{");
        if (temperature != null && !temperature.isEmpty()) {
            json.append("\"temperature\":\"").append(temperature).append("\",");
        }
        if (frequency != null && !frequency.isEmpty()) {
            json.append("\"frequency\":\"").append(frequency).append("\",");
        }
        if (pressure != null && !pressure.isEmpty()) {
            json.append("\"pressure\":\"").append(pressure).append("\",");
        }
        if (voltage != null && !voltage.isEmpty()) {
            json.append("\"voltage\":\"").append(voltage).append("\",");
        }
        if (current != null && !current.isEmpty()) {
            json.append("\"current\":\"").append(current).append("\",");
        }
        if (phase != null && !phase.isEmpty()) {
            json.append("\"phase\":\"").append(phase).append("\",");
        }
        if (count != null && !count.isEmpty()) {
            json.append("\"count\":\"").append(count).append("\",");
        }
        if (relativeHumidity != null && !relativeHumidity.isEmpty()) {
            json.append("\"relative_humidity\":\"").append(relativeHumidity).append("\",");
        }
        if (temperatureRTD != null && !temperatureRTD.isEmpty()) {
            json.append("\"temperature_rtd\":\"").append(temperatureRTD).append("\",");
        }
        if (json.toString().endsWith(",")) {
            json.deleteCharAt(json.length() - 1);
        }
        json.append("}}");
        
        return json.toString();
    }

    public String getAccelerationWaveform() {
        return accelerationWaveform;
    }

    public void setAccelerationWaveform(String accelerationWaveform) {
        this.accelerationWaveform = accelerationWaveform;
    }

    public String getAccelerationSpectrum() {
        return accelerationSpectrum;
    }

    public void setAccelerationSpectrum(String accelerationSpectrum) {
        this.accelerationSpectrum = accelerationSpectrum;
    }

    public String getVelocityWaveform() {
        return velocityWaveform;
    }

    public void setVelocityWaveform(String velocityWaveform) {
        this.velocityWaveform = velocityWaveform;
    }

    public String getVelocitySpectrum() {
        return velocitySpectrum;
    }

    public void setVelocitySpectrum(String velocitySpectrum) {
        this.velocitySpectrum = velocitySpectrum;
    }

    public String getDisplacementWaveform() {
        return displacementWaveform;
    }

    public void setDisplacementWaveform(String displacementWaveform) {
        this.displacementWaveform = displacementWaveform;
    }

    public String getDisplacementSpectrum() {
        return displacementSpectrum;
    }

    public void setDisplacementSpectrum(String displacementSpectrum) {
        this.displacementSpectrum = displacementSpectrum;
    }

    public String getUltrasonicWaveform() {
        return ultrasonicWaveform;
    }

    public void setUltrasonicWaveform(String ultrasonicWaveform) {
        this.ultrasonicWaveform = ultrasonicWaveform;
    }

    public String getUltrasonicSpectrum() {
        return ultrasonicSpectrum;
    }

    public void setUltrasonicSpectrum(String ultrasonicSpectrum) {
        this.ultrasonicSpectrum = ultrasonicSpectrum;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    public String getPressure() {
        return pressure;
    }

    public void setPressure(String pressure) {
        this.pressure = pressure;
    }

    public String getVoltage() {
        return voltage;
    }

    public void setVoltage(String voltage) {
        this.voltage = voltage;
    }

    public String getCurrent() {
        return current;
    }

    public void setCurrent(String current) {
        this.current = current;
    }

    public String getPhase() {
        return phase;
    }

    public void setPhase(String phase) {
        this.phase = phase;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getRelativeHumidity() {
        return relativeHumidity;
    }

    public void setRelativeHumidity(String relativeHumidity) {
        this.relativeHumidity = relativeHumidity;
    }

    public String getTemperatureRTD() {
        return temperatureRTD;
    }

    public void setTemperatureRTD(String temperatureRTD) {
        this.temperatureRTD = temperatureRTD;
    }

    public String getAccelerationAmpFactor() {
        return accelerationAmpFactor;
    }

    public void setAccelerationAmpFactor(String accelerationAmpFactor) {
        this.accelerationAmpFactor = accelerationAmpFactor;
    }

    public String getVelocityAmpFactor() {
        return velocityAmpFactor;
    }

    public void setVelocityAmpFactor(String velocityAmpFactor) {
        this.velocityAmpFactor = velocityAmpFactor;
    }

    public String getDisplacementAmpFactor() {
        return displacementAmpFactor;
    }

    public void setDisplacementAmpFactor(String displacementAmpFactor) {
        this.displacementAmpFactor = displacementAmpFactor;
    }

    public String getUltrasonicAmpFactor() {
        return ultrasonicAmpFactor;
    }

    public void setUltrasonicAmpFactor(String ultrasonicAmpFactor) {
        this.ultrasonicAmpFactor = ultrasonicAmpFactor;
    }

    public String getTimeUnit() {
        return timeUnit;
    }

    public void setTimeUnit(String timeUnit) {
        this.timeUnit = timeUnit;
    }

    public String getFrequencyUnit() {
        return frequencyUnit;
    }

    public void setFrequencyUnit(String frequencyUnit) {
        this.frequencyUnit = frequencyUnit;
    }

    public boolean isAccelerationlogYaxis() {
        return accelerationlogYaxis;
    }

    public void setAccelerationlogYaxis(boolean accelerationlogYaxis) {
        this.accelerationlogYaxis = accelerationlogYaxis;
    }

    public boolean isVelocitylogYaxis() {
        return velocitylogYaxis;
    }

    public void setVelocitylogYaxis(boolean velocitylogYaxis) {
        this.velocitylogYaxis = velocitylogYaxis;
    }

    public boolean isDisplacementlogYaxis() {
        return displacementlogYaxis;
    }

    public void setDisplacementlogYaxis(boolean displacementlogYaxis) {
        this.displacementlogYaxis = displacementlogYaxis;
    }

    public boolean isUltrasoniclogYaxis() {
        return ultrasoniclogYaxis;
    }

    public void setUltrasoniclogYaxis(boolean ultrasoniclogYaxis) {
        this.ultrasoniclogYaxis = ultrasoniclogYaxis;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.accelerationWaveform);
        hash = 97 * hash + Objects.hashCode(this.accelerationSpectrum);
        hash = 97 * hash + Objects.hashCode(this.accelerationAmpFactor);
        hash = 97 * hash + (this.accelerationlogYaxis ? 1 : 0);
        hash = 97 * hash + Objects.hashCode(this.velocityWaveform);
        hash = 97 * hash + Objects.hashCode(this.velocitySpectrum);
        hash = 97 * hash + Objects.hashCode(this.velocityAmpFactor);
        hash = 97 * hash + (this.velocitylogYaxis ? 1 : 0);
        hash = 97 * hash + Objects.hashCode(this.displacementWaveform);
        hash = 97 * hash + Objects.hashCode(this.displacementSpectrum);
        hash = 97 * hash + Objects.hashCode(this.displacementAmpFactor);
        hash = 97 * hash + (this.displacementlogYaxis ? 1 : 0);
        hash = 97 * hash + Objects.hashCode(this.ultrasonicWaveform);
        hash = 97 * hash + Objects.hashCode(this.ultrasonicSpectrum);
        hash = 97 * hash + Objects.hashCode(this.ultrasonicAmpFactor);
        hash = 97 * hash + (this.ultrasoniclogYaxis ? 1 : 0);
        hash = 97 * hash + Objects.hashCode(this.timeUnit);
        hash = 97 * hash + Objects.hashCode(this.frequencyUnit);
        hash = 97 * hash + Objects.hashCode(this.temperature);
        hash = 97 * hash + Objects.hashCode(this.frequency);
        hash = 97 * hash + Objects.hashCode(this.pressure);
        hash = 97 * hash + Objects.hashCode(this.voltage);
        hash = 97 * hash + Objects.hashCode(this.current);
        hash = 97 * hash + Objects.hashCode(this.phase);
        hash = 97 * hash + Objects.hashCode(this.count);
        hash = 97 * hash + Objects.hashCode(this.relativeHumidity);
        hash = 97 * hash + Objects.hashCode(this.temperatureRTD);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DefaultUnits other = (DefaultUnits) obj;
        if (this.accelerationlogYaxis != other.accelerationlogYaxis) {
            return false;
        }
        if (this.velocitylogYaxis != other.velocitylogYaxis) {
            return false;
        }
        if (this.displacementlogYaxis != other.displacementlogYaxis) {
            return false;
        }
        if (this.ultrasoniclogYaxis != other.ultrasoniclogYaxis) {
            return false;
        }
        if (!Objects.equals(this.accelerationWaveform, other.accelerationWaveform)) {
            return false;
        }
        if (!Objects.equals(this.accelerationSpectrum, other.accelerationSpectrum)) {
            return false;
        }
        if (!Objects.equals(this.accelerationAmpFactor, other.accelerationAmpFactor)) {
            return false;
        }
        if (!Objects.equals(this.velocityWaveform, other.velocityWaveform)) {
            return false;
        }
        if (!Objects.equals(this.velocitySpectrum, other.velocitySpectrum)) {
            return false;
        }
        if (!Objects.equals(this.velocityAmpFactor, other.velocityAmpFactor)) {
            return false;
        }
        if (!Objects.equals(this.displacementWaveform, other.displacementWaveform)) {
            return false;
        }
        if (!Objects.equals(this.displacementSpectrum, other.displacementSpectrum)) {
            return false;
        }
        if (!Objects.equals(this.displacementAmpFactor, other.displacementAmpFactor)) {
            return false;
        }
        if (!Objects.equals(this.ultrasonicWaveform, other.ultrasonicWaveform)) {
            return false;
        }
        if (!Objects.equals(this.ultrasonicSpectrum, other.ultrasonicSpectrum)) {
            return false;
        }
        if (!Objects.equals(this.ultrasonicAmpFactor, other.ultrasonicAmpFactor)) {
            return false;
        }
        if (!Objects.equals(this.timeUnit, other.timeUnit)) {
            return false;
        }
        if (!Objects.equals(this.frequencyUnit, other.frequencyUnit)) {
            return false;
        }
        if (!Objects.equals(this.temperature, other.temperature)) {
            return false;
        }
        if (!Objects.equals(this.frequency, other.frequency)) {
            return false;
        }
        if (!Objects.equals(this.pressure, other.pressure)) {
            return false;
        }
        if (!Objects.equals(this.voltage, other.voltage)) {
            return false;
        }
        if (!Objects.equals(this.current, other.current)) {
            return false;
        }
        if (!Objects.equals(this.phase, other.phase)) {
            return false;
        }
        if (!Objects.equals(this.count, other.count)) {
            return false;
        }
        if (!Objects.equals(this.relativeHumidity, other.relativeHumidity)) {
            return false;
        }
        return Objects.equals(this.temperatureRTD, other.temperatureRTD);
    }

    @Override
    public String toString() {
        return "DefaultUnits{" + "accelerationWaveform=" + accelerationWaveform + ", accelerationSpectrum=" + accelerationSpectrum + ", accelerationAmpFactor=" + accelerationAmpFactor + ", accelerationlogYaxis=" + accelerationlogYaxis + ", velocityWaveform=" + velocityWaveform + ", velocitySpectrum=" + velocitySpectrum + ", velocityAmpFactor=" + velocityAmpFactor + ", velocitylogYaxis=" + velocitylogYaxis + ", displacementWaveform=" + displacementWaveform + ", displacementSpectrum=" + displacementSpectrum + ", displacementAmpFactor=" + displacementAmpFactor + ", displacementlogYaxis=" + displacementlogYaxis + ", ultrasonicWaveform=" + ultrasonicWaveform + ", ultrasonicSpectrum=" + ultrasonicSpectrum + ", ultrasonicAmpFactor=" + ultrasonicAmpFactor + ", ultrasoniclogYaxis=" + ultrasoniclogYaxis + ", timeUnit=" + timeUnit + ", frequencyUnit=" + frequencyUnit + ", temperature=" + temperature + ", frequency=" + frequency + ", pressure=" + pressure + ", voltage=" + voltage + ", current=" + current + ", phase=" + phase + ", count=" + count + ", relativeHumidity=" + relativeHumidity + ", temperatureRTD=" + temperatureRTD + '}';
    }
    
   
   
}
