/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.utils.helperclass;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.bean.CreatePointLocationBean;
import com.uptime.upcastcm.bean.UserManageBean;
import com.uptime.upcastcm.http.client.APALSetClient;
import com.uptime.upcastcm.http.client.AssetPresetClient;
import com.uptime.upcastcm.http.client.FaultFrequenciesClient;
import com.uptime.upcastcm.http.client.PointLocationNamesClient;
import com.uptime.upcastcm.http.client.TachometerClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.DEFAULT_UPTIME_ACCOUNT;
import static com.uptime.upcastcm.utils.ApplicationConstants.PRESET_SERVICE_NAME;
import static com.uptime.upcastcm.utils.helperclass.Utils.distinctByKeys;
import com.uptime.upcastcm.utils.vo.ApAlSetVO;
import com.uptime.upcastcm.utils.vo.AssetPresetVO;
import com.uptime.upcastcm.utils.vo.DevicePresetVO;
import com.uptime.upcastcm.utils.vo.FaultFrequenciesVO;
import com.uptime.upcastcm.utils.vo.PointLocationNamesVO;
import com.uptime.upcastcm.utils.vo.TachometerVO;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.faces.model.SelectItem;

/**
 *
 * @author kpati
 */
public class PresetUtil {

    private final List<TachometerVO> globalTachList;
    private final List<PointLocationNamesVO> globalPtLoNamePresetList;
    private final List<TachometerVO> globalTachByDefaultCompanyList;
    private final List<TachometerVO> siteTachList;
    private final List<PointLocationNamesVO> sitePtLoNameList;
    private final List<TachometerVO> siteTachByPK;
    private final List<FaultFrequenciesVO> siteFaultFrequenciesList;
    private final List<FaultFrequenciesVO> globalFaultFrequenciesList;
    private final List<ApAlSetVO> globalApAlSetSensorList;
    private final List<ApAlSetVO> siteApAlSetSensorList;
    private final List<DevicePresetVO> devicePresetList;
    private final List<AssetPresetVO> globalAssetPresetList;
    private final List<AssetPresetVO> siteAssetPresetList;

    /**
     * Constructor
     */
    public PresetUtil() {
        globalTachList = new ArrayList();
        globalPtLoNamePresetList = new ArrayList();
        globalTachByDefaultCompanyList = new ArrayList();
        siteTachList = new ArrayList();
        sitePtLoNameList = new ArrayList();
        siteTachByPK = new ArrayList();
        siteFaultFrequenciesList = new ArrayList();
        globalFaultFrequenciesList = new ArrayList();
        globalApAlSetSensorList = new ArrayList();
        siteApAlSetSensorList = new ArrayList();
        devicePresetList = new ArrayList();
        globalAssetPresetList = new ArrayList();
        siteAssetPresetList = new ArrayList();

    }

    /**
     * Clear of presets fields
     */
    public void clearAllPresets() {
        globalTachList.clear();
        globalPtLoNamePresetList.clear();
        globalTachByDefaultCompanyList.clear();
        siteTachList.clear();
        sitePtLoNameList.clear();
        siteTachByPK.clear();
        siteFaultFrequenciesList.clear();
        globalFaultFrequenciesList.clear();
        globalApAlSetSensorList.clear();
        siteApAlSetSensorList.clear();
        devicePresetList.clear();
        globalAssetPresetList.clear();
        siteAssetPresetList.clear();
    }

    /**
     * Returns the rows from the table customer_ap_set based on the given account
     *
     * @param customerAccount, String Object
     * @param sensorType, String Object
     * @return List Object of ApALSetVO Objects
     */
    public List<ApAlSetVO> getGlobalApAlSetsByCustomerSensorType(String customerAccount, String sensorType) {
        try {
            if (globalApAlSetSensorList.isEmpty() || globalApAlSetSensorList.stream().filter(f -> f.getCustomerAccount().equalsIgnoreCase(customerAccount) && f.getSensorType().equalsIgnoreCase(sensorType)).findAny().orElse(null) == null) {
                globalApAlSetSensorList.addAll(APALSetClient.getInstance().getGlobalApAlSetsByCustomerSensorType(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), customerAccount, sensorType));
            }
        } catch (Exception e) {
            Logger.getLogger(PresetUtil.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
        return globalApAlSetSensorList;
    }

    /**
     * Returns the rows from the table customer_ap_set based on the given account
     *
     * @param customerAccount, String Object
     * @param siteId, UUID
     * @param sensorType, String Object
     * @return List Object of ApALSetVO Objects
     */
    public List<ApAlSetVO> getSiteApAlSetsByCustomerSiteIdSensorType(String customerAccount, UUID siteId, String sensorType) {
        try {
            if (siteApAlSetSensorList.isEmpty()) {
                siteApAlSetSensorList.addAll(APALSetClient.getInstance().getSiteApAlSetsByCustomerSiteIdSensorType(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), customerAccount, siteId, sensorType));
            }
        } catch (Exception e) {
            Logger.getLogger(PresetUtil.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
        return siteApAlSetSensorList;
    }

    /**
     * Returns the rows from the table customer_ap_set based on the given account
     *
     * @param customerAccount, String Object
     * @return List Object of TachometerVO Objects
     */
    public List<TachometerVO> getGlobalTachList(String customerAccount) {
        try {
            if (globalTachList.isEmpty() || globalTachList.stream().filter(f -> f.getCustomerAccount().equalsIgnoreCase(customerAccount)).findAny().orElse(null) == null) {
                globalTachList.addAll(TachometerClient.getInstance().getGlobalTachometersByCustomer(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), customerAccount));
            }
        } catch (Exception e) {
            Logger.getLogger(PresetUtil.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
        return globalTachList;
    }

    public List<PointLocationNamesVO> getGlobalptLoNameList(String customerAccount) {
        try {
            if (globalPtLoNamePresetList.isEmpty() || globalPtLoNamePresetList.stream().filter(f -> f.getCustomerAccount().equalsIgnoreCase(customerAccount)).findAny().orElse(null) == null) {
                globalPtLoNamePresetList.addAll(PointLocationNamesClient.getInstance().getGlobalPointLocationNamesByCustAcc(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), customerAccount));
            }
        } catch (Exception e) {
            Logger.getLogger(PresetUtil.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
        return globalPtLoNamePresetList;
    }

    public List<AssetPresetVO> getGlobalAssetsList(String customerAccount) {
        try {
            if (globalAssetPresetList.isEmpty() || globalAssetPresetList.stream().filter(f -> f.getCustomerAccount().equalsIgnoreCase(customerAccount)).findAny().orElse(null) == null) {
                globalAssetPresetList.addAll(AssetPresetClient.getInstance().getGlobalAssetPresetsByCustomer(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), customerAccount));
            }
        } catch (Exception e) {
            Logger.getLogger(PresetUtil.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
        return globalAssetPresetList;
    }

    public List<AssetPresetVO> getSiteAssetPresetList(String customerAccount, UUID siteId) {
        try {
            if (siteAssetPresetList.isEmpty()) {
                siteAssetPresetList.addAll(AssetPresetClient.getInstance().getSiteAssetPresetsByCustAccSiteId(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), customerAccount, siteId));
            }
        } catch (Exception e) {
            Logger.getLogger(PresetUtil.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
        return siteAssetPresetList;
    }

    /**
     * Returns the rows from the table customer_ap_set based on the given account
     *
     * @param customerAccount, String Object
     * @param siteId , UUID
     * @return List Object of TachometerVO Objects
     */
    public List<TachometerVO> getSiteTachList(String customerAccount, UUID siteId) {
        try {
            if (siteTachList.isEmpty()) {
                siteTachList.addAll(TachometerClient.getInstance().getSiteTachometersByCustomerSiteId(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), customerAccount, siteId));
            }
        } catch (Exception e) {
            Logger.getLogger(PresetUtil.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
        return siteTachList;
    }

    public List<PointLocationNamesVO> getSitePtLoNameList(String customerAccount, UUID siteId) {
        try {
            if (sitePtLoNameList.isEmpty()) {
                sitePtLoNameList.addAll(PointLocationNamesClient.getInstance().getSitePointLocationNamesByCustAccSiteId(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), customerAccount, siteId));
            }
        } catch (Exception e) {
            Logger.getLogger(PresetUtil.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
        return sitePtLoNameList;
    }

    /**
     * Returns the rows from the table customer_ap_set based on the given account
     *
     * @param customerAccount, String Object
     * @param siteId , UUID Object
     * @param tackId, UUID Object
     * @return List Object of TachometerVO Objects
     */
    public List<TachometerVO> getSiteTachByPK(String customerAccount, UUID siteId, UUID tackId) {
        try {
            if (siteTachByPK.isEmpty()) {
                siteTachByPK.addAll(TachometerClient.getInstance().getSiteTachometersByPK(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), customerAccount, siteId, tackId));
            }
        } catch (Exception e) {
            Logger.getLogger(PresetUtil.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
        return siteTachByPK;
    }

    /**
     * Returns the rows from the table customer_ap_set based on the given account
     *
     * @param customerAccount, String Object
     * @return List Object of FaultFrequenciesVO Objects
     */
    public List<FaultFrequenciesVO> getGlobalFaultFrequenciesByCustomer(String customerAccount) {
        try {
            if (globalFaultFrequenciesList.isEmpty() || globalFaultFrequenciesList.stream().filter(f -> f.getCustomerAccount().equalsIgnoreCase(customerAccount)).findAny().orElse(null) == null) {
                globalFaultFrequenciesList.addAll(FaultFrequenciesClient.getInstance().getGlobalFaultFrequenciesByCustomer(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), customerAccount));
            }
        } catch (Exception e) {
            Logger.getLogger(PresetUtil.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
        return globalFaultFrequenciesList;
    }

    /**
     * Returns the rows from the table customer_ap_set based on the given account
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @return List Object of FaultFrequenciesVO Objects
     */
    public List<FaultFrequenciesVO> getSiteFaultFrequenciesList(String customerAccount, UUID siteId) {
        try {
            if (siteFaultFrequenciesList.isEmpty()) {
                siteFaultFrequenciesList.addAll(FaultFrequenciesClient.getInstance().getSiteFaultFrequenciesByCustomerAndSiteId(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), customerAccount, siteId));
            }
        } catch (Exception e) {
            Logger.getLogger(PresetUtil.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
        return siteFaultFrequenciesList;
    }

    /**
     * Return List of Global and Site Ap Sets in a List of SelectItems
     *
     * @param userManageBean, UserManageBean Object
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param sensorType, String Object
     * @return List Object of SelectItem Objects
     */
    public List<SelectItem> populateApSetSensorTypeList(UserManageBean userManageBean, String customerAccount, UUID siteId, String sensorType) {
        List<SelectItem> customerApSetSensorTypeList = new ArrayList();
        List<ApAlSetVO> globalApSetSensorTypeList = new ArrayList();
        List<ApAlSetVO> apAlSetVOs;
        try {
            if (!userManageBean.getCurrentSite().getCustomerAccount().equals(DEFAULT_UPTIME_ACCOUNT) && (apAlSetVOs = getGlobalApAlSetsByCustomerSensorType(DEFAULT_UPTIME_ACCOUNT, sensorType)) != null && !apAlSetVOs.isEmpty()) {
                globalApSetSensorTypeList.addAll(apAlSetVOs);
            }
            if ((apAlSetVOs = getGlobalApAlSetsByCustomerSensorType(customerAccount, sensorType)) != null && !apAlSetVOs.isEmpty()) {
                globalApSetSensorTypeList.addAll(apAlSetVOs);
            }
            if (!globalApSetSensorTypeList.isEmpty()) {
                customerApSetSensorTypeList.addAll(Utils.groupApSetList(Utils.getUniqueApAlSetList(globalApSetSensorTypeList), userManageBean.getResourceBundleString("label_global_apset")));
            }
            if ((apAlSetVOs = getSiteApAlSetsByCustomerSiteIdSensorType(customerAccount, siteId, sensorType)) != null && !apAlSetVOs.isEmpty()) {
                customerApSetSensorTypeList.addAll(Utils.groupApSetList(Utils.getUniqueApAlSetList(apAlSetVOs), userManageBean.getResourceBundleString("label_site_apset")));
            }
        } catch (Exception e) {
            Logger.getLogger(PresetUtil.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
        return customerApSetSensorTypeList;
    }

    /**
     * Return List of Global and Site Ap Sets in a List of SelectItems
     *
     * @param userManageBean, UserManageBean Object
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param sensorType, String Object
     * @param excludeList, List of ApAlSetVO Objects
     * @return List Object of SelectItem Objects
     */
    public List<SelectItem> populateApSetSensorTypeList(UserManageBean userManageBean, String customerAccount, UUID siteId, String sensorType, List<ApAlSetVO> excludeList) {
        List<SelectItem> customerApSetSensorTypeList = new ArrayList();
        List<ApAlSetVO> globalApSetSensorTypeList = new ArrayList();
        List<ApAlSetVO> apAlSetVOs;
        ApAlSetVO item;
        boolean found;

        try {
            if (!userManageBean.getCurrentSite().getCustomerAccount().equals(DEFAULT_UPTIME_ACCOUNT) && (apAlSetVOs = getGlobalApAlSetsByCustomerSensorType(DEFAULT_UPTIME_ACCOUNT, sensorType)) != null && !apAlSetVOs.isEmpty()) {
                globalApSetSensorTypeList.addAll(apAlSetVOs);
            }
            if ((apAlSetVOs = getGlobalApAlSetsByCustomerSensorType(customerAccount, sensorType)) != null && !apAlSetVOs.isEmpty()) {
                globalApSetSensorTypeList.addAll(apAlSetVOs);
            }
            if (!globalApSetSensorTypeList.isEmpty()) {
                if (excludeList != null && !excludeList.isEmpty()) {
                    for (Iterator<ApAlSetVO> i = globalApSetSensorTypeList.iterator(); i.hasNext();) {
                        item = i.next();

                        found = false;
                        for (ApAlSetVO vo : excludeList) {
                            if (vo.getApSetId().equals(item.getApSetId())) {
                                found = true;
                                break;
                            }
                        }

                        if (found) {
                            i.remove();
                        }
                    }
                }

                if (!globalApSetSensorTypeList.isEmpty()) {
                    customerApSetSensorTypeList.addAll(Utils.groupApSetList(Utils.getUniqueApAlSetList(globalApSetSensorTypeList), userManageBean.getResourceBundleString("label_global_apset")));
                }
            }
            if ((apAlSetVOs = getSiteApAlSetsByCustomerSiteIdSensorType(customerAccount, siteId, sensorType)) != null && !apAlSetVOs.isEmpty()) {
                if (excludeList != null && !excludeList.isEmpty()) {
                    for (Iterator<ApAlSetVO> i = apAlSetVOs.iterator(); i.hasNext();) {
                        item = i.next();

                        found = false;
                        for (ApAlSetVO vo : excludeList) {
                            if (vo.getApSetId().equals(item.getApSetId())) {
                                found = true;
                                break;
                            }
                        }

                        if (found) {
                            i.remove();
                        }
                    }
                }

                if (!apAlSetVOs.isEmpty()) {
                    customerApSetSensorTypeList.addAll(Utils.groupApSetList(Utils.getUniqueApAlSetList(apAlSetVOs), userManageBean.getResourceBundleString("label_site_apset")));
                }
            }
        } catch (Exception e) {
            Logger.getLogger(PresetUtil.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
        return customerApSetSensorTypeList;
    }

    public List<SelectItem> populateTachometerList(UserManageBean userManageBean, String customerAccount, UUID siteId) {
        List<SelectItem> tachometerList = new ArrayList();
        List<TachometerVO> globaltachometerList = new ArrayList();
        List<TachometerVO> tachList;
        try {
            if (!userManageBean.getCurrentSite().getCustomerAccount().equals(DEFAULT_UPTIME_ACCOUNT) && (tachList = getGlobalTachList(DEFAULT_UPTIME_ACCOUNT)) != null && !tachList.isEmpty()) {
                globaltachometerList.addAll(tachList);
            }
            if ((tachList = getGlobalTachList(customerAccount)) != null && !tachList.isEmpty()) {
                globaltachometerList.addAll(tachList);
            }
            if (!globaltachometerList.isEmpty()) {
                tachometerList.addAll(Utils.groupTachometerList(Utils.getUniqueTachometerList(globaltachometerList), userManageBean.getResourceBundleString("label_global_tachometers")));
            }
            if (siteId != null && (tachList = getSiteTachList(customerAccount, siteId)) != null && !tachList.isEmpty()) {
                tachometerList.addAll(Utils.groupTachometerList(tachList, userManageBean.getResourceBundleString("label_site_tachometers")));
            }
        } catch (Exception e) {
            Logger.getLogger(PresetUtil.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
        return tachometerList;
    }

    public List<SelectItem> populatePtLoNamesList(UserManageBean userManageBean, String customerAccount, UUID siteId) {
        List<SelectItem> ptLoNamesList = new ArrayList();
        List<PointLocationNamesVO> globalPtLoNamesList = new ArrayList();
        List<PointLocationNamesVO> ptLoNamesVoList;
        try {
            if (!userManageBean.getCurrentSite().getCustomerAccount().equals(DEFAULT_UPTIME_ACCOUNT) && (ptLoNamesVoList = getGlobalptLoNameList(DEFAULT_UPTIME_ACCOUNT)) != null && !ptLoNamesVoList.isEmpty()) {
                globalPtLoNamesList.addAll(ptLoNamesVoList);
            }
            if ((ptLoNamesVoList = getGlobalptLoNameList(customerAccount)) != null && !ptLoNamesVoList.isEmpty()) {
                globalPtLoNamesList.addAll(ptLoNamesVoList);
            }
            if (!globalPtLoNamesList.isEmpty()) {
                ptLoNamesList.addAll(Utils.groupPointLocationList(Utils.getUniquePointLocationNameList(globalPtLoNamesList), userManageBean.getResourceBundleString("label_global_ptLoNames")));
            }
            if (siteId != null && (ptLoNamesVoList = getSitePtLoNameList(customerAccount, siteId)) != null && !ptLoNamesVoList.isEmpty()) {
                ptLoNamesList.addAll(Utils.groupPointLocationList(ptLoNamesVoList, userManageBean.getResourceBundleString("label_site_ptLoNames")));
            }
        } catch (Exception e) {
            Logger.getLogger(PresetUtil.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
        return ptLoNamesList;
    }

    public List<SelectItem> populateFFSetNameList(UserManageBean userManageBean, List<FaultFrequenciesVO> siteFFSetFavorites) {
        List<SelectItem> ffSetList = new ArrayList();
        List<FaultFrequenciesVO> siteFaultFavorites, siteBearingFavorites;
        try {
            // for siteBearingFavorites ff set id is null.
            siteFaultFavorites = siteFFSetFavorites.stream().filter(ff -> ff.getFfSetId() != null).filter(Utils.distinctByKeys(FaultFrequenciesVO::getFfSetId, FaultFrequenciesVO::getFfSetName)).collect(Collectors.toList());
            siteBearingFavorites = siteFFSetFavorites.stream().filter(ff -> ff.getFfSetId() == null).collect(Collectors.toList());
            siteBearingFavorites = siteBearingFavorites.stream().filter(Utils.distinctByKeys(FaultFrequenciesVO::getFfSetName)).collect(Collectors.toList());
            if (siteFaultFavorites != null && !siteFaultFavorites.isEmpty()) {
                ffSetList.addAll(Utils.groupFaultFrequencyList(siteFaultFavorites, userManageBean.getResourceBundleString("label_custom_fault_frequency_sets")));
            }
            if (siteBearingFavorites != null && !siteBearingFavorites.isEmpty()) {
                ffSetList.addAll(Utils.groupFaultFrequencySetNameList(siteBearingFavorites, userManageBean.getResourceBundleString("label_bearing_catalog")));
            }
        } catch (Exception e) {
            Logger.getLogger(CreatePointLocationBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }

        return ffSetList;
    }

    public List<SelectItem> populateDevicePresetList(UserManageBean userManageBean, List<DevicePresetVO> devicePresetVOs) {
        List<SelectItem> ffSetList = new ArrayList();
        List<DevicePresetVO> globalDevicePresetVO, siteDevicePresetVO;
        try {

            siteDevicePresetVO = devicePresetVOs.stream().filter(ff -> ff.getSiteId() != null).filter(Utils.distinctByKeys(DevicePresetVO::getName)).collect(Collectors.toList());
            globalDevicePresetVO = devicePresetVOs.stream().filter(ff -> ff.getSiteId() == null).collect(Collectors.toList());
            globalDevicePresetVO = globalDevicePresetVO.stream().filter(Utils.distinctByKeys(DevicePresetVO::getName)).collect(Collectors.toList());
            if (globalDevicePresetVO != null && !globalDevicePresetVO.isEmpty()) {
                ffSetList.addAll(Utils.groupDeviceTemplateList(globalDevicePresetVO, userManageBean.getResourceBundleString("label_global_device_template")));
            }
            if (siteDevicePresetVO != null && !siteDevicePresetVO.isEmpty()) {
                ffSetList.addAll(Utils.groupDeviceTemplateList(siteDevicePresetVO, userManageBean.getResourceBundleString("label_site_device_template")));
            }
        } catch (Exception e) {
            Logger.getLogger(CreatePointLocationBean.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }

        return ffSetList;
    }

    public List<SelectItem> populateDevicePresetListByDeviceType(UserManageBean userManageBean, String customerAccount, UUID siteId) {
        List<SelectItem> tachometerList = new ArrayList();
        List<TachometerVO> globaltachometerList = new ArrayList();
        List<TachometerVO> tachList;
        try {
            if (!userManageBean.getCurrentSite().getCustomerAccount().equals(DEFAULT_UPTIME_ACCOUNT) && (tachList = getGlobalTachList(DEFAULT_UPTIME_ACCOUNT)) != null && !tachList.isEmpty()) {
                globaltachometerList.addAll(tachList);
            }
            if ((tachList = getGlobalTachList(customerAccount)) != null && !tachList.isEmpty()) {
                globaltachometerList.addAll(tachList);
            }
            if (!globaltachometerList.isEmpty()) {
                tachometerList.addAll(Utils.groupTachometerList(Utils.getUniqueTachometerList(globaltachometerList), userManageBean.getResourceBundleString("label_global_tachometers")));
            }
            if (siteId != null && (tachList = getSiteTachList(customerAccount, siteId)) != null && !tachList.isEmpty()) {
                tachometerList.addAll(Utils.groupTachometerList(tachList, userManageBean.getResourceBundleString("label_site_tachometers")));
            }
        } catch (Exception e) {
            Logger.getLogger(PresetUtil.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
        return tachometerList;
    }

    /**
     * Returns the rows from the table customer_ap_set based on the given account
     *
     * @param customerAccount, String Object
     * @param deviceType, String Object
     * @return List Object of TachometerVO Objects
     */
    public List<DevicePresetVO> getGlobalDeviceTypeListByDeviceType(String customerAccount, String deviceType) {
        try {
            if (devicePresetList.isEmpty() || devicePresetList.stream().filter(f -> f.getCustomerAccount().equalsIgnoreCase(customerAccount)).findAny().orElse(null) == null) {
                //devicePresetList.addAll(TachometerClient.getInstance().getGlobalTachometersByCustomer(ServiceRegistrarClient.getInstance().getServiceHostURL(PRESET_SERVICE_NAME), customerAccount));
            }
        } catch (Exception e) {
            Logger.getLogger(PresetUtil.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
        return devicePresetList;
    }

    /**
     * Return List of SelectItems
     *
     * @param siteApAlSetsVOList
     * @return List Object of SelectItem Objects
     */
    public List<SelectItem> populateAlSetByApAlsetList(List<ApAlSetVO> siteApAlSetsVOList) {
        List<SelectItem> alSetByApAlsetList = new ArrayList();
        List<ApAlSetVO> apAlSetVOs;
        try {
            apAlSetVOs = siteApAlSetsVOList.stream().filter(distinctByKeys(ApAlSetVO::getCustomerAccount, ApAlSetVO::getSiteId, ApAlSetVO::getSensorType, ApAlSetVO::getApSetId, ApAlSetVO::getAlSetId)).collect(Collectors.toList());
            alSetByApAlsetList.addAll(Utils.groupAlSetList(apAlSetVOs));

        } catch (Exception e) {
            Logger.getLogger(PresetUtil.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
        return alSetByApAlsetList;
    }

    /**
     * Clear the field globalApAlSetSensorList
     */
    public void clearGlobalApAlSetSensorList() {
        try {
            globalApAlSetSensorList.clear();
        } catch (Exception e) {
            Logger.getLogger(PresetUtil.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Clear the field siteApAlSetSensorList
     */
    public void clearSiteApAlSetSensorList() {
        try {
            siteApAlSetSensorList.clear();
        } catch (Exception e) {
            Logger.getLogger(PresetUtil.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Clear the field globalTachList
     */
    public void clearGlobalTachList() {
        try {
            globalTachList.clear();
        } catch (Exception e) {
            Logger.getLogger(PresetUtil.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Clear the field globalPtLoNamePresetList
     */
    public void clearGlobalPtLoNamePresetList() {
        try {
            globalPtLoNamePresetList.clear();
        } catch (Exception e) {
            Logger.getLogger(PresetUtil.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Clear the field siteTachList
     */
    public void clearSiteTachList() {
        try {
            siteTachList.clear();
        } catch (Exception e) {
            Logger.getLogger(PresetUtil.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Clear the field sitePtLoNameList
     */
    public void clearSitePointLocationNameList() {
        try {
            sitePtLoNameList.clear();
        } catch (Exception e) {
            Logger.getLogger(PresetUtil.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Clear the field siteAssetPresetList
     */
    public void clearSiteAssetPresetList() {
        try {
            siteAssetPresetList.clear();
        } catch (Exception e) {
            Logger.getLogger(PresetUtil.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Clear the field globalAssetPresetList
     */
    public void clearGlobalAssetPresetList() {
        try {
            globalAssetPresetList.clear();
        } catch (Exception e) {
            Logger.getLogger(PresetUtil.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Clear the field siteTachByPK
     */
    public void clearSiteTachByPK() {
        try {
            siteTachByPK.clear();
        } catch (Exception e) {
            Logger.getLogger(PresetUtil.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Clear the field siteFaultFrequenciesList
     */
    public void clearSiteFaultFrequenciesList() {
        try {
            siteFaultFrequenciesList.clear();
        } catch (Exception e) {
            Logger.getLogger(PresetUtil.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Clear the field globalFaultFrequenciesList
     */
    public void clearGlobalFaultFrequenciesList() {
        try {
            globalFaultFrequenciesList.clear();
        } catch (Exception e) {
            Logger.getLogger(PresetUtil.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }
}
