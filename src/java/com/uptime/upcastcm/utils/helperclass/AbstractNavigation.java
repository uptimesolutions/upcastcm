/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.utils.helperclass;

import com.uptime.upcastcm.bean.NavigationBean;
import static com.uptime.upcastcm.utils.ApplicationConstants.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import org.primefaces.event.NodeCollapseEvent;
import org.primefaces.event.NodeExpandEvent;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.MenuElement;

/**
 *
 * @author twilcox
 */
public abstract class AbstractNavigation {
    protected final Map<String, Map<String, List<SelectItem>>> regionCountrySiteMap;
    protected final List<SelectItem> areaList, assetList, pointLocationList, pointList, apSetList, alSetParamList;
    protected Filters selectedFilters;
    protected NavigationBean navigationBean;
    protected final List<Short> yearList;
    private final String maxTreeDepth;
    private boolean end;
    
    /**
     * Constructor
     * @param maxTreeDepth, String Object 
     */
    public AbstractNavigation(String maxTreeDepth) {
        this.maxTreeDepth = maxTreeDepth;
        regionCountrySiteMap = new HashMap();
        areaList = new ArrayList();
        assetList = new ArrayList();
        pointLocationList = new ArrayList();
        pointList = new ArrayList();
        apSetList = new ArrayList();
        alSetParamList = new ArrayList();
        yearList = new ArrayList();
        end = false;
        
        try {
            navigationBean = (NavigationBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("navigationBean");
        } catch (Exception e) {
            Logger.getLogger(AbstractNavigation.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            navigationBean = null;
        }
    }
    
    /**
     * Method to help the updateBreadcrumbModel method
     * @param value, String Object
     * @param onCommand, String Object
     * @param list, List Object of MenuElement Objects
     * @throws Exception 
     */
    public void updateBreadcrumbModelHelper(String value, String onCommand, List<MenuElement> list) throws Exception {
        DefaultMenuItem index = new DefaultMenuItem();
        index.setValue(value);
        index.setCommand(onCommand);
        index.setOncomplete("updateTreeNodes()");
        list.add(index);
    }
    
    /**
     * Method to help the treeContentMenuBuilder method
     * @param type, String Object
     * @param data, SelectItem Object
     * @param childType, String Object
     * @param parent, TreeNode Object
     * @param list, List Object of Objects
     * @param useList, boolean
     */
    public void treeContentMenuBuilderHelper(String type, Object data, String childType, TreeNode parent, List<Object> list, boolean useList) {
        TreeNode newNode;
        StringBuilder sb;
        String[] elements;
        int size, startingIndex;
        
        try {
            if (type != null && !type.isEmpty() && childType != null && !childType.isEmpty() && parent != null) {
                if (data != null && (data instanceof String && !((String)data).isEmpty()) || (data instanceof SelectItem && ((SelectItem)data).getLabel() != null && !((SelectItem)data).getLabel().isEmpty())) {
                    newNode = new DefaultTreeNode(type, data, parent);
                    newNode.setSelectable(false);
                    if (data instanceof String) {
                        elements = ((String)data).split(" - ");
                    } else {
                        elements = ((SelectItem)data).getLabel().split(" - ");
                    }
                    startingIndex = Integer.parseInt(elements[0].replace("(", "")) - 1;
                    size = ((list.size() < (startingIndex + 100)) ? list.size() : (startingIndex + 100));
                    for (int i = startingIndex; i < size; i++) {
                        treeContentMenuBuilder(childType, list.get(i), newNode, useList ? list : null);
                    }
                } else {
                    for (int i = 0; i < (list.size()/100); i++) {
                        sb = new StringBuilder();
                        if (list.get(i * 100) instanceof String) {
                            sb.append("(").append(i * 100 + 1).append(" - ").append(i * 100 + 100).append(") - ").append((String)list.get(i * 100));
                        } else if (list.get(i * 100) instanceof SelectItem) {
                            sb.append("(").append(i * 100 + 1).append(" - ").append(i * 100 + 100).append(") - ").append(((SelectItem)list.get(i * 100)).getLabel());
                        }
                        treeContentMenuBuilder(childType + "_over_100", sb.toString(), parent, useList ? list : null);
                        if (i + 1 == (list.size()/100)) {
                            sb = new StringBuilder();
                            if (list.get(i * 100) instanceof String) {
                                sb.append("(").append((i + 1) * 100 + 1).append(" - ").append(list.size()).append(") - ").append((String)list.get((i + 1) * 100));
                            } else {
                                sb.append("(").append((i + 1) * 100 + 1).append(" - ").append(list.size()).append(") - ").append(((SelectItem)list.get((i + 1) * 100)).getLabel());
                            }
                            treeContentMenuBuilder(childType + "_over_100", sb.toString(), parent, useList ? list : null);
                        }
                    }
                }
            }
        } catch (Exception e) {
            Logger.getLogger(AbstractNavigation.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }
    
    /**
     * Method to help the treeContentMenuBuilder method
     * @param type, String Object
     * @param data, Object
     * @param parent, TreeNode Object
     * @param list, List Object of Objects
     * @param empty, boolean
     */
    public void treeContentMenuBuilderHelper(String type, Object data, TreeNode parent, List<Object> list, boolean empty) {
        TreeNode newNode, forLoopNewNode;
        
        try {
            if (type != null && !type.isEmpty() && parent != null) {
                if (data != null && (data instanceof String && !((String)data).isEmpty()) || (data instanceof SelectItem && ((SelectItem)data).getLabel() != null && !((SelectItem)data).getLabel().isEmpty())) {
                    newNode = new DefaultTreeNode(type, data, parent);
                    if (empty) {
                        treeContentMenuBuilder(EMPTY, null, newNode, null);
                    }
                } else if (list != null) {
                    for (Object item : list) {
                        forLoopNewNode = new DefaultTreeNode(type, item, parent);
                        if (empty) {
                            treeContentMenuBuilder(EMPTY, null, forLoopNewNode, null);
                        }
                    }
                }
            }
        } catch (Exception e) {
            Logger.getLogger(AbstractNavigation.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }
    
    /**
     * Update the treeNodeRoot field from the NavigationBean based on the given value
     * @param value, String Object 
     */
    public void updateTreeNodeRoot(String value) {
        try {
            if (value != null) {
                switch(value) {
                    case YEAR:
                        collapseTreeToRoot();
                        break;
                    case REGION:
                        if (selectedFilters.getRegion() != null) {
                            collapseTreeToRoot();
                            expandTreeNodeRoot(1);
                        }
                        break;
                    case COUNTRY:
                        if (selectedFilters.getCountry() != null) {
                            expandTreeNodeRoot(2);
                        }
                        break;
                    case SITE:
                        if (selectedFilters.getSite() != null && selectedFilters.getSite().getValue() != null) {
                            expandTreeNodeRoot(3);
                        }
                        break;
                    case AREA:
                        if (selectedFilters.getArea() != null && selectedFilters.getArea().getValue() != null) {
                            expandTreeNodeRoot(4);
                        }
                        break;
                    case ASSET:
                        if (selectedFilters.getAsset() != null && selectedFilters.getAsset().getValue() != null) {
                            expandTreeNodeRoot(5);
                        }
                        break;
                    case POINT_LOCATION:
                        if (selectedFilters.getPointLocation() != null && selectedFilters.getPointLocation().getValue() != null) {
                            expandTreeNodeRoot(6);
                        }
                        break;
                    case POINT:
                        if (selectedFilters.getPoint() != null && selectedFilters.getPoint().getValue() != null) {
                            expandTreeNodeRoot(7);
                        }
                        break;
                    case AP_SET:
                        if (selectedFilters.getApSet() != null && selectedFilters.getApSet().getValue() != null) {
                            expandTreeNodeRoot(8);
                        }
                        break;
                    case AL_SET_PARAM:
                        if(selectedFilters.getAlSetParam() != null && selectedFilters.getAlSetParam().getValue() != null && selectedFilters.getAlSetParam().getLabel() != null) {
                            expandTreeNodeRoot(9);
                        }
                        break;
                }
                updateUI("tree");
            }
        } catch (Exception e) {
            Logger.getLogger(AbstractNavigation.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }
    
    /**
     * collapse the treeNodeRoot field from the NavigationBean
     * @throws Exception 
     */
    public void collapseTreeToRoot() throws Exception {
        if (navigationBean != null && navigationBean.getTreeNodeRoot().getChildren() != null) {
            navigationBean.getTreeNodeRoot().getChildren().stream().forEachOrdered(regionNode -> {
                regionNode.setExpanded(false);
                regionNode.getChildren().clear();
                treeContentMenuBuilder(EMPTY, null, regionNode, null);
            });
        }
    }
    
    /**
     * expand the treeNodeRoot field from the NavigationBean based on the given level
     * @param level, int 
     * @throws Exception 
     */
    public void expandTreeNodeRoot(int level) throws Exception {
        end = false;
        
        collapseTreeToRoot();
        if (level > 0 && navigationBean != null && navigationBean.getTreeNodeRoot().getChildren() != null && selectedFilters.getRegion() != null && !selectedFilters.getRegion().isEmpty()) {
            for (TreeNode regionNode : navigationBean.getTreeNodeRoot().getChildren()) {
                if (((String)regionNode.getData()).equals(selectedFilters.getRegion())) {
                    if (regionNode.getChildren() != null && regionNode.getChildCount() == 1 && regionNode.getChildren().get(0).getType().equals(EMPTY)) {
                        if (!regionCountrySiteMap.get((String)regionNode.getData()).isEmpty()) {
                            regionNode.getChildren().clear();
                            treeContentMenuBuilder(COUNTRY, null, regionNode, null);
                        }
                    }
                    regionNode.setExpanded(true);
                    
                    if (level > 1 && selectedFilters.getCountry() != null && !selectedFilters.getCountry().isEmpty() && regionNode.getChildren() != null) {
                        regionNode.setSelected(false);
                        for (TreeNode countryNode : regionNode.getChildren()) {
                            if (((String)countryNode.getData()).equals(selectedFilters.getCountry())) {
                                if (countryNode.getChildren() != null && countryNode.getChildCount() == 1 && countryNode.getChildren().get(0).getType().equals(EMPTY)) {
                                    if (regionCountrySiteMap.get((String)regionNode.getData()).get((String)countryNode.getData()).size() > 100) {
                                        countryNode.getChildren().clear();
                                        treeContentMenuBuilder(SITE_OVER_100, null, countryNode, null);
                                    } else if (!regionCountrySiteMap.get((String)regionNode.getData()).get((String)countryNode.getData()).isEmpty()) {
                                        countryNode.getChildren().clear();
                                        treeContentMenuBuilder(SITE, null, countryNode, null);
                                    }
                                }
                                countryNode.setExpanded(true);
                                
                                if(level > 2 && selectedFilters.getSite() != null &&  selectedFilters.getSite().getValue() != null && countryNode.getChildren() != null && !countryNode.getChildren().isEmpty()) {
                                    countryNode.setSelected(false);
                                    if (countryNode.getChildren().get(0).getType().equals(SITE_OVER_100)) {
                                        siteOver100NodeLoop(countryNode, level);
                                    } else {
                                        siteNodeLoop(countryNode, level);
                                    }
                                } else {
                                    countryNode.setSelected(true);
                                }
                                break;
                            }
                        }
                    } else {
                        regionNode.setSelected(true);
                    }
                    break;
                }
            }
        }
    }
    
    /**
     * Loop, for over 100 sites child nodes
     * Note: A helper method for the expandTreeNodeRoot
     * 
     * @param node, TreeNode Object
     * @param level, int
     * @throws Exception 
     */
    private void siteOver100NodeLoop(TreeNode node, int level) throws Exception {
        for (TreeNode siteOver100Node : node.getChildren()) {
            if(siteOver100Node.getChildren() != null && !siteOver100Node.getChildren().isEmpty() && siteOver100Node.getChildren().get(0).getType().equals(SITE)) {
                for (TreeNode siteNode : siteOver100Node.getChildren()) {
                    if ((((SelectItem)siteNode.getData()).getValue()).equals(selectedFilters.getSite().getValue())) {
                        if (maxTreeDepth != null && maxTreeDepth.equalsIgnoreCase(SITE)) {
                            selectedFilters.setSiteOver100((String)siteOver100Node.getData());
                            siteOver100Node.setExpanded(true);
                            siteNode.setExpanded(true);
                            siteNode.setSelected(true);
                            end = true;
                            break;
                        } else {
                            selectedFilters.setSiteOver100((String)siteOver100Node.getData());
                            if (siteNode.getChildren() != null && siteNode.getChildCount() == 1 && siteNode.getChildren().get(0).getType().equals(EMPTY)) {
                                if (areaList.size() > 100) {
                                    siteNode.getChildren().clear();
                                    treeContentMenuBuilder(AREA_OVER_100, null, siteNode, null);
                                } else if (!areaList.isEmpty()) {
                                    siteNode.getChildren().clear();
                                    treeContentMenuBuilder(AREA, null, siteNode, null);
                                }
                            }
                            siteOver100Node.setExpanded(true);
                            siteNode.setExpanded(true);

                            if(level > 3 && selectedFilters.getArea() != null && selectedFilters.getArea().getValue() != null && siteNode.getChildren() != null && !siteNode.getChildren().isEmpty()) {
                                siteNode.setSelected(false);
                                if (siteNode.getChildren().get(0).getType().equals(AREA_OVER_100)) {
                                    areaOver100NodeLoop(siteNode, level);
                                } else {
                                    areaNodeLoop(siteNode, level);
                                }
                            } else {
                                siteNode.setSelected(true);
                            }
                            end = true;
                            break;
                        }
                    }
                }
                if (end) {
                    break;
                }
            }
        } 
    }
    
    /**
     * Loop, for under 100 sites child nodes
     * Note: A helper method for the expandTreeNodeRoot
     * 
     * @param node, TreeNode Object
     * @param level, int
     * @throws Exception 
     */
    private void siteNodeLoop(TreeNode node, int level) throws Exception {
        for (TreeNode siteNode : node.getChildren()) {
            if ((((SelectItem)siteNode.getData()).getValue()).equals(selectedFilters.getSite().getValue())) {
                if (maxTreeDepth != null && maxTreeDepth.equalsIgnoreCase(SITE)) {
                    siteNode.setExpanded(true);
                    siteNode.setSelected(true);
                    break;
                } else {
                    if (siteNode.getChildren() != null && siteNode.getChildCount() == 1 && siteNode.getChildren().get(0).getType().equals(EMPTY)) {
                        if (areaList.size() > 100) {
                            siteNode.getChildren().clear();
                            treeContentMenuBuilder(AREA_OVER_100, null, siteNode, null);
                        } else if (!areaList.isEmpty()) {
                            siteNode.getChildren().clear();
                            treeContentMenuBuilder(AREA, null, siteNode, null);
                        }
                    }
                    siteNode.setExpanded(true);

                    if(level > 3 && selectedFilters.getArea() != null && selectedFilters.getArea().getValue() != null && siteNode.getChildren() != null && !siteNode.getChildren().isEmpty()) {
                        siteNode.setSelected(false);
                        if (siteNode.getChildren().get(0).getType().equals(AREA_OVER_100)) {
                            areaOver100NodeLoop(siteNode, level);
                        } else {
                            areaNodeLoop(siteNode, level);
                        }
                    } else {
                        siteNode.setSelected(true);
                    }
                    break;
                }
            }
        }
    }
    
    /**
     * Loop, for over 100 areas child nodes
     * Note: A helper method for the expandTreeNodeRoot
     * 
     * @param node, TreeNode Object
     * @param level, int
     * @throws Exception 
     */
    private void areaOver100NodeLoop(TreeNode node, int level) throws Exception {
        for (TreeNode areaOver100Node : node.getChildren()) {
            if(areaOver100Node.getChildren() != null && !areaOver100Node.getChildren().isEmpty() && areaOver100Node.getChildren().get(0).getType().equals(AREA)) {
                for (TreeNode areaNode : areaOver100Node.getChildren()) {
                    if ((((SelectItem)areaNode.getData()).getValue()).equals(selectedFilters.getArea().getValue())) {
                        if (maxTreeDepth != null && maxTreeDepth.equalsIgnoreCase(AREA)) {
                            selectedFilters.setAreaOver100((String)areaOver100Node.getData());
                            areaOver100Node.setExpanded(true);
                            areaNode.setExpanded(true);
                            areaNode.setSelected(true);
                            end = true;
                            break;
                        } else {
                            selectedFilters.setAreaOver100((String)areaOver100Node.getData());
                            if (areaNode.getChildren() != null && areaNode.getChildCount() == 1 && areaNode.getChildren().get(0).getType().equals(EMPTY)) {
                                if (assetList.size() > 100) {
                                    areaNode.getChildren().clear();
                                    treeContentMenuBuilder(ASSET_OVER_100, null, areaNode, null);
                                } else if (!assetList.isEmpty()) {
                                    areaNode.getChildren().clear();
                                    treeContentMenuBuilder(ASSET, null, areaNode, null);
                                }
                            }
                            areaOver100Node.setExpanded(true);
                            areaNode.setExpanded(true);

                            if(level > 4 && selectedFilters.getAsset() != null && selectedFilters.getAsset().getValue() != null && areaNode.getChildren() != null && !areaNode.getChildren().isEmpty()) {
                                areaNode.setSelected(false);
                                if (areaNode.getChildren().get(0).getType().equals(ASSET_OVER_100)) {
                                    assetOver100NodeLoop(areaNode, level);
                                } else {
                                    assetNodeLoop(areaNode, level);
                                }
                            } else {
                                areaNode.setSelected(true);
                            }
                            end = true;
                            break;
                        }
                    }
                }
                if (end) {
                    break;
                }
            }
        }
    }
    
    /**
     * Loop, for under 100 areas child nodes
     * Note: A helper method for the expandTreeNodeRoot
     * 
     * @param node, TreeNode Object
     * @param level, int
     * @throws Exception 
     */
    private void areaNodeLoop(TreeNode node, int level) throws Exception {
        for (TreeNode areaNode : node.getChildren()) {
            if ((((SelectItem)areaNode.getData()).getValue()).equals(selectedFilters.getArea().getValue())) {
                if (maxTreeDepth != null && maxTreeDepth.equalsIgnoreCase(AREA)) {
                    areaNode.setExpanded(true);
                    areaNode.setSelected(true);
                    break;
                } else {
                    if (areaNode.getChildren() != null && areaNode.getChildCount() == 1 && areaNode.getChildren().get(0).getType().equals(EMPTY)) {
                        if (assetList.size() > 100) {
                            areaNode.getChildren().clear();
                            treeContentMenuBuilder(ASSET_OVER_100, null, areaNode, null);
                        } else if (!assetList.isEmpty()) {
                            areaNode.getChildren().clear();
                            treeContentMenuBuilder(ASSET, null, areaNode, null);
                        }
                    }
                    areaNode.setExpanded(true);

                    if(level > 4 && selectedFilters.getAsset() != null && selectedFilters.getAsset().getValue() != null && areaNode.getChildren() != null && !areaNode.getChildren().isEmpty()) {
                        areaNode.setSelected(false);
                        if (areaNode.getChildren().get(0).getType().equals(ASSET_OVER_100)) {
                            assetOver100NodeLoop(areaNode, level);
                        } else {
                            assetNodeLoop(areaNode, level);
                        }
                    } else {
                        areaNode.setSelected(true);
                    }
                    break;
                }
            }
        }
    }
    
    /**
     * Loop, for over 100 assets child nodes
     * Note: A helper method for the expandTreeNodeRoot
     * 
     * @param node, TreeNode Object
     * @param level, int
     * @throws Exception 
     */
    private void assetOver100NodeLoop(TreeNode node, int level) throws Exception {
        for (TreeNode assetOver100Node : node.getChildren()) {
            if(assetOver100Node.getChildren() != null && !assetOver100Node.getChildren().isEmpty() && assetOver100Node.getChildren().get(0).getType().equals(ASSET)) {
                for (TreeNode assetNode : assetOver100Node.getChildren()) {
                    if ((((SelectItem)assetNode.getData()).getValue()).equals(selectedFilters.getAsset().getValue())) {
                        if (maxTreeDepth != null && maxTreeDepth.equalsIgnoreCase(ASSET)) {
                            selectedFilters.setAssetOver100((String)assetOver100Node.getData());
                            assetOver100Node.setExpanded(true);
                            assetNode.setExpanded(true);
                            assetNode.setSelected(true);
                            end = true;
                            break;
                        } else {
                            selectedFilters.setAssetOver100((String)assetOver100Node.getData());
                            if (assetNode.getChildren() != null && assetNode.getChildCount() == 1 && assetNode.getChildren().get(0).getType().equals(EMPTY)) {
                                if (pointLocationList.size() > 100) {
                                    assetNode.getChildren().clear();
                                    treeContentMenuBuilder(POINT_LOCATION_OVER_100, null, assetNode, null);
                                } else if (!pointLocationList.isEmpty()) {
                                    assetNode.getChildren().clear();
                                    treeContentMenuBuilder(POINT_LOCATION, null, assetNode, null);
                                }
                            }
                            assetOver100Node.setExpanded(true);
                            assetNode.setExpanded(true);

                            if(level > 5 && selectedFilters.getPointLocation() != null && selectedFilters.getPointLocation().getValue() != null && assetNode.getChildren() != null && !assetNode.getChildren().isEmpty()) {
                                assetNode.setSelected(false);
                                if (assetNode.getChildren().get(0).getType().equals(POINT_LOCATION_OVER_100)) {
                                    pointLocationOver100NodeLoop(assetNode, level);
                                } else {
                                    pointLocationNodeLoop(assetNode, level);
                                }
                            } else {
                                assetNode.setSelected(true);
                            }
                            end = true;
                            break;
                        }   
                    }
                }
                if (end) {
                    break;
                }
            }
        }
    }
    
    /**
     * Loop, for under 100 assets child nodes
     * Note: A helper method for the expandTreeNodeRoot
     * 
     * @param node, TreeNode Object
     * @param level, int
     * @throws Exception 
     */
    private void assetNodeLoop(TreeNode node, int level) throws Exception {
        for (TreeNode assetNode : node.getChildren()) {
            if ((((SelectItem)assetNode.getData()).getValue()).equals(selectedFilters.getAsset().getValue())) {
                if (maxTreeDepth != null && maxTreeDepth.equalsIgnoreCase(ASSET)) {
                    assetNode.setExpanded(true);
                    assetNode.setSelected(true);
                    break;
                } else {
                    if (assetNode.getChildren() != null && assetNode.getChildCount() == 1 && assetNode.getChildren().get(0).getType().equals(EMPTY)) {
                        if (pointLocationList.size() > 100) {
                            assetNode.getChildren().clear();
                            treeContentMenuBuilder(POINT_LOCATION_OVER_100, null, assetNode, null);
                        } else if (!pointLocationList.isEmpty()) {
                            assetNode.getChildren().clear();
                            treeContentMenuBuilder(POINT_LOCATION, null, assetNode, null);
                        }
                    }
                    assetNode.setExpanded(true);

                    if(level > 5 && selectedFilters.getPointLocation() != null && selectedFilters.getPointLocation().getValue() != null && assetNode.getChildren() != null && !assetNode.getChildren().isEmpty()) {
                        assetNode.setSelected(false);
                        if (assetNode.getChildren().get(0).getType().equals(POINT_LOCATION_OVER_100)) {
                            pointLocationOver100NodeLoop(assetNode, level);
                        } else {
                            pointLocationNodeLoop(assetNode, level);
                        }
                    } else {
                        assetNode.setSelected(true);
                    }
                    break;
                }   
            }
        }
    }
    
    /**
     * Loop, for over 100 point locations child nodes
     * Note: A helper method for the expandTreeNodeRoot
     * 
     * @param node, TreeNode Object
     * @param level, int
     * @throws Exception 
     */
    private void pointLocationOver100NodeLoop(TreeNode node, int level) throws Exception {
        for (TreeNode pointLocationOver100Node : node.getChildren()) {
            if(pointLocationOver100Node.getChildren() != null && !pointLocationOver100Node.getChildren().isEmpty() && pointLocationOver100Node.getChildren().get(0).getType().equals(POINT_LOCATION)) {
                for (TreeNode pointLocationNode : pointLocationOver100Node.getChildren()) {
                    if ((((SelectItem)pointLocationNode.getData()).getValue()).equals(selectedFilters.getPointLocation().getValue())) {
                        if (maxTreeDepth != null && maxTreeDepth.equalsIgnoreCase(POINT_LOCATION)) {
                            selectedFilters.setPointLocationOver100((String)pointLocationOver100Node.getData());
                            pointLocationOver100Node.setExpanded(true);
                            pointLocationNode.setExpanded(true);
                            pointLocationNode.setSelected(true);
                            end = true;
                            break;
                        } else {
                            selectedFilters.setPointLocationOver100((String)pointLocationOver100Node.getData());
                            if (pointLocationNode.getChildren() != null && pointLocationNode.getChildCount() == 1 && pointLocationNode.getChildren().get(0).getType().equals(EMPTY)) {
                                if (pointList.size() > 100) {
                                    pointLocationNode.getChildren().clear();
                                    treeContentMenuBuilder(POINT_OVER_100, null, pointLocationNode, null);
                                } else if (!pointList.isEmpty()) {
                                    pointLocationNode.getChildren().clear();
                                    treeContentMenuBuilder(POINT, null, pointLocationNode, null);
                                }
                            }
                            pointLocationOver100Node.setExpanded(true);
                            pointLocationNode.setExpanded(true);

                            if(level > 6 && selectedFilters.getPoint() != null && selectedFilters.getPoint().getValue() != null && pointLocationNode.getChildren() != null && !pointLocationNode.getChildren().isEmpty()) {
                                pointLocationNode.setSelected(false);
                                if (pointLocationNode.getChildren().get(0).getType().equals(POINT_OVER_100)) {
                                    pointOver100NodeLoop(pointLocationNode, level);
                                } else {
                                    pointNodeLoop(pointLocationNode, level);
                                }
                            } else {
                                pointLocationNode.setSelected(true);
                            }
                            end = true;
                            break;
                        }    
                    }
                }
                if (end) {
                    break;
                }
            }
        }
    }
    
    /**
     * Loop, for under 100 point locations child nodes
     * Note: A helper method for the expandTreeNodeRoot
     * 
     * @param node, TreeNode Object
     * @param level, int
     * @throws Exception 
     */
    private void pointLocationNodeLoop(TreeNode node, int level) throws Exception {
        for (TreeNode pointLocationNode : node.getChildren()) {
            if ((((SelectItem)pointLocationNode.getData()).getValue()).equals(selectedFilters.getPointLocation().getValue())) {
                if (maxTreeDepth != null && maxTreeDepth.equalsIgnoreCase(POINT_LOCATION)) {
                    pointLocationNode.setExpanded(true);
                    pointLocationNode.setSelected(true);
                    break;
                } else {
                    if (pointLocationNode.getChildren() != null && pointLocationNode.getChildCount() == 1 && pointLocationNode.getChildren().get(0).getType().equals(EMPTY)) {
                        if (pointList.size() > 100) {
                            pointLocationNode.getChildren().clear();
                            treeContentMenuBuilder(POINT_OVER_100, null, pointLocationNode, null);
                        } else if (!pointList.isEmpty()) {
                            pointLocationNode.getChildren().clear();
                            treeContentMenuBuilder(POINT, null, pointLocationNode, null);
                        }
                    }
                    pointLocationNode.setExpanded(true);

                    if(level > 6 && selectedFilters.getPoint() != null && selectedFilters.getPoint().getValue() != null && pointLocationNode.getChildren() != null && !pointLocationNode.getChildren().isEmpty()) {
                        pointLocationNode.setSelected(false);
                        if (pointLocationNode.getChildren().get(0).getType().equals(POINT_OVER_100)) {
                            pointOver100NodeLoop(pointLocationNode, level);
                        } else {
                            pointNodeLoop(pointLocationNode, level);
                        }
                    } else {
                        pointLocationNode.setSelected(true);
                    }
                    break;
                }    
            }
        }
    }
    
    /**
     * Loop, for over 100 points child nodes
     * Note: A helper method for the expandTreeNodeRoot
     * 
     * @param node, TreeNode Object
     * @param level, int
     * @throws Exception 
     */
    private void pointOver100NodeLoop(TreeNode node, int level) throws Exception {
        for (TreeNode pointOver100Node : node.getChildren()) {
            if(pointOver100Node.getChildren() != null && !pointOver100Node.getChildren().isEmpty() && pointOver100Node.getChildren().get(0).getType().equals(POINT)) {
                for (TreeNode pointNode : pointOver100Node.getChildren()) {
                    if ((((SelectItem)pointNode.getData()).getValue()).equals(selectedFilters.getPoint().getValue())) {
                        if (maxTreeDepth != null && maxTreeDepth.equalsIgnoreCase(POINT)) {
                            selectedFilters.setPointOver100((String)pointOver100Node.getData());
                            pointOver100Node.setExpanded(true);
                            pointNode.setExpanded(true);
                            pointNode.setSelected(true);
                            end = true;
                            break;
                        } else {
                            selectedFilters.setPointOver100((String)pointOver100Node.getData());
                            if (pointNode.getChildren() != null && pointNode.getChildCount() == 1 && pointNode.getChildren().get(0).getType().equals(EMPTY)) {
                                if (apSetList.size() > 100) {
                                    pointNode.getChildren().clear();
                                    treeContentMenuBuilder(AP_SET_OVER_100, null, pointNode, null);
                                } else if (!apSetList.isEmpty()) {
                                    pointNode.getChildren().clear();
                                    treeContentMenuBuilder(AP_SET, null, pointNode, null);
                                }
                            }
                            pointOver100Node.setExpanded(true);
                            pointNode.setExpanded(true);

                            if(level > 7 && selectedFilters.getApSet() != null && selectedFilters.getApSet().getValue() != null && pointNode.getChildren() != null && !pointNode.getChildren().isEmpty()) {
                                pointNode.setSelected(false);
                                if (pointNode.getChildren().get(0).getType().equals(AP_SET_OVER_100)) {
                                    apSetOver100NodeLoop(pointNode, level);
                                } else {
                                    apSetNodeLoop(pointNode, level);
                                }
                            } else {
                                pointNode.setSelected(true);
                            }
                            end = true;
                            break;
                        }    
                    }
                }
                if (end) {
                    break;
                }
            }
        }
    }
    
    /**
     * Loop, for under 100 points child nodes
     * Note: A helper method for the expandTreeNodeRoot
     * 
     * @param node, TreeNode Object
     * @param level, int
     * @throws Exception 
     */
    private void pointNodeLoop(TreeNode node, int level) throws Exception {
        for (TreeNode pointNode : node.getChildren()) {
            if ((((SelectItem)pointNode.getData()).getValue()).equals(selectedFilters.getPoint().getValue())) {
                if (maxTreeDepth != null && maxTreeDepth.equalsIgnoreCase(POINT)) {
                    pointNode.setExpanded(true);
                    pointNode.setSelected(true);
                    break;
                } else {
                    if (pointNode.getChildren() != null && pointNode.getChildCount() == 1 && pointNode.getChildren().get(0).getType().equals(EMPTY)) {
                        if (apSetList.size() > 100) {
                            pointNode.getChildren().clear();
                            treeContentMenuBuilder(AP_SET_OVER_100, null, pointNode, null);
                        } else if (!apSetList.isEmpty()) {
                            pointNode.getChildren().clear();
                            treeContentMenuBuilder(AP_SET, null, pointNode, null);
                        }
                    }
                    pointNode.setExpanded(true);

                    if(level > 7 && selectedFilters.getApSet() != null && selectedFilters.getApSet().getValue() != null && pointNode.getChildren() != null && !pointNode.getChildren().isEmpty()) {
                        pointNode.setSelected(false);
                        if (pointNode.getChildren().get(0).getType().equals(AP_SET_OVER_100)) {
                            apSetOver100NodeLoop(pointNode, level);
                        } else {
                            apSetNodeLoop(pointNode, level);
                        }
                    } else {
                        pointNode.setSelected(true);
                    }
                    break;
                }    
            }
        }
    }
    
    /**
     * Loop, for over 100 AP sets child nodes
     * Note: A helper method for the expandTreeNodeRoot
     * 
     * @param node, TreeNode Object
     * @param level, int
     * @throws Exception 
     */
    private void apSetOver100NodeLoop(TreeNode node, int level) throws Exception {
        for (TreeNode apSetOver100Node : node.getChildren()) {
            if(apSetOver100Node.getChildren() != null && !apSetOver100Node.getChildren().isEmpty() && apSetOver100Node.getChildren().get(0).getType().equals(AP_SET)) {
                for (TreeNode apSetNode : apSetOver100Node.getChildren()) {
                    if ((((SelectItem)apSetNode.getData()).getValue()).equals(selectedFilters.getApSet().getValue())) {
                        if (maxTreeDepth != null && maxTreeDepth.equalsIgnoreCase(AP_SET)) {
                            selectedFilters.setApSetOver100((String)apSetOver100Node.getData());
                            apSetOver100Node.setExpanded(true);
                            apSetNode.setExpanded(true);
                            apSetNode.setSelected(true);
                            end = true;
                            break;
                        } else {
                            selectedFilters.setApSetOver100((String)apSetOver100Node.getData());
                            if (apSetNode.getChildren() != null && apSetNode.getChildCount() == 1 && apSetNode.getChildren().get(0).getType().equals(EMPTY)) {
                                if (alSetParamList.size() > 100) {
                                    apSetNode.getChildren().clear();
                                    treeContentMenuBuilder(AL_SET_PARAM_OVER_100, null, apSetNode, null);
                                } else if (!alSetParamList.isEmpty()) {
                                    apSetNode.getChildren().clear();
                                    treeContentMenuBuilder(AL_SET_PARAM, null, apSetNode, null);
                                }
                            }
                            apSetOver100Node.setExpanded(true);
                            apSetNode.setExpanded(true);

                            if(level > 8 && selectedFilters.getAlSetParam() != null && selectedFilters.getAlSetParam().getValue() != null && selectedFilters.getAlSetParam().getLabel() != null && !selectedFilters.getAlSetParam().getLabel().isEmpty() && apSetNode.getChildren() != null && !apSetNode.getChildren().isEmpty()) {
                                apSetNode.setSelected(false);
                                if (apSetNode.getChildren().get(0).getType().equals(AL_SET_PARAM_OVER_100)) {
                                    alSetParamOver100NodeLoop(apSetNode);
                                } else {
                                    alSetParamNodeLoop(apSetNode);
                                }
                            } else {
                                apSetNode.setSelected(true);
                            }
                            end = true;
                            break;
                        }
                    }
                }
                if (end) {
                    break;
                }
            }
        }
    }
    
    /**
     * Loop, for under 100 AP sets child nodes
     * Note: A helper method for the expandTreeNodeRoot
     * 
     * @param node, TreeNode Object
     * @param level, int
     * @throws Exception 
     */
    private void apSetNodeLoop(TreeNode node, int level) throws Exception {
        for (TreeNode apSetNode : node.getChildren()) {
            if ((((SelectItem)apSetNode.getData()).getValue()).equals(selectedFilters.getApSet().getValue())) {
                if (maxTreeDepth != null && maxTreeDepth.equalsIgnoreCase(AP_SET)) {
                    apSetNode.setExpanded(true);
                    apSetNode.setSelected(true);
                    break;
                } else {
                    if (apSetNode.getChildren() != null && apSetNode.getChildCount() == 1 && apSetNode.getChildren().get(0).getType().equals(EMPTY)) {
                        if (alSetParamList.size() > 100) {
                            apSetNode.getChildren().clear();
                            treeContentMenuBuilder(AL_SET_PARAM_OVER_100, null, apSetNode, null);
                        } else if (!alSetParamList.isEmpty()) {
                            apSetNode.getChildren().clear();
                            treeContentMenuBuilder(AL_SET_PARAM, null, apSetNode, null);
                        }
                    }
                    apSetNode.setExpanded(true);

                    if(level > 8 && selectedFilters.getAlSetParam() != null && selectedFilters.getAlSetParam().getValue() != null && selectedFilters.getAlSetParam().getLabel() != null && !selectedFilters.getAlSetParam().getLabel().isEmpty() && apSetNode.getChildren() != null && !apSetNode.getChildren().isEmpty()) {
                        apSetNode.setSelected(false);
                        if (apSetNode.getChildren().get(0).getType().equals(AL_SET_PARAM_OVER_100)) {
                            alSetParamOver100NodeLoop(apSetNode);
                        } else {
                            alSetParamNodeLoop(apSetNode);
                        }
                    } else {
                        apSetNode.setSelected(true);
                    }
                    break;
                }
            }
        }
    }
    
    /**
     * Loop, for over 100 alSetParams child nodes
     * Note: A helper method for the expandTreeNodeRoot
     * 
     * @param node, TreeNode Object
     * @param level, int
     * @throws Exception 
     */
    private void alSetParamOver100NodeLoop(TreeNode node) throws Exception {
        for (TreeNode alSetParamOver100Node : node.getChildren()) {
            if(alSetParamOver100Node.getChildren() != null && !alSetParamOver100Node.getChildren().isEmpty() && alSetParamOver100Node.getChildren().get(0).getType().equals(AL_SET_PARAM)) {
                for (TreeNode alSetParamNode : alSetParamOver100Node.getChildren()) {
                    if ((((SelectItem)alSetParamNode.getData()).getValue()).equals(selectedFilters.getAlSetParam().getValue()) && (((SelectItem)alSetParamNode.getData()).getLabel()).equals(selectedFilters.getAlSetParam().getLabel())) {
                        selectedFilters.setAlSetParamOver100((String)alSetParamOver100Node.getData());
                        alSetParamOver100Node.setExpanded(true);
                        alSetParamNode.setExpanded(true);
                        alSetParamNode.setSelected(true);
                        end = true;
                        break;
                    }
                }
                if (end) {
                    break;
                }
            }
        }
    }
    
    /**
     * Loop, for under 100 alSetParams child nodes
     * Note: A helper method for the expandTreeNodeRoot
     * 
     * @param node, TreeNode Object
     * @param level, int
     * @throws Exception 
     */
    private void alSetParamNodeLoop(TreeNode node) throws Exception {
        for (TreeNode alSetParamNode : node.getChildren()) {
            if ((((SelectItem)alSetParamNode.getData()).getValue()).equals(selectedFilters.getAlSetParam().getValue()) && (((SelectItem)alSetParamNode.getData()).getLabel()).equals(selectedFilters.getAlSetParam().getLabel())) {
                alSetParamNode.setExpanded(true);
                alSetParamNode.setSelected(true);
                break;
            }
        }
    }
    
    /**
     * check if there are any regions
     * @return boolean 
     */
    public boolean regionListCheck() {
        return !(regionCountrySiteMap != null && !regionCountrySiteMap.keySet().isEmpty());
    }

    /**
     * check if there are any countries
     * @return boolean 
     */
    public boolean countryListCheck() {
        if (selectedFilters != null && selectedFilters.getRegion() != null && !selectedFilters.getRegion().isEmpty()) {
            return !(regionCountrySiteMap.get(selectedFilters.getRegion()) != null && !regionCountrySiteMap.get(selectedFilters.getRegion()).isEmpty());
        }
        return true;
    }

    /**
     * check if there are any sites
     * @return boolean 
     */
    public boolean siteListCheck() {
        if (selectedFilters != null && selectedFilters.getRegion() != null && !selectedFilters.getRegion().isEmpty() &&
                selectedFilters.getCountry() != null && !selectedFilters.getCountry().isEmpty()) {
            return !(regionCountrySiteMap.get(selectedFilters.getRegion()).get(selectedFilters.getCountry()) != null && !regionCountrySiteMap.get(selectedFilters.getRegion()).get(selectedFilters.getCountry()).isEmpty());
        }
        return true;
    }

    /**
     * check if there are any areas
     * @return boolean 
     */
    public boolean areaListCheck() {
        return !(areaList != null && !areaList.isEmpty());
    }

    /**
     * check if there are any assets
     * @return boolean 
     */
    public boolean assetListCheck() {
        return !(assetList != null && !assetList.isEmpty());
    }

    /**
     * check if there are any point locations
     * @return boolean 
     */
    public boolean pointLocationListCheck() {
        return !(pointLocationList != null && !pointLocationList.isEmpty());
    }

    /**
     * check if there are any points
     * @return boolean 
     */
    public boolean pointListCheck() {
        return !(pointList != null && !pointList.isEmpty());
    }

    /**
     * check if there are any apSet Objects
     * @return boolean 
     */
    public boolean apSetListCheck() {
        return !(apSetList != null && !apSetList.isEmpty());
    }

    /**
     * check if there are any alSetParam Objects
     * @return boolean 
     */
    public boolean alSetParamListCheck() {
        return !(alSetParamList != null && !alSetParamList.isEmpty());
    }

    /**
     * Query the region list for all items that contain the given value
     * @param query, String Object
     * @return List Object of String Objects
     */
    public List<String> queryRegionList(String query) {
        List<String> list;
        
        try {
            list = new ArrayList(regionCountrySiteMap.keySet());
            return list.stream().filter(region -> region.toLowerCase().contains(query.toLowerCase())).sorted().collect(Collectors.toList());
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Query the country list for all items that contain the given value
     * @param query, String Object
     * @return List Object of String Objects
     */
    public List<String> queryCountryList(String query) {
        List<String> list;
        
        try {
            list = new ArrayList(regionCountrySiteMap.get(selectedFilters.getRegion()).keySet());
            return list.stream().filter(country -> country.toLowerCase().contains(query.toLowerCase())).sorted().collect(Collectors.toList());
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Query the siteName list for all items that contain the given value
     * @param query, String Object
     * @return List Object of SelectItem Objects
     */
    public List<SelectItem> querySiteNameList(String query) {
        try {
            return regionCountrySiteMap.get(selectedFilters.getRegion()).get(selectedFilters.getCountry()).stream().filter(site -> site.getLabel().toLowerCase().contains(query.toLowerCase())).sorted(Comparator.comparing(SelectItem::getLabel)).collect(Collectors.toList());
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Query the areaName list for all items that contain the given value
     * @param query, String Object
     * @return List Object of SelectItem Objects
     */
    public List<SelectItem> queryAreaNameList(String query) {
        try {
            return areaList.stream().filter(asset -> asset.getLabel().toLowerCase().contains(query.toLowerCase())).sorted(Comparator.comparing(SelectItem::getLabel)).collect(Collectors.toList());
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Query the assetName list for all items that contain the given value
     * @param query, String Object
     * @return List Object of SelectItem Objects
     */
    public List<SelectItem> queryAssetNameList(String query) {
        try {
            return assetList.stream().filter(asset -> asset.getLabel().toLowerCase().contains(query.toLowerCase())).sorted(Comparator.comparing(SelectItem::getLabel)).collect(Collectors.toList());
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Query the pointLocation list for all items that contain the given value
     * @param query, String Object
     * @return List Object of SelectItem Objects
     */
    public List<SelectItem> queryPointLocationList(String query) {
        try {
            return pointLocationList.stream().filter(pointLocation -> pointLocation.getLabel().toLowerCase().contains(query.toLowerCase())).sorted(Comparator.comparing(SelectItem::getLabel)).collect(Collectors.toList());
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Query the point list for all items that contain the given value
     * @param query, String Object
     * @return List Object of SelectItem Objects
     */
    public List<SelectItem> queryPointList(String query) {
        try {
            return pointList.stream().filter(point -> point.getLabel().toLowerCase().contains(query.toLowerCase())).sorted(Comparator.comparing(SelectItem::getLabel)).collect(Collectors.toList());
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Query the apSet list for all items that contain the given value
     * @param query, String Object
     * @return List Object of SelectItem Objects
     */
    public List<SelectItem> queryApSetList(String query) {
        try {
            return apSetList.stream().filter(ApSet -> ApSet.getLabel().toLowerCase().contains(query.toLowerCase())).sorted(Comparator.comparing(SelectItem::getLabel)).collect(Collectors.toList());
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Query the alSetParam list for all items that contain the given value
     * @param query, String Object
     * @return List Object of String Objects
     */
    public List<SelectItem> queryAlSetParamList(String query) {
        try {
            return alSetParamList.stream().filter(alSetParam -> alSetParam.getLabel().toLowerCase().contains(query.toLowerCase())).sorted(Comparator.comparing(SelectItem::getLabel)).collect(Collectors.toList());
        } catch (Exception e) {
            return null;
        }
    }

    public Filters getSelectedFilters() {
        return selectedFilters;
    }

    public void setSelectedFilters() {
        selectedFilters = new Filters();
    }

    public List<Short> getYearList() {
        return yearList;
    }
    
    public void setYearList() {
        yearList.clear();
        
        if (selectedFilters != null) {
            if (selectedFilters.getCreatedYear() > 2000) {
                for (short i = selectedFilters.getCreatedYear(); i >= 2000; i--) {
                    yearList.add(i);
                }
            }
        }
    }
    
    public abstract void resetPage();
    public abstract void onYearChange(boolean reloadUI);
    public abstract void onRegionChange(boolean reloadUI);
    public abstract void onCountryChange(boolean reloadUI);
    public abstract void onSiteChange(boolean reloadUI);
    public abstract void onAreaChange(boolean reloadUI);
    public abstract void onAssetChange(boolean reloadUI);
    public abstract void onPointLocationChange(boolean reloadUI);
    public abstract void onPointChange(boolean reloadUI);
    public abstract void onApSetChange(boolean reloadUI);
    public abstract void onAlSetParamChange(boolean reloadUI);
    public abstract void updateUI(String value);
    public abstract void updateBreadcrumbModel();
    public abstract void treeContentMenuBuilder(String type, Object data, TreeNode parent, List<Object> list);
    public abstract void onNodeExpand(NodeExpandEvent event);
    public abstract void onNodeCollapse(NodeCollapseEvent event);
    public abstract void onNodeSelect(NodeSelectEvent event);
    public abstract void setRegionCountrySiteMap();
}
