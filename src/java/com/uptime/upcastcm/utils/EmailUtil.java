/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.utils;

import com.uptime.client.http.client.ServiceRegistrarClient;
import com.uptime.upcastcm.http.client.EmailClient;
import com.uptime.upcastcm.http.client.UserClient;
import static com.uptime.upcastcm.utils.ApplicationConstants.DEFAULT_UPTIME_SIGNATURE;
import static com.uptime.upcastcm.utils.ApplicationConstants.EMAIL_SERVICE_NAME;
import static com.uptime.upcastcm.utils.ApplicationConstants.USER_PREFERENCE_SERVICE_NAME;
import com.uptime.upcastcm.utils.vo.EmailVO;
import com.uptime.upcastcm.utils.vo.LdapUserVO;
import com.uptime.upcastcm.utils.vo.PasswordTokenVO;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author madhavi
 */
public class EmailUtil {

    /**
     * Create password token and Send password reset link in email
     * @param ldapUserVO, LdapUserVO Object
     * @param customerAccount, String Object
     * @param host, String Object     
     * @param protocol, String Object
     * @return boolean, true if email sent successfully, else false
     */
    public static boolean createAndSendPasswordResetEmail(LdapUserVO ldapUserVO, String customerAccount, String host, String protocol) {
        StringBuilder passwordResetLink, messageBodyBuilder;
        PasswordTokenVO passwordTokenVO;
        List<String> toEmails;
        EmailVO emailVO;
        
        try {
            if (ldapUserVO != null && ldapUserVO.getUserId() != null && ldapUserVO.getMail() != null && ldapUserVO.getFirstName() != null && ldapUserVO.getLastName() != null && customerAccount != null && host != null && protocol != null) {
                
                // Create new Password Token
                passwordTokenVO = new PasswordTokenVO(ldapUserVO.getUserId(), UUID.randomUUID());

                // Insert Password Token in to DB
                if (UserClient.getInstance().createPasswordToken(ServiceRegistrarClient.getInstance().getServiceHostURL(USER_PREFERENCE_SERVICE_NAME), passwordTokenVO)) {
                    Logger.getLogger(EmailUtil.class.getName()).log(Level.INFO, "Token created for email: {0}", passwordTokenVO.getUserId() + " & token:" + passwordTokenVO.getPasswordToken());
                }

                // Create Password Reset Link
                passwordResetLink = new StringBuilder();
                passwordResetLink
                        .append(protocol).append("://").append(host).append("/UpCastCM/userPassword.xhtml")
                        .append("?email=").append(ldapUserVO.getMail())
                        .append("&uuid=").append(passwordTokenVO.getPasswordToken().toString());

                // Create Email Body
                messageBodyBuilder = new StringBuilder();
                messageBodyBuilder
                        .append("Dear ").append(ldapUserVO.getFirstName()).append(" ").append(ldapUserVO.getLastName()).append(",")
                        .append("<br/><br/>")
                        .append("We're sending you this email because you requested a password reset. You can reset your password ")
                        .append("<a href=\"").append(passwordResetLink).append("\">here</a>.")
                        .append("<br/>")
                        .append("This link will expire in 30 minutes. After that, you'll need to submit a new request in order to reset your password.")
                        .append("<br/><br/>")
                        .append("Thanks,")
                        .append("<br/><br/>")
                        .append(DEFAULT_UPTIME_SIGNATURE);

                // Add who the email is being sent to
                toEmails = new ArrayList();
                toEmails.add(ldapUserVO.getMail());

                // Construct Email
                emailVO = new EmailVO();
                emailVO.setFromEmail("DoNotReply@upcastcm.com");
                emailVO.setToEmails(toEmails);
                emailVO.setSubject("UpCastCM - Your request for a password reset");
                emailVO.setMailBody(messageBodyBuilder.toString());

                // Send Email
                return EmailClient.getInstance().sendEmail(ServiceRegistrarClient.getInstance().getServiceHostURL(EMAIL_SERVICE_NAME), emailVO);
            }
        } catch (Exception e) {
            Logger.getLogger(EmailUtil.class.getName()).log(Level.INFO, e.getMessage(), e);
        }
        return false;
    }

}
