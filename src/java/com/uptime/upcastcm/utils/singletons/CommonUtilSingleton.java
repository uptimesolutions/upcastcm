/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.utils.singletons;

import com.uptime.upcastcm.utils.vo.ApAlSetVO;
import com.uptime.upcastcm.utils.vo.DeviceVO;
import com.uptime.upcastcm.utils.vo.PointVO;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.faces.model.SelectItem;

/**
 *
 * @author kpati
 */
public class CommonUtilSingleton {

    private static final CommonUtilSingleton INSTANCE = new CommonUtilSingleton();

    private CommonUtilSingleton() {

    }

    public static CommonUtilSingleton getInstance() {
        return INSTANCE;
    }

    public String getDeviceType(String serialNumber) {
        char[] prefix;
        String deviceType = null;

        if (serialNumber != null && serialNumber.length() == 8) {
            prefix = new char[2];
            serialNumber = serialNumber.toUpperCase();
            serialNumber.getChars(0, 2, prefix, 0);

            switch (String.valueOf(prefix)) {
                case "BB":
                    deviceType = "MistLX";
                    break;
                case "00":
                    if (serialNumber.startsWith("001")) {
                        deviceType = "TS1X";
                    } else {
                        Logger.getLogger(CommonUtilSingleton.class.getName()).log(Level.INFO, "**The Serial Number:{0} isn't valid: {0}", serialNumber);
                    }
                    break;
                case "BA":
                    deviceType = "StormX";
                    break;
                case "BF":
                    deviceType = "StormXRTD";
                    break;
                case "BE":
                    deviceType = "StormXT";
                    break;
                default:
                    Logger.getLogger(CommonUtilSingleton.class.getName()).log(Level.INFO, "**DeviceType doesn't match with any serialNumber prefix: {0}", prefix);
            }
        } else {
            Logger.getLogger(CommonUtilSingleton.class.getName()).log(Level.INFO, "**The Serial Number isn't valid");
        }
        return deviceType;
    }

    public List<DeviceVO> populateDeviceTypeList(String deviceType) {
        List<DeviceVO> deviceTypeList = new ArrayList();
        switch (deviceType) {
            case "MistLX":
                deviceTypeList.add(new DeviceVO(0, "Ultrasonic", "AC", "Ultrasonic"));
                deviceTypeList.add(new DeviceVO(1, "Acceleration", "AC", "Acceleration"));
                deviceTypeList.add(new DeviceVO(0, "Temperature", "DC", "Temperature"));
                deviceTypeList.add(new DeviceVO(1, "Battery", "DC", "Voltage"));
                break;
            case "TS1X":
                deviceTypeList.add(new DeviceVO(0, "1", "AC", "Waveform"));
                deviceTypeList.add(new DeviceVO(1, "2", "AC", "Waveform"));
                deviceTypeList.add(new DeviceVO(2, "3", "AC", "Waveform"));
                deviceTypeList.add(new DeviceVO(4, "4", "AC", "Waveform"));
                deviceTypeList.add(new DeviceVO(5, "5", "AC", "Waveform"));
                deviceTypeList.add(new DeviceVO(6, "6", "AC", "Waveform"));
                deviceTypeList.add(new DeviceVO(7, "7", "AC", "Waveform"));
                deviceTypeList.add(new DeviceVO(3, "8", "AC", "Waveform"));
                deviceTypeList.add(new DeviceVO(0, "1", "DC", "Single-Measurement"));
                deviceTypeList.add(new DeviceVO(1, "2", "DC", "Single-Measurement"));
                deviceTypeList.add(new DeviceVO(2, "3", "DC", "Single-Measurement"));
                deviceTypeList.add(new DeviceVO(3, "4", "DC", "Single-Measurement"));
                deviceTypeList.add(new DeviceVO(4, "5", "DC", "Single-Measurement"));
                deviceTypeList.add(new DeviceVO(5, "6", "DC", "Single-Measurement"));
                deviceTypeList.add(new DeviceVO(6, "7", "DC", "Single-Measurement"));
                deviceTypeList.add(new DeviceVO(7, "8", "DC", "Single-Measurement"));
                deviceTypeList.add(new DeviceVO(8, "9", "DC", "Single-Measurement"));
                deviceTypeList.add(new DeviceVO(9, "10", "DC", "Single-Measurement"));
                deviceTypeList.add(new DeviceVO(10, "11", "DC", "Single-Measurement"));
                deviceTypeList.add(new DeviceVO(11, "12", "DC", "Single-Measurement"));
                deviceTypeList.add(new DeviceVO(12, "13", "DC", "Single-Measurement"));
                deviceTypeList.add(new DeviceVO(13, "14", "DC", "Single-Measurement"));
                deviceTypeList.add(new DeviceVO(14, "15", "DC", "Single-Measurement"));
                deviceTypeList.add(new DeviceVO(15, "16", "DC", "Single-Measurement"));
                break;
            case "StormX":
                deviceTypeList.add(new DeviceVO(5, "X", "AC", "Acceleration"));
                deviceTypeList.add(new DeviceVO(6, "Y", "AC", "Acceleration"));
                deviceTypeList.add(new DeviceVO(7, "Z", "AC", "Acceleration"));
                deviceTypeList.add(new DeviceVO(4, "Temperature", "DC", "Temperature"));
                deviceTypeList.add(new DeviceVO(7, "Battery", "DC", "Voltage"));
                break;
            case "StormXRTD":
                deviceTypeList.add(new DeviceVO(4, "Temp_RTD", "DC", "Temperature RTD"));
                deviceTypeList.add(new DeviceVO(3, "Battery", "DC", "Voltage"));
                deviceTypeList.add(new DeviceVO(7, "Sample_Count", "DC", "Count"));
                deviceTypeList.add(new DeviceVO(8, "Awake_Count", "DC", "Count"));
                deviceTypeList.add(new DeviceVO(9, "Radio_Count", "DC", "Count"));
                break;
            case "StormXT":
                deviceTypeList.add(new DeviceVO(0, "Temp_F", "DC", "Temperature"));
                deviceTypeList.add(new DeviceVO(1, "Temp_C", "DC", "Temperature"));
                deviceTypeList.add(new DeviceVO(2, "Relative_Humidity", "DC", "Relative Humidity"));
                deviceTypeList.add(new DeviceVO(3, "Battery", "DC", "Voltage"));
                deviceTypeList.add(new DeviceVO(7, "Sample_Count", "DC", "Count"));
                deviceTypeList.add(new DeviceVO(8, "Awake_Count", "DC", "Count"));
                deviceTypeList.add(new DeviceVO(9, "Radio_Count", "DC", "Count"));
                break;
            default:
                Logger.getLogger(CommonUtilSingleton.class.getName()).log(Level.INFO, "**deviceType doesn't match with any serialNumber : {0}", deviceType);
        }
        return deviceTypeList;
    }

    public SelectItem populateSensitivityAndOffsetValues(String deviceType, String channelType, int channelNmm) {

        if (deviceType.equalsIgnoreCase("STORMXRTD") && channelType.equalsIgnoreCase("DC") && channelNmm == 4) {
            return new SelectItem("0", "1000", "F");
        } else if (deviceType.equalsIgnoreCase("STORMXRTD") && channelType.equalsIgnoreCase("DC") && channelNmm == 3) {
            return new SelectItem("0", "1000", "V");
        } else if (deviceType.equalsIgnoreCase("STORMXRTD") && channelType.equalsIgnoreCase("DC") && channelNmm == 7) {
            return new SelectItem("0", "1000", "Count");
        } else if (deviceType.equalsIgnoreCase("STORMXRTD") && channelType.equalsIgnoreCase("DC") && channelNmm == 8) {
            return new SelectItem("0", "1000", "Count");
        } else if (deviceType.equalsIgnoreCase("STORMXRTD") && channelType.equalsIgnoreCase("DC") && channelNmm == 9) {
            return new SelectItem("0", "1000", "Count");
        } else if (deviceType.equalsIgnoreCase("STORMXT") && channelType.equalsIgnoreCase("DC") && channelNmm == 0) {
            return new SelectItem("0", "1000", "F");
        } else if (deviceType.equalsIgnoreCase("STORMXT") && channelType.equalsIgnoreCase("DC") && channelNmm == 1) {
            return new SelectItem("0", "1000", "C");
        } else if (deviceType.equalsIgnoreCase("STORMXT") && channelType.equalsIgnoreCase("DC") && channelNmm == 2) {
            return new SelectItem("0", "1000", "%RH");
        } else if (deviceType.equalsIgnoreCase("STORMXT") && channelType.equalsIgnoreCase("DC") && channelNmm == 3) {
            return new SelectItem("0", "1000", "V");
        } else if (deviceType.equalsIgnoreCase("STORMXT") && channelType.equalsIgnoreCase("DC") && channelNmm == 7) {
            return new SelectItem("0", "1000", "Count");
        } else if (deviceType.equalsIgnoreCase("STORMXT") && channelType.equalsIgnoreCase("DC") && channelNmm == 8) {
            return new SelectItem("0", "1000", "Count");
        } else if (deviceType.equalsIgnoreCase("STORMXT") && channelType.equalsIgnoreCase("DC") && channelNmm == 9) {
            return new SelectItem("0", "1000", "Count");
        } else if (deviceType.equalsIgnoreCase("MISTLX") && channelType.equalsIgnoreCase("AC") && channelNmm == 0) {
            return new SelectItem("0", "50", "Pascals");
        } else if (deviceType.equalsIgnoreCase("MISTLX") && channelType.equalsIgnoreCase("AC") && channelNmm == 1) {
            return new SelectItem("0", "50", "Gs");
        } else if (deviceType.equalsIgnoreCase("MISTLX") && channelType.equalsIgnoreCase("DC") && channelNmm == 0) {
            return new SelectItem("0", "1000", "F");
        } else if (deviceType.equalsIgnoreCase("MISTLX") && channelType.equalsIgnoreCase("DC") && channelNmm == 1) {
            return new SelectItem("0", "1000", "V");
        } else if (deviceType.equalsIgnoreCase("STORMX") && channelType.equalsIgnoreCase("AC") && channelNmm == 5) {
            return new SelectItem("0", "50", "Gs");
        } else if (deviceType.equalsIgnoreCase("STORMX") && channelType.equalsIgnoreCase("AC") && channelNmm == 6) {
            return new SelectItem("0", "50", "Gs");
        } else if (deviceType.equalsIgnoreCase("STORMX") && channelType.equalsIgnoreCase("AC") && channelNmm == 7) {
            return new SelectItem("0", "50", "Gs");
        } else if (deviceType.equalsIgnoreCase("STORMX") && channelType.equalsIgnoreCase("DC") && channelNmm == 5) {
            return new SelectItem("0", "7.228", "F");
        } else if (deviceType.equalsIgnoreCase("STORMX") && channelType.equalsIgnoreCase("DC") && channelNmm == 7) {
            return new SelectItem("0", "1968", "V");
        } else if (channelType.equalsIgnoreCase("AC")) {
            return new SelectItem("0", "100", "Gs");
        } else if (channelType.equalsIgnoreCase("DC")) {
            return new SelectItem("0", "7.228", "F");
        }
        return new SelectItem("", "");
    }

    public SelectItem populateSensitivityAndOffsetValuesForTS1X(String channelType, String sensorType) {

        if (channelType.equalsIgnoreCase("AC") && sensorType.equalsIgnoreCase("Acceleration")) {
            return new SelectItem("0", "100", "Gs");
        } else if (channelType.equalsIgnoreCase("AC") && sensorType.equalsIgnoreCase("Velocity")) {
            return new SelectItem("0", "100", "in/s");
        } else if (channelType.equalsIgnoreCase("AC") && sensorType.equalsIgnoreCase("Displacement")) {
            return new SelectItem("0", "100", "mils");
        } else if (channelType.equalsIgnoreCase("AC") && sensorType.equalsIgnoreCase("Ultrasonic")) {
            return new SelectItem("0", "100", "Pascals");
        } else if (channelType.equalsIgnoreCase("DC") && sensorType.equalsIgnoreCase("Temperature")) {
            return new SelectItem("0", "7.228", "F");
        } else if (channelType.equalsIgnoreCase("DC") && sensorType.equalsIgnoreCase("Frequency")) {
            return new SelectItem("0", "7.228", "Hz");
        } else if (channelType.equalsIgnoreCase("DC") && sensorType.equalsIgnoreCase("Pressure")) {
            return new SelectItem("0", "7.228", "Pascals");
        } else if (channelType.equalsIgnoreCase("DC") && sensorType.equalsIgnoreCase("Voltage")) {
            return new SelectItem("0", "7.228", "mV");
        } else if (channelType.equalsIgnoreCase("DC") && sensorType.equalsIgnoreCase("Current")) {
            return new SelectItem("0", "7.228", "mA");
        } else if (channelType.equalsIgnoreCase("DC") && sensorType.equalsIgnoreCase("Phase")) {
            return new SelectItem("0", "7.228", "Degrees");
        } else if (channelType.equalsIgnoreCase("DC") && sensorType.equalsIgnoreCase("Count")) {
            return new SelectItem("0", "7.228", "Count");
        } else if (channelType.equalsIgnoreCase("DC") && sensorType.equalsIgnoreCase("Relative Humidity")) {
            return new SelectItem("0", "7.228", "%RH");
        } else if (channelType.equalsIgnoreCase("DC") && sensorType.equalsIgnoreCase("Temperature RTD")) {
            return new SelectItem("0", "7.228", "F");
        }
        return new SelectItem("", "");
    }

    public String getSensorUnitBySensorType(String sensorType) {
        if (sensorType != null) {
            switch (sensorType) {
                case "Ultrasonic":
                    return "Pascals";
                case "Acceleration":
                    return "Gs";
                case "Voltage":
                    return "V";
                case "Temperature":
                    return "F";
            }
        }
        return "";
    }

    public String getPointNameBySensorType(String sensorType) {
        if (sensorType != null) {
            switch (sensorType) {
                case "Ultrasonic":
                    return "ULTRA";
                case "Acceleration":
                    return "VIBE";
                case "Voltage":
                    return "BATT";
                case "Temperature":
                    return "TEMP";
                case "Count":
                    return "CT";
                case "Relative Humidity":
                    return "RH";
                case "Temperature RTD":
                    return "TEMP RTD";
                case "Phase":
                    return "PH";
                case "Current":
                    return "CUR";
                case "Pressure":
                    return "P";
                case "Frequency":
                    return "FREQ";
                case "Displacement":
                    return "S";
                case "Velocity":
                    return "V";
            }
        }
        return "";
    }

    public void updatePointForInstallNewDevice(PointVO pointVO, String serialNumber) throws Exception {
        String deviceType;

        if (serialNumber != null && pointVO != null && (deviceType = getDeviceType(serialNumber)) != null) {
            switch (deviceType) {
                case "MistLX":
                    switch (pointVO.getSensorType()) {
                        case "Ultrasonic":
                            pointVO.setDisabled(false);
                            pointVO.setSensorChannelNum(0);
                            pointVO.setSensorOffset(0);
                            pointVO.setSensorSensitivity(50);
                            pointVO.setSensorUnits("Pascals");
                            break;
                        case "Acceleration":
                            pointVO.setDisabled(false);
                            pointVO.setSensorChannelNum(1);
                            pointVO.setSensorOffset(0);
                            pointVO.setSensorSensitivity(50);
                            pointVO.setSensorUnits("Gs");
                            break;
                        case "Temperature":
                            pointVO.setDisabled(false);
                            pointVO.setSensorChannelNum(0);
                            pointVO.setSensorOffset(0);
                            pointVO.setSensorSensitivity(1000);
                            pointVO.setSensorUnits("F");
                            break;
                        case "Voltage":
                            pointVO.setDisabled(false);
                            pointVO.setSensorChannelNum(1);
                            pointVO.setSensorOffset(0);
                            pointVO.setSensorSensitivity(1000);
                            pointVO.setSensorUnits("V");
                            break;
                    }
                    break;
                case "TS1X": // TO DO
                    break;
                case "StormX":
                    //Set Channel Number only for DC Channel
                    switch (pointVO.getSensorType()) {
                        case "Acceleration":
                            pointVO.setSensorSubType("Piezo");
                            pointVO.setSensorSensitivity(50);
                            break;
                        case "Temperature":
                            pointVO.setSensorChannelNum(4);
                            pointVO.setSensorSensitivity(7.228f);
                            break;
                        case "Voltage":
                            pointVO.setSensorChannelNum(7);
                            pointVO.setSensorSensitivity(1968);
                            break;
                    }
                    break;
                case "StormXRTD": // TO DO
                    break;
                case "StormXT": // TO DO
                    break;
            }
        }
    }

    public void sortPointsByPointType(List<PointVO> pointVOList) {
        
        if (pointVOList != null && !pointVOList.isEmpty()) {
            //StormX
            if (pointVOList.size() == 5 && pointVOList.stream().filter(p -> p.getPointType().equals("AC")).collect(Collectors.toList()).size() == 3) {
                List<PointVO> missingACSensorChannlList = new ArrayList();
                Set<Integer> validACChannelNumSet = new HashSet();
                for (PointVO p : pointVOList) {
                    switch (p.getSensorType()) {
                        case "Acceleration":
                            switch (p.getSensorChannelNum()) {
                                case 5:
                                    p.setPointSerialNumber(1);
                                    break;
                                case 6:
                                    p.setPointSerialNumber(2);
                                    break;
                                case 7:
                                    p.setPointSerialNumber(3);
                                    break;
                                default:
                                    missingACSensorChannlList.add(p);
                                    break;
                            }
                            break;
                        case "Temperature":
                            p.setPointSerialNumber(4);
                            break;
                        case "Voltage":
                            p.setPointSerialNumber(5);
                            break;
                        default:
                            break;
                    }
                }

                pointVOList.stream().filter(p -> (p.getPointType().equals("AC"))).forEachOrdered(p -> {
                    validACChannelNumSet.add(p.getSensorChannelNum());
                });

                //when AC(Acceleration) channel nums are missing/invalid
                if (!missingACSensorChannlList.isEmpty() || validACChannelNumSet.size() != 3) {
                    missingACSensorChannlList.clear();
                    missingACSensorChannlList.addAll(pointVOList.stream().filter(p -> p.getPointType().equals("AC")).collect(Collectors.toList()));
                    missingACSensorChannlList.sort(Comparator.comparing(PointVO::getPointName));
                    AtomicInteger counter = new AtomicInteger(1);
                    missingACSensorChannlList.forEach(_item -> {
                        _item.setPointSerialNumber(counter.getAndIncrement());
                    });
                }

            } else {
                //All others Device points.
                final List<PointVO> acPointsList = new ArrayList();
                final List<PointVO> dcPointsList = new ArrayList();
                AtomicInteger counter = new AtomicInteger(1);
                pointVOList.forEach(p -> {
                    if (p.getPointType().equals("AC")) {
                        acPointsList.add(p);
                    } else {
                        dcPointsList.add(p);
                    }
                });

                acPointsList.forEach(acp -> acp.setPointSerialNumber(counter.getAndIncrement()));
                dcPointsList.forEach(dcp -> dcp.setPointSerialNumber(counter.getAndIncrement()));
            }
            
            pointVOList.sort(Comparator.comparing(PointVO::getPointSerialNumber));
            
        }
    }

    public boolean isValidPointListForInstallNewDevice(List<PointVO> pointVOList, String serialNumber) throws Exception {
        String deviceType;
        boolean ultra, accel, temp, volt;

        if (serialNumber != null && pointVOList != null && (deviceType = getDeviceType(serialNumber)) != null) {
            if (deviceType.equals("MistLX") && pointVOList.size() == 4) {
                ultra = false;
                accel = false;
                temp = false;
                volt = false;

                for (PointVO pVO : pointVOList) {
                    switch (pVO.getSensorType()) {
                        case "Ultrasonic":
                            if (!pVO.getPointType().equals("AC")) {
                                return false;
                            } else if (ultra) {
                                return false;
                            } else {
                                ultra = true;
                            }
                            break;
                        case "Acceleration":
                            if (!pVO.getPointType().equals("AC")) {
                                return false;
                            } else if (accel) {
                                return false;
                            } else {
                                accel = true;
                            }
                            break;
                        case "Temperature":
                            if (!pVO.getPointType().equals("DC")) {
                                return false;
                            } else if (temp) {
                                return false;
                            } else {
                                temp = true;
                            }
                            break;
                        case "Voltage":
                            if (!pVO.getPointType().equals("DC")) {
                                return false;
                            } else if (volt) {
                                return false;
                            } else {
                                volt = true;
                            }
                            break;
                        default:
                            return false;
                    }
                }
                return true;
            } else if (deviceType.equals("TS1X") && pointVOList.size() == 24) {
                // TO DO
            } else if (deviceType.equals("StormX") && pointVOList.size() == 5) {

                accel = pointVOList.stream().filter(p -> p.getSensorType().equals("Acceleration")).collect(Collectors.toList()).size() != 3;
                temp = pointVOList.stream().filter(p -> p.getSensorType().equals("Temperature")).collect(Collectors.toList()).size() != 1;
                volt = pointVOList.stream().filter(p -> p.getSensorType().equals("Voltage")).collect(Collectors.toList()).size() != 1;

                for (PointVO pVO : pointVOList) {
                    switch (pVO.getSensorType()) {
                        case "Acceleration":
                            if (!pVO.getPointType().equals("AC")) {
                                return false;
                            } else if (accel) {
                                return false;
                            } else if (pVO.getSensorOffset() != Float.parseFloat("0")) {
                                return false;
                            } else if (pVO.getSensorSensitivity() != Float.parseFloat("0") && pVO.getSensorSensitivity() != Float.parseFloat("50")) {
                                return false;
                            } else if (pVO.getSensorUnits() != null && !pVO.getSensorUnits().isEmpty() && !pVO.getSensorUnits().equals("Gs")) {
                                return false;
                            } else if (pVO.getSensorLocalOrientation() != null && !pVO.getSensorLocalOrientation().isEmpty() && !Arrays.asList("X", "Y", "Z").contains(pVO.getSensorLocalOrientation())) {
                                return false;
                            } else if (pVO.getSensorSubType() != null && !pVO.getSensorSubType().isEmpty() && !pVO.getSensorSubType().equals("Piezo")) {
                                return false;
                            }
                            //Logger.getLogger(CommonUtilSingleton.class.getName()).log(Level.INFO, "*********************pVO.getSensorSensitivity() : {0}   ***********pVO.getSensorOffset(): {1}   ******************getSensorChannelNum: {2} ", new Object[]{pVO.getSensorSensitivity(), pVO.getSensorOffset(), pVO.getSensorChannelNum()});
                            break;
                        case "Temperature":
                            if (!pVO.getPointType().equals("DC")) {
                                return false;
                            } else if (temp) {
                                return false;
                            } else if (pVO.getSensorOffset() != Float.parseFloat("0")) {
                                return false;
                            } else if (pVO.getSensorSensitivity() != Float.parseFloat("0") && pVO.getSensorSensitivity() != Float.parseFloat("7.228")) {
                                return false;
                            } else if (pVO.getSensorUnits() != null && !pVO.getSensorUnits().isEmpty() && !pVO.getSensorUnits().equals("F")) {
                                return false;
                            } else if (pVO.getSensorLocalOrientation() != null && !pVO.getSensorLocalOrientation().isEmpty()) {
                                return false;
                            } else if (pVO.getSensorSubType() != null && !pVO.getSensorSubType().isEmpty()) {
                                return false;
                            }
                            break;
                        case "Voltage":
                            if (!pVO.getPointType().equals("DC")) {
                                return false;
                            } else if (volt) {
                                return false;
                            } else if (pVO.getSensorOffset() != Float.parseFloat("0")) {
                                return false;
                            } else if (pVO.getSensorSensitivity() != Float.parseFloat("0") && pVO.getSensorSensitivity() != Float.parseFloat("1968")) {
                                return false;
                            } else if (pVO.getSensorUnits() != null && !pVO.getSensorUnits().isEmpty() && !pVO.getSensorUnits().equals("V")) {
                                return false;
                            } else if (pVO.getSensorLocalOrientation() != null && !pVO.getSensorLocalOrientation().isEmpty()) {
                                return false;
                            } else if (pVO.getSensorSubType() != null && !pVO.getSensorSubType().isEmpty()) {
                                return false;
                            }
                            //Logger.getLogger(CommonUtilSingleton.class.getName()).log(Level.INFO, "*********************pVO to be set : {0}", pVO);
                            break;
                        default:
                            return false;
                    }
                }
                return true;
            } else if (deviceType.equals("StormXRTD") && pointVOList.size() == 5) {
                // TO DO
            } else if (deviceType.equals("StormXT") && pointVOList.size() == 7) {
                // TO DO
            }
        }
        return false;
    }

    public boolean isMissingChannelNumberForStormXDevice(List<PointVO> pointVOList, String serialNumber) throws Exception {
        String deviceType;
        if (serialNumber != null && pointVOList != null && (deviceType = getDeviceType(serialNumber)) != null) {
            if (deviceType.equals("StormX") && pointVOList.size() == 5) {

                for (PointVO pVO : pointVOList) {
                    switch (pVO.getSensorType()) {
                        case "Acceleration":
                            if (!Arrays.asList(5, 6, 7).contains(pVO.getSensorChannelNum())) {
                                return false;
                            }
                            //Logger.getLogger(CommonUtilSingleton.class.getName()).log(Level.INFO, "*********************pVO.getSensorSensitivity() : {0}   ***********pVO.getSensorOffset(): {1}   ******************getSensorChannelNum: {2} ", new Object[]{pVO.getSensorSensitivity(), pVO.getSensorOffset(), pVO.getSensorChannelNum()});
                            break;
                        case "Temperature":
                            if (pVO.getSensorChannelNum() != 4) {
                                return false;
                            }
                            break;
                        case "Voltage":
                            if (pVO.getSensorChannelNum() != 7) {
                                return false;
                            }
                            //Logger.getLogger(CommonUtilSingleton.class.getName()).log(Level.INFO, "*********************pVO to be set : {0}", pVO);
                            break;
                        default:
                            return false;
                    }
                }
                return true;
            } else if (deviceType.equals("StormXRTD") && pointVOList.size() == 5) {
                // TO DO
            } else if (deviceType.equals("StormXT") && pointVOList.size() == 7) {
                // TO DO
            }
        }
        return false;
    }

    /**
     * perform the given action when the fmax field is changed in the given
     * Object
     *
     * @param apAlSetVO
     */
    public void onFmaxChange(ApAlSetVO apAlSetVO) {
        if (apAlSetVO != null) {
            switch (apAlSetVO.getFmax()) {
                case 61:
                    apAlSetVO.setSampleRate((float) 156.25);
                    break;
                case 122:
                    apAlSetVO.setSampleRate((float) 312.5);
                    break;
                case 244:
                    apAlSetVO.setSampleRate(625);
                    break;
                case 488:
                    apAlSetVO.setSampleRate(1250);
                    break;
                case 977:
                    apAlSetVO.setSampleRate(2500);
                    break;
                case 1953:
                    apAlSetVO.setSampleRate(5000);
                    break;
                case 3906:
                    apAlSetVO.setSampleRate(10000);
                    break;
                case 7813:
                    apAlSetVO.setSampleRate(20000);
                    break;
                default:
                    apAlSetVO.setSampleRate(0);
            }

            if (apAlSetVO.getResolution() > 0 && apAlSetVO.getSampleRate() > 0) {
                onResolutionChange(apAlSetVO);
            }
        }
    }

    /**
     * perform the given action when the resolution field is changed in the
     * given Object
     *
     * @param apAlSetVO
     */
    private void onResolutionChange(ApAlSetVO apAlSetVO) {
        if (apAlSetVO != null && apAlSetVO.getSampleRate() > 0) {
            switch (apAlSetVO.getResolution()) {
                case 100:
                    apAlSetVO.setPeriod((1 / apAlSetVO.getSampleRate()) * 256);
                    break;
                case 200:
                    apAlSetVO.setPeriod((1 / apAlSetVO.getSampleRate()) * 512);
                    break;
                case 400:
                    apAlSetVO.setPeriod((1 / apAlSetVO.getSampleRate()) * 1024);
                    break;
                case 800:
                    apAlSetVO.setPeriod((1 / apAlSetVO.getSampleRate()) * 2048);
                    break;
                case 1600:
                    apAlSetVO.setPeriod((1 / apAlSetVO.getSampleRate()) * 4096);
                    break;
                case 3200:
                    apAlSetVO.setPeriod((1 / apAlSetVO.getSampleRate()) * 8192);
                    break;
                case 6400:
                    apAlSetVO.setPeriod((1 / apAlSetVO.getSampleRate()) * 16384);
                    break;
                case 12800:
                    apAlSetVO.setPeriod((1 / apAlSetVO.getSampleRate()) * 32768);
                    break;
                case 25600:
                    apAlSetVO.setPeriod((1 / apAlSetVO.getSampleRate()) * 65536);
                    break;
                case 51200:
                    apAlSetVO.setPeriod((1 / apAlSetVO.getSampleRate()) * 131072);
                    break;
                default:
                    apAlSetVO.setPeriod(0);
            }
        }
    }

    public static <T> Predicate<T> distinctByKey(Function<? super T, Object> keyExtractor) {
        Map<Object, Boolean> map;

        try {
            map = new ConcurrentHashMap();
            return t -> map.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
        } catch (Exception e) {
            Logger.getLogger(CommonUtilSingleton.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
        return null;
    }

    public List<Integer> getAcChannelNumbersList() {
        return new ArrayList(Arrays.asList(5, 6, 7));
    }

    public List<Integer> getDcChannelNumbersList() {
        return new ArrayList(Arrays.asList(4, 7));
    }
}
