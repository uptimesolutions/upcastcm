/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.utils.converter;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.uptime.client.utils.JsonConverterUtil;
import com.uptime.upcastcm.utils.vo.DevicePresetVO;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author kpati
 */
@FacesConverter("devicePresetVOConverter")
public class SiteDevicePresetVOConverter implements Converter {
    
    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String value) {
        DevicePresetVO devicePresetVO = null; 
        JsonElement element;
        JsonObject jsonObject;
        
        if(value != null && !value.isEmpty()) {
            if(value.equals("Not DevicePresetVO")) {
                return null;
            }
            try {
                if ((element = JsonParser.parseString(value)) != null && (jsonObject = element.getAsJsonObject()) != null) {
                    devicePresetVO = new DevicePresetVO();
                    if (jsonObject.has("presetId")) {
                        try {
                            devicePresetVO.setPresetId(UUID.fromString(jsonObject.get("presetId").getAsString()));
                        }catch (Exception ex) {}
                    }
                    if (jsonObject.has("customerAccount")) {
                        try {
                            devicePresetVO.setCustomerAccount(jsonObject.get("customerAccount").getAsString());
                        }catch (Exception ex) {}
                    }
                    if (jsonObject.has("siteId")) {
                        try {
                            devicePresetVO.setSiteId(UUID.fromString(jsonObject.get("siteId").getAsString()));
                        }catch (Exception ex) {}
                    }
                    if (jsonObject.has("deviceType")) {
                        try {
                            devicePresetVO.setDeviceType(jsonObject.get("deviceType").getAsString());
                        }catch (Exception ex) {}
                    }
                    if (jsonObject.has("channelType")) {
                        try {
                            devicePresetVO.setChannelType(jsonObject.get("channelType").getAsString());
                        }catch (Exception ex) {}
                    }
                    if (jsonObject.has("channelNum")) {
                        try {
                            devicePresetVO.setChannelNumber(jsonObject.get("channelNum").getAsByte());
                        }catch (Exception ex) {}
                    }
                }
            } catch (Exception e) {
                Logger.getLogger(SiteDevicePresetVOConverter.class.getName()).log(Level.SEVERE, e.getMessage(), e);
                devicePresetVO = null;
            }
        }
        return devicePresetVO;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object object) {
        if (object != null) {
            if (object instanceof DevicePresetVO) {
                try {
                    return JsonConverterUtil.toJSON((DevicePresetVO)object);
                } catch (Exception e) {
                    Logger.getLogger(SiteDevicePresetVOConverter.class.getName()).log(Level.SEVERE, e.getMessage(), e);
                }
            } else {
                Logger.getLogger(SiteDevicePresetVOConverter.class.getName()).log(Level.SEVERE, "Error: Unknown Object type sent.");
            }
        }
        return "Not DevicePresetVO";
    }
    
}
