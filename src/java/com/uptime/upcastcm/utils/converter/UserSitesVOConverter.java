/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.utils.converter;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.uptime.client.utils.JsonConverterUtil;
import com.uptime.upcastcm.utils.vo.UserSitesVO;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author twilcox
 */
@FacesConverter("userSitesVOConverter")
public class UserSitesVOConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String value) {
        UserSitesVO userSitesVO = null; 
        JsonElement element;
        JsonObject jsonObject;
        
        if(value != null && !value.isEmpty()) {
            if(value.equals("Not UserSitesVO")) {
                return null;
            }
            try {
                if ((element = JsonParser.parseString(value)) != null && (jsonObject = element.getAsJsonObject()) != null) {
                    userSitesVO = new UserSitesVO();
                    if (jsonObject.has("userId")) {
                        try {
                            userSitesVO.setUserId(jsonObject.get("userId").getAsString());
                        }catch (Exception ex) {}
                    }
                    if (jsonObject.has("customerAccount")) {
                        try {
                            userSitesVO.setCustomerAccount(jsonObject.get("customerAccount").getAsString());
                        }catch (Exception ex) {}
                    }
                    if (jsonObject.has("siteId")) {
                        try {
                            userSitesVO.setSiteId(UUID.fromString(jsonObject.get("siteId").getAsString()));
                        }catch (Exception ex) {}
                    }
                    if (jsonObject.has("siteName")) {
                        try {
                            userSitesVO.setSiteName(jsonObject.get("siteName").getAsString());
                        }catch (Exception ex) {}
                    }
                    if (jsonObject.has("siteRole")) {
                        try {
                            userSitesVO.setSiteRole(jsonObject.get("siteRole").getAsString());
                        }catch (Exception ex) {}
                    }
                }
            } catch (Exception e) {
                Logger.getLogger(UserSitesVOConverter.class.getName()).log(Level.SEVERE, e.getMessage(), e);
                userSitesVO = null;
            }
        }
        return userSitesVO;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object object) {
        if (object != null) {
            if (object instanceof UserSitesVO) {
                try {
                    return JsonConverterUtil.toJSON((UserSitesVO)object);
                } catch (Exception e) {
                    Logger.getLogger(UserSitesVOConverter.class.getName()).log(Level.SEVERE, e.getMessage(), e);
                }
            } else {
                Logger.getLogger(UserSitesVOConverter.class.getName()).log(Level.SEVERE, "Error: Unknown Object type sent.");
            }
        }
        return "Not UserSitesVO";
    }
}
