/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.utils.converter;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.uptime.client.utils.JsonConverterUtil;
import com.uptime.upcastcm.utils.vo.UUIDItemVO;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author twilcox
 */
@FacesConverter("uuidItemVOConverter")
public class UUIDItemVOConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String value) {
        UUIDItemVO uUIDItemVO = null; 
        JsonElement element;
        JsonObject jsonObject;
        
        if(value != null && !value.isEmpty()) {
            if(value.equals("Not UUIDItemVO")) {
                return null;
            }
            try {
                if ((element = JsonParser.parseString(value)) != null && (jsonObject = element.getAsJsonObject()) != null) {
                    uUIDItemVO = new UUIDItemVO();
                    if (jsonObject.has("label")) {
                        try {
                            uUIDItemVO.setLabel(jsonObject.get("label").getAsString());
                        }catch (Exception ex) {}
                    }
                    if (jsonObject.has("value")) {
                        try {
                            uUIDItemVO.setValue(UUID.fromString(jsonObject.get("value").getAsString()));
                        }catch (Exception ex) {}
                    }
                    if (jsonObject.has("description")) {
                        try {
                            uUIDItemVO.setDescription(jsonObject.get("description").getAsString());
                        }catch (Exception ex) {}
                    }
                }
            } catch (Exception e) {
                Logger.getLogger(UUIDItemVOConverter.class.getName()).log(Level.SEVERE, e.getMessage(), e);
                uUIDItemVO = null;
            }
        }
        return uUIDItemVO;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object object) {
        if (object != null) {
            if (object instanceof UUIDItemVO) {
                try {
                    return JsonConverterUtil.toJSON((UUIDItemVO)object);
                } catch (Exception e) {
                    Logger.getLogger(UUIDItemVOConverter.class.getName()).log(Level.SEVERE, e.getMessage(), e);
                }
            } else {
                Logger.getLogger(UUIDItemVOConverter.class.getName()).log(Level.SEVERE, "Error: Unknown Object type sent.");
            }
        }
        return "Not UserSitesVO";
    }
}
