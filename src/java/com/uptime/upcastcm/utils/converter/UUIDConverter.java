/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.utils.converter;

import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author twilcox
 */
@FacesConverter("uuidConverter")
public class UUIDConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String value) {
        if (value != null) {
            if (value.equals("Not UUID") || value.equals("Select One")) {
                return null;
            }

            try {
                return UUID.fromString(value);
            } catch (Exception e) {
                Logger.getLogger(UUIDConverter.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            }
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object object) {
        if (object != null) {
            if (object instanceof UUID) {
                return object.toString();
            }
        }
        return "Not UUID";
    }
}
