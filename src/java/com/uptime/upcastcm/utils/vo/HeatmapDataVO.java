/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.upcastcm.utils.vo;

import java.util.List;
import java.util.Map;

/**
 *
 * @author gopal
 */
public class HeatmapDataVO {

    private List<String> paramNames;
    private List<String> pointNames;
    private List<List<Object>> dataset;
    private List<Map<String, Object>> customDataSet;

    // Constructor
    public HeatmapDataVO(List<String> paramNames, List<String> pointNames, List<List<Object>> dataset, List<Map<String, Object>> customDataSet) {
        this.paramNames = paramNames;
        this.pointNames = pointNames;
        this.dataset = dataset;
        this.customDataSet = customDataSet;
    }
    // Getters and Setters for all fields

    public List<String> getParamNames() {
        return paramNames;
    }

    public void setParamNames(List<String> paramNames) {
        this.paramNames = paramNames;
    }

    public List<String> getPointNames() {
        return pointNames;
    }

    public void setPointNames(List<String> pointNames) {
        this.pointNames = pointNames;
    }

    public List<List<Object>> getDataset() {
        return dataset;
    }

    public void setDataset(List<List<Object>> dataset) {
        this.dataset = dataset;
    }

    public List<Map<String, Object>> getCustomDataSet() {
        return customDataSet;
    }  // New getter

    public void setCustomDataSet(List<Map<String, Object>> customDataSet) {
        this.customDataSet = customDataSet;
    }  // New setter

    @Override
    public String toString() {
        return "HeatmapDataVO [paramNames=" + paramNames + ", pointNames=" + pointNames + ", dataset=" + dataset
                + ", customDataSet=" + customDataSet + "]";
    }

}
