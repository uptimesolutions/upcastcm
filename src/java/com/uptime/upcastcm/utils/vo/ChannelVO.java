/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.utils.vo;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

/**
 *
 * @author madhavi
 */
public class ChannelVO implements Serializable {

    private String customerAccount;
    private UUID siteId;
    private UUID areaId;
    private UUID assetId;
    private UUID pointLocationId;
    private UUID pointId;
    private UUID apSetId;
    private UUID alSetId;
    private String pointName;
    private String pointType;
    private int sensorChannelNum;
    private float sensorOffset;
    private float sensorSensitivity;
    private String sensorType;
    private String sensorUnits;

    private String siteName;
    private String areaName;
    private String assetName;
    private String pointLocationName;
    private String pointLocationDesc;
    private String apSetName;
    private String alSetName;
    private short basestationPortNum;
    private String ffSetNames;
    private String ffSetIds;
    private String tachName;
    private UUID tachId;
    private short sampleInterval;
    private boolean alarmEnabled;
    private float rollDiameter;
    private String rollDiameterUnits;
    private float speedRatio;
    private String tachReferenceUnits;
    Set<String> ffSetName;

    private String paramName;
    private boolean demodEnabled;
    private float demodHighPass;
    private float demodLowPass;
    private int dwell;
    private int fMax;
    private int demodInterval;
    private String paramType;
    private String freqUnits;
    private float highAlert;
    private boolean highAlertActive;
    private float highFault;
    private boolean highFaultActive;
    private boolean customized;
    private float lowAlert;
    private boolean lowAlertActive;
    private float lowFault;
    private boolean lowFaultActive;
    private float maxFreq;
    private float minFreq;
    private String paramAmpFactor;
    private String paramUnits;
    private float period;
    private int resolution;
    private float sampleRate;
    private List<ApAlSetVO> apAlSetVOs;
    private String deviceId;
    private Boolean hwDisabled;
    private boolean newChannel;
    private String alarmEnabledValue;

    public ChannelVO() {
    }

    public ChannelVO(ChannelVO channelVO) {
        customerAccount = channelVO.getCustomerAccount();
        siteId = channelVO.getSiteId();
        areaId = channelVO.getAreaId();
        assetId = channelVO.getAssetId();
        pointLocationId = channelVO.getPointLocationId();
        pointId = channelVO.getPointId();
        apSetId = channelVO.getApSetId();
        alSetId = channelVO.getAlSetId();
        pointName = channelVO.getPointName();
        pointType = channelVO.getPointType();
        sensorChannelNum = channelVO.getSensorChannelNum();
        sensorOffset = channelVO.getSensorOffset();
        sensorSensitivity = channelVO.getSensorSensitivity();
        sensorType = channelVO.getSensorType();
        sensorUnits = channelVO.getSensorUnits();
        paramName = channelVO.getParamName();
        demodEnabled = channelVO.isDemodEnabled();
        demodHighPass = channelVO.getDemodHighPass();
        demodLowPass = channelVO.getDemodLowPass();
        dwell = channelVO.getDwell();
        fMax = channelVO.getfMax();
        siteName = channelVO.getSiteName();
        areaName = channelVO.getAreaName();
        assetName = channelVO.getAssetName();
        pointLocationName = channelVO.getPointLocationName();
        pointLocationDesc = channelVO.getPointLocationDesc();
        apSetName = channelVO.getApSetName();
        alSetName = channelVO.getAlSetName();
        demodInterval = channelVO.getDemodInterval();
        paramType = channelVO.getParamType();
        alarmEnabled = channelVO.isAlarmEnabled();
        freqUnits = channelVO.getFreqUnits();
        highAlert = channelVO.getHighAlert();
        highAlertActive = channelVO.isHighAlertActive();
        highFault = channelVO.getHighFault();
        highFaultActive = channelVO.isHighFaultActive();
        customized = channelVO.isCustomized();
        lowAlert = channelVO.getLowAlert();
        lowAlertActive = channelVO.isLowAlertActive();
        lowFault = channelVO.getLowFault();
        lowFaultActive = channelVO.isLowFaultActive();
        maxFreq = channelVO.getMaxFreq();
        minFreq = channelVO.getMinFreq();
        paramAmpFactor = channelVO.getParamAmpFactor();
        paramUnits = channelVO.getParamUnits();
        period = channelVO.getPeriod();
        resolution = channelVO.getResolution();
        sampleRate = channelVO.getSampleRate();
        apAlSetVOs = channelVO.getApAlSetVOs();
        ffSetNames = channelVO.getFfSetNames();
        ffSetIds = channelVO.getFfSetIds();
        speedRatio = channelVO.getSpeedRatio();
        sampleInterval = channelVO.getSampleInterval();
        tachName = channelVO.getTachName();
        tachId = channelVO.getTachId();
        alarmEnabled = channelVO.isAlarmEnabled();
        basestationPortNum = channelVO.getBasestationPortNum();
        rollDiameter = channelVO.getRollDiameter();
        rollDiameterUnits = channelVO.getRollDiameterUnits();
        tachReferenceUnits = channelVO.getTachReferenceUnits();
        deviceId = channelVO.getDeviceId();
        hwDisabled = channelVO.getHwDisabled();
        newChannel = channelVO.isNewChannel();
        ffSetName = channelVO.getFfSetName();
        alarmEnabledValue = channelVO.getAlarmEnabledValue();
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public UUID getAreaId() {
        return areaId;
    }

    public void setAreaId(UUID areaId) {
        this.areaId = areaId;
    }

    public UUID getAssetId() {
        return assetId;
    }

    public void setAssetId(UUID assetId) {
        this.assetId = assetId;
    }

    public UUID getPointLocationId() {
        return pointLocationId;
    }

    public void setPointLocationId(UUID pointLocationId) {
        this.pointLocationId = pointLocationId;
    }

    public UUID getPointId() {
        return pointId;
    }

    public void setPointId(UUID pointId) {
        this.pointId = pointId;
    }

    public UUID getApSetId() {
        return apSetId;
    }

    public void setApSetId(UUID apSetId) {
        this.apSetId = apSetId;
    }

    public UUID getAlSetId() {
        return alSetId;
    }

    public void setAlSetId(UUID alSetId) {
        this.alSetId = alSetId;
    }

    public String getPointName() {
        return pointName;
    }

    public void setPointName(String pointName) {
        this.pointName = pointName;
    }

    public String getPointType() {
        return pointType;
    }

    public void setPointType(String pointType) {
        this.pointType = pointType;
    }

    public int getSensorChannelNum() {
        return sensorChannelNum;
    }

    public void setSensorChannelNum(int sensorChannelNum) {
        this.sensorChannelNum = sensorChannelNum;
    }

    public float getSensorOffset() {
        return sensorOffset;
    }

    public void setSensorOffset(float sensorOffset) {
        this.sensorOffset = sensorOffset;
    }

    public float getSensorSensitivity() {
        return sensorSensitivity;
    }

    public void setSensorSensitivity(float sensorSensitivity) {
        this.sensorSensitivity = sensorSensitivity;
    }

    public String getSensorType() {
        return sensorType;
    }

    public void setSensorType(String sensorType) {
        this.sensorType = sensorType;
    }

    public String getSensorUnits() {
        return sensorUnits;
    }

    public void setSensorUnits(String sensorUnits) {
        this.sensorUnits = sensorUnits;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getAssetName() {
        return assetName;
    }

    public void setAssetName(String assetName) {
        this.assetName = assetName;
    }

    public String getPointLocationName() {
        return pointLocationName;
    }

    public void setPointLocationName(String pointLocationName) {
        this.pointLocationName = pointLocationName;
    }

    public String getPointLocationDesc() {
        return pointLocationDesc;
    }

    public void setPointLocationDesc(String pointLocationDesc) {
        this.pointLocationDesc = pointLocationDesc;
    }
    

    public String getApSetName() {
        return apSetName;
    }

    public void setApSetName(String apSetName) {
        this.apSetName = apSetName;
    }

    public String getAlSetName() {
        return alSetName;
    }

    public void setAlSetName(String alSetName) {
        this.alSetName = alSetName;
    }

    public short getBasestationPortNum() {
        return basestationPortNum;
    }

    public void setBasestationPortNum(short basestationPortNum) {
        this.basestationPortNum = basestationPortNum;
    }

    public String getFfSetNames() {
        return ffSetNames;
    }

    public void setFfSetNames(String ffSetNames) {
        this.ffSetNames = ffSetNames;
    }

    public String getFfSetIds() {
        return ffSetIds;
    }

    public void setFfSetIds(String ffSetIds) {
        this.ffSetIds = ffSetIds;
    }

    public String getTachName() {
        return tachName;
    }

    public void setTachName(String tachName) {
        this.tachName = tachName;
    }

    public UUID getTachId() {
        return tachId;
    }

    public void setTachId(UUID tachId) {
        this.tachId = tachId;
    }

    public short getSampleInterval() {
        return sampleInterval;
    }

    public void setSampleInterval(short sampleInterval) {
        this.sampleInterval = sampleInterval;
    }

    public boolean isAlarmEnabled() {
        return alarmEnabled;
    }

    public void setAlarmEnabled(boolean alarmEnabled) {
        this.alarmEnabled = alarmEnabled;
    }

    public float getRollDiameter() {
        return rollDiameter;
    }

    public void setRollDiameter(float rollDiameter) {
        this.rollDiameter = rollDiameter;
    }

    public String getRollDiameterUnits() {
        return rollDiameterUnits;
    }

    public void setRollDiameterUnits(String rollDiameterUnits) {
        this.rollDiameterUnits = rollDiameterUnits;
    }

    public float getSpeedRatio() {
        return speedRatio;
    }

    public void setSpeedRatio(float speedRatio) {
        this.speedRatio = speedRatio;
    }

    public String getTachReferenceUnits() {
        return tachReferenceUnits;
    }

    public void setTachReferenceUnits(String tachReferenceUnits) {
        this.tachReferenceUnits = tachReferenceUnits;
    }

    public String getParamName() {
        return paramName;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    public boolean isDemodEnabled() {
        return demodEnabled;
    }

    public void setDemodEnabled(boolean demodEnabled) {
        this.demodEnabled = demodEnabled;
    }

    public float getDemodHighPass() {
        return demodHighPass;
    }

    public void setDemodHighPass(float demodHighPass) {
        this.demodHighPass = demodHighPass;
    }

    public float getDemodLowPass() {
        return demodLowPass;
    }

    public void setDemodLowPass(float demodLowPass) {
        this.demodLowPass = demodLowPass;
    }

    public int getDwell() {
        return dwell;
    }

    public void setDwell(int dwell) {
        this.dwell = dwell;
    }

    public int getfMax() {
        return fMax;
    }

    public void setfMax(int fMax) {
        this.fMax = fMax;
    }

    public int getDemodInterval() {
        return demodInterval;
    }

    public void setDemodInterval(int demodInterval) {
        this.demodInterval = demodInterval;
    }

    public String getParamType() {
        return paramType;
    }

    public void setParamType(String paramType) {
        this.paramType = paramType;
    }

    public String getFreqUnits() {
        return freqUnits;
    }

    public void setFreqUnits(String freqUnits) {
        this.freqUnits = freqUnits;
    }

    public float getHighAlert() {
        return highAlert;
    }

    public void setHighAlert(float highAlert) {
        this.highAlert = highAlert;
    }

    public boolean isHighAlertActive() {
        return highAlertActive;
    }

    public void setHighAlertActive(boolean highAlertActive) {
        this.highAlertActive = highAlertActive;
    }

    public float getHighFault() {
        return highFault;
    }

    public void setHighFault(float highFault) {
        this.highFault = highFault;
    }

    public boolean isHighFaultActive() {
        return highFaultActive;
    }

    public void setHighFaultActive(boolean highFaultActive) {
        this.highFaultActive = highFaultActive;
    }

    public boolean isCustomized() {
        return customized;
    }

    public void setCustomized(boolean customized) {
        this.customized = customized;
    }

    public float getLowAlert() {
        return lowAlert;
    }

    public void setLowAlert(float lowAlert) {
        this.lowAlert = lowAlert;
    }

    public boolean isLowAlertActive() {
        return lowAlertActive;
    }

    public void setLowAlertActive(boolean lowAlertActive) {
        this.lowAlertActive = lowAlertActive;
    }

    public float getLowFault() {
        return lowFault;
    }

    public void setLowFault(float lowFault) {
        this.lowFault = lowFault;
    }

    public boolean isLowFaultActive() {
        return lowFaultActive;
    }

    public void setLowFaultActive(boolean lowFaultActive) {
        this.lowFaultActive = lowFaultActive;
    }

    public float getMaxFreq() {
        return maxFreq;
    }

    public void setMaxFreq(float maxFreq) {
        this.maxFreq = maxFreq;
    }

    public float getMinFreq() {
        return minFreq;
    }

    public void setMinFreq(float minFreq) {
        this.minFreq = minFreq;
    }

    public String getParamAmpFactor() {
        return paramAmpFactor;
    }

    public void setParamAmpFactor(String paramAmpFactor) {
        this.paramAmpFactor = paramAmpFactor;
    }

    public String getParamUnits() {
        return paramUnits;
    }

    public void setParamUnits(String paramUnits) {
        this.paramUnits = paramUnits;
    }

    public float getPeriod() {
        return period;
    }

    public void setPeriod(float period) {
        this.period = period;
    }

    public int getResolution() {
        return resolution;
    }

    public void setResolution(int resolution) {
        this.resolution = resolution;
    }

    public float getSampleRate() {
        return sampleRate;
    }

    public void setSampleRate(float sampleRate) {
        this.sampleRate = sampleRate;
    }

    public List<ApAlSetVO> getApAlSetVOs() {
        return apAlSetVOs;
    }

    public void setApAlSetVOs(List<ApAlSetVO> apAlSetVOs) {
        this.apAlSetVOs = apAlSetVOs;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public Boolean getHwDisabled() {
        return hwDisabled;
    }

    public void setHwDisabled(Boolean hwDisabled) {
        this.hwDisabled = hwDisabled;
    }

    public boolean isNewChannel() {
        return newChannel;
    }

    public void setNewChannel(boolean newChannel) {
        this.newChannel = newChannel;
    }

    public Set<String> getFfSetName() {
        return ffSetName;
    }

    public void setFfSetName(Set<String> ffSetName) {
        this.ffSetName = ffSetName;
    }

    public String getAlarmEnabledValue() {
        return alarmEnabledValue;
    }

    public void setAlarmEnabledValue(String alarmEnabledValue) {
        this.alarmEnabledValue = alarmEnabledValue;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 79 * hash + Objects.hashCode(this.customerAccount);
        hash = 79 * hash + Objects.hashCode(this.siteId);
        hash = 79 * hash + Objects.hashCode(this.areaId);
        hash = 79 * hash + Objects.hashCode(this.assetId);
        hash = 79 * hash + Objects.hashCode(this.pointLocationId);
        hash = 79 * hash + Objects.hashCode(this.pointId);
        hash = 79 * hash + Objects.hashCode(this.apSetId);
        hash = 79 * hash + Objects.hashCode(this.alSetId);
        hash = 79 * hash + Objects.hashCode(this.pointName);
        hash = 79 * hash + Objects.hashCode(this.pointType);
        hash = 79 * hash + this.sensorChannelNum;
        hash = 79 * hash + Float.floatToIntBits(this.sensorOffset);
        hash = 79 * hash + Float.floatToIntBits(this.sensorSensitivity);
        hash = 79 * hash + Objects.hashCode(this.sensorType);
        hash = 79 * hash + Objects.hashCode(this.sensorUnits);
        hash = 79 * hash + Objects.hashCode(this.siteName);
        hash = 79 * hash + Objects.hashCode(this.areaName);
        hash = 79 * hash + Objects.hashCode(this.assetName);
        hash = 79 * hash + Objects.hashCode(this.pointLocationName);
        hash = 79 * hash + Objects.hashCode(this.pointLocationDesc);
        hash = 79 * hash + Objects.hashCode(this.apSetName);
        hash = 79 * hash + Objects.hashCode(this.alSetName);
        hash = 79 * hash + this.basestationPortNum;
        hash = 79 * hash + Objects.hashCode(this.ffSetNames);
        hash = 79 * hash + Objects.hashCode(this.ffSetIds);
        hash = 79 * hash + Objects.hashCode(this.tachName);
        hash = 79 * hash + Objects.hashCode(this.tachId);
        hash = 79 * hash + this.sampleInterval;
        hash = 79 * hash + (this.alarmEnabled ? 1 : 0);
        hash = 79 * hash + Float.floatToIntBits(this.rollDiameter);
        hash = 79 * hash + Objects.hashCode(this.rollDiameterUnits);
        hash = 79 * hash + Float.floatToIntBits(this.speedRatio);
        hash = 79 * hash + Objects.hashCode(this.tachReferenceUnits);
        hash = 79 * hash + Objects.hashCode(this.ffSetName);
        hash = 79 * hash + Objects.hashCode(this.paramName);
        hash = 79 * hash + (this.demodEnabled ? 1 : 0);
        hash = 79 * hash + Float.floatToIntBits(this.demodHighPass);
        hash = 79 * hash + Float.floatToIntBits(this.demodLowPass);
        hash = 79 * hash + this.dwell;
        hash = 79 * hash + this.fMax;
        hash = 79 * hash + this.demodInterval;
        hash = 79 * hash + Objects.hashCode(this.paramType);
        hash = 79 * hash + Objects.hashCode(this.freqUnits);
        hash = 79 * hash + Float.floatToIntBits(this.highAlert);
        hash = 79 * hash + (this.highAlertActive ? 1 : 0);
        hash = 79 * hash + Float.floatToIntBits(this.highFault);
        hash = 79 * hash + (this.highFaultActive ? 1 : 0);
        hash = 79 * hash + (this.customized ? 1 : 0);
        hash = 79 * hash + Float.floatToIntBits(this.lowAlert);
        hash = 79 * hash + (this.lowAlertActive ? 1 : 0);
        hash = 79 * hash + Float.floatToIntBits(this.lowFault);
        hash = 79 * hash + (this.lowFaultActive ? 1 : 0);
        hash = 79 * hash + Float.floatToIntBits(this.maxFreq);
        hash = 79 * hash + Float.floatToIntBits(this.minFreq);
        hash = 79 * hash + Objects.hashCode(this.paramAmpFactor);
        hash = 79 * hash + Objects.hashCode(this.paramUnits);
        hash = 79 * hash + Float.floatToIntBits(this.period);
        hash = 79 * hash + this.resolution;
        hash = 79 * hash + Float.floatToIntBits(this.sampleRate);
        hash = 79 * hash + Objects.hashCode(this.apAlSetVOs);
        hash = 79 * hash + Objects.hashCode(this.deviceId);
        hash = 79 * hash + Objects.hashCode(this.hwDisabled);
        hash = 79 * hash + (this.newChannel ? 1 : 0);
        hash = 79 * hash + Objects.hashCode(this.alarmEnabledValue);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ChannelVO other = (ChannelVO) obj;
        if (this.sensorChannelNum != other.sensorChannelNum) {
            return false;
        }
        if (Float.floatToIntBits(this.sensorOffset) != Float.floatToIntBits(other.sensorOffset)) {
            return false;
        }
        if (Float.floatToIntBits(this.sensorSensitivity) != Float.floatToIntBits(other.sensorSensitivity)) {
            return false;
        }
        if (this.basestationPortNum != other.basestationPortNum) {
            return false;
        }
        if (this.sampleInterval != other.sampleInterval) {
            return false;
        }
        if (this.alarmEnabled != other.alarmEnabled) {
            return false;
        }
        if (Float.floatToIntBits(this.rollDiameter) != Float.floatToIntBits(other.rollDiameter)) {
            return false;
        }
        if (Float.floatToIntBits(this.speedRatio) != Float.floatToIntBits(other.speedRatio)) {
            return false;
        }
        if (this.demodEnabled != other.demodEnabled) {
            return false;
        }
        if (Float.floatToIntBits(this.demodHighPass) != Float.floatToIntBits(other.demodHighPass)) {
            return false;
        }
        if (Float.floatToIntBits(this.demodLowPass) != Float.floatToIntBits(other.demodLowPass)) {
            return false;
        }
        if (this.dwell != other.dwell) {
            return false;
        }
        if (this.fMax != other.fMax) {
            return false;
        }
        if (this.demodInterval != other.demodInterval) {
            return false;
        }
        if (Float.floatToIntBits(this.highAlert) != Float.floatToIntBits(other.highAlert)) {
            return false;
        }
        if (this.highAlertActive != other.highAlertActive) {
            return false;
        }
        if (Float.floatToIntBits(this.highFault) != Float.floatToIntBits(other.highFault)) {
            return false;
        }
        if (this.highFaultActive != other.highFaultActive) {
            return false;
        }
        if (this.customized != other.customized) {
            return false;
        }
        if (Float.floatToIntBits(this.lowAlert) != Float.floatToIntBits(other.lowAlert)) {
            return false;
        }
        if (this.lowAlertActive != other.lowAlertActive) {
            return false;
        }
        if (Float.floatToIntBits(this.lowFault) != Float.floatToIntBits(other.lowFault)) {
            return false;
        }
        if (this.lowFaultActive != other.lowFaultActive) {
            return false;
        }
        if (Float.floatToIntBits(this.maxFreq) != Float.floatToIntBits(other.maxFreq)) {
            return false;
        }
        if (Float.floatToIntBits(this.minFreq) != Float.floatToIntBits(other.minFreq)) {
            return false;
        }
        if (Float.floatToIntBits(this.period) != Float.floatToIntBits(other.period)) {
            return false;
        }
        if (this.resolution != other.resolution) {
            return false;
        }
        if (Float.floatToIntBits(this.sampleRate) != Float.floatToIntBits(other.sampleRate)) {
            return false;
        }
        if (this.newChannel != other.newChannel) {
            return false;
        }
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.pointName, other.pointName)) {
            return false;
        }
        if (!Objects.equals(this.pointType, other.pointType)) {
            return false;
        }
        if (!Objects.equals(this.sensorType, other.sensorType)) {
            return false;
        }
        if (!Objects.equals(this.sensorUnits, other.sensorUnits)) {
            return false;
        }
        if (!Objects.equals(this.siteName, other.siteName)) {
            return false;
        }
        if (!Objects.equals(this.areaName, other.areaName)) {
            return false;
        }
        if (!Objects.equals(this.assetName, other.assetName)) {
            return false;
        }
        if (!Objects.equals(this.pointLocationName, other.pointLocationName)) {
            return false;
        }
        if (!Objects.equals(this.pointLocationDesc, other.pointLocationDesc)) {
            return false;
        }
        if (!Objects.equals(this.apSetName, other.apSetName)) {
            return false;
        }
        if (!Objects.equals(this.alSetName, other.alSetName)) {
            return false;
        }
        if (!Objects.equals(this.ffSetNames, other.ffSetNames)) {
            return false;
        }
        if (!Objects.equals(this.ffSetIds, other.ffSetIds)) {
            return false;
        }
        if (!Objects.equals(this.tachName, other.tachName)) {
            return false;
        }
        if (!Objects.equals(this.rollDiameterUnits, other.rollDiameterUnits)) {
            return false;
        }
        if (!Objects.equals(this.tachReferenceUnits, other.tachReferenceUnits)) {
            return false;
        }
        if (!Objects.equals(this.paramName, other.paramName)) {
            return false;
        }
        if (!Objects.equals(this.paramType, other.paramType)) {
            return false;
        }
        if (!Objects.equals(this.freqUnits, other.freqUnits)) {
            return false;
        }
        if (!Objects.equals(this.paramAmpFactor, other.paramAmpFactor)) {
            return false;
        }
        if (!Objects.equals(this.paramUnits, other.paramUnits)) {
            return false;
        }
        if (!Objects.equals(this.deviceId, other.deviceId)) {
            return false;
        }
        if (!Objects.equals(this.alarmEnabledValue, other.alarmEnabledValue)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        if (!Objects.equals(this.areaId, other.areaId)) {
            return false;
        }
        if (!Objects.equals(this.assetId, other.assetId)) {
            return false;
        }
        if (!Objects.equals(this.pointLocationId, other.pointLocationId)) {
            return false;
        }
        if (!Objects.equals(this.pointId, other.pointId)) {
            return false;
        }
        if (!Objects.equals(this.apSetId, other.apSetId)) {
            return false;
        }
        if (!Objects.equals(this.alSetId, other.alSetId)) {
            return false;
        }
        if (!Objects.equals(this.tachId, other.tachId)) {
            return false;
        }
        if (!Objects.equals(this.ffSetName, other.ffSetName)) {
            return false;
        }
        if (!Objects.equals(this.apAlSetVOs, other.apAlSetVOs)) {
            return false;
        }
        return Objects.equals(this.hwDisabled, other.hwDisabled);
    }

    @Override
    public String toString() {
        return "ChannelVO{" + "customerAccount=" + customerAccount + ", siteId=" + siteId + ", areaId=" + areaId + ", assetId=" + assetId + ", pointLocationId=" + pointLocationId + ", pointId=" + pointId + ", apSetId=" + apSetId + ", alSetId=" + alSetId + ", pointName=" + pointName + ", pointType=" + pointType + ", sensorChannelNum=" + sensorChannelNum + ", sensorOffset=" + sensorOffset + ", sensorSensitivity=" + sensorSensitivity + ", sensorType=" + sensorType + ", sensorUnits=" + sensorUnits + ", siteName=" + siteName + ", areaName=" + areaName + ", assetName=" + assetName + ", pointLocationName=" + pointLocationName + ", pointLocationDesc=" + pointLocationDesc + ", apSetName=" + apSetName + ", alSetName=" + alSetName + ", basestationPortNum=" + basestationPortNum + ", ffSetNames=" + ffSetNames + ", ffSetIds=" + ffSetIds + ", tachName=" + tachName + ", tachId=" + tachId + ", sampleInterval=" + sampleInterval + ", alarmEnabled=" + alarmEnabled + ", rollDiameter=" + rollDiameter + ", rollDiameterUnits=" + rollDiameterUnits + ", speedRatio=" + speedRatio + ", tachReferenceUnits=" + tachReferenceUnits + ", ffSetName=" + ffSetName + ", paramName=" + paramName + ", demodEnabled=" + demodEnabled + ", demodHighPass=" + demodHighPass + ", demodLowPass=" + demodLowPass + ", dwell=" + dwell + ", fMax=" + fMax + ", demodInterval=" + demodInterval + ", paramType=" + paramType + ", freqUnits=" + freqUnits + ", highAlert=" + highAlert + ", highAlertActive=" + highAlertActive + ", highFault=" + highFault + ", highFaultActive=" + highFaultActive + ", customized=" + customized + ", lowAlert=" + lowAlert + ", lowAlertActive=" + lowAlertActive + ", lowFault=" + lowFault + ", lowFaultActive=" + lowFaultActive + ", maxFreq=" + maxFreq + ", minFreq=" + minFreq + ", paramAmpFactor=" + paramAmpFactor + ", paramUnits=" + paramUnits + ", period=" + period + ", resolution=" + resolution + ", sampleRate=" + sampleRate + ", apAlSetVOs=" + apAlSetVOs + ", deviceId=" + deviceId + ", hwDisabled=" + hwDisabled + ", newChannel=" + newChannel + ", alarmEnabledValue=" + alarmEnabledValue + '}';
    }
    
    
}
