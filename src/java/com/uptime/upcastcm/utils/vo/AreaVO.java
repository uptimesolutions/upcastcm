/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.utils.vo;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.UUID; 

/**
 *
 * @author gsingh
 */
public class AreaVO implements Serializable {
    String customerAccount;
    UUID siteId;
    UUID areaId;
    String siteName;
    String areaName;
    boolean climateControlled;
    String description;
    String environment;
    List<AreaConfigScheduleVO> scheduleVOList;
    String climateControlledValue;
    
    public AreaVO() {
    }

    public AreaVO(String customerAccount, UUID siteId, UUID areaId, String siteName, String areaName, boolean climateControlled, String description, String environment, List<AreaConfigScheduleVO> scheduleVOList,String climateControlledValue) {
        this.customerAccount = customerAccount;
        this.siteId = siteId;
        this.areaId = areaId;
        this.siteName = siteName;
        this.areaName = areaName;
        this.climateControlled = climateControlled;
        this.description = description;
        this.environment = environment;
        this.scheduleVOList = scheduleVOList;
        this.climateControlledValue = climateControlledValue;
    }

    public AreaVO(AreaVO areaVO) {
        customerAccount = areaVO.getCustomerAccount();
        siteId = areaVO.getSiteId();
        areaId = areaVO.getAreaId();
        siteName = areaVO.getSiteName();
        areaName = areaVO.getAreaName();
        climateControlled = areaVO.isClimateControlled();
        description = areaVO.getDescription();
        environment = areaVO.getEnvironment();
        scheduleVOList = areaVO.getScheduleVOList();
       climateControlledValue=areaVO.getClimateControlledValue();
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public UUID getAreaId() {
        return areaId;
    }

    public void setAreaId(UUID areaId) {
        this.areaId = areaId;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public boolean isClimateControlled() {
        return climateControlled;
    }

    public void setClimateControlled(boolean climateControlled) {
        this.climateControlled = climateControlled;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEnvironment() {
        return environment;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    public List<AreaConfigScheduleVO> getScheduleVOList() {
        return scheduleVOList;
    }

    public void setScheduleVOList(List<AreaConfigScheduleVO> scheduleVOList) {
        this.scheduleVOList = scheduleVOList;
    }

    public String getClimateControlledValue() {
        return climateControlledValue;
    }

    public void setClimateControlledValue(String climateControlledValue) {
        this.climateControlledValue = climateControlledValue;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.customerAccount);
        hash = 89 * hash + Objects.hashCode(this.siteId);
        hash = 89 * hash + Objects.hashCode(this.areaId);
        hash = 89 * hash + Objects.hashCode(this.siteName);
        hash = 89 * hash + Objects.hashCode(this.areaName);
        hash = 89 * hash + (this.climateControlled ? 1 : 0);
        hash = 89 * hash + Objects.hashCode(this.description);
        hash = 89 * hash + Objects.hashCode(this.environment);
        hash = 89 * hash + Objects.hashCode(this.scheduleVOList);
        hash = 89 * hash + Objects.hashCode(this.climateControlledValue);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AreaVO other = (AreaVO) obj;
        if (this.climateControlled != other.climateControlled) {
            return false;
        }
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.siteName, other.siteName)) {
            return false;
        }
        if (!Objects.equals(this.areaName, other.areaName)) {
            return false;
        }
        if (!Objects.equals(this.description, other.description)) {
            return false;
        }
        if (!Objects.equals(this.environment, other.environment)) {
            return false;
        }
        if (!Objects.equals(this.climateControlledValue, other.climateControlledValue)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        if (!Objects.equals(this.areaId, other.areaId)) {
            return false;
        }
        return Objects.equals(this.scheduleVOList, other.scheduleVOList);
    }
    
    

    

    @Override
    public String toString() {
        return "AreaVO{" + "customerAccount=" + customerAccount + ", siteId=" + siteId + ", areaId=" + areaId + ", siteName=" + siteName + ", areaName=" + areaName + ", climateControlled=" + climateControlled + ", description=" + description + ", environment=" + environment + ", scheduleVOList=" + scheduleVOList + ", climateControlledValue=" + climateControlledValue + '}';
    }

   
}
