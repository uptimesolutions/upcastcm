/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.utils.vo;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author kpati
 */
public class PointToHwVO implements Serializable {
    private String deviceId;
    private String channelType;
    private byte channelNum;
    private String customerAccount;
    private UUID siteId;
    private UUID areaId;
    private UUID assetId;
    private UUID pointLocationId;
    private UUID pointId;
    
    public PointToHwVO(){ 
    }

    public PointToHwVO(String deviceId, String channelType, byte channelNum, String customerAccount, UUID siteId, UUID areaId, UUID assetId, UUID pointLocationId, UUID pointId) {
        this.deviceId = deviceId;
        this.channelType = channelType;
        this.channelNum = channelNum;
        this.customerAccount = customerAccount;
        this.siteId = siteId;
        this.areaId = areaId;
        this.assetId = assetId;
        this.pointLocationId = pointLocationId;
        this.pointId = pointId;
    }

    public PointToHwVO(PointToHwVO pointToHwVO) {
        deviceId = pointToHwVO.getDeviceId();
        channelType = pointToHwVO.getChannelType();
        channelNum = pointToHwVO.getChannelNum();
        customerAccount = pointToHwVO.getCustomerAccount();
        siteId = pointToHwVO.getSiteId();
        areaId = pointToHwVO.getAreaId();
        assetId = pointToHwVO.getAssetId();
        pointLocationId = pointToHwVO.getPointLocationId();
        pointId = pointToHwVO.getPointId();
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getChannelType() {
        return channelType;
    }

    public void setChannelType(String channelType) {
        this.channelType = channelType;
    }

    public byte getChannelNum() {
        return channelNum;
    }

    public void setChannelNum(byte channelNum) {
        this.channelNum = channelNum;
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public UUID getAreaId() {
        return areaId;
    }

    public void setAreaId(UUID areaId) {
        this.areaId = areaId;
    }

    public UUID getAssetId() {
        return assetId;
    }

    public void setAssetId(UUID assetId) {
        this.assetId = assetId;
    }

    public UUID getPointLocationId() {
        return pointLocationId;
    }

    public void setPointLocationId(UUID pointLocationId) {
        this.pointLocationId = pointLocationId;
    }

    public UUID getPointId() {
        return pointId;
    }

    public void setPointId(UUID pointId) {
        this.pointId = pointId;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this.deviceId);
        hash = 37 * hash + Objects.hashCode(this.channelType);
        hash = 37 * hash + this.channelNum;
        hash = 37 * hash + Objects.hashCode(this.customerAccount);
        hash = 37 * hash + Objects.hashCode(this.siteId);
        hash = 37 * hash + Objects.hashCode(this.areaId);
        hash = 37 * hash + Objects.hashCode(this.assetId);
        hash = 37 * hash + Objects.hashCode(this.pointLocationId);
        hash = 37 * hash + Objects.hashCode(this.pointId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PointToHwVO other = (PointToHwVO) obj;
        if (this.channelNum != other.channelNum) {
            return false;
        }
        if (!Objects.equals(this.deviceId, other.deviceId)) {
            return false;
        }
        if (!Objects.equals(this.channelType, other.channelType)) {
            return false;
        }
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        if (!Objects.equals(this.areaId, other.areaId)) {
            return false;
        }
        if (!Objects.equals(this.assetId, other.assetId)) {
            return false;
        }
        if (!Objects.equals(this.pointLocationId, other.pointLocationId)) {
            return false;
        }
        if (!Objects.equals(this.pointId, other.pointId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "PointToHwVO{" + "deviceId=" + deviceId + ", channelType=" + channelType + ", channelNum=" + channelNum + ", customerAccount=" + customerAccount + ", siteId=" + siteId + ", areaId=" + areaId + ", assetId=" + assetId + ", pointLocationId=" + pointLocationId + ", pointId=" + pointId + '}';
    }
}
