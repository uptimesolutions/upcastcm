/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.upcastcm.utils.vo;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import javax.faces.model.SelectItem;

/**
 *
 * @author gopal
 */
public class ChartInfoVO implements Serializable {
    private UUID siteId;
    private UUID areaId;
    private UUID assetId;
    private UUID pointLocationId;
    private UUID pointId;
    private UUID apSetId;
    private String siteName;
    private String region;
    private String country;
    private String areaName;
    private String assetName;
    private String pointLocationName;
    private String serialNum;
    private String pointName;
    private int sampleInterval;
    private int resolution;
    private String pointType;
    private List<String> paramNameList;
    private WaveDataVO waveDataVO;
    private String graphTimestamp;
    private PointVO selectedApAlByPoint;
    private List<PointVO> apAlByPointList;
    private GraphDataVO spectrumGraphData;
    private GraphDataVO waveGraphData;
    private int selectedPointIndex;
    private int selectedPlotLineIndex;
    private List<FaultFrequenciesVO> ffSetVOList;
    private List<String> workOrderSelectedParamList;
    private WorkOrderVO workOrderVO;
    private PointVO selectedAlarmLimitPoint;
    private boolean spectrumLogYAxis;
    private boolean lowFaultValidationFailed; 
    private boolean lowAlertValidationFailed;
    private boolean highAlertValidationFailed;
    private boolean highFaultValidationFailed;
    private boolean logYaxis;
    private boolean plotOptionsModified;
    private boolean disableParamAllSelection;
    private int count;
    private String paramName;
    private String graphType;
    private String amplitudeUnit;
    private String timeUnit;
    private String frequencyUnit;
    private String ampFactor;
    private String graphTitle;
    private String windowDisplayName;
    private float yAxisUpperBound;
    private List<UUIDItemVO> apSetList;
    private boolean refreshWaveAndSpectrum;
    private UUIDItemVO apSetItem;

    public ChartInfoVO() {
    }

    public ChartInfoVO(UUID siteId, UUID areaId, UUID assetId, UUID pointLocationId, UUID pointId, UUID apSetId, String siteName, String region, String country, String areaName, String assetName, String pointLocationName, String serialNum, String pointName, int sampleInterval, int resolution, String pointType, List<String> paramNameList, WaveDataVO waveDataVO, String graphTimestamp, PointVO selectedApAlByPoint, List<PointVO> apAlByPointList, GraphDataVO spectrumGraphData, GraphDataVO waveGraphData, int selectedPointIndex, int selectedPlotLineIndex, List<FaultFrequenciesVO> ffSetVOList, List<String> workOrderSelectedParamList, WorkOrderVO workOrderVO, PointVO selectedAlarmLimitPoint, boolean spectrumLogYAxis, boolean lowFaultValidationFailed, boolean lowAlertValidationFailed, boolean highAlertValidationFailed, boolean highFaultValidationFailed, boolean logYaxis, boolean plotOptionsModified, boolean disableParamAllSelection, int count, String paramName, String graphType, String amplitudeUnit, String timeUnit, String frequencyUnit, String ampFactor, String graphTitle, String windowDisplayName, float yAxisUpperBound, List<UUIDItemVO> apSetList, boolean refreshWaveAndSpectrum, UUIDItemVO apSetItem) {
        this.siteId = siteId;
        this.areaId = areaId;
        this.assetId = assetId;
        this.pointLocationId = pointLocationId;
        this.pointId = pointId;
        this.apSetId = apSetId;
        this.siteName = siteName;
        this.region = region;
        this.country = country;
        this.areaName = areaName;
        this.assetName = assetName;
        this.pointLocationName = pointLocationName;
        this.serialNum = serialNum;
        this.pointName = pointName;
        this.sampleInterval = sampleInterval;
        this.resolution = resolution;
        this.pointType = pointType;
        this.paramNameList = paramNameList;
        this.waveDataVO = waveDataVO;
        this.graphTimestamp = graphTimestamp;
        this.selectedApAlByPoint = selectedApAlByPoint;
        this.apAlByPointList = apAlByPointList;
        this.spectrumGraphData = spectrumGraphData;
        this.waveGraphData = waveGraphData;
        this.selectedPointIndex = selectedPointIndex;
        this.selectedPlotLineIndex = selectedPlotLineIndex;
        this.ffSetVOList = ffSetVOList;
        this.workOrderSelectedParamList = workOrderSelectedParamList;
        this.workOrderVO = workOrderVO;
        this.selectedAlarmLimitPoint = selectedAlarmLimitPoint;
        this.spectrumLogYAxis = spectrumLogYAxis;
        this.lowFaultValidationFailed = lowFaultValidationFailed;
        this.lowAlertValidationFailed = lowAlertValidationFailed;
        this.highAlertValidationFailed = highAlertValidationFailed;
        this.highFaultValidationFailed = highFaultValidationFailed;
        this.logYaxis = logYaxis;
        this.plotOptionsModified = plotOptionsModified;
        this.disableParamAllSelection = disableParamAllSelection;
        this.count = count;
        this.paramName = paramName;
        this.graphType = graphType;
        this.amplitudeUnit = amplitudeUnit;
        this.timeUnit = timeUnit;
        this.frequencyUnit = frequencyUnit;
        this.ampFactor = ampFactor;
        this.graphTitle = graphTitle;
        this.windowDisplayName = windowDisplayName;
        this.yAxisUpperBound = yAxisUpperBound;
        this.apSetList = apSetList;
        this.refreshWaveAndSpectrum = refreshWaveAndSpectrum;
        this.apSetItem = apSetItem;
    }

    public ChartInfoVO(ChartInfoVO chartInfoVO) {
        siteId = chartInfoVO.getSiteId();
        areaId = chartInfoVO.getAreaId();
        assetId = chartInfoVO.getAssetId();
        pointLocationId = chartInfoVO.getPointLocationId();
        pointId = chartInfoVO.getPointId();
        apSetId = chartInfoVO.getApSetId();
        siteName = chartInfoVO.getSiteName();
        region = chartInfoVO.getRegion();
        country = chartInfoVO.getCountry();
        areaName = chartInfoVO.getAreaName();
        assetName = chartInfoVO.getAssetName();
        pointLocationName = chartInfoVO.getPointLocationName();
        serialNum = chartInfoVO.getSerialNum();
        pointName = chartInfoVO.getPointName();
        sampleInterval = chartInfoVO.getSampleInterval();
        resolution = chartInfoVO.getResolution();
        pointType = chartInfoVO.getPointType();
        paramNameList = chartInfoVO.getParamNameList();
        waveDataVO = chartInfoVO.getWaveDataVO();
        graphTimestamp = chartInfoVO.getGraphTimestamp();
        selectedApAlByPoint = chartInfoVO.getSelectedApAlByPoint();
        apAlByPointList = chartInfoVO.getApAlByPointList();
        spectrumGraphData = chartInfoVO.getSpectrumGraphData();
        waveGraphData = chartInfoVO.getWaveGraphData();
        selectedPointIndex = chartInfoVO.getSelectedPointIndex();
        selectedPlotLineIndex = chartInfoVO.getSelectedPlotLineIndex();
        ffSetVOList = chartInfoVO.getFfSetVOList();
        workOrderSelectedParamList = chartInfoVO.getWorkOrderSelectedParamList();
        workOrderVO = chartInfoVO.getWorkOrderVO();
        selectedAlarmLimitPoint = chartInfoVO.getSelectedAlarmLimitPoint();
        spectrumLogYAxis = chartInfoVO.isSpectrumLogYAxis();
        lowFaultValidationFailed = chartInfoVO.isLowFaultValidationFailed();
        lowAlertValidationFailed = chartInfoVO.isLowAlertValidationFailed();
        highAlertValidationFailed = chartInfoVO.isHighAlertValidationFailed();
        highFaultValidationFailed = chartInfoVO.isHighFaultValidationFailed();
        logYaxis = chartInfoVO.isLogYaxis();
        plotOptionsModified = chartInfoVO.isPlotOptionsModified();
        disableParamAllSelection = chartInfoVO.isDisableParamAllSelection();
        count = chartInfoVO.getCount();
        paramName = chartInfoVO.getParamName();
        graphType = chartInfoVO.getGraphType();
        amplitudeUnit = chartInfoVO.getAmplitudeUnit();
        timeUnit = chartInfoVO.getTimeUnit();
        frequencyUnit = chartInfoVO.getFrequencyUnit();
        ampFactor = chartInfoVO.getAmpFactor();
        graphTitle = chartInfoVO.getGraphTitle();
        windowDisplayName = chartInfoVO.getWindowDisplayName();
        apSetList = chartInfoVO.getApSetList();
        refreshWaveAndSpectrum = chartInfoVO.isRefreshWaveAndSpectrum();
        apSetItem = chartInfoVO.getApSetItem();
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public UUID getAreaId() {
        return areaId;
    }

    public void setAreaId(UUID areaId) {
        this.areaId = areaId;
    }

    public UUID getAssetId() {
        return assetId;
    }

    public void setAssetId(UUID assetId) {
        this.assetId = assetId;
    }

    public UUID getPointLocationId() {
        return pointLocationId;
    }

    public void setPointLocationId(UUID pointLocationId) {
        this.pointLocationId = pointLocationId;
    }

    public UUID getPointId() {
        return pointId;
    }

    public void setPointId(UUID pointId) {
        this.pointId = pointId;
    }

    public UUID getApSetId() {
        return apSetId;
    }

    public void setApSetId(UUID apSetId) {
        this.apSetId = apSetId;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getAssetName() {
        return assetName;
    }

    public void setAssetName(String assetName) {
        this.assetName = assetName;
    }

    public String getPointLocationName() {
        return pointLocationName;
    }

    public void setPointLocationName(String pointLocationName) {
        this.pointLocationName = pointLocationName;
    }

    public String getSerialNum() {
        return serialNum;
    }

    public void setSerialNum(String serialNum) {
        this.serialNum = serialNum;
    }

    public String getPointName() {
        return pointName;
    }

    public void setPointName(String pointName) {
        this.pointName = pointName;
    }

    public int getSampleInterval() {
        return sampleInterval;
    }

    public void setSampleInterval(int sampleInterval) {
        this.sampleInterval = sampleInterval;
    }

    public int getResolution() {
        return resolution;
    }

    public void setResolution(int resolution) {
        this.resolution = resolution;
    }

    public String getPointType() {
        return pointType;
    }

    public void setPointType(String pointType) {
        this.pointType = pointType;
    }

    public List<String> getParamNameList() {
        return paramNameList;
    }

    public void setParamNameList(List<String> paramNameList) {
        this.paramNameList = paramNameList;
    }

    public WaveDataVO getWaveDataVO() {
        return waveDataVO;
    }

    public void setWaveDataVO(WaveDataVO waveDataVO) {
        this.waveDataVO = waveDataVO;
    }

    public String getGraphTimestamp() {
        return graphTimestamp;
    }

    public void setGraphTimestamp(String graphTimestamp) {
        this.graphTimestamp = graphTimestamp;
    }

    public PointVO getSelectedApAlByPoint() {
        return selectedApAlByPoint;
    }

    public void setSelectedApAlByPoint(PointVO selectedApAlByPoint) {
        this.selectedApAlByPoint = selectedApAlByPoint;
    }

    public List<PointVO> getApAlByPointList() {
        return apAlByPointList;
    }

    public void setApAlByPointList(List<PointVO> apAlByPointList) {
        this.apAlByPointList = apAlByPointList;
    }

    public GraphDataVO getSpectrumGraphData() {
        return spectrumGraphData;
    }

    public void setSpectrumGraphData(GraphDataVO spectrumGraphData) {
        this.spectrumGraphData = spectrumGraphData;
    }

    public GraphDataVO getWaveGraphData() {
        return waveGraphData;
    }

    public void setWaveGraphData(GraphDataVO waveGraphData) {
        this.waveGraphData = waveGraphData;
    }

    public int getSelectedPointIndex() {
        return selectedPointIndex;
    }

    public void setSelectedPointIndex(int selectedPointIndex) {
        this.selectedPointIndex = selectedPointIndex;
    }

    public int getSelectedPlotLineIndex() {
        return selectedPlotLineIndex;
    }

    public void setSelectedPlotLineIndex(int selectedPlotLineIndex) {
        this.selectedPlotLineIndex = selectedPlotLineIndex;
    }

    public List<FaultFrequenciesVO> getFfSetVOList() {
        return ffSetVOList;
    }

    public void setFfSetVOList(List<FaultFrequenciesVO> ffSetVOList) {
        this.ffSetVOList = ffSetVOList;
    }

    public List<String> getWorkOrderSelectedParamList() {
        return workOrderSelectedParamList;
    }

    public void setWorkOrderSelectedParamList(List<String> workOrderSelectedParamList) {
        this.workOrderSelectedParamList = workOrderSelectedParamList;
    }

    public WorkOrderVO getWorkOrderVO() {
        return workOrderVO;
    }

    public void setWorkOrderVO(WorkOrderVO workOrderVO) {
        this.workOrderVO = workOrderVO;
    }

    public PointVO getSelectedAlarmLimitPoint() {
        return selectedAlarmLimitPoint;
    }

    public void setSelectedAlarmLimitPoint(PointVO selectedAlarmLimitPoint) {
        this.selectedAlarmLimitPoint = selectedAlarmLimitPoint;
    }

    public boolean isSpectrumLogYAxis() {
        return spectrumLogYAxis;
    }

    public void setSpectrumLogYAxis(boolean spectrumLogYAxis) {
        this.spectrumLogYAxis = spectrumLogYAxis;
    }

    public boolean isLowFaultValidationFailed() {
        return lowFaultValidationFailed;
    }

    public void setLowFaultValidationFailed(boolean lowFaultValidationFailed) {
        this.lowFaultValidationFailed = lowFaultValidationFailed;
    }

    public boolean isLowAlertValidationFailed() {
        return lowAlertValidationFailed;
    }

    public void setLowAlertValidationFailed(boolean lowAlertValidationFailed) {
        this.lowAlertValidationFailed = lowAlertValidationFailed;
    }

    public boolean isHighAlertValidationFailed() {
        return highAlertValidationFailed;
    }

    public void setHighAlertValidationFailed(boolean highAlertValidationFailed) {
        this.highAlertValidationFailed = highAlertValidationFailed;
    }

    public boolean isHighFaultValidationFailed() {
        return highFaultValidationFailed;
    }

    public void setHighFaultValidationFailed(boolean highFaultValidationFailed) {
        this.highFaultValidationFailed = highFaultValidationFailed;
    }

    public boolean isLogYaxis() {
        return logYaxis;
    }

    public void setLogYaxis(boolean logYaxis) {
        this.logYaxis = logYaxis;
    }

    public boolean isPlotOptionsModified() {
        return plotOptionsModified;
    }

    public void setPlotOptionsModified(boolean plotOptionsModified) {
        this.plotOptionsModified = plotOptionsModified;
    }

    public boolean isDisableParamAllSelection() {
        return disableParamAllSelection;
    }

    public void setDisableParamAllSelection(boolean disableParamAllSelection) {
        this.disableParamAllSelection = disableParamAllSelection;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getParamName() {
        return paramName;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    public String getGraphType() {
        return graphType;
    }

    public void setGraphType(String graphType) {
        this.graphType = graphType;
    }

    public String getAmplitudeUnit() {
        return amplitudeUnit;
    }

    public void setAmplitudeUnit(String amplitudeUnit) {
        this.amplitudeUnit = amplitudeUnit;
    }

    public String getTimeUnit() {
        return timeUnit;
    }

    public void setTimeUnit(String timeUnit) {
        this.timeUnit = timeUnit;
    }

    public String getFrequencyUnit() {
        return frequencyUnit;
    }

    public void setFrequencyUnit(String frequencyUnit) {
        this.frequencyUnit = frequencyUnit;
    }

    public String getAmpFactor() {
        return ampFactor;
    }

    public void setAmpFactor(String ampFactor) {
        this.ampFactor = ampFactor;
    }

    public String getGraphTitle() {
        return graphTitle;
    }

    public void setGraphTitle(String graphTitle) {
        this.graphTitle = graphTitle;
    }

    public String getWindowDisplayName() {
        return windowDisplayName;
    }

    public void setWindowDisplayName(String windowDisplayName) {
        this.windowDisplayName = windowDisplayName;
    }

    public float getyAxisUpperBound() {
        return yAxisUpperBound;
    }

    public void setyAxisUpperBound(float yAxisUpperBound) {
        this.yAxisUpperBound = yAxisUpperBound;
    }

    public List<UUIDItemVO> getApSetList() {
        return apSetList;
    }

    public void setApSetList(List<UUIDItemVO> apSetList) {
        this.apSetList = apSetList;
    }

    public boolean isRefreshWaveAndSpectrum() {
        return refreshWaveAndSpectrum;
    }

    public void setRefreshWaveAndSpectrum(boolean refreshWaveAndSpectrum) {
        this.refreshWaveAndSpectrum = refreshWaveAndSpectrum;
    }

    public UUIDItemVO getApSetItem() {
        return apSetItem;
    }

    public void setApSetItem(UUIDItemVO apSetItem) {
        this.apSetItem = apSetItem;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.siteId);
        hash = 59 * hash + Objects.hashCode(this.areaId);
        hash = 59 * hash + Objects.hashCode(this.assetId);
        hash = 59 * hash + Objects.hashCode(this.pointLocationId);
        hash = 59 * hash + Objects.hashCode(this.pointId);
        hash = 59 * hash + Objects.hashCode(this.apSetId);
        hash = 59 * hash + Objects.hashCode(this.siteName);
        hash = 59 * hash + Objects.hashCode(this.region);
        hash = 59 * hash + Objects.hashCode(this.country);
        hash = 59 * hash + Objects.hashCode(this.areaName);
        hash = 59 * hash + Objects.hashCode(this.assetName);
        hash = 59 * hash + Objects.hashCode(this.pointLocationName);
        hash = 59 * hash + Objects.hashCode(this.serialNum);
        hash = 59 * hash + Objects.hashCode(this.pointName);
        hash = 59 * hash + this.sampleInterval;
        hash = 59 * hash + this.resolution;
        hash = 59 * hash + Objects.hashCode(this.pointType);
        hash = 59 * hash + Objects.hashCode(this.paramNameList);
        hash = 59 * hash + Objects.hashCode(this.waveDataVO);
        hash = 59 * hash + Objects.hashCode(this.graphTimestamp);
        hash = 59 * hash + Objects.hashCode(this.selectedApAlByPoint);
        hash = 59 * hash + Objects.hashCode(this.apAlByPointList);
        hash = 59 * hash + Objects.hashCode(this.spectrumGraphData);
        hash = 59 * hash + Objects.hashCode(this.waveGraphData);
        hash = 59 * hash + this.selectedPointIndex;
        hash = 59 * hash + this.selectedPlotLineIndex;
        hash = 59 * hash + Objects.hashCode(this.ffSetVOList);
        hash = 59 * hash + Objects.hashCode(this.workOrderSelectedParamList);
        hash = 59 * hash + Objects.hashCode(this.workOrderVO);
        hash = 59 * hash + Objects.hashCode(this.selectedAlarmLimitPoint);
        hash = 59 * hash + (this.spectrumLogYAxis ? 1 : 0);
        hash = 59 * hash + (this.lowFaultValidationFailed ? 1 : 0);
        hash = 59 * hash + (this.lowAlertValidationFailed ? 1 : 0);
        hash = 59 * hash + (this.highAlertValidationFailed ? 1 : 0);
        hash = 59 * hash + (this.highFaultValidationFailed ? 1 : 0);
        hash = 59 * hash + (this.logYaxis ? 1 : 0);
        hash = 59 * hash + (this.plotOptionsModified ? 1 : 0);
        hash = 59 * hash + (this.disableParamAllSelection ? 1 : 0);
        hash = 59 * hash + this.count;
        hash = 59 * hash + Objects.hashCode(this.paramName);
        hash = 59 * hash + Objects.hashCode(this.graphType);
        hash = 59 * hash + Objects.hashCode(this.amplitudeUnit);
        hash = 59 * hash + Objects.hashCode(this.timeUnit);
        hash = 59 * hash + Objects.hashCode(this.frequencyUnit);
        hash = 59 * hash + Objects.hashCode(this.ampFactor);
        hash = 59 * hash + Objects.hashCode(this.graphTitle);
        hash = 59 * hash + Objects.hashCode(this.windowDisplayName);
        hash = 59 * hash + Float.floatToIntBits(this.yAxisUpperBound);
        hash = 59 * hash + Objects.hashCode(this.apSetList);
        hash = 59 * hash + (this.refreshWaveAndSpectrum ? 1 : 0);
        hash = 59 * hash + Objects.hashCode(this.apSetItem);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ChartInfoVO other = (ChartInfoVO) obj;
        if (this.sampleInterval != other.sampleInterval) {
            return false;
        }
        if (this.resolution != other.resolution) {
            return false;
        }
        if (this.selectedPointIndex != other.selectedPointIndex) {
            return false;
        }
        if (this.selectedPlotLineIndex != other.selectedPlotLineIndex) {
            return false;
        }
        if (this.spectrumLogYAxis != other.spectrumLogYAxis) {
            return false;
        }
        if (this.lowFaultValidationFailed != other.lowFaultValidationFailed) {
            return false;
        }
        if (this.lowAlertValidationFailed != other.lowAlertValidationFailed) {
            return false;
        }
        if (this.highAlertValidationFailed != other.highAlertValidationFailed) {
            return false;
        }
        if (this.highFaultValidationFailed != other.highFaultValidationFailed) {
            return false;
        }
        if (this.logYaxis != other.logYaxis) {
            return false;
        }
        if (this.plotOptionsModified != other.plotOptionsModified) {
            return false;
        }
        if (this.disableParamAllSelection != other.disableParamAllSelection) {
            return false;
        }
        if (this.count != other.count) {
            return false;
        }
        if (Float.floatToIntBits(this.yAxisUpperBound) != Float.floatToIntBits(other.yAxisUpperBound)) {
            return false;
        }
        if (this.refreshWaveAndSpectrum != other.refreshWaveAndSpectrum) {
            return false;
        }
        if (!Objects.equals(this.siteName, other.siteName)) {
            return false;
        }
        if (!Objects.equals(this.region, other.region)) {
            return false;
        }
        if (!Objects.equals(this.country, other.country)) {
            return false;
        }
        if (!Objects.equals(this.areaName, other.areaName)) {
            return false;
        }
        if (!Objects.equals(this.assetName, other.assetName)) {
            return false;
        }
        if (!Objects.equals(this.pointLocationName, other.pointLocationName)) {
            return false;
        }
        if (!Objects.equals(this.serialNum, other.serialNum)) {
            return false;
        }
        if (!Objects.equals(this.pointName, other.pointName)) {
            return false;
        }
        if (!Objects.equals(this.pointType, other.pointType)) {
            return false;
        }
        if (!Objects.equals(this.graphTimestamp, other.graphTimestamp)) {
            return false;
        }
        if (!Objects.equals(this.paramName, other.paramName)) {
            return false;
        }
        if (!Objects.equals(this.graphType, other.graphType)) {
            return false;
        }
        if (!Objects.equals(this.amplitudeUnit, other.amplitudeUnit)) {
            return false;
        }
        if (!Objects.equals(this.timeUnit, other.timeUnit)) {
            return false;
        }
        if (!Objects.equals(this.frequencyUnit, other.frequencyUnit)) {
            return false;
        }
        if (!Objects.equals(this.ampFactor, other.ampFactor)) {
            return false;
        }
        if (!Objects.equals(this.graphTitle, other.graphTitle)) {
            return false;
        }
        if (!Objects.equals(this.windowDisplayName, other.windowDisplayName)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        if (!Objects.equals(this.areaId, other.areaId)) {
            return false;
        }
        if (!Objects.equals(this.assetId, other.assetId)) {
            return false;
        }
        if (!Objects.equals(this.pointLocationId, other.pointLocationId)) {
            return false;
        }
        if (!Objects.equals(this.pointId, other.pointId)) {
            return false;
        }
        if (!Objects.equals(this.apSetId, other.apSetId)) {
            return false;
        }
        if (!Objects.equals(this.paramNameList, other.paramNameList)) {
            return false;
        }
        if (!Objects.equals(this.waveDataVO, other.waveDataVO)) {
            return false;
        }
        if (!Objects.equals(this.selectedApAlByPoint, other.selectedApAlByPoint)) {
            return false;
        }
        if (!Objects.equals(this.apAlByPointList, other.apAlByPointList)) {
            return false;
        }
        if (!Objects.equals(this.spectrumGraphData, other.spectrumGraphData)) {
            return false;
        }
        if (!Objects.equals(this.waveGraphData, other.waveGraphData)) {
            return false;
        }
        if (!Objects.equals(this.ffSetVOList, other.ffSetVOList)) {
            return false;
        }
        if (!Objects.equals(this.workOrderSelectedParamList, other.workOrderSelectedParamList)) {
            return false;
        }
        if (!Objects.equals(this.workOrderVO, other.workOrderVO)) {
            return false;
        }
        if (!Objects.equals(this.selectedAlarmLimitPoint, other.selectedAlarmLimitPoint)) {
            return false;
        }
        if (!Objects.equals(this.apSetList, other.apSetList)) {
            return false;
        }
        if (!Objects.equals(this.apSetItem, other.apSetItem)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ChartInfoVO{" + "siteId=" + siteId + ", areaId=" + areaId + ", assetId=" + assetId + ", pointLocationId=" + pointLocationId + ", pointId=" + pointId + ", apSetId=" + apSetId + ", siteName=" + siteName + ", region=" + region + ", country=" + country + ", areaName=" + areaName + ", assetName=" + assetName + ", pointLocationName=" + pointLocationName + ", serialNum=" + serialNum + ", pointName=" + pointName + ", sampleInterval=" + sampleInterval + ", resolution=" + resolution + ", pointType=" + pointType + ", paramNameList=" + paramNameList + ", waveDataVO=" + waveDataVO + ", graphTimestamp=" + graphTimestamp + ", selectedApAlByPoint=" + selectedApAlByPoint + ", apAlByPointList=" + apAlByPointList + ", spectrumGraphData=" + spectrumGraphData + ", waveGraphData=" + waveGraphData + ", selectedPointIndex=" + selectedPointIndex + ", selectedPlotLineIndex=" + selectedPlotLineIndex + ", ffSetVOList=" + ffSetVOList + ", workOrderSelectedParamList=" + workOrderSelectedParamList + ", workOrderVO=" + workOrderVO + ", selectedAlarmLimitPoint=" + selectedAlarmLimitPoint + ", spectrumLogYAxis=" + spectrumLogYAxis + ", lowFaultValidationFailed=" + lowFaultValidationFailed + ", lowAlertValidationFailed=" + lowAlertValidationFailed + ", highAlertValidationFailed=" + highAlertValidationFailed + ", highFaultValidationFailed=" + highFaultValidationFailed + ", logYaxis=" + logYaxis + ", plotOptionsModified=" + plotOptionsModified + ", disableParamAllSelection=" + disableParamAllSelection + ", count=" + count + ", paramName=" + paramName + ", graphType=" + graphType + ", amplitudeUnit=" + amplitudeUnit + ", timeUnit=" + timeUnit + ", frequencyUnit=" + frequencyUnit + ", ampFactor=" + ampFactor + ", graphTitle=" + graphTitle + ", windowDisplayName=" + windowDisplayName + ", yAxisUpperBound=" + yAxisUpperBound + ", apSetList=" + apSetList + ", refreshWaveAndSpectrum=" + refreshWaveAndSpectrum + ", apSetItem=" + apSetItem + '}';
    }
}
