/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.upcastcm.utils.vo;

/**
 *
 * @author gopal
 */
public class CustomHeatMapData {

    private String apSetId;
    private String pointId;
    private String pointLocationId;
    private Double value;

    public String getApSetId() {
        return apSetId;
    }

    public void setApSetId(String apSetId) {
        this.apSetId = apSetId;
    }

    public String getPointId() {
        return pointId;
    }

    public void setPointId(String pointId) {
        this.pointId = pointId;
    }

    public String getPointLocationId() {
        return pointLocationId;
    }

    public void setPointLocationId(String pointLocationId) {
        this.pointLocationId = pointLocationId;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "CustomHeatMapData{" + "apSetId=" + apSetId + ", pointId=" + pointId + ", pointLocationId=" + pointLocationId + ", value=" + value + '}';
    }

}
