/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.utils.vo;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author twilcox
 */
public class AlSetVO implements Serializable {
    private String alSetName;
    private UUID alSetId;
    private List<ApAlSetVO> AlSetList;

    public AlSetVO() {
    }

    public AlSetVO(String alSetName, UUID alSetId, List<ApAlSetVO> AlSetList) {
        this.alSetName = alSetName;
        this.alSetId = alSetId;
        this.AlSetList = AlSetList;
    }

    public AlSetVO(AlSetVO alSetVO) {
        alSetName = alSetVO.getAlSetName();
        alSetId = alSetVO.getAlSetId();
        AlSetList = alSetVO.getAlSetList();
    }

    public String getAlSetName() {
        return alSetName;
    }

    public void setAlSetName(String alSetName) {
        this.alSetName = alSetName;
    }

    public UUID getAlSetId() {
        return alSetId;
    }

    public void setAlSetId(UUID alSetId) {
        this.alSetId = alSetId;
    }

    public List<ApAlSetVO> getAlSetList() {
        return AlSetList;
    }

    public void setAlSetList(List<ApAlSetVO> AlSetList) {
        this.AlSetList = AlSetList;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 59 * hash + Objects.hashCode(this.alSetName);
        hash = 59 * hash + Objects.hashCode(this.alSetId);
        hash = 59 * hash + Objects.hashCode(this.AlSetList);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AlSetVO other = (AlSetVO) obj;
        if (!Objects.equals(this.alSetName, other.alSetName)) {
            return false;
        }
        if (!Objects.equals(this.alSetId, other.alSetId)) {
            return false;
        }
        if (!Objects.equals(this.AlSetList, other.AlSetList)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "AlSetVO{" + "alSetName=" + alSetName + ", alSetId=" + alSetId + ", AlSetList=" + AlSetList + '}';
    }
}
