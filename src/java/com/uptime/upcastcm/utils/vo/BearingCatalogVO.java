/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.upcastcm.utils.vo;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author madhavi
 */
public class BearingCatalogVO implements Serializable{

    private String customerAcct;
    private UUID siteId;
    private String vendorId;
    private String bearingId;
    private short bc;
    private float bpfi;
    private float bpfo;
    private float bsf;
    private float ftf;

    public BearingCatalogVO() {
    }

    public String getCustomerAcct() {
        return customerAcct;
    }

    public void setCustomerAcct(String customerAcct) {
        this.customerAcct = customerAcct;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getBearingId() {
        return bearingId;
    }

    public void setBearingId(String bearingId) {
        this.bearingId = bearingId;
    }

    public short getBc() {
        return bc;
    }

    public void setBc(short bc) {
        this.bc = bc;
    }

    public float getBpfi() {
        return bpfi;
    }

    public void setBpfi(float bpfi) {
        this.bpfi = bpfi;
    }

    public float getBpfo() {
        return bpfo;
    }

    public void setBpfo(float bpfo) {
        this.bpfo = bpfo;
    }

    public float getBsf() {
        return bsf;
    }

    public void setBsf(float bsf) {
        this.bsf = bsf;
    }

    public float getFtf() {
        return ftf;
    }

    public void setFtf(float ftf) {
        this.ftf = ftf;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.customerAcct);
        hash = 89 * hash + Objects.hashCode(this.siteId);
        hash = 89 * hash + Objects.hashCode(this.vendorId);
        hash = 89 * hash + Objects.hashCode(this.bearingId);
        hash = 89 * hash + this.bc;
        hash = 89 * hash + Float.floatToIntBits(this.bpfi);
        hash = 89 * hash + Float.floatToIntBits(this.bpfo);
        hash = 89 * hash + Float.floatToIntBits(this.bsf);
        hash = 89 * hash + Float.floatToIntBits(this.ftf);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BearingCatalogVO other = (BearingCatalogVO) obj;
        if (this.bc != other.bc) {
            return false;
        }
        if (Float.floatToIntBits(this.bpfi) != Float.floatToIntBits(other.bpfi)) {
            return false;
        }
        if (Float.floatToIntBits(this.bpfo) != Float.floatToIntBits(other.bpfo)) {
            return false;
        }
        if (Float.floatToIntBits(this.bsf) != Float.floatToIntBits(other.bsf)) {
            return false;
        }
        if (Float.floatToIntBits(this.ftf) != Float.floatToIntBits(other.ftf)) {
            return false;
        }
        if (!Objects.equals(this.customerAcct, other.customerAcct)) {
            return false;
        }
        if (!Objects.equals(this.vendorId, other.vendorId)) {
            return false;
        }
        if (!Objects.equals(this.bearingId, other.bearingId)) {
            return false;
        }
        return Objects.equals(this.siteId, other.siteId);
    }

    @Override
    public String toString() {
        return "BearingCatalogVO{" + "customerAcct=" + customerAcct + ", siteId=" + siteId + ", vendorId=" + vendorId + ", bearingId=" + bearingId + ", bc=" + bc + ", bpfi=" + bpfi + ", bpfo=" + bpfo + ", bsf=" + bsf + ", ftf=" + ftf + '}';
    }

}
