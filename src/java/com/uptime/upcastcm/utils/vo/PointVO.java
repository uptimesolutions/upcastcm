/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.utils.vo;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author twilcox
 */
public class PointVO implements Serializable {
    String customerAccount;
    UUID siteId;
    UUID areaId;
    UUID assetId;
    UUID pointLocationId;
    UUID pointId;
    UUID apSetId;
    UUID alSetId;
    String pointName;
    boolean disabled;
    String pointType;
    int sensorChannelNum;
    float sensorOffset;
    float sensorSensitivity;
    String sensorType;
    String sensorUnits;
    String sensorSubType;
    String sensorLocalOrientation;
    String paramName;
    boolean demodEnabled;
    float demodHighPass;
    float demodLowPass;
    int dwell;
    int fMax;
    String siteName;
    String areaName;
    String assetName;
    String pointLocationName;
    String apSetName;
    String alSetName;
    int demodInterval;
    String paramType;
    boolean alarmEnabled;
    String freqUnits;
    float highAlert;
    boolean highAlertActive;
    float highFault;
    boolean highFaultActive;
    boolean customized;
    float lowAlert;
    boolean lowAlertActive;
    float lowFault;
    boolean lowFaultActive;
    float maxFreq;
    float minFreq;
    String paramAmpFactor;
    String paramUnits;
    float period;
    int resolution;
    float sampleRate;
    List<ApAlSetVO> apAlSetVOs;
    int pointSerialNumber;
    String alarmEnabledValue;

    public PointVO() {
    }

    public PointVO(String customerAccount, UUID siteId, UUID areaId, UUID assetId, UUID pointLocationId, UUID pointId, UUID apSetId, UUID alSetId, String pointName, boolean disabled, String pointType, int sampleInterval, int sensorChannelNum, float sensorOffset, float sensorSensitivity, String sensorType, String sensorUnits, String sensorSubType, String sensorLocalOrientation, String paramName, boolean demodEnabled, float demodHighPass, float demodLowPass, int dwell, int fMax, String siteName, String areaName, String assetName, String pointLocationName, String apSetName, String alSetName, int demodInterval, String paramType, boolean alarmEnabled, String freqUnits, float highAlert, boolean highAlertActive, float highFault, boolean highFaultActive, boolean customized, float lowAlert, boolean lowAlertActive, float lowFault, boolean lowFaultActive, float maxFreq, float minFreq, String paramAmpFactor, String paramUnits, float period, int resolution, float sampleRate, List<ApAlSetVO> apAlSetVOs,String alarmEnabledValue) {
        this.customerAccount = customerAccount;
        this.siteId = siteId;
        this.areaId = areaId;
        this.assetId = assetId;
        this.pointLocationId = pointLocationId;
        this.pointId = pointId;
        this.apSetId = apSetId;
        this.alSetId = alSetId;
        this.pointName = pointName;
        this.disabled = disabled;
        this.pointType = pointType;
        this.sensorChannelNum = sensorChannelNum;
        this.sensorOffset = sensorOffset;
        this.sensorSensitivity = sensorSensitivity;
        this.sensorType = sensorType;
        this.sensorUnits = sensorUnits;
        this.sensorSubType = sensorSubType;
        this.sensorLocalOrientation = sensorLocalOrientation;
        this.paramName = paramName;
        this.demodEnabled = demodEnabled;
        this.demodHighPass = demodHighPass;
        this.demodLowPass = demodLowPass;
        this.dwell = dwell;
        this.fMax = fMax;
        this.siteName = siteName;
        this.areaName = areaName;
        this.assetName = assetName;
        this.pointLocationName = pointLocationName;
        this.apSetName = apSetName;
        this.alSetName = alSetName;
        this.demodInterval = demodInterval;
        this.paramType = paramType;
        this.alarmEnabled = alarmEnabled;
        this.freqUnits = freqUnits;
        this.highAlert = highAlert;
        this.highAlertActive = highAlertActive;
        this.highFault = highFault;
        this.highFaultActive = highFaultActive;
        this.customized = customized;
        this.lowAlert = lowAlert;
        this.lowAlertActive = lowAlertActive;
        this.lowFault = lowFault;
        this.lowFaultActive = lowFaultActive;
        this.maxFreq = maxFreq;
        this.minFreq = minFreq;
        this.paramAmpFactor = paramAmpFactor;
        this.paramUnits = paramUnits;
        this.period = period;
        this.resolution = resolution;
        this.sampleRate = sampleRate;
        this.apAlSetVOs = apAlSetVOs;
        this.alarmEnabledValue=alarmEnabledValue;
    }
    
    //this overloading constructor will be used for validate point data by providing these values.
    public PointVO(String pointType, int sensorChannelNum, float sensorSensitivity, String sensorUnits, String sensorSubType, String sensorLocalOrientation, float sensorOffset){
        this.pointType = pointType;
        this.sensorChannelNum = sensorChannelNum;
        this.sensorSensitivity = sensorSensitivity;
        this.sensorUnits = sensorUnits;
        this.sensorOffset = sensorOffset;
        this.sensorSubType = sensorSubType;
        this.sensorLocalOrientation = sensorLocalOrientation;
    }

    public PointVO(PointVO pointVO) {
        customerAccount = pointVO.getCustomerAccount();
        siteId = pointVO.getSiteId();
        areaId = pointVO.getAreaId();
        assetId = pointVO.getAssetId();
        pointLocationId = pointVO.getPointLocationId();
        pointId = pointVO.getPointId();
        apSetId = pointVO.getApSetId();
        alSetId = pointVO.getAlSetId();
        pointName = pointVO.getPointName();
        disabled = pointVO.isDisabled();
        pointType = pointVO.getPointType();
        sensorChannelNum = pointVO.getSensorChannelNum();
        sensorOffset = pointVO.getSensorOffset();
        sensorSensitivity = pointVO.getSensorSensitivity();
        sensorType = pointVO.getSensorType();
        sensorUnits = pointVO.getSensorUnits();
        sensorSubType = pointVO.getSensorSubType();
        sensorLocalOrientation = pointVO.getSensorLocalOrientation();
        paramName = pointVO.getParamName();
        demodEnabled = pointVO.isDemodEnabled();
        demodHighPass = pointVO.getDemodHighPass();
        demodLowPass = pointVO.getDemodLowPass();
        dwell = pointVO.getDwell();
        fMax = pointVO.getfMax();
        siteName = pointVO.getSiteName();
        areaName = pointVO.getAreaName();
        assetName = pointVO.getAssetName();
        pointLocationName = pointVO.getPointLocationName();
        apSetName = pointVO.getApSetName();
        alSetName = pointVO.getAlSetName();
        demodInterval = pointVO.getDemodInterval();
        paramType = pointVO.getParamType();
        alarmEnabled = pointVO.isAlarmEnabled();
        freqUnits = pointVO.getFreqUnits();
        highAlert = pointVO.getHighAlert();
        highAlertActive = pointVO.isHighAlertActive();
        highFault = pointVO.getHighFault();
        highFaultActive = pointVO.isHighFaultActive();
        customized = pointVO.isCustomized();
        lowAlert = pointVO.getLowAlert();
        lowAlertActive = pointVO.isLowAlertActive();
        lowFault = pointVO.getLowFault();
        lowFaultActive = pointVO.isLowFaultActive();
        maxFreq = pointVO.getMaxFreq();
        minFreq = pointVO.getMinFreq();
        paramAmpFactor = pointVO.getParamAmpFactor();
        paramUnits = pointVO.getParamUnits();
        period = pointVO.getPeriod();
        resolution = pointVO.getResolution();
        sampleRate = pointVO.getSampleRate();
        apAlSetVOs = pointVO.getApAlSetVOs();
        alarmEnabledValue =pointVO.getAlarmEnabledValue();
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public UUID getAreaId() {
        return areaId;
    }

    public void setAreaId(UUID areaId) {
        this.areaId = areaId;
    }

    public UUID getAssetId() {
        return assetId;
    }

    public void setAssetId(UUID assetId) {
        this.assetId = assetId;
    }

    public UUID getPointLocationId() {
        return pointLocationId;
    }

    public void setPointLocationId(UUID pointLocationId) {
        this.pointLocationId = pointLocationId;
    }

    public UUID getPointId() {
        return pointId;
    }

    public void setPointId(UUID pointId) {
        this.pointId = pointId;
    }

    public UUID getApSetId() {
        return apSetId;
    }

    public void setApSetId(UUID apSetId) {
        this.apSetId = apSetId;
    }

    public UUID getAlSetId() {
        return alSetId;
    }

    public void setAlSetId(UUID alSetId) {
        this.alSetId = alSetId;
    }

    public String getPointName() {
        return pointName;
    }

    public void setPointName(String pointName) {
        this.pointName = pointName;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    public String getPointType() {
        return pointType;
    }

    public void setPointType(String pointType) {
        this.pointType = pointType;
    }

    public int getSensorChannelNum() {
        return sensorChannelNum;
    }

    public void setSensorChannelNum(int sensorChannelNum) {
        this.sensorChannelNum = sensorChannelNum;
    }

    public float getSensorOffset() {
        return sensorOffset;
    }

    public void setSensorOffset(float sensorOffset) {
        this.sensorOffset = sensorOffset;
    }

    public float getSensorSensitivity() {
        return sensorSensitivity;
    }

    public void setSensorSensitivity(float sensorSensitivity) {
        this.sensorSensitivity = sensorSensitivity;
    }

    public String getSensorType() {
        return sensorType;
    }

    public void setSensorType(String sensorType) {
        this.sensorType = sensorType;
    }

    public String getSensorUnits() {
        return sensorUnits;
    }

    public void setSensorUnits(String sensorUnits) {
        this.sensorUnits = sensorUnits;
    }

    public String getParamName() {
        return paramName;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    public boolean isDemodEnabled() {
        return demodEnabled;
    }

    public void setDemodEnabled(boolean demodEnabled) {
        this.demodEnabled = demodEnabled;
    }

    public float getDemodHighPass() {
        return demodHighPass;
    }

    public void setDemodHighPass(float demodHighPass) {
        this.demodHighPass = demodHighPass;
    }

    public float getDemodLowPass() {
        return demodLowPass;
    }

    public void setDemodLowPass(float demodLowPass) {
        this.demodLowPass = demodLowPass;
    }

    public int getDwell() {
        return dwell;
    }

    public void setDwell(int dwell) {
        this.dwell = dwell;
    }

    public int getfMax() {
        return fMax;
    }

    public void setfMax(int fMax) {
        this.fMax = fMax;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getAssetName() {
        return assetName;
    }

    public void setAssetName(String assetName) {
        this.assetName = assetName;
    }

    public String getPointLocationName() {
        return pointLocationName;
    }

    public void setPointLocationName(String pointLocationName) {
        this.pointLocationName = pointLocationName;
    }

    public String getApSetName() {
        return apSetName;
    }

    public void setApSetName(String apSetName) {
        this.apSetName = apSetName;
    }

    public String getAlSetName() {
        return alSetName;
    }

    public void setAlSetName(String alSetName) {
        this.alSetName = alSetName;
    }

    public int getDemodInterval() {
        return demodInterval;
    }

    public void setDemodInterval(int demodInterval) {
        this.demodInterval = demodInterval;
    }

    public String getParamType() {
        return paramType;
    }

    public void setParamType(String paramType) {
        this.paramType = paramType;
    }

    public boolean isAlarmEnabled() {
        return alarmEnabled;
    }

    public void setAlarmEnabled(boolean alarmEnabled) {
        this.alarmEnabled = alarmEnabled;
    }

    public String getFreqUnits() {
        return freqUnits;
    }

    public void setFreqUnits(String freqUnits) {
        this.freqUnits = freqUnits;
    }

    public float getHighAlert() {
        return highAlert;
    }

    public void setHighAlert(float highAlert) {
        this.highAlert = highAlert;
    }

    public boolean isHighAlertActive() {
        return highAlertActive;
    }

    public void setHighAlertActive(boolean highAlertActive) {
        this.highAlertActive = highAlertActive;
    }

    public float getHighFault() {
        return highFault;
    }

    public void setHighFault(float highFault) {
        this.highFault = highFault;
    }

    public boolean isHighFaultActive() {
        return highFaultActive;
    }

    public void setHighFaultActive(boolean highFaultActive) {
        this.highFaultActive = highFaultActive;
    }

    public boolean isCustomized() {
        return customized;
    }

    public void setCustomized(boolean customized) {
        this.customized = customized;
    }

    public float getLowAlert() {
        return lowAlert;
    }

    public void setLowAlert(float lowAlert) {
        this.lowAlert = lowAlert;
    }

    public boolean isLowAlertActive() {
        return lowAlertActive;
    }

    public void setLowAlertActive(boolean lowAlertActive) {
        this.lowAlertActive = lowAlertActive;
    }

    public float getLowFault() {
        return lowFault;
    }

    public void setLowFault(float lowFault) {
        this.lowFault = lowFault;
    }

    public boolean isLowFaultActive() {
        return lowFaultActive;
    }

    public void setLowFaultActive(boolean lowFaultActive) {
        this.lowFaultActive = lowFaultActive;
    }

    public float getMaxFreq() {
        return maxFreq;
    }

    public void setMaxFreq(float maxFreq) {
        this.maxFreq = maxFreq;
    }

    public float getMinFreq() {
        return minFreq;
    }

    public void setMinFreq(float minFreq) {
        this.minFreq = minFreq;
    }

    public String getParamAmpFactor() {
        return paramAmpFactor;
    }

    public void setParamAmpFactor(String paramAmpFactor) {
        this.paramAmpFactor = paramAmpFactor;
    }

    public String getParamUnits() {
        return paramUnits;
    }

    public void setParamUnits(String paramUnits) {
        this.paramUnits = paramUnits;
    }

    public float getPeriod() {
        return period;
    }

    public void setPeriod(float period) {
        this.period = period;
    }

    public int getResolution() {
        return resolution;
    }

    public void setResolution(int resolution) {
        this.resolution = resolution;
    }

    public float getSampleRate() {
        return sampleRate;
    }

    public void setSampleRate(float sampleRate) {
        this.sampleRate = sampleRate;
    }

    public List<ApAlSetVO> getApAlSetVOs() {
        return apAlSetVOs;
    }

    public void setApAlSetVOs(List<ApAlSetVO> apAlSetVOs) {
        this.apAlSetVOs = apAlSetVOs;
    }

    public String getSensorSubType() {
        return sensorSubType;
    }

    public void setSensorSubType(String sensorSubType) {
        this.sensorSubType = sensorSubType;
    }

    public String getSensorLocalOrientation() {
        return sensorLocalOrientation;
    }

    public void setSensorLocalOrientation(String sensorLocalOrientation) {
        this.sensorLocalOrientation = sensorLocalOrientation;
    }

    public int getPointSerialNumber() {
        return pointSerialNumber;
    }

    public void setPointSerialNumber(int pointSerialNumber) {
        this.pointSerialNumber = pointSerialNumber;
    }

    public String getAlarmEnabledValue() {
        return alarmEnabledValue;
    }

    public void setAlarmEnabledValue(String alarmEnabledValue) {
        this.alarmEnabledValue = alarmEnabledValue;
    }
    
    
    
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 79 * hash + Objects.hashCode(this.customerAccount);
        hash = 79 * hash + Objects.hashCode(this.siteId);
        hash = 79 * hash + Objects.hashCode(this.areaId);
        hash = 79 * hash + Objects.hashCode(this.assetId);
        hash = 79 * hash + Objects.hashCode(this.pointLocationId);
        hash = 79 * hash + Objects.hashCode(this.pointId);
        hash = 79 * hash + Objects.hashCode(this.apSetId);
        hash = 79 * hash + Objects.hashCode(this.alSetId);
        hash = 79 * hash + Objects.hashCode(this.pointName);
        hash = 79 * hash + (this.disabled ? 1 : 0);
        hash = 79 * hash + Objects.hashCode(this.pointType);
        hash = 79 * hash + this.sensorChannelNum;
        hash = 79 * hash + Float.floatToIntBits(this.sensorOffset);
        hash = 79 * hash + Float.floatToIntBits(this.sensorSensitivity);
        hash = 79 * hash + Objects.hashCode(this.sensorType);
        hash = 79 * hash + Objects.hashCode(this.sensorUnits);
        hash = 79 * hash + Objects.hashCode(this.sensorSubType);
        hash = 79 * hash + Objects.hashCode(this.sensorLocalOrientation);
        hash = 79 * hash + Objects.hashCode(this.paramName);
        hash = 79 * hash + (this.demodEnabled ? 1 : 0);
        hash = 79 * hash + Float.floatToIntBits(this.demodHighPass);
        hash = 79 * hash + Float.floatToIntBits(this.demodLowPass);
        hash = 79 * hash + this.dwell;
        hash = 79 * hash + this.fMax;
        hash = 79 * hash + Objects.hashCode(this.siteName);
        hash = 79 * hash + Objects.hashCode(this.areaName);
        hash = 79 * hash + Objects.hashCode(this.assetName);
        hash = 79 * hash + Objects.hashCode(this.pointLocationName);
        hash = 79 * hash + Objects.hashCode(this.apSetName);
        hash = 79 * hash + Objects.hashCode(this.alSetName);
        hash = 79 * hash + this.demodInterval;
        hash = 79 * hash + Objects.hashCode(this.paramType);
        hash = 79 * hash + (this.alarmEnabled ? 1 : 0);
        hash = 79 * hash + Objects.hashCode(this.freqUnits);
        hash = 79 * hash + Float.floatToIntBits(this.highAlert);
        hash = 79 * hash + (this.highAlertActive ? 1 : 0);
        hash = 79 * hash + Float.floatToIntBits(this.highFault);
        hash = 79 * hash + (this.highFaultActive ? 1 : 0);
        hash = 79 * hash + (this.customized ? 1 : 0);
        hash = 79 * hash + Float.floatToIntBits(this.lowAlert);
        hash = 79 * hash + (this.lowAlertActive ? 1 : 0);
        hash = 79 * hash + Float.floatToIntBits(this.lowFault);
        hash = 79 * hash + (this.lowFaultActive ? 1 : 0);
        hash = 79 * hash + Float.floatToIntBits(this.maxFreq);
        hash = 79 * hash + Float.floatToIntBits(this.minFreq);
        hash = 79 * hash + Objects.hashCode(this.paramAmpFactor);
        hash = 79 * hash + Objects.hashCode(this.paramUnits);
        hash = 79 * hash + Float.floatToIntBits(this.period);
        hash = 79 * hash + this.resolution;
        hash = 79 * hash + Float.floatToIntBits(this.sampleRate);
        hash = 79 * hash + Objects.hashCode(this.apAlSetVOs);
        hash = 79 * hash + this.pointSerialNumber;
        hash = 79 * hash + Objects.hashCode(this.alarmEnabledValue);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PointVO other = (PointVO) obj;
        if (this.disabled != other.disabled) {
            return false;
        }
        if (this.sensorChannelNum != other.sensorChannelNum) {
            return false;
        }
        if (Float.floatToIntBits(this.sensorOffset) != Float.floatToIntBits(other.sensorOffset)) {
            return false;
        }
        if (Float.floatToIntBits(this.sensorSensitivity) != Float.floatToIntBits(other.sensorSensitivity)) {
            return false;
        }
        if (this.demodEnabled != other.demodEnabled) {
            return false;
        }
        if (Float.floatToIntBits(this.demodHighPass) != Float.floatToIntBits(other.demodHighPass)) {
            return false;
        }
        if (Float.floatToIntBits(this.demodLowPass) != Float.floatToIntBits(other.demodLowPass)) {
            return false;
        }
        if (this.dwell != other.dwell) {
            return false;
        }
        if (this.fMax != other.fMax) {
            return false;
        }
        if (this.demodInterval != other.demodInterval) {
            return false;
        }
        if (this.alarmEnabled != other.alarmEnabled) {
            return false;
        }
        if (Float.floatToIntBits(this.highAlert) != Float.floatToIntBits(other.highAlert)) {
            return false;
        }
        if (this.highAlertActive != other.highAlertActive) {
            return false;
        }
        if (Float.floatToIntBits(this.highFault) != Float.floatToIntBits(other.highFault)) {
            return false;
        }
        if (this.highFaultActive != other.highFaultActive) {
            return false;
        }
        if (this.customized != other.customized) {
            return false;
        }
        if (Float.floatToIntBits(this.lowAlert) != Float.floatToIntBits(other.lowAlert)) {
            return false;
        }
        if (this.lowAlertActive != other.lowAlertActive) {
            return false;
        }
        if (Float.floatToIntBits(this.lowFault) != Float.floatToIntBits(other.lowFault)) {
            return false;
        }
        if (this.lowFaultActive != other.lowFaultActive) {
            return false;
        }
        if (Float.floatToIntBits(this.maxFreq) != Float.floatToIntBits(other.maxFreq)) {
            return false;
        }
        if (Float.floatToIntBits(this.minFreq) != Float.floatToIntBits(other.minFreq)) {
            return false;
        }
        if (Float.floatToIntBits(this.period) != Float.floatToIntBits(other.period)) {
            return false;
        }
        if (this.resolution != other.resolution) {
            return false;
        }
        if (Float.floatToIntBits(this.sampleRate) != Float.floatToIntBits(other.sampleRate)) {
            return false;
        }
        if (this.pointSerialNumber != other.pointSerialNumber) {
            return false;
        }
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.pointName, other.pointName)) {
            return false;
        }
        if (!Objects.equals(this.pointType, other.pointType)) {
            return false;
        }
        if (!Objects.equals(this.sensorType, other.sensorType)) {
            return false;
        }
        if (!Objects.equals(this.sensorUnits, other.sensorUnits)) {
            return false;
        }
        if (!Objects.equals(this.sensorSubType, other.sensorSubType)) {
            return false;
        }
        if (!Objects.equals(this.sensorLocalOrientation, other.sensorLocalOrientation)) {
            return false;
        }
        if (!Objects.equals(this.paramName, other.paramName)) {
            return false;
        }
        if (!Objects.equals(this.siteName, other.siteName)) {
            return false;
        }
        if (!Objects.equals(this.areaName, other.areaName)) {
            return false;
        }
        if (!Objects.equals(this.assetName, other.assetName)) {
            return false;
        }
        if (!Objects.equals(this.pointLocationName, other.pointLocationName)) {
            return false;
        }
        if (!Objects.equals(this.apSetName, other.apSetName)) {
            return false;
        }
        if (!Objects.equals(this.alSetName, other.alSetName)) {
            return false;
        }
        if (!Objects.equals(this.paramType, other.paramType)) {
            return false;
        }
        if (!Objects.equals(this.freqUnits, other.freqUnits)) {
            return false;
        }
        if (!Objects.equals(this.paramAmpFactor, other.paramAmpFactor)) {
            return false;
        }
        if (!Objects.equals(this.paramUnits, other.paramUnits)) {
            return false;
        }
        if (!Objects.equals(this.alarmEnabledValue, other.alarmEnabledValue)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        if (!Objects.equals(this.areaId, other.areaId)) {
            return false;
        }
        if (!Objects.equals(this.assetId, other.assetId)) {
            return false;
        }
        if (!Objects.equals(this.pointLocationId, other.pointLocationId)) {
            return false;
        }
        if (!Objects.equals(this.pointId, other.pointId)) {
            return false;
        }
        if (!Objects.equals(this.apSetId, other.apSetId)) {
            return false;
        }
        if (!Objects.equals(this.alSetId, other.alSetId)) {
            return false;
        }
        return Objects.equals(this.apAlSetVOs, other.apAlSetVOs);
    }

    @Override
    public String toString() {
        return "PointVO{" + "customerAccount=" + customerAccount + ", siteId=" + siteId + ", areaId=" + areaId + ", assetId=" + assetId + ", pointLocationId=" + pointLocationId + ", pointId=" + pointId + ", apSetId=" + apSetId + ", alSetId=" + alSetId + ", pointName=" + pointName + ", disabled=" + disabled + ", pointType=" + pointType + ", sensorChannelNum=" + sensorChannelNum + ", sensorOffset=" + sensorOffset + ", sensorSensitivity=" + sensorSensitivity + ", sensorType=" + sensorType + ", sensorUnits=" + sensorUnits + ", sensorSubType=" + sensorSubType + ", sensorLocalOrientation=" + sensorLocalOrientation + ", paramName=" + paramName + ", demodEnabled=" + demodEnabled + ", demodHighPass=" + demodHighPass + ", demodLowPass=" + demodLowPass + ", dwell=" + dwell + ", fMax=" + fMax + ", siteName=" + siteName + ", areaName=" + areaName + ", assetName=" + assetName + ", pointLocationName=" + pointLocationName + ", apSetName=" + apSetName + ", alSetName=" + alSetName + ", demodInterval=" + demodInterval + ", paramType=" + paramType + ", alarmEnabled=" + alarmEnabled + ", freqUnits=" + freqUnits + ", highAlert=" + highAlert + ", highAlertActive=" + highAlertActive + ", highFault=" + highFault + ", highFaultActive=" + highFaultActive + ", customized=" + customized + ", lowAlert=" + lowAlert + ", lowAlertActive=" + lowAlertActive + ", lowFault=" + lowFault + ", lowFaultActive=" + lowFaultActive + ", maxFreq=" + maxFreq + ", minFreq=" + minFreq + ", paramAmpFactor=" + paramAmpFactor + ", paramUnits=" + paramUnits + ", period=" + period + ", resolution=" + resolution + ", sampleRate=" + sampleRate + ", apAlSetVOs=" + apAlSetVOs + ", pointSerialNumber=" + pointSerialNumber + ", alarmEnabledValue=" + alarmEnabledValue + '}';
    }

   
}
