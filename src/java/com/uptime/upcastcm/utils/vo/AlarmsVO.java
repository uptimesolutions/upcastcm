/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.utils.vo;

import static com.uptime.client.utils.JsonConverterUtil.DATE_TIME_OFFSET_FORMAT;
import com.uptime.upcastcm.bean.UserManageBean;
import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Objects;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;

/**
 *
 * @author gsingh
 */
public class AlarmsVO implements Serializable {

    private String customerAccount;
    private UUID siteId;
    private UUID areaId;
    private UUID assetId;
    private UUID pointLocationId;
    private UUID pointId;
    private UUID apSetId;
    private float exceedPercent;
    private float highAlert;
    private float highFault;
    private float lowAlert;
    private float lowFault;
    private String paramType;
    private String siteName;
    private String sensorType;
    private float trendValue;
    private float trendValueFormatted;
    private int createdYear;
    private int createdMonth;
    private String alSetName;
    private String alarmType;
    private String apSetName;
    private String areaName;
    private String assetName;
    private int createdDate;
    private String createdTime;
    private boolean acknowledged;
    private boolean deleted;
    private String paramName;
    private String pointLocationName;
    private String pointName;
    private String sampleTimestamp;
    private String triggerTime;
    private UUID spectrumId;
    private UUID trendId;
    private float triggerValue;
    private UUID waveId;
    private Integer alarmCount;
    private int index;
    private String formattedSampleTimestamp;
    private Integer alertCount;
    private Integer faultCount;

    public AlarmsVO() {
    }

    public AlarmsVO(String customerAccount, UUID siteId, UUID areaId, UUID assetId, UUID pointLocationId, UUID pointId, UUID apSetId, float exceedPercent, float highAlert, float highFault, float lowAlert, float lowFault, String paramType, String siteName, String sensorType, float trendValue, int createdYear, int createdMonth, String alSetName, String alarmType, String apSetName, String areaName, String assetName, int createdDate, String createdTime, boolean acknowledged, boolean deleted, String paramName, String pointLocationName, String pointName, String sampleTimestamp, String triggerTime, UUID spectrumId, UUID trendId, float triggerValue, UUID waveId, int alarmCount, int index, int alertCount, int faultCount) {
        this.customerAccount = customerAccount;
        this.siteId = siteId;
        this.areaId = areaId;
        this.assetId = assetId;
        this.pointLocationId = pointLocationId;
        this.pointId = pointId;
        this.apSetId = apSetId;
        this.exceedPercent = exceedPercent;
        this.highAlert = highAlert;
        this.highFault = highFault;
        this.lowAlert = lowAlert;
        this.lowFault = lowFault;
        this.paramType = paramType;
        this.siteName = siteName;
        this.sensorType = sensorType;
        this.trendValue = trendValue;
        this.createdYear = createdYear;
        this.createdMonth = createdMonth;
        this.alSetName = alSetName;
        this.alarmType = alarmType;
        this.apSetName = apSetName;
        this.areaName = areaName;
        this.assetName = assetName;
        this.createdDate = createdDate;
        this.createdTime = createdTime;
        this.acknowledged = acknowledged;
        this.deleted = deleted;
        this.paramName = paramName;
        this.pointLocationName = pointLocationName;
        this.pointName = pointName;
        this.sampleTimestamp = sampleTimestamp;
        this.triggerTime = triggerTime;
        this.spectrumId = spectrumId;
        this.trendId = trendId;
        this.triggerValue = triggerValue;
        this.waveId = waveId;
        this.alarmCount = alarmCount;
        this.index = index;
        this.alertCount=alertCount;
        this.faultCount=faultCount;
    }

    public AlarmsVO(AlarmsVO alarmsVO) {
        customerAccount = alarmsVO.getCustomerAccount();
        siteId = alarmsVO.getSiteId();
        areaId = alarmsVO.getAreaId();
        assetId = alarmsVO.getAssetId();
        pointLocationId = alarmsVO.getPointLocationId();
        pointId = alarmsVO.getPointId();
        apSetId = alarmsVO.getApSetId();
        exceedPercent = alarmsVO.getExceedPercent();
        highAlert = alarmsVO.getHighAlert();
        highFault = alarmsVO.getHighFault();
        lowAlert = alarmsVO.getLowAlert();
        lowFault = alarmsVO.getLowFault();
        paramType = alarmsVO.getParamType();
        siteName = alarmsVO.getSiteName();
        sensorType = alarmsVO.getSensorType();
        trendValue = alarmsVO.getTrendValue();
        trendValueFormatted = alarmsVO.getTrendValueFormatted();
        createdYear = alarmsVO.getCreatedYear();
        createdMonth = alarmsVO.getCreatedMonth();
        alSetName = alarmsVO.getAlSetName();
        alarmType = alarmsVO.getAlarmType();
        apSetName = alarmsVO.getApSetName();
        areaName = alarmsVO.getAreaName();
        assetName = alarmsVO.getAssetName();
        createdDate = alarmsVO.getCreatedDate();
        createdTime = alarmsVO.getCreatedTime();
        acknowledged = alarmsVO.isAcknowledged();
        deleted = alarmsVO.isDeleted();
        paramName = alarmsVO.getParamName();
        pointLocationName = alarmsVO.getPointLocationName();
        pointName = alarmsVO.getPointName();
        sampleTimestamp = alarmsVO.getSampleTimestamp();
        triggerTime = alarmsVO.getTriggerTime();
        spectrumId = alarmsVO.getSpectrumId();
        trendId = alarmsVO.getTrendId();
        triggerValue = alarmsVO.getTriggerValue();
        waveId = alarmsVO.getWaveId();
        alarmCount = alarmsVO.getAlarmCount();
        alertCount = alarmsVO.getAlertCount();
        faultCount = alarmsVO.getFaultCount();
        index = alarmsVO.getIndex();
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public UUID getAreaId() {
        return areaId;
    }

    public void setAreaId(UUID areaId) {
        this.areaId = areaId;
    }

    public UUID getAssetId() {
        return assetId;
    }

    public void setAssetId(UUID assetId) {
        this.assetId = assetId;
    }

    public UUID getPointLocationId() {
        return pointLocationId;
    }

    public void setPointLocationId(UUID pointLocationId) {
        this.pointLocationId = pointLocationId;
    }

    public UUID getPointId() {
        return pointId;
    }

    public void setPointId(UUID pointId) {
        this.pointId = pointId;
    }

    public UUID getApSetId() {
        return apSetId;
    }

    public void setApSetId(UUID apSetId) {
        this.apSetId = apSetId;
    }

    public float getExceedPercent() {
        return exceedPercent;
    }

    public void setExceedPercent(float exceedPercent) {
        this.exceedPercent = exceedPercent;
    }

    public float getHighAlert() {
        return highAlert;
    }

    public void setHighAlert(float highAlert) {
        this.highAlert = highAlert;
    }

    public float getHighFault() {
        return highFault;
    }

    public void setHighFault(float highFault) {
        this.highFault = highFault;
    }

    public float getLowAlert() {
        return lowAlert;
    }

    public void setLowAlert(float lowAlert) {
        this.lowAlert = lowAlert;
    }

    public float getLowFault() {
        return lowFault;
    }

    public void setLowFault(float lowFault) {
        this.lowFault = lowFault;
    }

    public String getParamType() {
        return paramType;
    }

    public void setParamType(String paramType) {
        this.paramType = paramType;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public String getSensorType() {
        return sensorType;
    }

    public void setSensorType(String sensorType) {
        this.sensorType = sensorType;
    }

    public float getTrendValue() {
        return trendValue;
    }

    public void setTrendValue(float trendValue) {
        this.trendValue = trendValue;
    }

    public float getTrendValueFormatted() {
        return trendValueFormatted;
    }

    public void setTrendValueFormatted(float trendValueFormatted) {
        this.trendValueFormatted = trendValueFormatted;
    }

    public int getCreatedYear() {
        return createdYear;
    }

    public void setCreatedYear(int createdYear) {
        this.createdYear = createdYear;
    }

    public int getCreatedMonth() {
        return createdMonth;
    }

    public void setCreatedMonth(int createdMonth) {
        this.createdMonth = createdMonth;
    }

    public String getAlSetName() {
        return alSetName;
    }

    public void setAlSetName(String alSetName) {
        this.alSetName = alSetName;
    }

    public String getAlarmType() {
        return alarmType;
    }

    public void setAlarmType(String alarmType) {
        this.alarmType = alarmType;
    }

    public String getApSetName() {
        return apSetName;
    }

    public void setApSetName(String apSetName) {
        this.apSetName = apSetName;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getAssetName() {
        return assetName;
    }

    public void setAssetName(String assetName) {
        this.assetName = assetName;
    }

    public int getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(int createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public boolean isAcknowledged() {
        return acknowledged;
    }

    public void setAcknowledged(boolean acknowledged) {
        this.acknowledged = acknowledged;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public String getParamName() {
        return paramName;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    public String getPointLocationName() {
        return pointLocationName;
    }

    public void setPointLocationName(String pointLocationName) {
        this.pointLocationName = pointLocationName;
    }

    public String getPointName() {
        return pointName;
    }

    public void setPointName(String pointName) {
        this.pointName = pointName;
    }

    public String getSampleTimestamp() {
        return sampleTimestamp;
    }

    public void setSampleTimestamp(String sampleTimestamp) {
        UserManageBean userManageBean;

        this.sampleTimestamp = sampleTimestamp;

        if (sampleTimestamp != null && !sampleTimestamp.isEmpty() && (userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean")) != null) {
            this.formattedSampleTimestamp = userManageBean.getTzOffsetString(getInstantDateTimestamp(), 1);
        }
    }

    public String getTriggerTime() {
        return triggerTime;
    }

    public void setTriggerTime(String triggerTime) {
        this.triggerTime = triggerTime;
    }

    public UUID getSpectrumId() {
        return spectrumId;
    }

    public void setSpectrumId(UUID spectrumId) {
        this.spectrumId = spectrumId;
    }

    public UUID getTrendId() {
        return trendId;
    }

    public void setTrendId(UUID trendId) {
        this.trendId = trendId;
    }

    public float getTriggerValue() {
        return triggerValue;
    }

    public void setTriggerValue(float triggerValue) {
        this.triggerValue = triggerValue;
    }

    public UUID getWaveId() {
        return waveId;
    }

    public void setWaveId(UUID waveId) {
        this.waveId = waveId;
    }

    public Integer getAlarmCount() {
        return alarmCount;
    }

    public void setAlarmCount(Integer alarmCount) {
        this.alarmCount = alarmCount;
    }

    public Integer getAlertCount() {
        return alertCount;
    }

    public void setAlertCount(Integer alertCount) {
        this.alertCount = alertCount;
    }

    public Integer getFaultCount() {
        return faultCount;
    }

    public void setFaultCount(Integer faultCount) {
        this.faultCount = faultCount;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public Instant getInstantDateTimestamp() {
        if (sampleTimestamp != null && !sampleTimestamp.isEmpty()) {
            try {
                return LocalDateTime.parse(sampleTimestamp, DateTimeFormatter.ofPattern(DATE_TIME_OFFSET_FORMAT)).atZone(ZoneOffset.UTC).toInstant();
            } catch (Exception e) {
                Logger.getLogger(AlarmsVO.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            }
        }
        return null;
    }

    public String getFormattedSampleTimestamp() {
        return formattedSampleTimestamp;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 61 * hash + Objects.hashCode(this.customerAccount);
        hash = 61 * hash + Objects.hashCode(this.siteId);
        hash = 61 * hash + Objects.hashCode(this.areaId);
        hash = 61 * hash + Objects.hashCode(this.assetId);
        hash = 61 * hash + Objects.hashCode(this.pointLocationId);
        hash = 61 * hash + Objects.hashCode(this.pointId);
        hash = 61 * hash + Objects.hashCode(this.apSetId);
        hash = 61 * hash + Float.floatToIntBits(this.exceedPercent);
        hash = 61 * hash + Float.floatToIntBits(this.highAlert);
        hash = 61 * hash + Float.floatToIntBits(this.highFault);
        hash = 61 * hash + Float.floatToIntBits(this.lowAlert);
        hash = 61 * hash + Float.floatToIntBits(this.lowFault);
        hash = 61 * hash + Objects.hashCode(this.paramType);
        hash = 61 * hash + Objects.hashCode(this.siteName);
        hash = 61 * hash + Objects.hashCode(this.sensorType);
        hash = 61 * hash + Float.floatToIntBits(this.trendValue);
        hash = 61 * hash + this.createdYear;
        hash = 61 * hash + this.createdMonth;
        hash = 61 * hash + Objects.hashCode(this.alSetName);
        hash = 61 * hash + Objects.hashCode(this.alarmType);
        hash = 61 * hash + Objects.hashCode(this.apSetName);
        hash = 61 * hash + Objects.hashCode(this.areaName);
        hash = 61 * hash + Objects.hashCode(this.assetName);
        hash = 61 * hash + this.createdDate;
        hash = 61 * hash + Objects.hashCode(this.createdTime);
        hash = 61 * hash + (this.acknowledged ? 1 : 0);
        hash = 61 * hash + (this.deleted ? 1 : 0);
        hash = 61 * hash + Objects.hashCode(this.paramName);
        hash = 61 * hash + Objects.hashCode(this.pointLocationName);
        hash = 61 * hash + Objects.hashCode(this.pointName);
        hash = 61 * hash + Objects.hashCode(this.sampleTimestamp);
        hash = 61 * hash + Objects.hashCode(this.triggerTime);
        hash = 61 * hash + Objects.hashCode(this.spectrumId);
        hash = 61 * hash + Objects.hashCode(this.trendId);
        hash = 61 * hash + Float.floatToIntBits(this.triggerValue);
        hash = 61 * hash + Objects.hashCode(this.waveId);
        hash = 61 * hash + Objects.hashCode(this.alarmCount);
        hash = 61 * hash + Objects.hashCode(this.alertCount);
        hash = 61 * hash + Objects.hashCode(this.faultCount);
        hash = 61 * hash + this.index;
        hash = 61 * hash + Objects.hashCode(this.formattedSampleTimestamp);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AlarmsVO other = (AlarmsVO) obj;
        if (Float.floatToIntBits(this.exceedPercent) != Float.floatToIntBits(other.exceedPercent)) {
            return false;
        }
        if (Float.floatToIntBits(this.highAlert) != Float.floatToIntBits(other.highAlert)) {
            return false;
        }
        if (Float.floatToIntBits(this.highFault) != Float.floatToIntBits(other.highFault)) {
            return false;
        }
        if (Float.floatToIntBits(this.lowAlert) != Float.floatToIntBits(other.lowAlert)) {
            return false;
        }
        if (Float.floatToIntBits(this.lowFault) != Float.floatToIntBits(other.lowFault)) {
            return false;
        }
        if (Float.floatToIntBits(this.trendValue) != Float.floatToIntBits(other.trendValue)) {
            return false;
        }
        if (this.createdYear != other.createdYear) {
            return false;
        }
        if (this.createdMonth != other.createdMonth) {
            return false;
        }
        if (this.createdDate != other.createdDate) {
            return false;
        }
        if (this.acknowledged != other.acknowledged) {
            return false;
        }
        if (this.deleted != other.deleted) {
            return false;
        }
        if (Float.floatToIntBits(this.triggerValue) != Float.floatToIntBits(other.triggerValue)) {
            return false;
        }
        if (this.index != other.index) {
            return false;
        }
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.paramType, other.paramType)) {
            return false;
        }
        if (!Objects.equals(this.siteName, other.siteName)) {
            return false;
        }
        if (!Objects.equals(this.sensorType, other.sensorType)) {
            return false;
        }
        if (!Objects.equals(this.alSetName, other.alSetName)) {
            return false;
        }
        if (!Objects.equals(this.alarmType, other.alarmType)) {
            return false;
        }
        if (!Objects.equals(this.apSetName, other.apSetName)) {
            return false;
        }
        if (!Objects.equals(this.areaName, other.areaName)) {
            return false;
        }
        if (!Objects.equals(this.assetName, other.assetName)) {
            return false;
        }
        if (!Objects.equals(this.createdTime, other.createdTime)) {
            return false;
        }
        if (!Objects.equals(this.paramName, other.paramName)) {
            return false;
        }
        if (!Objects.equals(this.pointLocationName, other.pointLocationName)) {
            return false;
        }
        if (!Objects.equals(this.pointName, other.pointName)) {
            return false;
        }
        if (!Objects.equals(this.sampleTimestamp, other.sampleTimestamp)) {
            return false;
        }
        if (!Objects.equals(this.triggerTime, other.triggerTime)) {
            return false;
        }
        if (!Objects.equals(this.formattedSampleTimestamp, other.formattedSampleTimestamp)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        if (!Objects.equals(this.areaId, other.areaId)) {
            return false;
        }
        if (!Objects.equals(this.assetId, other.assetId)) {
            return false;
        }
        if (!Objects.equals(this.pointLocationId, other.pointLocationId)) {
            return false;
        }
        if (!Objects.equals(this.pointId, other.pointId)) {
            return false;
        }
        if (!Objects.equals(this.apSetId, other.apSetId)) {
            return false;
        }
        if (!Objects.equals(this.spectrumId, other.spectrumId)) {
            return false;
        }
        if (!Objects.equals(this.trendId, other.trendId)) {
            return false;
        }
        if (!Objects.equals(this.waveId, other.waveId)) {
            return false;
        }
        if (!Objects.equals(this.alarmCount, other.alarmCount)) {
            return false;
        }
        if (!Objects.equals(this.alertCount, other.alertCount)) {
            return false;
        }
        if (!Objects.equals(this.faultCount, other.faultCount)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "AlarmsVO{" + "customerAccount=" + customerAccount + ", siteId=" + siteId + ", areaId=" + areaId + ", assetId=" + assetId + ", pointLocationId=" + pointLocationId + ", pointId=" + pointId + ", apSetId=" + apSetId + ", exceedPercent=" + exceedPercent + ", highAlert=" + highAlert + ", highFault=" + highFault + ", lowAlert=" + lowAlert + ", lowFault=" + lowFault + ", paramType=" + paramType + ", siteName=" + siteName + ", sensorType=" + sensorType + ", trendValue=" + trendValue + ", createdYear=" + createdYear + ", createdMonth=" + createdMonth + ", alSetName=" + alSetName + ", alarmType=" + alarmType + ", apSetName=" + apSetName + ", areaName=" + areaName + ", assetName=" + assetName + ", createdDate=" + createdDate + ", createdTime=" + createdTime + ", acknowledged=" + acknowledged + ", deleted=" + deleted + ", paramName=" + paramName + ", pointLocationName=" + pointLocationName + ", pointName=" + pointName + ", sampleTimestamp=" + sampleTimestamp + ", triggerTime=" + triggerTime + ", spectrumId=" + spectrumId + ", trendId=" + trendId + ", triggerValue=" + triggerValue + ", waveId=" + waveId + ", alarmCount=" + alarmCount + ", alertCount=" + alertCount + ", faultCount=" + faultCount + ", index=" + index + ", formattedSampleTimestamp=" + formattedSampleTimestamp + '}';
    }
}
