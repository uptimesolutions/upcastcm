/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.upcastcm.utils.vo;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author Joseph
 */
public class FaultFrequenciesVO implements Serializable {
    private String customerAccount;
    private String siteName;
    private String ffSetName;
    private String ffName;
    private String ffSetDesc;
    private float ffValue;
    private String ffUnit;
    private int ffStartHarmonic;
    private int ffEndHarmonic;
    private String ffType;
    private UUID siteId;
    private UUID ffSetId;
    private UUID ffId;
    private boolean global;
    

    public FaultFrequenciesVO() {
    }

    public FaultFrequenciesVO(FaultFrequenciesVO faultFrequenciesVO) {
        customerAccount = faultFrequenciesVO.getCustomerAccount();
        siteName = faultFrequenciesVO.getSiteName();
        ffSetName = faultFrequenciesVO.getFfSetName();
        ffName = faultFrequenciesVO.getFfName();
        ffSetDesc = faultFrequenciesVO.getFfSetDesc();
        ffValue = faultFrequenciesVO.getFfValue();
        ffUnit = faultFrequenciesVO.getFfUnit();
        ffStartHarmonic = faultFrequenciesVO.getFfStartHarmonic();
        ffEndHarmonic = faultFrequenciesVO.getFfEndHarmonic();
        ffType = faultFrequenciesVO.getFfType();
        siteId = faultFrequenciesVO.getSiteId();
        ffSetId = faultFrequenciesVO.getFfSetId();
        ffId = faultFrequenciesVO.getFfId();
        global = faultFrequenciesVO.isGlobal();
        
    }

    public FaultFrequenciesVO(String customerAccount, String siteName, String ffSetName, String ffName, String ffSetDesc, float ffValue, String ffUnit, int ffStartHarmonic, int ffEndHarmonic, String ffType, UUID siteId, UUID ffSetId, UUID ffId) {
        this.customerAccount = customerAccount;
        this.siteName = siteName;
        this.ffSetName = ffSetName;
        this.ffName = ffName;
        this.ffSetDesc = ffSetDesc;
        this.ffValue = ffValue;
        this.ffUnit = ffUnit;
        this.ffStartHarmonic = ffStartHarmonic;
        this.ffEndHarmonic = ffEndHarmonic;
        this.ffType = ffType;
        this.siteId = siteId;
        this.ffSetId = ffSetId;
        this.ffId = ffId;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public UUID getFfSetId() {
        return ffSetId;
    }

    public void setFfSetId(UUID ffSetId) {
        this.ffSetId = ffSetId;
    }

    public UUID getFfId() {
        return ffId;
    }

    public void setFfId(UUID ffId) {
        this.ffId = ffId;
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public String getFfSetName() {
        return ffSetName;
    }

    public void setFfSetName(String ffSetName) {
        this.ffSetName = ffSetName;
    }

    public String getFfName() {
        return ffName;
    }

    public void setFfName(String ffName) {
        this.ffName = ffName;
    }

    public String getFfSetDesc() {
        return ffSetDesc;
    }

    public void setFfSetDesc(String ffSetDesc) {
        this.ffSetDesc = ffSetDesc;
    }

    public float getFfValue() {
        return ffValue;
    }

    public void setFfValue(float ffValue) {
        this.ffValue = ffValue;
    }

    public String getFfUnit() {
        return ffUnit;
    }

    public void setFfUnit(String ffUnit) {
        this.ffUnit = ffUnit;
    }

    public int getFfStartHarmonic() {
        return ffStartHarmonic;
    }

    public void setFfStartHarmonic(int ffStartHarmonic) {
        this.ffStartHarmonic = ffStartHarmonic;
    }

    public int getFfEndHarmonic() {
        return ffEndHarmonic;
    }

    public void setFfEndHarmonic(int ffEndHarmonic) {
        this.ffEndHarmonic = ffEndHarmonic;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public String getFfType() {
        return ffType;
    }

    public void setFfType(String ffType) {
        this.ffType = ffType;
    }

    public boolean isGlobal() {
        return global;
    }

    public void setGlobal(boolean global) {
        this.global = global;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + Objects.hashCode(this.customerAccount);
        hash = 71 * hash + Objects.hashCode(this.siteName);
        hash = 71 * hash + Objects.hashCode(this.ffSetName);
        hash = 71 * hash + Objects.hashCode(this.ffName);
        hash = 71 * hash + Objects.hashCode(this.ffSetDesc);
        hash = 71 * hash + Float.floatToIntBits(this.ffValue);
        hash = 71 * hash + Objects.hashCode(this.ffUnit);
        hash = 71 * hash + this.ffStartHarmonic;
        hash = 71 * hash + this.ffEndHarmonic;
        hash = 71 * hash + Objects.hashCode(this.ffType);
        hash = 71 * hash + Objects.hashCode(this.siteId);
        hash = 71 * hash + Objects.hashCode(this.ffSetId);
        hash = 71 * hash + Objects.hashCode(this.ffId);
        hash = 71 * hash + (this.global ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FaultFrequenciesVO other = (FaultFrequenciesVO) obj;
        if (Float.floatToIntBits(this.ffValue) != Float.floatToIntBits(other.ffValue)) {
            return false;
        }
        if (this.ffStartHarmonic != other.ffStartHarmonic) {
            return false;
        }
        if (this.ffEndHarmonic != other.ffEndHarmonic) {
            return false;
        }
        if (this.global != other.global) {
            return false;
        }
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.siteName, other.siteName)) {
            return false;
        }
        if (!Objects.equals(this.ffSetName, other.ffSetName)) {
            return false;
        }
        if (!Objects.equals(this.ffName, other.ffName)) {
            return false;
        }
        if (!Objects.equals(this.ffSetDesc, other.ffSetDesc)) {
            return false;
        }
        if (!Objects.equals(this.ffUnit, other.ffUnit)) {
            return false;
        }
        if (!Objects.equals(this.ffType, other.ffType)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        if (!Objects.equals(this.ffSetId, other.ffSetId)) {
            return false;
        }
        if (!Objects.equals(this.ffId, other.ffId)) {
            return false;
        }
        return true;
    }

    
    @Override
    public String toString() {
        return "FaultFrequenciesVO{" + "customerAccount=" + customerAccount + ", siteName=" + siteName + ", ffSetName=" + ffSetName + ", ffName=" + ffName + ", ffSetDesc=" + ffSetDesc + ", ffValue=" + ffValue + ", ffUnit=" + ffUnit + ", ffStartHarmonic=" + ffStartHarmonic + ", ffEndHarmonic=" + ffEndHarmonic + ", ffType=" + ffType + ", siteId=" + siteId + ", ffSetId=" + ffSetId + ", ffId=" + ffId + ", global=" + global + '}';
    }

  

}
