/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.utils.vo;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;
import javax.faces.model.SelectItem;


/**
 *
 * @author madhavi
 */

public class ApAlSetVO implements Serializable {
    private String customerAccount;
    private String siteName;
    private UUID siteId;
    private String apSetName;
    private UUID apSetId;
    private String alSetName;
    private UUID alSetId;
    private String paramName;
    private String sensorType;
    private boolean active;
    private int fmax;
    private int resolution;
    private float period;
    private float sampleRate;
    private boolean demod;
    private float demodHighPass;
    private float demodLowPass;
    private String paramType;
    private String frequencyUnits;
    private String paramUnits;
    private String paramAmpFactor;
    private float minFrequency;
    private float maxFrequency;
    private int dwell;
    private boolean lowFaultActive;
    private boolean lowAlertActive;
    private boolean highAlertActive;
    private boolean highFaultActive;
    private float lowFault;
    private float lowAlert;
    private float highAlert;
    private float highFault;
    private SelectItem selectedAlSet;
    
    private boolean lowFaultFailure;
    private boolean lowAlertFailure;
    private boolean highAlertFailure;
    private boolean highFaultFailure;    
    private boolean customized;   
    private boolean readOnly; 
    
    private String lowFaultActiveValue;
    private String lowAlertActiveValue;
    private String highAlertActiveValue;
    private String highFaultActiveValue;
    
  
    public ApAlSetVO() {
    }

    public ApAlSetVO(ApAlSetVO apAlSetVO) {
        customerAccount = apAlSetVO.getCustomerAccount();
        siteName = apAlSetVO.getSiteName();
        siteId = apAlSetVO.getSiteId();
        apSetName = apAlSetVO.getApSetName();
        apSetId = apAlSetVO.getApSetId();
        alSetName = apAlSetVO.getAlSetName();
        alSetId = apAlSetVO.getAlSetId();
        paramName = apAlSetVO.getParamName();
        sensorType = apAlSetVO.getSensorType();
        fmax = apAlSetVO.getFmax();
        resolution = apAlSetVO.getResolution();
        period = apAlSetVO.getPeriod();
        sampleRate = apAlSetVO.getSampleRate();
        demod = apAlSetVO.isDemod();
        demodHighPass = apAlSetVO.getDemodHighPass();
        demodLowPass = apAlSetVO.getDemodLowPass();
        paramType = apAlSetVO.getParamType();
        frequencyUnits = apAlSetVO.getFrequencyUnits();
        paramUnits = apAlSetVO.getParamUnits();
        paramAmpFactor = apAlSetVO.getParamAmpFactor();
        minFrequency = apAlSetVO.getMinFrequency();
        maxFrequency = apAlSetVO.getMaxFrequency();
        dwell = apAlSetVO.getDwell();
        lowFaultActive = apAlSetVO.isLowFaultActive();
        lowAlertActive = apAlSetVO.isLowAlertActive();
        highAlertActive = apAlSetVO.isHighAlertActive();
        highFaultActive = apAlSetVO.isHighFaultActive();
        lowFault = apAlSetVO.getLowFault();
        lowAlert = apAlSetVO.getLowAlert();
        highAlert = apAlSetVO.getHighAlert();
        highFault = apAlSetVO.getHighFault();
        selectedAlSet = apAlSetVO.getSelectedAlSet();
        active = apAlSetVO.isActive();
        lowFaultFailure = apAlSetVO.isLowFaultFailure();
        lowAlertFailure = apAlSetVO.isLowAlertFailure();
        highAlertFailure = apAlSetVO.isHighAlertFailure();
        highFaultFailure = apAlSetVO.isHighFaultFailure();
        customized = apAlSetVO.isCustomized();
        readOnly = apAlSetVO.isReadOnly();
         lowFaultActiveValue=apAlSetVO.getLowFaultActiveValue();
         lowAlertActiveValue=apAlSetVO.getLowAlertActiveValue();
         highAlertActiveValue=apAlSetVO.getHighAlertActiveValue();
	 highFaultActiveValue=apAlSetVO.getHighFaultActiveValue();
        
    }

    public ApAlSetVO(PointVO pointVO) {
        customerAccount = pointVO.getCustomerAccount();
        siteName = pointVO.getSiteName();
        siteId = pointVO.getSiteId();
        apSetName = pointVO.getApSetName();
        apSetId = pointVO.getApSetId();
        alSetName = pointVO.getAlSetName();
        alSetId = pointVO.getAlSetId();
        paramName = pointVO.getParamName();
        sensorType = pointVO.getSensorType();
        fmax = pointVO.getfMax();
        resolution = pointVO.getResolution();
        period = pointVO.getPeriod();
        sampleRate = pointVO.getSampleRate();
        demod = pointVO.isDemodEnabled();
        demodHighPass = pointVO.getDemodHighPass();
        demodLowPass = pointVO.getDemodLowPass();
        paramType = pointVO.getParamType();
        frequencyUnits = pointVO.getFreqUnits();
        paramUnits = pointVO.getParamUnits();
        paramAmpFactor = pointVO.getParamAmpFactor();
        minFrequency = pointVO.getMinFreq();
        maxFrequency = pointVO.getMaxFreq();
        dwell = pointVO.getDwell();
        lowFaultActive = pointVO.isLowFaultActive();
        lowAlertActive = pointVO.isLowAlertActive();
        highAlertActive = pointVO.isHighAlertActive();
        highFaultActive = pointVO.isHighFaultActive();
        lowFault = pointVO.getLowFault();
        lowAlert = pointVO.getLowAlert();
        highAlert = pointVO.getHighAlert();
        highFault = pointVO.getHighFault();
        active = pointVO.isDisabled();
        
    }

    public ApAlSetVO(String customerAccount, String siteName, UUID siteId, String apSetName, UUID apSetId, String alSetName, UUID alSetId, String paramName, String sensorType, boolean active, int fmax, int resolution, float period, float sampleRate, boolean demod, float demodHighPass, float demodLowPass, String paramType, String frequencyUnits, String paramUnits, String paramAmpFactor, float minFrequency, float maxFrequency, int dwell, boolean lowFaultActive, boolean lowAlertActive, boolean highAlertActive, boolean highFaultActive, float lowFault, float lowAlert, float highAlert, float highFault, SelectItem selectedAlSet, boolean lowFaultFailure, boolean lowAlertFailure, boolean highAlertFailure, boolean highFaultFailure, boolean customized, boolean readOnly,String lowFaultActiveValue, String lowAlertActiveValue, String highAlertActiveValue, String	 highFaultActiveValue) {
        this.customerAccount = customerAccount;
        this.siteName = siteName;
        this.siteId = siteId;
        this.apSetName = apSetName;
        this.apSetId = apSetId;
        this.alSetName = alSetName;
        this.alSetId = alSetId;
        this.paramName = paramName;
        this.sensorType = sensorType;
        this.active = active;
        this.fmax = fmax;
        this.resolution = resolution;
        this.period = period;
        this.sampleRate = sampleRate;
        this.demod = demod;
        this.demodHighPass = demodHighPass;
        this.demodLowPass = demodLowPass;
        this.paramType = paramType;
        this.frequencyUnits = frequencyUnits;
        this.paramUnits = paramUnits;
        this.paramAmpFactor = paramAmpFactor;
        this.minFrequency = minFrequency;
        this.maxFrequency = maxFrequency;
        this.dwell = dwell;
        this.lowFaultActive = lowFaultActive;
        this.lowAlertActive = lowAlertActive;
        this.highAlertActive = highAlertActive;
        this.highFaultActive = highFaultActive;
        this.lowFault = lowFault;
        this.lowAlert = lowAlert;
        this.highAlert = highAlert;
        this.highFault = highFault;
        this.selectedAlSet = selectedAlSet;
        this.lowFaultFailure = lowFaultFailure;
        this.lowAlertFailure = lowAlertFailure;
        this.highAlertFailure = highAlertFailure;
        this.highFaultFailure = highFaultFailure;
        this.customized = customized;
        this.readOnly = readOnly;
        this.lowFaultActiveValue = lowFaultActiveValue;
        this.lowAlertActiveValue = lowAlertActiveValue;
        this.highAlertActiveValue = highAlertActiveValue;
        this.highFaultActiveValue = highFaultActiveValue;
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public String getApSetName() {
        return apSetName;
    }

    public void setApSetName(String apSetName) {
        this.apSetName = apSetName;
    }

    public UUID getApSetId() {
        return apSetId;
    }

    public void setApSetId(UUID apSetId) {
        this.apSetId = apSetId;
    }

    public String getAlSetName() {
        return alSetName;
    }

    public void setAlSetName(String alSetName) {
        this.alSetName = alSetName;
    }

    public UUID getAlSetId() {
        return alSetId;
    }

    public void setAlSetId(UUID alSetId) {
        this.alSetId = alSetId;
    }

    public String getParamName() {
        return paramName;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    public String getSensorType() {
        return sensorType;
    }

    public void setSensorType(String sensorType) {
        this.sensorType = sensorType;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public int getFmax() {
        return fmax;
    }

    public void setFmax(int fmax) {
        this.fmax = fmax;
    }

    public int getResolution() {
        return resolution;
    }

    public void setResolution(int resolution) {
        this.resolution = resolution;
    }

    public float getPeriod() {
        return period;
    }

    public void setPeriod(float period) {
        this.period = period;
    }

    public float getSampleRate() {
        return sampleRate;
    }

    public void setSampleRate(float sampleRate) {
        this.sampleRate = sampleRate;
    }

    public boolean isDemod() {
        return demod;
    }

    public void setDemod(boolean demod) {
        this.demod = demod;
    }

    public float getDemodHighPass() {
        return demodHighPass;
    }

    public void setDemodHighPass(float demodHighPass) {
        this.demodHighPass = demodHighPass;
    }

    public float getDemodLowPass() {
        return demodLowPass;
    }

    public void setDemodLowPass(float demodLowPass) {
        this.demodLowPass = demodLowPass;
    }

    public String getParamType() {
        return paramType;
    }

    public void setParamType(String paramType) {
        this.paramType = paramType;
    }

    public String getFrequencyUnits() {
        return frequencyUnits;
    }

    public void setFrequencyUnits(String frequencyUnits) {
        this.frequencyUnits = frequencyUnits;
    }

    public String getParamUnits() {
        return paramUnits;
    }

    public void setParamUnits(String paramUnits) {
        this.paramUnits = paramUnits;
    }

    public String getParamAmpFactor() {
        return paramAmpFactor;
    }

    public void setParamAmpFactor(String paramAmpFactor) {
        this.paramAmpFactor = paramAmpFactor;
    }

    public float getMinFrequency() {
        return minFrequency;
    }

    public void setMinFrequency(float minFrequency) {
        this.minFrequency = minFrequency;
    }

    public float getMaxFrequency() {
        return maxFrequency;
    }

    public void setMaxFrequency(float maxFrequency) {
        this.maxFrequency = maxFrequency;
    }

    public int getDwell() {
        return dwell;
    }

    public void setDwell(int dwell) {
        this.dwell = dwell;
    }

    public boolean isLowFaultActive() {
        return lowFaultActive;
    }

    public void setLowFaultActive(boolean lowFaultActive) {
        this.lowFaultActive = lowFaultActive;
    }

    public boolean isLowAlertActive() {
        return lowAlertActive;
    }

    public void setLowAlertActive(boolean lowAlertActive) {
        this.lowAlertActive = lowAlertActive;
    }

    public boolean isHighAlertActive() {
        return highAlertActive;
    }

    public void setHighAlertActive(boolean highAlertActive) {
        this.highAlertActive = highAlertActive;
    }

    public boolean isHighFaultActive() {
        return highFaultActive;
    }

    public void setHighFaultActive(boolean highFaultActive) {
        this.highFaultActive = highFaultActive;
    }

    public float getLowFault() {
        return lowFault;
    }

    public void setLowFault(float lowFault) {
        this.lowFault = lowFault;
    }

    public float getLowAlert() {
        return lowAlert;
    }

    public void setLowAlert(float lowAlert) {
        this.lowAlert = lowAlert;
    }

    public float getHighAlert() {
        return highAlert;
    }

    public void setHighAlert(float highAlert) {
        this.highAlert = highAlert;
    }

    public float getHighFault() {
        return highFault;
    }

    public void setHighFault(float highFault) {
        this.highFault = highFault;
    }

    public SelectItem getSelectedAlSet() {
        return selectedAlSet;
    }

    public void setSelectedAlSet(SelectItem selectedAlSet) {
        this.selectedAlSet = selectedAlSet;
    }

    public boolean isLowFaultFailure() {
        return lowFaultFailure;
    }

    public void setLowFaultFailure(boolean lowFaultFailure) {
        this.lowFaultFailure = lowFaultFailure;
    }

    public boolean isLowAlertFailure() {
        return lowAlertFailure;
    }

    public void setLowAlertFailure(boolean lowAlertFailure) {
        this.lowAlertFailure = lowAlertFailure;
    }

    public boolean isHighAlertFailure() {
        return highAlertFailure;
    }

    public void setHighAlertFailure(boolean highAlertFailure) {
        this.highAlertFailure = highAlertFailure;
    }

    public boolean isHighFaultFailure() {
        return highFaultFailure;
    }

    public void setHighFaultFailure(boolean highFaultFailure) {
        this.highFaultFailure = highFaultFailure;
    }

    public boolean isCustomized() {
        return customized;
    }

    public void setCustomized(boolean customized) {
        this.customized = customized;
    }

    public boolean isReadOnly() {
        return readOnly;
    }

    public void setReadOnly(boolean readOnly) {
        this.readOnly = readOnly;
    }

    public String getLowFaultActiveValue() {
        return lowFaultActiveValue;
    }

    public void setLowFaultActiveValue(String lowFaultActiveValue) {
        this.lowFaultActiveValue = lowFaultActiveValue;
    }

    public String getLowAlertActiveValue() {
        return lowAlertActiveValue;
    }

    public void setLowAlertActiveValue(String lowAlertActiveValue) {
        this.lowAlertActiveValue = lowAlertActiveValue;
    }

    public String getHighAlertActiveValue() {
        return highAlertActiveValue;
    }

    public void setHighAlertActiveValue(String highAlertActiveValue) {
        this.highAlertActiveValue = highAlertActiveValue;
    }

    public String getHighFaultActiveValue() {
        return highFaultActiveValue;
    }

    public void setHighFaultActiveValue(String highFaultActiveValue) {
        this.highFaultActiveValue = highFaultActiveValue;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 23 * hash + Objects.hashCode(this.customerAccount);
        hash = 23 * hash + Objects.hashCode(this.siteName);
        hash = 23 * hash + Objects.hashCode(this.siteId);
        hash = 23 * hash + Objects.hashCode(this.apSetName);
        hash = 23 * hash + Objects.hashCode(this.apSetId);
        hash = 23 * hash + Objects.hashCode(this.alSetName);
        hash = 23 * hash + Objects.hashCode(this.alSetId);
        hash = 23 * hash + Objects.hashCode(this.paramName);
        hash = 23 * hash + Objects.hashCode(this.sensorType);
        hash = 23 * hash + (this.active ? 1 : 0);
        hash = 23 * hash + this.fmax;
        hash = 23 * hash + this.resolution;
        hash = 23 * hash + Float.floatToIntBits(this.period);
        hash = 23 * hash + Float.floatToIntBits(this.sampleRate);
        hash = 23 * hash + (this.demod ? 1 : 0);
        hash = 23 * hash + Float.floatToIntBits(this.demodHighPass);
        hash = 23 * hash + Float.floatToIntBits(this.demodLowPass);
        hash = 23 * hash + Objects.hashCode(this.paramType);
        hash = 23 * hash + Objects.hashCode(this.frequencyUnits);
        hash = 23 * hash + Objects.hashCode(this.paramUnits);
        hash = 23 * hash + Objects.hashCode(this.paramAmpFactor);
        hash = 23 * hash + Float.floatToIntBits(this.minFrequency);
        hash = 23 * hash + Float.floatToIntBits(this.maxFrequency);
        hash = 23 * hash + this.dwell;
        hash = 23 * hash + (this.lowFaultActive ? 1 : 0);
        hash = 23 * hash + (this.lowAlertActive ? 1 : 0);
        hash = 23 * hash + (this.highAlertActive ? 1 : 0);
        hash = 23 * hash + (this.highFaultActive ? 1 : 0);
        hash = 23 * hash + Float.floatToIntBits(this.lowFault);
        hash = 23 * hash + Float.floatToIntBits(this.lowAlert);
        hash = 23 * hash + Float.floatToIntBits(this.highAlert);
        hash = 23 * hash + Float.floatToIntBits(this.highFault);
        hash = 23 * hash + Objects.hashCode(this.selectedAlSet);
        hash = 23 * hash + (this.lowFaultFailure ? 1 : 0);
        hash = 23 * hash + (this.lowAlertFailure ? 1 : 0);
        hash = 23 * hash + (this.highAlertFailure ? 1 : 0);
        hash = 23 * hash + (this.highFaultFailure ? 1 : 0);
        hash = 23 * hash + (this.customized ? 1 : 0);
        hash = 23 * hash + (this.readOnly ? 1 : 0);
        hash = 23 * hash + Objects.hashCode(this.lowFaultActiveValue);
        hash = 23 * hash + Objects.hashCode(this.lowAlertActiveValue);
        hash = 23 * hash + Objects.hashCode(this.highAlertActiveValue);
        hash = 23 * hash + Objects.hashCode(this.highFaultActiveValue);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ApAlSetVO other = (ApAlSetVO) obj;
        if (this.active != other.active) {
            return false;
        }
        if (this.fmax != other.fmax) {
            return false;
        }
        if (this.resolution != other.resolution) {
            return false;
        }
        if (Float.floatToIntBits(this.period) != Float.floatToIntBits(other.period)) {
            return false;
        }
        if (Float.floatToIntBits(this.sampleRate) != Float.floatToIntBits(other.sampleRate)) {
            return false;
        }
        if (this.demod != other.demod) {
            return false;
        }
        if (Float.floatToIntBits(this.demodHighPass) != Float.floatToIntBits(other.demodHighPass)) {
            return false;
        }
        if (Float.floatToIntBits(this.demodLowPass) != Float.floatToIntBits(other.demodLowPass)) {
            return false;
        }
        if (Float.floatToIntBits(this.minFrequency) != Float.floatToIntBits(other.minFrequency)) {
            return false;
        }
        if (Float.floatToIntBits(this.maxFrequency) != Float.floatToIntBits(other.maxFrequency)) {
            return false;
        }
        if (this.dwell != other.dwell) {
            return false;
        }
        if (this.lowFaultActive != other.lowFaultActive) {
            return false;
        }
        if (this.lowAlertActive != other.lowAlertActive) {
            return false;
        }
        if (this.highAlertActive != other.highAlertActive) {
            return false;
        }
        if (this.highFaultActive != other.highFaultActive) {
            return false;
        }
        if (Float.floatToIntBits(this.lowFault) != Float.floatToIntBits(other.lowFault)) {
            return false;
        }
        if (Float.floatToIntBits(this.lowAlert) != Float.floatToIntBits(other.lowAlert)) {
            return false;
        }
        if (Float.floatToIntBits(this.highAlert) != Float.floatToIntBits(other.highAlert)) {
            return false;
        }
        if (Float.floatToIntBits(this.highFault) != Float.floatToIntBits(other.highFault)) {
            return false;
        }
        if (this.lowFaultFailure != other.lowFaultFailure) {
            return false;
        }
        if (this.lowAlertFailure != other.lowAlertFailure) {
            return false;
        }
        if (this.highAlertFailure != other.highAlertFailure) {
            return false;
        }
        if (this.highFaultFailure != other.highFaultFailure) {
            return false;
        }
        if (this.customized != other.customized) {
            return false;
        }
        if (this.readOnly != other.readOnly) {
            return false;
        }
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.siteName, other.siteName)) {
            return false;
        }
        if (!Objects.equals(this.apSetName, other.apSetName)) {
            return false;
        }
        if (!Objects.equals(this.alSetName, other.alSetName)) {
            return false;
        }
        if (!Objects.equals(this.paramName, other.paramName)) {
            return false;
        }
        if (!Objects.equals(this.sensorType, other.sensorType)) {
            return false;
        }
        if (!Objects.equals(this.paramType, other.paramType)) {
            return false;
        }
        if (!Objects.equals(this.frequencyUnits, other.frequencyUnits)) {
            return false;
        }
        if (!Objects.equals(this.paramUnits, other.paramUnits)) {
            return false;
        }
        if (!Objects.equals(this.paramAmpFactor, other.paramAmpFactor)) {
            return false;
        }
        if (!Objects.equals(this.lowFaultActiveValue, other.lowFaultActiveValue)) {
            return false;
        }
        if (!Objects.equals(this.lowAlertActiveValue, other.lowAlertActiveValue)) {
            return false;
        }
        if (!Objects.equals(this.highAlertActiveValue, other.highAlertActiveValue)) {
            return false;
        }
        if (!Objects.equals(this.highFaultActiveValue, other.highFaultActiveValue)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        if (!Objects.equals(this.apSetId, other.apSetId)) {
            return false;
        }
        if (!Objects.equals(this.alSetId, other.alSetId)) {
            return false;
        }
        return Objects.equals(this.selectedAlSet, other.selectedAlSet);
    }

    @Override
    public String toString() {
        return "ApAlSetVO{" + "customerAccount=" + customerAccount + ", siteName=" + siteName + ", siteId=" + siteId + ", apSetName=" + apSetName + ", apSetId=" + apSetId + ", alSetName=" + alSetName + ", alSetId=" + alSetId + ", paramName=" + paramName + ", sensorType=" + sensorType + ", active=" + active + ", fmax=" + fmax + ", resolution=" + resolution + ", period=" + period + ", sampleRate=" + sampleRate + ", demod=" + demod + ", demodHighPass=" + demodHighPass + ", demodLowPass=" + demodLowPass + ", paramType=" + paramType + ", frequencyUnits=" + frequencyUnits + ", paramUnits=" + paramUnits + ", paramAmpFactor=" + paramAmpFactor + ", minFrequency=" + minFrequency + ", maxFrequency=" + maxFrequency + ", dwell=" + dwell + ", lowFaultActive=" + lowFaultActive + ", lowAlertActive=" + lowAlertActive + ", highAlertActive=" + highAlertActive + ", highFaultActive=" + highFaultActive + ", lowFault=" + lowFault + ", lowAlert=" + lowAlert + ", highAlert=" + highAlert + ", highFault=" + highFault + ", selectedAlSet=" + selectedAlSet + ", lowFaultFailure=" + lowFaultFailure + ", lowAlertFailure=" + lowAlertFailure + ", highAlertFailure=" + highAlertFailure + ", highFaultFailure=" + highFaultFailure + ", customized=" + customized + ", readOnly=" + readOnly + ", lowFaultActiveValue=" + lowFaultActiveValue + ", lowAlertActiveValue=" + lowAlertActiveValue + ", highAlertActiveValue=" + highAlertActiveValue + ", highFaultActiveValue=" + highFaultActiveValue + '}';
    }
    
    

}
