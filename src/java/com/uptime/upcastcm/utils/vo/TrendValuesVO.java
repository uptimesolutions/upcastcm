/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.utils.vo;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author gsingh
 */
public class TrendValuesVO implements Serializable{
    
    private String customerAccount;
    private UUID siteId;
    private UUID areaId;
    private UUID assetId;
    private UUID pointLocationId;
    private UUID pointId;
    private UUID apSetId;
    private UUID alSetId;
    private String paramName;
    private Instant sampleTime;
    private String label;
    private String notes;
    private float highAlert;
    private float highFault;
    private float lowAlert;
    private float lowFault;
    private String sensorType;
    private String units;
    private float value;

    public TrendValuesVO() {
    }
    
    public TrendValuesVO(TrendValuesVO entity) {
        this.alSetId = entity.getAlSetId();
        this.apSetId = entity.getApSetId();
        this.areaId = entity.getAreaId();
        this.assetId = entity.getAssetId();
        this.customerAccount = entity.getCustomerAccount();
        this.highAlert = entity.getHighAlert();
        this.highFault = entity.getLowFault();
        this.lowAlert = entity.getLowAlert();
        this.lowFault = entity.getLowFault();
        this.notes = entity.getNotes();
        this.label = entity.getLabel();
        this.pointId = entity.getPointId();
        this.paramName = entity.getParamName();
        this.pointLocationId = entity.getPointLocationId();
        this.sampleTime = entity.getSampleTime();
        this.sensorType = entity.getSensorType();
        this.siteId = entity.getSiteId();
        this.value = entity.getValue();
        this.units = entity.getUnits();

    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public UUID getAreaId() {
        return areaId;
    }

    public void setAreaId(UUID areaId) {
        this.areaId = areaId;
    }

    public UUID getAssetId() {
        return assetId;
    }

    public void setAssetId(UUID assetId) {
        this.assetId = assetId;
    }

    public UUID getPointLocationId() {
        return pointLocationId;
    }

    public void setPointLocationId(UUID pointLocationId) {
        this.pointLocationId = pointLocationId;
    }

    public UUID getPointId() {
        return pointId;
    }

    public void setPointId(UUID pointId) {
        this.pointId = pointId;
    }

    public UUID getApSetId() {
        return apSetId;
    }

    public void setApSetId(UUID apSetId) {
        this.apSetId = apSetId;
    }

    public String getParamName() {
        return paramName;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    public Instant getSampleTime() {
        return sampleTime;
    }

    public void setSampleTime(Instant sampleTime) {
        this.sampleTime = sampleTime;
    }

    public UUID getAlSetId() {
        return alSetId;
    }

    public void setAlSetId(UUID alSetId) {
        this.alSetId = alSetId;
    }
    
    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public float getHighAlert() {
        return highAlert;
    }

    public void setHighAlert(float highAlert) {
        this.highAlert = highAlert;
    }

    public float getHighFault() {
        return highFault;
    }

    public void setHighFault(float highFault) {
        this.highFault = highFault;
    }

    public float getLowAlert() {
        return lowAlert;
    }

    public void setLowAlert(float lowAlert) {
        this.lowAlert = lowAlert;
    }

    public float getLowFault() {
        return lowFault;
    }

    public void setLowFault(float lowFault) {
        this.lowFault = lowFault;
    }

    public String getSensorType() {
        return sensorType;
    }

    public void setSensorType(String sensorType) {
        this.sensorType = sensorType;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + Objects.hashCode(this.customerAccount);
        hash = 89 * hash + Objects.hashCode(this.siteId);
        hash = 89 * hash + Objects.hashCode(this.areaId);
        hash = 89 * hash + Objects.hashCode(this.assetId);
        hash = 89 * hash + Objects.hashCode(this.pointLocationId);
        hash = 89 * hash + Objects.hashCode(this.pointId);
        hash = 89 * hash + Objects.hashCode(this.apSetId);
        hash = 89 * hash + Objects.hashCode(this.paramName);
        hash = 89 * hash + Objects.hashCode(this.sampleTime);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TrendValuesVO other = (TrendValuesVO) obj;
        if (Float.floatToIntBits(this.highAlert) != Float.floatToIntBits(other.highAlert)) {
            return false;
        }
        if (Float.floatToIntBits(this.highFault) != Float.floatToIntBits(other.highFault)) {
            return false;
        }
        if (Float.floatToIntBits(this.lowAlert) != Float.floatToIntBits(other.lowAlert)) {
            return false;
        }
        if (Float.floatToIntBits(this.lowFault) != Float.floatToIntBits(other.lowFault)) {
            return false;
        }
        if (Float.floatToIntBits(this.value) != Float.floatToIntBits(other.value)) {
            return false;
        }
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.label, other.label)) {
            return false;
        }
        if (!Objects.equals(this.notes, other.notes)) {
            return false;
        }
        if (!Objects.equals(this.sensorType, other.sensorType)) {
            return false;
        }
        if (!Objects.equals(this.units, other.units)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        if (!Objects.equals(this.areaId, other.areaId)) {
            return false;
        }
        if (!Objects.equals(this.assetId, other.assetId)) {
            return false;
        }
        if (!Objects.equals(this.pointLocationId, other.pointLocationId)) {
            return false;
        }
        if (!Objects.equals(this.pointId, other.pointId)) {
            return false;
        }
        if (!Objects.equals(this.apSetId, other.apSetId)) {
            return false;
        }
        if (!Objects.equals(this.paramName, other.paramName)) {
            return false;
        }
        if (!Objects.equals(this.sampleTime, other.sampleTime)) {
            return false;
        }
        if (!Objects.equals(this.alSetId, other.alSetId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "TrendValues{" + "customerAccount=" + customerAccount + ", siteId=" + siteId + ", areaId=" + areaId + ", assetId=" + assetId + ", pointLocationId=" + pointLocationId + ", pointId=" + pointId + ", apSetId=" + apSetId + ", paramName=" + paramName + ", sampleTime=" + sampleTime + ", alSetId=" + alSetId + ", label=" + label + ", notes=" + notes + ", highAlert=" + highAlert + ", highFault=" + highFault + ", lowAlert=" + lowAlert + ", lowFault=" + lowFault + ", sensorType=" + sensorType + ", units=" + units + ", value=" + value + '}';
    }

}
