/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.utils.vo;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author twilcox
 */
public class ApALIdVO implements Serializable {
    private UUID apSetId;
    private UUID alSetId;

    public ApALIdVO() {
    }

    public ApALIdVO(UUID apSetId, UUID alSetId) {
        this.apSetId = apSetId;
        this.alSetId = alSetId;
    }

    public ApALIdVO(ApALIdVO apALIdVO) {
        this.apSetId = apALIdVO.getApSetId();
        this.alSetId = apALIdVO.getAlSetId();
    }

    public UUID getApSetId() {
        return apSetId;
    }

    public void setApSetId(UUID apSetId) {
        this.apSetId = apSetId;
    }

    public UUID getAlSetId() {
        return alSetId;
    }

    public void setAlSetId(UUID alSetId) {
        this.alSetId = alSetId;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 29 * hash + Objects.hashCode(this.apSetId);
        hash = 29 * hash + Objects.hashCode(this.alSetId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ApALIdVO other = (ApALIdVO) obj;
        if (!Objects.equals(this.apSetId, other.apSetId)) {
            return false;
        }
        if (!Objects.equals(this.alSetId, other.alSetId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ApALIdVO{" + "apSetId=" + apSetId + ", alSetId=" + alSetId + '}';
    }
}
