/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.utils.vo;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author madhavi
 */
public class AssetVO implements Serializable {
    String customerAccount;
    UUID siteId;
    String siteName;
    UUID areaId;
    String areaName;
    UUID assetId;
    String assetName;
    UUID assetTypeId;
    String assetType;
    String description;
    int sampleInterval;
    List<PointLocationVO> pointLocationList;
    boolean alarmEnabled;
    String alarmEnabledValue;
    String sensorTypes;

    public AssetVO() {
    }

    public AssetVO(String customerAccount, UUID siteId, String siteName, UUID areaId, String areaName, UUID assetId, String assetName, UUID assetTypeId, String assetType, String description, int sampleInterval, List<PointLocationVO> pointLocationList, boolean alarmEnabled,String alarmEnabledValue) {
        this.customerAccount = customerAccount;
        this.siteId = siteId;
        this.siteName = siteName;
        this.areaId = areaId;
        this.areaName = areaName;
        this.assetId = assetId;
        this.assetName = assetName;
        this.assetTypeId = assetTypeId;
        this.assetType = assetType;
        this.description = description;
        this.sampleInterval = sampleInterval;
        this.pointLocationList = pointLocationList;
        this.alarmEnabled = alarmEnabled;
        this.alarmEnabledValue=alarmEnabledValue;
    }

    public AssetVO(AssetVO assetVO) {
        customerAccount = assetVO.getCustomerAccount();
        siteId = assetVO.getSiteId();
        siteName = assetVO.getSiteName();
        areaId = assetVO.getAreaId();
        areaName = assetVO.getAreaName();
        assetId = assetVO.getAssetId();
        assetName = assetVO.getAssetName();
        assetTypeId = assetVO.getAssetTypeId();
        assetType = assetVO.getAssetType();
        description = assetVO.getDescription();
        sampleInterval = assetVO.getSampleInterval();
        pointLocationList = assetVO.getPointLocationList();
        alarmEnabled = assetVO.isAlarmEnabled();
        alarmEnabledValue=assetVO.getAlarmEnabledValue();
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public UUID getAreaId() {
        return areaId;
    }

    public void setAreaId(UUID areaId) {
        this.areaId = areaId;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public UUID getAssetId() {
        return assetId;
    }

    public void setAssetId(UUID assetId) {
        this.assetId = assetId;
    }

    public String getAssetName() {
        return assetName;
    }

    public void setAssetName(String assetName) {
        this.assetName = assetName;
    }

    public UUID getAssetTypeId() {
        return assetTypeId;
    }

    public void setAssetTypeId(UUID assetTypeId) {
        this.assetTypeId = assetTypeId;
    }

    public String getAssetType() {
        return assetType;
    }

    public void setAssetType(String assetType) {
        this.assetType = assetType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getSampleInterval() {
        return sampleInterval;
    }

    public void setSampleInterval(int sampleInterval) {
        this.sampleInterval = sampleInterval;
    }

    public List<PointLocationVO> getPointLocationList() {
        return pointLocationList;
    }

    public void setPointLocationList(List<PointLocationVO> pointLocationList) {
        this.pointLocationList = pointLocationList;
    }

    public boolean isAlarmEnabled() {
        return alarmEnabled;
    }

    public void setAlarmEnabled(boolean alarmEnabled) {
        this.alarmEnabled = alarmEnabled;
    }

    public String getAlarmEnabledValue() {
        return alarmEnabledValue;
    }

    public void setAlarmEnabledValue(String alarmEnabledValue) {
        this.alarmEnabledValue = alarmEnabledValue;
    }

    public String getSensorTypes() {
        return sensorTypes;
    }

    public void setSensorTypes(String sensorTypes) {
        this.sensorTypes = sensorTypes;
    }
    
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 43 * hash + Objects.hashCode(this.customerAccount);
        hash = 43 * hash + Objects.hashCode(this.siteId);
        hash = 43 * hash + Objects.hashCode(this.siteName);
        hash = 43 * hash + Objects.hashCode(this.areaId);
        hash = 43 * hash + Objects.hashCode(this.areaName);
        hash = 43 * hash + Objects.hashCode(this.assetId);
        hash = 43 * hash + Objects.hashCode(this.assetName);
        hash = 43 * hash + Objects.hashCode(this.assetTypeId);
        hash = 43 * hash + Objects.hashCode(this.assetType);
        hash = 43 * hash + Objects.hashCode(this.description);
        hash = 43 * hash + this.sampleInterval;
        hash = 43 * hash + Objects.hashCode(this.pointLocationList);
        hash = 43 * hash + (this.alarmEnabled ? 1 : 0);
        hash = 43 * hash + Objects.hashCode(this.alarmEnabledValue);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AssetVO other = (AssetVO) obj;
        if (this.sampleInterval != other.sampleInterval) {
            return false;
        }
        if (this.alarmEnabled != other.alarmEnabled) {
            return false;
        }
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.siteName, other.siteName)) {
            return false;
        }
        if (!Objects.equals(this.areaName, other.areaName)) {
            return false;
        }
        if (!Objects.equals(this.assetName, other.assetName)) {
            return false;
        }
        if (!Objects.equals(this.assetType, other.assetType)) {
            return false;
        }
        if (!Objects.equals(this.description, other.description)) {
            return false;
        }
        if (!Objects.equals(this.alarmEnabledValue, other.alarmEnabledValue)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        if (!Objects.equals(this.areaId, other.areaId)) {
            return false;
        }
        if (!Objects.equals(this.assetId, other.assetId)) {
            return false;
        }
        if (!Objects.equals(this.assetTypeId, other.assetTypeId)) {
            return false;
        }
        return Objects.equals(this.pointLocationList, other.pointLocationList);
    }

    @Override
    public String toString() {
        return "AssetVO{" + "customerAccount=" + customerAccount + ", siteId=" + siteId + ", siteName=" + siteName + ", areaId=" + areaId + ", areaName=" + areaName + ", assetId=" + assetId + ", assetName=" + assetName + ", assetTypeId=" + assetTypeId + ", assetType=" + assetType + ", description=" + description + ", sampleInterval=" + sampleInterval + ", pointLocationList=" + pointLocationList + ", alarmEnabled=" + alarmEnabled + ", alarmEnabledValue=" + alarmEnabledValue + '}';
    }
   
    
    

   
}
