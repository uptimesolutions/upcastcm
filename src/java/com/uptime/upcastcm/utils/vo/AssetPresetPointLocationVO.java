/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.upcastcm.utils.vo;

import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

/**
 *
 * @author joseph
 */
public class AssetPresetPointLocationVO {

    private String customerAccount;
    private UUID siteId;
    private String pointLocationName;
    private String tachName;
    private UUID tachId;
    private float speedRatio;
    private float rollDiameter;
    private String rollDiameterUnits;
    private String ffSetNames;
    private Set<String> ffSetName;
    private String deviceType;
    private UUID deviceId;
    private String deviceName;
    private String tachReferenceUnits;
    private List<DevicePresetVO> distinctDevicePresetList;
    private String ffSetIds;
    private String rollDiameterText;
    private String speedRatioText;

    public AssetPresetPointLocationVO() {
    }

    public AssetPresetPointLocationVO(AssetPresetPointLocationVO assetPresetPointLocationVO) {
        this.customerAccount = assetPresetPointLocationVO.getCustomerAccount();
        this.siteId = assetPresetPointLocationVO.getSiteId();
        this.pointLocationName = assetPresetPointLocationVO.getPointLocationName();
        this.tachName = assetPresetPointLocationVO.getTachName();
        this.tachId = assetPresetPointLocationVO.getTachId();
        this.speedRatio = assetPresetPointLocationVO.getSpeedRatio();
        this.rollDiameter = assetPresetPointLocationVO.getRollDiameter();
        this.rollDiameterUnits = assetPresetPointLocationVO.getRollDiameterUnits();
        this.ffSetNames = assetPresetPointLocationVO.getFfSetNames();
        this.ffSetName = assetPresetPointLocationVO.getFfSetName();
        this.deviceType = assetPresetPointLocationVO.getDeviceType();
        this.deviceId = assetPresetPointLocationVO.getDeviceId();
        this.deviceName = assetPresetPointLocationVO.getDeviceName();
        this.tachReferenceUnits = assetPresetPointLocationVO.getTachReferenceUnits();
        this.distinctDevicePresetList = assetPresetPointLocationVO.getDistinctDevicePresetList();
        this.ffSetIds = assetPresetPointLocationVO.getFfSetIds();
        this.rollDiameterText = assetPresetPointLocationVO.getRollDiameterText();
        this.speedRatioText = assetPresetPointLocationVO.getSpeedRatioText();
    }

    public AssetPresetPointLocationVO(String customerAccount, UUID siteId, String pointLocationName, String tachName, UUID tachId, float speedRatio, float rollDiameter, String rollDiameterUnits, String ffSetNames, Set<String> ffSetName, String deviceType, UUID deviceId, String deviceName, String tachReferenceUnits, List<DevicePresetVO> distinctDevicePresetList, String ffSetIds) {
        this.customerAccount = customerAccount;
        this.siteId = siteId;
        this.pointLocationName = pointLocationName;
        this.tachName = tachName;
        this.tachId = tachId;
        this.speedRatio = speedRatio;
        this.rollDiameter = rollDiameter;
        this.rollDiameterUnits = rollDiameterUnits;
        this.ffSetNames = ffSetNames;
        this.ffSetName = ffSetName;
        this.deviceType = deviceType;
        this.deviceId = deviceId;
        this.deviceName = deviceName;
        this.tachReferenceUnits = tachReferenceUnits;
        this.distinctDevicePresetList = distinctDevicePresetList;
        this.ffSetIds = ffSetIds;
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public String getPointLocationName() {
        return pointLocationName;
    }

    public void setPointLocationName(String pointLocationName) {
        this.pointLocationName = pointLocationName;
    }

    public String getTachName() {
        return tachName;
    }

    public void setTachName(String tachName) {
        this.tachName = tachName;
    }

    public UUID getTachId() {
        return tachId;
    }

    public void setTachId(UUID tachId) {
        this.tachId = tachId;
    }

    public float getSpeedRatio() {
        return speedRatio;
    }

    public void setSpeedRatio(float speedRatio) {
        this.speedRatio = speedRatio;
    }

    public float getRollDiameter() {
        return rollDiameter;
    }

    public void setRollDiameter(float rollDiameter) {
        this.rollDiameter = rollDiameter;
    }

    public String getRollDiameterUnits() {
        return rollDiameterUnits;
    }

    public void setRollDiameterUnits(String rollDiameterUnits) {
        this.rollDiameterUnits = rollDiameterUnits;
    }

    public String getFfSetNames() {
        return ffSetNames;
    }

    public void setFfSetNames(String ffSetNames) {
        this.ffSetNames = ffSetNames;
    }

    public Set<String> getFfSetName() {
        return ffSetName;
    }

    public void setFfSetName(Set<String> ffSetName) {
        this.ffSetName = ffSetName;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public UUID getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(UUID deviceId) {
        this.deviceId = deviceId;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getTachReferenceUnits() {
        return tachReferenceUnits;
    }

    public void setTachReferenceUnits(String tachReferenceUnits) {
        this.tachReferenceUnits = tachReferenceUnits;
    }

    public List<DevicePresetVO> getDistinctDevicePresetList() {
        return distinctDevicePresetList;
    }

    public void setDistinctDevicePresetList(List<DevicePresetVO> distinctDevicePresetList) {
        this.distinctDevicePresetList = distinctDevicePresetList;
    }

    public String getFfSetIds() {
        return ffSetIds;
    }

    public void setFfSetIds(String ffSetIds) {
        this.ffSetIds = ffSetIds;
    }

    public String getRollDiameterText() {
        return rollDiameterText;
    }

    public void setRollDiameterText(String rollDiameterText) {
        this.rollDiameterText = rollDiameterText;
    }

    public String getSpeedRatioText() {
        return speedRatioText;
    }

    public void setSpeedRatioText(String speedRatioText) {
        this.speedRatioText = speedRatioText;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 37 * hash + Objects.hashCode(this.customerAccount);
        hash = 37 * hash + Objects.hashCode(this.siteId);
        hash = 37 * hash + Objects.hashCode(this.pointLocationName);
        hash = 37 * hash + Objects.hashCode(this.tachName);
        hash = 37 * hash + Objects.hashCode(this.tachId);
        hash = 37 * hash + Float.floatToIntBits(this.speedRatio);
        hash = 37 * hash + Float.floatToIntBits(this.rollDiameter);
        hash = 37 * hash + Objects.hashCode(this.rollDiameterUnits);
        hash = 37 * hash + Objects.hashCode(this.ffSetNames);
        hash = 37 * hash + Objects.hashCode(this.ffSetName);
        hash = 37 * hash + Objects.hashCode(this.deviceType);
        hash = 37 * hash + Objects.hashCode(this.deviceId);
        hash = 37 * hash + Objects.hashCode(this.deviceName);
        hash = 37 * hash + Objects.hashCode(this.tachReferenceUnits);
        hash = 37 * hash + Objects.hashCode(this.distinctDevicePresetList);
        hash = 37 * hash + Objects.hashCode(this.ffSetIds);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AssetPresetPointLocationVO other = (AssetPresetPointLocationVO) obj;
        if (Float.floatToIntBits(this.speedRatio) != Float.floatToIntBits(other.speedRatio)) {
            return false;
        }
        if (Float.floatToIntBits(this.rollDiameter) != Float.floatToIntBits(other.rollDiameter)) {
            return false;
        }
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.pointLocationName, other.pointLocationName)) {
            return false;
        }
        if (!Objects.equals(this.tachName, other.tachName)) {
            return false;
        }
        if (!Objects.equals(this.rollDiameterUnits, other.rollDiameterUnits)) {
            return false;
        }
        if (!Objects.equals(this.ffSetNames, other.ffSetNames)) {
            return false;
        }
        if (!Objects.equals(this.deviceType, other.deviceType)) {
            return false;
        }
        if (!Objects.equals(this.deviceName, other.deviceName)) {
            return false;
        }
        if (!Objects.equals(this.tachReferenceUnits, other.tachReferenceUnits)) {
            return false;
        }
        if (!Objects.equals(this.ffSetIds, other.ffSetIds)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        if (!Objects.equals(this.tachId, other.tachId)) {
            return false;
        }
        if (!Objects.equals(this.ffSetName, other.ffSetName)) {
            return false;
        }
        if (!Objects.equals(this.deviceId, other.deviceId)) {
            return false;
        }
        if (!Objects.equals(this.distinctDevicePresetList, other.distinctDevicePresetList)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "AssetPresetPointLocationVO{" + "customerAccount=" + customerAccount + ", siteId=" + siteId + ", pointLocationName=" + pointLocationName + ", tachName=" + tachName + ", tachId=" + tachId + ", speedRatio=" + speedRatio + ", rollDiameter=" + rollDiameter + ", rollDiameterUnits=" + rollDiameterUnits + ", ffSetNames=" + ffSetNames + ", ffSetName=" + ffSetName + ", deviceType=" + deviceType + ", deviceId=" + deviceId + ", deviceName=" + deviceName + ", tachReferenceUnits=" + tachReferenceUnits + ", distinctDevicePresetList=" + distinctDevicePresetList + ", ffSetIds=" + ffSetIds + '}';
    }

}
