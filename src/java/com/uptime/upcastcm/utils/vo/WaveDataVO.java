package com.uptime.upcastcm.utils.vo;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author aswani
 */
public class WaveDataVO implements Serializable {
    private String customerAccount;
    private UUID siteId;
    private UUID areaId;
    private UUID assetId;
    private UUID pointLocationId;
    private UUID pointId;
    private short sampleYear;
    private byte sampleMonth;
    private Instant sampleTime;
    private float sensitivityValue;
    private WaveformVO wave; 

    public WaveDataVO() {
    }

    public WaveDataVO(String customerAccount, UUID siteId, UUID areaId, UUID assetId, UUID pointLocationId, UUID pointId, short sampleYear, byte sampleMonth, Instant sampleTime, float sensitivityValue, WaveformVO wave) {
        this.customerAccount = customerAccount;
        this.siteId = siteId;
        this.areaId = areaId;
        this.assetId = assetId;
        this.pointLocationId = pointLocationId;
        this.pointId = pointId;
        this.sampleYear = sampleYear;
        this.sampleMonth = sampleMonth;
        this.sampleTime = sampleTime;
        this.sensitivityValue = sensitivityValue;
        this.wave = wave != null ? new WaveformVO(wave) : null;
    }

    public WaveDataVO(WaveDataVO waveDataVO) {
        customerAccount = waveDataVO.getCustomerAccount();
        siteId = waveDataVO.getSiteId();
        areaId = waveDataVO.getAreaId();
        assetId = waveDataVO.getAssetId();
        pointLocationId = waveDataVO.getPointLocationId();
        pointId = waveDataVO.getPointId();
        sampleYear = waveDataVO.getSampleYear();
        sampleMonth = waveDataVO.getSampleMonth();
        sampleTime = waveDataVO.getSampleTime();
        sensitivityValue = waveDataVO.getSensitivityValue();
        wave = waveDataVO.getWave() != null ? new WaveformVO(waveDataVO.getWave()) : null;
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public UUID getAreaId() {
        return areaId;
    }

    public void setAreaId(UUID areaId) {
        this.areaId = areaId;
    }

    public UUID getAssetId() {
        return assetId;
    }

    public void setAssetId(UUID assetId) {
        this.assetId = assetId;
    }

    public UUID getPointLocationId() {
        return pointLocationId;
    }

    public void setPointLocationId(UUID pointLocationId) {
        this.pointLocationId = pointLocationId;
    }

    public UUID getPointId() {
        return pointId;
    }

    public void setPointId(UUID pointId) {
        this.pointId = pointId;
    }

    public short getSampleYear() {
        return sampleYear;
    }

    public void setSampleYear(short sampleYear) {
        this.sampleYear = sampleYear;
    }

    public byte getSampleMonth() {
        return sampleMonth;
    }

    public void setSampleMonth(byte sampleMonth) {
        this.sampleMonth = sampleMonth;
    }

    public Instant getSampleTime() {
        return sampleTime;
    }

    public void setSampleTime(Instant sampleTime) {
        this.sampleTime = sampleTime;
    }

    public float getSensitivityValue() {
        return sensitivityValue;
    }

    public void setSensitivityValue(float sensitivityValue) {
        this.sensitivityValue = sensitivityValue;
    }

    public WaveformVO getWave() {
        return wave;
    }

    public void setWave(WaveformVO wave) {
        this.wave = wave;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + Objects.hashCode(this.customerAccount);
        hash = 67 * hash + Objects.hashCode(this.siteId);
        hash = 67 * hash + Objects.hashCode(this.areaId);
        hash = 67 * hash + Objects.hashCode(this.assetId);
        hash = 67 * hash + Objects.hashCode(this.pointLocationId);
        hash = 67 * hash + Objects.hashCode(this.pointId);
        hash = 67 * hash + this.sampleYear;
        hash = 67 * hash + this.sampleMonth;
        hash = 67 * hash + Objects.hashCode(this.sampleTime);
        hash = 67 * hash + Float.floatToIntBits(this.sensitivityValue);
        hash = 67 * hash + Objects.hashCode(this.wave);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final WaveDataVO other = (WaveDataVO) obj;
        if (this.sampleYear != other.sampleYear) {
            return false;
        }
        if (this.sampleMonth != other.sampleMonth) {
            return false;
        }
        if (Float.floatToIntBits(this.sensitivityValue) != Float.floatToIntBits(other.sensitivityValue)) {
            return false;
        }
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        if (!Objects.equals(this.areaId, other.areaId)) {
            return false;
        }
        if (!Objects.equals(this.assetId, other.assetId)) {
            return false;
        }
        if (!Objects.equals(this.pointLocationId, other.pointLocationId)) {
            return false;
        }
        if (!Objects.equals(this.pointId, other.pointId)) {
            return false;
        }
        if (!Objects.equals(this.sampleTime, other.sampleTime)) {
            return false;
        }
        if (!Objects.equals(this.wave, other.wave)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "WaveDataVO{" + "customerAccount=" + customerAccount + ", siteId=" + siteId + ", areaId=" + areaId + ", assetId=" + assetId + ", pointLocationId=" + pointLocationId + ", pointId=" + pointId + ", sampleYear=" + sampleYear + ", sampleMonth=" + sampleMonth + ", sampleTime=" + sampleTime + ", sensitivityValue=" + sensitivityValue + ", wave=" + wave + '}';
    }
}
