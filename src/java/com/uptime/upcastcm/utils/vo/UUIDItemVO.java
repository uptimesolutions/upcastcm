/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.utils.vo;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author twilcox
 */
public class UUIDItemVO implements Serializable {
    private UUID value;
    private String label;
    private String description;

    public UUIDItemVO() {
    }

    public UUIDItemVO(UUID value) {
        this.value = value;
    }

    public UUIDItemVO(UUID value, String label) {
        this.value = value;
        this.label = label;
    }

    public UUIDItemVO(UUID value, String label, String description) {
        this.value = value;
        this.label = label;
        this.description = description;
    }

    public UUIDItemVO(UUIDItemVO uUIDItemVO) {
        value = uUIDItemVO.getValue();
        label = uUIDItemVO.getLabel();
        description = uUIDItemVO.getDescription();
    }

    public UUID getValue() {
        return value;
    }

    public void setValue(UUID value) {
        this.value = value;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + Objects.hashCode(this.value);
        hash = 97 * hash + Objects.hashCode(this.label);
        hash = 97 * hash + Objects.hashCode(this.description);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UUIDItemVO other = (UUIDItemVO) obj;
        if (!Objects.equals(this.label, other.label)) {
            return false;
        }
        if (!Objects.equals(this.description, other.description)) {
            return false;
        }
        if (!Objects.equals(this.value, other.value)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "UUIDItemVO{" + "value=" + value + ", label=" + label + ", description=" + description + '}';
    }
}
