/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.utils.vo;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author madhavi
 */
public class AssetDeviceVO implements Serializable {
    String customerAccount;
    UUID siteId;
    UUID assetId;
    String deviceId;


    public AssetDeviceVO() {
    }

    public AssetDeviceVO(String customerAccount, UUID siteId, UUID assetId, String deviceId) {
        this.customerAccount = customerAccount;
        this.siteId = siteId;
        this.assetId = assetId;
        this.deviceId = deviceId;
    }

    public AssetDeviceVO(AssetDeviceVO assetVO) {
        customerAccount = assetVO.getCustomerAccount();
        siteId = assetVO.getSiteId();
        assetId = assetVO.getAssetId();
        deviceId=assetVO.getDeviceId();
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public UUID getAssetId() {
        return assetId;
    }

    public void setAssetId(UUID assetId) {
        this.assetId = assetId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + Objects.hashCode(this.customerAccount);
        hash = 67 * hash + Objects.hashCode(this.siteId);
        hash = 67 * hash + Objects.hashCode(this.assetId);
        hash = 67 * hash + Objects.hashCode(this.deviceId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AssetDeviceVO other = (AssetDeviceVO) obj;
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        if (!Objects.equals(this.assetId, other.assetId)) {
            return false;
        }
        return Objects.equals(this.deviceId, other.deviceId);
    }

    @Override
    public String toString() {
        return "AssetDeviceVO{" + "customerAccount=" + customerAccount + ", siteId=" + siteId + ", assetId=" + assetId + ", deviceId=" + deviceId + '}';
    }

}
