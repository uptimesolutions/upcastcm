/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.utils.vo;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

/**
 *
 * @author madhavi
 */
public class PointLocationVO implements Serializable {
    private String customerAccount;
    private UUID siteId;
    private String siteName;
    private UUID areaId;
    private String areaName;
    private UUID assetId;
    private String assetName;
    private UUID pointLocationId;
    private String pointLocationName;
    private String description;
    private String deviceSerialNumber;
    private String ffSetNames;
    private String ffSetIds;
    private float speedRatio;
    private short sampleInterval;
    private String tachName;
    private UUID tachId;
    private boolean alarmEnabled;
    private short basestationPortNum;
    private float rollDiameter;
    private String rollDiameterUnits;
    private String tachReferenceUnits;
    private List<PointVO> pointList;
    private Set<String> ffSetName;
    private String sensorTypes;

    public PointLocationVO() {
    }

    public PointLocationVO(String customerAccount, UUID siteId, String siteName, UUID areaId, String areaName, UUID assetId, String assetName, UUID pointLocationId, String pointLocationName, String description, String deviceSerialNumber, String ffSetNames, String ffSetIds, float speedRatio, short sampleInterval, String tachName, UUID tachId, boolean alarmEnabled, short basestationPortNum, float rollDiameter, String rollDiameterUnits, String tachReferenceUnits, List<PointVO> pointList, Set<String> ffSetName, String sensorTypes) {
        this.customerAccount = customerAccount;
        this.siteId = siteId;
        this.siteName = siteName;
        this.areaId = areaId;
        this.areaName = areaName;
        this.assetId = assetId;
        this.assetName = assetName;
        this.pointLocationId = pointLocationId;
        this.pointLocationName = pointLocationName;
        this.description = description;
        this.deviceSerialNumber = deviceSerialNumber;
        this.ffSetNames = ffSetNames;
        this.ffSetIds = ffSetIds;
        this.speedRatio = speedRatio;
        this.sampleInterval = sampleInterval;
        this.tachName = tachName;
        this.tachId = tachId;
        this.alarmEnabled = alarmEnabled;
        this.basestationPortNum = basestationPortNum;
        this.rollDiameter = rollDiameter;
        this.rollDiameterUnits = rollDiameterUnits;
        this.tachReferenceUnits = tachReferenceUnits;
        this.pointList = pointList;
        this.ffSetName = ffSetName;
        this.sensorTypes = sensorTypes;
    }

    public PointLocationVO(PointLocationVO pointLocationVO) {
        customerAccount = pointLocationVO.getCustomerAccount();
        siteId = pointLocationVO.getSiteId();
        siteName = pointLocationVO.getSiteName();
        areaId = pointLocationVO.getAreaId();
        areaName = pointLocationVO.getAreaName();
        assetId = pointLocationVO.getAssetId();
        assetName = pointLocationVO.getAssetName();
        pointLocationId = pointLocationVO.getPointLocationId();
        pointLocationName = pointLocationVO.getPointLocationName();
        description = pointLocationVO.getDescription();
        deviceSerialNumber = pointLocationVO.getDeviceSerialNumber();
        ffSetNames = pointLocationVO.getFfSetNames();
        ffSetIds = pointLocationVO.getFfSetIds();
        speedRatio = pointLocationVO.getSpeedRatio();
        sampleInterval = pointLocationVO.getSampleInterval();
        tachName = pointLocationVO.getTachName();
        tachId = pointLocationVO.getTachId();
        alarmEnabled = pointLocationVO.isAlarmEnabled();
        basestationPortNum = pointLocationVO.getBasestationPortNum();
        rollDiameter = pointLocationVO.getRollDiameter();
        rollDiameterUnits = pointLocationVO.getRollDiameterUnits();
        tachReferenceUnits = pointLocationVO.getTachReferenceUnits();
        pointList = pointLocationVO.getPointList();
        ffSetName = pointLocationVO.getFfSetName();
        sensorTypes = pointLocationVO.getSensorTypes();
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public UUID getAreaId() {
        return areaId;
    }

    public void setAreaId(UUID areaId) {
        this.areaId = areaId;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public UUID getAssetId() {
        return assetId;
    }

    public void setAssetId(UUID assetId) {
        this.assetId = assetId;
    }

    public String getAssetName() {
        return assetName;
    }

    public void setAssetName(String assetName) {
        this.assetName = assetName;
    }

    public UUID getPointLocationId() {
        return pointLocationId;
    }

    public void setPointLocationId(UUID pointLocationId) {
        this.pointLocationId = pointLocationId;
    }

    public String getPointLocationName() {
        return pointLocationName;
    }

    public void setPointLocationName(String pointLocationName) {
        this.pointLocationName = pointLocationName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDeviceSerialNumber() {
        return deviceSerialNumber;
    }

    public void setDeviceSerialNumber(String deviceSerialNumber) {
        this.deviceSerialNumber = deviceSerialNumber;
    }

    public String getFfSetNames() {
        return ffSetNames;
    }

    public void setFfSetNames(String ffSetNames) {
        this.ffSetNames = ffSetNames;
    }

    public String getFfSetIds() {
        return ffSetIds;
    }

    public void setFfSetIds(String ffSetIds) {
        this.ffSetIds = ffSetIds;
    }

    public float getSpeedRatio() {
        return speedRatio;
    }

    public void setSpeedRatio(float speedRatio) {
        this.speedRatio = speedRatio;
    }

    public short getSampleInterval() {
        return sampleInterval;
    }

    public void setSampleInterval(short sampleInterval) {
        this.sampleInterval = sampleInterval;
    }

    public String getTachName() {
        return tachName;
    }

    public void setTachName(String tachName) {
        this.tachName = tachName;
    }

    public UUID getTachId() {
        return tachId;
    }

    public void setTachId(UUID tachId) {
        this.tachId = tachId;
    }

    public boolean isAlarmEnabled() {
        return alarmEnabled;
    }

    public void setAlarmEnabled(boolean alarmEnabled) {
        this.alarmEnabled = alarmEnabled;
    }

    public short getBasestationPortNum() {
        return basestationPortNum;
    }

    public void setBasestationPortNum(short basestationPortNum) {
        this.basestationPortNum = basestationPortNum;
    }

    public float getRollDiameter() {
        return rollDiameter;
    }

    public void setRollDiameter(float rollDiameter) {
        this.rollDiameter = rollDiameter;
    }

    public String getRollDiameterUnits() {
        return rollDiameterUnits;
    }

    public void setRollDiameterUnits(String rollDiameterUnits) {
        this.rollDiameterUnits = rollDiameterUnits;
    }

    public String getTachReferenceUnits() {
        return tachReferenceUnits;
    }

    public void setTachReferenceUnits(String tachReferenceUnits) {
        this.tachReferenceUnits = tachReferenceUnits;
    }

    public List<PointVO> getPointList() {
        return pointList;
    }

    public void setPointList(List<PointVO> pointList) {
        this.pointList = pointList;
    }

    public Set<String> getFfSetName() {
        return ffSetName;
    }

    public void setFfSetName(Set<String> ffSetName) {
        this.ffSetName = ffSetName;
    }

    public String getSensorTypes() {
        return sensorTypes;
    }

    public void setSensorTypes(String sensorTypes) {
        this.sensorTypes = sensorTypes;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + Objects.hashCode(this.customerAccount);
        hash = 97 * hash + Objects.hashCode(this.siteId);
        hash = 97 * hash + Objects.hashCode(this.siteName);
        hash = 97 * hash + Objects.hashCode(this.areaId);
        hash = 97 * hash + Objects.hashCode(this.areaName);
        hash = 97 * hash + Objects.hashCode(this.assetId);
        hash = 97 * hash + Objects.hashCode(this.assetName);
        hash = 97 * hash + Objects.hashCode(this.pointLocationId);
        hash = 97 * hash + Objects.hashCode(this.pointLocationName);
        hash = 97 * hash + Objects.hashCode(this.description);
        hash = 97 * hash + Objects.hashCode(this.deviceSerialNumber);
        hash = 97 * hash + Objects.hashCode(this.ffSetNames);
        hash = 97 * hash + Objects.hashCode(this.ffSetIds);
        hash = 97 * hash + Float.floatToIntBits(this.speedRatio);
        hash = 97 * hash + this.sampleInterval;
        hash = 97 * hash + Objects.hashCode(this.tachName);
        hash = 97 * hash + Objects.hashCode(this.tachId);
        hash = 97 * hash + (this.alarmEnabled ? 1 : 0);
        hash = 97 * hash + this.basestationPortNum;
        hash = 97 * hash + Float.floatToIntBits(this.rollDiameter);
        hash = 97 * hash + Objects.hashCode(this.rollDiameterUnits);
        hash = 97 * hash + Objects.hashCode(this.tachReferenceUnits);
        hash = 97 * hash + Objects.hashCode(this.pointList);
        hash = 97 * hash + Objects.hashCode(this.ffSetName);
        hash = 97 * hash + Objects.hashCode(this.sensorTypes);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PointLocationVO other = (PointLocationVO) obj;
        if (Float.floatToIntBits(this.speedRatio) != Float.floatToIntBits(other.speedRatio)) {
            return false;
        }
        if (this.sampleInterval != other.sampleInterval) {
            return false;
        }
        if (this.alarmEnabled != other.alarmEnabled) {
            return false;
        }
        if (this.basestationPortNum != other.basestationPortNum) {
            return false;
        }
        if (Float.floatToIntBits(this.rollDiameter) != Float.floatToIntBits(other.rollDiameter)) {
            return false;
        }
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.siteName, other.siteName)) {
            return false;
        }
        if (!Objects.equals(this.areaName, other.areaName)) {
            return false;
        }
        if (!Objects.equals(this.assetName, other.assetName)) {
            return false;
        }
        if (!Objects.equals(this.pointLocationName, other.pointLocationName)) {
            return false;
        }
        if (!Objects.equals(this.description, other.description)) {
            return false;
        }
        if (!Objects.equals(this.deviceSerialNumber, other.deviceSerialNumber)) {
            return false;
        }
        if (!Objects.equals(this.ffSetNames, other.ffSetNames)) {
            return false;
        }
        if (!Objects.equals(this.ffSetIds, other.ffSetIds)) {
            return false;
        }
        if (!Objects.equals(this.tachName, other.tachName)) {
            return false;
        }
        if (!Objects.equals(this.rollDiameterUnits, other.rollDiameterUnits)) {
            return false;
        }
        if (!Objects.equals(this.tachReferenceUnits, other.tachReferenceUnits)) {
            return false;
        }
        if (!Objects.equals(this.sensorTypes, other.sensorTypes)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        if (!Objects.equals(this.areaId, other.areaId)) {
            return false;
        }
        if (!Objects.equals(this.assetId, other.assetId)) {
            return false;
        }
        if (!Objects.equals(this.pointLocationId, other.pointLocationId)) {
            return false;
        }
        if (!Objects.equals(this.tachId, other.tachId)) {
            return false;
        }
        if (!Objects.equals(this.pointList, other.pointList)) {
            return false;
        }
        if (!Objects.equals(this.ffSetName, other.ffSetName)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "PointLocationVO{" + "customerAccount=" + customerAccount + ", siteId=" + siteId + ", siteName=" + siteName + ", areaId=" + areaId + ", areaName=" + areaName + ", assetId=" + assetId + ", assetName=" + assetName + ", pointLocationId=" + pointLocationId + ", pointLocationName=" + pointLocationName + ", description=" + description + ", deviceSerialNumber=" + deviceSerialNumber + ", ffSetNames=" + ffSetNames + ", ffSetIds=" + ffSetIds + ", speedRatio=" + speedRatio + ", sampleInterval=" + sampleInterval + ", tachName=" + tachName + ", tachId=" + tachId + ", alarmEnabled=" + alarmEnabled + ", basestationPortNum=" + basestationPortNum + ", rollDiameter=" + rollDiameter + ", rollDiameterUnits=" + rollDiameterUnits + ", tachReferenceUnits=" + tachReferenceUnits + ", pointList=" + pointList + ", ffSetName=" + ffSetName + ", sensorTypes=" + sensorTypes + '}';
    }
}
