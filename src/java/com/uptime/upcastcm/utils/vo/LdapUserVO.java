/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.utils.vo;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author gsingh
 */
public class LdapUserVO implements Serializable {
    private String userId;
    private String mail;
    private String fullName;
    private String firstName;
    private String lastName;
    private String company;
    private String userPassword;
    private List<String> groups;
    private String customerAccount;

    public LdapUserVO() {
    }

    public LdapUserVO(LdapUserVO ldapUserVO) {
        userId = ldapUserVO.getUserId();
        mail = ldapUserVO.getMail();
        fullName = ldapUserVO.getFullName();
        firstName = ldapUserVO.getFirstName();
        lastName = ldapUserVO.getLastName();
        company = ldapUserVO.getCompany();
        userPassword = ldapUserVO.getUserPassword();
        groups = ldapUserVO.getGroups(); 
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public List<String> getGroups() {
        return groups;
    }

    public void setGroups(List<String> groups) {
        this.groups = groups;
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }
    
    

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + Objects.hashCode(this.userId);
        hash = 29 * hash + Objects.hashCode(this.mail);
        hash = 29 * hash + Objects.hashCode(this.fullName);
        hash = 29 * hash + Objects.hashCode(this.firstName);
        hash = 29 * hash + Objects.hashCode(this.lastName);
        hash = 29 * hash + Objects.hashCode(this.company);
        hash = 29 * hash + Objects.hashCode(this.userPassword);
        hash = 29 * hash + Objects.hashCode(this.groups);
        hash = 29 * hash + Objects.hashCode(this.customerAccount);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final LdapUserVO other = (LdapUserVO) obj;
        if (!Objects.equals(this.userId, other.userId)) {
            return false;
        }
        if (!Objects.equals(this.mail, other.mail)) {
            return false;
        }
        if (!Objects.equals(this.fullName, other.fullName)) {
            return false;
        }
        if (!Objects.equals(this.firstName, other.firstName)) {
            return false;
        }
        if (!Objects.equals(this.lastName, other.lastName)) {
            return false;
        }
        if (!Objects.equals(this.company, other.company)) {
            return false;
        }
        if (!Objects.equals(this.userPassword, other.userPassword)) {
            return false;
        }
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        return Objects.equals(this.groups, other.groups);
    }

    @Override
    public String toString() {
        return "LdapUserVO{" + "userId=" + userId + ", mail=" + mail + ", fullName=" + fullName + ", firstName=" + firstName + ", lastName=" + lastName + ", company=" + company + ", userPassword=" + userPassword + ", groups=" + groups + ", customerAccount=" + customerAccount + '}';
    }

    
    
}
