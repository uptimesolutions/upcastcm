/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.utils.vo;

import static com.uptime.client.utils.JsonConverterUtil.DATE_TIME_FORMATTER;
import java.io.Serializable;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 *
 * @author gopal
 */
public class AirbaseStatusVO implements Serializable {

    private String baseStationId;
    private String siteName;
    private String customerId;
    private Instant lastSampleReceived;
    private ZonedDateTime lastSampleReceivedWithSpecfZone;

    public AirbaseStatusVO() {
    }

    public AirbaseStatusVO(String baseStationId, String siteName, String customerId, Instant lastSampleReceived, ZonedDateTime lastSampleReceivedWithSpecfZone) {
        this.baseStationId = baseStationId;
        this.siteName = siteName;
        this.customerId = customerId;
        this.lastSampleReceived = lastSampleReceived;
        this.lastSampleReceivedWithSpecfZone = lastSampleReceivedWithSpecfZone;
    }
    
    public AirbaseStatusVO(GatewayLastUploadVO gatewayLastUploadVO){
        customerId = gatewayLastUploadVO.getCustomerAccount();
        siteName = gatewayLastUploadVO.getSiteName();
        baseStationId = gatewayLastUploadVO.getDeviceId();
        lastSampleReceived = gatewayLastUploadVO.getLastUpload();
    }

    public String getBaseStationId() {
        return baseStationId;
    }

    public void setBaseStationId(String baseStationId) {
        this.baseStationId = baseStationId;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public Instant getLastSampleReceived() {
        return lastSampleReceived;
    }

    public void setLastSampleReceived(Instant lastSampleReceived) {
        this.lastSampleReceived = lastSampleReceived;
    }

    public ZonedDateTime getLastSampleReceivedWithSpecfZone() {
        return lastSampleReceivedWithSpecfZone;
    }

    public void setLastSampleReceivedWithSpecfZone(ZonedDateTime lastSampleReceivedWithSpecfZone) {
        this.lastSampleReceivedWithSpecfZone = lastSampleReceivedWithSpecfZone;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getFormatedLastSampleReceived() {
        if (lastSampleReceivedWithSpecfZone != null) {
            return lastSampleReceivedWithSpecfZone.format(DATE_TIME_FORMATTER);
        }
        return "";
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + Objects.hashCode(this.baseStationId);
        hash = 97 * hash + Objects.hashCode(this.siteName);
        hash = 97 * hash + Objects.hashCode(this.customerId);
        hash = 97 * hash + Objects.hashCode(this.lastSampleReceived);
        hash = 97 * hash + Objects.hashCode(this.lastSampleReceivedWithSpecfZone);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AirbaseStatusVO other = (AirbaseStatusVO) obj;
        if (!Objects.equals(this.baseStationId, other.baseStationId)) {
            return false;
        }
        if (!Objects.equals(this.siteName, other.siteName)) {
            return false;
        }
        if (!Objects.equals(this.customerId, other.customerId)) {
            return false;
        }
        if (!Objects.equals(this.lastSampleReceived, other.lastSampleReceived)) {
            return false;
        }
        return Objects.equals(this.lastSampleReceivedWithSpecfZone, other.lastSampleReceivedWithSpecfZone);
    }

    @Override
    public String toString() {
        return "AirbaseStatusVO{" + "baseStationId=" + baseStationId + ", siteName=" + siteName + ", customerId=" + customerId + ", lastSampleReceived=" + lastSampleReceived + ", lastSampleReceivedWithSpecfZone=" + lastSampleReceivedWithSpecfZone + '}';
    }

}
