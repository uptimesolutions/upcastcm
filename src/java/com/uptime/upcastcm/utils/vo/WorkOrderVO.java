/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.utils.vo;

import com.uptime.upcastcm.bean.UserManageBean;
import java.io.Serializable;
import java.time.Instant;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import javax.faces.context.FacesContext;

/**
 *
 * @author twilcox
 */
public class WorkOrderVO implements Serializable {
    private String customerAccount;
    private UUID siteId;
    private UUID areaId;
    private UUID assetId;
    private UUID pointLocationId;
    private UUID pointId;
    private UUID rowId;
    private UUID apSetId;
    private String paramName;
    private byte priority;
    private byte newPriority;
    private Instant createdDate;
    private Instant closeDate;
    private short openedYear;
    private String action;
    private String areaName;
    private String assetComponent;
    private String assetName;
    private String channelType;
    private String description;
    private String issue;
    private String notes;
    private String user;
    private int priority1;
    private int priority2;
    private int priority3;
    private int priority4;
    private int totalCount;
    private String siteName;
    private String pointLocationName;
    private String pointName;
    private List<TrendDetailsVO> trendDetails;
    private String formattedCreatedDateString;
    private String formattedCloseDateString;
    private String deviceIds;
    
    public WorkOrderVO() {
    }

    public WorkOrderVO(String customerAccount, UUID siteId, UUID areaId, UUID assetId, UUID pointLocationId, UUID pointId, UUID rowId, UUID apSetId, String paramName, byte priority, byte newPriority, Instant createdDate, Instant closeDate, short openedYear, String action, String areaName, String assetComponent, String assetName, String channelType, String description, String issue, String notes, String user, int priority1, int priority2, int priority3, int priority4, int totalCount, String siteName, String pointLocationName, String pointName, List<TrendDetailsVO> trendDetails) {
        this.customerAccount = customerAccount;
        this.siteId = siteId;
        this.areaId = areaId;
        this.assetId = assetId;
        this.pointLocationId = pointLocationId;
        this.pointId = pointId;
        this.rowId = rowId;
        this.apSetId = apSetId;
        this.paramName = paramName;
        this.priority = priority;
        this.newPriority = newPriority;
        this.createdDate = createdDate;
        this.closeDate = closeDate;
        this.openedYear = openedYear;
        this.action = action;
        this.areaName = areaName;
        this.assetComponent = assetComponent;
        this.assetName = assetName;
        this.channelType = channelType;
        this.description = description;
        this.issue = issue;
        this.notes = notes;
        this.user = user;
        this.priority1 = priority1;
        this.priority2 = priority2;
        this.priority3 = priority3;
        this.priority4 = priority4;
        this.totalCount = totalCount;
        this.siteName = siteName;
        this.pointLocationName = pointLocationName;
        this.pointName = pointName;
        this.trendDetails = trendDetails;
    }

    public WorkOrderVO(WorkOrderVO workOrderVO) {
        customerAccount = workOrderVO.getCustomerAccount();
        siteId = workOrderVO.getSiteId();
        areaId = workOrderVO.getAreaId();
        assetId = workOrderVO.getAssetId();
        pointLocationId = workOrderVO.getPointLocationId();
        pointId = workOrderVO.getPointId();
        rowId = workOrderVO.getRowId();
        apSetId = workOrderVO.getApSetId();
        paramName = workOrderVO.getParamName();
        priority = workOrderVO.getPriority();
        newPriority = workOrderVO.getNewPriority();
        createdDate = workOrderVO.getCreatedDate();
        closeDate = workOrderVO.getCloseDate();
        openedYear = workOrderVO.getOpenedYear();
        action = workOrderVO.getAction();
        areaName = workOrderVO.getAreaName();
        assetComponent = workOrderVO.getAssetComponent();
        assetName = workOrderVO.getAssetName();
        channelType = workOrderVO.getChannelType();
        description = workOrderVO.getDescription();
        issue = workOrderVO.getIssue();
        notes = workOrderVO.getNotes();
        user = workOrderVO.getUser();
        priority1 = workOrderVO.getPriority1();
        priority2 = workOrderVO.getPriority2();
        priority3 = workOrderVO.getPriority3();
        priority4 = workOrderVO.getPriority4();
        totalCount = workOrderVO.getTotalCount();
        siteName = workOrderVO.getSiteName();
        pointLocationName = workOrderVO.getPointLocationName();
        pointName = workOrderVO.getPointName();
        trendDetails = workOrderVO.getTrendDetails();
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public UUID getAreaId() {
        return areaId;
    }

    public void setAreaId(UUID areaId) {
        this.areaId = areaId;
    }

    public UUID getAssetId() {
        return assetId;
    }

    public void setAssetId(UUID assetId) {
        this.assetId = assetId;
    }

    public UUID getPointLocationId() {
        return pointLocationId;
    }

    public void setPointLocationId(UUID pointLocationId) {
        this.pointLocationId = pointLocationId;
    }

    public UUID getPointId() {
        return pointId;
    }

    public void setPointId(UUID pointId) {
        this.pointId = pointId;
    }

    public UUID getRowId() {
        return rowId;
    }

    public void setRowId(UUID rowId) {
        this.rowId = rowId;
    }

    public UUID getApSetId() {
        return apSetId;
    }

    public void setApSetId(UUID apSetId) {
        this.apSetId = apSetId;
    }

    public byte getPriority() {
        return priority;
    }

    public void setPriority(byte priority) {
        this.priority = priority;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        UserManageBean userManageBean;
        
        this.createdDate = createdDate;
        
        if (createdDate != null && (userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean")) != null) {
            this.formattedCreatedDateString = userManageBean.getTzOffsetString(createdDate, 1);
        }
    }

    public Instant getCloseDate() {
        return closeDate;
    }

    public void setCloseDate(Instant closeDate) {
        UserManageBean userManageBean;
        
        this.closeDate = closeDate;
        
        if (closeDate != null && (userManageBean = (UserManageBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userManageBean")) != null) {
            this.formattedCloseDateString = userManageBean.getTzOffsetString(closeDate, 1);
        }
    }

    public short getOpenedYear() {
        return openedYear;
    }

    public void setOpenedYear(short openedYear) {
        this.openedYear = openedYear;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getAssetComponent() {
        return assetComponent;
    }

    public void setAssetComponent(String assetComponent) {
        this.assetComponent = assetComponent;
    }

    public String getAssetName() {
        return assetName;
    }

    public void setAssetName(String assetName) {
        this.assetName = assetName;
    }

    public String getChannelType() {
        return channelType;
    }

    public void setChannelType(String channelType) {
        this.channelType = channelType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIssue() {
        return issue;
    }

    public void setIssue(String issue) {
        this.issue = issue;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public int getPriority1() {
        return priority1;
    }

    public void setPriority1(int priority1) {
        this.priority1 = priority1;
    }

    public int getPriority2() {
        return priority2;
    }

    public void setPriority2(int priority2) {
        this.priority2 = priority2;
    }

    public int getPriority3() {
        return priority3;
    }

    public void setPriority3(int priority3) {
        this.priority3 = priority3;
    }

    public int getPriority4() {
        return priority4;
    }

    public void setPriority4(int priority4) {
        this.priority4 = priority4;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public String getParamName() {
        return paramName;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public String getPointLocationName() {
        return pointLocationName;
    }

    public void setPointLocationName(String pointLocationName) {
        this.pointLocationName = pointLocationName;
    }

    public byte getNewPriority() {
        return newPriority;
    }

    public void setNewPriority(byte newPriority) {
        this.newPriority = newPriority;
    }

    public List<TrendDetailsVO> getTrendDetails() {
        return trendDetails;
    }

    public void setTrendDetails(List<TrendDetailsVO> trendDetails) {
        this.trendDetails = trendDetails;
    }

    public String getPointName() {
        return pointName;
    }

    public void setPointName(String pointName) {
        this.pointName = pointName;
    }

    public String getFormattedCreatedDateString() {
        return formattedCreatedDateString;
    }

    public String getFormattedCloseDateString() {
        return formattedCloseDateString;
    }

    public String getDeviceIds() {
        return deviceIds;
    }

    public void setDeviceIds(String deviceIds) {
        this.deviceIds = deviceIds;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 89 * hash + Objects.hashCode(this.customerAccount);
        hash = 89 * hash + Objects.hashCode(this.siteId);
        hash = 89 * hash + Objects.hashCode(this.areaId);
        hash = 89 * hash + Objects.hashCode(this.assetId);
        hash = 89 * hash + Objects.hashCode(this.pointLocationId);
        hash = 89 * hash + Objects.hashCode(this.pointId);
        hash = 89 * hash + Objects.hashCode(this.rowId);
        hash = 89 * hash + Objects.hashCode(this.apSetId);
        hash = 89 * hash + Objects.hashCode(this.paramName);
        hash = 89 * hash + this.priority;
        hash = 89 * hash + this.newPriority;
        hash = 89 * hash + Objects.hashCode(this.createdDate);
        hash = 89 * hash + Objects.hashCode(this.closeDate);
        hash = 89 * hash + this.openedYear;
        hash = 89 * hash + Objects.hashCode(this.action);
        hash = 89 * hash + Objects.hashCode(this.areaName);
        hash = 89 * hash + Objects.hashCode(this.assetComponent);
        hash = 89 * hash + Objects.hashCode(this.assetName);
        hash = 89 * hash + Objects.hashCode(this.channelType);
        hash = 89 * hash + Objects.hashCode(this.description);
        hash = 89 * hash + Objects.hashCode(this.issue);
        hash = 89 * hash + Objects.hashCode(this.notes);
        hash = 89 * hash + Objects.hashCode(this.user);
        hash = 89 * hash + this.priority1;
        hash = 89 * hash + this.priority2;
        hash = 89 * hash + this.priority3;
        hash = 89 * hash + this.priority4;
        hash = 89 * hash + this.totalCount;
        hash = 89 * hash + Objects.hashCode(this.siteName);
        hash = 89 * hash + Objects.hashCode(this.pointLocationName);
        hash = 89 * hash + Objects.hashCode(this.pointName);
        hash = 89 * hash + Objects.hashCode(this.trendDetails);
        hash = 89 * hash + Objects.hashCode(this.formattedCreatedDateString);
        hash = 89 * hash + Objects.hashCode(this.formattedCloseDateString);
        hash = 89 * hash + Objects.hashCode(this.deviceIds);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final WorkOrderVO other = (WorkOrderVO) obj;
        if (this.priority != other.priority) {
            return false;
        }
        if (this.newPriority != other.newPriority) {
            return false;
        }
        if (this.openedYear != other.openedYear) {
            return false;
        }
        if (this.priority1 != other.priority1) {
            return false;
        }
        if (this.priority2 != other.priority2) {
            return false;
        }
        if (this.priority3 != other.priority3) {
            return false;
        }
        if (this.priority4 != other.priority4) {
            return false;
        }
        if (this.totalCount != other.totalCount) {
            return false;
        }
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.paramName, other.paramName)) {
            return false;
        }
        if (!Objects.equals(this.action, other.action)) {
            return false;
        }
        if (!Objects.equals(this.areaName, other.areaName)) {
            return false;
        }
        if (!Objects.equals(this.assetComponent, other.assetComponent)) {
            return false;
        }
        if (!Objects.equals(this.assetName, other.assetName)) {
            return false;
        }
        if (!Objects.equals(this.channelType, other.channelType)) {
            return false;
        }
        if (!Objects.equals(this.description, other.description)) {
            return false;
        }
        if (!Objects.equals(this.issue, other.issue)) {
            return false;
        }
        if (!Objects.equals(this.notes, other.notes)) {
            return false;
        }
        if (!Objects.equals(this.user, other.user)) {
            return false;
        }
        if (!Objects.equals(this.siteName, other.siteName)) {
            return false;
        }
        if (!Objects.equals(this.pointLocationName, other.pointLocationName)) {
            return false;
        }
        if (!Objects.equals(this.pointName, other.pointName)) {
            return false;
        }
        if (!Objects.equals(this.formattedCreatedDateString, other.formattedCreatedDateString)) {
            return false;
        }
        if (!Objects.equals(this.formattedCloseDateString, other.formattedCloseDateString)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        if (!Objects.equals(this.areaId, other.areaId)) {
            return false;
        }
        if (!Objects.equals(this.assetId, other.assetId)) {
            return false;
        }
        if (!Objects.equals(this.pointLocationId, other.pointLocationId)) {
            return false;
        }
        if (!Objects.equals(this.pointId, other.pointId)) {
            return false;
        }
        if (!Objects.equals(this.rowId, other.rowId)) {
            return false;
        }
        if (!Objects.equals(this.apSetId, other.apSetId)) {
            return false;
        }
        if (!Objects.equals(this.createdDate, other.createdDate)) {
            return false;
        }
        if (!Objects.equals(this.closeDate, other.closeDate)) {
            return false;
        }
        if (!Objects.equals(this.trendDetails, other.trendDetails)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "WorkOrderVO{" + "customerAccount=" + customerAccount + ", siteId=" + siteId + ", areaId=" + areaId + ", assetId=" + assetId + ", pointLocationId=" + pointLocationId + ", pointId=" + pointId + ", rowId=" + rowId + ", apSetId=" + apSetId + ", paramName=" + paramName + ", priority=" + priority + ", newPriority=" + newPriority + ", createdDate=" + createdDate + ", closeDate=" + closeDate + ", openedYear=" + openedYear + ", action=" + action + ", areaName=" + areaName + ", assetComponent=" + assetComponent + ", assetName=" + assetName + ", channelType=" + channelType + ", description=" + description + ", issue=" + issue + ", notes=" + notes + ", user=" + user + ", priority1=" + priority1 + ", priority2=" + priority2 + ", priority3=" + priority3 + ", priority4=" + priority4 + ", totalCount=" + totalCount + ", siteName=" + siteName + ", pointLocationName=" + pointLocationName + ", pointName=" + pointName + ", trendDetails=" + trendDetails + ", formattedCreatedDateString=" + formattedCreatedDateString + ", formattedCloseDateString=" + formattedCloseDateString + ", deviceIds=" + deviceIds + '}';
    }
}
