/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.utils.vo;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author gsingh
 */
public class SiteUsersVO implements Serializable {

    private String customerAccount;
    private UUID siteId;
    private String userId;
    private String firstName;
    private String lastName;
    private String siteRole;
    private String company;
 
    public SiteUsersVO() {
    }

    public SiteUsersVO(SiteUsersVO svo) {
        customerAccount = svo.getCustomerAccount();
        siteId = svo.getSiteId();
        userId = svo.getUserId();
        firstName = svo.getFirstName();
        lastName = svo.getLastName();
        siteRole = svo.getSiteRole();
        company  = svo.getCompany();
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSiteRole() {
        return siteRole;
    }

    public void setSiteRole(String siteRole) {
        this.siteRole = siteRole;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }
 

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 59 * hash + Objects.hashCode(this.customerAccount);
        hash = 59 * hash + Objects.hashCode(this.siteId);
        hash = 59 * hash + Objects.hashCode(this.userId);
        hash = 59 * hash + Objects.hashCode(this.firstName);
        hash = 59 * hash + Objects.hashCode(this.lastName);
        hash = 59 * hash + Objects.hashCode(this.siteRole);
         hash = 59 * hash + Objects.hashCode(this.company);
         return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SiteUsersVO other = (SiteUsersVO) obj;
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.userId, other.userId)) {
            return false;
        }
        if (!Objects.equals(this.firstName, other.firstName)) {
            return false;
        }
        if (!Objects.equals(this.lastName, other.lastName)) {
            return false;
        }
        if (!Objects.equals(this.siteRole, other.siteRole)) {
            return false;
        }
        if (!Objects.equals(this.company, other.company)) {
            return false;
        }
        return Objects.equals(this.siteId, other.siteId);
    }

    

    @Override
    public String toString() {
        return "SiteUsersVO{" + "customerAccount=" + customerAccount + ", siteId=" + siteId + ", userId=" + userId + ", firstName=" + firstName + ", lastName=" + lastName + ", siteRole=" + siteRole + ", company=" + company + '}';
    }

   

}
