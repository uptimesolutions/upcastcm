/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.utils.vo;

import java.io.Serializable;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author twilcox
 */
public class WaveformVO implements Serializable {
    private Instant sampleTime;
    private String deviceId;
    private byte channelNum;
    private float sampleRateHz;
    private String sensitivityUnits;
    private float tachSpeed;
    private List<Float> samples;

    public WaveformVO() {
    }

    public WaveformVO(Instant sampleTime, String deviceId, byte channelNum, float sampleRateHz, String sensitivityUnits, float tachSpeed, List<Float> samples) {
        this.sampleTime = sampleTime;
        this.deviceId = deviceId;
        this.channelNum = channelNum;
        this.sampleRateHz = sampleRateHz;
        this.sensitivityUnits = sensitivityUnits;
        this.tachSpeed = tachSpeed;
        this.samples = samples != null ? new ArrayList(samples) : null;
    }

    public WaveformVO(WaveformVO waveformVO) {
        sampleTime = waveformVO.getSampleTime();
        deviceId = waveformVO.getDeviceId();
        channelNum = waveformVO.getChannelNum();
        sampleRateHz = waveformVO.getSampleRateHz();
        sensitivityUnits = waveformVO.getSensitivityUnits();
        tachSpeed = waveformVO.getTachSpeed();
        samples = waveformVO.getSamples() != null ? new ArrayList(waveformVO.getSamples()) : null;
    }

    public Instant getSampleTime() {
        return sampleTime;
    }

    public void setSampleTime(Instant sampleTime) {
        this.sampleTime = sampleTime;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public byte getChannelNum() {
        return channelNum;
    }

    public void setChannelNum(byte channelNum) {
        this.channelNum = channelNum;
    }

    public float getSampleRateHz() {
        return sampleRateHz;
    }

    public void setSampleRateHz(float sampleRateHz) {
        this.sampleRateHz = sampleRateHz;
    }

    public String getSensitivityUnits() {
        return sensitivityUnits;
    }

    public void setSensitivityUnits(String sensitivityUnits) {
        this.sensitivityUnits = sensitivityUnits;
    }

    public float getTachSpeed() {
        return tachSpeed;
    }

    public void setTachSpeed(float tachSpeed) {
        this.tachSpeed = tachSpeed;
    }

    public List<Float> getSamples() {
        return samples;
    }

    public void setSamples(List<Float> samples) {
        this.samples = samples;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + Objects.hashCode(this.sampleTime);
        hash = 71 * hash + Objects.hashCode(this.deviceId);
        hash = 71 * hash + this.channelNum;
        hash = 71 * hash + Float.floatToIntBits(this.sampleRateHz);
        hash = 71 * hash + Objects.hashCode(this.sensitivityUnits);
        hash = 71 * hash + Float.floatToIntBits(this.tachSpeed);
        hash = 71 * hash + Objects.hashCode(this.samples);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final WaveformVO other = (WaveformVO) obj;
        if (this.channelNum != other.channelNum) {
            return false;
        }
        if (Float.floatToIntBits(this.sampleRateHz) != Float.floatToIntBits(other.sampleRateHz)) {
            return false;
        }
        if (Float.floatToIntBits(this.tachSpeed) != Float.floatToIntBits(other.tachSpeed)) {
            return false;
        }
        if (!Objects.equals(this.deviceId, other.deviceId)) {
            return false;
        }
        if (!Objects.equals(this.sensitivityUnits, other.sensitivityUnits)) {
            return false;
        }
        if (!Objects.equals(this.sampleTime, other.sampleTime)) {
            return false;
        }
        if (!Objects.equals(this.samples, other.samples)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "WaveformVO{" + "sampleTime=" + sampleTime + ", deviceId=" + deviceId + ", channelNum=" + channelNum + ", sampleRateHz=" + sampleRateHz + ", sensitivityUnits=" + sensitivityUnits + ", tachSpeed=" + tachSpeed + ", samples=" + samples + '}';
    }
}
