/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.upcastcm.utils.vo;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author gopal
 */
public class GraphDataVO implements Serializable {
    private String selectedAmplitudeUnit;
    private String selectedTimeUnit;
    private String selectedFrequencyUnit;
    private String selectedAmpFactor;

    private float rmsWave;
    private float totalAmplSpec;
    
    private float tachSpeed;
    private String tachSpeedUnit;
    private float originalTachSpeed;
    
    private UUID selectedFfSetId;
    private List<UUID> selectedFfIds;
    
    private Integer startingHarmonic;
    private Integer endingHarmonic;

    private List<FaultFrequenciesVO> frequencyList;
    private Map<String, String> ffMarkerMap;

    public GraphDataVO() {
    }

    public GraphDataVO(String selectedAmplitudeUnit, String selectedTimeUnit, String selectedFrequencyUnit, String selectedAmpFactor, float rmsWave, float totalAmplSpec, float tachSpeed, String tachSpeedUnit, float originalTachSpeed, UUID selectedFfSetId, List<UUID> selectedFfIds, Integer startingHarmonic, Integer endingHarmonic, List<FaultFrequenciesVO> frequencyList, Map<String, String> ffMarkerMap) {
        this.selectedAmplitudeUnit = selectedAmplitudeUnit;
        this.selectedTimeUnit = selectedTimeUnit;
        this.selectedFrequencyUnit = selectedFrequencyUnit;
        this.selectedAmpFactor = selectedAmpFactor;
        this.rmsWave = rmsWave;
        this.totalAmplSpec = totalAmplSpec;
        this.tachSpeed = tachSpeed;
        this.tachSpeedUnit = tachSpeedUnit;
        this.originalTachSpeed = originalTachSpeed;
        this.selectedFfSetId = selectedFfSetId;
        this.selectedFfIds = selectedFfIds;
        this.startingHarmonic = startingHarmonic;
        this.endingHarmonic = endingHarmonic;
        this.frequencyList = frequencyList;
        this.ffMarkerMap = ffMarkerMap;
    }


    public GraphDataVO(GraphDataVO graphDataVO) {
        selectedAmplitudeUnit = graphDataVO.getSelectedAmplitudeUnit();
        selectedTimeUnit = graphDataVO.getSelectedTimeUnit();
        selectedFrequencyUnit = graphDataVO.getSelectedFrequencyUnit();
        selectedAmpFactor = graphDataVO.getSelectedAmpFactor();
        rmsWave = graphDataVO.getRmsWave();
        totalAmplSpec = graphDataVO.getTotalAmplSpec();
        tachSpeed = graphDataVO.getTachSpeed();
        selectedFfSetId = graphDataVO.getSelectedFfSetId();
        selectedFfIds = graphDataVO.getSelectedFfIds();
        startingHarmonic = graphDataVO.getStartingHarmonic();
        endingHarmonic = graphDataVO.getEndingHarmonic();
        frequencyList = graphDataVO.getFrequencyList();
        ffMarkerMap = graphDataVO.getFfMarkerMap();
        originalTachSpeed = graphDataVO.getOriginalTachSpeed();
        tachSpeedUnit = graphDataVO.getTachSpeedUnit();
    }

    public String getSelectedAmplitudeUnit() {
        return selectedAmplitudeUnit;
    }

    public void setSelectedAmplitudeUnit(String selectedAmplitudeUnit) {
        this.selectedAmplitudeUnit = selectedAmplitudeUnit;
    }

    public String getSelectedTimeUnit() {
        return selectedTimeUnit;
    }

    public void setSelectedTimeUnit(String selectedTimeUnit) {
        this.selectedTimeUnit = selectedTimeUnit;
    }

    public String getSelectedFrequencyUnit() {
        return selectedFrequencyUnit;
    }

    public void setSelectedFrequencyUnit(String selectedFrequencyUnit) {
        this.selectedFrequencyUnit = selectedFrequencyUnit;
    }

    public String getSelectedAmpFactor() {
        return selectedAmpFactor;
    }

    public void setSelectedAmpFactor(String selectedAmpFactor) {
        this.selectedAmpFactor = selectedAmpFactor;
    }

    public float getRmsWave() {
        return rmsWave;
    }

    public void setRmsWave(float rmsWave) {
        this.rmsWave = rmsWave;
    }

    public float getTotalAmplSpec() {
        return totalAmplSpec;
    }

    public void setTotalAmplSpec(float totalAmplSpec) {
        this.totalAmplSpec = totalAmplSpec;
    }

    public float getTachSpeed() {
        return tachSpeed;
    }

    public void setTachSpeed(float tachSpeed) {
        this.tachSpeed = tachSpeed;
    }

    public UUID getSelectedFfSetId() {
        return selectedFfSetId;
    }

    public void setSelectedFfSetId(UUID selectedFfSetId) {
        this.selectedFfSetId = selectedFfSetId;
    }

    public List<UUID> getSelectedFfIds() {
        return selectedFfIds;
    }

    public void setSelectedFfIds(List<UUID> selectedFfIds) {
        this.selectedFfIds = selectedFfIds;
    }

    public Integer getStartingHarmonic() {
        return startingHarmonic;
    }

    public void setStartingHarmonic(Integer startingHarmonic) {
        this.startingHarmonic = startingHarmonic;
    }

    public Integer getEndingHarmonic() {
        return endingHarmonic;
    }

    public void setEndingHarmonic(Integer endingHarmonic) {
        this.endingHarmonic = endingHarmonic;
    }

    public List<FaultFrequenciesVO> getFrequencyList() {
        return frequencyList;
    }

    public void setFrequencyList(List<FaultFrequenciesVO> frequencyList) {
        this.frequencyList = frequencyList;
    }

    public Map<String, String> getFfMarkerMap() {
        return ffMarkerMap;
    }

    public void setFfMarkerMap(Map<String, String> ffMarkerMap) {
        this.ffMarkerMap = ffMarkerMap;
    }

    public String getTachSpeedUnit() {
        return tachSpeedUnit;
    }

    public void setTachSpeedUnit(String tachSpeedUnit) {
        this.tachSpeedUnit = tachSpeedUnit;
    }

    public float getOriginalTachSpeed() {
        return originalTachSpeed;
    }

    public void setOriginalTachSpeed(float originalTachSpeed) {
        this.originalTachSpeed = originalTachSpeed;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 43 * hash + Objects.hashCode(this.selectedAmplitudeUnit);
        hash = 43 * hash + Objects.hashCode(this.selectedTimeUnit);
        hash = 43 * hash + Objects.hashCode(this.selectedFrequencyUnit);
        hash = 43 * hash + Objects.hashCode(this.selectedAmpFactor);
        hash = 43 * hash + Float.floatToIntBits(this.rmsWave);
        hash = 43 * hash + Float.floatToIntBits(this.totalAmplSpec);
        hash = 43 * hash + Float.floatToIntBits(this.tachSpeed);
        hash = 43 * hash + Objects.hashCode(this.tachSpeedUnit);
        hash = 43 * hash + Float.floatToIntBits(this.originalTachSpeed);
        hash = 43 * hash + Objects.hashCode(this.selectedFfSetId);
        hash = 43 * hash + Objects.hashCode(this.selectedFfIds);
        hash = 43 * hash + Objects.hashCode(this.startingHarmonic);
        hash = 43 * hash + Objects.hashCode(this.endingHarmonic);
        hash = 43 * hash + Objects.hashCode(this.frequencyList);
        hash = 43 * hash + Objects.hashCode(this.ffMarkerMap);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final GraphDataVO other = (GraphDataVO) obj;
        if (Float.floatToIntBits(this.rmsWave) != Float.floatToIntBits(other.rmsWave)) {
            return false;
        }
        if (Float.floatToIntBits(this.totalAmplSpec) != Float.floatToIntBits(other.totalAmplSpec)) {
            return false;
        }
        if (Float.floatToIntBits(this.tachSpeed) != Float.floatToIntBits(other.tachSpeed)) {
            return false;
        }
        if (Float.floatToIntBits(this.originalTachSpeed) != Float.floatToIntBits(other.originalTachSpeed)) {
            return false;
        }
        if (!Objects.equals(this.selectedAmplitudeUnit, other.selectedAmplitudeUnit)) {
            return false;
        }
        if (!Objects.equals(this.selectedTimeUnit, other.selectedTimeUnit)) {
            return false;
        }
        if (!Objects.equals(this.selectedFrequencyUnit, other.selectedFrequencyUnit)) {
            return false;
        }
        if (!Objects.equals(this.selectedAmpFactor, other.selectedAmpFactor)) {
            return false;
        }
        if (!Objects.equals(this.tachSpeedUnit, other.tachSpeedUnit)) {
            return false;
        }
        if (!Objects.equals(this.selectedFfSetId, other.selectedFfSetId)) {
            return false;
        }
        if (!Objects.equals(this.selectedFfIds, other.selectedFfIds)) {
            return false;
        }
        if (!Objects.equals(this.startingHarmonic, other.startingHarmonic)) {
            return false;
        }
        if (!Objects.equals(this.endingHarmonic, other.endingHarmonic)) {
            return false;
        }
        if (!Objects.equals(this.frequencyList, other.frequencyList)) {
            return false;
        }
        return Objects.equals(this.ffMarkerMap, other.ffMarkerMap);
    }

    @Override
    public String toString() {
        return "GraphDataVO{" + "selectedAmplitudeUnit=" + selectedAmplitudeUnit + ", selectedTimeUnit=" + selectedTimeUnit + ", selectedFrequencyUnit=" + selectedFrequencyUnit + ", selectedAmpFactor=" + selectedAmpFactor + ", rmsWave=" + rmsWave + ", totalAmplSpec=" + totalAmplSpec + ", tachSpeed=" + tachSpeed + ", tachSpeedUnit=" + tachSpeedUnit + ", originalTachSpeed=" + originalTachSpeed + ", selectedFfSetId=" + selectedFfSetId + ", selectedFfIds=" + selectedFfIds + ", startingHarmonic=" + startingHarmonic + ", endingHarmonic=" + endingHarmonic + ", frequencyList=" + frequencyList + ", ffMarkerMap=" + ffMarkerMap + '}';
    }

}
