/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.utils.vo;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author twilcox
 */
public class UserPreferencesVO implements Serializable {
    String userId;
    String customerAccount;
    String timezone;
    String localeDisplayName;
    UUID defaultSite;
    String defaultUnits;
    String defaultSiteName;
    String language;
  

    public UserPreferencesVO() {
    }

    public UserPreferencesVO(String timezone, String displayName) {
        this.timezone = timezone;
        this.localeDisplayName = displayName;
        this.language = displayName;
    }

    public UserPreferencesVO(UserPreferencesVO upvo) {
        this.timezone = upvo.getTimezone();
        this.localeDisplayName = upvo.getLocaleDisplayName();
        this.customerAccount = upvo.getCustomerAccount();
        this.defaultSite = upvo.getDefaultSite();
        this.userId = upvo.getUserId();
        this.defaultSiteName = upvo.getDefaultSiteName();
        this.language = upvo.getLanguage();
        this.defaultUnits = upvo.getDefaultUnits();
      
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getLocaleDisplayName() {
        return localeDisplayName;
    }

    public void setLocaleDisplayName(String localeDisplayName) {
        this.localeDisplayName = localeDisplayName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

        public UUID getDefaultSite() {
        return defaultSite;
    }

    public void setDefaultSite(UUID defaultSite) {
        this.defaultSite = defaultSite;
    }

    public String getDefaultUnits() {
        return defaultUnits;
    }

    public void setDefaultUnits(String defaultUnits) {
        this.defaultUnits = defaultUnits;
    }

    public String getDefaultSiteName() {
        return defaultSiteName;
    }

    public void setDefaultSiteName(String defaultSiteName) {
        this.defaultSiteName = defaultSiteName;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

   
    
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 47 * hash + Objects.hashCode(this.userId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UserPreferencesVO other = (UserPreferencesVO) obj;
        if (!Objects.equals(this.userId, other.userId)) {
            return false;
        }
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.timezone, other.timezone)) {
            return false;
        }
        if (!Objects.equals(this.localeDisplayName, other.localeDisplayName)) {
            return false;
        }
        if (!Objects.equals(this.defaultSiteName, other.defaultSiteName)) {
            return false;
        }
        if (!Objects.equals(this.defaultUnits, other.defaultUnits)) {
            return false;
        }
        if (!Objects.equals(this.language, other.language)) {
            return false;
        }
       
        return Objects.equals(this.defaultSite, other.defaultSite);
    }

    @Override
    public String toString() {
        return "UserPreferencesVO{" + "userId=" + userId + ", customerAccount=" + customerAccount + ", timezone=" + timezone + ", localeDisplayName=" + localeDisplayName + ", defaultSite=" + defaultSite + ", defaultUnits=" + defaultUnits + ", defaultSiteName=" + defaultSiteName + ", language=" + language + "}";
    }

}
