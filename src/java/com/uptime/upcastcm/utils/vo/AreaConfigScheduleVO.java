/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.utils.vo;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author gsingh
 */
public class AreaConfigScheduleVO implements Serializable {

    String customerAccount;
    UUID siteId;
    UUID areaId;
    UUID scheduleId;
    String siteName;
    String areaName;
    String scheduleName;
    byte dayOfWeek;
    String schDescription;
    String hoursOfDay;
    boolean continuous;
    List<DayVO> dayVOList;
    String type;

    public AreaConfigScheduleVO() {
    }

    public AreaConfigScheduleVO(String customerAccount, UUID siteId, UUID areaId, UUID scheduleId, String siteName, String areaName, String scheduleName, byte dayOfWeek, String schDescription, String hoursOfDay, boolean continuous, List<DayVO> dayVOList, String type) {
        this.customerAccount = customerAccount;
        this.siteId = siteId;
        this.areaId = areaId;
        this.scheduleId = scheduleId;
        this.siteName = siteName;
        this.areaName = areaName;
        this.scheduleName = scheduleName;
        this.dayOfWeek = dayOfWeek;
        this.schDescription = schDescription;
        this.hoursOfDay = hoursOfDay;
        this.continuous = continuous;
        this.dayVOList = dayVOList;
        this.type = type;
    }

    public AreaConfigScheduleVO(AreaConfigScheduleVO svo) {
        this.customerAccount = svo.getCustomerAccount();
        this.siteId = svo.getSiteId();
        this.areaId = svo.getAreaId();
        this.scheduleId = svo.getScheduleId();
        this.siteName = svo.getSiteName();
        this.areaName = svo.getAreaName();
        this.scheduleName = svo.getScheduleName();
        this.schDescription = svo.getSchDescription();
        this.hoursOfDay = svo.getHoursOfDay();
        this.continuous = svo.isContinuous();
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getScheduleName() {
        return scheduleName;
    }

    public void setScheduleName(String scheduleName) {
        this.scheduleName = scheduleName;
    }

    public byte getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(byte dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public String getSchDescription() {
        return schDescription;
    }

    public void setSchDescription(String schDescription) {
        this.schDescription = schDescription;
    }

    public String getHoursOfDay() {
        return hoursOfDay;
    }

    public void setHoursOfDay(String hoursOfDay) {
        this.hoursOfDay = hoursOfDay;
    }

    public boolean isContinuous() {
        return continuous;
    }

    public void setContinuous(boolean continuous) {
        this.continuous = continuous;
    }

    public List<DayVO> getDayVOList() {
        return dayVOList;
    }

    public void setDayVOList(List<DayVO> dayVOList) {
        this.dayVOList = dayVOList;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public UUID getAreaId() {
        return areaId;
    }

    public void setAreaId(UUID areaId) {
        this.areaId = areaId;
    }

    public UUID getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(UUID scheduleId) {
        this.scheduleId = scheduleId;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 47 * hash + Objects.hashCode(this.customerAccount);
        hash = 47 * hash + Objects.hashCode(this.siteId);
        hash = 47 * hash + Objects.hashCode(this.areaId);
        hash = 47 * hash + Objects.hashCode(this.scheduleId);
        hash = 47 * hash + Objects.hashCode(this.siteName);
        hash = 47 * hash + Objects.hashCode(this.areaName);
        hash = 47 * hash + Objects.hashCode(this.scheduleName);
        hash = 47 * hash + this.dayOfWeek;
        hash = 47 * hash + Objects.hashCode(this.schDescription);
        hash = 47 * hash + Objects.hashCode(this.hoursOfDay);
        hash = 47 * hash + (this.continuous ? 1 : 0);
        hash = 47 * hash + Objects.hashCode(this.dayVOList);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AreaConfigScheduleVO other = (AreaConfigScheduleVO) obj;
        if (this.dayOfWeek != other.dayOfWeek) {
            return false;
        }
        if (this.continuous != other.continuous) {
            return false;
        }
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        if (!Objects.equals(this.areaId, other.areaId)) {
            return false;
        }
        if (!Objects.equals(this.scheduleId, other.scheduleId)) {
            return false;
        }
        if (!Objects.equals(this.siteName, other.siteName)) {
            return false;
        }
        if (!Objects.equals(this.areaName, other.areaName)) {
            return false;
        }
        if (!Objects.equals(this.scheduleName, other.scheduleName)) {
            return false;
        }
        if (!Objects.equals(this.schDescription, other.schDescription)) {
            return false;
        }
        if (!Objects.equals(this.hoursOfDay, other.hoursOfDay)) {
            return false;
        }
        if (!Objects.equals(this.dayVOList, other.dayVOList)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "AreaConfigScheduleVO{" + "customerAccount=" + customerAccount + ", siteId=" + siteId + ", areaId=" + areaId + ", scheduleId=" + scheduleId + ", siteName=" + siteName + ", areaName=" + areaName + ", scheduleName=" + scheduleName + ", dayOfWeek=" + dayOfWeek + ", schDescription=" + schDescription + ", hoursOfDay=" + hoursOfDay + ", continuous=" + continuous + ", dayVOList=" + dayVOList + '}';
    }

}
