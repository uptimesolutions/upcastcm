/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.utils.vo;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import javax.faces.model.SelectItem;

/**
 *
 * @author kpati
 */
public class DevicePresetVO implements Serializable {

    private String customerAccount;
    private String name;
    private UUID siteId;
    private String deviceType;
    private UUID presetId;
    private String channelType;
    private byte channelNumber;
    private UUID alSetId;
    private UUID apSetId;
    private boolean alarmEnabled;
    private boolean disabled;
    private int sampleInterval;
    private float sensorOffset;
    private float sensorSensitivity;
    private String sensorType;
    private String sensorUnits;
    private String deviceId;
    private UUID areaId;
    private UUID assetId;
    private UUID pointLocationId;
    private UUID pointId;
    private String pointName;
    private String paramName;
    private String siteName;
    private String type;
    private List<ApAlSetVO> apAlSetVOList;
    private String apSetName;
    private String alSetName;
    private List<ApAlSetVO> alSetVOList;
    private String channelNumberLabel;
    private List<SelectItem> apAlSetSelectItemList;
    private String restrictions;
    private String pointType;
    private int sensorChannelNum;
    private String sensorSubType;
    private String sensorLocalOrientation;
    private String alarmEnabledValue;

    public DevicePresetVO() {

    }

    public DevicePresetVO(DevicePresetVO devicePresetVO) {
        customerAccount = devicePresetVO.getCustomerAccount();
        name = devicePresetVO.getName();
        siteId = devicePresetVO.getSiteId();
        deviceType = devicePresetVO.getDeviceType();
        presetId = devicePresetVO.getPresetId();
        channelType = devicePresetVO.getChannelType();
        channelNumber = devicePresetVO.getChannelNumber();
        alSetId = devicePresetVO.getAlSetId();
        apSetId = devicePresetVO.getApSetId();
        alarmEnabled = devicePresetVO.isAlarmEnabled();
        disabled = devicePresetVO.isDisabled();
        sampleInterval = devicePresetVO.getSampleInterval();
        sensorOffset = devicePresetVO.getSensorOffset();
        sensorSensitivity = devicePresetVO.getSensorSensitivity();
        sensorType = devicePresetVO.getSensorType();
        sensorUnits = devicePresetVO.getSensorUnits();
        deviceId = devicePresetVO.getDeviceId();
        areaId = devicePresetVO.getAreaId();
        assetId = devicePresetVO.getAssetId();
        pointLocationId = devicePresetVO.getPointLocationId();
        pointName = devicePresetVO.getPointName();
        paramName = devicePresetVO.getParamName();
        siteName = devicePresetVO.getSiteName();
        type = devicePresetVO.getType();
        apAlSetVOList = devicePresetVO.getApAlSetVOList();
        apSetName = devicePresetVO.getApSetName();
        alSetName = devicePresetVO.getAlSetName();
        alSetVOList = devicePresetVO.getAlSetVOList();
        channelNumberLabel = devicePresetVO.getChannelNumberLabel();
        apAlSetSelectItemList = devicePresetVO.getApAlSetSelectItemList();
        restrictions = devicePresetVO.getRestrictions();
        sensorSubType= devicePresetVO.getSensorSubType();
        sensorLocalOrientation=devicePresetVO.getSensorLocalOrientation();
        alarmEnabledValue = devicePresetVO.getAlarmEnabledValue();
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public UUID getPresetId() {
        return presetId;
    }

    public void setPresetId(UUID presetId) {
        this.presetId = presetId;
    }

    public String getChannelType() {
        return channelType;
    }

    public void setChannelType(String channelType) {
        this.channelType = channelType;
    }

    public byte getChannelNumber() {
        return channelNumber;
    }

    public void setChannelNumber(byte channelNumber) {
        this.channelNumber = channelNumber;
    }

    public UUID getAlSetId() {
        return alSetId;
    }

    public void setAlSetId(UUID alSetId) {
        this.alSetId = alSetId;
    }

    public UUID getApSetId() {
        return apSetId;
    }

    public void setApSetId(UUID apSetId) {
        this.apSetId = apSetId;
    }

    public boolean isAlarmEnabled() {
        return alarmEnabled;
    }

    public void setAlarmEnabled(boolean alarmEnabled) {
        this.alarmEnabled = alarmEnabled;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    public int getSampleInterval() {
        return sampleInterval;
    }

    public void setSampleInterval(int sampleInterval) {
        this.sampleInterval = sampleInterval;
    }

    public float getSensorOffset() {
        return sensorOffset;
    }

    public void setSensorOffset(float sensorOffset) {
        this.sensorOffset = sensorOffset;
    }

    public float getSensorSensitivity() {
        return sensorSensitivity;
    }

    public void setSensorSensitivity(float sensorSensitivity) {
        this.sensorSensitivity = sensorSensitivity;
    }

    public String getSensorType() {
        return sensorType;
    }

    public void setSensorType(String sensorType) {
        this.sensorType = sensorType;
    }

    public String getSensorUnits() {
        return sensorUnits;
    }

    public void setSensorUnits(String sensorUnits) {
        this.sensorUnits = sensorUnits;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public UUID getAreaId() {
        return areaId;
    }

    public void setAreaId(UUID areaId) {
        this.areaId = areaId;
    }

    public UUID getAssetId() {
        return assetId;
    }

    public void setAssetId(UUID assetId) {
        this.assetId = assetId;
    }

    public UUID getPointLocationId() {
        return pointLocationId;
    }

    public void setPointLocationId(UUID pointLocationId) {
        this.pointLocationId = pointLocationId;
    }

    public String getPointName() {
        return pointName;
    }

    public void setPointName(String pointName) {
        this.pointName = pointName;
    }

    public String getParamName() {
        return paramName;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<ApAlSetVO> getApAlSetVOList() {
        return apAlSetVOList;
    }

    public void setApAlSetVOList(List<ApAlSetVO> apAlSetVOList) {
        this.apAlSetVOList = apAlSetVOList;
    }

    public String getApSetName() {
        return apSetName;
    }

    public void setApSetName(String apSetName) {
        this.apSetName = apSetName;
    }

    public String getAlSetName() {
        return alSetName;
    }

    public void setAlSetName(String alSetName) {
        this.alSetName = alSetName;
    }

    public List<ApAlSetVO> getAlSetVOList() {
        return alSetVOList;
    }

    public void setAlSetVOList(List<ApAlSetVO> alSetVOList) {
        this.alSetVOList = alSetVOList;
    }

    public String getChannelNumberLabel() {
        return channelNumberLabel;
    }

    public void setChannelNumberLabel(String channelNumberLabel) {
        this.channelNumberLabel = channelNumberLabel;
    }

    public List<SelectItem> getApAlSetSelectItemList() {
        return apAlSetSelectItemList;
    }

    public void setApAlSetSelectItemList(List<SelectItem> apAlSetSelectItemList) {
        this.apAlSetSelectItemList = apAlSetSelectItemList;
    }

    public String getRestrictions() {
        return restrictions;
    }

    public void setRestrictions(String restrictions) {
        this.restrictions = restrictions;
    }

    public UUID getPointId() {
        return pointId;
    }

    public void setPointId(UUID pointId) {
        this.pointId = pointId;
    }

    public String getPointType() {
        return pointType;
    }

    public void setPointType(String pointType) {
        this.pointType = pointType;
    }

    public int getSensorChannelNum() {
        return sensorChannelNum;
    }

    public void setSensorChannelNum(int sensorChannelNum) {
        this.sensorChannelNum = sensorChannelNum;
    }

    public String getSensorSubType() {
        return sensorSubType;
    }

    public void setSensorSubType(String sensorSubType) {
        this.sensorSubType = sensorSubType;
    }

    public String getSensorLocalOrientation() {
        return sensorLocalOrientation;
    }

    public void setSensorLocalOrientation(String sensorLocalOrientation) {
        this.sensorLocalOrientation = sensorLocalOrientation;
    }

    public String getAlarmEnabledValue() {
        return alarmEnabledValue;
    }

    public void setAlarmEnabledValue(String alarmEnabledValue) {
        this.alarmEnabledValue = alarmEnabledValue;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.customerAccount);
        hash = 97 * hash + Objects.hashCode(this.name);
        hash = 97 * hash + Objects.hashCode(this.siteId);
        hash = 97 * hash + Objects.hashCode(this.deviceType);
        hash = 97 * hash + Objects.hashCode(this.presetId);
        hash = 97 * hash + Objects.hashCode(this.channelType);
        hash = 97 * hash + this.channelNumber;
        hash = 97 * hash + Objects.hashCode(this.alSetId);
        hash = 97 * hash + Objects.hashCode(this.apSetId);
        hash = 97 * hash + (this.alarmEnabled ? 1 : 0);
        hash = 97 * hash + (this.disabled ? 1 : 0);
        hash = 97 * hash + this.sampleInterval;
        hash = 97 * hash + Float.floatToIntBits(this.sensorOffset);
        hash = 97 * hash + Float.floatToIntBits(this.sensorSensitivity);
        hash = 97 * hash + Objects.hashCode(this.sensorType);
        hash = 97 * hash + Objects.hashCode(this.sensorUnits);
        hash = 97 * hash + Objects.hashCode(this.deviceId);
        hash = 97 * hash + Objects.hashCode(this.areaId);
        hash = 97 * hash + Objects.hashCode(this.assetId);
        hash = 97 * hash + Objects.hashCode(this.pointLocationId);
        hash = 97 * hash + Objects.hashCode(this.pointId);
        hash = 97 * hash + Objects.hashCode(this.pointName);
        hash = 97 * hash + Objects.hashCode(this.paramName);
        hash = 97 * hash + Objects.hashCode(this.siteName);
        hash = 97 * hash + Objects.hashCode(this.type);
        hash = 97 * hash + Objects.hashCode(this.apAlSetVOList);
        hash = 97 * hash + Objects.hashCode(this.apSetName);
        hash = 97 * hash + Objects.hashCode(this.alSetName);
        hash = 97 * hash + Objects.hashCode(this.alSetVOList);
        hash = 97 * hash + Objects.hashCode(this.channelNumberLabel);
        hash = 97 * hash + Objects.hashCode(this.apAlSetSelectItemList);
        hash = 97 * hash + Objects.hashCode(this.restrictions);
        hash = 97 * hash + Objects.hashCode(this.pointType);
        hash = 97 * hash + this.sensorChannelNum;
        hash = 97 * hash + Objects.hashCode(this.sensorSubType);
        hash = 97 * hash + Objects.hashCode(this.sensorLocalOrientation);
        hash = 97 * hash + Objects.hashCode(this.alarmEnabledValue);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DevicePresetVO other = (DevicePresetVO) obj;
        if (this.channelNumber != other.channelNumber) {
            return false;
        }
        if (this.alarmEnabled != other.alarmEnabled) {
            return false;
        }
        if (this.disabled != other.disabled) {
            return false;
        }
        if (this.sampleInterval != other.sampleInterval) {
            return false;
        }
        if (Float.floatToIntBits(this.sensorOffset) != Float.floatToIntBits(other.sensorOffset)) {
            return false;
        }
        if (Float.floatToIntBits(this.sensorSensitivity) != Float.floatToIntBits(other.sensorSensitivity)) {
            return false;
        }
        if (this.sensorChannelNum != other.sensorChannelNum) {
            return false;
        }
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.deviceType, other.deviceType)) {
            return false;
        }
        if (!Objects.equals(this.channelType, other.channelType)) {
            return false;
        }
        if (!Objects.equals(this.sensorType, other.sensorType)) {
            return false;
        }
        if (!Objects.equals(this.sensorUnits, other.sensorUnits)) {
            return false;
        }
        if (!Objects.equals(this.deviceId, other.deviceId)) {
            return false;
        }
        if (!Objects.equals(this.pointName, other.pointName)) {
            return false;
        }
        if (!Objects.equals(this.paramName, other.paramName)) {
            return false;
        }
        if (!Objects.equals(this.siteName, other.siteName)) {
            return false;
        }
        if (!Objects.equals(this.type, other.type)) {
            return false;
        }
        if (!Objects.equals(this.apSetName, other.apSetName)) {
            return false;
        }
        if (!Objects.equals(this.alSetName, other.alSetName)) {
            return false;
        }
        if (!Objects.equals(this.channelNumberLabel, other.channelNumberLabel)) {
            return false;
        }
        if (!Objects.equals(this.restrictions, other.restrictions)) {
            return false;
        }
        if (!Objects.equals(this.pointType, other.pointType)) {
            return false;
        }
        if (!Objects.equals(this.sensorSubType, other.sensorSubType)) {
            return false;
        }
        if (!Objects.equals(this.sensorLocalOrientation, other.sensorLocalOrientation)) {
            return false;
        }
        if (!Objects.equals(this.alarmEnabledValue, other.alarmEnabledValue)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        if (!Objects.equals(this.presetId, other.presetId)) {
            return false;
        }
        if (!Objects.equals(this.alSetId, other.alSetId)) {
            return false;
        }
        if (!Objects.equals(this.apSetId, other.apSetId)) {
            return false;
        }
        if (!Objects.equals(this.areaId, other.areaId)) {
            return false;
        }
        if (!Objects.equals(this.assetId, other.assetId)) {
            return false;
        }
        if (!Objects.equals(this.pointLocationId, other.pointLocationId)) {
            return false;
        }
        if (!Objects.equals(this.pointId, other.pointId)) {
            return false;
        }
        if (!Objects.equals(this.apAlSetVOList, other.apAlSetVOList)) {
            return false;
        }
        if (!Objects.equals(this.alSetVOList, other.alSetVOList)) {
            return false;
        }
        return Objects.equals(this.apAlSetSelectItemList, other.apAlSetSelectItemList);
    }

    @Override
    public String toString() {
        return "DevicePresetVO{" + "customerAccount=" + customerAccount + ", name=" + name + ", siteId=" + siteId + ", deviceType=" + deviceType + ", presetId=" + presetId + ", channelType=" + channelType + ", channelNumber=" + channelNumber + ", alSetId=" + alSetId + ", apSetId=" + apSetId + ", alarmEnabled=" + alarmEnabled + ", disabled=" + disabled + ", sampleInterval=" + sampleInterval + ", sensorOffset=" + sensorOffset + ", sensorSensitivity=" + sensorSensitivity + ", sensorType=" + sensorType + ", sensorUnits=" + sensorUnits + ", deviceId=" + deviceId + ", areaId=" + areaId + ", assetId=" + assetId + ", pointLocationId=" + pointLocationId + ", pointId=" + pointId + ", pointName=" + pointName + ", paramName=" + paramName + ", siteName=" + siteName + ", type=" + type + ", apAlSetVOList=" + apAlSetVOList + ", apSetName=" + apSetName + ", alSetName=" + alSetName + ", alSetVOList=" + alSetVOList + ", channelNumberLabel=" + channelNumberLabel + ", apAlSetSelectItemList=" + apAlSetSelectItemList + ", restrictions=" + restrictions + ", pointType=" + pointType + ", sensorChannelNum=" + sensorChannelNum + ", sensorSubType=" + sensorSubType + ", sensorLocalOrientation=" + sensorLocalOrientation + ", alarmEnabledValue=" + alarmEnabledValue + '}';
    }
    
    
    
}
