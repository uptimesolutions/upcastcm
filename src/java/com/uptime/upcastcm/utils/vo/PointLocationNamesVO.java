/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.utils.vo;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author Mohd Juned Alam
 */
public class PointLocationNamesVO implements Serializable {
    private String customerAccount;
    private UUID siteId;
    private String pointLocationName;
    private String description;
    private String siteName;

    public PointLocationNamesVO() {
    }

    public PointLocationNamesVO(PointLocationNamesVO pointLocationNamesVO) {
        customerAccount = pointLocationNamesVO.getCustomerAccount();
        siteId = pointLocationNamesVO.getSiteId();
        pointLocationName = pointLocationNamesVO.getPointLocationName();
        description = pointLocationNamesVO.getDescription();
        siteName = pointLocationNamesVO.getSiteName();
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public String getPointLocationName() {
        return pointLocationName;
    }

    public void setPointLocationName(String pointLocationName) {
        this.pointLocationName = pointLocationName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.customerAccount);
        hash = 89 * hash + Objects.hashCode(this.siteId);
        hash = 89 * hash + Objects.hashCode(this.pointLocationName);
        hash = 89 * hash + Objects.hashCode(this.description);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PointLocationNamesVO other = (PointLocationNamesVO) obj;
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.pointLocationName, other.pointLocationName)) {
            return false;
        }
        if (!Objects.equals(this.description, other.description)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "PointLocationNamesVO{" + "customerAccount=" + customerAccount + ", siteId=" + siteId + ", pointLocationName=" + pointLocationName + ", description=" + description + '}';
    }

}
