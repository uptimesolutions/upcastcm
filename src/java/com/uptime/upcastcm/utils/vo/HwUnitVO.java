/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.utils.vo;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author kpati
 */
public class HwUnitVO implements Serializable{
    //private String deviceId;
    private String existingDeviceId;
    private String newDeviceId;
    private String channelType;
    private byte channelNum;
    private String customerAccount;
    private UUID siteId;
    private UUID areaId;
    private UUID assetId;
    private UUID pointLocationId;
    private UUID pointId;
    private UUID alSetId;
    private UUID apSetId;
    private String pointName;
    private String paramName;
    private boolean disabled;
    private String pointType;
    private int sampleInterval;
    private int sensorChannelNum;
    private float sensorOffset;
    private float sensorSensitivity;
    private String sensorType;
    private String sensorUnits;
    private boolean useCustomLimits;
    private boolean demodEnabled;
    private float demodHighPass;
    private float demodLowPass;
    private int dwell;
    private int fMax;
    private String freqUnits;
    private float highAlert;
    private boolean highAlertActive;
    private float highFault;
    private boolean highFaultActive;
    private boolean customized;
    private float lowAlert;
    private boolean lowAlertActive;
    private float lowFault;
    private boolean lowFaultActive;
    private float maxFreq;
    private float minFreq;
    private String paramAmpFactor;
    private String paramType;
    private String paramUnits;
    private float period;
    private int resolution;
    private float sampleRate;  
    private boolean alarmEnabled;

    public String getExistingDeviceId() {
        return existingDeviceId;
    }

    public void setExistingDeviceId(String existingDeviceId) {
        this.existingDeviceId = existingDeviceId;
    }

    public String getNewDeviceId() {
        return newDeviceId;
    }

    public void setNewDeviceId(String newDeviceId) {
        this.newDeviceId = newDeviceId;
    }

    public String getChannelType() {
        return channelType;
    }

    public void setChannelType(String channelType) {
        this.channelType = channelType;
    }

    public byte getChannelNum() {
        return channelNum;
    }

    public void setChannelNum(byte channelNum) {
        this.channelNum = channelNum;
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public UUID getAreaId() {
        return areaId;
    }

    public void setAreaId(UUID areaId) {
        this.areaId = areaId;
    }

    public UUID getAssetId() {
        return assetId;
    }

    public void setAssetId(UUID assetId) {
        this.assetId = assetId;
    }

    public UUID getPointLocationId() {
        return pointLocationId;
    }

    public void setPointLocationId(UUID pointLocationId) {
        this.pointLocationId = pointLocationId;
    }

    public UUID getPointId() {
        return pointId;
    }

    public void setPointId(UUID pointId) {
        this.pointId = pointId;
    }

    public UUID getAlSetId() {
        return alSetId;
    }

    public void setAlSetId(UUID alSetId) {
        this.alSetId = alSetId;
    }

    public UUID getApSetId() {
        return apSetId;
    }

    public void setApSetId(UUID apSetId) {
        this.apSetId = apSetId;
    }

    public String getPointName() {
        return pointName;
    }

    public void setPointName(String pointName) {
        this.pointName = pointName;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    public String getPointType() {
        return pointType;
    }

    public void setPointType(String pointType) {
        this.pointType = pointType;
    }

    public int getSampleInterval() {
        return sampleInterval;
    }

    public void setSampleInterval(int sampleInterval) {
        this.sampleInterval = sampleInterval;
    }

    public int getSensorChannelNum() {
        return sensorChannelNum;
    }

    public void setSensorChannelNum(int sensorChannelNum) {
        this.sensorChannelNum = sensorChannelNum;
    }

    public float getSensorOffset() {
        return sensorOffset;
    }

    public void setSensorOffset(float sensorOffset) {
        this.sensorOffset = sensorOffset;
    }

    public float getSensorSensitivity() {
        return sensorSensitivity;
    }

    public void setSensorSensitivity(float sensorSensitivity) {
        this.sensorSensitivity = sensorSensitivity;
    }

    public String getSensorType() {
        return sensorType;
    }

    public void setSensorType(String sensorType) {
        this.sensorType = sensorType;
    }

    public String getSensorUnits() {
        return sensorUnits;
    }

    public void setSensorUnits(String sensorUnits) {
        this.sensorUnits = sensorUnits;
    }

    public boolean isUseCustomLimits() {
        return useCustomLimits;
    }

    public void setUseCustomLimits(boolean useCustomLimits) {
        this.useCustomLimits = useCustomLimits;
    }

    public String getParamName() {
        return paramName;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    public boolean isDemodEnabled() {
        return demodEnabled;
    }

    public void setDemodEnabled(boolean demodEnabled) {
        this.demodEnabled = demodEnabled;
    }

    public float getDemodHighPass() {
        return demodHighPass;
    }

    public void setDemodHighPass(float demodHighPass) {
        this.demodHighPass = demodHighPass;
    }

    public float getDemodLowPass() {
        return demodLowPass;
    }

    public void setDemodLowPass(float demodLowPass) {
        this.demodLowPass = demodLowPass;
    }

    public int getDwell() {
        return dwell;
    }

    public void setDwell(int dwell) {
        this.dwell = dwell;
    }

    public int getfMax() {
        return fMax;
    }

    public void setfMax(int fMax) {
        this.fMax = fMax;
    }

    public String getFreqUnits() {
        return freqUnits;
    }

    public void setFreqUnits(String freqUnits) {
        this.freqUnits = freqUnits;
    }

    public float getHighAlert() {
        return highAlert;
    }

    public void setHighAlert(float highAlert) {
        this.highAlert = highAlert;
    }

    public boolean isHighAlertActive() {
        return highAlertActive;
    }

    public void setHighAlertActive(boolean highAlertActive) {
        this.highAlertActive = highAlertActive;
    }

    public float getHighFault() {
        return highFault;
    }

    public void setHighFault(float highFault) {
        this.highFault = highFault;
    }

    public boolean isHighFaultActive() {
        return highFaultActive;
    }

    public void setHighFaultActive(boolean highFaultActive) {
        this.highFaultActive = highFaultActive;
    }

    public boolean isCustomized() {
        return customized;
    }

    public void setCustomized(boolean customized) {
        this.customized = customized;
    }

    public float getLowAlert() {
        return lowAlert;
    }

    public void setLowAlert(float lowAlert) {
        this.lowAlert = lowAlert;
    }

    public boolean isLowAlertActive() {
        return lowAlertActive;
    }

    public void setLowAlertActive(boolean lowAlertActive) {
        this.lowAlertActive = lowAlertActive;
    }

    public float getLowFault() {
        return lowFault;
    }

    public void setLowFault(float lowFault) {
        this.lowFault = lowFault;
    }

    public boolean isLowFaultActive() {
        return lowFaultActive;
    }

    public void setLowFaultActive(boolean lowFaultActive) {
        this.lowFaultActive = lowFaultActive;
    }

    public float getMaxFreq() {
        return maxFreq;
    }

    public void setMaxFreq(float maxFreq) {
        this.maxFreq = maxFreq;
    }

    public float getMinFreq() {
        return minFreq;
    }

    public void setMinFreq(float minFreq) {
        this.minFreq = minFreq;
    }

    public String getParamAmpFactor() {
        return paramAmpFactor;
    }

    public void setParamAmpFactor(String paramAmpFactor) {
        this.paramAmpFactor = paramAmpFactor;
    }

    public String getParamType() {
        return paramType;
    }

    public void setParamType(String paramType) {
        this.paramType = paramType;
    }

    public String getParamUnits() {
        return paramUnits;
    }

    public void setParamUnits(String paramUnits) {
        this.paramUnits = paramUnits;
    }

    public float getPeriod() {
        return period;
    }

    public void setPeriod(float period) {
        this.period = period;
    }

    public int getResolution() {
        return resolution;
    }

    public void setResolution(int resolution) {
        this.resolution = resolution;
    }

    public float getSampleRate() {
        return sampleRate;
    }

    public void setSampleRate(float sampleRate) {
        this.sampleRate = sampleRate;
    }

    public boolean isAlarmEnabled() {
        return alarmEnabled;
    }

    public void setAlarmEnabled(boolean alarmEnabled) {
        this.alarmEnabled = alarmEnabled;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 13 * hash + Objects.hashCode(this.existingDeviceId);
        hash = 13 * hash + Objects.hashCode(this.channelType);
        hash = 13 * hash + this.channelNum;
        hash = 13 * hash + Objects.hashCode(this.customerAccount);
        hash = 13 * hash + Objects.hashCode(this.siteId);
        hash = 13 * hash + Objects.hashCode(this.areaId);
        hash = 13 * hash + Objects.hashCode(this.assetId);
        hash = 13 * hash + Objects.hashCode(this.pointLocationId);
        hash = 13 * hash + Objects.hashCode(this.pointId);
        hash = 13 * hash + Objects.hashCode(this.alSetId);
        hash = 13 * hash + Objects.hashCode(this.apSetId);
        hash = 13 * hash + Objects.hashCode(this.pointName);
        hash = 13 * hash + Objects.hashCode(this.paramName);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final HwUnitVO other = (HwUnitVO) obj;
        if (this.channelNum != other.channelNum) {
            return false;
        }
        if (this.disabled != other.disabled) {
            return false;
        }
        if (this.sampleInterval != other.sampleInterval) {
            return false;
        }
        if (this.sensorChannelNum != other.sensorChannelNum) {
            return false;
        }
        if (Float.floatToIntBits(this.sensorOffset) != Float.floatToIntBits(other.sensorOffset)) {
            return false;
        }
        if (Float.floatToIntBits(this.sensorSensitivity) != Float.floatToIntBits(other.sensorSensitivity)) {
            return false;
        }
        if (this.useCustomLimits != other.useCustomLimits) {
            return false;
        }
        if (this.demodEnabled != other.demodEnabled) {
            return false;
        }
        if (Float.floatToIntBits(this.demodHighPass) != Float.floatToIntBits(other.demodHighPass)) {
            return false;
        }
        if (Float.floatToIntBits(this.demodLowPass) != Float.floatToIntBits(other.demodLowPass)) {
            return false;
        }
        if (this.dwell != other.dwell) {
            return false;
        }
        if (this.fMax != other.fMax) {
            return false;
        }
        if (Float.floatToIntBits(this.highAlert) != Float.floatToIntBits(other.highAlert)) {
            return false;
        }
        if (this.highAlertActive != other.highAlertActive) {
            return false;
        }
        if (Float.floatToIntBits(this.highFault) != Float.floatToIntBits(other.highFault)) {
            return false;
        }
        if (this.highFaultActive != other.highFaultActive) {
            return false;
        }
        if (this.customized != other.customized) {
            return false;
        }
        if (Float.floatToIntBits(this.lowAlert) != Float.floatToIntBits(other.lowAlert)) {
            return false;
        }
        if (this.lowAlertActive != other.lowAlertActive) {
            return false;
        }
        if (Float.floatToIntBits(this.lowFault) != Float.floatToIntBits(other.lowFault)) {
            return false;
        }
        if (this.lowFaultActive != other.lowFaultActive) {
            return false;
        }
        if (Float.floatToIntBits(this.maxFreq) != Float.floatToIntBits(other.maxFreq)) {
            return false;
        }
        if (Float.floatToIntBits(this.minFreq) != Float.floatToIntBits(other.minFreq)) {
            return false;
        }
        if (Float.floatToIntBits(this.period) != Float.floatToIntBits(other.period)) {
            return false;
        }
        if (this.resolution != other.resolution) {
            return false;
        }
        if (Float.floatToIntBits(this.sampleRate) != Float.floatToIntBits(other.sampleRate)) {
            return false;
        }
        if (this.alarmEnabled != other.alarmEnabled) {
            return false;
        }
        if (!Objects.equals(this.existingDeviceId, other.existingDeviceId)) {
            return false;
        }
        if (!Objects.equals(this.channelType, other.channelType)) {
            return false;
        }
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.pointName, other.pointName)) {
            return false;
        }
        if (!Objects.equals(this.pointType, other.pointType)) {
            return false;
        }
        if (!Objects.equals(this.sensorType, other.sensorType)) {
            return false;
        }
        if (!Objects.equals(this.sensorUnits, other.sensorUnits)) {
            return false;
        }
        if (!Objects.equals(this.paramName, other.paramName)) {
            return false;
        }
        if (!Objects.equals(this.freqUnits, other.freqUnits)) {
            return false;
        }
        if (!Objects.equals(this.paramAmpFactor, other.paramAmpFactor)) {
            return false;
        }
        if (!Objects.equals(this.paramType, other.paramType)) {
            return false;
        }
        if (!Objects.equals(this.paramUnits, other.paramUnits)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        if (!Objects.equals(this.areaId, other.areaId)) {
            return false;
        }
        if (!Objects.equals(this.assetId, other.assetId)) {
            return false;
        }
        if (!Objects.equals(this.pointLocationId, other.pointLocationId)) {
            return false;
        }
        if (!Objects.equals(this.pointId, other.pointId)) {
            return false;
        }
        if (!Objects.equals(this.alSetId, other.alSetId)) {
            return false;
        }
        if (!Objects.equals(this.apSetId, other.apSetId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "HwUnitVO{" + "existingDeviceId=" + existingDeviceId + ", newDeviceId=" + newDeviceId + ", channelType=" + channelType + ", channelNum=" + channelNum + ", customerAccount=" + customerAccount + ", siteId=" + siteId + ", areaId=" + areaId + ", assetId=" + assetId + ", pointLocationId=" + pointLocationId + ", pointId=" + pointId + ", alSetId=" + alSetId + ", apSetId=" + apSetId + ", pointName=" + pointName + ", paramName=" + paramName + ", disabled=" + disabled + ", pointType=" + pointType + ", sampleInterval=" + sampleInterval + ", sensorChannelNum=" + sensorChannelNum + ", sensorOffset=" + sensorOffset + ", sensorSensitivity=" + sensorSensitivity + ", sensorType=" + sensorType + ", sensorUnits=" + sensorUnits + ", useCustomLimits=" + useCustomLimits + ", demodEnabled=" + demodEnabled + ", demodHighPass=" + demodHighPass + ", demodLowPass=" + demodLowPass + ", dwell=" + dwell + ", fMax=" + fMax + ", freqUnits=" + freqUnits + ", highAlert=" + highAlert + ", highAlertActive=" + highAlertActive + ", highFault=" + highFault + ", highFaultActive=" + highFaultActive + ", customized=" + customized + ", lowAlert=" + lowAlert + ", lowAlertActive=" + lowAlertActive + ", lowFault=" + lowFault + ", lowFaultActive=" + lowFaultActive + ", maxFreq=" + maxFreq + ", minFreq=" + minFreq + ", paramAmpFactor=" + paramAmpFactor + ", paramType=" + paramType + ", paramUnits=" + paramUnits + ", period=" + period + ", resolution=" + resolution + ", sampleRate=" + sampleRate + ", alarmEnabled=" + alarmEnabled + '}';
    }

}

