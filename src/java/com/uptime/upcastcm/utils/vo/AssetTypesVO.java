/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.upcastcm.utils.vo;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author venka
 */
public class AssetTypesVO implements Serializable {

    private String customerAccount;
    private String assetTypeName;
    private String description;

    public AssetTypesVO() {
    }

    public AssetTypesVO(AssetTypesVO assetTypesVO) {
        this.customerAccount = assetTypesVO.getCustomerAccount();
        this.assetTypeName = assetTypesVO.getAssetTypeName();
        this.description = assetTypesVO.getDescription();
    }

    public AssetTypesVO(String customerAccount, String assetTypeName, String description) {
        this.customerAccount = customerAccount;
        this.assetTypeName = assetTypeName;
        this.description = description;
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public String getAssetTypeName() {
        return assetTypeName;
    }

    public void setAssetTypeName(String assetTypeName) {
        this.assetTypeName = assetTypeName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.customerAccount);
        hash = 53 * hash + Objects.hashCode(this.assetTypeName);
        hash = 53 * hash + Objects.hashCode(this.description);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AssetTypesVO other = (AssetTypesVO) obj;
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.assetTypeName, other.assetTypeName)) {
            return false;
        }
        if (!Objects.equals(this.description, other.description)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "AssetTypesVO{" + "customerAccount=" + customerAccount + ", assetTypeName=" + assetTypeName + ", description=" + description + '}';
    }

}
