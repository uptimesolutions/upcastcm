/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.utils.vo;

import java.io.Serializable;
import java.time.Instant;

/**
 *
 * @author venka
 */
public class DeviceStatusVO implements Serializable{
    private String deviceId;
    private String customerAcct;
    private String siteName;
    private String areaName;
    private String assetName;
    private String pointLocationName;
     private Instant lastSample;
    private float batteryVoltage;
    private boolean disabled;
    
    public DeviceStatusVO() {

    }

    public DeviceStatusVO(String deviceId, String customerAcct, String siteName, String areaName, String assetName, String pointLocationName, Instant lastSample, float batteryVoltage, boolean disabled) {
        this.deviceId = deviceId;
        this.customerAcct = customerAcct;
        this.siteName = siteName;
        this.areaName = areaName;
        this.assetName = assetName;
        this.pointLocationName = pointLocationName;
        this.lastSample = lastSample;
        this.batteryVoltage = batteryVoltage;
        this.disabled = disabled;
    }

    public DeviceStatusVO(DeviceStatusVO deviceStatusVO) {
        customerAcct = deviceStatusVO.getCustomerAcct();
        deviceId = deviceStatusVO.getDeviceId();
        siteName = deviceStatusVO.getSiteName();
        areaName = deviceStatusVO.getAreaName();
        assetName = deviceStatusVO.getAssetName();
        lastSample = deviceStatusVO.getLastSample();
        pointLocationName = deviceStatusVO.getPointLocationName();
        lastSample = deviceStatusVO.getLastSample();
        batteryVoltage = deviceStatusVO.getBatteryVoltage();
        disabled = deviceStatusVO.isDisabled();
        
        
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getCustomerAcct() {
        return customerAcct;
    }

    public void setCustomerAcct(String customerAcct) {
        this.customerAcct = customerAcct;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getAssetName() {
        return assetName;
    }

    public void setAssetName(String assetName) {
        this.assetName = assetName;
    }

    public String getPointLocationName() {
        return pointLocationName;
    }

    public void setPointLocationName(String pointLocationName) {
        this.pointLocationName = pointLocationName;
    }

    public Instant getLastSample() {
        return lastSample;
    }

    public void setLastSample(Instant lastSample) {
        this.lastSample = lastSample;
    }
    
    public float getBatteryVoltage() {
        return batteryVoltage;
    }

    public void setBatteryVoltage(float batteryVoltage) {
        this.batteryVoltage = batteryVoltage;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

   
    
}
