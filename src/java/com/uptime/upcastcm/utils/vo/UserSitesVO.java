/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.utils.vo;


import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author madhavi
 */

public class UserSitesVO implements Serializable {
    private String userId;
    private String customerAccount;
    private UUID siteId;
    private String siteName;
    private String siteRole;

    public UserSitesVO() {
    }

    public UserSitesVO(UserSitesVO userSitesVO) {
        userId = userSitesVO.getUserId();
        customerAccount = userSitesVO.getCustomerAccount();
        siteId = userSitesVO.getSiteId();
        siteName = userSitesVO.getSiteName();
        siteRole = userSitesVO.getSiteRole();
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public String getSiteRole() {
        return siteRole;
    }

    public void setSiteRole(String siteRole) {
        this.siteRole = siteRole;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 17 * hash + Objects.hashCode(this.userId);
        hash = 17 * hash + Objects.hashCode(this.customerAccount);
        hash = 17 * hash + Objects.hashCode(this.siteId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UserSitesVO other = (UserSitesVO) obj;
        if (!Objects.equals(this.userId, other.userId)) {
            return false;
        }
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.siteName, other.siteName)) {
            return false;
        }
        if (!Objects.equals(this.siteRole, other.siteRole)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "UserSitesVO{" + "userId=" + userId + ", customerAccount=" + customerAccount + ", siteId=" + siteId + ", siteName=" + siteName + ", siteRole=" + siteRole + '}';
    }
}
