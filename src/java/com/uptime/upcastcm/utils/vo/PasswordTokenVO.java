/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.utils.vo;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author twilcox
 */
public class PasswordTokenVO implements Serializable {
    private String userId;
    private UUID passwordToken;

    /**
     * Constructor
     */
    public PasswordTokenVO() {
    }
    
    /**
     * Constructor
     * @param userId, String Object
     */
    public PasswordTokenVO(String userId) {
        this.userId = userId;
    }

    /**
     * Constructor
     * @param userId, String Object
     * @param passwordToken, UUID Object 
     */
    public PasswordTokenVO(String userId, UUID passwordToken) {
        this.userId = userId;
        this.passwordToken = passwordToken;
    }

    /**
     * Constructor
     * @param passwordTokenVO, PasswordTokenVO Object 
     */
    public PasswordTokenVO(PasswordTokenVO passwordTokenVO) {
        userId = passwordTokenVO.getUserId();
        passwordToken = passwordTokenVO.getPasswordToken();
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public UUID getPasswordToken() {
        return passwordToken;
    }

    public void setPasswordToken(UUID passwordToken) {
        this.passwordToken = passwordToken;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + Objects.hashCode(this.userId);
        hash = 29 * hash + Objects.hashCode(this.passwordToken);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PasswordTokenVO other = (PasswordTokenVO) obj;
        if (!Objects.equals(this.userId, other.userId)) {
            return false;
        }
        if (!Objects.equals(this.passwordToken, other.passwordToken)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "PasswordTokenVO{" + "userId=" + userId + ", passwordToken=" + passwordToken + '}';
    }
}
