/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.utils.vo;

import java.io.Serializable;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

/**
 *
 * @author aswani
 */
public class AlarmNotificationVO implements Serializable {
    private String userId;
    private UUID siteId;
    private UUID assetId;
    private String emailAddress;
    private boolean alert;
    private boolean fault;
    private Set<UUID> createAssetIds;
    private Set<UUID> deleteAssetIds;

    public AlarmNotificationVO() {
    }

    public AlarmNotificationVO(String userId, UUID siteId, UUID assetId, String emailAddress, boolean alert, boolean fault, Set<UUID> createAssetIds, Set<UUID> deleteAssetIds) {
        this.userId = userId;
        this.siteId = siteId;
        this.assetId = assetId;
        this.emailAddress = emailAddress;
        this.alert = alert;
        this.fault = fault;
        this.createAssetIds = createAssetIds;
        this.deleteAssetIds = deleteAssetIds;
    }

    public AlarmNotificationVO(AlarmNotificationVO alarmNotificationVO) {
        userId = alarmNotificationVO.getUserId();
        siteId = alarmNotificationVO.getSiteId();
        assetId = alarmNotificationVO.getAssetId();
        emailAddress = alarmNotificationVO.getEmailAddress();
        alert = alarmNotificationVO.isAlert();
        fault = alarmNotificationVO.isFault();
        createAssetIds = alarmNotificationVO.getCreateAssetIds();
        deleteAssetIds = alarmNotificationVO.getDeleteAssetIds();
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public UUID getAssetId() {
        return assetId;
    }

    public void setAssetId(UUID assetId) {
        this.assetId = assetId;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public boolean isAlert() {
        return alert;
    }

    public void setAlert(boolean alert) {
        this.alert = alert;
    }

    public boolean isFault() {
        return fault;
    }

    public void setFault(boolean fault) {
        this.fault = fault;
    }

    public Set<UUID> getCreateAssetIds() {
        return createAssetIds;
    }

    public void setCreateAssetIds(Set<UUID> createAssetIds) {
        this.createAssetIds = createAssetIds;
    }

    public Set<UUID> getDeleteAssetIds() {
        return deleteAssetIds;
    }

    public void setDeleteAssetIds(Set<UUID> deleteAssetIds) {
        this.deleteAssetIds = deleteAssetIds;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.userId);
        hash = 59 * hash + Objects.hashCode(this.siteId);
        hash = 59 * hash + Objects.hashCode(this.assetId);
        hash = 59 * hash + Objects.hashCode(this.emailAddress);
        hash = 59 * hash + (this.alert ? 1 : 0);
        hash = 59 * hash + (this.fault ? 1 : 0);
        hash = 59 * hash + Objects.hashCode(this.createAssetIds);
        hash = 59 * hash + Objects.hashCode(this.deleteAssetIds);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AlarmNotificationVO other = (AlarmNotificationVO) obj;
        if (this.alert != other.alert) {
            return false;
        }
        if (this.fault != other.fault) {
            return false;
        }
        if (!Objects.equals(this.userId, other.userId)) {
            return false;
        }
        if (!Objects.equals(this.emailAddress, other.emailAddress)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        if (!Objects.equals(this.assetId, other.assetId)) {
            return false;
        }
        if (!Objects.equals(this.createAssetIds, other.createAssetIds)) {
            return false;
        }
        if (!Objects.equals(this.deleteAssetIds, other.deleteAssetIds)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "AlarmNotificationVO{" + "userId=" + userId + ", siteId=" + siteId + ", assetId=" + assetId + ", emailAddress=" + emailAddress + ", alert=" + alert + ", fault=" + fault + ", createAssetIds=" + createAssetIds + ", deleteAssetIds=" + deleteAssetIds + '}';
    }
}
