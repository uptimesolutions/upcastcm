/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.upcastcm.utils.vo;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author joseph
 */
public class AssetPresetVO implements Serializable {

    private String customerAccount;
    private UUID siteId;
    private String siteName;
    private UUID assetTypeId;
    private String assetTypeName;
    private int sampleInterval;
    private String description;
    private List<AssetPresetPointLocationVO> assetPresetPointLocationVOList;
    private String type;
    private UUID assetPresetId;
    private String pointLocationName;
    private String assetPresetName;
    private UUID devicePresetId;
    private String ffSetIds;
    private Float rollDiameter;
    private String rollDiameterUnits;
    private Float speedRatio;
    private UUID tachId;
    private String ffSetNames;
    private String devicePresetType;
    private String devicePresetName;
    private String tachName;
    private String rollDiameterText;
    private String speedRatioText;

    public AssetPresetVO() {

    }

    public AssetPresetVO(String customerAccount, UUID siteId, String siteName, String assetTemplateName, UUID assetTypeId, String assetTypeName, int sampleInterval, String description, List<AssetPresetPointLocationVO> assetPresetPointLocationVOList, String type, UUID assetPresetId, String pointLocationName, String assetPresetName, UUID devicePresetId, String ffSetIds, Float rollDiameter, String rollDiameterUnits, Float speedRatio, UUID tachId, String ffSetNames, String devicePresetType, String devicePresetName, String tachName) {
        this.customerAccount = customerAccount;
        this.siteId = siteId;
        this.siteName = siteName;
        this.assetTypeId = assetTypeId;
        this.assetTypeName = assetTypeName;
        this.sampleInterval = sampleInterval;
        this.description = description;
        this.assetPresetPointLocationVOList = assetPresetPointLocationVOList;
        this.type = type;
        this.assetPresetId = assetPresetId;
        this.pointLocationName = pointLocationName;
        this.assetPresetName = assetPresetName;
        this.devicePresetId = devicePresetId;
        this.ffSetIds = ffSetIds;
        this.rollDiameter = rollDiameter;
        this.rollDiameterUnits = rollDiameterUnits;
        this.speedRatio = speedRatio;
        this.tachId = tachId;
        this.ffSetNames = ffSetNames;
        this.devicePresetType = devicePresetType;
        this.devicePresetName = devicePresetName;
        this.tachName = tachName;
    }

    public AssetPresetVO(AssetPresetVO assetPresetVO) {
        this.customerAccount = assetPresetVO.getCustomerAccount();
        this.siteId = assetPresetVO.getSiteId();
        this.siteName = assetPresetVO.getSiteName();
        this.assetTypeId = assetPresetVO.getAssetTypeId();
        this.assetTypeName = assetPresetVO.getAssetTypeName();
        this.sampleInterval = assetPresetVO.getSampleInterval();
        this.description = assetPresetVO.getDescription();
        this.assetPresetPointLocationVOList = assetPresetVO.getAssetPresetPointLocationVOList();
        this.type = assetPresetVO.getType();
        this.assetPresetId = assetPresetVO.getAssetPresetId();
        this.pointLocationName = assetPresetVO.getPointLocationName();
        this.assetPresetName = assetPresetVO.getAssetPresetName();
        this.devicePresetId = assetPresetVO.getDevicePresetId();
        this.ffSetIds = assetPresetVO.getFfSetIds();
        this.rollDiameter = assetPresetVO.getRollDiameter();
        this.rollDiameterUnits = assetPresetVO.getRollDiameterUnits();
        this.speedRatio = assetPresetVO.getSpeedRatio();
        this.tachId = assetPresetVO.getTachId();
        this.devicePresetType = assetPresetVO.getDevicePresetType();
        this.devicePresetName = assetPresetVO.getDevicePresetName();
        this.tachName = assetPresetVO.getTachName();
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public UUID getAssetTypeId() {
        return assetTypeId;
    }

    public void setAssetTypeId(UUID assetTypeId) {
        this.assetTypeId = assetTypeId;
    }

    public String getAssetTypeName() {
        return assetTypeName;
    }

    public void setAssetTypeName(String assetTypeName) {
        this.assetTypeName = assetTypeName;
    }

    public int getSampleInterval() {
        return sampleInterval;
    }

    public void setSampleInterval(int sampleInterval) {
        this.sampleInterval = sampleInterval;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<AssetPresetPointLocationVO> getAssetPresetPointLocationVOList() {
        return assetPresetPointLocationVOList;
    }

    public void setAssetPresetPointLocationVOList(List<AssetPresetPointLocationVO> assetPresetPointLocationVOList) {
        this.assetPresetPointLocationVOList = assetPresetPointLocationVOList;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public UUID getAssetPresetId() {
        return assetPresetId;
    }

    public void setAssetPresetId(UUID assetPresetId) {
        this.assetPresetId = assetPresetId;
    }

    public String getPointLocationName() {
        return pointLocationName;
    }

    public void setPointLocationName(String pointLocationName) {
        this.pointLocationName = pointLocationName;
    }

    public String getAssetPresetName() {
        return assetPresetName;
    }

    public void setAssetPresetName(String assetPresetName) {
        this.assetPresetName = assetPresetName;
    }

    public UUID getDevicePresetId() {
        return devicePresetId;
    }

    public void setDevicePresetId(UUID devicePresetId) {
        this.devicePresetId = devicePresetId;
    }

    public String getFfSetIds() {
        return ffSetIds;
    }

    public void setFfSetIds(String ffSetIds) {
        this.ffSetIds = ffSetIds;
    }

    public Float getRollDiameter() {
        return rollDiameter;
    }

    public void setRollDiameter(Float rollDiameter) {
        this.rollDiameter = rollDiameter;
    }

    public String getRollDiameterUnits() {
        return rollDiameterUnits;
    }

    public void setRollDiameterUnits(String rollDiameterUnits) {
        this.rollDiameterUnits = rollDiameterUnits;
    }

    public Float getSpeedRatio() {
        return speedRatio;
    }

    public void setSpeedRatio(Float speedRatio) {
        this.speedRatio = speedRatio;
    }

    public UUID getTachId() {
        return tachId;
    }

    public void setTachId(UUID tachId) {
        this.tachId = tachId;
    }

    public String getFfSetNames() {
        return ffSetNames;
    }

    public void setFfSetNames(String ffSetNames) {
        this.ffSetNames = ffSetNames;
    }

    public String getDevicePresetType() {
        return devicePresetType;
    }

    public void setDevicePresetType(String devicePresetType) {
        this.devicePresetType = devicePresetType;
    }

    public String getDevicePresetName() {
        return devicePresetName;
    }

    public void setDevicePresetName(String devicePresetName) {
        this.devicePresetName = devicePresetName;
    }

    public String getTachName() {
        return tachName;
    }

    public void setTachName(String tachName) {
        this.tachName = tachName;
    }

    public String getRollDiameterText() {
        return rollDiameterText;
    }

    public void setRollDiameterText(String rollDiameterText) {
        this.rollDiameterText = rollDiameterText;
    }

    public String getSpeedRatioText() {
        return speedRatioText;
    }

    public void setSpeedRatioText(String speedRatioText) {
        this.speedRatioText = speedRatioText;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 61 * hash + Objects.hashCode(this.customerAccount);
        hash = 61 * hash + Objects.hashCode(this.siteId);
        hash = 61 * hash + Objects.hashCode(this.siteName);
        hash = 61 * hash + Objects.hashCode(this.assetTypeId);
        hash = 61 * hash + Objects.hashCode(this.assetTypeName);
        hash = 61 * hash + this.sampleInterval;
        hash = 61 * hash + Objects.hashCode(this.description);
        hash = 61 * hash + Objects.hashCode(this.assetPresetPointLocationVOList);
        hash = 61 * hash + Objects.hashCode(this.type);
        hash = 61 * hash + Objects.hashCode(this.assetPresetId);
        hash = 61 * hash + Objects.hashCode(this.pointLocationName);
        hash = 61 * hash + Objects.hashCode(this.assetPresetName);
        hash = 61 * hash + Objects.hashCode(this.devicePresetId);
        hash = 61 * hash + Objects.hashCode(this.ffSetIds);
        hash = 61 * hash + Objects.hashCode(this.rollDiameter);
        hash = 61 * hash + Objects.hashCode(this.rollDiameterUnits);
        hash = 61 * hash + Objects.hashCode(this.speedRatio);
        hash = 61 * hash + Objects.hashCode(this.tachId);
        hash = 61 * hash + Objects.hashCode(this.ffSetNames);
        hash = 61 * hash + Objects.hashCode(this.devicePresetType);
        hash = 61 * hash + Objects.hashCode(this.devicePresetName);
        hash = 61 * hash + Objects.hashCode(this.tachName);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AssetPresetVO other = (AssetPresetVO) obj;
        if (this.sampleInterval != other.sampleInterval) {
            return false;
        }
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.siteName, other.siteName)) {
            return false;
        }
        if (!Objects.equals(this.assetTypeName, other.assetTypeName)) {
            return false;
        }
        if (!Objects.equals(this.description, other.description)) {
            return false;
        }
        if (!Objects.equals(this.type, other.type)) {
            return false;
        }
        if (!Objects.equals(this.pointLocationName, other.pointLocationName)) {
            return false;
        }
        if (!Objects.equals(this.assetPresetName, other.assetPresetName)) {
            return false;
        }
        if (!Objects.equals(this.ffSetIds, other.ffSetIds)) {
            return false;
        }
        if (!Objects.equals(this.rollDiameterUnits, other.rollDiameterUnits)) {
            return false;
        }
        if (!Objects.equals(this.ffSetNames, other.ffSetNames)) {
            return false;
        }
        if (!Objects.equals(this.devicePresetType, other.devicePresetType)) {
            return false;
        }
        if (!Objects.equals(this.devicePresetName, other.devicePresetName)) {
            return false;
        }
        if (!Objects.equals(this.tachName, other.tachName)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        if (!Objects.equals(this.assetTypeId, other.assetTypeId)) {
            return false;
        }
        if (!Objects.equals(this.assetPresetPointLocationVOList, other.assetPresetPointLocationVOList)) {
            return false;
        }
        if (!Objects.equals(this.assetPresetId, other.assetPresetId)) {
            return false;
        }
        if (!Objects.equals(this.devicePresetId, other.devicePresetId)) {
            return false;
        }
        if (!Objects.equals(this.rollDiameter, other.rollDiameter)) {
            return false;
        }
        if (!Objects.equals(this.speedRatio, other.speedRatio)) {
            return false;
        }
        if (!Objects.equals(this.tachId, other.tachId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "AssetPresetVO{" + "customerAccount=" + customerAccount + ", siteId=" + siteId + ", siteName=" + siteName + ", assetTypeId=" + assetTypeId + ", assetTypeName=" + assetTypeName + ", sampleInterval=" + sampleInterval + ", description=" + description + ", assetPresetPointLocationVOList=" + assetPresetPointLocationVOList + ", type=" + type + ", assetPresetId=" + assetPresetId + ", pointLocationName=" + pointLocationName + ", assetPresetName=" + assetPresetName + ", devicePresetId=" + devicePresetId + ", ffSetIds=" + ffSetIds + ", rollDiameter=" + rollDiameter + ", rollDiameterUnits=" + rollDiameterUnits + ", speedRatio=" + speedRatio + ", tachId=" + tachId + ", ffSetNames=" + ffSetNames + ", devicePresetType=" + devicePresetType + ", devicePresetName=" + devicePresetName + ", tachName=" + tachName + '}';
    }

}
