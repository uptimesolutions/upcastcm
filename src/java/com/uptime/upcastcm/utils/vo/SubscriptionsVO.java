/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.utils.vo;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author madhavi
 */
public class SubscriptionsVO implements Serializable{
    
    private String customerAccount;
    private UUID siteId;
    private String subscriptionType;
    private String mqProtocol;
    private String mqConnectString;
    private String mqUser;
    private String mqPwd;
    private String mqQueueName;
    private String mqClientId;
    private boolean internal;
    private String webhookUrl;
    private String requestType;
    
    public SubscriptionsVO() {
        
    }
    
    public SubscriptionsVO(SubscriptionsVO subscriptionsVO) {
        this.customerAccount = subscriptionsVO.getCustomerAccount();
        this.siteId = subscriptionsVO.getSiteId();
        this.subscriptionType = subscriptionsVO.getSubscriptionType();
        this.mqProtocol = subscriptionsVO.getMqProtocol();
        this.mqConnectString = subscriptionsVO.getMqConnectString();
        this.mqUser = subscriptionsVO.getMqUser();
        this.mqPwd = subscriptionsVO.getMqPwd();
        this.mqQueueName = subscriptionsVO.getMqQueueName();
        this.mqClientId = subscriptionsVO.getMqClientId();
        this.internal = subscriptionsVO.isInternal();
        this.webhookUrl = subscriptionsVO.getWebhookUrl();
    }
    
    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public String getSubscriptionType() {
        return subscriptionType;
    }

    public void setSubscriptionType(String subscriptionType) {
        this.subscriptionType = subscriptionType;
    }

    public String getMqProtocol() {
        return mqProtocol;
    }

    public void setMqProtocol(String mqProtocol) {
        this.mqProtocol = mqProtocol;
    }

    public String getMqConnectString() {
        return mqConnectString;
    }

    public void setMqConnectString(String mqConnectString) {
        this.mqConnectString = mqConnectString;
    }

    public String getMqUser() {
        return mqUser;
    }

    public void setMqUser(String mqUser) {
        this.mqUser = mqUser;
    }

    public String getMqPwd() {
        return mqPwd;
    }

    public void setMqPwd(String mqPwd) {
        this.mqPwd = mqPwd;
    }

    public String getMqQueueName() {
        return mqQueueName;
    }

    public void setMqQueueName(String mqQueueName) {
        this.mqQueueName = mqQueueName;
    }

    public String getMqClientId() {
        return mqClientId;
    }

    public void setMqClientId(String mqClientId) {
        this.mqClientId = mqClientId;
    }

    public boolean isInternal() {
        return internal;
    }

    public void setInternal(boolean internal) {
        this.internal = internal;
    }

    public String getWebhookUrl() {
        return webhookUrl;
    }

    public void setWebhookUrl(String webhookUrl) {
        this.webhookUrl = webhookUrl;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    @Override
    public String toString() {
        return "SubscriptionsVO{" + "customerAccount=" + customerAccount + ", siteId=" + siteId + ", subscriptionType=" + subscriptionType + ", mqProtocol=" + mqProtocol + ", mqConnectString=" + mqConnectString + ", mqUser=" + mqUser + ", mqPwd=" + mqPwd + ", mqQueueName=" + mqQueueName + ", mqClientId=" + mqClientId + ", internal=" + internal + ", webhookUrl=" + webhookUrl + ", requestType=" + requestType + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + Objects.hashCode(this.customerAccount);
        hash = 17 * hash + Objects.hashCode(this.siteId);
        hash = 17 * hash + Objects.hashCode(this.subscriptionType);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SubscriptionsVO other = (SubscriptionsVO) obj;
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.subscriptionType, other.subscriptionType)) {
            return false;
        }
        return Objects.equals(this.siteId, other.siteId);
    }

}
