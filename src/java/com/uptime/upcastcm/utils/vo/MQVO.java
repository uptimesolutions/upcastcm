/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.utils.vo;

import java.io.Serializable;
import java.util.Objects;


/**
 *
 * @author madhavi
 */
public class MQVO implements Serializable {

    private String customer_id;
    private String site_id;
    private String mq_connect_string;
    private String mq_user;
    private String mq_pwd;
    private String mq_queue_name;
    private String mq_client_id;
    private boolean use_internal;
    private String device_action;
    private String site_name;
    private String area;
    private String asset;
    private String point_location;
    private String device_id;
    private String old_device_id;
    private String new_device_id;

    public MQVO() {
    }

    public MQVO(String customer_id, String site_id, String mq_connect_string, String mq_user, String mq_pwd, String mq_queue_name, String mq_client_id, boolean use_internal, String device_action, String site_name, String area, String asset, String point_location, String device_id, String old_device_id, String new_device_id) {
        this.customer_id = customer_id;
        this.site_id = site_id;
        this.mq_connect_string = mq_connect_string;
        this.mq_user = mq_user;
        this.mq_pwd = mq_pwd;
        this.mq_queue_name = mq_queue_name;
        this.mq_client_id = mq_client_id;
        this.use_internal = use_internal;
        this.device_action = device_action;
        this.site_name = site_name;
        this.area = area;
        this.asset = asset;
        this.point_location = point_location;
        this.device_id = device_id;
        this.old_device_id = old_device_id;
        this.new_device_id = new_device_id;
    }

    public MQVO(MQVO mqVO) {
        customer_id = mqVO.getCustomer_id();
        site_id = mqVO.getSite_id();
        mq_connect_string = mqVO.getMq_connect_string();
        mq_user = mqVO.getMq_user();
        mq_pwd = mqVO.getMq_pwd();
        mq_queue_name = mqVO.getMq_queue_name();
        mq_client_id = mqVO.getMq_client_id();
        use_internal = mqVO.isUse_internal();
        device_action = mqVO.getDevice_action();
        site_name = mqVO.getSite_name();
        area = mqVO.getArea();
        asset = mqVO.getAsset();
        point_location = mqVO.getPoint_location();
        device_id = mqVO.getDevice_id();
        old_device_id = mqVO.getOld_device_id();
        new_device_id = mqVO.getNew_device_id();
    }

    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }

    public String getSite_id() {
        return site_id;
    }

    public void setSite_id(String site_id) {
        this.site_id = site_id;
    }

    public String getMq_connect_string() {
        return mq_connect_string;
    }

    public void setMq_connect_string(String mq_connect_string) {
        this.mq_connect_string = mq_connect_string;
    }

    public String getMq_user() {
        return mq_user;
    }

    public void setMq_user(String mq_user) {
        this.mq_user = mq_user;
    }

    public String getMq_pwd() {
        return mq_pwd;
    }

    public void setMq_pwd(String mq_pwd) {
        this.mq_pwd = mq_pwd;
    }

    public String getMq_queue_name() {
        return mq_queue_name;
    }

    public void setMq_queue_name(String mq_queue_name) {
        this.mq_queue_name = mq_queue_name;
    }

    public String getMq_client_id() {
        return mq_client_id;
    }

    public void setMq_client_id(String mq_client_id) {
        this.mq_client_id = mq_client_id;
    }

    public boolean isUse_internal() {
        return use_internal;
    }

    public void setUse_internal(boolean use_internal) {
        this.use_internal = use_internal;
    }

    public String getDevice_action() {
        return device_action;
    }

    public void setDevice_action(String device_action) {
        this.device_action = device_action;
    }

    public String getSite_name() {
        return site_name;
    }

    public void setSite_name(String site_name) {
        this.site_name = site_name;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getAsset() {
        return asset;
    }

    public void setAsset(String asset) {
        this.asset = asset;
    }

    public String getPoint_location() {
        return point_location;
    }

    public void setPoint_location(String point_location) {
        this.point_location = point_location;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public String getOld_device_id() {
        return old_device_id;
    }

    public void setOld_device_id(String old_device_id) {
        this.old_device_id = old_device_id;
    }

    public String getNew_device_id() {
        return new_device_id;
    }

    public void setNew_device_id(String new_device_id) {
        this.new_device_id = new_device_id;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + Objects.hashCode(this.customer_id);
        hash = 53 * hash + Objects.hashCode(this.site_id);
        hash = 53 * hash + Objects.hashCode(this.mq_connect_string);
        hash = 53 * hash + Objects.hashCode(this.mq_user);
        hash = 53 * hash + Objects.hashCode(this.mq_pwd);
        hash = 53 * hash + Objects.hashCode(this.mq_queue_name);
        hash = 53 * hash + Objects.hashCode(this.mq_client_id);
        hash = 53 * hash + (this.use_internal ? 1 : 0);
        hash = 53 * hash + Objects.hashCode(this.device_action);
        hash = 53 * hash + Objects.hashCode(this.site_name);
        hash = 53 * hash + Objects.hashCode(this.area);
        hash = 53 * hash + Objects.hashCode(this.asset);
        hash = 53 * hash + Objects.hashCode(this.point_location);
        hash = 53 * hash + Objects.hashCode(this.device_id);
        hash = 53 * hash + Objects.hashCode(this.old_device_id);
        hash = 53 * hash + Objects.hashCode(this.new_device_id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MQVO other = (MQVO) obj;
        if (this.use_internal != other.use_internal) {
            return false;
        }
        if (!Objects.equals(this.customer_id, other.customer_id)) {
            return false;
        }
        if (!Objects.equals(this.site_id, other.site_id)) {
            return false;
        }
        if (!Objects.equals(this.mq_connect_string, other.mq_connect_string)) {
            return false;
        }
        if (!Objects.equals(this.mq_user, other.mq_user)) {
            return false;
        }
        if (!Objects.equals(this.mq_pwd, other.mq_pwd)) {
            return false;
        }
        if (!Objects.equals(this.mq_queue_name, other.mq_queue_name)) {
            return false;
        }
        if (!Objects.equals(this.mq_client_id, other.mq_client_id)) {
            return false;
        }
        if (!Objects.equals(this.device_action, other.device_action)) {
            return false;
        }
        if (!Objects.equals(this.site_name, other.site_name)) {
            return false;
        }
        if (!Objects.equals(this.area, other.area)) {
            return false;
        }
        if (!Objects.equals(this.asset, other.asset)) {
            return false;
        }
        if (!Objects.equals(this.point_location, other.point_location)) {
            return false;
        }
        if (!Objects.equals(this.device_id, other.device_id)) {
            return false;
        }
        if (!Objects.equals(this.old_device_id, other.old_device_id)) {
            return false;
        }
        if (!Objects.equals(this.new_device_id, other.new_device_id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "MQVO{" + "customer_id=" + customer_id + ", site_id=" + site_id + ", mq_connect_string=" + mq_connect_string + ", mq_user=" + mq_user + ", mq_pwd=" + mq_pwd + ", mq_queue_name=" + mq_queue_name + ", mq_client_id=" + mq_client_id + ", use_internal=" + use_internal + ", device_action=" + device_action + ", site_name=" + site_name + ", area=" + area + ", asset=" + asset + ", point_location=" + point_location + ", device_id=" + device_id + ", old_device_id=" + old_device_id + ", new_device_id=" + new_device_id + '}';
    }
}
