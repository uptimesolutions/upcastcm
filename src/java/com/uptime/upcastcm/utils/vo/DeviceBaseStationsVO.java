/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.utils.vo;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author kpati
 */
public class DeviceBaseStationsVO implements Serializable {
    
    private String customerAccount;
    private UUID siteId;
    private String deviceId;
    private short baseStationPort;
    private boolean disabled;
    
    public DeviceBaseStationsVO(){
        
    }
    
    public DeviceBaseStationsVO(DeviceBaseStationsVO deviceBaseStationsVO){
        this.customerAccount = deviceBaseStationsVO.getCustomerAccount();
        this.siteId = deviceBaseStationsVO.getSiteId();
        this.deviceId = deviceBaseStationsVO.getDeviceId();
        this.disabled = deviceBaseStationsVO.isDisabled();
        
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public short getBaseStationPort() {
        return baseStationPort;
    }

    public void setBaseStationPort(short baseStationPort) {
        this.baseStationPort = baseStationPort;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 53 * hash + Objects.hashCode(this.customerAccount);
        hash = 53 * hash + Objects.hashCode(this.siteId);
        hash = 53 * hash + Objects.hashCode(this.deviceId);
        hash = 53 * hash + this.baseStationPort;
        hash = 53 * hash + (this.disabled ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DeviceBaseStationsVO other = (DeviceBaseStationsVO) obj;
        if (this.baseStationPort != other.baseStationPort) {
            return false;
        }
        if (this.disabled != other.disabled) {
            return false;
        }
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.deviceId, other.deviceId)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "DeviceBaseStationsVO{" + "customerAccount=" + customerAccount + ", siteId=" + siteId + ", deviceId=" + deviceId + ", baseStationPort=" + baseStationPort + ", disabled=" + disabled + '}';
    }
    
}
