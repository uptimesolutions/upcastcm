/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.utils.vo;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author kpati
 */
public class DeviceVO implements Serializable {
    
    private String customerAccount;
    private UUID siteId;
    private UUID areaId;
    private UUID assetId;
    private UUID pointLocationId;
    private UUID pointId;
    private UUID alSetId;
    private UUID apSetId;
    private String pointName;
    private String paramName;
    private int channelNum;
    private String channelNumLabel;
    private String channelType;
    private String restrictions;
    private String deviceType;
    private List<PointVO> pointList;
    private String existingDeviceId;
    private String newDeviceId;
    private String siteName;
    private String areaName;
    private String assetName;
    private String pointLocationName;
    private boolean dbpointListExists;
    private short basestationPortNum;
    private int index;
    
    public DeviceVO(){
        
    }
    
    public DeviceVO(int channelNum, String channelNumLabel, String channelType, String restrictions){
        this.channelNum = channelNum;
        this.channelNumLabel = channelNumLabel;
        this.channelType = channelType;
        this.restrictions = restrictions;
        
    }

    public DeviceVO(String customerAccount, UUID siteId, UUID areaId, UUID assetId, UUID pointLocationId, UUID pointId, UUID alSetId, UUID apSetId, String pointName, String paramName, int channelNum, String channelNumLabel, String channelType, String restrictions, List<PointVO> pointList, String existingDeviceId, String newDeviceId, String siteName, String areaName, String assetName, String pointLocationName, boolean dbpointListExists, short basestationPortNum, int index, String deviceType) {
        this.customerAccount = customerAccount;
        this.siteId = siteId;
        this.areaId = areaId;
        this.assetId = assetId;
        this.pointLocationId = pointLocationId;
        this.pointId = pointId;
        this.alSetId = alSetId;
        this.apSetId = apSetId;
        this.pointName = pointName;
        this.paramName = paramName;
        this.channelNum = channelNum;
        this.channelNumLabel = channelNumLabel;
        this.channelType = channelType;
        this.restrictions = restrictions;
        this.pointList = pointList;
        this.existingDeviceId = existingDeviceId;
        this.newDeviceId = newDeviceId;
        this.siteName = siteName;
        this.areaName = areaName;
        this.assetName = assetName;
        this.pointLocationName = pointLocationName;
        this.dbpointListExists = dbpointListExists;
        this.basestationPortNum = basestationPortNum;
        this.index = index;
        this.deviceType = deviceType;
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public UUID getAreaId() {
        return areaId;
    }

    public void setAreaId(UUID areaId) {
        this.areaId = areaId;
    }

    public UUID getAssetId() {
        return assetId;
    }

    public void setAssetId(UUID assetId) {
        this.assetId = assetId;
    }

    public UUID getPointLocationId() {
        return pointLocationId;
    }

    public void setPointLocationId(UUID pointLocationId) {
        this.pointLocationId = pointLocationId;
    }

    public UUID getPointId() {
        return pointId;
    }

    public void setPointId(UUID pointId) {
        this.pointId = pointId;
    }

    public UUID getAlSetId() {
        return alSetId;
    }

    public void setAlSetId(UUID alSetId) {
        this.alSetId = alSetId;
    }

    public UUID getApSetId() {
        return apSetId;
    }

    public void setApSetId(UUID apSetId) {
        this.apSetId = apSetId;
    }

    public String getPointName() {
        return pointName;
    }

    public void setPointName(String pointName) {
        this.pointName = pointName;
    }

    public String getParamName() {
        return paramName;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    public int getChannelNum() {
        return channelNum;
    }

    public void setChannelNum(int channelNum) {
        this.channelNum = channelNum;
    }

    public String getChannelNumLabel() {
        return channelNumLabel;
    }

    public void setChannelNumLabel(String channelNumLabel) {
        this.channelNumLabel = channelNumLabel;
    }

    public String getChannelType() {
        return channelType;
    }

    public void setChannelType(String channelType) {
        this.channelType = channelType;
    }

    public String getRestrictions() {
        return restrictions;
    }

    public void setRestrictions(String restrictions) {
        this.restrictions = restrictions;
    }

    public List<PointVO> getPointList() {
        return pointList;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }
    
    public void setPointList(List<PointVO> pointList) {
        this.pointList = pointList;
    }

    public String getExistingDeviceId() {
        return existingDeviceId;
    }

    public void setExistingDeviceId(String existingDeviceId) {
        this.existingDeviceId = existingDeviceId;
    }

    public String getNewDeviceId() {
        return newDeviceId;
    }

    public void setNewDeviceId(String newDeviceId) {
        this.newDeviceId = newDeviceId;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getAssetName() {
        return assetName;
    }

    public void setAssetName(String assetName) {
        this.assetName = assetName;
    }

    public String getPointLocationName() {
        return pointLocationName;
    }

    public void setPointLocationName(String pointLocationName) {
        this.pointLocationName = pointLocationName;
    }

    public boolean isDbpointListExists() {
        return dbpointListExists;
    }

    public void setDbpointListExists(boolean dbpointListExists) {
        this.dbpointListExists = dbpointListExists;
    }

    public short getBasestationPortNum() {
        return basestationPortNum;
    }

    public void setBasestationPortNum(short basestationPortNum) {
        this.basestationPortNum = basestationPortNum;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 53 * hash + Objects.hashCode(this.customerAccount);
        hash = 53 * hash + Objects.hashCode(this.siteId);
        hash = 53 * hash + Objects.hashCode(this.areaId);
        hash = 53 * hash + Objects.hashCode(this.assetId);
        hash = 53 * hash + Objects.hashCode(this.pointLocationId);
        hash = 53 * hash + Objects.hashCode(this.pointId);
        hash = 53 * hash + Objects.hashCode(this.alSetId);
        hash = 53 * hash + Objects.hashCode(this.apSetId);
        hash = 53 * hash + Objects.hashCode(this.pointName);
        hash = 53 * hash + Objects.hashCode(this.paramName);
        hash = 53 * hash + this.channelNum;
        hash = 53 * hash + Objects.hashCode(this.channelNumLabel);
        hash = 53 * hash + Objects.hashCode(this.channelType);
        hash = 53 * hash + Objects.hashCode(this.restrictions);
        hash = 53 * hash + Objects.hashCode(this.pointList);
        hash = 53 * hash + Objects.hashCode(this.existingDeviceId);
        hash = 53 * hash + Objects.hashCode(this.newDeviceId);
        hash = 53 * hash + Objects.hashCode(this.siteName);
        hash = 53 * hash + Objects.hashCode(this.areaName);
        hash = 53 * hash + Objects.hashCode(this.assetName);
        hash = 53 * hash + Objects.hashCode(this.pointLocationName);
        hash = 53 * hash + (this.dbpointListExists ? 1 : 0);
        hash = 53 * hash + this.basestationPortNum;
        hash = 53 * hash + this.index;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DeviceVO other = (DeviceVO) obj;
        if (this.channelNum != other.channelNum) {
            return false;
        }
        if (this.dbpointListExists != other.dbpointListExists) {
            return false;
        }
        if (this.basestationPortNum != other.basestationPortNum) {
            return false;
        }
        if (this.index != other.index) {
            return false;
        }
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.pointName, other.pointName)) {
            return false;
        }
        if (!Objects.equals(this.paramName, other.paramName)) {
            return false;
        }
        if (!Objects.equals(this.channelNumLabel, other.channelNumLabel)) {
            return false;
        }
        if (!Objects.equals(this.channelType, other.channelType)) {
            return false;
        }
        if (!Objects.equals(this.restrictions, other.restrictions)) {
            return false;
        }
        if (!Objects.equals(this.existingDeviceId, other.existingDeviceId)) {
            return false;
        }
        if (!Objects.equals(this.newDeviceId, other.newDeviceId)) {
            return false;
        }
        if (!Objects.equals(this.siteName, other.siteName)) {
            return false;
        }
        if (!Objects.equals(this.areaName, other.areaName)) {
            return false;
        }
        if (!Objects.equals(this.assetName, other.assetName)) {
            return false;
        }
        if (!Objects.equals(this.pointLocationName, other.pointLocationName)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        if (!Objects.equals(this.areaId, other.areaId)) {
            return false;
        }
        if (!Objects.equals(this.assetId, other.assetId)) {
            return false;
        }
        if (!Objects.equals(this.pointLocationId, other.pointLocationId)) {
            return false;
        }
        if (!Objects.equals(this.pointId, other.pointId)) {
            return false;
        }
        if (!Objects.equals(this.alSetId, other.alSetId)) {
            return false;
        }
        if (!Objects.equals(this.apSetId, other.apSetId)) {
            return false;
        }
        if (!Objects.equals(this.pointList, other.pointList)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "DeviceVO{" + "customerAccount=" + customerAccount + ", siteId=" + siteId + ", areaId=" + areaId + ", assetId=" + assetId + ", pointLocationId=" + pointLocationId + ", pointId=" + pointId + ", alSetId=" + alSetId + ", apSetId=" + apSetId + ", pointName=" + pointName + ", paramName=" + paramName + ", channelNum=" + channelNum + ", channelNumLabel=" + channelNumLabel + ", channelType=" + channelType + ", restrictions=" + restrictions + ", deviceType=" + deviceType + ", pointList=" + pointList + ", existingDeviceId=" + existingDeviceId + ", newDeviceId=" + newDeviceId + ", siteName=" + siteName + ", areaName=" + areaName + ", assetName=" + assetName + ", pointLocationName=" + pointLocationName + ", dbpointListExists=" + dbpointListExists + ", basestationPortNum=" + basestationPortNum + ", index=" + index + '}';
    }
}
