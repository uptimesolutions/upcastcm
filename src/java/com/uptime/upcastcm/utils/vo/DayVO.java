/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.upcastcm.utils.vo;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author gsingh
 */
public class DayVO implements Serializable {

    String label;
    String day;
    List<String> selectedHrsList;
    String type;

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public List<String> getSelectedHrsList() {
        return selectedHrsList;
    }

    public void setSelectedHrsList(List<String> selectedHrsList) {
        this.selectedHrsList = selectedHrsList;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 67 * hash + Objects.hashCode(this.label);
        hash = 67 * hash + Objects.hashCode(this.day);
        hash = 67 * hash + Objects.hashCode(this.selectedHrsList);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DayVO other = (DayVO) obj;
        if (!Objects.equals(this.label, other.label)) {
            return false;
        }
        if (!Objects.equals(this.day, other.day)) {
            return false;
        }
        if (!Objects.equals(this.selectedHrsList, other.selectedHrsList)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "DayVO{" + "label=" + label + ", day=" + day + ", selectedHrsList=" + selectedHrsList + '}';
    }

}
