/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.utils.vo;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author twilcox
 */
public class TachometerVO implements Serializable {
    private String customerAccount;
    private String tachName;
    private UUID tachId;
    private String siteName;
    private UUID siteId;
    private float tachReferenceSpeed;
    private String tachType;
    float fpm;
    float rollDiameter;
    private String tachReferenceUnits;

    public TachometerVO() {
    }

    public TachometerVO(TachometerVO tachometerVO) {
        customerAccount = tachometerVO.getCustomerAccount();
        tachName = tachometerVO.getTachName();
        tachId = tachometerVO.getTachId();
        siteName = tachometerVO.getSiteName();
        siteId = tachometerVO.getSiteId();
        tachReferenceSpeed = tachometerVO.getTachReferenceSpeed();
        tachType = tachometerVO.getTachType();
        fpm = tachometerVO.getFpm();
        rollDiameter = tachometerVO.getRollDiameter();
        tachReferenceUnits = tachometerVO.getTachReferenceUnits();
    }

    public TachometerVO(String customerAccount, String tachName, UUID tachId, String siteName, UUID siteId, float tachReferenceSpeed, String tachType, float fpm, float rollDiameter, String tachReferenceUnits) {
        this.customerAccount = customerAccount;
        this.tachName = tachName;
        this.tachId = tachId;
        this.siteName = siteName;
        this.siteId = siteId;
        this.tachReferenceSpeed = tachReferenceSpeed;
        this.tachType = tachType;
        this.fpm = fpm;
        this.rollDiameter = rollDiameter;
        this.tachReferenceUnits = tachReferenceUnits;
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public String getTachName() {
        return tachName;
    }

    public void setTachName(String tachName) {
        this.tachName = tachName;
    }

    public UUID getTachId() {
        return tachId;
    }

    public void setTachId(UUID tachId) {
        this.tachId = tachId;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public float getTachReferenceSpeed() {
        return tachReferenceSpeed;
    }

    public void setTachReferenceSpeed(float tachReferenceSpeed) {
        this.tachReferenceSpeed = tachReferenceSpeed;
    }

    public String getTachType() {
        return tachType;
    }

    public void setTachType(String tachType) {
        this.tachType = tachType;
    }

    public float getFpm() {
        return fpm;
    }

    public void setFpm(float fpm) {
        this.fpm = fpm;
    }

    public float getRollDiameter() {
        return rollDiameter;
    }

    public void setRollDiameter(float rollDiameter) {
        this.rollDiameter = rollDiameter;
    }

    public String getTachReferenceUnits() {
        return tachReferenceUnits;
    }

    public void setTachReferenceUnits(String tachReferenceUnits) {
        this.tachReferenceUnits = tachReferenceUnits;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 31 * hash + Objects.hashCode(this.customerAccount);
        hash = 31 * hash + Objects.hashCode(this.tachName);
        hash = 31 * hash + Objects.hashCode(this.tachId);
        hash = 31 * hash + Objects.hashCode(this.siteName);
        hash = 31 * hash + Objects.hashCode(this.siteId);
        hash = 31 * hash + Float.floatToIntBits(this.tachReferenceSpeed);
        hash = 31 * hash + Objects.hashCode(this.tachType);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TachometerVO other = (TachometerVO) obj;

        if (Float.floatToIntBits(this.tachReferenceSpeed) != Float.floatToIntBits(other.tachReferenceSpeed)) {
            return false;
        }

        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.tachName, other.tachName)) {
            return false;
        }
        if (!Objects.equals(this.siteName, other.siteName)) {
            return false;
        }

        if (!Objects.equals(this.tachType, other.tachType)) {
            return false;
        }
        if (!Objects.equals(this.tachId, other.tachId)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "TachometerVO{" + "customerAccount=" + customerAccount + ", tachName=" + tachName + ", tachId=" + tachId + ", siteName=" + siteName + ", siteId=" + siteId + ", tachReferenceSpeed=" + tachReferenceSpeed + ", tachType=" + tachType + ", fpm=" + fpm + ", rollDiameter=" + rollDiameter + ", tachReferenceUnits=" + tachReferenceUnits + '}';
    }

}
