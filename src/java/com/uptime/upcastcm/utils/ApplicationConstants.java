/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.utils;

/**
 *
 * @author twilcox
 */
public class ApplicationConstants {
    public static final String DEFAULT_UPTIME_ACCOUNT = "UPTIME";
    public static final String DEFAULT_UPTIME_NAME = "Uptime Solutions";
    public static final String DEFAULT_UPTIME_SIGNATURE = "Uptime360.com";
    public static final String DEFAULT_TIME_ZONE = "UTC";
    public static final String DEFAULT_LOCALE_DISPLAY_NAME = "English (United States)";
    public static final String DEFAULT_SKIN = "uptime";
    public static final String DEFAULT_LOGO = "Uptime_Solutions.png";
    public static final String HOME_PAGE_REDIRECT = "/user/home.xhtml?faces-redirect=true";
    public static final String TIME_ZONE_SITE_DEFAULT = "Site Default";
    
    // Regex
    public static final String REGEX_INCLUDE_STORM_SERIAL_NUMBER = "^((BB|BA)[0-9,A-F]{6})|(001[0-9,A-F]{5})$";
    public static final String REGEX_EXCLUDE_STORM_SERIAL_NUMBER = "^((BB)[0-9,A-F]{6})|(001[0-9,A-F]{5})$";
    public static final String REGEX_ECHO_SERIAL_NUMBER = "^(BC)[0-9,A-F]{6}$";
    public static final String REGEX_EMAIL = "(?:[a-zA-Z0-9!#$%&'*+\\/=?^_{|}~-]+(?:\\.[a-zA-Z0-9!#$%&'*+\\/=?^_{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?|\\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-zA-Z0-9-]*[a-zA-Z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
    
    // Nav Constants
    public static final String ROOT = "root";
    public static final String EMPTY = "empty";
    public static final String PRE_NODE_ICON = "pre-node-icon";
    public static final String YEAR = "year";
    public static final String REGION = "region";
    public static final String COUNTRY = "country";
    public static final String SITE = "site";
    public static final String SITE_OVER_100 = "site_over_100";
    public static final String AREA = "area";
    public static final String AREA_OVER_100 = "area_over_100";
    public static final String ASSET = "asset";
    public static final String ASSET_OVER_100 = "asset_over_100";
    public static final String POINT_LOCATION = "point_location";
    public static final String POINT_LOCATION_OVER_100 = "point_location_over_100";
    public static final String POINT = "point";
    public static final String POINT_OVER_100 = "point_over_100";
    public static final String AP_SET = "ap_set";
    public static final String AP_SET_OVER_100 = "ap_set_over_100";
    public static final String AL_SET_PARAM = "al_set_param";
    public static final String AL_SET_PARAM_OVER_100 = "al_set_param_over_100";
    
    // Service names
    public static final String ALARM_SERVICE_NAME = "Alarms";
    public static final String CONFIGURATION_SERVICE_NAME = "Config";
    public static final String EMAIL_SERVICE_NAME = "Email";
    public static final String EVENT_SERVICE_NAME = "Events";
    public static final String KAFKA_PROXY_SERVICE_NAME = "KafkaProxy";
    public static final String LDAP_SERVICE_NAME = "LDAP";
    public static final String MONITORING_SERVICE_NAME = "Monitoring";
    public static final String PRESET_SERVICE_NAME = "Presets";
    public static final String USER_PREFERENCE_SERVICE_NAME = "User";
    public static final String WAVEFORM_SERVICE_NAME = "WaveformData";
    public static final String WORK_ORDER_SERVICE_NAME = "WorkOrder";
}
