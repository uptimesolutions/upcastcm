/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.utils;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author kpati
 */
public class SessionUtils {

	public static HttpSession getSession() {
            return (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
	}

	public static HttpServletRequest getRequest() {
            return (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
	}
	
        public static HttpServletResponse getResponse() {
            return (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
	}

	public static String getUserId() {
            HttpSession session;
            
            return ((session = getSession()) != null) ? (String) session.getAttribute("userid") : null;
	}
}
