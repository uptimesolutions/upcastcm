/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.dao;


import static com.uptime.client.http.servlet.AbstractServiceManagerServlet.getPropertyValue;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This DAO encapsulates the interactions with the underlying PrestoDB
 * @author ksimmons
 */
public class PrestoDAO {
    //private static final Logger LOGGER = LoggerFactory.getLogger(PrestoDAO.class.getName());
    private final String URL = getPropertyValue("PRESTO_URL");
    private final String SCHEMA = getPropertyValue("PRESTO_CASSANDRA_SCHEMA");
    //private final String URL = "jdbc:presto://10.1.11.37:8080/?SSL=false";

    public PrestoDAO() throws ClassNotFoundException{
        //Register JDBC driver
        Class.forName("com.facebook.presto.jdbc.PrestoDriver");
    }
    
    /**
     * Gets a list of the area, asset, point location and device serial number for the given customer and site.
     * @param customerId
     * @param siteName the name of the site (not the ID)
     * @return Results are returned as a JSON.
     */
    public String getDeviceMapping(String customerId, String siteName){
        StringBuilder query, result;
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs;
        int count;
                
        // create the query
        query = new StringBuilder();
        query
                .append("SELECT location.device_serial_number as deviceId, site.site_name as siteName, area.area_name as areaName, asset.asset_name as assetName, location.point_location_name as pointLocation ")
                .append("FROM ").append(SCHEMA).append(".point_locations location ")
                .append("INNER JOIN ").append(SCHEMA).append(".site_customer site ON location.site_id=site.site_id AND site.customer_acct='").append(customerId).append("' ")
                .append("INNER JOIN ").append(SCHEMA).append(".area_site area ON location.area_id=area.area_id ")//AND area.customer_acct='").append(customerId).append("' AND area.site_id=site.site_id ");
                .append("INNER JOIN ").append(SCHEMA).append(".asset_site_area asset ON location.asset_id=asset.asset_id ")//AND asset.customer_acct='").append(customerId).append("' AND asset.site_id=site.site_id ");
                .append("WHERE site.customer_acct='").append(customerId).append("' AND site.site_name='").append(siteName).append("'");

        result = new StringBuilder();
        result
                .append("{\"customerId\":\"").append(customerId).append("\",")
                .append("\"siteName\":\"").append(siteName).append("\",")
                .append("\"devices\":[");
        try{
            // connect to presto
            conn = DriverManager.getConnection(URL,"username","");
            stmt = conn.createStatement();
            count = 0;
            
            // execute the query
            rs = stmt.executeQuery(query.toString()); 
            while(rs.next()) {
                result
                        .append("{\"areaName\":\"").append(rs.getString("areaName")).append("\",")
                        .append("\"assetName\":\"").append(rs.getString("assetName")).append("\",")
                        .append("\"pointLocationName\":\"").append(rs.getString("pointLocation")).append("\",")
                        .append("\"deviceId\":\"").append(rs.getString("deviceId")).append("\"},");
                count++;
            }
            if (count > 0) {
                result.deleteCharAt(result.length() - 1);
            }
        } catch(Exception e){
            Logger.getLogger(PrestoDAO.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        } finally{
            try{
                if(stmt != null) {
                    stmt.close();
                }
                if(conn != null) {
                    conn.close();
                }
            } catch(Exception e){
                Logger.getLogger(PrestoDAO.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            }
        }
        result.append("]}");
        
        return result.toString();
    }
    
    /**
     * Gets the timestamp of the last sample received for each airbase at the given customer and site
     * @param customerId
     * @param siteName the name of the site (not the ID)
     * @return Results are returned as a JSON.
     */
    public String getBasestationStatus(String customerId, String siteName){
        StringBuilder query, result;
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs;
        int count;
        
        // create the query
        query = new StringBuilder();
        query
                .append("SELECT customerId,siteName,baseStationId,max(eventTimestampMillis) as LastSampleReceived ")
                .append("FROM pinot.default.baseStationTelemetry ")
                .append("WHERE customerId='").append(customerId).append("' AND siteName='").append(siteName).append("' ")
                .append("GROUP BY customerId,siteName,baseStationId");
        
        result = new StringBuilder();
        result
                .append("{\"customerId\":\"").append(customerId).append("\",")
                .append("\"siteName\":\"").append(siteName).append("\",")
                .append("\"baseStations\":[");
        
        try{
            // connect to presto
            conn = DriverManager.getConnection(URL,"username","");
            stmt = conn.createStatement();
            count = 0;
            
            // execute the query
            rs = stmt.executeQuery(query.toString()); 
            while(rs.next()) {
                result.append("{\"baseStationId\":\"").append(rs.getString("baseStationId")).append("\",");
                result.append("\"lastSampleReceived\":\"").append(rs.getString("LastSampleReceived")).append("\"},");
                count++;
            }
            if(count > 0) {
                result.deleteCharAt(result.length() - 1);
            }
        } catch(Exception e){
            Logger.getLogger(PrestoDAO.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        } finally{
            try{
                if(stmt != null) {
                    stmt.close();
                }
                if(conn != null) {
                    conn.close();
                }
            } catch(Exception e){
                Logger.getLogger(PrestoDAO.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            }
        }
        result.append("]}");
        
        return result.toString();
    }
    
    /**
     * Gets the devices for each asset for the given customer and site
     * @param customerId
     * @param siteName
     * @return JSON containing the device IDs for each asset
     */
    public String getDevicesByCustomerSite(String customerId, String siteName){
        StringBuilder query, result;
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs;
        int count;
        
        // create the query
        query = new StringBuilder();
        query.append("SELECT assetId,deviceId,max(sampleTimeMillis) as LastSampleReceived ")
                .append("FROM pinot.default.acSampleConfig ")
                .append("WHERE customerId='").append(customerId).append("' AND siteName='").append(siteName).append("' ")
                .append("GROUP BY assetId,deviceId");
        
        result = new StringBuilder();
        result.append("{\"customer_id\":\"").append(customerId).append("\",");
        result.append("\"site_name\":\"").append(siteName).append("\",");
        result.append("\"devices\":[");
        
        Logger.getLogger(PrestoDAO.class.getName()).log(Level.INFO, "Presto Query:"+query.toString());
        
        try{
            // connect to presto
            conn = DriverManager.getConnection(URL,"username","");
            stmt = conn.createStatement();
            count = 0;
            
            // execute the query
            rs = stmt.executeQuery(query.toString()); 
            while(rs.next()) {
                result.append("{\"asset_id\":\"").append(rs.getString("assetId")).append("\",");
                result.append("\"device_id\":\"").append(rs.getString("deviceId")).append("\"},");
                count++;
            }
            if(count > 0) {
                result.deleteCharAt(result.length() - 1);
            }
            result.append("]}");
        } catch(Exception e){
            e.printStackTrace();
            Logger.getLogger(PrestoDAO.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        } finally{
            try{
                if(stmt != null) {
                    stmt.close();
                }
                if(conn != null) {
                    conn.close();
                }
            } catch(Exception e){
                Logger.getLogger(PrestoDAO.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            }
        }
        
        return result.toString();
    }
    
    /**
     * Gets the PointLocations for each asset for the given customer, site, area & asset
     * @param customerId
     * @param siteId
     * @param areaId     
     * @param assetId
     * @return JSON containing the point location details for each asset
     */
    public String getPointLocationsByAsset(String customerId, String siteId, String areaId, String assetId){
        StringBuilder query, result;
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs;
        int count;
        
        // create the query
    /*    query = new StringBuilder();
        query.append("SELECT assetId,deviceId,max(sampleTimeMillis) as LastSampleReceived ")
                .append("FROM pinot.default.acSampleConfig ")
                .append("WHERE customerId='").append(customerId).append("' AND siteName='").append(siteName).append("' ")
                .append("GROUP BY assetId,deviceId");
        
        result = new StringBuilder();
        result.append("{\"customer_id\":\"").append(customerId).append("\",");
        result.append("\"site_name\":\"").append(siteName).append("\",");
        result.append("\"devices\":[");
        
        Logger.getLogger(PrestoDAO.class.getName()).log(Level.INFO, "Presto Query:"+query.toString());
        
        try{
            // connect to presto
            conn = DriverManager.getConnection(URL,"username","");
            stmt = conn.createStatement();
            count = 0;
            
            // execute the query
            rs = stmt.executeQuery(query.toString()); 
            while(rs.next()) {
                result.append("{\"asset_id\":\"").append(rs.getString("assetId")).append("\",");
                result.append("\"device_id\":\"").append(rs.getString("deviceId")).append("\"},");
                count++;
            }
            if(count > 0) {
                result.deleteCharAt(result.length() - 1);
            }
            result.append("]}");
        } catch(Exception e){
            e.printStackTrace();
            Logger.getLogger(PrestoDAO.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        } finally{
            try{
                if(stmt != null) {
                    stmt.close();
                }
                if(conn != null) {
                    conn.close();
                }
            } catch(Exception e){
                Logger.getLogger(PrestoDAO.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            }
        }
        
        */
        
    //    return result.toString();
        return "{\n" +
"	\"customerAcct\": \"FEDEXEXPRESS\",\n" +
"	\"siteId\": \"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\n" +
"	\"areaId\": \"1d122a59-11e1-46cd-aa28-7d7c23689f12\",\n" +
"	\"assetId\": \"2d122a59-11e1-46cd-aa28-7d7c23689f12\",\n" +
"	\"pointLocations\": [\n" +
"		{\n" +
"			\"pointLocationId\": \"035742fd-3665-4c9d-9917-09fba5b88c5f\",\n" +
"			\"points\": [\n" +
"				{\n" +
"					\"pointId\": \"a62bca07-25ac-4106-9697-3a641e96f5ad\",\n" +
"					\"pointName\": \"MIB-H1\",\n" +
"					\"apSetId\": \"8e8904a9-403a-440a-ad5f-ef411044e951\",\n" +
"					\"lastSampled\": \"2023-09-20T10:26:56Z\",\n" +
"					\"params\": [\n" +
"						{\n" +
"							\"paramName\": \"Overall\",\n" +
"							\"paramType\": \"TotalSpectralEnergy\",\n" +
"							\"paramUnits\": \"Gs\",\n" +
"							\"paramAmpFactor\": \"RMS\",\n" +
"							\"freqUnits\": \"null\",\n" +
"							\"maxFreq\": -1,\n" +
"							\"minFreq\": -1,\n" +
"							\"value\": 0.045654\n" +
"						},\n" +
"						{\n" +
"							\"paramName\": \"Band2-10X\",\n" +
"							\"paramType\": \"SpectralBand\",\n" +
"							\"paramUnits\": \"Gs\",\n" +
"							\"paramAmpFactor\": \"RMS\",\n" +
"							\"freqUnits\": \"Orders\",\n" +
"							\"maxFreq\": 2,\n" +
"							\"minFreq\": 10,\n" +
"							\"value\": 3.25123\n" +
"						}\n" +
"					]\n" +
"				},\n" +
"				{\n" +
"					\"pointId\": \"a62bca07-25ac-4106-9697-3a641e96f5ac\",\n" +
"					\"pointName\": \"MIB-V1\",\n" +
"					\"apSetId\": \"8e8904a9-403a-440a-ad5f-ef411044e951\",\n" +
"					\"lastSampled\": \"2023-10-26T19:04:17Z\",\n" +
"					\"params\": [\n" +
"						{\n" +
"							\"paramName\": \"Overall\",\n" +
"							\"paramType\": \"TotalSpectralEnergy\",\n" +
"							\"paramUnits\": \"GS\",\n" +
"							\"paramAmpFactor\": \"RMS\",\n" +
"							\"freqUnits\": \"Orders\",\n" +
"							\"maxFreq\": 2,\n" +
"							\"minFreq\": 10,\n" +
"							\"value\": 0.05\n" +
"						},\n" +
"						{\n" +
"							\"paramName\": \"TotalSubSync\",\n" +
"							\"paramType\": \"TotalSpectralEnergy\",\n" +
"							\"paramUnits\": \"GS\",\n" +
"							\"paramAmpFactor\": \"RMS\",\n" +
"							\"freqUnits\": \"Orders\",\n" +
"							\"maxFreq\": 2,\n" +
"							\"minFreq\": 10,\n" +
"							\"value\": 2.1\n" +
"						}\n" +
"					]\n" +
"				}\n" +
"			]\n" +
"		},\n" +
"		{\n" +
"			\"pointLocationId\": \"e39034e4-9ec7-472e-9d27-fc4fd69eb3f7\",\n" +
"			\"points\": [\n" +
"				{\n" +
"					\"pointId\": \"083e16fc-0142-4bb5-849f-49329a182177\",\n" +
"					\"pointName\": \"MIB-H2\",\n" +
"					\"apSetId\": \"8e8904a9-403a-440a-ad5f-ef411044e951\",\n" +
"					\"lastSampled\": \"2023-10-26T20:12:37Z\", \n" +
"					\"params\": [\n" +
"						{\n" +
"							\"paramName\": \"Overall\",\n" +
"							\"paramType\": \"TotalSpectralEnergy\",\n" +
"							\"paramUnits\": \"Gs\",\n" +
"							\"paramAmpFactor\": \"RMS\",\n" +
"							\"freqUnits\": \"null\",\n" +
"							\"maxFreq\": -1,\n" +
"							\"minFreq\": -1,\n" +
"							\"value\": 0.9\n" +
"						},\n" +
"						{\n" +
"							\"paramName\": \"Band2-10X\",\n" +
"							\"paramType\": \"SpectralBandTotalSpectralEnergy\",\n" +
"							\"paramUnits\": \"Gs\",\n" +
"							\"paramAmpFactor\": \"RMS\",\n" +
"							\"freqUnits\": \"Orders\",\n" +
"							\"maxFreq\": 2,\n" +
"							\"minFreq\": 10,\n" +
"							\"value\": 1.25\n" +
"						}\n" +
"					]\n" +
"				},\n" +
"				{\n" +
"					\"pointId\": \"a62bca07-25ac-4106-9697-3a641e96f5aa\",\n" +
"					\"pointName\": \"MIB-V2\",\n" +
"					\"apSetId\": \"8e8904a9-403a-440a-ad5f-ef411044e951\",\n" +
"					\"lastSampled\": \"2023-10-26T19:04:17Z\",\n" +
"					\"params\": [\n" +
"						{\n" +
"							\"paramName\": \"Overall\",\n" +
"							\"paramType\": \"TotalSpectralEnergy\",\n" +
"							\"paramUnits\": \"GS\",\n" +
"							\"paramAmpFactor\": \"RMS\",\n" +
"							\"freqUnits\": \"Orders\",\n" +
"							\"maxFreq\": 2,\n" +
"							\"minFreq\": 10,\n" +
"							\"value\": 0.3\n" +
"						},\n" +
"						{\n" +
"							\"paramName\": \"TotalSubSync\",\n" +
"							\"paramType\": \"TotalSpectralEnergy\",\n" +
"							\"paramUnits\": \"GS\",\n" +
"							\"paramAmpFactor\": \"RMS\",\n" +
"							\"freqUnits\": \"Orders\",\n" +
"							\"maxFreq\": 2,\n" +
"							\"minFreq\": 10,\n" +
"							\"value\": 8.03\n" +
"						}\n" +
"					]\n" +
"				}\n" +
"			]\n" +
"		}\n" +
"	]\n" +
"}";
        
    }

    public static void main(String[] args){
        PrestoDAO dao;
        String s;
        
        try{
            dao = new PrestoDAO();
            //s = dao.getDeviceMapping("55180","OCAL");
            s = dao.getDevicesByCustomerSite("90000", "Orange Park");
            System.out.println(s);
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }
}