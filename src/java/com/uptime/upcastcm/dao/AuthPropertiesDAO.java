/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.dao;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Properties;
import javax.servlet.ServletContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author ksimmons
 */
public class AuthPropertiesDAO {
    private static final Logger LOGGER = LoggerFactory.getLogger(AuthPropertiesDAO.class.getName());
    private Properties properties;
    private ServletContext context;
    
    private HashMap<String, String> accountSubdomainMap;
    
    private AuthPropertiesDAO(){
    }
    
    public AuthPropertiesDAO(ServletContext context){
        this.context = context;
        properties = new Properties();
        readPropertiesFile();
        PropertyFileToHashMap();
    }
    
    private void readPropertiesFile(){
        try(FileInputStream input = new FileInputStream(context.getRealPath("/")+"WEB-INF/classes/auth.properties")){
            properties.load(input);
            input.close();
        } catch(IOException e){
            LOGGER.error(e.getMessage(), e);
        }
    }
    
    /**
     * Gets the contents of the specified saml.properties file
     * @param fileName the saml.properties
     * @return 
     */
    public Properties getSamlPropertiesFile(String fileName){
        Properties samlProperties = null;
        
        try(FileInputStream input = new FileInputStream(context.getRealPath("/")+"WEB-INF/classes/" + fileName)){
            samlProperties = new Properties();
            samlProperties.load(input);
            input.close();
        } catch(IOException e){
            LOGGER.error(e.getMessage(), e);
        }
        return samlProperties;
    }
    
    
    private void PropertyFileToHashMap() {
        accountSubdomainMap = new HashMap();
        loadProperties(context.getRealPath("/") + "WEB-INF/classes/auth.properties");
    }
    
    
    private void loadProperties(String filePath) {
        String accountNumber, subdomain;

        try (FileInputStream fis = new FileInputStream(filePath)) {
            properties.load(fis);

            // Iterate over the property keys
            for (String key : properties.stringPropertyNames()) {
                
                // Check if the key contains "account.number" to identify account entries
                if (key.endsWith("account.number")) {
                    accountNumber = properties.getProperty(key);

                    // get the subdomain of the key
                    subdomain = key.split("\\.")[0];
                    
                    // Put the account number and subdomain in the map
                    accountSubdomainMap.put(accountNumber, subdomain);
                }
            }
            
            fis.close();
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    
    /**
     * Gets the authentication type for the given subdomain
     * @param subdomain
     * @return the authentication type (saml or ldap)
     */
    public String getAuthType(String subdomain){
        return properties.getProperty((subdomain+".auth.type"));
    }
    
    /**
     * Gets the file location of the saml.properties for the given subdomain
     * @param subdomain
     * @return the file location of the saml.properties
     */
    public String getSamlProperties(String subdomain){
        return properties.getProperty((subdomain+".saml.properties"));
    }
    
    /**
     * Gets the account number for the given subdomain
     * @param subdomain
     * @return the account number
     */
    public String getAccountNumber(String subdomain){
        return properties.getProperty((subdomain+".account.number"));
    }
    
    /**
     * Gets the account name for the given subdomain
     * @param subdomain
     * @return the account name
     */
    public String getAccountName(String subdomain){
        return properties.getProperty((subdomain+".account.name"));
    }
    
     /**
     * Gets the subdomain name for the given accountNumber
     * @param accountNumber
     * @return the subdomain
     */
    public String getSubDomain(String accountNumber) {
        return accountSubdomainMap.get(accountNumber);
    }
}
