/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.featuresflag.enums;

import org.togglz.core.Feature;
import org.togglz.core.annotation.EnabledByDefault;
import org.togglz.core.annotation.Label;
import org.togglz.core.context.FeatureContext;

/**
 *
 * @author kpati
 */
public class UptimeFeaturesEnum {

    public enum UptimeFeatures  implements Feature {
        
        @EnabledByDefault
        @Label("Configuration")
        DISPLAY_CONFIG,
        
        @Label("Presets")
        DISPLAY_PRESETS,
        
        @Label("Workorder")
        DISPLAY_WORKORDER,
        
        @Label("StormX")
        DISPLAY_STORMX;
        
        public boolean isActive() {
            return FeatureContext.getFeatureManager().isActive(this);
        }
    }
}
