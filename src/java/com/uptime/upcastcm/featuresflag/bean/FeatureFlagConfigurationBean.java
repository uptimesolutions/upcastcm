/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.upcastcm.featuresflag.bean;

import com.uptime.upcastcm.featuresflag.enums.UptimeFeaturesEnum;
import java.io.File;
import org.togglz.core.Feature;
import org.togglz.core.manager.TogglzConfig;
import org.togglz.core.repository.StateRepository;
import org.togglz.core.repository.file.FileBasedStateRepository;
import org.togglz.core.user.FeatureUser;
import org.togglz.core.user.SimpleFeatureUser;
import org.togglz.core.user.UserProvider;

/**
 *
 * @author kpati
 */
public class FeatureFlagConfigurationBean implements TogglzConfig {

    /**
     * JAR TO BE ADDED TO ENABLED THIS FEATURE
     togglz-core-3.1.2.jar, 
     togglz-console-3.1.2.jar,
     togglz-servlet-3.1.2.jar, 
     togglz-jsf-2.8.0.jar,
     commons-collections4-4.1.jar
     * @return 
     */
    @Override
    public Class<? extends Feature> getFeatureClass() {
        return UptimeFeaturesEnum.UptimeFeatures.class;
    }

    @Override
    public StateRepository getStateRepository() {
        return new FileBasedStateRepository(new File("uptime-features.properties"));
    }

    @Override
    public UserProvider getUserProvider() {
        return new UserProvider() {
            @Override
            public FeatureUser getCurrentUser() {
                return new SimpleFeatureUser("admin", true);
            }
        };
    }

}
