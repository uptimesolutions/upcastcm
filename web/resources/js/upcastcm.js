var pageSize;

// alarms.xhtml
function selectAlarm(rowNo) {
    $('#alarm-form\\:dt-alarms_data > tr').each(function () {
        if ($(this).attr('data-ri') == rowNo && $(this).attr('aria-selected') == 'false') {
            $('.rowCheck' + rowNo).click();
        }
    });
}

// copy-edit-fault-frequencies.xhtml
function validateHarmonic(message) {
    if ($('#copyEditFaultFrequenciesFormId\\:ffStartHarmonic_input').val() < $('#copyEditFaultFrequenciesFormId\\:ffEndHarmonic_input').val()) {
        $('#copyEditFaultFrequenciesFormId\\:faultFrequenciesSaveId1').click();
    } else {
        alert(message);
    }
}

// create-fault-frequencies.xhtml
function validateCreateHarmonic(message) {
    if ($('#createFaultFrequenciesFormId\\:ffStartHarmonic_input').val() < $('#createFaultFrequenciesFormId\\:ffEndHarmonic_input').val()) {
        $('#createFaultFrequenciesFormId\\:faultFrequenciesSaveId1').click();
    } else {
        alert(message);
    }
}

// Used to update the leftTreeFormId
function updateTreeNodes() {
    $('#leftTreeFormId').find('.ui-treenode-content').removeClass('treenode-selected-path');
    $('.ui-treenode-label.ui-state-highlight').parents('.ui-treenode').children('.ui-treenode-content').addClass('treenode-selected-path');
}

// Used to update to add a div with the class 'timeline-topbar-cover' in the html tag timeline
function addTimelineTopbarCover() {
    $('.vis-top').append('<div class="timeline-topbar-cover" />');
}

function copyPrimaryKey(id) {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val($(id).html()).select();
    document.execCommand("copy");
    $temp.remove();
}

// Hides the all opened tooltips         
function hideTooltips() {
    $('.ui-tooltip').css('display', 'none');
}

// Close Asset Analysis Window
function closeAssetAnalysisWindow(windowId) {
    let openedWindow;

    openedWindow = window.open('', windowId, '');
    openedWindow.close();
}

// Open browser window and add id to localStorage
function openChartInNewBrowser(path, dialogId) {
    var child;
    var childWindows = localStorage.getItem("windowNames");

    if (!childWindows) {
        childWindows = "";
    }
    var left = screen.width - 850;
    var top = 0;
    var features = 'width=850,height=600,toolbar=0,location=0,directories=0,status=0,location=no,menubar=0,scrollbars=yes,resizable=yes,top=' + top + ',left=' + left;

    child = window.open(path, dialogId, features);
    childWindows += dialogId + ',';
    localStorage.setItem("windowNames", childWindows);
}

// Close Asset Analysis Window
function closeAllWindows() {
    let windowNames, winNames;

    windowNames = localStorage.getItem("windowNames");
    if (windowNames) {
        winNames = windowNames.split(",");
        winNames.forEach(function (value) {
            let openedWindow;
            openedWindow = window.open('', value, '');
            openedWindow.close();
        });
        localStorage.removeItem("windowNames");
    }
}

function readPageSize(id) {
    pageSize = $('[name$="' + id + '"]').val();
}
//function setPageSize(id) {
//    $('[name$="' + id + '"]').val(pageSize);
//}
 
 function setPageSize(id,defaultSize) {
    if(pageSize !==defaultSize){
        $('[name$="' + id + '"]').val(defaultSize);
       $('[name$="' + id + '"]').change();
    }
}

function downloadErrorCsv() {
    $('#confirmImportConfigDialogForm\\:hiddenDownloadButton1').click();
}

function assetTypeChange() {
    if ($('#configurationResultFormId\\:assetType1_input').val() !== '') {
        $('#configurationResultFormId\\:assetType1').removeClass('ui-state-error');
    }
}
